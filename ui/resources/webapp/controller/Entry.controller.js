/*global location */
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"com/adweko/adapter/osx/ui/utils/ClientSecurity"
], function (Controller, JSONModel, MessageBox, ClientSecurity) {
	"use strict";

	return Controller.extend("com.adweko.adapter.osx.ui.controller.AccessDenied", {
		onInit: function () {

			this.loadUserContext();
		},

		loadUserContext: function () {
			var oController = this;

			ClientSecurity.checkScope("Display", this, function (bResult) {
				if (bResult) {
					$.ajax({
						type: 'GET',
						url: '/',
						headers: {
							'X-Csrf-Token': 'Fetch'
						},
						success: function (res, status, xhr) {
							var sHeaderCsrfToken = 'X-Csrf-Token';
							var sCsrfToken = xhr.getResponseHeader(sHeaderCsrfToken);

							$.ajax({
								url: "/userContext.xsjs",
								beforeSend: function (xhrP) {
									xhrP.setRequestHeader(sHeaderCsrfToken, sCsrfToken);
								},
								async: true,
								success: function (oData) {
									sap.ui.getCore().setModel(new JSONModel(oData), "user");
									oController.getOwnerComponent().getRouter().navTo("main");
								}
							});
						}
					});
				} else {
					oController.getOwnerComponent().getRouter().navTo("accessdenied");
				}
			});

			// oModelUserContext
			// 	.attachRequestFailed(function (oEvent) {
			// 		var errorObject = oEvent.getParameters();

			// 		oController._parseResponse(errorObject);
			// 	})
			// 	.attachRequestCompleted(function (oEvent) {
			// 		var dataResponse = oEvent.getSource(),
			// 			errorObject = oEvent.getParameter("errorobject");

			// 		oController._parseResponse(errorObject, dataResponse);
			// 	});
			// sap.ui.getCore().setModel(oModelUserContext, "user");
		},

		_parseResponse: function (oError, oData) {
			if (oError) {
				if (oError.statusCode === 403) {
					this.getOwnerComponent().getRouter().navTo("accessdenied");
				} else {
					jQuery.sap.delayedCall(1000, this, function () {
						var errormsg = "Unable to connect to server. Please check with IT helpdesk, or try again later.".concat(oError.statusCode, ":",
							oError.statusText, ")");
						MessageBox.error(errormsg);
					});
				}
			} else {
				// Verify Access
				var oScopes = oData.getProperty("/scopes");

				if (oScopes.filter(function (row) {
						return (row.endsWith(".Display"));
					}).length === 0) {
					// No Access
					this.getOwnerComponent().getRouter().navTo("accessdenied");
				} else {
					this.getOwnerComponent().getRouter().navTo("main");
				}
			}
		}
	});
});