sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/m/MessageBox",
	"com/adweko/adapter/osx/ui/utils/SmartTable",
	"com/adweko/adapter/osx/ui/utils/ErrorHandler",
	"sap/m/Popover",
	"sap/m/library",
	"com/adweko/adapter/osx/ui/controls/mapping/MappingEditor",
	"com/adweko/adapter/osx/ui/utils/ClientSecurity",
	"com/adweko/adapter/osx/ui/utils/SessionHandler"
], function (Controller, JSONModel, ODataModel, MessageBox, SmartTable, ErrorHandler, Popover, library, MappingEditor, ClientSecurity,
	SessionHandler) {
	"use strict";
	var _VIEW = "view";
	var _MAPPING = "mapping";
	var _CONFIG = "config";
	var _PRODUCTCONFIG = "productConfig";
	var _SAP_UI_TABLE = "sap.ui.table.Table";
	return Controller.extend("com.adweko.adapter.osx.ui.controller.Main", {

		onInit: function () {
			var that = this;
			this._bEditMode = false;
			this._aTables = [];
			this.initViewModel();

			this._iCompletedRequests = 0;
			this._iInitialLoading = true;

			this.setGuiBusy(true);

			this.getView().setModel(
				new sap.ui.model.odata.v2.ODataModel("/xsodata/fsdm2onesumxMapping.xsodata", {
					defaultUpdateMethod: "PUT",
					useBatch: false,
					defaultBindingMode: sap.ui.model.BindingMode.TwoWay
				}),
				_MAPPING
			);
			this.getView().setModel(
				new sap.ui.model.odata.v2.ODataModel("/xsodata/fsdm2onesumxConfiguration.xsodata", {
					defaultUpdateMethod: "PUT",
					defaultBindingMode: sap.ui.model.BindingMode.TwoWay
				}),
				"config"
			);

			this.getView().getModel(_MAPPING).attachRequestCompleted(function () {
				that.onRequestCompleted.call(that);
			});
			this.getView().getModel("config").attachRequestCompleted(function () {
				that.onRequestCompleted.call(that);
			});
			this.loadCodeListMappingMenuItems();
			this.setVersionInfo();
		},
		onRequestCompleted: function (oEvent) {
			if (this._iInitialLoading) {
				this._iCompletedRequests++;

				if (this._iCompletedRequests === 1) {
					this.setGuiBusy(false);
				}
			}
		},

		setVersionInfo: function () {
			var that = this;

			this.getView().getModel(_CONFIG).read("/VersionInfo('OSX2FSDM_RISK_VERSION_NO')", {
				success: function (oData) {
					that.getView().byId("txtVersion").setText("Adapter Version: " + oData.VersionNo);
				},
				error: function (oError) {
					ErrorHandler.handleOdataError(oError);
				}
			});
		},

		onAfterRendering: function () {
			SessionHandler.setSessionListener();
		},

		loadCodeListMappingMenuItems: function () {
			var that = this;

			this.getView().getModel(_MAPPING).read("/CodeListConfiguration", {
				urlParameters: {
					"$expand": "Category"
				},
				success: function (oRetrievedResult) {
					var oModel = that.getView().getModel(_VIEW),
						aItemsCodeList = [],
						aItemsKeyValue = [],
						aItemsStreamBased = [],
						oData = oModel.getData();

					oRetrievedResult.results.forEach(function (oItem) {
						if (oItem.Category.MappingCategory === "CodeListMapping") {
							aItemsCodeList.push(oItem);
						} else if (oItem.Category.MappingCategory === "KeyValueMapping") {
							aItemsKeyValue.push(oItem);
						} else if (oItem.Category.MappingCategory === "StreamBasedMapping") {
							aItemsStreamBased.push(oItem);
						}
					});
					oData.CodeListNavItems = aItemsCodeList;
					oData.KeyValueNavItems = aItemsKeyValue;
					oData.StreamBasedNavItems = aItemsStreamBased;
					oModel.setData(oData);
				},
				error: function (oError) {
					ErrorHandler.handleOdataError(oError);
				}
			});
		},

		getVersionNumber: function (sVersion) {
			return "Adapter Version: " + sVersion;
		},

		setGuiBusy: function (bBusy) {
			this.getView().byId("toolPage").setBusy(bBusy);
			this.byId("sideNavigation").setBusy(bBusy);
		},

		loadProductConfigData: function () {
			var oModel = new sap.ui.model.odata.v2.ODataModel("/xsodata/onesumxProductConfiguration.xsodata", {
				defaultUpdateMethod: "PUT"
			});
			oModel.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
			var that = this;

			this.getView().setModel(oModel, _PRODUCTCONFIG);
			this.getView().byId("toolPage").setBusy(true);

			oModel.attachMetadataLoaded(null, function () {
				that.createProductConfigGui.call(that, oModel);
			}, null);
			oModel.attachMetadataFailed(function (oEvent) {
				var errorObject = oEvent.getParameter("response");

				if (errorObject) {
					if (errorObject.statusCode === 403) {
						that.getOwnerComponent().getRouter().navTo("accessdenied");
					}
				}
			});
		},

		createProductConfigGui: function (oModel) {
			if (oModel) {
				var oEntitySet = this.getEntitySet(oModel),
					that = this;

				if (oEntitySet) {

					var oSet = oEntitySet.find(function (oItem) {
						return oItem.name === _PRODUCTCONFIG;
					});

					if (oSet) {
						var sId = "tbl" + oSet.name,
							mParams = null,
							oTbl = SmartTable.generateTableControl(
								that.getView().createId(sId),
								oModel,
								oSet.name, [],
								this,
								_SAP_UI_TABLE,
								mParams, ["ID"], ["Active"], {
									valueChangeHandle: this.onValueChanged
								}
							),
							oPageContent = this.generatePageContent(oSet.name);

						oPageContent.getContent()[0].getContent()[1].getContent()[0].addContent(oTbl);
					}
				}
			}
			this.getView().byId("toolPage").setBusy(false);
		},

		createDataGroupMappingPage: function (oModel, oMappingConfig) {
			if (oModel) {
				var bPermissionToEdit = ClientSecurity.userHasPermissionToEdit(),
					oSet = SmartTable.getEntityByName("DataGroupMapping", oModel);

				if (oSet) {
					var oTableGenParams, oTbl;
					var sId = "tbl" + oSet.name,
						oPageContent = this.generatePageContent(oSet.name,
							oMappingConfig.MappingTitle,
							oMappingConfig.Description),
						aHiddenCols = ["LastChangeTimestamp","LastChangeApplicationUserName"],
						aToolBarActions = ["Search"];

					oSet.__entityType.property.forEach(function (oProperty) {
						if (oProperty.name !== "ruleName" && oProperty.name !== "Desc" && oProperty.name !== "dataGroup" && oProperty.name !==
							"BusinessValidFrom" && oProperty.name !== "BusinessValidTo") {
							aHiddenCols.push(oProperty.name);
						}
					});

					if (bPermissionToEdit) {
						aToolBarActions.push("UploadCsv");
						aToolBarActions.push("DownloadCsv");
					}

					oTableGenParams = {
						Title: this.getView().getModel("i18n").getResourceBundle().getText("datagroup.assignment_rules"),
						Editable: bPermissionToEdit,
						AllowDeleting: bPermissionToEdit,
						AllowEditing: false,
						ToolbarActions: aToolBarActions,
						Handles: {
							beforeCreateEntry: this.beforeCreateEntry,
							tableGettingBusyHandle: this.tableGettingBusyHandle,
							tableLoadingCompleted: this.tableLoadingCompleted,
							CustomEditHandle: this.editDatagroupHandle,
							oDataLoadedHandle: this.mappingEditorDataLoadedHandle
						},
						HiddenColumns: aHiddenCols
					};
					oTbl = SmartTable.generateTableControl(
						this.getView().createId(sId),
						oModel,
						oSet.name,
						this,
						_SAP_UI_TABLE,
						null,
						oTableGenParams
					);
					if (oTbl) {
						if (bPermissionToEdit) {
							var oToolbar = oTbl.getToolbar(),
								aContents = oToolbar.getContent();
							for( var i = 0; i < aContents.length; i++) {
								var iIndex = oToolbar.indexOfContent(aContents[i]);
								if (aContents[i].sId === "__button1") {
									oToolbar.insertContent(
										new sap.m.Button({
											type: "Default",
											icon: "sap-icon://add",
											tooltip: "{i18n>tooltip_add_entry}",
											press: [this.handlePressAddDataGroupLookupRule, this]
										}),
										iIndex
									);	
									break;
								}
							}
						}
						if (!oTbl.getRowActionTemplate()) {
							oTbl.setRowActionTemplate(new sap.ui.table.RowAction({
								items: []
							}));
						}

						if (oTbl.getRowActionTemplate()) {
							oTbl.getRowActionTemplate().insertItem(
								new sap.ui.table.RowActionItem({
									icon: "sap-icon://detail-view",
									text: "{i18n>tooltip_view_entry}",
									press: [this.viewEditMappings, this],
									visible: true
								}), 0
							);
							oTbl.setRowActionCount(oTbl.getRowActionTemplate().getItems().length);
						}
						oPageContent.getContent()[0].getContent()[1].getContent()[0].addContent(oTbl);
					}
				}
			}
		},

		createStreamBasedMappingPage: function (oModel, oCodeListConfig) {
			if (oModel) {
				var bPermissionToEdit = ClientSecurity.userHasPermissionToEdit(),
					oSet = SmartTable.getEntityByName("ComplexMapping", oModel);

				if (oSet) {
					var oTableGenParams, oTbl;
					var sId = "tbl" + oCodeListConfig.Id,
						oPageContent = this.generatePageContent(oCodeListConfig.Id,
							oCodeListConfig.MappingTitle,
							oCodeListConfig.Description),
						oRscBundle = this.getView().getModel("i18n").getResourceBundle();

					oTableGenParams = {
						Title: oRscBundle.getText("stream_based_complex_mappings_table_title"),
						Editable: bPermissionToEdit,
						AllowEditing: false,
						AllowCreating: false,
						ToolbarActions: ["Search"],
						Handles: {
							beforeCreateEntry: this.beforeCreateEntry,
							tableGettingBusyHandle: this.tableGettingBusyHandle,
							tableLoadingCompleted: this.tableLoadingCompleted,
							CustomEditHandle: this.editComplexMappingHandle,
							oDataLoadedHandle: this.mappingEditorDataLoadedHandle
						},
						HiddenColumns: ["SystemValidFrom", "SystemValidTo", "StreamName", "MappingContent", "KeyValues", "IsComplex", "CustomMapping",
							"Active","LastChangeTimestamp","LastChangeApplicationUserName"
						]
					};
					oTbl = SmartTable.generateTableControl(
						this.getView().createId(sId),
						oModel,
						oSet.name,
						this,
						_SAP_UI_TABLE,
						null,
						oTableGenParams
					);
					if (oTbl) {
						if (!oTbl.getRowActionTemplate()) {
							oTbl.setRowActionTemplate(new sap.ui.table.RowAction({
								items: []
							}));
						}

						if (oTbl.getRowActionTemplate()) {
							oTbl.getRowActionTemplate().addItem(
								new sap.ui.table.RowActionItem({
									icon: "sap-icon://detail-view",
									text: "{i18n>tooltip_view_entry}",
									press: [this.viewEditMappings, this],
									visible: true
								})
							);
							oTbl.setRowActionCount(oTbl.getRowActionTemplate().getItems().length);
						}

						if (bPermissionToEdit && (((!oCodeListConfig.UseMapping || oCodeListConfig.UseMapping2 === "false") && oCodeListConfig.LoadProcedureName) ||
								(
									oCodeListConfig.UseMapping2 === "true" && oCodeListConfig.LoadProcedureName2))) {
							sId = "btnLoadDefaultMapping" + oCodeListConfig.Id;
							oPageContent.getContent()[0].getContent()[0].getContent()[1].addContent(
								new sap.m.Button(this.getView().createId(sId), {
									text: oRscBundle.getText("load_mapping"),
									type: sap.m.ButtonType.Emphasized,
									press: [this.handleLoadMappingPress, this],
									enabled: false
								})
							);
						}

						oPageContent.getContent()[0].getContent()[1].getContent()[0].addContent(oTbl);
					}
				}
			}
		},

		createCodeListMappingPage: function (oModel, oCodeListConfig) {
			if (oModel) {
				var oEntitySet = this.getEntitySet(oModel),
					that = this;

				if (oEntitySet) {
					var bPermissionToEdit = ClientSecurity.userHasPermissionToEdit();

					var oSet = oEntitySet.find(function (oItem) {
						return oItem.name === oCodeListConfig.MappingSrvEntityName;
					});

					if (oSet) {
						var oTableGenParams, oTbl, oPageContent;
						var sId = "tbl" + oSet.name;
						var aCodeListBindings = [],
							oRscBundle = this.getView().getModel("i18n").getResourceBundle(),
							bAllowCreating = (oCodeListConfig.AllowCreating === "true" || oCodeListConfig.AllowCreating === "TRUE" ? true : false),
							bAllowDeleting = (oCodeListConfig.AllowDeleting === "true" || oCodeListConfig.AllowDeleting === "TRUE" ? true : false),
							aToolBarActions = ["Search"];

						if (bPermissionToEdit && bAllowCreating) {
							aToolBarActions.push("Add");
							aToolBarActions.push("UploadCsv");
							aToolBarActions.push("DownloadCsv");
						}

						if (oCodeListConfig.CodeListSourceSrvEntityName && oCodeListConfig.CodeListSourceSrvAttribute) {
							aCodeListBindings[oCodeListConfig.MappingKeyAttribute] = {
								Path: oCodeListConfig.CodeListSourceSrvEntityName.concat("/", oCodeListConfig.CodeListSourceSrvAttribute),
								AllowCustomValues: (oCodeListConfig.CLS_AllowCustomValues === "true" || oCodeListConfig.CLS_AllowCustomValues === "TRUE" ?
									true : false)
							};
						}
						if (oCodeListConfig.CodeListTargetSrvEntityName && oCodeListConfig.CodeListTargetSrvAttribute) {
							aCodeListBindings[oCodeListConfig.MappingValueAttribute] = {
								Path: oCodeListConfig.CodeListTargetSrvEntityName.concat("/", oCodeListConfig.CodeListTargetSrvAttribute),
								AllowCustomValues: (oCodeListConfig.CLT_AllowCustomValues === "true" || oCodeListConfig.CLT_AllowCustomValues === "TRUE" ?
									true : false)
							};
						}

						oTableGenParams = {
							Editable: bPermissionToEdit,
							AllowEditing: bPermissionToEdit,
							AllowCreating: bPermissionToEdit && oCodeListConfig.AllowCreating,
							AllowDeleting: bPermissionToEdit && bAllowDeleting,
							ToolbarActions: aToolBarActions,
							Handles: {
								beforeCreateEntry: this.beforeCreateEntry,
								tableGettingBusyHandle: this.tableGettingBusyHandle,
								tableLoadingCompleted: this.tableLoadingCompleted
							},
							HiddenColumns: ["SystemValidFrom", "SystemValidTo", "CustomMapping", "IsCustomMapping", "Active","LastChangeTimestamp","LastChangeApplicationUserName"],
							BooleanColumns: ["Active"],
							ColumnBindings: aCodeListBindings
						};
						oTbl = SmartTable.generateTableControl(
							that.getView().createId(sId),
							oModel,
							oSet.name,
							that,
							_SAP_UI_TABLE,
							null,
							oTableGenParams
						);
						oPageContent = that.generatePageContent(oSet.name, oCodeListConfig.MappingTitle, oCodeListConfig.Description);
						oPageContent.getContent()[0].getContent()[1].getContent()[0].addContent(oTbl);

						if (bPermissionToEdit && (((!oCodeListConfig.UseMapping || oCodeListConfig.UseMapping2 === "false") && oCodeListConfig.LoadProcedureName) ||
								(
									oCodeListConfig.UseMapping2 === "true" && oCodeListConfig.LoadProcedureName2))) {
							sId = "btnLoadDefaultMapping" + oSet.name;
							oPageContent.getContent()[0].getContent()[0].getContent()[1].addContent(
								new sap.m.Button(that.getView().createId(sId), {
									text: oRscBundle.getText("load_mapping"),
									type: sap.m.ButtonType.Emphasized,
									press: [this.handleLoadMappingPress, this],
									enabled: false
								})
							);
						}
						that._aTables.push(oTbl);
					}
				}
				this.getView().byId("toolPage").setBusy(false);
			}
		},

		generatePageContent: function (sPageId, sTitle, sDescription) {
			var oPageContent = sap.ui.xmlfragment("com.adweko.adapter.osx.ui.view.PageContent", this),
				oRscBundle = this.getView().getModel("i18n").getResourceBundle(),
				oScrollContainer = new sap.m.ScrollContainer(this.getView().createId(sPageId), {
					horizontal: true,
					vertical: true,
					height: "100%",
					content: [
						oPageContent
					]
				});
			sTitle = sTitle ? sTitle : oRscBundle.getText(sPageId);
			sDescription = sDescription ? sDescription : oRscBundle.getText(sPageId + ".Description");
			oPageContent.getContent()[0].getContent()[0].getContent()[0].addContent(new sap.m.Text({
				text: sDescription
			}));
			oPageContent.getContent()[0].getContent()[0].getContent()[0].setTitle(sTitle);

			this.byId("pageContainer").addPage(oScrollContainer);

			return oPageContent;
		},

		getEntitySet: function (oModel) {
			var oResult;
			if (oModel) {
				var aSchemas = oModel.getServiceMetadata().dataServices.schema;

				aSchemas.forEach(function (oSchema) {
					if (oSchema.hasOwnProperty("entityContainer")) {
						oResult = oSchema.entityContainer[0].entitySet;
					}
				});
			}
			return oResult;
		},

		getMappingEditor: function (sType, sMode) {
			if (!sType) {
				sType = "default";
			}

			if (!sMode) {
				sMode = "view";
			}

			if (!this._oMappingEditor) {
				var RscBundle = this.getView().getModel("i18n").getResourceBundle(),
					that = this;

				this._oMappingEditor = new MappingEditor("test", {
					title: RscBundle.getText("mapping.editor_title_create"),
					type: sType,
					mode: sMode
				});
				this._oMappingEditor.setModel(this.getView().getModel("i18n"));
				this._oMappingEditor.attachOnSubmitMappings(function (oEvent) {
					that.handleMappingSubmitted.call(that, oEvent);
				});
			} else {
				if (sType !== this._oMappingEditor.getType()) {
					this._oMappingEditor.setType(sType);
				}
				if (sMode !== this._oMappingEditor.getMode()) {
					this._oMappingEditor.setMode(sMode);
				}
			}
			return this._oMappingEditor;
		},

		handlePressAddDataGroupLookupRule: function () {
			var oTable = this.byId("tbl" + this.getView().getModel("view").getProperty("/PageKey"));

			this.showMappingEditor(oTable);
		},

		viewEditMappings: function (oEvent) {
			var oTable = oEvent.getSource().getParent().getParent().getParent(),
				iIdx = oEvent.getSource().getParent().getParent().getIndex();

			this._sEditItemPath = oTable.getContextByIndex(iIdx).sPath;
			this.showMappingEditor(oTable, iIdx);
		},

		showMappingEditor: function (oTable, iRowIndex) {
			if (oTable) {
				var oEntry,
					oBondedEntity = SmartTable.getBondedEntity(oTable, this.getView().getModel("config")),
					sType = "default",
					oMappingData,
					oEditor,
					bSetPropertiesAutmatically = true,
					sMode = "edit";

				this._iCurrentRowIndex = iRowIndex;

				if (iRowIndex >= 0) {
					oEntry = oTable.getContextByIndex(iRowIndex).getObject();
				}
				if (oBondedEntity.entityType === "default.ComplexMappingType") {
					if (oEntry) {
						sType = oEntry.IsComplex === "true" || oEntry.IsComplex === "TRUE" || oEntry.MappingContent ? "default" : "simple";
					}
					bSetPropertiesAutmatically = false;
					sMode = "edit_limited";
				}

				if (oEntry) {
					if (oBondedEntity.entityType === "default.ComplexMappingType") {
						oMappingData = this.getComplexMappingObjByEntry(oEntry);
					} else if (oBondedEntity.entityType === "default.DataGroupMappingType") {
						oMappingData = this.getDataGroupMappingObjByEntry(oEntry);
					}

					oEditor = this.getMappingEditor(sType, ClientSecurity.userHasPermissionToEdit() ? sMode : "view");
					oEditor.setMappingData(oMappingData, bSetPropertiesAutmatically);
				} else {
					oEditor = this.getMappingEditor(sType, "create");
					oEditor.setInitialMappingData();
				}

				if (!bSetPropertiesAutmatically && oMappingData && oMappingData.expressions) {
					oEditor.setPropertyList(oEditor.getPropertyListByExpressionList(oMappingData.expressions));
				}
				oEditor.open();
			}
		},

		getComplexMappingObjByEntry: function (oEntry) {
			var oResult;

			if (oEntry) {
				var oMappingData = {
					Name: oEntry.MappingId,
					Description: oEntry.Desc,
					BusinessValidFrom: oEntry.BusinessValidFrom,
					BusinessValidTo: oEntry.BusinessValidTo,
					SourceStream: oEntry.StreamName,
					TargetView: "",
					TargetViewAttribute: oEntry.TargetProperty,
					Value: oEntry.dataGroup,
					SystemValidFrom: oEntry.SystemValidFrom,
					SystemValidTo: oEntry.SystemValidTo,
					KeyValues: oEntry.KeyValues ? JSON.parse(oEntry.KeyValues) : null,
					expressions: oEntry.MappingContent ? JSON.parse(oEntry.MappingContent) : null
				};
				oResult = oMappingData;
			}
			return oResult;
		},

		getDataGroupMappingObjByEntry: function (oEntry) {
			var oResult;

			if (oEntry) {
				var oMappingData = {
						Name: oEntry.ruleName,
						Description: oEntry.Desc,
						BusinessValidFrom: oEntry.BusinessValidFrom,
						BusinessValidTo: oEntry.BusinessValidTo,
						SourceStream: oEntry.StreamName,
						TargetView: "Contract",
						TargetViewAttribute: "dataGroup",
						Value: oEntry.dataGroup,
						SystemValidFrom: oEntry.SystemValidFrom,
						SystemValidTo: oEntry.SystemValidTo,
						expressions: []
					},
					aColsToIgnore = [
						"ruleName",
						"Desc",
						"BusinessValidFrom",
						"BusinessValidTo",
						"SystemValidFrom",
						"SystemValidTo",
						"dataGroup",
						"StreamName",
						"LastChangeTimestamp",
						"LastChangeApplicationUserName"
					],
					iRuleId = 0;

				Object.keys(oEntry).forEach(function (sKey) {
					if (sKey !== "__metadata" && !Object.keys(oMappingData).includes(sKey) && !aColsToIgnore.includes(sKey)) {
						var oValue = oEntry[sKey];

						if (oValue && oValue !== "" && oValue != undefined) {
							iRuleId++;

							oMappingData.expressions.push({
								id: iRuleId,
								parent: null,
								operatorLogical: iRuleId - 1 === 0 ? "None" : "AND",
								property: sKey,
								operator: "EQ",
								value: oValue,
								expressions: []
							});
						}
					}
				});
				oResult = oMappingData;
			}
			return oResult;
		},

		mappingEditorDataLoadedHandle: function (oError) {
			this._oMappingEditor.setBusy(false);

			if (!oError && this._oMappingEditor && this._oMappingEditor.isOpen()) {
				this._oMappingEditor.close();
			}
		},

		handleMappingSubmitted: function (oEvent) {
			if (oEvent) {
				var oData = oEvent.getParameter("mappings"),
					oModel = this.getView().getModel("config"),
					oEntry;

				var oTable = this.byId("tbl" + this.getView().getModel("view").getProperty("/PageKey")),
					oBondedEntity = SmartTable.getBondedEntity(oTable, oModel);

				if (oBondedEntity) {
					this._oMappingEditor.setBusy(true);

					if (oBondedEntity.entityType === "default.ComplexMappingType") {
						var aKeyValues = this._oMappingEditor.getType() === "simple" ?
							oData.KeyValues : this._oMappingEditor.getKeyValuePairsByStaticStreamBasedMapping(oData.expressions);

						oEntry = oTable.getContextByIndex(this._iCurrentRowIndex).getObject();
						oEntry.KeyValues = JSON.stringify(aKeyValues);
						oEntry.MappingContent = oData.expressions ? JSON.stringify(oData.expressions) : null;

						if (this._oMappingEditor.getMode() === "create") {
							if (!SmartTable.entryExistInTable(oEntry, oTable)) {
								SmartTable.createEntry(oTable, oEntry);
							} else {
								this.showInfoPrimaryKeyNotUnique();
								this._oMappingEditor.setBusy(false);
								this._oMappingEditor.setKeyNotUniqueState();
							}
						} else {
							SmartTable.updateEntry(oTable, oEntry, this._sEditItemPath);
						}
					} else if (oBondedEntity.entityType === "default.DataGroupMappingType") {
						if (this._oMappingEditor.getMode() === "create") {
							oEntry = SmartTable.createOdataEntryByTableBinding(oTable, oModel);
							oEntry.ruleName = oData.Name;
							oEntry.BusinessValidFrom = oData.BusinessValidFrom;
							oEntry.BusinessValidTo = oData.BusinessValidTo;
							oEntry.StreamName = oData.SourceStream;
						} else {
							oEntry = oTable.getContextByIndex(this._iCurrentRowIndex).getObject();
						}

						oEntry.Desc = oData.Description;
						oEntry.dataGroup = oData.Value;

						oData.expressions.forEach(function (oMappingRule) {
							if (oEntry.hasOwnProperty(oMappingRule.property)) {
								oEntry[oMappingRule.property] = oMappingRule.value;
							}
						});
						if (this._oMappingEditor.getMode() === "create") {
							if (!SmartTable.entryExistInTable(oEntry, oTable)) {
								SmartTable.createEntry(oTable, oEntry);
							} else {
								this.showInfoPrimaryKeyNotUnique();
								this._oMappingEditor.setBusy(false);
								this._oMappingEditor.setKeyNotUniqueState();
							}
						} else {
							SmartTable.updateEntry(oTable, oEntry, this._sEditItemPath);
						}
					}
				}
			}
		},

		showInfoPrimaryKeyNotUnique: function () {
			var that = this,
				oRscBundle = this.getView().getModel("i18n").getResourceBundle();

			sap.m.MessageBox.show(
				oRscBundle.getText("err_msg_not_unique"), {
					icon: MessageBox.Icon.INFORMATION,
					title: oRscBundle.getText("err_not_unique_title"),
					actions: [
						sap.m.MessageBox.Action.OK
					],
					emphasizedAction: sap.m.MessageBox.Action.OK,
					onClose: function () {
						that.close();
					}
				}
			);
		},

		onItemSelect: function (oEvent) {
			var oItem = oEvent.getParameter("item"),
				sKey = oItem.getKey(),
				that = this,
				sId = this.getView().createId(sKey);

			if (oItem.getId() === this.getView().createId("navItemHelp")) {
				this.showUserManual();
			} else {
				if (this.byId("pageContainer").getPages().find(function (oPage) {
						return oPage.getId() === sId;
					}) === undefined) {
					if (sKey === _PRODUCTCONFIG) {
						var oModel = that.getView().getModel(_PRODUCTCONFIG);

						that.createProductConfigGui(oModel, that);
					} else if (sKey === "DataGroupMapping") {
						this.createDataGroupMappingPage(this.getView().getModel("config"), oItem.getBindingContext(_VIEW).getObject());
					} else if (oItem.getParent().getId() === this.getView().createId("navItemMapping")) {
						this.createCodeListMappingPage(this.getView().getModel(_MAPPING), oItem.getBindingContext(_VIEW).getObject());
					} else if (oItem.getParent().getId() === this.getView().createId("nliKeyValueMapping")) {
						this.createCodeListMappingPage(this.getView().getModel(_MAPPING), oItem.getBindingContext(_VIEW).getObject());
					} else if (oItem.getParent().getId() === this.getView().createId("navItemStreamBasedMapping")) {
						this.createStreamBasedMappingPage(this.getView().getModel("config"), oItem.getBindingContext(_VIEW).getObject());
					}
				}
				this.byId("pageContainer").to(this.getView().createId(sKey));
				this.setPageInformation(sKey);
			}
		},

		initViewModel: function () {
			//var oRscBundle = this.getView().getModel("i18n").getResourceBundle();
			this.getView().setModel(
				new JSONModel({
					UserInfo: sap.ui.getCore().getModel("user") ? sap.ui.getCore().getModel("user").getData().userInfo : null,
					PageKey: this.getView().byId("sideNavigation").getSelectedKey(),
					CodeListNavItems: [],
					KeyValueNavItems: [{
						key: "DataGroupMapping",
						text: "Datagroup lookup rules" //oRscBundle.getText("dataGroupRules_nav_txt")
					}]
				}),
				_VIEW
			);
			// <tnt:NavigationListItem key="DataGroupMapping" text="{i18n>dataGroupRules_nav_txt}"/>-->
		},

		onValueChanged: function (oEvent) {
			var iIndex = oEvent.getSource().getParent().getIndex(),
				oTable = oEvent.getSource().getParent().getParent(),
				sPath = oTable.getContextByIndex(iIndex).sPath,
				oObj = oTable.getModel().getObject(sPath),
				oCtx = SmartTable.getOdataContextByJsonModelPath(oTable, sPath, this.getView().getModel(_PRODUCTCONFIG));

			this.copyValuesToOdataSource(oObj, oCtx);
		},

		copyValuesToOdataSource: function (oSourceEntry, oContext) {
			if (oSourceEntry && oContext) {
				var oModel = oContext.getModel();

				Object.getOwnPropertyNames(oContext.getObject()).forEach(function (sProperty) {
					if (oSourceEntry.hasOwnProperty(sProperty)) {
						var sPath = oContext.sPath.concat("/", sProperty);
						oModel.setProperty(sPath, oSourceEntry[sProperty]);
					}
				});
			}
		},

		beforeCreateEntry: function (oArgs) {
			if (oArgs && oArgs.hasOwnProperty("Entry") && oArgs.hasOwnProperty("Table")) {
				var oTable = oArgs.Table,
					oEntry = oArgs.Entry;

				if (oTable.getId() !== "tblDatagroupMapping") {
					if (oEntry.hasOwnProperty("Active")) {
						oEntry.Active = "true";
					}

					if (oEntry.hasOwnProperty("CustomMapping")) {
						oEntry.CustomMapping = "true";
					}
				}
			}
		},

		getPageCodeListLoadButton: function () {
			return this.getView().byId("pageContainer").getCurrentPage().getContent()[0]
				.getContent()[0].getContent()[0].getContent()[1].getContent()[0];
		},

		tableGettingBusyHandle: function () {
			var oLoadBtn = this.getPageCodeListLoadButton();

			if (oLoadBtn) {
				oLoadBtn.setEnabled(false);
			}
		},

		tableLoadingCompleted: function () {
			var oLoadBtn = this.getPageCodeListLoadButton();

			if (oLoadBtn) {
				oLoadBtn.setEnabled(true);
			}
		},

		setPageInformation: function (sPageKey) {
			var oModel = this.getView().getModel(_VIEW);

			if (!oModel) {
				this.initViewModel();
			} else {
				this.getView().getModel(_VIEW).setProperty("/PageKey", sPageKey);
			}
		},

		addDeferredGroup: function (oModel, sDeferredGroupId) {
			var aGroups = oModel.getDeferredGroups();

			if (aGroups) {
				if (!aGroups.find(function (sGrId) {
						return sGrId === sDeferredGroupId;
					})) {
					aGroups.push(sDeferredGroupId);
					oModel.setDeferredGroups(aGroups);
				}
			}
		},

		handleLoadMappingPress: function () {
			this.loadDefaultMapping();
		},

		loadDefaultMapping: function () {
			var oRscBundle = this.getView().getModel("i18n").getResourceBundle(),
				that = this;

			sap.m.MessageBox.show(
				oRscBundle.getText("mapping.load_default_question"), {
					icon: MessageBox.Icon.INFORMATION,
					title: oRscBundle.getText("mapping.load_default_title"),
					actions: [
						sap.m.MessageBox.Action.YES,
						sap.m.MessageBox.Action.NO,
						sap.m.MessageBox.Action.CANCEL
					],
					emphasizedAction: sap.m.MessageBox.Action.YES,
					onClose: function (oAction) {
						if (oAction !== "CANCEL") {
							var oMappingId = that.getView().getModel(_VIEW).getData().PageKey,
								sPageKey = that.getView().getModel(_VIEW).getData().PageKey,
								oTable = that.byId("tbl" + sPageKey),
								oBtn = that.getView().byId("btnLoadDefaultMapping" + sPageKey),
								bDelCustMappings = oAction === "NO",
								url = "../xsjs/configuration/loadCodeListMapping.xsjs";

							SmartTable.enableToolBar(oTable, false);
							oTable.setBusy(true);
							oBtn.setEnabled(false);

							$.ajax({
								type: 'GET',
								url: '/',
								headers: {
									'X-Csrf-Token': 'Fetch'
								},
								success: function (res, status, xhr) {
									var sHeaderCsrfToken = 'X-Csrf-Token';
									var sCsrfToken = xhr.getResponseHeader(sHeaderCsrfToken);

									$.ajax({
										url: url,
										type: 'POST',
										data: $.param({
											"Id": oMappingId,
											"DelCustMappings": bDelCustMappings.toString()
										}),
										contentType: 'application/x-www-form-urlencoded',
										beforeSend: function (xhrP) {
											xhrP.setRequestHeader(sHeaderCsrfToken, sCsrfToken);
										},
										async: true,
										success: function () {
											var oModel = that.getView().getModel(_MAPPING);

											if (that.getView().getModel(_VIEW).getData().StreamBasedNavItems && Array.isArray(that.getView().getModel(_VIEW).getData()
													.StreamBasedNavItems)) {
												if (that.getView().getModel(_VIEW).getData().StreamBasedNavItems.find(function (oItem) {
														return oItem.Id === sPageKey;
													}) !== undefined) {
													oModel = that.getView().getModel(_CONFIG);
												}
											}

											sap.m.MessageToast.show(oRscBundle.getText("mapping_successfully_loaded"));

											if (oTable && oModel) {
												oModel.refresh();
												SmartTable.setDataModel(oTable, oModel);
											}
										},
										error: function (oError) {
											ErrorHandler.handleOdataError(oError);

											SmartTable.enableToolBar(oTable, false);
											oTable.setBusy(false);
											oBtn.setEnabled(true);
										}
									});

								}
							});
						}
					}
				}
			);
		},

		onSideNavButtonPress: function () {
			var oToolPage = this.byId("toolPage");
			var bSideExpanded = oToolPage.getSideExpanded();

			this.setToggleButtonTooltip(bSideExpanded);

			oToolPage.setSideExpanded(!oToolPage.getSideExpanded());
		},

		setToggleButtonTooltip: function (bLarge) {
			var oToggleButton = this.byId('sideNavigationToggleButton');

			if (bLarge) {
				oToggleButton.setTooltip('Large Size Navigation');
			} else {
				oToggleButton.setTooltip('Small Size Navigation');
			}
		},

		handleUserNamePress: function (event) {
			var oRscBundle = this.getView().getModel("i18n").getResourceBundle(),
				oPopover = new Popover({
					showHeader: false,
					placement: library.PlacementType.Bottom,
					content: [
						new sap.m.Button({
							text: oRscBundle.getText("logout"),
							type: library.ButtonType.Transparent,
							press: [this.handleLogoutPress, this]
						})
					]
				}).addStyleClass('sapMOTAPopover sapTntToolHeaderPopover');

			oPopover.openBy(event.getSource());
		},

		handleLogoutPress: function () {
			this.handleLogout();
		},

		handleLogout: function () {
			sap.m.URLHelper.redirect("/logout", false);
		},

		showUserManual: function () {
			sap.m.URLHelper.redirect(sap.ui.require.toUrl(
				"com/adweko/adapter/osx/ui/files/User_Manual_EN_Risk.pdf"), true);
		}
	});
});