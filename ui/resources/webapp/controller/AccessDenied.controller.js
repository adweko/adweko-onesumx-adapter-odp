/*global location */
sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("com.adweko.adapter.osx.ui.controller.AccessDenied", {
		onInit: function () {

		}
	});
});