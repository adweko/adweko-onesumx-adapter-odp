sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"com/adweko/adapter/osx/ui/utils/ErrorHandler",
	"sap/ui/core/util/Export",
	"sap/ui/core/util/ExportTypeCSV",
	"com/adweko/adapter/osx/ui/utils/Formatter"
], function (JSONModel, MessageBox, ErrorHandler, Export, ExportTypeCSV, Formatter) {
	"use strict";
	var _EDM_DATETIME = "Edm.DateTime";
	var _SAP_UI_TABLE = "sap.ui.table.Table";
	var _ICON_EDIT = "sap-icon://edit";
	var BUSINESS_VALID_FROM = "BusinessValidFrom";
	var BUSINESS_VALID_TO = "BusinessValidTo";
	var SYSTEM_VALID_FROM = "SystemValidFrom";
	var SYSTEM_VALID_TO = "SystemValidTo";
	var LAST_CHANGE_TIMESTAMP = "LastChangeTimestamp";
	var LAST_CHANGE_APPLICATION_USER_NAME = "LastChangeApplicationUserName";
	var CUSTOM_MAPPING = "CustomMapping";
	var ACTIVE = "Active";

	var TableGenerator = {
		formatter: Formatter,

		generateTableControl: function (sTableId, oModel, sEntitySetName, oController, sTableType, mTableParameters, mGeneratorParameters) {
			var oTable,
				oRscBundle,
				aColumns = [],
				aCells = [],
				sEntityName,
				mParams = mTableParameters,
				oTableData = {
					Title: null,
					Editable: false,
					AllowCreating: false,
					AllowEditing: false,
					AllowDeleting: false,
					ToolbarActions: [],
					TableId: sTableId,
					ODataModel: oModel,
					EntitySetName: sEntitySetName,
					Controller: oController,
					Handles: null,
					HiddenColumns: [],
					BooleanColumns: [],
					ColumnBindings: [],
					InLineEditableColumns: []
				};

			if (mGeneratorParameters) {
				if (mGeneratorParameters.hasOwnProperty("Title")) {
					oTableData.Title = mGeneratorParameters.Title;
				}
				if (mGeneratorParameters.hasOwnProperty("Editable")) {
					oTableData.Editable = mGeneratorParameters.Editable;
				}
				if (mGeneratorParameters.hasOwnProperty("AllowCreating")) {
					oTableData.AllowCreating = mGeneratorParameters.AllowCreating;
				}
				if (mGeneratorParameters.hasOwnProperty("AllowEditing")) {
					oTableData.AllowEditing = mGeneratorParameters.AllowEditing;
				}
				if (mGeneratorParameters.hasOwnProperty("AllowDeleting")) {
					oTableData.AllowDeleting = mGeneratorParameters.AllowDeleting;
				}
				if (mGeneratorParameters.hasOwnProperty("ToolbarActions") && Array.isArray(mGeneratorParameters.ToolbarActions)) {
					oTableData.ToolbarActions = mGeneratorParameters.ToolbarActions;
				}
				if (mGeneratorParameters.hasOwnProperty("Handles")) {
					oTableData.Handles = mGeneratorParameters.Handles;
				}
				if (mGeneratorParameters.hasOwnProperty("HiddenColumns") && Array.isArray(mGeneratorParameters.HiddenColumns)) {
					oTableData.HiddenColumns = mGeneratorParameters.HiddenColumns;
				}
				if (mGeneratorParameters.hasOwnProperty("BooleanColumns") && Array.isArray(mGeneratorParameters.BooleanColumns)) {
					oTableData.BooleanColumns = mGeneratorParameters.BooleanColumns;
				}
				if (mGeneratorParameters.hasOwnProperty("ColumnBindings") && Array.isArray(mGeneratorParameters.ColumnBindings)) {
					oTableData.ColumnBindings = mGeneratorParameters.ColumnBindings;
				}
				if (mGeneratorParameters.hasOwnProperty("InLineEditableColumns") && Array.isArray(mGeneratorParameters.InLineEditableColumns)) {
					oTableData.InLineEditableColumns = mGeneratorParameters.InLineEditableColumns;
				}
			}

			var oGenModel = this.getGeneratorModel(),
				that = this;
			sap.ui.getCore().setModel(oController.getView().getModel("i18n"), "i18n");

			if (!mParams) {
				mParams = {
					rowSelectionChange: [this.onRowSelectionChange, this]
				};
			} else {
				mParams.rowSelectionChange = [this.onRowSelectionChange, this];
			}

			oGenModel.getProperty("/TableCache")[sTableId] = oTableData;

			if (oModel && sEntitySetName) {
				var aSchemas = oModel.getServiceMetadata().dataServices.schema;
				oRscBundle = oController.getView().getModel("i18n").getResourceBundle();
				var oEntityType,
					oEntitySet,
					schemaEntity;

				aSchemas.forEach(function (oSchema) {
					if (oSchema.hasOwnProperty("entityContainer")) {
						oEntitySet = oSchema.entityContainer[0].entitySet;
					}
				});

				if (oEntitySet) {
					oEntityType = oEntitySet.find(function (oType) {
						return oType.name === sEntitySetName;
					});

					if (oEntityType) {
						schemaEntity = aSchemas.find(function (oSchema) {
							return oSchema.hasOwnProperty("entityType");
						});
						sEntityName = oEntityType.entityType;

						if (sEntityName.indexOf(".") >= 0) {
							sEntityName = sEntityName.substring(sEntityName.lastIndexOf(".") + 1);
						}
					}
				}

				if (schemaEntity && schemaEntity.hasOwnProperty("entityType")) {
					oEntityType = schemaEntity.entityType.find(function (oEntity) {
						return oEntity.name === sEntityName;
					});
				} else if (schemaEntity) {
					oEntitySet = schemaEntity.entityContainer[0].entitySet;
					if (oEntitySet) {
						var oTemplateEntity = oEntitySet.find(function (oEntity) {
							return oEntity.name === sEntitySetName;
						});

						if (oTemplateEntity) {
							oEntityType = oTemplateEntity.__entityType;
						}
					}
				}

				if (oEntityType) {
					oEntityType.property.forEach(function (oProperty) {
						var bSkip = false;

						if (oTableData.HiddenColumns && Array.isArray(oTableData.HiddenColumns) && oTableData.HiddenColumns.length > 0) {
							bSkip = oTableData.HiddenColumns.includes(oProperty.name);
						}

						if (!bSkip) {
							var oCol,
								oLbl = new sap.m.Label({
									text: oRscBundle.getText(oProperty.name)
								}),
								oTxt;

							if (oProperty.type === _EDM_DATETIME) {
								oTxt = new sap.m.Text({
									text: {
										path: oProperty.name,
										formatter: function (oValue) {
											return that.dateToString(oValue);
										}
									}
								});
							} else {
								if (oTableData.BooleanColumns && Array.isArray(oTableData.BooleanColumns) && oTableData.BooleanColumns.length > 0 &&
									oTableData.BooleanColumns.includes(oProperty.name)) {
									oTxt = new sap.m.Switch({
										type: "AcceptReject",
										state: "{".concat(oProperty.name, "}"),
										change: [that.stateChangeHandler, that],
										enabled: Array.isArray(oTableData.InLineEditableColumns) && oTableData.InLineEditableColumns.includes(oProperty.name)
									});
								} else {
									oTxt = new sap.m.Text({
										text: "{".concat(oProperty.name, "}")
									});
								}
							}

							if (sTableType === _SAP_UI_TABLE) {
								oCol = new sap.ui.table.Column({
									label: oLbl,
									template: oTxt
								});

								if (oProperty.type === _EDM_DATETIME) {
									oCol.setWidth("8rem");
								}
							} else {
								oCol = new sap.m.Column({
									header: oLbl
								});
								aCells.push(oTxt);
							}
							aColumns.push(oCol);
						}
					});
				} else {
					console.log("entity '".concat(sEntityName, "'not found in metadata"));
				}
			}
			if (sTableType === _SAP_UI_TABLE) {
				mParams.toolbar = this.createTableToolBar(sTableId, oTableData.ToolbarActions, oController);
				mParams.columns = aColumns;

				oTable = new sap.ui.table.Table(sTableId, mParams);

				if (oTableData.Editable) {
					var oTemplate = new sap.ui.table.RowAction({
						items: []
					});

					if (oTableData.AllowEditing) {
						oTemplate.addItem(new sap.ui.table.RowActionItem({
							icon: _ICON_EDIT,
							text: "{i18n>tooltip_edit_entry}",
							press: [that.handleEditEntry, this],
							visible: true
						}));
					}

					if (oTableData.AllowDeleting) {
						oTemplate.addItem(
							new sap.ui.table.RowActionItem({
								type: "Delete",
								text: "{i18n>tooltip_delete_entry}",
								press: [this.handleDeleteEntry, this],
								visible: true
							})
						);
					}
					oTable.setRowActionTemplate(oTemplate);
					oTable.setRowActionCount(oTemplate.getItems().length);
				}

				oTable.bindRows("/" + sEntitySetName);
				oTable.setSelectionMode("Single");
			} else {
				mParams.headerToolbar = this.createTableToolBar(sTableId, oTableData.ToolbarActions, oController);
				mParams.columns = aColumns;

				oTable = new sap.m.Table(sTableId, mParams);

				oTable.bindItems("/" + sEntitySetName,
					new sap.m.ColumnListItem({
						cells: aCells
					})
				);
				oTable.setMode("SingleSelect");
			}

			if (oTable && oRscBundle) {
				var sText = oTableData.Title ? oTableData.Title : oRscBundle.getText(sEntitySetName);

				this.setTableTitle(oTable, sText + " (0)");
				this.setDataModel(oTable, oModel);
				this.setToolbarButtonsEnabled(oTable, false);
			}
			return oTable;
		},

		toBoolean: function (sValue) {
			var bResult;

			if (sValue) {
				if (sValue.toUpperCase() === "TRUE") {
					return true;
				} else if (sValue.toUpperCase() === "FALSE") {
					return false;
				}
			}
			return bResult;
		},

		getDefaultDateFormat: function () {
			return "MMM d, y";
		},

		dateToString: function (oDate) {
			var sResult = "";

			if (oDate) {
				if (oDate.getFullYear().toString() === "9999" && (oDate.getMonth() + 1).toString() && oDate.getDate().toString() === "31") {
					sResult = "unlimited";
				} else {
					var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
							pattern: this.getDefaultDateFormat()
						}),
						dateFormatted = dateFormat.format(oDate);

					sResult = dateFormatted.toString();
				}
			}
			return sResult;
		},

		toString: function (oValue) {
			return oValue ? oValue.toString() : "";
		},

		getGeneratorModel: function () {
			var oModel = sap.ui.getCore().getModel("tableGenerator");

			if (!oModel) {
				this.initGeneratorModel();
			}
			return sap.ui.getCore().getModel("tableGenerator");
		},

		initGeneratorModel: function () {
			sap.ui.getCore().setModel(
				new JSONModel({
					TableCache: []
				}),
				"tableGenerator"
			);
		},

		getColumnValuesModel: function () {
			var oModel = sap.ui.getCore().getModel("columnValues");

			if (!oModel) {
				this.initColumnValuesModel();
			}
			return sap.ui.getCore().getModel("columnValues");
		},

		initColumnValuesModel: function () {
			sap.ui.getCore().setModel(
				new JSONModel({
					TableCache: []
				}),
				"columnValues"
			);
		},

		getCachedTableData: function (sTableId) {
			var oModel = this.getGeneratorModel(),
				oResult;

			if (oModel) {
				var oData = oModel.getProperty("/TableCache");

				if (oData) {
					return oData.hasOwnProperty(sTableId) ? oData[sTableId] : null;
				}
			}
			return oResult;
		},

		onRowSelectionChange: function (oEvent) {
			var oTable = oEvent.getSource();

			if (oTable) {
				this.setToolbarButtonsEnabled(oTable, oTable.getSelectedIndex() >= 0);
			}
		},

		createTableToolBar: function (sTableId, aActions, oController, sToolbarTxt) {
			var oToolBar,
				aContent = null,
				oData = this.getCachedTableData(sTableId);

			if (!oData.Handles) {
				oData.Handles = {
					addItemHandle: null,
					editItemHandle: null,
					deleteItemHandle: null,
					valueChangeHandle: null,
					uploadCsvHandle: null,
					beforeCreateEntry: null,
					tableGettingBusyHandler: null,
					tableLoadingCompleted: null,
					CustomEditHandle: null,
					oDataLoadedHandle: null
				};
			}

			if (aActions && Array.isArray(aActions) && aActions.length > 0) {
				var bAdd = false,
					bDelete = false,
					bEdit = false,
					bUploadCsv = false,
					bDownloadCsv = false,
					bSearch = false,
					oBtn,
					that = this;
				aContent = [
					new sap.m.Title({
						text: sToolbarTxt
					}),
					new sap.m.ToolbarSpacer({})
				];

				aActions.forEach(function (sAction) {
					if (sAction === "Add") {
						bAdd = true;
					} else if (sAction === "Delete") {
						bDelete = true;
					} else if (sAction === "Edit") {
						bEdit = true;
					} else if (sAction === "UploadCsv") {
						bUploadCsv = true;
					} else if (sAction === "Search") {
						bSearch = true;
					} else if (sAction === "DownloadCsv") {
						bDownloadCsv = true;
					}
				});

				if (bSearch) {
					aContent.push(
						new sap.m.SearchField({
							placeholder: "{i18n>Search}",
							width: "250px",
							liveChange: [this.handleSearchChange, this]
						})
					);
				}
				if (bEdit) {
					oBtn = new sap.m.Button({
						type: "Default",
						icon: _ICON_EDIT,
						tooltip: "{i18n>tooltip_edit_entry}",
						enabled: "{view>/editable}",
						press: [that.handleEditEntry, this]
					});
					aContent.push(oBtn);
				}
				if (bAdd) {
					oBtn = new sap.m.Button({
						type: "Default",
						icon: "sap-icon://add",
						tooltip: "{i18n>tooltip_add_entry}",
						press: [this.handleAddEntry, this]
					});
					aContent.push(oBtn);
				}
				if (bDelete) {
					oBtn = new sap.m.Button({
						type: "Default",
						icon: "sap-icon://delete",
						tooltip: "{i18n>tooltip_delete_entry}",
						press: [that.handleDeleteEntry, that]
					});
					aContent.push(oBtn);
					oData.Handles.deleteItemHandle = oController.onDeleteEntryPress;
				}
				if (bUploadCsv) {
					oBtn = new sap.m.Button({
						type: "Default",
						icon: "sap-icon://upload",
						tooltip: "{i18n>csv_import}",
						press: [that.handleUploadCsv, that]
					});
					aContent.push(oBtn);
				}
				if (bDownloadCsv) {
					oBtn = new sap.m.Button({
						type: "Default",
						icon: "sap-icon://download",
						tooltip: "{i18n>csv_export}",
						press: [that.handleDownloadCsv, that]
					});
					aContent.push(oBtn);
				}

				if (aContent.length > 0) {
					oToolBar = new sap.m.Toolbar({
						content: aContent
					});
				}
			}
			return oToolBar;
		},

		handlePressSettingsDialog: function (oEvent) {
			var oTable = oEvent.getSource().getParent().getParent();

			this.getViewSettingsDialog(oTable);
		},

		handleSearchChange: function (oEvent) {
			this.filterTableDate(oEvent.getSource().getParent().getParent(), oEvent.getParameter("newValue"));
		},

		filterTableDate: function (oTable, sValue) {
			if (oTable) {
				var oTblCache = this.getCachedTableData(oTable.getId()),
					oEntity = this.getBondedEntity(oTable, oTblCache.ODataModel),
					oBinding = oTable.getBinding("rows");

				if (oEntity && oBinding) {
					var aFilter = [];

					oBinding.aFilters = [];

					if (sValue !== "") {
						oEntity.__entityType.property.forEach(function (oProperty) {
							if (oProperty.type.includes("String") && !oTblCache.BooleanColumns.includes(oProperty.name)) {
								aFilter.push(new sap.ui.model.Filter({
									path: oProperty.name,
									operator: sap.ui.model.FilterOperator.Contains,
									value1: sValue.toString()
								}));
							}
						});
					}
					if (aFilter.length > 0) {
						oBinding.filter(new sap.ui.model.Filter({
							filters: aFilter,
							and: false
						}));
					} else {
						oBinding.filter();
					}
				}
			}
		},

		handleUploadCsv: function (oEvent) {
			this._sCurrentTableId = this.getTableIdByToolBarAction(oEvent);

			if (!this.oUploadCsvDialog) {
				if (this._sCurrentTableId) {
					var that = this,
						oTableCache = that.getCachedTableData(this._sCurrentTableId);

					if (oTableCache) {
						var oRscBundle = oTableCache.Controller.getView().getModel("i18n").getResourceBundle(),
							oLayout = new sap.ui.layout.VerticalLayout({
								content: [
									new sap.ui.unified.FileUploader({
										buttonText: oRscBundle.getText("btn_txt_browse"),
										fileType: "csv",
										buttonOnly: false,
										uploadComplete: "onUploadCompleteFUP",
										typeMissmatch: "onFailedFUP",
										fileSizeExceed: "onFailedFUP",
										uploadAborted: "onFailedFUP",
										tooltip: oRscBundle.getText("tooltip_browse_csv_file"),
										filenameLengthExceed: "onFailedFUP"
									})
								]
							});
						oLayout.addStyleClass("sapUiResponsiveMargin");

						this.oUploadCsvDialog = new sap.m.Dialog({
							title: oRscBundle.getText("csv_import"),
							content: [
								oLayout
							],
							beginButton: new sap.m.Button({
								text: oRscBundle.getText("start_import"),
								press: function () {
									var oFileUploader = this.oUploadCsvDialog.getContent()[0].getContent()[0],
										oDomRef = oFileUploader.getFocusDomRef();

									this.importFromCsv(this._sCurrentTableId, oDomRef.files[0]);
									this.oUploadCsvDialog.close();
								}.bind(this)
							}),
							endButton: new sap.m.Button({
								text: oRscBundle.getText("close"),
								press: function () {
									this.oUploadCsvDialog.close();
								}.bind(this)
							})
						});
					}
				}
			}
			this.oUploadCsvDialog.open();
		},

		importFromCsv: function (sTableId, oFile) {
			if (sTableId && oFile) {
				var reader = new FileReader(),
					that = this,
					oData = this.getCachedTableData(sTableId),
					oModel = oData.ODataModel,
					oTable = oData.Controller.getView().byId(sTableId),
					oEntity = this.getBondedEntity(oTable, oModel);

				if (oEntity) {
					this.enableToolBar(oTable, false);
					oTable.setBusy(true);

					reader.onload = function (e) {
						try {
							var aCsvLines = e.target.result.split(/\r\n|\n/);
							var aLines = [];

							for (var i = 0; i < aCsvLines.length; i++) {
								var aData = aCsvLines[i].split(';'),
									tarr = [],
									bEmptyRow = true;

								for (var j = 0; j < aData.length; j++) {
									var sValue = aData[j];

									if (sValue !== "") {
										bEmptyRow = false;
										if(i !== 0 && (sValue ==='FALSE' || sValue === 'TRUE')){
											sValue = sValue.toLowerCase();
										}
									}
									tarr.push(sValue ? sValue : undefined);
								}

								if (!bEmptyRow) {
									aLines.push(tarr);
								}
							}
							that.processImportData.call(that, aLines, oEntity, oTable);
						} catch (err) {
							ErrorHandler.handleOdataError(err);

							this.enableToolBar(oTable, true);
							oTable.setBusy(false);
						}
					};
					reader.readAsText(oFile);
				}
			}
		},

		processImportData: function (aData, oEntity, oTable) {
			var oEntityType = oEntity.__entityType,
				hdrRow = aData[0],
				bStructureValid = true,
				iColCount = 0,
				aErrors = [],
				aColDataTypes = [],
				iExpectedCols = oEntityType.property.length,
				oTblCache = this.getCachedTableData(oTable.getId()),
				oController = oTblCache.Controller,
				oRscBundle = oController.getView().getModel("i18n").getResourceBundle(),
				bErrorOccured,
				i;

			if (hdrRow.length <= oEntityType.property.length) {
				hdrRow.forEach(function (sColTitle, iIndex) {
					if (iIndex < oEntityType.property.length) {
						var oProperty = oEntityType.property.find(function (oItem) {
							return sColTitle === oItem.name;
						});

						if (oProperty) {
							iColCount += 1;
							aColDataTypes[oProperty.name] = oProperty.type;
						} else {
							if (bStructureValid) {
								aErrors.push(oRscBundle.getText("csv_invalid_header"));
							}
							aErrors.push("\t".concat(oRscBundle.getText("csv_unvalid_column_name"), " '", sColTitle, "'"));
							bStructureValid = false;
						}
					}
				});
				bStructureValid = bStructureValid && iColCount === iExpectedCols;

				if (bStructureValid) {
					bErrorOccured = false;
					var that = this,
						aEntries = [],
						oTmpModel = new sap.ui.model.json.JSONModel({});

					oTblCache.ODataModel.refresh(); //reload to get the latest data...

					for (i = 1; i < aData.length; i++) {
						var row = aData[i], // extract remaining rows one by one
							oEntry = this.createOdataEntryByTableBinding(oTable, oTblCache.ODataModel);

						for (var j = 0; j < row.length; j++) {
							var sProperty = hdrRow[j];

							if (row[j] && sProperty !== SYSTEM_VALID_FROM && sProperty !== SYSTEM_VALID_TO && sProperty !== LAST_CHANGE_TIMESTAMP && sProperty !== LAST_CHANGE_APPLICATION_USER_NAME) {
								var oDataType = aColDataTypes[hdrRow[j]],
									sValue = row[j].trim(),
									sColName = hdrRow[j];

								if (oDataType === _EDM_DATETIME) {
									var sErrMsg = oRscBundle.getText("csv_imp_err_invalid_date").concat(" ", oRscBundle.getText("csv_row"), " ", i, " ",
										oRscBundle.getText("csv_column"), " '", sColName, "'");

									try {
										oEntry[sColName] = new Date(sValue);

										if (!that.isValidDate(oEntry[sColName])) {
											bErrorOccured = true;
											aErrors.push(sErrMsg);

											break;
										}
									} catch (e) {
										console.log(e);
										bErrorOccured = true;
										aErrors.push(sErrMsg);

										break;
									}
								} else if (sColName !== SYSTEM_VALID_FROM || sColName !== SYSTEM_VALID_TO || sColName !== CUSTOM_MAPPING || sColName !== LAST_CHANGE_TIMESTAMP || sColName !== LAST_CHANGE_APPLICATION_USER_NAME) {
									if(sValue === ""){
										sValue = " "; 
									}
									oEntry[sColName] = sValue;
								}
							}
						}

						if (!bErrorOccured) {
							var bValid = this.validateInputEntry(oEntry, oEntityType );


							if(!bValid){
							aEntries.push({
								status: {
									status: "Error",
									text: "not matching data type"
								},
								data: oEntry
							});
							}
					
							if(bValid){
							var iStatus = this.checkEntryStatus(oEntry, oTable);
							switch (iStatus){
								case 0:
									aEntries.push({
										status: {
											status: "Success",
											text: "OK"
										},
										data: oEntry
									});
									break;
								case 1 : 
										aEntries.push({
										status: {
											status: "Warning",
											text: "Overlapping of validities with existing mapping!"
										},
										data: oEntry
									});
									break;
								case 2:
									aEntries.push({
										status: {
											status: "Error",
											text: "Entry already exists!"
										},
										data: oEntry
									});
									break;
								case 3:
									aEntries.push({
										status: {
											status: "Error",
											text: "A record with ELSECase = true already exists."
										},
										data: oEntry
									});
									break;
								}
							}
						}
					}

					if (aErrors.length === 0) {
						var oDialog = this.getImportDialog(oTable);

						oTmpModel.setProperty("/Entries", aEntries);

						sap.ui.getCore().setModel(oTmpModel, "tmp");
						oDialog.setModel(oTmpModel, "tmp");
						oDialog.setModel(oTblCache.ODataModel, "odata");

						oDialog.open();
	/*					oDialog.getContent()[0].getRows().forEach(function (oRow) {
							that.setImportEntryStatus(oRow);
						});*/
					}
				}
				if (!bStructureValid || bErrorOccured) {
					var sOutput = "";

					aErrors.push(oRscBundle.getText("csv_import_canceled"));

					for (i = 0; i < aErrors.length; i++) {
						sOutput += i > 0 ? "\r\n" : "";
						sOutput += aErrors[i];
					}
					MessageBox.error(sOutput);
				}
			}
		},

		getImportDialog: function (oTable) {
			var oResult;

			if (oTable) {
				var oTblCache = this.getCachedTableData(oTable.getId());

				if (oTblCache) {
					var oRscBundle = oTblCache.Controller.getView().getModel("i18n").getResourceBundle(),
						oEntity = this.getBondedEntity(oTable, oTblCache.ODataModel);

					if (!oTblCache.ImportDialog) {
						var oPopUpTable,
							that = this,
							aColumns = [
								new sap.ui.table.Column({
									label: new sap.m.Label({
										text: oRscBundle.getText("import_status_text")
									}),
									template: new sap.m.Text({
										text: "{tmp>status/text}"
									})
								})
							];

						oEntity.__entityType.property.forEach(function (oProperty) {
							if (!(oProperty.name === SYSTEM_VALID_FROM || oProperty.name === SYSTEM_VALID_TO || oProperty.name === CUSTOM_MAPPING ||
									oProperty.name === ACTIVE || oProperty.name === LAST_CHANGE_TIMESTAMP || oProperty.name === LAST_CHANGE_APPLICATION_USER_NAME)) {
								var oCtrl = that.getControlByProperty.call(that, oProperty, undefined, undefined, that, true, oTable.getId(), "tmp>data/");

								aColumns.push(new sap.ui.table.Column({
									label: new sap.m.Label({
										text: oRscBundle.getText(oProperty.name)
									}),
									template: oCtrl
								}));
							}
						});
						oPopUpTable = new sap.ui.table.Table(oTable.getId().concat("_import"), {
							columns: aColumns
						});
						oPopUpTable.setRowSettingsTemplate(new sap.ui.table.RowSettings({
							highlight: "{tmp>status/status}"
						}));
						//oPopUpTable.addStyleClass("myVeryCompactTable");

						for (var i = 0; i < oPopUpTable.getColumns().length; i++) {
							oPopUpTable.autoResizeColumn(i);
						}

						oTblCache.ImportDialog = new sap.m.Dialog({
							title: oRscBundle.getText("import.dialog_title"),
							buttons: [
								new sap.m.Button({
									text: oRscBundle.getText("import.btn_submit_txt"),
									tap: [this.handleImportEntries, this]
								}),
								new sap.m.Button({
									text: oRscBundle.getText("import.btn_cancel_txt"),
									tap: [this.handleCloseDialog, this]
								})
							],
							content: oPopUpTable
						});
						oPopUpTable.bindRows("tmp>/Entries/");
					}
					oResult = oTblCache.ImportDialog;
				}
			}
			return oResult;
		},

		setImportEntryStatus: function (oRow) {
			if (oRow.getBindingContext("tmp")) {
				var oEntry = oRow.getBindingContext("tmp").getObject(),
					sBaseTableId = oRow.getParent().getId().replace("_import", ""),
					oTblCache = this.getCachedTableData(sBaseTableId),
					oModel = oRow.getBindingContext("tmp").getModel(),
					oTable = oTblCache.Controller.getView().byId(sBaseTableId);

				if (oEntry && oTable) {
					var bValid = this.validateInput(oRow.getCells());

					if (bValid) {
						var iStatus = this.checkEntryStatus(oEntry.data, oTable);

						switch (iStatus) {
						case 0:
							oEntry.status.status = "Success";
							oEntry.status.text = "OK";
							break;
						case 1:
							oEntry.status.status = "Warning";
							oEntry.status.text = "Overlapping of validities with existing mapping!";
							break;
						case 2:
							oEntry.status.status = "Error";
							oEntry.status.text = "Entry already exists!";
							break;
						case 3:
							oEntry.status.status = "Error";
							oEntry.status.text = "A record with ELSECase = true already exists.";
							break;
						default:
						}
					} else {
						oEntry.status.status = "Error";
						oEntry.status.text = "Input validation failed!";
					}
					oModel.setData(oModel.getData());
				}
			}
		},

		handleDownloadCsv: function (oEvent) {
			this._sCurrentTableId = this.getTableIdByToolBarAction(oEvent);

			this.onDataExport();
		},

		onDataExport: sap.m.Table.prototype.exportData || function () {
			if (this._sCurrentTableId) {
				var that = this,
					oTableCache = that.getCachedTableData(this._sCurrentTableId);

				if (oTableCache) {
					var oTable = oTableCache.Controller.getView().byId(this._sCurrentTableId),
						oModel = oTableCache.ODataModel;

					if (oTable && oModel) {
						var oEntity = this.getBondedEntity(oTable, oTableCache.ODataModel);

						if (oEntity) {
							var aExportColumns = [];

							oEntity.__entityType.property.forEach(function (oProperty) {
								aExportColumns.push({
									name: oProperty.name,
									template: {
										content: "{".concat(oProperty.name + "}")
									}
								});
							});

							if (aExportColumns.length > 0) {
								oModel.setSizeLimit(oTable.getBinding("rows").iLength); 
								var oExport = new Export({
									exportType: new ExportTypeCSV({
										fileExtension: "csv",
										separatorChar: ";"
									}),
									models: oModel,
									rows: {
										path: oTable.getBindingPath("rows")
									},
									columns: aExportColumns
								});
								oExport.saveFile().catch(function (err) {
									ErrorHandler.handleOdataError(err);
								}).then(function () {
									oExport.destroy();
								});
							}
						}
					}
				}
			}
		},

		isValidDate: function (d) {
			return d instanceof Date && !isNaN(d);
		},

		callTableBusyEvent: function (oTable) {
			if (oTable) {
				var oTblCache = this.getCachedTableData(oTable.getId());

				if (oTblCache) {
					if (oTblCache.Handles && oTblCache.Handles.hasOwnProperty("tableGettingBusyHandler")) {
						try {
							oTblCache.Handles.gettingBusyHandler.call(oTblCache.Controller, oTable);
						} catch (e) {
							ErrorHandler.handleOdataError(e);
						}
					}
				}
			}
		},

		callTableLoadingCompletedEvent: function (oTable) {
			if (oTable) {
				var oTblCache = this.getCachedTableData(oTable.getId());

				if (oTblCache.Handles && oTblCache.Handles.hasOwnProperty("tableLoadingCompleted")) {
					try {
						oTblCache.Handles.tableLoadingCompleted.call(oTblCache.Controller, oTable);
					} catch (e) {
						ErrorHandler.handleOdataError(e);
					}
				}
			}
		},

		setDataModel: function (oTable, oModel) {
			if (oModel) {
				var that = this,
					oEntity = this.getBondedEntity(oTable, oModel),
					oTblCache = this.getCachedTableData(oTable.getId());

				this.enableToolBar(oTable, false);
				oTable.setBusy(true);

				this.callTableBusyEvent(oTable);

				oModel.read("/" + oEntity.name, {
					success: function (oRetrievedResult) {
						var oTblModel = new sap.ui.model.json.JSONModel();

						if (oRetrievedResult && oRetrievedResult.results) {
							var oData = oRetrievedResult.results;

							if (oTblCache.BooleanColumns && Array.isArray(oTblCache.BooleanColumns)) {
								oData.forEach(function (oItem) {
									oTblCache.BooleanColumns.forEach(function (sColName) {
										oItem[sColName] = that.toBoolean(oItem[sColName]);
									});
								});
							}
							oTblModel.setProperty("/" + oEntity.name, oData);
							oTable.setModel(oTblModel);

							that.refreshRowCount(oTable, oModel);
						}
						oTable.setBusy(false);
						that.enableToolBar(oTable, true);
						that.setToolbarButtonsEnabled(oTable, true);

						that.callTableLoadingCompletedEvent.call(that, oTable);
					},
					error: function (oError) {
						/* do something */
						oTable.setBusy(false);
						that.enableToolBar(oTable, true);

						that.callTableLoadingCompletedEvent.call(that, oTable);
					}
				});
			}
		},

		setToolbarButtonsEnabled: function (oTable, bEnabled) {
			if (oTable) {
				if (oTable.getMetadata().getName() === _SAP_UI_TABLE) {
					var oToolBar = oTable.getToolbar();

					if (oToolBar) {
						var aContent = oToolBar.getContent();

						if (aContent) {
							aContent.forEach(function (oCtrl) {
								if (oCtrl.getMetadata().getName() === "sap.m.Button") {
									if (oCtrl.getIcon() === _ICON_EDIT || oCtrl.getIcon() === "sap-icon://delete") {
										oCtrl.setEnabled(bEnabled);
									}
								}
							});
						}
					}
				}
			}
		},

		enableToolBar: function (oTable, bEnable) {
			if (oTable) {
				if (oTable.getMetadata().getName() === _SAP_UI_TABLE) {
					var oToolBar = oTable.getToolbar();

					if (oToolBar) {
						oTable.getToolbar().setEnabled(bEnable);
					}
				}
			}
		},

		setTableTitle: function (oTable, sTitle) {
			if (oTable) {
				if (oTable.getMetadata().getName() === _SAP_UI_TABLE) {
					var oToolBar = oTable.getToolbar();

					if (oToolBar) {
						oTable.getToolbar().getTitleControl().setText(sTitle);
					} else {
						oTable.setTitle(sTitle);
					}
				}
			}
		},

		getTableTitle: function (oTable) {
			var sResult;

			if (oTable) {
				if (oTable.getMetadata().getName() === _SAP_UI_TABLE) {
					var oToolBar = oTable.getToolbar();

					if (oToolBar) {
						sResult = oTable.getToolbar().getTitleControl().getText();
					} else {
						sResult = oTable.getTitle().getText();
					}
				}
			}
			return sResult;
		},

		refreshRowCount: function (oTable, oModel) {
			if (oTable && oModel) {
				var sText = this.getTableTitle(oTable),
					patt = /([0-9]+)/,
					aMatches = sText.match(patt),
					oTblModel = oTable.getModel(),
					oEntity = this.getBondedEntity(oTable, oModel),
					oData = oTblModel.getData();

				if (aMatches && aMatches.length >= 1 && oData && oData.hasOwnProperty(oEntity.name) && oData[oEntity.name] && Array.isArray(oData[
						oEntity.name])) {
					sText = sText.replace(aMatches[1], oData[oEntity.name].length);
				}
				this.setTableTitle(oTable, sText);
			}
		},

		getBondedEntity: function (oCtrl, oModel) {
			var oResult;

			if (oCtrl) {
				var sAggregation;

				sAggregation = this.getAggregationProperty(oCtrl);

				var sBindingPath = oCtrl.getBindingInfo(sAggregation).path,
					sEntityName = sBindingPath.substring(sBindingPath.lastIndexOf("/") + 1);

				oResult = this.getEntityByName(sEntityName, oModel);
			}
			return oResult;
		},

		getEntityByName: function (sEntityName, oModel) {
			var oResult;

			if (sEntityName && oModel) {
				var aSchemas = oModel.getServiceMetadata().dataServices.schema,
					oEntitySet;

				for (var i = 0; i < aSchemas.length; i++) {
					var oSchema = aSchemas[i];

					if (oSchema.hasOwnProperty("entityContainer")) {
						oEntitySet = oSchema.entityContainer[0].entitySet;

						if (oEntitySet) {
							var oTemplateEntity = oEntitySet.find(function (oEntity) {
								return oEntity.name === sEntityName;
							});

							if (oTemplateEntity) {
								oResult = oTemplateEntity;

								if (!oResult.hasOwnProperty("__entityType")) {
									oResult.__entityType = oSchema.entityType.find(function (oType) {
										return (oTemplateEntity.entityType.includes(".") ?
											oTemplateEntity.entityType.substring(oTemplateEntity.entityType.lastIndexOf(".") + 1) :
											oTemplateEntity.entityType) === oType.name;
									});
								}
								break;
							}
						}
					}
				}
			}
			return oResult;
		},

		getAggregationProperty: function (oCtrl) {
			var sResult;

			if (oCtrl) {
				var sMetaName = oCtrl.getMetadata().getName();

				if (sMetaName === _SAP_UI_TABLE) {
					sResult = "rows";
				} else if (sMetaName === "sap.m.Table") {
					sResult = "items";
				}
			}
			return sResult;
		},

		generateControlContentByTableBinding: function (oTable, oController, bSetPrimaryKeysEditable, fnValueChangeHandler) {
			var aContent = [],
				oTblCache = this.getCachedTableData(oTable.getId()),
				oTemplateEntity = this.getBondedEntity(oTable, oTblCache.ODataModel);

			if (oTemplateEntity) {
				var oEntityType = oTemplateEntity.__entityType;

				if (oEntityType) {
					var oRscBundle = oController.getView().getModel("i18n").getResourceBundle(),
						sEntityName = oTemplateEntity.name,
						that = this;

					oEntityType.property.forEach(function (oProperty) {
						if (!oTblCache.HiddenColumns.includes(oProperty.name)) {
							var sName = sEntityName.concat("_", oProperty.name),
								bIsKey = false,
								sId = "inp" + sName,
								oLbl = new sap.m.Label("lbl" + sName, {
									text: oRscBundle.getText(oProperty.name),
									labelFor: sId
								}),
								oCtrl;

							bIsKey = that.propertyIsPrimaryKey(oTemplateEntity, oProperty.name);

							if (that.checkFieldEditable(sEntityName, oProperty.name)) {
								var bEditable = (!bSetPrimaryKeysEditable && bIsKey) ? false : true;

								if (oCtrl === undefined) { //Defines property by meta data if no custom field was specified
									oCtrl = that.getControlByProperty.call(that, oProperty, sId, fnValueChangeHandler, that, bEditable, oTable.getId());
								}
								if (oCtrl !== undefined) {
									if (that.checkControlHasFunction(oCtrl, "setRequired")) {
										if ((bIsKey) || (oProperty.nullable === "false")) {
											oLbl.setRequired(true);
											oCtrl.setRequired(true);
										}
									}
									aContent.push(oLbl);
									aContent.push(oCtrl);
								}
							}
						}
					});
				}
			}
			return aContent;
		},

		propertyIsPrimaryKey: function (oEntity, sPropertyName) {
			var bResult = false;

			if (oEntity) {
				bResult = oEntity.__entityType.key.propertyRef.find(function (oElement) {
					return oElement.name === sPropertyName;
				}) !== undefined; //part of the primary key
			}
			return bResult;
		},

		checkFieldEditable: function (sEntityName, sPropertyName) {
			return (sPropertyName !== SYSTEM_VALID_FROM && sPropertyName !== SYSTEM_VALID_TO 
				&& sPropertyName !== LAST_CHANGE_TIMESTAMP && sPropertyName !== LAST_CHANGE_APPLICATION_USER_NAME);
		},

		checkControlHasFunction: function (oCtrl, sFunction) {
			var bResult = false;

			if (oCtrl) {
				var oCtrlMetaData = oCtrl.getMetadata();

				if (oCtrlMetaData.getAllPublicMethods().includes(sFunction)) {
					bResult = true;
				}
			}
			return bResult;
		},

		getControlByProperty: function (oProperty, sId, fChangeHandler, oController, bEditable, sTableId, sBindingPath) {
			var oCtrl;

			if (oProperty) {
				var sBinding = (sBindingPath ? sBindingPath : "tmp>/Entry/").concat(oProperty.name),
					oData = sTableId ? this.getCachedTableData(sTableId) : null,
					that = this;

				if (oData && oData.ColumnBindings && Array.isArray(oData.ColumnBindings) && oData.ColumnBindings.hasOwnProperty(oProperty.name) &&
					oData.ColumnBindings[oProperty.name] && oData.ColumnBindings[oProperty.name].hasOwnProperty("Path")) {
					var sColBinding = oData.ColumnBindings[oProperty.name].Path,
						aPath = sColBinding.startsWith("/") ? sColBinding.substring(1).split("/") : sColBinding.split("/"),
						sPath = "",
						sProperty = aPath[aPath.length - 1];

					for (var i = 0; i < aPath.length - 1; i++) {
						sPath += "/" + aPath[i];
					}

					oCtrl = new sap.m.ComboBox(sId, {
						width: "100%",
						value: "{" + sBinding + "}",
						change: [this.valueChangeHandler, this],
						items: {
							path: "odata>" + sPath,
							template: new sap.ui.core.Item({
								key: "{odata>".concat(sProperty, "}"),
								text: {
									path: "odata>".concat(sProperty),
									formatter: function (sValue) {
										return that.toString(sValue);
									}
								}
							}),
							templateShareable: true
						}
					});
				} else if (oProperty.type.includes("String")) {
					if (oProperty.hasOwnProperty("maxLength") && parseInt(oProperty.maxLength) === 5) { //special case for boolean fields since boolean is not supported yet...
						if (sTableId === '__xmlview1--tblRatingMapping') {
							oCtrl = new sap.m.RadioButtonGroup(sId, {
								columns: 3,
								selectedIndex: {
									path: sBinding,
									formatter: function (sValue) {
										var index;
										    if (sValue) {
											switch (that.toBoolean(sValue)) {
											case true:
												index = 0;
												break;
											case false:
												index = 1;
												break;
											default:
												index = 2;
											}
										} else {
											index = 1;
										}
										return index;
									}
								},
								select: [this.valueChangeHandler, this]
							});
							oCtrl.addButton(new sap.m.RadioButton({
								text: 'true'
							}));
							oCtrl.addButton(new sap.m.RadioButton({
								text: 'false'
							}));
							oCtrl.addButton(new sap.m.RadioButton({
								text: 'N/A'
							}));	
						} else {
							oCtrl = new sap.m.Switch(sId, {
								type: "AcceptReject",
								state: {
									path: sBinding,
									formatter: function (sValue) {
										return that.toBoolean(sValue);
									}
								},
								change: [this.valueChangeHandler, this]
							});	
						}
					} else if (oProperty.hasOwnProperty("maxLength") && oProperty.maxLength > 50) {
						oCtrl = new sap.m.TextArea(sId, {
							width: "100%",
							value: "{" + sBinding + "}",
							change: [this.valueChangeHandler, this]
						});
					} else if (oProperty.name === 'RatingScale') {
						oCtrl = new sap.m.RadioButtonGroup(sId, {
								columns: 2,
								selectedIndex: {
									path: sBinding,
									formatter: function (sValue) {
										var index;
										switch (that.toString(sValue)) {
											case 'Rating':
												index = 0;
												break;
											case 'RatingRank':
												index = 1;
												break;
											default:
												index = 0;
										}
										return index;
									}
								},
								select: [this.valueChangeHandler, this]
							});
							oCtrl.addButton(new sap.m.RadioButton({
								text: 'Rating'
							}));
							oCtrl.addButton(new sap.m.RadioButton({
								text: 'RatingRank'
							}));
					} else {
						oCtrl = new sap.m.Input(sId, {
							width: "100%",
							value: "{" + sBinding + "}",
							type: "Text",
							change: [this.valueChangeHandler, this]
						});
					}
				} else if (oProperty.type.includes("Int") || oProperty.type.includes("Decimal")) {
					oCtrl = new sap.m.Input(sId, {
						type: "Number",
						value: "{" + sBinding + "}",
						change: [this.valueChangeHandler, this]
					});
				} else if (oProperty.type === _EDM_DATETIME) {
					oCtrl = new sap.m.DatePicker(sId, {
						value: {
							path: sBinding,
							type: "sap.ui.model.type.Date",
							formatOptions: {
								pattern: that.getDefaultDateFormat(),
								strictParsing: true,
								UTC: true
							}
						},
						change: [this.valueChangeHandler, this]
					});
				} else {
					console.log("unhandled data type: '".concat(oProperty.type, "'"));
				}
			}

			if (oCtrl) {
				if (this.checkControlHasFunction(oCtrl, "setEditable")) {
					oCtrl.setEditable(bEditable);
				} else {
					oCtrl.setEnabled(bEditable);
				}

				if (oProperty.hasOwnProperty("maxLength")) {
					if (that.checkControlHasFunction(oCtrl, "setMaxLength")) {
						oCtrl.setMaxLength(parseInt(oProperty.maxLength));
					}
				}
			}
			return oCtrl;
		},

		getViewSettingsDialog: function (oTable) {
			var oResult,
				that = this,
				sTableId = oTable.getId();

			if (sTableId) {
				if (!this._aSortDialogs) {
					this._aSortDialogs = [];
				}
				var sId = "vsd" + sTableId;

				oResult = this._aSortDialogs[sTableId];

				if (!oResult) {
					var oTblCache = this.getCachedTableData(sTableId),
						oEntity = this.getBondedEntity(oTable, oTblCache.ODataModel);

					if (oEntity) {
						var oEntityType = oEntity.__entityType;

						if (oEntityType) {
							var oRscBundle = oTblCache.Controller.getView().getModel("i18n").getResourceBundle();

							oResult = new sap.m.ViewSettingsDialog(sId, {
								confirm: [that.handleSortDialogConfirm, that]
							});

							oEntityType.property.forEach(function (oProperty) {
								if (that.checkFieldEditable(oEntity.name, oProperty.name)) {
									var oItem = new sap.m.ViewSettingsItem({
										key: oProperty.name,
										text: oRscBundle.getText(oProperty.name)
									});
									oResult.addSortItem(oItem);
								}
							});
							this._aSortDialogs[sTableId] = oResult;
						}
					}
				}
			}
			return oResult;
		},

		handleSortDialogConfirm: function () {

		},

		createOdataEntryByTableBinding: function (oTable, oModel) {
			var oResult,
				oEntity = this.getBondedEntity(oTable, oModel);

			if (oEntity) {
				oResult = this.createOdataEntryByEntity(oEntity, oModel);
			}
			return oResult;
		},
		propertyValidator: function (oProperty, oResult) {
			if (oProperty.name !== SYSTEM_VALID_FROM && oProperty.name !== SYSTEM_VALID_TO 
					&& oProperty.name !== LAST_CHANGE_TIMESTAMP && oProperty.name !== LAST_CHANGE_APPLICATION_USER_NAME) {
				if (oProperty.type === _EDM_DATETIME && oProperty.name === BUSINESS_VALID_FROM) {
					oResult[oProperty.name] = new Date("1900-01-01");
				} else if (oProperty.name === BUSINESS_VALID_TO) {
					oResult[oProperty.name] = null;
				} else {
					oResult[oProperty.name] = undefined;
				}
			} else {
				oResult[oProperty.name] = undefined;
			}
		},
		createOdataEntryByEntity: function (oEntity, oModel) {
			var oResult;

			if (oEntity && oModel) {
				var oEntityType = oEntity.__entityType,
					that = this;

				if (oEntityType) {
					oResult = {};
					oEntityType.property.forEach(function (oProperty) {
						that.propertyValidator(oProperty, oResult);
					});
				}
			}
			return oResult;
		},
		copyEntry: function (oOriginalEntry, oEntity, oModel) { 
			var oResult;

			if (oEntity && oModel) {
				var oEntityType = oEntity.__entityType;

				oResult = JSON.parse(JSON.stringify(oOriginalEntry));

				if (oEntityType) {
					oEntityType.property.forEach(function (oProperty) {
						if (oProperty.type === _EDM_DATETIME) {
							oResult[oProperty.name] = new Date(oResult[oProperty.name]);
						}
					});
				}
			}
			return oResult;
		},

		createColumnListItem: function (oTable) {
			var oResult;

			if (oTable) {
				if (oTable.getMetadata().getName() === "sap.ui.comp.smarttable.SmartTable") {
					oTable = oTable.getParent();
				}

				if (oTable) {
					var oTemplate = oTable.getBindingInfo("items").template;

					if (oTemplate) {
						oResult = oTemplate.clone();
					}
				}
			}
			return oResult;
		},

		createUiTableRow: function (oTable) {
			var oResult;

			if (oTable) {
				if (oTable.getMetadata().getName() === "sap.ui.comp.smarttable.SmartTable") {
					oTable = oTable.getParent();
				}

				if (oTable) {
					var oTemplate = oTable.getBindingInfo("rows").template;

					if (oTemplate) {
						oResult = oTemplate.clone();
					}
				}
			}
			return oResult;
		},

		getDialogEditEntry: function (oTable, oController, sPath, fnValueChangeHandler) {
			var oTblContent,
				oRscBundle = oController.getView().getModel("i18n").getResourceBundle(),
				that = this,
				oEntry,
				oTblCache = this.getCachedTableData(oTable.getId()),
				oTmpModel = new sap.ui.model.json.JSONModel({
					Entry: null,
					ColumnValues: {}
				}),
				sTableName = oTable.getId(),
				sMode = "AddNewEntry",
				sTitle = "",
				oOriginal,
				oEntity,
				bJustCreated = true;

			this._sEditItemPath = "";

			if (sPath) {
				oOriginal = oTable.getModel().getObject(sPath);
				oEntity = this.getBondedEntity(oTable, oTblCache.ODataModel);

				oEntry = this.copyEntry(oOriginal, oEntity, oTblCache.ODataModel);
				this._sEditItemPath = sPath;

				sMode = "EditEntry";
			} else {
				oEntry = this.createOdataEntryByTableBinding(oTable, oTblCache.ODataModel);
			}

			sTitle = sMode === "AddNewEntry" ?
				oRscBundle.getText("create_new_entry") : oRscBundle.getText("edit_entry");

			if (!this._aDialogContent) {
				this._aDialogContent = [];
			}

			// set the data for the model
			oTmpModel.setProperty("/Entry/", oEntry);
			// set the model to the core
			sap.ui.getCore().setModel(oTmpModel, "tmp");
			oController.getView().setModel(oTmpModel, "tmp");

			oTblContent = this._aDialogContent.find(function (oContent) {
				return oContent.Table === oTable;
			});

			if (!oTblContent) {
				oTblContent = {
					Table: oTable,
					Content: []
				};

				oTblContent.Content = new sap.ui.layout.VerticalLayout({
					width: "90%",
					content: this.generateControlContentByTableBinding(oTable, oController, (sMode !== "EditEntry"), fnValueChangeHandler)
				});
				oTblContent.Content.addStyleClass("sapUiSmallMarginBegin");
				oTblContent.Content.addStyleClass("sapUiSmallMarginTopBottom");

				oTblContent.sPath = sTableName;

				this._aDialogContent.push(oTblContent);
			} else {
				bJustCreated = false;
			}

			if (!this._oEditEntryDialog) {
				var oDialog = new sap.m.Dialog("dialEditEntry", {
					title: sTitle,
					//modal: true,
					contentWidth: "1em",
					buttons: [
						new sap.m.Button("btnSubmitEntry", {
							text: oRscBundle.getText("submit"),
							tap: [this.handleSubmitEntry, this]
						}),
						new sap.m.Button("btnCancelEdit", {
							text: oRscBundle.getText("cancel"),
							tap: [this.handleCloseDialog, this]
						})
					],
					content: oTblContent.Content
				});
				oDialog.setModel(oTmpModel);
				oDialog.setModel(oTblCache.ODataModel, "odata");
				this._oEditEntryDialog = oDialog;
			} else {
				if (this._oContent !== oTblContent || this._sMode !== sMode) {
					var oModel = oTblCache.ODataModel,
						oLayout = new sap.ui.layout.VerticalLayout({
							width: "90%"
						}),
						bFocusSet = false;

					oEntity = this.getBondedEntity(oTable, oModel);
					oLayout.addStyleClass("sapUiSmallMarginBegin");
					oLayout.addStyleClass("sapUiSmallMarginTopBottom");

					this._oEditEntryDialog.removeAllContent();

					oTblContent.Content.getContent().forEach(function (oCtrl) {
						if (that.checkControlHasFunction(oCtrl, "setValueState")) {
							oCtrl.setValueState(sap.ui.core.ValueState.None);
						}

						if (!bJustCreated && oCtrl.getMetadata().getName() !== "sap.m.Label") {
							var sProperty = oCtrl.getId(),
								bIsKey = false,
								bEditable = true;

							sProperty = sProperty.substring(sProperty.lastIndexOf("_") + 1);
							bIsKey = that.propertyIsPrimaryKey(oEntity, sProperty);

							bEditable = sMode === "AddNewEntry" || !bIsKey;

							if (that.checkControlHasFunction(oCtrl, "setEditable")) {
								oCtrl.setEditable(bEditable);
							} else {
								oCtrl.setEnabled(bEditable);
							}

							if (bEditable && !bFocusSet && typeof oCtrl.setFocus === "function") {
								oCtrl.setFocus();
								bFocusSet = true;
							}
						}
						oLayout.addContent(oCtrl);
					});
					this._oEditEntryDialog.addContent(oLayout);
					oTblContent.Content = oLayout;
				} else {
					oTblContent.Content.getContent().forEach(function (oCtrl) {
						if (that.checkControlHasFunction(oCtrl, "setValueState")) {
							oCtrl.setValueState(sap.ui.core.ValueState.None);
						}
					});
				}
				this._oEditEntryDialog.setTitle(sTitle);
			}
			this._oContent = oController._oContent = oTblContent;
			this._sMode = sMode;

			return this._oEditEntryDialog;
		},

		validateInput: function (aInputControls) {
			var bResult = true;

			if (aInputControls && Array.isArray(aInputControls)) {
				var oTblCache = this.getCachedTableData(this._sCurrentTableId),
					oTemplateEntity = this.getBondedEntity(oTblCache.Controller.getView().byId(this._sCurrentTableId), oTblCache.ODataModel);

				if (oTemplateEntity) {
					var oEntityType = oTemplateEntity.__entityType;

					if (oEntityType) {
						var oCtrlBusinessValidFrom,
							oCtrlBusinessValidTo,
							that = this;

						aInputControls.forEach(function (oCtrl) {
							var sType = oCtrl.getMetadata().getName();

							if (sType !== "sap.m.Label" && sType !== "sap.m.Text" && oCtrl.getVisible()) {
								if (sType === "sap.m.Input" || sType === "sap.m.TextArea" || sType === "sap.m.ComboBox" || sType === "sap.m." || sType ===
									"sap.m.DatePicker") {
									var oInfo,
										oValue,
										bChecked = false;

									if (sType === "sap.m.DatePicker") {
										oInfo = oCtrl.getBindingInfo("value");
										oValue = oCtrl.getDateValue();

										if (oCtrl.getValue() && !oCtrl.isValidValue()) {
											oCtrl.setValueStateText(that.getText("mapping.invalid_date"));
											oCtrl.setValueState(sap.ui.core.ValueState.Error);

											bChecked = true;
											bResult = false;
										} else {
											oCtrl.setValueState(sap.ui.core.ValueState.None);
										}
									} else {
										oInfo = oCtrl.getBindingInfo("value");
										oValue = oCtrl.getValue();
									}

									if (oInfo && oInfo.parts.length === 1) {
										var sProperty = oInfo.parts[0].path.substring(oInfo.parts[0].path.lastIndexOf("/") + 1);

										if (sProperty === BUSINESS_VALID_FROM) {
											oCtrlBusinessValidFrom = oCtrl;
										} else if (sProperty === BUSINESS_VALID_TO) {
											oCtrlBusinessValidTo = oCtrl;
										}

										if (!bChecked && sProperty) {
											var oBondedProp = oEntityType.property.find(function (oProp) {
												return oProp.name === sProperty;
											});

											if (sProperty !== BUSINESS_VALID_TO) {
												if (oBondedProp && oBondedProp.nullable === "false" && (oValue === null || oValue.toString() === "")) {
													oCtrl.setValueState(sap.ui.core.ValueState.Error); // if the field is empty after change, it will go red
													oCtrl.setValueStateText(that.getText("mapping.value_must_not_be_empty"));

													bResult = false;
												} else if (oBondedProp.type.includes("Int") && oValue && isNaN(parseInt(oValue.toString()))) {
													oCtrl.setValueState(sap.ui.core.ValueState.Error);
													oCtrl.setValueStateText(that.getText("mapping.integer_expected"));

													bResult = false;
												} else if (oBondedProp.hasOwnProperty("maxLength") && oValue !== null && oValue.length > oBondedProp.maxLength) {
													oCtrl.setValueState(sap.ui.core.ValueState.Error);
													oCtrl.setValueStateText(that.getText("mapping.value_too_large"));

													bResult = false;
												} else {
													oCtrl.setValueState(sap.ui.core.ValueState.None); // if the field is not empty after change, the value state (if any) is removed
												}
											} else {
												oCtrl.setValueState(sap.ui.core.ValueState.None); // if the field is not empty after change, the value state (if any) is removed
											}
										}
									}
								}
							}
						});

						if (oCtrlBusinessValidTo.getDateValue() && oCtrlBusinessValidFrom.getDateValue() && oCtrlBusinessValidTo.getDateValue() <
							oCtrlBusinessValidFrom.getDateValue()) {
							oCtrlBusinessValidTo.setValueState(sap.ui.core.ValueState.Error); // if the field is empty after change, it will go red
						}
					}
				}
			}
			return bResult;
		},
		
			validateInputEntry: function (oEntry, oEntityType ) {
			var bResult = true;
			var Properties = oEntityType["property"];
			
			for (var i=0; i< Properties.length ; i++){
			var name = Properties[i].name,
				type = Properties[i].type;
			var Entry = Object.entries(oEntry);
			

			for (var j = 0; j < Entry.length; j++) {
				if (Entry[j][0] === name){
						if(Entry[j][0] === "BusinessValidFrom"){
							var valid_from = Entry[j][1];
						}
						if(Entry[j][0] === "BusinessValidTo"){
							var valid_to = Entry[j][1];
						}
						if( Entry[j][1] !== undefined && Properties[i].hasOwnProperty("maxLength") && Properties[i].maxLength < Entry[j][1].length){
							bResult = false;
						}
						
						if( Entry[j][1] !== undefined && Properties[i].hasOwnProperty("maxLength") && Properties[i].maxLength == 5 && Entry[j][1].toLowerCase() !== "true" && Entry[j][1].toLowerCase() !== "false" ){
							var klein = Entry[j][1].toLowerCase();
							bResult = false;
						}
						
						if(Properties[i].hasOwnProperty("nullable") && Properties[i].nullable === false && (Entry[j][1] === undefined || Entry[j][1] === "" ) ){
							bResult = false;
						}
						
						if(type === "Edm.DateTime" && Entry[j][1] !== undefined && !this.isValidDate(Entry[j][1])){
							bResult = false;
						}	
						
						if(type.includes("Int") && Entry[j][1] !== undefined && !Number.isInteger(Entry[j][1] - 0) ){
							bResult = false;
						}

						if(type === "Edm.Float" && Entry[j][1] !== undefined && Number.parseFloat(Entry[j][1]) !== Entry[j][1]){
							bResult = false;	
						}
						
						if(type === "Edm.Decimal" && (Properties[i].precision < (Entry[j][1].length -1) || Number.parseFloat(Entry[j][1]).toFixed(Properties[i].scale) !== Entry[j][1] )){
							bResult = false;
						}

						if(valid_from && valid_to && valid_from > valid_to)	{
							bResult = false;
						}
							
				}
			}	
			}
	
			return bResult;
		},

			
		valueChangeHandler: function (oEvent) {
			if (this._sCurrentTableId) {
				if (oEvent.getSource().getMetadata().getName() === "sap.m.Switch") {
					oEvent.getSource().getBinding("state").setValue(oEvent.getSource().getState());
				} else if (oEvent.getSource().getMetadata().getName() === "sap.m.RadioButtonGroup") {
					var text = oEvent.getSource().getSelectedButton().getText();
					if (text === 'N/A') {
						oEvent.getSource().getBinding("selectedIndex").setValue(" ");
					} else {
						oEvent.getSource().getBinding("selectedIndex").setValue(text);
					}
				}

				if (this._oEditEntryDialog && this._oEditEntryDialog.isOpen()) {
					this.validateInput(this._oEditEntryDialog.getContent()[0].getContent());
				} else if (this.getCachedTableData(this._sCurrentTableId).ImportDialog.isOpen()) {
					this.setImportEntryStatus(oEvent.getSource().getParent());
				}
			}
		},

		stateChangeHandler: function (oEvent) {
			var oTable = oEvent.getSource().getParent().getParent();

			this._sCurrentTableId = oTable.getId();

			if (this._sCurrentTableId) {
				var oTableCache = this.getCachedTableData(this._sCurrentTableId),
					oTblEvent = new sap.ui.base.Event(oEvent.getId(), oEvent.getSource(), oEvent.getParameters());

				if (oTableCache.Handles.valueChangeHandle) {
					oTableCache.Handles.valueChangeHandle.call(oTableCache.Controller, oTblEvent);
				}
			}
		},

		getView: function (oCtrl) {
			var result = null;

			if (oCtrl) {
				while (oCtrl && oCtrl.getParent()) {
					oCtrl = oCtrl.getParent();

					if (oCtrl instanceof sap.ui.core.mvc.View) {
						result = oCtrl;
						break;
					}
				}
			}
			return result;
		},

		getTableIdByToolBarAction: function (oEvent) {
			var sResult = null;

			if (oEvent) {
				if (oEvent.getSource().getParent().getMetadata().getName() === "sap.m.Toolbar" && oEvent.getSource().getParent().getParent().getMetadata()
					.getName() === _SAP_UI_TABLE) {
					sResult = oEvent.getSource().getParent().getParent().getId();
				}
			}
			return sResult;
		},

		handleAddEntry: function (oEvent) {
			this._sCurrentTableId = this.getTableIdByToolBarAction(oEvent);

			if (this._sCurrentTableId) {
				var oController = this.getCachedTableData(this._sCurrentTableId).Controller,
					oTable = oController.getView().byId(this._sCurrentTableId);

				this.getDialogEditEntry(oTable, oController, "", this.valueChangeHandler).open();
			}
		},

		handleEditEntry: function (oEvent) {
			var oTable,
				oTblCache,
				oController,
				sPath,
				aSelected;

			if (oEvent.getSource().getParent().getMetadata().getName() === "sap.ui.table.RowAction") {
				oTable = oEvent.getSource().getParent().getParent().getParent();

				if (oTable) {
					this._sCurrentTableId = oTable.getId();
					oTable.setSelectedIndex(oEvent.getSource().getParent().getParent().getIndex());
				}
			} else {
				this._sCurrentTableId = this.getTableIdByToolBarAction(oEvent);
			}

			if (this._sCurrentTableId) {
				oTblCache = this.getCachedTableData(this._sCurrentTableId);
				oController = oTblCache.Controller;

				if (oTable) {
					aSelected = oTable.getSelectedIndices();
					var that = this;

					if (aSelected.length > 0) {
						aSelected.forEach(function (iIndex) {
							sPath = oTable.getContextByIndex(iIndex).sPath;

							if (oTblCache.Handles.hasOwnProperty("CustomEditHandle")) {
								try {
									that._sEditItemPath = sPath;
									oTblCache.Handles.CustomEditHandle.call(oTblCache.Controller, oTable, iIndex);
								} catch (err) {
									ErrorHandler.handleOdataError(err);
								}
							} else {
								that.getDialogEditEntry(oTable, oController, sPath).open();
							}
						});
					}
				}
			}
		},

		handleDeleteEntry: function (oEvent) {
			if (oEvent.getSource().getParent().getMetadata().getName() === "sap.ui.table.RowAction") {
				var oTable = oEvent.getSource().getParent().getParent().getParent();

				if (oTable) {
					this._sCurrentTableId = oTable.getId();
					oTable.setSelectedIndex(oEvent.getSource().getParent().getParent().getIndex());
				}
			} else {
				this._sCurrentTableId = this.getTableIdByToolBarAction(oEvent);
			}

			if (this._sCurrentTableId) {
				var that = this,
					oTblDelBtnEvent = new sap.ui.base.Event(oEvent.getId(), oEvent.getSource(), oEvent.getParameters()),
					oTableCache = this.getCachedTableData(this._sCurrentTableId);
				if (oTableCache) {
					var oRscBundle = oTableCache.Controller.getView().getModel("i18n").getResourceBundle();
					var oSubmitDial = this.oApproveDialog = new sap.m.Dialog({
						type: sap.m.DialogType.Message,
						title: oRscBundle.getText("title_confirm_delete"),
						content: new sap.m.Text({
							text: oRscBundle.getText("question_delete_items")
						}),
						beginButton: new sap.m.Button({
							type: sap.m.ButtonType.Emphasized,
							text: oRscBundle.getText("submit"),
							press: function () {
								that.onDeleteEntryPress.call(this, oTblDelBtnEvent);
								this.oApproveDialog.close();
							}.bind(this)
						}),
						endButton: new sap.m.Button({
							text: oRscBundle.getText("cancel"),
							press: function () {
								this.oApproveDialog.close();
							}.bind(this)
						})
					});
					oSubmitDial.open();
				}
			}
		},

		entryExistInTable: function (oEntry, oTable) {
			var bResult = false;

			if (oEntry && oTable) {
				var oTblCache = this.getCachedTableData(oTable.getId()),
					oEntity = this.getBondedEntity(oTable, oTblCache.ODataModel);

				if (oEntity) {
					var that = this,
						aEntries = Object.entries(oTblCache.ODataModel.oData);

					for (var i = 0; i < oEntity.__entityType.key.propertyRef.length; i++) {
						var oProperty = oEntity.__entityType.key.propertyRef[i];

						aEntries = aEntries.filter(function (oData) {
							if (Array.isArray(oData) && oData.length > 1) {
								var oValue = oEntry[oProperty.name],
									oDataEntry = oData[1];

								if (oProperty.name === BUSINESS_VALID_FROM || oProperty.name === BUSINESS_VALID_TO) {
									if (!oValue) {
										oValue = that.getMaxDate();
									}
									return oValue.getFullYear() === oDataEntry[oProperty.name].getFullYear() && oValue.getMonth() === oDataEntry[oProperty.name]
										.getMonth() && oValue.getDate() === oDataEntry[oProperty.name].getDate();
								} else {
									return oDataEntry[oProperty.name] === oValue;
								}
							}
						});
						bResult = aEntries && aEntries.length > 0;

						if (!bResult) {
							break;
						}
					}
				}
			}
			return bResult;
		},

		checkEntryStatus: function (oEntry, oTable) {
			var iResult = 0;

			if (oEntry && oTable) {
				var oTblCache = this.getCachedTableData(oTable.getId()),
					oEntity = this.getBondedEntity(oTable, oTblCache.ODataModel);

				if (oEntity) {
					var that = this,
						oValidFrom,
						oValidTo,
						sPath = "/" + oEntity.name;

					if (oTblCache.ODataModel) {
						var oData = oTblCache.ODataModel.getProperty("/");

						if (oData) {
							oData = Object.entries(oData);

							for (var i = 0; i < oData.length; i++) {
								if (oData[i][0].startsWith(sPath.substring(1))) {
									var oDataEntry = oData[i][1],
										bDubplicate = true;

									oEntity.__entityType.key.propertyRef.forEach(function (oProperty) {
										if (oProperty.name === BUSINESS_VALID_FROM) {
											oValidFrom = oEntry[oProperty.name];

											if (oValidFrom) {
												oValidFrom = oValidFrom.getTime();
											}
										} else if (oProperty.name === BUSINESS_VALID_TO) {
											oValidTo = oEntry[oProperty.name];

											if (oValidTo) {
												oValidTo = oValidTo.getTime();
											} else {
												oValidTo = that.getMaxDate();
											}
										} else if (oProperty.name !== SYSTEM_VALID_FROM && oProperty.name !== SYSTEM_VALID_TO && oProperty.name !== LAST_CHANGE_TIMESTAMP && oProperty.name !== LAST_CHANGE_APPLICATION_USER_NAME && oEntry[oProperty.name] !== oDataEntry[oProperty.name] && (oEntry[oProperty.name].length !== 0 || oDataEntry[oProperty.name].length !== 0) ) {
											if(oEntry[oProperty.name].length === 1 || oDataEntry[oProperty.name].length === 1){
												if(oEntry[oProperty.name].replace(/\s+/g, '') !== oDataEntry[oProperty.name].replace(/\s+/g, '')){
												bDubplicate = false;}
												
											} else{ bDubplicate = false;} 
										}
									});

									if (bDubplicate) {
										var oDataEntry_ValidFrom = oDataEntry[BUSINESS_VALID_FROM],
											oDataEntry_ValidTo = oDataEntry[BUSINESS_VALID_TO];

										if (oDataEntry_ValidFrom) {
											oDataEntry_ValidFrom = oDataEntry_ValidFrom.getTime();
										}
										if (oDataEntry_ValidTo) {
											oDataEntry_ValidTo = oDataEntry_ValidTo.getTime();
										} else {
											oDataEntry_ValidTo = that.getMaxDate();
										}

										if (!(oDataEntry_ValidFrom === oValidFrom && oDataEntry_ValidTo === oValidTo)) {
											bDubplicate = false;

											if ((oDataEntry_ValidFrom <= oValidFrom && oDataEntry_ValidTo > oValidFrom && oDataEntry_ValidTo <= oValidTo) || (
													oDataEntry_ValidFrom >= oValidFrom && oDataEntry_ValidFrom < oValidTo && oDataEntry_ValidTo > oValidTo) || (
													oDataEntry_ValidFrom < oValidFrom && oDataEntry_ValidTo > oValidTo)) {
												iResult = 1;
												break;
											}
										} else {
											iResult = 2;
											break;
										}
									}
								}
							}
						}
					}
				}
			}
			if (oEntry.ELSECase === 'true' || oEntry.ELSECase === true) {
				var sTableName = oTable.sId.substring(15),
					oEntries = oTable.getModel(undefined).oData[sTableName];
				for (var i = 0; i < oEntries.length; i++) {
					if (oEntries[i].ELSECase === 'true' || oEntries[i].ELSECase === true) {
						iResult = 3;
						break;
					}
				}
			}
			return iResult;
		},

		getValidityOverlapFilter: function (oRefDateFrom, oRefDateTo) {
			var oResult;

			if (oRefDateFrom && oRefDateTo) {
				var sDateFrom = this.getDateString(oRefDateFrom),
					sDateTo = this.getDateString(oRefDateTo);

				oResult = new sap.ui.model.Filter({
					filters: [
						new sap.ui.model.Filter({
							filters: [
								new sap.ui.model.Filter({
									path: BUSINESS_VALID_FROM,
									operator: sap.ui.model.FilterOperator.LE,
									value1: sDateFrom
								}),
								new sap.ui.model.Filter({
									path: BUSINESS_VALID_TO,
									operator: sap.ui.model.FilterOperator.GT,
									value1: oRefDateFrom
								}),
								new sap.ui.model.Filter({
									path: BUSINESS_VALID_TO,
									operator: sap.ui.model.FilterOperator.LE,
									value1: sDateTo
								})
							],
							and: true
						}),
						new sap.ui.model.Filter({
							filters: [
								new sap.ui.model.Filter({
									path: BUSINESS_VALID_FROM,
									operator: sap.ui.model.FilterOperator.GE,
									value1: sDateFrom
								}),
								new sap.ui.model.Filter({
									path: BUSINESS_VALID_FROM,
									operator: sap.ui.model.FilterOperator.LT,
									value1: sDateTo
								}),
								new sap.ui.model.Filter({
									path: BUSINESS_VALID_TO,
									operator: sap.ui.model.FilterOperator.GE,
									value1: sDateTo
								})
							],
							and: true
						}),
						new sap.ui.model.Filter({
							filters: [
								new sap.ui.model.Filter({
									path: BUSINESS_VALID_FROM,
									operator: sap.ui.model.FilterOperator.LE,
									value1: sDateFrom
								}),
								new sap.ui.model.Filter({
									path: BUSINESS_VALID_TO,
									operator: sap.ui.model.FilterOperator.GE,
									value1: sDateTo
								})
							],
							and: true
						}),
						new sap.ui.model.Filter({
							filters: [
								new sap.ui.model.Filter({
									path: BUSINESS_VALID_FROM,
									operator: sap.ui.model.FilterOperator.EQ,
									value1: sDateFrom
								}),
								new sap.ui.model.Filter({
									path: BUSINESS_VALID_TO,
									operator: sap.ui.model.FilterOperator.EQ,
									value1: sDateTo
								})
							],
							and: true
						})
					],
					and: false
				});
			}
			return oResult;
		},

		getDateTimeString: function (oDate) {
			var sResult;

			if (oDate) {
				var sYear = oDate.getFullYear().toString(),
					sMonth = (oDate.getMonth() + 1).toString(),
					sDate = oDate.getDate().toString(),
					sHours = oDate.getHours().toString(),
					sMins = oDate.getMinutes().toString(),
					sSecs = oDate.getSeconds().toString(),
					sMilli = oDate.getMilliseconds().toString();

				sMonth = sMonth.length < 2 ? "0" + sMonth : sMonth;
				sDate = sDate.length < 2 ? "0" + sDate : sDate;
				sHours = sHours.length < 2 ? "0" + sHours : sHours;
				sMins = sMins.length < 2 ? "0" + sMins : sMins;
				sSecs = sSecs.length < 2 ? "0" + sSecs : sSecs;

				while (sMilli.length < 7) {
					sMilli = "0" + sMilli;
				}
				sResult = sYear.concat("-", sMonth, "-", sDate, "T", sHours, ":", sMins, ":", sSecs, ".", sMilli);
			}
			return sResult;
		},

		getDateString: function (oDate) {
			var sResult;

			if (oDate) {
				var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
					pattern: "YYYY-MM-dd"
				});

				sResult = dateFormat.format(oDate).toString();
			}
			return sResult;
		},

		getOdataContextByJsonModelPath: function (oCtrl, sPath, oDataModel) {
			var oCtx;

			if (oCtrl) {
				var oObj = oCtrl.getModel().getProperty(sPath);

				if (oObj) {
					var sOdataPath = oObj.__metadata.uri.substring(oObj.__metadata.uri.lastIndexOf("/"));

					oCtx = oDataModel.getContext(sOdataPath);
				}
			}
			return oCtx;
		},

		handleCloseDialog: function (oEvent) {
			this.clearTmpModel();

			if (oEvent && oEvent.getSource() && oEvent.getSource().getParent()) {
				oEvent.getSource().getParent().close();

				if (this._sCurrentTableId) {
					var oTableCache = this.getCachedTableData(this._sCurrentTableId);

					if (oTableCache.Controller) {
						var oTable = oTableCache.Controller.getView().byId(this._sCurrentTableId);

						if (oTable) {
							oTable.setBusy(false);
							this.enableToolBar(oTable, true);
						}
					}
				}
			}
		},

		clearTmpModel: function () {
			var oModel = sap.ui.getCore().getModel("tmp");

			if (oModel) {
				oModel.setData(undefined);
			}
		},

		setChangeGroup: function (oModel, sGroupId) {
			if (oModel && sGroupId && !oModel.getDeferredBatchGroups().includes(sGroupId)) {
				oModel.setDeferredGroups([sGroupId]);
			}
		},

		getUpdateParameters: function (sGroupId) {
			var that = this;
			var oParams = {
				success: function () {
					that.handleODataProcessingResult.call(that, true);
					that._iSuccessfullRequests++;
				},
				error: function (oError) {
					that._iFailedRequests++;
					that.handleODataProcessingResult.call(that, false, oError);
				}
			};

			if (sGroupId) {
				oParams.groupId = sGroupId;
			}
			return oParams;
		},

		handleODataProcessingResult: function (bSuccess, oError) {
			var oTblCache = this.getCachedTableData(this._sCurrentTableId),
				oModel = oTblCache.ODataModel;

			this._iUpdates++;

			if (this._iUpdates === this._iExpectedUpdates) {
				var sMsgErrorsOccured = this._iFailedRequests.toString().concat(" data records could not be processed due to errors!");

				if (this._oEditEntryDialog && this._oEditEntryDialog.isOpen()) {
					if (this._oEditEntryDialog.isBusy()) {
						this._oEditEntryDialog.setBusy(false);
					}
					this._oEditEntryDialog.close();
				}

				if (this._sAction && this._sAction === "delete") {
					this._sAction = "";
				}

				if (bSuccess) {
					this.clearTmpModel();
					oModel.refresh();

					if (this._sAction && this._sAction === "delete") {
						sap.m.MessageToast.show("item deleted");
					}

					if (this._iFailedRequests > 0) {
						ErrorHandler.handleOdataError(sMsgErrorsOccured);
					}
					this.setDataModel(oTblCache.Controller.getView().byId(this._sCurrentTableId), oModel);
				} else {
					var oTable = oTblCache.Controller.getView().byId(this._sCurrentTableId);

					if (this._iExpectedUpdates !== 1) {
						oError = sMsgErrorsOccured;
					}

					if (oTblCache.Handles && oTblCache.Handles.hasOwnProperty("oDataLoadedHandle") && oTblCache.Handles.oDataLoadedHandle) {
						try {
							oTblCache.Handles.oDataLoadedHandle.call(oTblCache.Controller, oError);
						} catch (err) {
							ErrorHandler.handleOdataError(err);
						}
					} else {
						ErrorHandler.handleOdataError(oError);
					}

					if (oTable) {
						oTable.setBusy(false);
						this.enableToolBar(oTable, true);
					}
				}
			} else if (oError) {
				console.log(oError);
			}
		},

		onDeleteEntryPress: function () {
			var oTblCache = this.getCachedTableData(this._sCurrentTableId);

			if (oTblCache) {
				var oTable = oTblCache.Controller.getView().byId(this._sCurrentTableId);

				if (oTable) {
					var aSelected = oTable.getSelectedIndices(),
						that = this;

					this._sCurrentTableId = oTable.getId();

					if (aSelected.length > 0) {
						var oModel = oTblCache.ODataModel;

						this._iUpdates = 0;
						this._iExpectedUpdates = aSelected.length;
						this._sAction = "delete";
						this._iSuccessfullRequests = 0;
						this._iFailedRequests = 0;

						this.setToolbarButtonsEnabled(oTable, false);
						oTable.setBusy(true);
						this.callTableBusyEvent(oTable);

						aSelected.forEach(function (iIndex) {
							var sPath = oTable.getContextByIndex(iIndex).sPath,
								oEntity = that.getBondedEntity(oTable, oModel),
								iDataIndex = sPath.substring(sPath.lastIndexOf("/") + 1),
								oTblModel = oTable.getModel(),
								oData = oTblModel.getData(),
								oEntry = oData[oEntity.name][iDataIndex],
								sOdataPath = oEntry.__metadata.uri.substring(oEntry.__metadata.uri.lastIndexOf("/")),
								oCtx = oModel.getContext(sOdataPath);

							oModel.remove(oCtx.getPath(), that.getUpdateParameters());
						});
					}
				}
			}
		},

		getMaxDate: function () {
			return new Date('9999-12-31');
		},

		createEntry: function (oTable, oEntryData) {
			if (oTable) {
				var oTblCache = this.getCachedTableData(oTable.getId()),
					oEntry = oEntryData ? oEntryData : oTblCache.Controller.getView().getModel("tmp").getObject("/Entry/"),
					oEntity = this.getBondedEntity(oTable, oTblCache.ODataModel),
					oModel = oTblCache.ODataModel,
					that = this;

				if (oEntry.hasOwnProperty(BUSINESS_VALID_TO) && !oEntry.BusinessValidTo) {
					oEntry.BusinessValidTo = this.getMaxDate();
				}

				Object.getOwnPropertyNames(oEntry).forEach(function (sName) {
					if (typeof oEntry[sName] === "boolean") {
						oEntry[sName] = oEntry[sName].toString();
					}
				});

				if (oTblCache.Handles.hasOwnProperty("beforeCreateEntry") && oTblCache.Handles.beforeCreateEntry) {
					var oArgs = {
						Table: oTable,
						Entry: oEntry,
						Cancel: false
					};
					try {
						oTblCache.Handles.beforeCreateEntry.call(oTblCache.Controller, oArgs);

						if (oArgs.Cancel) {
							this._oEditEntryDialog.setBusy(false);

							return;
						}
					} catch (err) {
						ErrorHandler.handleOdataError(err);
					}
				}

				var mParameters = that.getUpdateParameters();

				that._sCurrentTableId = oTable.getId();
				mParameters.properties = oEntry;
				that._iExpectedUpdates = 1;
				that._iUpdates = 0;
				that._iSuccessfullRequests = 0;
				that._iFailedRequests = 0;

				oModel.create("/" + oEntity.name, oEntry, mParameters);

			}
		},
		
		updateEntry: function (oTable, oEntryData, sPath) {
			var oTblCache = this.getCachedTableData(oTable.getId()),
				oModel = oTblCache.ODataModel,
				oEntry = oEntryData ? oEntryData : oTblCache.Controller.getView().getModel("tmp").getObject("/Entry/"),
				oCtx = this.getOdataContextByJsonModelPath(oTable, (!sPath ? this._sEditItemPath : sPath), oTblCache.ODataModel);
			
			// this.handleDefaultErrorDialog(oData);
			if (oEntry.hasOwnProperty("CustomMapping")) {
				oEntry.CustomMapping = 'true';
			} else if (oEntry.hasOwnProperty("IsCustomMapping")) {
				oEntry.IsCustomMapping = 'true';
			}
			
			Object.getOwnPropertyNames(oEntry).forEach(function (sName) {
				if (typeof oEntry[sName] === "boolean") {
					oEntry[sName] = oEntry[sName].toString();
				}
			});

			this._sCurrentTableId = oTable.getId();
			this._iExpectedUpdates = 1;
			this._iUpdates = 0;
			this._iSuccessfullRequests = 0;
			this._iFailedRequests = 0;

			this.callTableBusyEvent(oTable);
			oModel.update(oCtx.getPath(), oEntry, this.getUpdateParameters());
		},

		ensureExpectedODataTypes: function (oEntry, sEntityName, oModel) {
			if (oEntry && sEntityName && oModel) {
				var oEntity = this.getEntityByName(sEntityName, oModel);

				if (oEntity) {
					oEntity.__entityType.property.forEach(function (oProperty) {
						if (oProperty.type === "Edm.String") {
							oEntry[oProperty.name] = oEntry[oProperty.name] ? oEntry[oProperty.name].toString() : "";
						} else if (oProperty.type === _EDM_DATETIME) {
							oEntry[oProperty.name] = oEntry[oProperty.name].getDateValue();
						} else if (oProperty.type.includes("Int")) {
							oEntry[oProperty.name] = parseInt(oEntry[oProperty.name]);
						}
					});
				}
			}
		},

		handleSubmitEntry: function () {
			var defError = false,
				oTable = this._oContent.Table,
				oTblCache = this.getCachedTableData(oTable.getId()),
				oEntry = oTblCache.Controller.getView().getModel("tmp").getObject("/Entry/"),
				sTableName = oTable.sId.substring(15),
				oData = oTable.getModel(undefined).oData[sTableName];
			
			this._oEditEntryDialog.setBusy(true);
			this._oEditEntryDialog.invalidate();
			this.callTableBusyEvent(this._oContent.Table);
			
			if (oEntry.ELSECase === 'true' || oEntry.ELSECase === true) {
				for (var i = 0; i < oData.length; i++) {
					if (oData[i].ELSECase === 'true' || oData[i].ELSECase === true) {
						defError = true;
						break;
					}
				}
				
				if (defError === true) {
					MessageBox.error("A record with ELSECase = true already exists. Please set ELSECase to false in the relevant record before you can set ELSECase to true for this record.");
					this._oEditEntryDialog.setBusy(false);
					return;
				}
			}
			
			if (this.validateInput(this._oEditEntryDialog.getContent()[0].getContent())) {
				
				if (this._sEditItemPath === "") {
					this.createEntry(this._oContent.Table);
				} else {
					this.updateEntry(this._oContent.Table);
				}
			} else {
				this._oEditEntryDialog.setBusy(false);
			}
		},

		handleImportEntries: function (oEvent) {
			var oTable = oEvent.getSource().getParent().getContent()[0];
			if (oTable) {
				var aIndices = oTable.getSelectedIndices();

				if (aIndices && aIndices.length > 0) {
					var aEntries = [];

					for (var i = 0; i < aIndices.length; i++) {
						var oEntry = oTable.getContextByIndex(aIndices[i]).getObject();

						if (oEntry && oEntry.status) {
							if (oEntry.status.status === "Success" && oEntry.data) {
								aEntries.push(oEntry.data);
							} else if (oEntry.status.status === "Warning") {
								MessageBox.warning("At least one of the selected data contains warnings. The data includes overlapping time intervals with existing data."); 
								return;
							} else {
								MessageBox.error("At least one of the selected data contains errors. Please correct the data record or deselect it");
								return;
							}
						}
					}

					if (aEntries.length > 0) {
						var sBaseTableId = oTable.getId().replace("_import", ""),
							oTblCache = this.getCachedTableData(sBaseTableId);

						if (oTblCache && oTblCache.ODataModel) {
							var oModel = oTblCache.ODataModel,
								oOriginalTable = oTblCache.Controller.getView().byId(sBaseTableId);

							var sPath = oOriginalTable.getBindingInfo("rows").path,
								oParams = this.getUpdateParameters();

							this._iUpdates = 0;
							this._iExpectedUpdates = aEntries.length;
							this._iSuccessfullRequests = 0;
							this._iFailedRequests = 0;

							aEntries.forEach(function (oData) {
								oModel.create(sPath, oData, oParams);
							});
							oTblCache.ImportDialog.close();
						}
					}
				} else {
					MessageBox.error("Please select at least one entry");
				}
			}
		},

		showBusyIndicator: function (bShow) {
			if (bShow) {
				sap.ui.core.BusyIndicator.BusyIndicator.show();
			} else {
				sap.ui.core.BusyIndicator.BusyIndicator.hide();
			}
		},

		getText: function (sKey) {
			var sResult = sKey,
				oModel = sap.ui.getCore().getModel("i18n");

			if (oModel && oModel.getResourceBundle()) {
				sResult = oModel.getResourceBundle().getText(sKey);
			}
			return sResult;
		}
	};
	return TableGenerator;
});