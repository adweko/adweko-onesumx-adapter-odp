sap.ui.define([

], function () {
	"use strict";

	var Formatter = {
		getDefaultDateFormat: function () {
			return "MMM d, y";
		},

		dateToString: function (oDate) {
			var sResult = "";

			if (oDate) {
				if (oDate.getFullYear().toString() === "9999" && (oDate.getMonth() + 1).toString() && oDate.getDate().toString() === "31") {
					sResult = "unlimited";
				} else {
					var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
							pattern: this.getDefaultDateFormat()
						}),
						dateFormatted = dateFormat.format(oDate);

					sResult = dateFormatted.toString();
				}
			}
			return sResult;
		},

		toString: function (oValue) {
			return oValue ? oValue.toString() : "";
		}
	};
	return Formatter;
});
