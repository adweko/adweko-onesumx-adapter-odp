sap.ui.define([], function () {
	"use strict";
	var _COUNTDOWN = 840000; /* 14 minutes; SESSION_TIMEOUT defaults to 15 minutes */
	var _RESET_COUNTDOWN = 840000;

	var SessionHandler = {
		setSessionListener: function () {
			var that = this;

			this.setInactivityTimeout(840000);
			this.startInactivityTimer();

			document.addEventListener("keypress", function onPress() {
				that.resetInactivityTimeout();
			});

			document.onmousedown = function () {
				that.resetInactivityTimeout();
			};
		},

		/**
		 * Return number of milliseconds left till automatic logout
		 */
		getInactivityTimeout: function () {
			return _COUNTDOWN;
		},

		/**
		 * Set number of minutes left till automatic logout
		 */
		setInactivityTimeout: function (timeout_millisec) {
			_COUNTDOWN = timeout_millisec;
			_RESET_COUNTDOWN = _COUNTDOWN;
		},

		/**
		 * Set number of minutes left till automatic logout
		 */
		resetInactivityTimeout: function () {
			_COUNTDOWN = _RESET_COUNTDOWN;
		},

		/**
		 * Begin counting tracking inactivity
		 */
		startInactivityTimer: function () {
			var self = this;
			this.intervalHandle = setInterval(function () {
				self._inactivityCountdown();
			}, 10000);
		},

		stopInactivityTimer: function () {
			if (this.intervalHandle !== null) {
				clearInterval(this.intervalHandle);
				this.intervalHandle = null;
			}
		},

		_inactivityCountdown: function () {
			_COUNTDOWN -= 10000;
			if (_COUNTDOWN <= 0) {
				this.stopInactivityTimer();
				this.resetInactivityTimeout();
				sap.m.URLHelper.redirect("/logout", false);
			}
		}
	};
	return SessionHandler;
});