sap.ui.define([
	"sap/m/MessageBox"
], function (MessageBox) {
	"use strict";

	var ErrorHandler = {
		handleOdataError: function (oError) {
			var sMsg = "";
			console.log(oError);

			try {
				if (oError) {
					if (oError.hasOwnProperty("statusCode") && (typeof (oError.statusCode) !==
							'function' && typeof (oError.statusCode) !== 'object') && typeof (oError.statusCode) !== 'undefined' && oError.hasOwnProperty(
							"statusText") && typeof (oError.statusText) === 'string') {
						sMsg = oError.statusCode ? oError.statusCode.toString().concat(" - ", oError.statusText) : oError.statusText;
					} else if (oError.hasOwnProperty("status") && oError.hasOwnProperty("statusCode") && (typeof (oError.status) ===
							'string' ||
							typeof (oError.status) === 'number') && oError.hasOwnProperty("statusText") && typeof (oError.statusText) === 'string') {
						sMsg = oError.status ? oError.status.toString().concat(" - ", oError.statusText) : oError.statusText;
					}

					if (oError.hasOwnProperty("responseText") && (!oError.hasOwnProperty("statusText") || oError.statusText !== oError.responseText)) {
						var sRespText = oError.responseText;
						var aResult;

						if (oError.responseText.includes("</message>")) {
							aResult = sRespText.match(/<message.*>(.+?)<\/message>/);

							if (aResult && aResult.length >= 1) {
								sMsg += "\n" + aResult[1];
							}
						}
						if (oError.responseText.includes("</exception>")) {
							aResult = sRespText.match(/<exception.*>(.+?)<\/exception>/);

							if (aResult && aResult.length >= 1) {
								sMsg += "\n" + aResult[1];
							}
						} else {
							aResult = oError.responseText.match(/\{.*\:.*\}/);

							if (aResult && aResult.length > 0) {
								var oErrorObj = JSON.parse(oError.responseText);

								if (oErrorObj && oErrorObj.hasOwnProperty("error")) {
									sMsg += "\n";

									if (oErrorObj.error.hasOwnProperty("innererror")) {
										var aInnerMatches = oErrorObj.error.innererror.match(/\{.*\:.*\}/);

										if (aInnerMatches && aInnerMatches.length > 0) {
											oErrorObj = JSON.parse(oErrorObj.error.innererror);
										}
									} else {
										oErrorObj = oErrorObj.error;
									}
									if (oErrorObj) {
										if (oErrorObj.hasOwnProperty("code")) {
											sMsg = oErrorObj.code;
										}
										if (oErrorObj.hasOwnProperty("message")) {
											if (typeof (oErrorObj.message) === 'string') {
												sMsg += ' - ' + oErrorObj.message;
											} else {
												sMsg += ' - ' + oErrorObj.message.value;
											}
										}
									}
								}
							} else {
								sMsg = oError.responseText;
							}
						}
					}

					if (typeof (oError) === "string" && sMsg === "") {
						sMsg = oError;
					}
				}
			} catch (err) {
				console.log(err);
			} finally {
				if (sMsg === "") {
					sMsg = 'Something went totally wrong...';
				}

				MessageBox.error(
					sMsg, {
						styleClass: ""
					}
				);
			}
		}
	};
	return ErrorHandler;
});