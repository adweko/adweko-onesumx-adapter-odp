sap.ui.define([
	"com/adweko/adapter/osx/ui/utils/ErrorHandler"
], function (ErrorHandler) {
	"use strict";

	return {
		getPermission: function (sAction, oModel) {
			var bResult = false;

			if (oModel) {
				var aScopes = oModel.oData.scopes;

				if (aScopes) {
					bResult = aScopes.filter(function (row) {
						return (row.endsWith("." + sAction));
					}).length > 0;
				}
			}
			return bResult;
		},

		userHasPermissionToEdit: function () {
			// var incl = sap.ui.getCore().getModel("user").getData().scopes.find(element => element.includes(".Edit"));
			// if (incl == undefined) { return false; } else { return true; }
			return true;
		},

		checkScope: function (sScope, oController, cbResult) {
			$.ajax({
				type: 'GET',
				url: '/',
				headers: {
					'X-Csrf-Token': 'Fetch'
				},
				success: function (res, status, xhr) {
					var sHeaderCsrfToken = 'X-Csrf-Token';
					var sCsrfToken = xhr.getResponseHeader(sHeaderCsrfToken);

					$.ajax({
						url: "../xsjs/security/securityContext.xsjs/checkScope",
						type: 'POST',
						data: $.param({
							"scope": "$XSAPPNAME.".concat(sScope)
						}),
						contentType: 'application/x-www-form-urlencoded',
						beforeSend: function (xhrP) {
							xhrP.setRequestHeader(sHeaderCsrfToken, sCsrfToken);
						},
						async: true,
						success: function (oEvent) {
							if (oEvent && oEvent.hasOwnProperty("result")) {
								cbResult.call(oController, oEvent.result);
							} else {
								ErrorHandler.handleOdataError({});
							}
						},
						error: function (oError) {
							ErrorHandler.handleOdataError(oError);

							if (cbResult) {
								cbResult.call(oController, false);
							}
						}
					});
				}
			});
		}
	};
});