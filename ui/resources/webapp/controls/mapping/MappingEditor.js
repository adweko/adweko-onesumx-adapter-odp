sap.ui.define([
	"sap/ui/core/Control",
	"sap/m/Dialog",
	"sap/m/DialogRenderer",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"com/adweko/adapter/osx/ui/utils/Formatter"
], function (Control, Dialog, DialogRenderer, JSONModel, MessageBox, Formatter) {
	"use strict";
	return Dialog.extend("com.adweko.adapter.osx.ui.controls.mapping.MappingEditor", {
		metadata: {
			properties: {
				type: {
					type: "string",
					defaultValue: "default"
				},
				mode: {
					type: "string",
					defaultValue: "edit"
				},
				mappingName: {
					type: "string",
					defaultValue: ""
				},
				mappingDescription: {
					type: "string",
					defaultValue: ""
				},
				allowComplexNestedExpressions: {
					type: "boolean",
					defaultValue: false
				},
				showTargetViewField: {
					type: "boolean",
					defaultValue: true
				},
				showTargetViewAttributeField: {
					type: "boolean",
					defaultValue: true
				},
				limitedTableEditablility: {
					type: "boolean",
					defaultValue: false
				},
				editable: {
					type: "boolean",
					defaultValue: false
				},
				outputColumnTextKey: {
					type: "string",
					defaultValue: "mapping.target_attribute"
				}
			},
			aggregations: {

			},
			events: {
				"onMappingSourceSelected": {
					parameters: {
						"mapping": {
							type: "object"
						}
					}
				},
				"onSubmitMappings": {
					parameters: {
						"mappings": {
							type: "object"
						}
					}
				},
				"onCloseMappingEditor": {
					parameters: {
						"mappings": {
							type: "object"
						}
					}
				}
			}
		},

		renderer: DialogRenderer,

		constructor: function () {
			this._bIsInitial = true;
			this._bSetProperties = true;
			Dialog.prototype.constructor.apply(this, arguments);

			this.initEditorModel();
			this.loadConfigModel();

			if (arguments && arguments.length > 0) {
				var oParams;

				if (arguments.length === 1 && typeof (arguments[0]) === "object") {
					oParams = arguments[0];
				} else if (arguments.length === 2 && typeof (arguments[1]) === "object") {
					oParams = arguments[1];
				}

				if (oParams) {
					if (oParams.hasOwnProperty("type")) {
						this.setType(oParams.type);
					}
					if (oParams.hasOwnProperty("mode")) {
						this.setMode(oParams.mode);
					}
				}
			}
			this.initButtons();

			this.initDialog();
			this._bIsInitial = false;
		},

		onAfterRendering: function () {
			//SessionHandler.setSessionListener();
		},

		loadConfigModel: function () {
			var oModel = new sap.ui.model.odata.v2.ODataModel("/xsodata/fsdm2onesumxConfiguration.xsodata", {
				defaultUpdateMethod: "PUT",
				defaultBindingMode: sap.ui.model.BindingMode.TwoWay
			});
			this.setModel(oModel, "config");
			this.setConfigModel(oModel);
		},

		getEditorModel: function () {
			var oModel = this.getModel("editor");

			if (!oModel) {
				this.initEditorModel();
			}
			return this.getModel("editor");
		},

		initEditorModel: function () {
			this.setModel(
				new JSONModel({
					Operators_Simple: [{
						key: "None",
						text: ""
					}, {
						key: "EQ",
						text: "="
					}],
					Operators_Complex: [{
						key: "EQ",
						text: "="
					}, {
						key: "EQ",
						text: "="
					}, {
						key: "NOTIN",
						text: "NOT IN"
					}, {
						key: "IN",
						text: "IN"
					}, {
						key: "GT",
						text: ">"
					}, {
						key: "LT",
						text: "<"
					}, {
						key: "GE",
						text: ">="
					}, {
						key: "<=",
						text: "LE"
					}, {
						key: "LIKE",
						text: "LIKE"
					}],
					Operators_Logical: [{
						key: "None",
						text: ""
					}, {
						key: "AND",
						text: "AND"
					}, {
						key: "OR",
						text: "OR"
					}, {
						key: "WHEN",
						text: "WHEN"
					}, {
						key: "THEN",
						text: "THEN"
					}, {
						key: "ELSE",
						text: "ELSE"
					}],
					Properties: [],
					MappingData: this.getInitialMappingData(),
					StreamConfiguration: [],
					StreamAttributes: [],
					IsStaticMapping: false,
					IsEditable: false,
					IsCreateMode: false,
					DefinitionEditable: true,
					UseOutputValue: false
				}),
				"editor"
			);
		},

		getInitialMappingData: function () {
			return {
				Name: "",
				Description: "",
				BusinessValidFrom: new Date("1900-01-01"),
				BusinessValidTo: null,
				SourceStream: "",
				TargetView: "Contract",
				TargetViewAttribute: "dataGroup",
				Value: "",
				expressions: []
			};
		},

		showBusyIndicator: function () {
			if (!this._oBusyIndicator) {
				var oLayout = new sap.ui.layout.VerticalLayout({
					width: "100%",
					content: [
						new sap.m.FlexBox({
							alignItems: "Center",
							justifyContent: "Center",
							items: [
								new sap.m.BusyIndicator({
									//text: this.getText("mapping.editor_content_loading")
								})
							]
						})
					]
				});
				oLayout.addStyleClass("sapUiContentPadding");
				this._oBusyIndicator = oLayout;
			}

			if (this.getContent().length > 0) {
				this.removeAllContent();
			}

			this.setContentWidth("100px");
			this.setResizable(false);
			this.addContent(this._oBusyIndicator);
		},

		hideBusyIndicator: function () {
			if (this.getContent().length > 0) {
				this.removeAllContent();
			}
			this.addButtons();
		},

		initDialog: function () {
			if (this._bIsInitial || this._bNeedToRefreshUI) {
				this.loadUI();
			}
		},

		bindDatePicker: function () {
			this.getformFieldbyId(this.getCtrlId("dpBusinessValidFrom")).bindProperty("value", {
				path: "editor>/MappingData/BusinessValidFrom",
				type: 'sap.ui.model.type.Date',
				formatOptions: {
					pattern: Formatter.getDefaultDateFormat(),
					strictParsing: true,
					UTC: true
				},
				model: "tmp"
			});
			this.getformFieldbyId(this.getCtrlId("dpBusinessValidTo")).bindProperty("value", {
				path: "editor>/MappingData/BusinessValidTo",
				type: 'sap.ui.model.type.Date',
				formatOptions: {
					pattern: Formatter.getDefaultDateFormat(),
					strictParsing: true,
					UTC: true
				},
				model: "tmp"
			});
		},

		initButtons: function () {
			this._oBtnToggleEditMode = new sap.m.Button("btnToggleEditMode", {
				text: "{i18n>btn_txt_edit_mode}",
				tap: [this.handleToggleEditMode, this],
				enabled: true,
				visible: this.getMode() === "edit" || this.getMode() === "edit_limited",
				type: sap.m.ButtonType.Transparent
			});

			this._oBtnToggleEditMode.bindProperty("visible", "editor>/IsCreateMode", function (bValue) {
				return bValue ? false : true;
			});

			this._oBtnSubmit = new sap.m.Button("btnMappingEditorSubmitEntry", {
				text: "{i18n>submit}",
				tap: [this.handleSubmitEntry, this],
				enabled: false
			});
			this._oBtnCancel = new sap.m.Button("btnMappingEditorCancelEditEntry", {
				text: "{i18n>close}",
				tap: [this.handleCloseDialog, this]
			});
		},

		addButtons: function () {
			this.addButton(this._oBtnToggleEditMode);
			this.addButton(this._oBtnSubmit);
			this.addButton(this._oBtnCancel);
		},

		setRowActions: function (bShowActions) {
			var oTable = this.getTreeTable();

			if (oTable) {
				var oTemplate = oTable.getRowActionTemplate();

				if (!oTemplate) {
					oTemplate = new sap.ui.table.RowAction({
						items: [
							new sap.ui.table.RowActionItem({
								type: "Delete",
								text: "{i18n>tooltip_delete_entry}",
								press: [this.handleRemoveRule, this],
								visible: true
							})
						]
					});
				}

				oTable.setRowActionTemplate(oTemplate);
				oTable.setRowActionCount(bShowActions ? oTable.getRowActionTemplate().getItems().length : 0);
			}
		},

		handleSubmitEntry: function () {
			if (this.validateInput()) {
				if (this.getType() !== "simple") {
					if (this.countMappingCriteria() === 0) {
						MessageBox.information(this.getText("mapping.info_at_least_one_criterion"));
					} else {
						if (this.validateExpressions()) {
							this.fireEvent("onSubmitMappings", {
								source: this,
								mappings: this.getMappingData()
							});
						} else {
							MessageBox.information(this.getText("mapping.invalidMapping"));
						}
					}
				} else {
					this.fireEvent("onSubmitMappings", {
						source: this,
						mappings: this.getMappingData()
					});
				}
			} else {
				MessageBox.information(this.getText("mapping.invalidMapping"));
			}
		},

		validateExpressions: function () {
			var bResult = true,
				that = this;

			this.getTreeTable().getRows().forEach(function (oRow) {
				var oCtx = oRow.getBindingContext("editor"),
					oObj = oCtx ? oCtx.getObject() : null;

				if (oObj) {
					var aCells = oRow.getCells(),
						oProperty;

					if (oObj.hasOwnProperty("operatorLogical") && oObj.operatorLogical) {
						var oOpCell;

						if (that.getMode() === "edit" || (that.getMode() === "create" && that.getType() === "default")) {
							oOpCell = aCells.find(function (oCell) {
								return (oCell.getMetadata().getName() === "sap.m.Select" ?
									oCell.getBinding("items").getPath() : "") === "/StreamAttributes";
							});

							if (!oObj.property && oOpCell) {
								oOpCell.setValueState(sap.ui.core.ValueState.Error);
								oOpCell.setValueStateText(that.getText("mapping.property_must_not_be_empty"));

								bResult = false;
							} else if (oOpCell) {
								oOpCell.setValueState(sap.ui.core.ValueState.None);
								oProperty = oOpCell.getSelectedItem().getBindingContext("editor").getObject();
							}
						}
						oOpCell = aCells.find(function (oCell) {
							return (oCell.getMetadata().getName() === "sap.m.Input" ?
								oCell.getBinding("value").getPath() : "") === "value";
						});

						if ((!oObj.value && oOpCell) && !(oObj.operatorLogical === "THEN" && (that.getMode() === "edit_limited" || that.getMode() ===
								"default"))) {
							oOpCell.setValueState(sap.ui.core.ValueState.Error);
							oOpCell.setValueStateText(that.getText("mapping.value_must_not_be_empty"));

							bResult = false;
						} else if (oOpCell && oObj.value && oProperty && oObj.value.length > oProperty.Length) {
							oOpCell.setValueState(sap.ui.core.ValueState.Error);
							oOpCell.setValueStateText(that.getText("mapping.value_too_large"));
						} else {
							oOpCell.setValueState(sap.ui.core.ValueState.None);
						}
					}
				}
			});
			return bResult;
		},

		handleToggleEditMode: function () {
			this.setEditable(!this.getEditable());
		},

		setMode: function (sMode) {
			var oModel = this.getEditorModel(),
				sCurrentMode = this.getMode();

			if (oModel) {
				var oData = oModel.getData();

				oData.IsCreateMode = sMode === "create";

				if (sMode === "create") {
					this.setEditable(true);
				}
				oModel.setData(oData);
			}
			this.setProperty("mode", sMode);

			if (sMode !== sCurrentMode) {
				this._bNeedToRefreshUI = true;
			}
		},

		setEditable: function (bEditable) {
			var oModel = this.getEditorModel(),
				sBtnTxt = this.getText(bEditable ? "btn_txt_view_mode" : "btn_txt_edit_mode"),
				oTable = this.getTreeTable(),
				oFrmContainer = this.getFormContainer(),
				oToggleBtn = this.getToggleEditModeButton();

			if (oModel) {
				var oData = oModel.getData();

				oData.IsEditable = bEditable;
				oModel.setData(oData);
			}
			this.setProperty("editable", bEditable);

			if (oFrmContainer) {
				//oFrmContainer.setEditable(bEditable);
			}

			if (oTable) {
				//oTable.setEditable(bEditable);

				this.setRowActions(bEditable);
			}

			if (oToggleBtn) {
				oToggleBtn.setText(sBtnTxt);
			}
		},

		setErrorStateInputEmpty: function (oCtrl) {
			if (oCtrl) {
				var sId = oCtrl.getId();

				oCtrl.setValueState(sap.ui.core.ValueState.Error);

				if (sId === "inpMappingName") {
					oCtrl.setValueStateText(this.getText("mapping.name_must_not_be_empty"));
				}
				if (sId === "inpDescription") {
					oCtrl.setValueStateText(this.getText("mapping.description_must_not_be_empty"));
				}
				if (sId === "inpOutputValue") {
					oCtrl.setValueStateText(this.getText("mapping.value_must_not_be_empty"));
				}
			}
		},

		handleValidityChanged: function () {
			var oModel = this.getEditorModel(),
				oData = oModel.getData().MappingData,
				oCtrlValidFrom = this.getformFieldbyId("dpBusinessValidFrom"),
				oCtrlValidTo;

			//unfortunately the binding within the view does not work, so that this 2 lines are necessary to refresh the value ?!!
			//oModel.setProperty("/MappingData/BusinessValidFrom", this.getformFieldbyId("dpBusinessValidFrom").getDateValue());
			//oModel.setProperty("/MappingData/BusinessValidTo", this.getformFieldbyId("dpBusinessValidTo").getDateValue());

			if (!oData.BusinessValidFrom) {
				oCtrlValidFrom.setValueState(sap.ui.core.ValueState.Error);
				oCtrlValidFrom.setValueStateText(this.getText("mapping.businessValidFrom_must_not_be_empty"));
			} else {
				oCtrlValidFrom.setValueState(sap.ui.core.ValueState.None);

				if (oData.BusinessValidTo && oData.BusinessValidTo < oData.BusinessValidFrom) {
					oCtrlValidTo = this.getformFieldbyId("dpBusinessValidTo");

					oCtrlValidTo.setValueState(sap.ui.core.ValueState.Error);
					oCtrlValidTo.setValueStateText(this.getText("mapping.businessValidTo_lt_businessValidFrom"));
				} else {
					oCtrlValidTo = this.getformFieldbyId("dpBusinessValidTo");

					oCtrlValidTo.setValueState(sap.ui.core.ValueState.None);
				}
			}
		},

		handleCloseDialog: function () {
			if (this.hasChanges()) {
				var that = this;

				sap.m.MessageBox.show(
					this.getText("mapping.close_despite_unsaved_changes_question"), {
						icon: MessageBox.Icon.INFORMATION,
						title: this.getText("mapping.close_title"),
						actions: [
							sap.m.MessageBox.Action.YES,
							sap.m.MessageBox.Action.NO
						],
						emphasizedAction: sap.m.MessageBox.Action.NO,
						onClose: function (oAction) {
							if (oAction === "YES" && that.isOpen()) {
								that.close();
							}
						}
					}
				);
			} else {
				if (this.isOpen()) {
					this.close();
				}
			}
		},

		getText: function (sKey) {
			var sResult = sKey;

			if (sKey) {
				var oModel = this.getModel("i18n");

				if (oModel) {
					sResult = oModel.getResourceBundle().getText(sKey);
				}
			}
			return sResult;
		},

		showTargetViewField: function (bShow) {
			this.getformFieldbyId("selTargetView").setVisible(bShow);
			this.setProperty("showTargetViewField", bShow);
		},

		showTargetViewAttributeField: function (bShow) {
			this.getformFieldbyId("cboTargetAttribute").setVisible(bShow);
			this.setProperty("showTargetViewAttributeField", bShow);
		},

		setBusy: function (bBusy) {
			if (this.getFormContainer()) {
				this.getFormContainer().setBusy(bBusy);
			}

			if (this.getType() !== "simple") {
				this.getTreeTable().setBusy(bBusy);
				this.getTreeTable().getExtension()[0].setBlocked(bBusy);
			}
			this.setButtonsEnabled(!bBusy);
		},

		setButtonsEnabled: function (bEnabled) {
			var that = this;

			this.getButtons().forEach(function (oBtn) {
				if (!bEnabled) {
					oBtn.setEnabled(false);
				} else {
					oBtn.setEnabled((oBtn.getId() !== "btnMappingEditorSubmitEntry") || that.hasChanges());
				}
			});
		},

		byId: function (sId, oParent, bDoNotUsePraefix) {
			var oResult,
				aItems = [];

			sId = bDoNotUsePraefix ? sId : this.getCtrlId(sId);
			oParent = oParent ? oParent : this;

			if (oParent.getMetadata().hasAggregation("content")) {
				aItems = oParent.getContent();
			} else if (oParent.getMetadata().hasAggregation("items")) {
				aItems = oParent.getItems();
			}

			for (var i = 0; i < aItems.length; i++) {
				var oCtrl = aItems[i];

				if (oCtrl.getId() === sId) {
					oResult = oCtrl;

					break;
				}
			}

			if (!oResult) {
				for (i = 0; i < aItems.length; i++) {
					oResult = this.byId(sId, aItems[i], true);

					if (oResult) {
						break;
					}
				}
			}
			return oResult;
		},

		getformFieldbyId: function (sId) {
			var oResult;

			if (sId) {
				var oContainer = oResult = this.getFormContainer(),
					that = this;

				if (oContainer) {
					oResult = this.getFormContainer().getContent().find(function (oCtrl) {
						return oCtrl.getId() === that.getCtrlId(sId);
					});
				}
			}
			return oResult;
		},

		getTreeTable: function () {
			var oContainer = this.getTableContainer();

			return oContainer ? oContainer.getContent()[0] : null;
		},

		getFormContainer: function () {
			var oResult;

			try {
				oResult = this.byId("frmMappingDef");
			} catch (err) {
				oResult = null;
			}
			return oResult;
		},

		getTableContainer: function () {
			var oResult;

			try {
				oResult = this.getContent()[0].getContent()[0].getContent()[1].getContent()[0];
			} catch (err) {
				oResult = null;
			}
			return oResult;
		},

		getSubmitButton: function () {
			return this.getButtons().find(function (oBtn) {
				return oBtn.getId() === "btnMappingEditorSubmitEntry";
			});
		},

		getToggleEditModeButton: function () {
			return this.getButtons().find(function (oBtn) {
				return oBtn.getId() === "btnToggleEditMode";
			});
		},

		setLimitedTableEditability: function (bLimited) {
			var oContainer = this.getTableContainer();

			if (bLimited !== this.getLimitedTableEditablility() && oContainer.getContent().length > 0) {
				oContainer.removeAllContent();
			}

			if (oContainer.getContent().length === 0) {
				var sFragment = (bLimited ?
					"com.adweko.adapter.osx.ui.controls.mapping.ExpressionTableLimitedEditability" :
					"com.adweko.adapter.osx.ui.controls.mapping.ExpressionTable"
				);

				if (!(sFragment in this._aFragments)) {
					this._aFragments[sFragment] = new sap.ui.xmlfragment(sFragment, this);
					//this._aFragments[sFragment] = this.createTableFragment();
				}
				oContainer.addContent(this._aFragments[sFragment]);
			}
			this.setProperty("limitedTableEditablility", bLimited, true);
		},

		setConfigModel: function (oModel) {
			this.setModel(oModel, "config");

			var that = this;

			if (oModel) {
				oModel.read("/StreamConfiguration", {
					urlParameters: {
						"$expand": "Attributes"
					},
					success: function (oRetrievedResult) {
						var selStream = that.getformFieldbyId("selSourceStream"),
							aData = [{
								StreamName: "None",
								StreamTitle: ""
							}].concat(oRetrievedResult.results),
							oData = that.getEditorModel().getData();

						that.getEditorModel().setProperty("/StreamConfiguration", aData);

						if (that._bSetProperties) {
							that.setPropertyListByStream();
						}

						if (selStream) {
							selStream.setSelectedKey("None");
						}
						if (oData && oData.hasOwnProperty("PreEditData") && oData.PreEditData.hasOwnProperty("SourceStream")) {
							oData.MappingData.SourceStream = oData.PreEditData.SourceStream;
							that.getEditorModel().setData(oData);
						}
					},
					error: function (oError) {
						console.log(oError.message);
					}
				});
			}
		},

		handleAddRule: function () {
			var oTable = this.getTreeTable();

			if (oTable) {
				var oModel = this.getEditorModel(),
					iRowIdx = oTable.getSelectedIndex(),
					oEntry = this.createRuleEntry();

				if (!this.getAllowComplexNestedExpressions() || iRowIdx < 0) {
					var oData = oModel.getData();

					if (this.getformFieldbyId("selSourceStream").getSelectedKey() !== "None") {
						oEntry.id = oData.MappingData.expressions.length + 1;
						oEntry.operatorLogical = oData.MappingData.expressions.length === 0 ? "None" : "AND";
						oData.MappingData.expressions.push(oEntry);
						oModel.setData(oData);
					} else {
						MessageBox.information(this.getText("mapping.source_stream_required"));
					}
				} else {
					var oRule = oTable.getContextByIndex(oTable.getSelectedIndex()).getObject();

					oEntry.parent = oRule;
					oEntry.id = ((oEntry.parent) ? oEntry.parent.id + "." : "") + (oRule.expressions.length + 1);
					oEntry.operatorLogical = oRule.expressions.length === 0 ? "None" : "AND";
					oRule.expressions.push(oEntry);

					oModel.setData(oModel.getData());
				}
			}
			this.setFormFieldsEditability();
			this.setSubmitButtonEnabledIfChangesExist();
		},

		handleValueChanged: function (oEvent) {
			oEvent.getSource().setValueState(sap.ui.core.ValueState.None);
			this.setSubmitButtonEnabledIfChangesExist();
		},

		handleTokenUpdate: function (e) {
			if (e) {
				var oModel = this.getEditorModel(),
					oData = oModel.getData(),
					aKeyValues = oData.MappingData.KeyValues;

				if (e.getParameters().type === "removed") {
					e.getParameters().removedTokens.forEach(function (oToken) {
						aKeyValues = aKeyValues.filter(function (oKeyValue) {
							return oKeyValue.value !== oToken.getText();
						});
					});
				} else if (e.getParameters().type === "added") {
					e.getParameters().addedTokens.forEach(function (oToken) {
						aKeyValues.push({
							key: oData.PreEditData.KeyValues[0].key,
							value: oToken.getKey()
						});
					});
				}
				oData.MappingData.KeyValues = aKeyValues;
				oModel.setData(oData);
				e.getSource().setValueState(sap.ui.core.ValueState.None);
				this.setSubmitButtonEnabledIfChangesExist();
			}
		},

		multiInputValidator: function (args) {
			return new sap.m.Token({
				key: args.text,
				value: args.text
			});
		},

		hasChanges: function () {
			var oData = this.getEditorModel().getData(),
				bResult = false;

			if (oData && oData.MappingData && oData.PreEditData) {
				bResult = JSON.stringify(oData.MappingData) !== JSON.stringify(oData.PreEditData);
			}
			return bResult;
		},

		getLevel: function (oRule) {
			var iResult = 0;

			if (oRule) {
				while (oRule.parent !== null) {
					oRule = oRule.parent;

					iResult++;
				}
			}
			return iResult;
		},

		handleRemoveRule: function (oEvent) {
			var oTable = this.getTreeTable(),
				iRowIdx = oEvent.getSource().getParent().getParent().getIndex();

			if (iRowIdx >= 0) {
				var oModel = this.getEditorModel(),
					sPath = oTable.getContextByIndex(iRowIdx).getPath();

				if (sPath) {
					var iIdxInArray = parseInt(sPath.substring(sPath.lastIndexOf("/") + 1));

					oModel.getData().MappingData.expressions.splice(iIdxInArray, 1);

					if (iIdxInArray + 1 < oModel.getData().MappingData.expressions.length) {
						for (var i = iIdxInArray; i < oModel.getData().MappingData.expressions.length; i++) {
							var oItem = oModel.getData().MappingData.expressions[i];
							oItem.id = oItem.id - 1;
							oItem.operatorLogical = i === 0 ? "None" : "AND";
						}
					}
					oModel.setData(oModel.getData());
				}
			}
			this.setFormFieldsEditability();
		},

		setFormFieldsEditability: function () {
			var iRules = this.countMappingCriteria();

			if (this.getformFieldbyId("selSourceStream")) {
				this.getformFieldbyId("selSourceStream").setEnabled(iRules === 0);
			}
			if (this.getformFieldbyId("selTargetView")) {
				this.getformFieldbyId("selTargetView").setEnabled(iRules === 0);
			}
		},

		countMappingCriteria: function () {
			var aExpressions = this.getEditorModel().getData().MappingData.expressions;

			return aExpressions && Array.isArray(aExpressions) ? aExpressions.length : 0;
		},

		handleSelectionChanged: function (oEvent) {
			var oCtrl = oEvent.getSource(),
				sId = oCtrl.getId();

			if (sId === this.getCtrlId("selSourceStream")) {
				this.setPropertyListByStream();
			} else {

				if (oCtrl && oCtrl.getSelectedItem() && oCtrl.getSelectedItem().getBindingContext("editor") && oCtrl.getSelectedItem().getBindingContext(
						"editor").getPath().includes("StreamAttribute")) {
					var oMappingAttr = oCtrl.getSelectedItem().getBindingContext("editor").getObject();

					if (oMappingAttr.hasOwnProperty("Length") && oMappingAttr.Length > 0) {
						this.setExpressionValueCellMaxLength(oCtrl.getParent(), oMappingAttr.Length);
					}
				}
			}
			oCtrl.setValueState(sap.ui.core.ValueState.None);
			this.setSubmitButtonEnabledIfChangesExist();
		},

		setExpressionValueCellMaxLength: function (oExpressionTableRow, length) {
			if (oExpressionTableRow) {
				var oValueCell = oExpressionTableRow.getCells().find(function (oCell) {
					return oCell.getBindingInfo("value");
				});
				if (oValueCell) {
					oValueCell.setMaxLength(parseInt(length));
				}
			}
		},

		handleInputChanged: function (oEvent) {
			oEvent.getSource().setValueState(sap.ui.core.ValueState.None);
			this.setSubmitButtonEnabledIfChangesExist();
		},

		setPropertyListByStream: function () {
			var oSource = this.getformFieldbyId("selSourceStream"),
				sPath,
				oStream,
				oModel = this.getEditorModel(),
				oData = oModel.getData(),
				sStream;

			if (oSource) {
				sStream = oSource.getSelectedKey();

				if (sStream !== "None" && sStream !== "" && oSource.getSelectedItem() && oSource.getSelectedItem().getBindingContext("editor")) {
					sPath = oSource.getSelectedItem().getBindingContext("editor").getPath();
				}

				if (sPath) {
					var iIdxInArray = parseInt(sPath.substring(sPath.lastIndexOf("/") + 1));

					oStream = oData.StreamConfiguration[iIdxInArray];
				}
			} else if (this.getformFieldbyId("txtSourceStream")) {
				oSource = this.getformFieldbyId("txtSourceStream");

				if (oSource.getText() !== "") {
					oStream = oModel.getData().StreamConfiguration.find(function (oStreamConf) {
						return oStreamConf.StreamName === oSource.getText();
					});
					sStream = oSource.getText();
				}
			}
			if (oStream && oStream.hasOwnProperty("Attributes") && Array.isArray(oStream.Attributes.results)) {
				oData.StreamAttributes = oStream.Attributes.results;

				oData.MappingData.SourceStream = sStream;
				oModel.setData(oData);
			}
		},

		setPropertyList: function (aProperties) {
			this._bSetProperties = false;

			if (aProperties && Array.isArray(aProperties)) {
				var oModel = this.getEditorModel();

				if (oModel) {
					var oData = oModel.getData();

					oData.StreamAttributes = aProperties;
					oModel.setData(oData);
				}
			}
		},

		getPropertyListByExpressionList: function (aExpressions) {
			var aResult = [];

			if (aExpressions && Array.isArray(aExpressions)) {
				var that = this;

				aExpressions.forEach(function (oExpr) {
					that.getPropertyListByExpression(oExpr).forEach(function (oProperty) {
						if (aResult.find(function (oListProperty) {
								return oListProperty.AttributeName === oProperty.AttributeName;
							}) === undefined) {
							aResult.push(oProperty);
						}
					});
				});
			}
			return aResult;
		},

		getPropertyListByExpression: function (oExpression) {
			var aResult = [];

			if (oExpression) {
				if (oExpression.hasOwnProperty("property")) {
					aResult.push({
						AttributeName: oExpression.property
					});
				}

				if (oExpression.hasOwnProperty("expressions") && Array.isArray(oExpression.expressions)) {
					var that = this;

					oExpression.expressions.forEach(function (oExpr) {
						that.getPropertyListByExpression(oExpr).forEach(function (sProperty) {
							if (aResult.find(function (oProperty) {
									return oProperty.AttributeName === sProperty;
								}) === undefined) {
								aResult.push({
									AttributeName: sProperty
								});
							}
						});
					});
				}
			}
			return aResult;
		},

		getKeyValuePairsByStaticStreamBasedMapping: function (oExpression) {
			var aResult = [],
				that = this;

			if (oExpression) {
				if (Array.isArray(oExpression)) {
					oExpression.forEach(function (oExpr) {
						that.getKeyValuePairsByStaticStreamBasedMapping(oExpr).forEach(function (oKeyValue) {
							aResult.push(oKeyValue);
						});
					});
				} else {
					if (oExpression.hasOwnProperty("key")) {
						aResult.push({
							key: oExpression.key,
							value: oExpression.hasOwnProperty("value") ? oExpression.value : null
						});
					}

					if (oExpression.hasOwnProperty("expressions") && Array.isArray(oExpression.expressions)) {
						this.getKeyValuePairsByStaticStreamBasedMapping(oExpression.expressions).forEach(function (oKeyValue) {
							aResult.push(oKeyValue);
						});
					}
				}
			}
			return aResult;
		},

		createRuleEntry: function () {
			return {
				id: "0",
				parent: null,
				operatorLogical: "None",
				property: "",
				operator: "EQ",
				value: "",
				expressions: []
			};
		},

		validateInput: function () {
			var bResult = true,
				oData = this.getMappingData();

			if (oData) {
				var oCtrl;

				if (this.getMode() === "create") {
					oCtrl = this.getformFieldbyId("inpMappingName");

					if (!oData.Name) {
						oCtrl.setValueState(sap.ui.core.ValueState.Error);
						oCtrl.setValueStateText(this.getText("mapping.name_must_not_be_empty"));
						bResult = false;
					} else {
						oCtrl.setValueState(sap.ui.core.ValueState.None);
					}

					oCtrl = this.getformFieldbyId("dpBusinessValidFrom");

					if (!oData.BusinessValidFrom) {
						oCtrl.setValueState(sap.ui.core.ValueState.Error);
						oCtrl.setValueStateText(this.getText("mapping.businessValidFrom_must_not_be_empty"));
						bResult = false;
					} else {
						var oCtrlValidTo = this.getformFieldbyId("dpBusinessValidTo");

						oCtrl.setValueState(sap.ui.core.ValueState.None);

						if (oData.BusinessValidTo && oData.BusinessValidTo < oData.BusinessValidFrom) {
							oCtrlValidTo.setValueState(sap.ui.core.ValueState.Error);
							oCtrlValidTo.setValueStateText(this.getText("mapping.businessValidTo_lt_businessValidFrom"));
						} else {
							oCtrlValidTo.setValueState(sap.ui.core.ValueState.None);
						}
					}
				}

				oCtrl = this.getformFieldbyId("taMappingDescription");

				if (oCtrl) {
					if (!oData.Description) {
						oCtrl.setValueState(sap.ui.core.ValueState.Error);
						oCtrl.setValueStateText(this.getText("mapping.description_must_not_be_empty"));
						bResult = false;
					} else {
						oCtrl.setValueState(sap.ui.core.ValueState.None);
					}
				}

				oCtrl = this.getformFieldbyId("selSourceStream");

				if (oCtrl) {
					if (!oData.SourceStream) {
						oCtrl.setValueState(sap.ui.core.ValueState.Error);
						oCtrl.setValueStateText(this.getText("mapping.SourceStream_must_not_be_empty"));
						bResult = false;
					} else {
						oCtrl.setValueState(sap.ui.core.ValueState.None);
					}
				}
				oCtrl = this.getformFieldbyId("selTargetView");

				if (oCtrl) {
					if (!oData.TargetView) {
						oCtrl.setValueState(sap.ui.core.ValueState.Error);
						oCtrl.setValueStateText(this.getText("mapping.targetView_must_not_be_empty"));
						bResult = false;
					} else {
						oCtrl.setValueState(sap.ui.core.ValueState.None);
					}
				}

				oCtrl = this.getformFieldbyId("cboTargetAttribute");

				if (oCtrl) {
					if (!oData.TargetViewAttribute) {
						oCtrl.setValueState(sap.ui.core.ValueState.Error);
						oCtrl.setValueStateText(this.getText("mapping.targetView_must_not_be_empty"));
						bResult = false;
					} else {
						oCtrl.setValueState(sap.ui.core.ValueState.None);
					}
				}

				oCtrl = this.getformFieldbyId("inpOutputValue");

				if (oCtrl) {
					if (oCtrl.getVisible() && !oData.Value) {
						oCtrl.setValueState(sap.ui.core.ValueState.Error);
						oCtrl.setValueStateText(this.getText("mapping.value_must_not_be_empty"));
						bResult = false;
					} else {
						oCtrl.setValueState(sap.ui.core.ValueState.None);
					}
				}

				if (this.getType() === "simple" && oData.KeyValues.length === 0) {
					oCtrl = this.getformFieldbyId("minpValue");

					if (oCtrl) {
						oCtrl.setValueState(sap.ui.core.ValueState.Error);
						oCtrl.setValueStateText(this.getText("mapping.value_must_not_be_empty"));
						bResult = false;
					} else {
						oCtrl.setValueState(sap.ui.core.ValueState.None);
					}
				}
			}
			return bResult;
		},

		getMappingData: function () {
			return this.getEditorModel().getData().MappingData;
		},

		setMappingData: function (oMappingData, bSetProperties) {
			var oData = this.getEditorModel().getData(),
				selStream = this.getformFieldbyId("selSourceStream");

			if (bSetProperties === undefined) {
				bSetProperties = true;
			}

			oData.PreEditData = JSON.parse(JSON.stringify(oMappingData));
			oData.MappingData = oMappingData;
			this.getEditorModel().setData(oData);
			this._bSetProperties = bSetProperties;

			if (selStream) {
				selStream.setSelectedKey(oMappingData.SourceStream);
			}

			if (bSetProperties) {
				this.setPropertyListByStream();
			}
		},

		setFormFieldsEditable: function (bEditable) {
			if (this.getMode() === "create") {
				this.getformFieldbyId("inpMappingName").setEnabled(bEditable);
				this.getformFieldbyId("dpBusinessValidFrom").setEnabled(bEditable);
				this.getformFieldbyId("dpBusinessValidTo").setEnabled(bEditable);
			}
			this.setFormFieldsEditability();
		},

		resetValueStates: function () {
			if (this.getFormContainer()) {
				this.getFormContainer().getContent().forEach(function (oCtrl) {
					if (oCtrl.getMetadata().getAllPublicMethods().includes("setValueState")) {
						oCtrl.setValueState(sap.ui.core.ValueState.None);
					}
				});
				this.setFormFieldsEditability();
				this.setButtonsEnabled(true);
			}
		},

		setInitialMappingData: function () {
			this.setMappingData(this.getInitialMappingData());
		},

		setType: function (sType) {
			var sCurrentType = this.getType(),
				oModel = this.getEditorModel();

			if (oModel) {
				var oData = oModel.getData();

				//oData.IsStaticMapping = sType === "static";
				oModel.setData(oData);
			}
			this.setProperty("type", sType);

			if (sType !== sCurrentType) {
				this._bNeedToRefreshUI = true;
			}
		},

		loadUI: function () {
			if (!this._aFragments) {
				this._aFragments = [];
			}

			var sFragment = this.getType() === "simple" ?
				this.getFormContainerId() : "LayoutStructure";

			if (sFragment) {
				if (!(sFragment in this._aFragments)) {
					var oFragment;

					if (this.getType() !== "simple") {
						oFragment = this.createLayoutStructure();
						this._aFragments[sFragment] = oFragment;

						sFragment = this.getFormContainerId();

						if (!(sFragment in this._aFragments)) {
							this._aFragments[sFragment] = this.getMappingDefinitionFragment.call(this);
						}
						this.byId("blcDef", oFragment, true).addContent(this._aFragments[sFragment]);
					} else {
						this._aFragments[sFragment] = this.getMappingDefinitionFragment.call(this);
						oFragment = this._aFragments[sFragment];
					}

					this.removeAllContent();
					this.addContent(oFragment);
					this.prepareUI();

					this._bIsInitial = false;
				} else {
					try {
						oFragment = this._aFragments[sFragment];

						if (this.getType() === "simple") {
							if (oFragment.getParent()) {
								oFragment.getParent().removeContent(oFragment);
							}
						} else {
							var oCtrl = this.byId("blcDef", oFragment, true);

							oCtrl.removeAllContent();

							if (!(this.getFormContainerId() in this._aFragments)) {
								this._aFragments[this.getFormContainerId()] = this.getMappingDefinitionFragment.call(this);
							}

							oCtrl.addContent(
								this._aFragments[this.getFormContainerId()]
							);
						}

						if (this.getContent().length > 0) {
							this.removeAllContent();
						}
					} catch (err) {

					}

					this.addContent(oFragment);
					this.prepareUI();

					this._bIsInitial = false;
				}
			}
		},

		prepareUI: function () {
			var sType = this.getType(),
				sMode = this.getMode(),
				oSelStream;

			if (sType !== "simple") {
				if (sMode === "edit_limited") {
					this.setLimitedTableEditability(true);
				} else {
					this.setLimitedTableEditability(false);
					this.setRowActions(this.getEditable());
				}
				this.setContentWidth("1000px");
				this.setResizable(true);
			} else {
				this.setContentWidth("1em");
				this.setResizable(false);
			}
			this.addButtons();

			oSelStream = this.getformFieldbyId("selSourceStream");

			if (oSelStream) {
				oSelStream.setSelectedKey(this.getEditorModel().getData().MappingData.SourceStream);
			}

			if (this.byId("minpValue")) {
				this.byId("minpValue").addValidator(this.multiInputValidator);
			}
		},

		open: function () {
			//this.getInitialMappingData();

			if (this._bNeedToRefreshUI) {
				this.loadUI();
			}

			this.setEditable(this.getMode() === "create" ? true : false);

			if (this._oBtnToggleEditMode) {
				this._oBtnToggleEditMode.setVisible(this.getMode() === "edit" || this.getMode() === "edit_limited");
			}
			this.resetValueStates();

			Dialog.prototype.open.apply(this, arguments);
		},

		getExpressionValueVisibility: function (sProperty) {
			return sProperty;
		},

		getEditorFieldVisibility: function (type, valueByColReference) {
			return type !== 'CASE' && !valueByColReference;
		},

		setSubmitButtonEnabledIfChangesExist: function () {
			this.getSubmitButton().setEnabled(this.hasChanges());
		},

		getFormContainerId: function () {
			return this.getCtrlId("frmMappingDef");
		},

		getCtrlIdPraefix: function () {
			return this.getType() + "_" + this.getMode() + "_";
		},

		getCtrlId: function (sId) {
			return this.getCtrlIdPraefix() + "_" + sId;
		},

		getMappingDefinitionFragment: function () {
			var oResult,
				that = this,
				bPrimaryKeysEditable = (that.getMode() === "create" ? true : false),
				bDefinitionEditable = ((that.getMode() === "edit" || that.getMode() === "create") ? true : false),
				sId = this.getFormContainerId();

			try {
				oResult = new sap.ui.layout.form.SimpleForm(sId, {
					labelSpanXL: 3,
					labelSpanL: 3,
					labelSpanM: 3,
					labelSpanS: 12,
					adjustLabelSpan: false,
					emptySpanL: 4,
					emptySpanM: 4,
					emptySpanS: 0,
					columnsXL: 1,
					singleContainerFullSize: false,
					columnsL: 1,
					editable: true,
					layout: "ResponsiveGridLayout",
					content: []
				});

				oResult.addContent(
					//mapping name
					new sap.m.Label({
						text: this.getText("{i18n>mapping.name}"),
						required: bPrimaryKeysEditable ? true : false
					})
				);
				oResult.addContent(
					(bPrimaryKeysEditable ?
						new sap.m.Input(this.getCtrlId('inpMappingName'), {
							value: "{editor>/MappingData/Name}",
							required: true,
							liveChange: [this.handleInputChanged, this],
							maxLength: 128
						}) : new sap.m.Text(this.getCtrlId('txtMappingName'), {
							text: "{editor>/MappingData/Name}"
						})
					)
				);
				oResult.addContent(
					//vadlid from 
					new sap.m.Label({
						text: this.getText("{i18n>BusinessValidFrom}"),
						required: bPrimaryKeysEditable ? true : false
					})
				);
				oResult.addContent(
					(bPrimaryKeysEditable ?
						new sap.m.DatePicker(this.getCtrlId('dpBusinessValidFrom'), {
							required: true,
							change: [this.handleValidityChanged, this],
							formatOptions: {
								strictParsing: true,
								UTC: true
							}
						}) : new sap.m.Text(this.getCtrlId('txtBusinessValidFrom'), {
							text: {
								path: "editor>/MappingData/BusinessValidFrom",
								formatter: function (oValue) {
									return that.dateToString(oValue);
								}
							}
						})
					)
				);
				if (bPrimaryKeysEditable) {
					oResult.getContent()[oResult.getContent().length - 1].bindProperty("value", {
						path: "editor>/MappingData/BusinessValidFrom",
						type: 'sap.ui.model.type.Date',
						formatOptions: {
							pattern: Formatter.getDefaultDateFormat(),
							strictParsing: true,
							UTC: true
						},
						model: "tmp"
					});
				}
				oResult.addContent(
					//vadlid to 
					new sap.m.Label({
						text: this.getText("{i18n>BusinessValidTo}"),
						required: bPrimaryKeysEditable ? true : false
					})
				);
				oResult.addContent(
					(bPrimaryKeysEditable ?
						new sap.m.DatePicker(this.getCtrlId('dpBusinessValidTo'), {
							required: true,
							change: [this.handleValidityChanged, this],
							formatOptions: {
								strictParsing: true,
								UTC: true
							}
						}) : new sap.m.Text(this.getCtrlId('txtBusinessValidTo'), {
							text: {
								path: "editor>/MappingData/BusinessValidTo",
								formatter: function (oValue) {
									return that.dateToString(oValue);
								}
							}
						})
					)
				);
				if (bPrimaryKeysEditable) {
					oResult.getContent()[oResult.getContent().length - 1].bindProperty("value", {
						path: "editor>/MappingData/BusinessValidTo",
						type: 'sap.ui.model.type.Date',
						formatOptions: {
							pattern: Formatter.getDefaultDateFormat(),
							strictParsing: true,
							UTC: true
						},
						model: "tmp"
					});
				}

				oResult.addContent(
					//description
					new sap.m.Label({
						text: this.getText("{i18n>mapping.description}"),
						required: false
					})
				);
				oResult.addContent(
					(bDefinitionEditable ?
						new sap.m.TextArea(this.getCtrlId('taMappingDescription'), {
							value: "{editor>/MappingData/Description}",
							required: false,
							enabled: "{= ${editor>/IsEditable}}",
							liveChange: [this.handleInputChanged, this]
						}) : new sap.m.Text(this.getCtrlId('txtMappingDescription'), {
							text: "{editor>/MappingData/Description}"
						})
					)
				);
				oResult.addContent(
					//stream
					new sap.m.Label({
						text: this.getText("{i18n>mapping.source_contract_category}"),
						required: bDefinitionEditable ? true : false
					})
				);

				oResult.addContent((bDefinitionEditable ?
					new sap.m.Select(this.getCtrlId('selSourceStream'), {
						change: [this.handleSelectionChanged, this],
						selectedKey: "{editor>/MappingData/SourceStream}",
						required: bDefinitionEditable ? true : false,
						enabled: "{= ${editor>/IsEditable}}"
					}) : new sap.m.Text(this.getCtrlId('txtSourceStream'), {
						text: "{editor>/MappingData/SourceStream}"
					})
				));
				if (bDefinitionEditable) {
					oResult.getContent()[oResult.getContent().length - 1].bindAggregation("items", {
						path: "editor>/StreamConfiguration",
						template: new sap.ui.core.Item({
							key: "{editor>StreamName}",
							text: "{editor>StreamTitle}"
						})
					});
				}
				oResult.addContent(new sap.m.Label({
					text: this.getText("{i18n>mapping.target_view}"),
					required: bDefinitionEditable ? true : false,
					visible: false
				}));
				oResult.addContent((bDefinitionEditable ?
					new sap.m.Select(this.getCtrlId('selTargetView'), {
						change: [this.handleSelectionChanged, this],
						visible: false,
						enabled: "{= ${editor>/IsEditable}}"
					}) : new sap.m.Text(this.getCtrlId('txtTargetView'), {
						text: "TARGET",
						visible: false
					})
				));
				oResult.addContent(new sap.m.Label({
					text: this.getText("{i18n>mapping.target_attribute}"),
					required: false
				}));
				oResult.addContent(
					// (bDefinitionEditable ?
					// new sap.m.Select(this.getCtrlId('cboTargetAttribute'), {
					// 	change: [this.handleSelectionChanged, this]
					// }) : 
					new sap.m.Text(this.getCtrlId('txtTargetAttribute'), {
						text: "{editor>/MappingData/TargetViewAttribute}"
					})
					// )
				);

				//output column
				if (this.getType() === "simple" || (this.getType() === "default" && this.getMode() !== "edit_limited")) {
					oResult.addContent(
						new sap.m.Label({
							text: this.getText("{i18n>mapping.value}"),
							required: this.getMode() !== "view"
						})
					);
				}
				if (that.getType() === "simple") {
					oResult.addContent(
						new sap.m.MultiInput(this.getCtrlId('minpValue'), {
							required: true,
							tokenUpdate: [this.handleTokenUpdate, this],
							maxLength: 256,
							showValueHelp: false,
							editable: "{= ${editor>/IsEditable}}"
						})
					);
					oResult.getContent()[oResult.getContent().length - 1].bindAggregation("tokens", {
						path: "editor>/MappingData/KeyValues",
						template: new sap.m.Token({
							key: "{editor>key}",
							text: "{editor>value}"
						})
					});
				} else if (this.getType() === "default" && this.getMode() !== "edit_limited") {
					oResult.addContent(
						(this.getMode() !== "view" ?
							new sap.m.Input(this.getCtrlId('inpOutputValue'), {
								value: "{editor>/MappingData/Value}",
								required: true,
								liveChange: [this.handleInputChanged, this],
								maxLength: 128,
								enabled: "{= ${editor>/IsEditable}}"
							}) : new sap.m.Text(this.getCtrlId('txtOutputValue'), {
								text: "{editor>/MappingData/Value}"
							})
						)
					);
				}
			} catch (err) {
				console.log(err.message);
			}
			return oResult;
		},

		createLayoutStructure: function () {
			var oResult = new sap.ui.layout.VerticalLayout({
				width: "100%",
				content: [
					new sap.ui.layout.BlockLayout({
						content: [
							new sap.ui.layout.BlockLayoutRow({
								content: [
									new sap.ui.layout.BlockLayoutCell('blcDef', {
										width: 100,
										title: this.getText("{i18n>mapping.definition}")
									})
								]
							}),
							new sap.ui.layout.BlockLayoutRow({
								content: [
									new sap.ui.layout.BlockLayoutCell('blcTable', {
										width: 100,
										title: this.getText("{i18n>mapping.logic}")
									})
								]
							})
						]
					})
				]
			});
			return oResult;
		},

		setKeyNotUniqueState: function () {
			var oCtrl,
				sId;

			for (var i = 0; i < 3; i++) {
				switch (i) {
				case 0:
					sId = "inpMappingName";
					break;
				case 1:
					sId = "dpBusinessValidFrom";
					break;
				case 2:
					sId = "dpBusinessValidTo";
					break;
				default:
				}

				oCtrl = this.getformFieldbyId(sId);

				if (oCtrl) {
					oCtrl.setValueState(sap.ui.core.ValueState.Error);
					oCtrl.setValueStateText(this.getText("mapping.primarey_key_already_exist"));
				}
			}
		},

		dateToString: function (oDate) {
			var sResult = "";

			if (oDate) {
				if (oDate.getFullYear().toString() === "9999" && (oDate.getMonth() + 1).toString() && oDate.getDate().toString() === "31") {
					sResult = "unlimited";
				} else {
					var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
							pattern: this.getDefaultDateFormat()
						}),
						dateFormatted = dateFormat.format(oDate);

					sResult = dateFormatted.toString();
				}
			}
			return sResult;
		},

		getDefaultDateFormat: function () {
			return "MMM d, y";
		}
	});
});