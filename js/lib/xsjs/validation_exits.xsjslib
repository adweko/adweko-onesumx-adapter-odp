/*eslint no-console: 0, no-unused-vars: 0, dot-notation: 0, no-use-before-define: 0, no-redeclare: 0*/
"use strict";

/**
@param {connection} Connection - The SQL connection used in the OData request
@param {beforeTableName} String - The name of a temporary table with the single entry before the operation (UPDATE and DELETE events only)
@param {afterTableName} String -The name of a temporary table with the single entry after the operation (CREATE and UPDATE events only)
*/
$.import('utils', 'session');
$.import('utils', 'mapping');
$.import('utils', 'error');

var sEntFunct = "entered function";
var sSelectFrom = "select * from \"";

function beforeCreate(param) {
	$.trace.debug();

	try {
		if (!$.utils.session.checkSessionUserAllowedToCreate()) {
			$.utils.error.throwForbiddenError();
		} else {
			checkInputdataValid(param);
		}
	} catch (oErr) {
		$.utils.error.handleError(oErr);
	}
}

function beforeRead(param) {
	$.trace.debug(sEntFunct);

	try {
		if (!$.utils.session.checkSessionUserAllowedToDisplay()) {
			$.utils.error.throwForbiddenError();
		}
	} catch (oErr) {
		$.utils.error.handleError(oErr);
	}
}

function beforeUpdate(param) {
	$.trace.debug(sEntFunct);

	try {
		if (!$.utils.session.checkSessionUserAllowedToEdit()) {
			$.utils.error.throwForbiddenError();
		} else {
			checkInputdataValid(param);
		}
	} catch (oErr) {
		$.utils.error.handleError(oErr);
	}
}

function beforeDelete(param) {
	$.trace.debug(sEntFunct);

	try {
		if (!$.utils.session.checkSessionUserAllowedToDelete()) {
			$.utils.error.throwForbiddenError();
		}
	} catch (oErr) {
		$.utils.error.handleError(oErr);
	}
}

function checkInputdataValid(param) {
	$.trace.debug(sEntFunct);
	var pStmt,
		rs,
		aColumns = $.utils.mapping.selectMappingTableColumnInformation(param);

	pStmt = param.connection.prepareStatement(sSelectFrom.concat(param.afterTableName, "\""));
	rs = pStmt.executeQuery();

	pStmt.close();

	while (rs.next()) {
		for (var i = 0; i < rs._columnInfo.length; i++) {
			var oColumn = rs._columnInfo[i],
				oValue = $.utils.session.getValue(rs, i);

			if (oValue) {
				if (aColumns[oColumn.columnName].LENGTH < oValue.length) {
					$.utils.error.throwValueTooLargeError();
				}
			} else if (!aColumns[oColumn.columnName].IS_NULLABLE && (oColumn.columnName !== 'SystemValidFrom' && oColumn.columnName !==
					'SystemValidTo')) {
//					'SystemValidTo' && oColumn.columnName !== "LastChangeTimestamp" && oColumn.columnName !== "LastChangeApplicationUserName")) {
				$.utils.error.throwNullNotAllowedError();
			}
		} 
	}
}