try {
	var conn = $.hdb.getConnection(),
		sMappingId = $.request.parameters.get("Id"),
		bDeleteCustomMappings = $.request.parameters.get("DelCustMappings"),
		oProcedure = conn.loadProcedure("com.adweko.adapter.osx.configuration::Load_CodeListMapping"),
		oSession;

	$.response.contentType = "text/plain";
	$.import('utils', 'session');

	oSession = $.utils.session;

	if (oSession.checkSessionUserHasFullWritingAccess()) {
		if (sMappingId.length > 256 || oSession.containsSpecialChars(sMappingId)) {
			oSession.setResponseInvalidParameter ();
		} else {
			bDeleteCustomMappings = (bDeleteCustomMappings === null || bDeleteCustomMappings === undefined) ?
				false : oSession.parseBool(bDeleteCustomMappings);

			oProcedure(sMappingId, bDeleteCustomMappings, true);

			conn.commit();
		}
	} else {
		oSession.setResponseUnauthorized();
	}
	conn.close();
} catch (sqlException) {
	//console.log(sqlException);

	//$.response.setBody("error: " + sqlException.message);
	$.response.setBody("Error: A database error has occured");

	$.response.returnCode = 200;
	$.response.status = 500;
	conn.close();
}