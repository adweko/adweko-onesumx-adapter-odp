/*eslint no-console: 0, no-unused-vars: 0, dot-notation: 0, no-use-before-define: 0, no-redeclare: 0*/
"use strict";

/**
@param {connection} Connection - The SQL connection used in the OData request
@param {beforeTableName} String - The name of a temporary table with the single entry before the operation (UPDATE and DELETE events only)
@param {afterTableName} String -The name of a temporary table with the single entry after the operation (CREATE and UPDATE events only)
*/
$.import('utils', 'session');
$.import('utils', 'mapping');
$.import('utils', 'error');

var sEntFunct = "entered function";
var sSelectFrom = "select * from \"";

function create(param) {
	$.trace.debug(sEntFunct);
	var pStmt,
		oConn;
	try {
		if ($.utils.session.checkSessionUserAllowedToCreate()) {
			var sTableName = $.utils.mapping.getMappingTableName(param);

			if (sTableName) {
				if (isUpdateForPeriod(param)) {
					update(param, sTableName);
				} else {
					pStmt = param.connection.prepareStatement(sSelectFrom.concat(param.afterTableName, "\""));
					oConn = param.connection;
					var sInsertStmt = "insert into \"".concat(sTableName, "\" values("),
						aColsToIgnore = ["SystemValidFrom", "SystemValidTo"],
						rs = pStmt.executeQuery();

					while (rs.next()) {
						var iCount = rs._columnInfo.length - aColsToIgnore.length; // SystemValidFrom + SystemValidTo substracted...

						for (var i = 0; i < iCount; i++) {
							sInsertStmt += "?" + (i + 1 < iCount ? "," : "");
						}
						sInsertStmt += ")";
						pStmt = oConn.prepareStatement(sInsertStmt);
						setParams(rs, pStmt, aColsToIgnore);
						pStmt.executeQuery();
					}
				}
			}
		} else {
			//not authorized to create entries!!!
			$.utils.error.throwForbiddenError();
		}
	} catch (oErr) {
		if (pStmt) {
			pStmt.close();
		}
		$.utils.error.handleError(oErr);
	}
}

function isUpdateForPeriod(param) {
	$.trace.debug(sEntFunct);
	var pStmt,
		bResult;
	try {
		var sTableName = $.utils.mapping.getMappingTableName(param);

		if (sTableName) {
			var aKeyNames = $.utils.mapping.listMappingTablePrimaryKeys(param);
			pStmt = param.connection.prepareStatement(sSelectFrom.concat(param.afterTableName, "\""));
			bResult = false;
			var rs = pStmt.executeQuery(),
				sSelect = "select count(*) from \"".concat(sTableName, "\""),
				sValidFrom = "BusinessValidFrom",
				sValidTo = "BusinessValidTo";

			while (rs.next()) {
				var aKeyPositions = [],
					selRs,
					i,
					whereClause = "";
					
				// building where clause
				for (i = 0; i < rs._columnInfo.length; i++) {
					var sColName = rs._columnInfo[i].columnName;
					
					if (aKeyNames.includes(sColName)) {
						if (sColName === sValidFrom) {
							whereClause += (whereClause === "" ? " where " : " and ").concat("\"", sValidFrom, "\" <= ?");
							aKeyPositions[sColName] = i;
						} else if (sColName === sValidTo) {
							whereClause += (whereClause === "" ? " where " : " and ").concat("\"", sValidTo, "\" >= ?");
							aKeyPositions[sColName] = i;
						} else {
							whereClause += (whereClause === "" ? " where " : " and ").concat("\"", sColName, "\" = ?");
							aKeyPositions[sColName] = i;
							
						}
					}
				}
				sSelect += whereClause;
				
				pStmt = param.connection.prepareStatement(sSelect);
				
				// substituting parameters in 'where' clause
				for (i = 0; i < aKeyNames.length; i++) {
					if (aKeyNames[i] !== sValidFrom && aKeyNames[i] !== sValidTo) {
						var iPos = aKeyPositions[aKeyNames[i]],
							iColPos = i + 1;
						setParam(rs, pStmt, iPos, iColPos);
					} else {
						pStmt.setDate(i + 1, rs.getDate(aKeyPositions[aKeyNames[i]] + 1));
					}
				}

				selRs = pStmt.executeQuery();

				if (selRs) {
					selRs.next();

					bResult = selRs.getInteger(1) > 0;
				}
				pStmt.close();
			}
		}
	} catch (oErr) {
		if (pStmt) {
			pStmt.close();
		}
		$.utils.error.handleError(oErr);
	}
	return bResult;
}

function update(param, bUpdatePeriod) {
	$.trace.debug(sEntFunct);
	var pStmt;

	try {
		if ($.utils.session.checkSessionUserAllowedToEdit()) {
			var sTableName = $.utils.mapping.getMappingTableName(param);

			if (sTableName) {
				var aKeys = $.utils.mapping.listMappingTablePrimaryKeys(param);
				pStmt = param.connection.prepareStatement(sSelectFrom.concat(param.afterTableName, "\""));
				var sInsertStmt = "update \"".concat(sTableName, "\"", (bUpdatePeriod ? " FOR PORTION OF APPLICATION_TIME FROM ? TO ?" : ""), " set ");
				var aColsToIgnore = ["SystemValidFrom", "SystemValidTo"];

				aKeys.forEach(function (sKey) {
					aColsToIgnore.push(sKey);
				});

				var rs = pStmt.executeQuery();

				while (rs.next()) {
					var bFirstKey = true,
						bFirst = true,
						iParamCnt = 0,
						sCondition = "",
						iOffset = 0,
						sValidFrom = "BusinessValidFrom",
						sValidTo = "BusinessValidTo",
						aKeyPositions = [];

					rs._columnInfo.forEach(function (oColumn, i) {
						if (aKeys.includes(oColumn.columnName)) {
							aKeyPositions[oColumn.columnName] = i;
							if ((!bUpdatePeriod || (bUpdatePeriod && oColumn.columnName !== sValidFrom && oColumn.columnName !== sValidTo))) {
								sCondition += (bFirstKey ? " where " : " and ").concat("\"", oColumn.columnName, "\" = ?");
								bFirstKey = false;
							}
						}
						if ((!aColsToIgnore.includes(oColumn.columnName) && !aKeys.includes(oColumn.columnName))) {
							sInsertStmt += (bFirst ? "" : ", ").concat("\"", oColumn.columnName, "\" = ?");
							bFirst = false;
							iParamCnt++;
						}
					});
					sInsertStmt += sCondition;
					pStmt = param.connection.prepareStatement(sInsertStmt);

					if (bUpdatePeriod) {
						setParam(rs, pStmt, aKeyPositions["BusinessValidFrom"], 1); // Application time from
						setParam(rs, pStmt, aKeyPositions["BusinessValidTo"], 2); // Application time to
						iParamCnt += 2; // + 2: period parameters have to be adhered!
						iOffset = 2; // + 2: period parameters have to be adhered!
					}
					setParams(rs, pStmt, aColsToIgnore, iOffset);
					aKeys.forEach(function (sKey, i) {
						if ((!bUpdatePeriod || (bUpdatePeriod && sKey !== sValidFrom && sKey !== sValidTo))) {
							var iPos = aKeyPositions[sKey],
								iColPos = parseInt(iParamCnt) + i + 1;

							setParam(rs, pStmt, iPos, iColPos);
						}
					});
					pStmt.executeQuery();
				}
			}
		} else {
			//not authorized to edit entries!!!
			$.utils.error.throwForbiddenError();
		}
	} catch (oErr) {
		if (pStmt) {
			pStmt.close();
		}
		//param.connection.close();
		$.utils.error.handleError(oErr);
	}
}

function setParams(rs, pStmt, aColsToIgnore, iParamOffset) {
	var i = iParamOffset ? iParamOffset : 0;

	rs._columnInfo.forEach(function (oColumn, j) {
		if (!aColsToIgnore.includes(oColumn.columnName)) {
			i++;
			setParam(rs, pStmt, j, i);
		}
	});
}

function setParam(rs, pStmt, iSourceColumnIndex, iParamIndex) {
	var oColumn = rs._columnInfo[iSourceColumnIndex],
		iReadIdx = iSourceColumnIndex + 1, // plus 1 for non zero based array
		oDate,
		sValue;

	if (oColumn.nativeTypeName === "NVARCHAR") {
		sValue = rs.getString(iReadIdx);

		if (sValue) {
			pStmt.setString(iParamIndex, sValue);
		} else {
			pStmt.setNull(iParamIndex);
		}
	} else if ((oColumn.nativeTypeName === "DAYDATE") || (oColumn.nativeTypeName === "LONGDATE")) {
		oDate = rs.getDate(iReadIdx);
		if (oDate) {
			pStmt.setDate(iParamIndex, oDate);
		} else {
			pStmt.setNull(iParamIndex);
		}
	} else if (oColumn.nativeTypeName === "INT") {
		var iValue = rs.getInteger(iReadIdx);

		if (iValue) {
			pStmt.setInteger(iParamIndex, iValue);
		} else {
			pStmt.setNull(iParamIndex);
		}
	} else if (oColumn.nativeTypeName === "DECIMAL") {
		var iValue = rs.getDecimal(iReadIdx);

		if (iValue) {
			pStmt.setDecimal(iParamIndex, iValue);
		} else {
			pStmt.setNull(iParamIndex);
		} 
	} else if (oColumn.nativeTypeName === "NCLOB") {
		sValue = rs.getNClob(iReadIdx);

		if (sValue) {
			if (oColumn.precision < sValue.length) {
				$.utils.error.throwValueTooLargeError();
			}
			pStmt.setNClob(iParamIndex, sValue);
		} else {
			pStmt.setNull(iParamIndex);
		}
	} else {
		$.utils.error.throwUnsupportedDataTypeError();
	}
}