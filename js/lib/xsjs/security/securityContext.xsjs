try {
	var conn = $.hdb.getConnection(),
		oSession,
		resourcePath = $.request.path.substring($.request.path.lastIndexOf("/") + 1);

	if (resourcePath === "checkScope") {
		var scope = $.request.parameters.get("scope");

		if (scope) {
			var oResult;

			$.import('utils', 'session');
			$.response.contentType = "application/json";

			oSession = $.utils.session;
			oResult = {
				"scope": scope,
				"result": oSession.checkScope(scope),
				"loginName": $.session.user
			};

			$.response.returnCode = 200;
			$.response.status = $.net.http.OK;
			$.response.setBody(oResult);
		}
	}
	conn.close();
} catch (sqlException) {
	// Return error
	$.response.setBody("Failed to retrieve data");
	$.response.status = $.net.http.INTERNAL_SERVER_ERROR;
}