/*eslint no-console: 0, no-unused-vars: 0, dot-notation: 0, no-use-before-define: 0, no-redeclare: 0*/
"use strict";

function handleError(oErr) {
	if (oErr) {
		throw (oErr.message);
	} else {
		throw new Error('{"code": 500, "message": "Something went wrong..."}');
	}
}

function throwValueTooLargeError() {
	throw new Error('{"code": 274, "message": "Inserted value too large for column"}');
}

function throwForbiddenError() {
	throw new Error('{"code": 403, "message": "Forbidden"}');
}

function throwUnsupportedDataTypeError() {
	throw new Error('{"code": 500, "message": "Not supported datatype"}');
}

function throwNullNotAllowedError() {
	throw new Error('{"code": 500, "message": "Value must not be null"}');
}