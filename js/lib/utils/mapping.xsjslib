/*eslint no-console: 0, no-unused-vars: 0, dot-notation: 0, no-use-before-define: 0, no-redeclare: 0*/
"use strict";

function getSrvEntityName(param) {
	var sResult,
		sSrvName,
		aMatch;

	sSrvName = param.afterTableName;
	aMatch = sSrvName.match(new RegExp("([0-9_]+$)"));

	if (aMatch && Array.isArray(aMatch) && aMatch.length > 0) {
		sResult = sSrvName.substring(5, sSrvName.indexOf(aMatch[0]));
	}
	return sResult;
}

function getMappingTableName(param) {
	var sResult,
		pStmt;

	try {
		var sSelect =
			'SELECT "MappingTableName" FROM "com.adweko.adapter.osx.configuration::MappingConfiguration" WHERE "MappingSrvEntityName" = ?';
		pStmt = param.connection.prepareStatement(sSelect);
		var sSrvName = getSrvEntityName(param),
			selRs;

		pStmt.setString(1, sSrvName);
		selRs = pStmt.executeQuery();

		if (selRs && selRs.next()) {
			sResult = selRs.getString(1);
		}
		pStmt.close();
	} catch (oErr) {
		if (pStmt) {
			pStmt.close();
		}
		throw oErr;
	}
	return sResult;
}

function listMappingTablePrimaryKeys(param) {
	var aResult = {};

	var oCols = selectMappingTableColumnInformation(param);

	if (oCols) {
		Object.keys(oCols).forEach(function (sCol) {
			if (oCols[sCol].IS_PRIMARY_KEY) {
				aResult[sCol] = oCols[sCol].POSITION;
			}
		});
	}
	return sortDictByValues(aResult);
}

function selectMappingTableColumnInformation(param) {
	var aResult = {};
	var pStmt;

	try {
		var selRs,
			sSelect,
			sSrvEntity = getSrvEntityName(param);

		if (sSrvEntity) {
			sSelect = 'SELECT DISTINCT c."COLUMN_NAME", c."POSITION", c."IS_NULLABLE", c."DATA_TYPE_NAME", c."LENGTH", c."SCALE", c."DEFAULT_VALUE", ' +
				'CASE WHEN k."IS_PRIMARY_KEY" IS NULL THEN ? ELSE k."IS_PRIMARY_KEY" END AS "IS_PRIMARY_KEY" ' +
				'FROM "com.adweko.adapter.osx.synonyms::TABLES" as t ' +
				'JOIN "com.adweko.adapter.osx.synonyms::TABLE_COLUMNS" as c ON (c."TABLE_NAME" = t."TABLE_NAME" AND c."SCHEMA_NAME" = t."SCHEMA_NAME") ' +
				'LEFT JOIN "com.adweko.adapter.osx.synonyms::CONSTRAINTS" as k ' +
				'ON (k."TABLE_NAME" = c."TABLE_NAME" AND k."COLUMN_NAME" = c."COLUMN_NAME" AND k."SCHEMA_NAME" = t."SCHEMA_NAME" AND k."IS_PRIMARY_KEY" = ?)' +
				'JOIN "com.adweko.adapter.osx.configuration::MappingConfiguration" as a ON a."MappingTableName" = t."TABLE_NAME" ' +
				'WHERE a."MappingSrvEntityName" = ? AND t."SCHEMA_NAME" = current_schema';

			pStmt = param.connection.prepareStatement(sSelect);

			pStmt.setString(1, 'FALSE');
			pStmt.setString(2, 'TRUE');
			pStmt.setString(3, sSrvEntity);

			selRs = pStmt.executeQuery();

			if (selRs) {
				while (selRs.next()) {
					var oColInfo = {
						"COLUMN_NAME": selRs.getString(1),
						"POSITION": selRs.getInt(2),
						"IS_NULLABLE": parseBool(selRs.getString(3)),
						"DATA_TYPE_NAME": selRs.getString(4),
						"LENGTH": selRs.getInt(5),
						"SCALE": selRs.getInt(6),
						"DEFAULT_VALUE": selRs.getString(7),
						"IS_PRIMARY_KEY": parseBool(selRs.getString(8))
					};
					aResult[selRs.getString(1)] = oColInfo;
				}
			}
			pStmt.close();
		}
	} catch (oErr) {
		if (pStmt) {
			pStmt.close();
		}
		throw oErr;
	}
	return aResult;
}

function parseBool(value) {
	var bResult = false;

	if (value && Boolean(value)) {
		var sBool = value.toString().toUpperCase();

		bResult = sBool === "TRUE" ? true : false;
	}
	return bResult;
}

function sortDictByValues(dict) {
	var items = Object.keys(dict).map(
		(key) => { return [key, dict[key]] }
	);
	items.sort(
		(first, second) => { return first[1] - second[1] }
	);
	var keys = items.map(
		(e) => { return e[0] }
	);
	return keys;
}