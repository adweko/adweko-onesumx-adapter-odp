try {
	// Initialize hana connection/context
	var oConn = $.hdb.getConnection();

	$.response.status = $.net.http.OK;
	$.response.contentType = "application/json";
	$.response.setBody({
		"userInfo": $.session.securityContext.userInfo,
		"scopes": $.session.securityContext.scopes
	});

	oConn.close();
} catch (ex) {
	// Return error
	$.response.setBody("Failed to retrieve data");
	$.response.status = $.net.http.INTERNAL_SERVER_ERROR;
}