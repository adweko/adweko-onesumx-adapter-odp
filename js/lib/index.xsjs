var schema = $.hdb._dbReqOptions._globalOptions.schema;
				//--> HDB connection details 
var conn = $.hdb.getConnection();  
                //-->Input Parameter of HDB procedure
//var pSolveJobID = $.request.parameters.get('23056');  
                //-->Input Parameter of HDB procedure
//var pModelID = $.request.parameters.get('19');  
                //--> Result set to capture output 
var results = {};                                        
//results.data = [];		
 
try {
                //--> HDB connection details
	//var conn = $.hdb.getConnection(); 
	                //--> HDB proceude and scheman name                       
	var query = conn.loadProcedure( schema, "com.adweko.adapter.osx.procedures::Upload_Trigger_For_ResultData"); 
	            	//--> Pass the parameter values
	//var params = query(pSolveJobID, pModelID);    
	var params = query( );   
	            	//--> Capture the output using out paramater of HDB procedure     
	            	//--> Erfassen Sie die Ausgabe mit dem Parameter out der HDB-Prozedur
//	var outData = params['p_out_table'];                                 
	            	//--> Pass the output to result set                                                                        
//	results.data = outData;
	            	//--> pass the status code to  response                                           
	$.response.status = $.net.http.OK;                                   
	$.response.contentType = "application/json";
	        		//--> set response body with result output
	$.response.setBody(JSON.stringify(results));                         
        		//--> Error handeling              
conn.commit();
} catch (err) {                         
	$.response.status = 500;    
	$.response.setBody(err.message);
}

//conn.commit();
conn.close();
