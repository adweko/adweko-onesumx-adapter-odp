describe("utils/session test suite", function() {
	
	it("removeAllSpecialChars", function() {
		expect($.utils.session.removeAllSpecialChars("HelloWorld")).toBe("HelloWorld");
		expect($.utils.session.removeAllSpecialChars("HelloWorld!")).toBe("HelloWorld");
		expect($.utils.session.removeAllSpecialChars("#Hello#World")).toBe("HelloWorld");
		expect($.utils.session.removeAllSpecialChars("Hello_World")).toBe("Hello_World");
		expect($.utils.session.removeAllSpecialChars("Hello.World?")).toBe("HelloWorld");
		expect($.utils.session.removeAllSpecialChars("Hello-World.")).toBe("HelloWorld");
	});
	
	it("doesNotcontainSpecialChars", function() {
		expect($.utils.session.containsSpecialChars("input")).toBe(false);
		expect($.utils.session.containsSpecialChars("")).toBe(false);
		expect($.utils.session.containsSpecialChars(null)).toBe(false);
		expect($.utils.session.containsSpecialChars(undefined)).toBe(false);
	});
	
	it("containsPuncuation", function() {
		expect($.utils.session.containsSpecialChars("!input")).toBe(true);
		expect($.utils.session.containsSpecialChars("input?")).toBe(true);
		expect($.utils.session.containsSpecialChars("input:")).toBe(true);
		expect($.utils.session.containsSpecialChars("input;")).toBe(true);
		expect($.utils.session.containsSpecialChars("input,")).toBe(true);
	});
	
	it("containsBrackets", function() {
		expect($.utils.session.containsSpecialChars("[input]")).toBe(true);
		expect($.utils.session.containsSpecialChars("|input|")).toBe(true);
		expect($.utils.session.containsSpecialChars("{input}")).toBe(true);
		expect($.utils.session.containsSpecialChars("input/")).toBe(true);
		expect($.utils.session.containsSpecialChars("input/")).toBe(true);
		expect($.utils.session.containsSpecialChars("\"input")).toBe(true);
		expect($.utils.session.containsSpecialChars("<input>")).toBe(true);
	});
	
	it("containsOperator", function() {
		expect($.utils.session.containsSpecialChars("*input")).toBe(true);
		expect($.utils.session.containsSpecialChars("+input")).toBe(true);
		expect($.utils.session.containsSpecialChars("=input")).toBe(true);
		expect($.utils.session.containsSpecialChars("-input")).toBe(true);
		expect($.utils.session.containsSpecialChars("%input")).toBe(true);
		expect($.utils.session.containsSpecialChars("~input")).toBe(true);
	});
		
	it("containsQuoatationMark", function() {
		expect($.utils.session.containsSpecialChars("`input`")).toBe(true);
		expect($.utils.session.containsSpecialChars("'input'")).toBe(true);
	});
	
	it("containsSign", function() {
		expect($.utils.session.containsSpecialChars("#input")).toBe(true);
		expect($.utils.session.containsSpecialChars("$input")).toBe(true);
		expect($.utils.session.containsSpecialChars("^input")).toBe(true);
		expect($.utils.session.containsSpecialChars("&input")).toBe(true);
	});
	
	it("parseBoolTrue", function() {
		expect($.utils.session.parseBool(true)).toBe(true);
	});
	
	it("parseBoolFalse", function() {
	expect($.utils.session.parseBool(false)).toBe(false);
	});
	
	it("parseBoolString", function() {
	expect($.utils.session.parseBool("testString")).toBe(false);
	});
	
	it("parseBoolUndefined", function() {
	expect($.utils.session.parseBool(undefined)).toBe(false);
	});
	
	it("parseBoolNull", function() {
	expect($.utils.session.parseBool(null)).toBe(false);
	});
	
	it("escapeSpecialCharsUndefinedWithBackslash", function() {
		expect($.utils.session.escapeSpecialChars("\\input\\")).toBe("\\\\input\\\\");
	});
	
	it("escapeSpecialCharsUndefinedWithQuotationMark", function() {
		expect($.utils.session.escapeSpecialChars("\"input\"")).toBe("\\\"input\\\"");
	});
	
	it("escapeSpecialCharsUndefinedWithSlash", function() {
		expect($.utils.session.escapeSpecialChars("\/input\/")).toBe("\\/input\\/");
	});
	
	it("escapeSpecialCharsUndefinedAtBeginning", function() {
		expect($.utils.session.escapeSpecialChars("\binput")).toBe("\\binput");
	});
	
	it("escapeSpecialCharsUndefinedWithFormFeed", function() {
		expect($.utils.session.escapeSpecialChars("input\f")).toBe("input\\f");
	});
	
	it("escapeSpecialCharsUndefinedWithLineFeed", function() {
		expect($.utils.session.escapeSpecialChars("input\n")).toBe("input\\n");
	});
	
	it("escapeSpecialCharsUndefinedWithCarriageReturn", function() {
		expect($.utils.session.escapeSpecialChars("input\r")).toBe("input\\r");
	});
	
	it("escapeSpecialCharsUndefinedWithHorizontalTab", function() {
		expect($.utils.session.escapeSpecialChars("input\t")).toBe("input\\t");
	});
	
	it("escapeSpecialCharsNull", function() {
		expect($.utils.session.escapeSpecialChars(null)).toBe("");
	});
	
	it("escapeSpecialCharsUndefined", function() {
		expect($.utils.session.escapeSpecialChars(undefined)).toBe("");
	});
	
	it("escapeSpecialCharsNull", function() {
		expect($.utils.session.escapeSpecialChars(null)).toBe("");
	});
	
	it("escapeSpecialCharsTextWithQuotationMark", function() {
		expect($.utils.session.escapeSpecialCharsText("\"input\"")).toBe("\"\"input\"\"");
	});
	
	it("escapeSpecialCharsTextUndefined", function() {
		expect($.utils.session.escapeSpecialCharsText(undefined)).toBe("");
	});
	
	it("escapeSpecialCharsTextNull", function() {
		expect($.utils.session.escapeSpecialCharsText(null)).toBe("");
	});
});