describe("utils/mapping test suite", function() {
	
	it("getSrvEntityName", function () {
		var res = $.utils.mapping.getSrvEntityName({afterTableName: "@NEW_CommodityTypeMapping1_10_0_1", beforeTableName: "OLD_CommodityTypeMapping1_10_0_2"});
		expect(res).toBe("CommodityTypeMapping");
		
	});
	
	it("parseBoolTrue", function() {
		var bool = $.utils.mapping.parseBool(true);
		expect(true).toBe(bool);
	});
	
	it("parseBoolFalse", function() {
		var bool = $.utils.mapping.parseBool(false);
		expect(false).toBe(bool);
	});
	
	it("parseBoolString", function() {
		var bool = $.utils.mapping.parseBool("testString");
		expect(false).toBe(bool);
	});
	
	it("parseBoolUndefined", function() {
		var bool = $.utils.mapping.parseBool(undefined);
		expect(false).toBe(bool);
	});
	
	it("parseBoolNull", function() {
		var bool = $.utils.mapping.parseBool(null);
		expect(false).toBe(bool);
	});
	
	it("sortDictByValues", function() {
		var dict = {
			"four": 4,
			"two": 2,
			"five": 5,
			"one": 1,
			"six": 6,
			"three": 3
			};
		$.utils.mapping.sortDictByValues(dict);
		expect(dict).toEqual({
			"one": 1,
			"two": 2,
			"three": 3,
			"four": 4,
			"five": 5,
			"six": 6
			});
	});
});
