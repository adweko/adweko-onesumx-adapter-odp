describe("utils/error test suite", function() {

	it("handleError", function() {
		try {
			document.write();
		} catch (oErr) {
			expect(function() {$.utils.error.handleError(oErr);}).toThrow('document is not defined');
		}
	});
	
	it("handleUndefiendError", function() {
		try {
			$.utils.error.handleError(null);
		} catch (oErr) {
			expect(oErr.message).toBe('{"code": 500, "message": "Something went wrong..."}');
		}
	});
	
	it("throwValueTooLargeError", function() {
		try {
			$.utils.error.throwValueTooLargeError();
		} catch (oErr) {
			expect(oErr.message).toBe('{"code": 274, "message": "Inserted value too large for column"}');
		}
	});
	
	it("throwForbiddenError", function() {
		try {
			$.utils.error.throwForbiddenError();
		} catch (oErr) {
			expect(oErr.message).toBe('{"code": 403, "message": "Forbidden"}');
		}
	});
	
	it("throwUnsupportedDataTypeError", function() {
		try {
			$.utils.error.throwUnsupportedDataTypeError();
		} catch (oErr) {
			expect(oErr.message).toBe('{"code": 500, "message": "Not supported datatype"}');
		}
	});
	
	it("throwNullNotAllowedError", function() {
		try {
			$.utils.error.throwNullNotAllowedError();
		} catch (oErr) {
			expect(oErr.message).toBe('{"code": 500, "message": "Value must not be null"}');
		}
	});
});