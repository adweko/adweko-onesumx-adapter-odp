FUNCTION "com.adweko.adapter.osx.enhancement_content::get_RES_FACT_NPV_FairValue_Mapping"(	
		IN I_SOLVE_JOB_ID BIGINT,
		IN I_MODEL_ID BIGINT,
		IN I_SCENARIO_NAME NVARCHAR(100)
	)
	RETURNS TABLE(
		"RES_FACT_NPV_ID" BIGINT,
    	"AccountingChangeSequenceNumber" INTEGER ,
        "ComponentNumber" INTEGER ,
        "Currency" NVARCHAR (3),
        "FVCalculationMethod" NVARCHAR (20),
        "IndicatorResultBeforeChange" BOOLEAN ,
        "LotID" NVARCHAR (128),
        "RoleOfPayer" NVARCHAR (50),
        "_AccountingSystem.AccountingSystemID" NVARCHAR (128),
        "_Collection.CollectionID" NVARCHAR (128),
        "_Collection.IDSystem" NVARCHAR (40),
        "_Collection._Client.BusinessPartnerID" NVARCHAR (128),
        "_FinancialContract.FinancialContractID" NVARCHAR (128),
        "_FinancialContract.IDSystem" NVARCHAR (40),
        "_FinancialInstrument.FinancialInstrumentID" NVARCHAR (128),
        "_InvestmentAccount.FinancialContractID" NVARCHAR (128),
        "_InvestmentAccount.IDSystem" NVARCHAR (40),
        "BusinessValidFrom" DATE ,
        "BusinessValidTo" DATE ,
        "AccountingChangeDate" DATE ,
        "AccountingChangeReason" NVARCHAR (100),
        "Category" NVARCHAR (40),
        "FairValueInFunctionalCurrency" DECIMAL (34,6),
        "FairValueInGroupCurrency" DECIMAL (34,6),
        "FairValueInHardCurrency" DECIMAL (34,6),
        "FairValueInIndexlCurrency" DECIMAL (34,6),
        "FairValueInLocalCurrency" DECIMAL (34,6),
        "FairValueInPaymentCurrency" DECIMAL (34,6),
        "FairValueInPositionCurrency" DECIMAL (34,6),
        "FunctionalCurrency" NVARCHAR (3),
        "GroupCurrency" NVARCHAR (3),
        "HardCurrency" NVARCHAR (3),
        "IndexCurrency" NVARCHAR (3),
        "LocalCurrency" NVARCHAR (3),
        "PaymentCurrency" NVARCHAR (3),
        "PositionCurrency" NVARCHAR (3),
        "SourceSystemID" NVARCHAR (128),
        "ChangeTimestampInSourceSystem" TIMESTAMP ,
        "ChangingUserInSourceSystem" NVARCHAR (128),
        "ChangingProcessType" NVARCHAR (40),
        "ChangingProcessID" NVARCHAR (128)
	)
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN
    SELECT
        MIN("RES_FACT_NPV_ID") AS "RES_FACT_NPV_ID",
    	"AccountingChangeSequenceNumber",
        "ComponentNumber",
        "Currency",
        "FVCalculationMethod",
        "IndicatorResultBeforeChange",
        "LotID",
        "RoleOfPayer",
        "_AccountingSystem.AccountingSystemID",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InvestmentAccount.FinancialContractID",
        "_InvestmentAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AccountingChangeDate",
        "AccountingChangeReason",
        "Category",
        SUM("FairValueInFunctionalCurrency") AS "FairValueInFunctionalCurrency",
        SUM("FairValueInGroupCurrency") AS "FairValueInGroupCurrency",
        SUM("FairValueInHardCurrency") AS "FairValueInHardCurrency",
        SUM("FairValueInIndexlCurrency") AS "FairValueInIndexlCurrency",
        SUM("FairValueInLocalCurrency") AS "FairValueInLocalCurrency",
        SUM("FairValueInPaymentCurrency") AS "FairValueInPaymentCurrency",
        SUM("FairValueInPositionCurrency") AS "FairValueInPositionCurrency",
        "FunctionalCurrency",
        "GroupCurrency",
        "HardCurrency",
        "IndexCurrency",
        "LocalCurrency",
        "PaymentCurrency",
        "PositionCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    
    FROM (

        SELECT
        	res_fact."RES_FACT_NPV_ID" AS "RES_FACT_NPV_ID",
            CAST(00000001 AS INTEGER) AS "AccountingChangeSequenceNumber",
            CAST(-1 AS INTEGER) AS "ComponentNumber",
            basecur."CURRENCY_NAME" AS "Currency",
            'IncomeApproach' AS "FVCalculationMethod",
            false AS "IndicatorResultBeforeChange",
            NULL AS "LotID",
            NULL AS "RoleOfPayer",
            'IFRS' AS "_AccountingSystem.AccountingSystemID",
            NULL AS "_Collection.CollectionID",
            NULL AS "_Collection.IDSystem",
            NULL AS "_Collection._Client.BusinessPartnerID",
	    	CASE
	    		WHEN LEFT(IFNULL(res_fact."CONTRACT_NAME",''), 4) <> 'I-_-' 
	    			THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_NPV_ID" AS NVARCHAR (128)))
	    		ELSE NULL
	    	END AS "_FinancialContract.FinancialContractID",
	    	CASE
	    		WHEN LEFT(res_fact."CONTRACT_NAME", 4) <> 'I-_-'
	    			THEN CAST("com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME") AS NVARCHAR(40))
	    		ELSE NULL
	    	END AS "_FinancialContract.IDSystem",
	    	CASE
	    		WHEN LEFT(IFNULL(res_fact."CONTRACT_NAME",''), 4) = 'I-_-'
	    			THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialInstrumentID"(res_fact."CONTRACT_NAME") 
	    		ELSE NULL	
	    	END AS "_FinancialInstrument.FinancialInstrumentID",
	    	CASE
				WHEN LEFT(res_fact."CONTRACT_NAME", 4) = 'I-_-'
					THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_NPV_ID" AS NVARCHAR (128)))
				ELSE
					NULL
			END AS "_InvestmentAccount.FinancialContractID",
			CASE
				WHEN LEFT(res_fact."CONTRACT_NAME", 4) = 'I-_-'
					THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME")
				ELSE
					NULL
			END AS "_InvestmentAccount.IDSystem",
	    	"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(slv."ANALYSIS_DATE") AS "BusinessValidFrom",
	    	"com.adweko.adapter.osx.resultdata.common::get_MaxDate"() AS "BusinessValidTo",
	    	NULL AS "AccountingChangeDate",
            NULL AS "AccountingChangeReason",
            NULL AS "Category",
            TO_DECIMAL(res_fact."NPV_BAS_CUR", 34, 6) AS "FairValueInFunctionalCurrency",
            NULL AS "FairValueInGroupCurrency",
            NULL AS "FairValueInHardCurrency",
            NULL AS "FairValueInIndexlCurrency",
            NULL AS "FairValueInLocalCurrency",
            TO_DECIMAL(res_fact."NPV_CT_CUR", 34, 6) AS "FairValueInPaymentCurrency",
            TO_DECIMAL(res_fact."NPV_CT_CUR", 34, 6) AS "FairValueInPositionCurrency",
            basecur."CURRENCY_NAME" AS "FunctionalCurrency",
            NULL AS "GroupCurrency",
            NULL AS "HardCurrency",
            NULL AS "IndexCurrency",
            NULL AS "LocalCurrency",
            contcur."CURRENCY_NAME" AS "PaymentCurrency",
            contcur."CURRENCY_NAME" AS "PositionCurrency",
            'OneSumX RiskPro' AS "SourceSystemID",
	    	NULL AS "ChangeTimestampInSourceSystem",
	    	NULL AS "ChangingUserInSourceSystem",
	    	NULL AS "ChangingProcessType",
	    	NULL AS "ChangingProcessID"
	    									
	    	FROM "com.adweko.adapter.osx.synonyms::RES_FACT_NPV" AS res_fact
	    	INNER JOIN "com.adweko.adapter.osx.synonyms::RES_SOLVE" AS slv 
	    	ON (res_fact."SLV_JOB_ID"	= slv."SLV_JOB_ID" 
	    	AND res_fact."MODEL_ID" 	= slv."MODEL_ID")
	    	INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_static_scenario"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS sc
	    	ON (sc."RES_SOLVE_ID"		= slv."RES_SOLVE_ID"
	    	AND sc."STATIC_SCENARIO_ID" = res_fact."STAT_SCEN_ID")
	    	INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_base_currency"(:I_SOLVE_JOB_ID, :I_MODEL_ID ) AS basecur
			ON basecur."SLV_JOB_ID" = :I_SOLVE_JOB_ID
			AND basecur."MODEL_ID"	= :I_MODEL_ID
			LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_ct_ev_currency"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS contcur
			ON contcur."SLV_JOB_ID" 	= :I_SOLVE_JOB_ID
			AND contcur."SLV_JOB_ID"	= :I_SOLVE_JOB_ID
			AND contcur."CURRENCY_ID"	= res_fact."CONTRACT_CURRENCY_ID"
	    		
	    	WHERE res_fact."SLV_JOB_ID" = :I_SOLVE_JOB_ID
	    	AND res_fact."MODEL_ID" 	= :I_MODEL_ID
	    	AND res_fact."CONTRACT_NAME" IS NOT NULL
	    	AND SC."STATIC_SCENARIO_NAME" = :I_SCENARIO_NAME)
		
		GROUP BY
    	    "AccountingChangeSequenceNumber",
            "ComponentNumber",
            "Currency",
            "FVCalculationMethod",
            "IndicatorResultBeforeChange",
            "LotID",
            "RoleOfPayer",
            "_AccountingSystem.AccountingSystemID",
            "_Collection.CollectionID",
            "_Collection.IDSystem",
            "_Collection._Client.BusinessPartnerID",
            "_FinancialContract.FinancialContractID",
            "_FinancialContract.IDSystem",
            "_FinancialInstrument.FinancialInstrumentID",
            "_InvestmentAccount.FinancialContractID",
            "_InvestmentAccount.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo",
            "AccountingChangeDate",
            "AccountingChangeReason",
            "Category",
            "FunctionalCurrency",
            "GroupCurrency",
            "HardCurrency",
            "IndexCurrency",
            "LocalCurrency",
            "PaymentCurrency",
            "PositionCurrency",
            "SourceSystemID",
            "ChangeTimestampInSourceSystem",
            "ChangingUserInSourceSystem",
            "ChangingProcessType",
            "ChangingProcessID"
		;
	   
END;