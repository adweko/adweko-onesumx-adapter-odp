view "com.adweko.adapter.osx.inputdata.fra::ForwardRateAgreement_PPayment_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        root."FinancialContract_IDSystem",
    	root."FinancialContract_FinancialContractID",
		root."dataGroup",
		root."contractID",
		root."node_No",
		root."isActiveFlagLegSign",
		CASE 
			WHEN
				paym."PaymentScheduleCategory" = 'Installment'
				AND paym."InstallmentsRelativeToInterestPaymentDates" = true
					THEN 1	
			WHEN
				paym."PaymentScheduleCategory" = 'Installment'
				AND paym."InstallmentsRelativeToInterestPaymentDates" = false
					THEN 2
			WHEN 
				paym."PaymentScheduleCategory" = 'PaymentList'
				AND 
				(
					paym."InstallmentsRelativeToInterestPaymentDates" = false
					OR paym."InstallmentsRelativeToInterestPaymentDates" IS NULL
				)
					THEN 3
		END AS "PaymentCase",
		paym."InstallmentAmount"					AS "PaymentSchedule_InstallmentAmount",
		paym."FirstPaymentDate"						AS "PaymentSchedule_FirstPaymentDate",
		paym."InstallmentLagPeriodLength"			AS "PaymentSchedule_InstallmentLagPeriodLength",
		paym."InstallmentPeriodLength"				AS "PaymentSchedule_InstallmentPeriodLength",
		paym."InstallmentLagTimeUnit"				AS "PaymentSchedule_InstallmentLagTimeUnit",
		paym."InstallmentPeriodTimeUnit"			AS "PaymentSchedule_InstallmentPeriodTimeUnit",
		pl."PaymentAmount"							AS "PaymentList_PaymentAmount",
		pl."PaymentDate"							AS "PaymentList_PaymentDate"
    FROM "com.adweko.adapter.osx.inputdata.fra::ForwardRateAgreement_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) root

    INNER JOIN "com.adweko.adapter.osx.synonyms::PaymentSchedule_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) paym 
    	ON paym."ASSOC_FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"
    		AND paym."ASSOC_FinancialContract_IDSystem"			= root."FinancialContract_IDSystem"
    		AND paym."PaymentScheduleType"	= 'Payout'
    		AND paym."PaymentScheduleCategory" IN ('Installment','PaymentList')

	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::PaymentListEntry_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) pl
    	ON pl."ASSOC_PaymentList_ASSOC_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
    		AND pl."ASSOC_PaymentList_ASSOC_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
    		AND	pl."ASSOC_PaymentList_SequenceNumber" = paym."SequenceNumber"
    		AND paym."PaymentScheduleCategory" = 'PaymentList'
