view "com.adweko.adapter.osx.inputdata.creditDefaultSwap.index::CreditDefaultSwap_DatagroupAssignment_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."IDSystem"															AS "FinancialContract_IDSystem",
    	fc."FinancialContractID"												AS "FinancialContract_FinancialContractID",
        fc."FinancialContractCategory"											AS "FinancialContract_FinancialContractCategory",
        fc."OTCDerivativeContractCategory"										AS "FinancialContract_OTCDerivativeContractCategory",
        fc."CreditDefaultSwapCategory"											AS "FinancialContract_CreditDefaultSwapCategory",
		fc."OriginalSigningDate"												AS "FinancialContract_OriginalSigningDate",
		fc."NotionalAmount"														AS "FinancialContract_NotionalAmount",
		fc."NotionalAmountCurrency"												AS "FinancialContract_NotionalAmountCurrency",
		fc."TerminationDate"													AS "FinancialContract_TerminationDate",
		fc."MaturityDate"														AS "FinancialContract_MaturityDate",
		fc."EffectiveDate"														AS "FinancialContract_EffectiveDate",
		fc."GoverningLawCountry"												AS "FinancialContract_GoverningLawCountry",
		fc."_Index_IndexID"														AS "FinancialContract_Index_IndexID",
	    fc."AttachmentPoint"													AS "FinancialContract_AttachmentPoint",
	    fc."ExhaustionPoint"													AS "FinancialContract_ExhaustionPoint",
	    fc."_StructuredProduct_FinancialContractID"								AS "FinancialContract_StructuredProduct_FinancialContractID",
		dg."dataGroup"

    FROM "com.adweko.adapter.osx.inputdata.creditDefaultSwap.index::CreditDefaultSwap_Datagroup_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) dg
    
    INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) fc
    	ON	fc."FinancialContractCategory"				= dg."FinancialContractCategory"
    	AND fc."OTCDerivativeContractCategory"			= dg."OTCDerivativeContractCategory"
    	AND fc."CreditDefaultSwapCategory"				= dg."CreditDefaultSwapCategory"

    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ProductCatalogAssignment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pc
    	ON	pc."ProductCatalogAssignment_FinancialContract_FinancialContractID" 		= fc."FinancialContractID" 
    	AND pc."ProductCatalogAssignment_FinancialContract_IDSystem"					= fc."IDSystem"
   		AND pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem" 		= dg."ProductCatalogItem"
    	AND pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID"	= dg."ProductCatalog_CatalogID"
    	
   WHERE	IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",'')			= IFNULL(dg."ProductCatalogItem",'')
		AND IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID",'')	= IFNULL(dg."ProductCatalog_CatalogID",'')
    	AND (dg."IDSystem" IS NULL OR dg."IDSystem" = fc."IDSystem")