VIEW "com.adweko.adapter.osx.inputdata.facility::Facility_Root_View"(
	IN I_BUSINESS_DATE DATE,
	IN I_SYSTEM_TIMESTAMP TIMESTAMP,
	IN I_DATAGROUP_STRING NVARCHAR(128))
AS
	SELECT
	
	    /*------ dg = Identifier for Facility_DatagroupAssignment_View ------*/
		dg."FinancialContract_FinancialContractID"						AS "FinancialContract_FinancialContractID",
		dg."FinancialContract_IDSystem"									AS "FinancialContract_IDSystem",
		dg."FinancialContract_OriginalSigningDate"						AS "FinancialContract_OriginalSigningDate",
		dg."FinancialContract_FacilityStartDate"						AS "FinancialContract_FacilityStartDate",
        dg."FinancialContract_FacilityEndDate"							AS "FinancialContract_FacilityEndDate",
        dg."FinancialContract_Seniority"								AS "FinancialContract_Seniority",
        dg."FinancialContract_FacilityOfDrawing_IDSystem"				AS "FinancialContract_FacilityOfDrawing_IDSystem",
        dg."FinancialContract_FacilityOfDrawing_FinancialContractID"	AS "FinancialContract_FacilityOfDrawing_FinancialContractID",
        dg."FinancialContract_ApprovedNominalAmount"					AS "FinancialContract_ApprovedNominalAmount",
		dg."FinancialContract_FinancialContractCategory"				AS "FinancialContract_FinancialContractCategory",
		CAST("com.adweko.adapter.osx.inputdata.common::get_contractID"(
			'F',
			'',
			dg."FinancialContract_FinancialContractID",
			dg."FinancialContract_IDSystem",
			''
		) AS NVARCHAR(128))												AS "contractID",
		'0001'															AS "node_No",
		dg."dataGroup",
		
		/*------ mb = Identifier for MonetaryBalance_View ------*/
		mb."BalanceCurrency"											AS "MonetaryBalance_BalanceCurrency",
		
		/*------ agrd = Identifier for AgreedLimit ------*/
		agrd."LimitType"												AS "AgreedLimit_LimitType",
		agrd."LimitValidFrom"											AS "AgreedLimit_LimitValidFrom",
		agrd."LimitValidTo"												AS "AgreedLimit_LimitValidTo",
		agrd."LimitAmount"												AS "AgreedLimit_LimitAmount",
		agrd."LimitCurrency"											AS "AgreedLimit_LimitCurrency",
		
		/*------ ouca = Identifier for OrganizationalUnitContractAssignment_View ------*/
		ouca."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID" AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationHostingOrganizationalUnit_BusinessPartnerID",
	
		/*------ bpcaBorrower = Identifier for BusinessPartnerContractAssignment_View ------*/
		bpcaBorrower."ASSOC_PartnerInParticipation_BusinessPartnerID"	AS "BusinessPartnerContractAssignment_Borrower_PartnerInParticipation_BusinessPartnerID",
		bpcaBorrower."ContractDataOwner"								AS "BusinessPartnerContractAssignment_Borrower_ContractDataOwner",
		bpcaLender."ASSOC_PartnerInParticipation_BusinessPartnerID"		AS "BusinessPartnerContractAssignment_Lender_PartnerInParticipation_BusinessPartnerID",
		bpcaLender."ContractDataOwner"									AS "BusinessPartnerContractAssignment_Lender_ContractDataOwner",
		
		/*------ pca = Identifier for ProductCatalogAssignment_View ------*/
		pca."StandardCatalog_ProductCatalogItem",
		pca."CustomCatalog_ProductCatalogItem",
		
		CASE
			WHEN bpcaLender."ContractDataOwner" = true
				THEN true
			WHEN bpcaBorrower."ContractDataOwner" = true
				THEN false
		END	 AS "isActive"

	FROM  "com.adweko.adapter.osx.inputdata.facility::Facility_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS dg
	
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMonetaryBalance"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Facility') mb
		 ON mb."FinancialContractID"									= dg."FinancialContract_FinancialContractID" 
		AND mb."IDSystem"												= dg."FinancialContract_IDSystem"
		AND mb."MonetaryBalanceCategory"								= 'FacilityBalance'
	
	INNER JOIN "com.adweko.adapter.osx.synonyms::AgreedLimit_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  agrd 
		 ON agrd."ASSOC_FinancialContract_FinancialContractID"			= dg."FinancialContract_FinancialContractID" 
		AND agrd."ASSOC_FinancialContract_IDSystem"						= dg."FinancialContract_IDSystem"
		AND agrd."LimitType"											= 'FacilityLine'
		AND agrd."LimitCurrency"										= mb."BalanceCurrency"
		AND agrd."LimitAmount"											> 0
		AND agrd."LimitValidFrom"										<= :I_BUSINESS_DATE
		AND agrd."LimitValidTo" 										> :I_BUSINESS_DATE
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS ouca
		 ON ouca."ASSOC_Contract_FinancialContractID"					= dg."FinancialContract_FinancialContractID"
		AND ouca."ASSOC_Contract_IDSystem"								= dg."FinancialContract_IDSystem"
		AND ouca."RoleOfOrganizationalUnit"								= 'ManagingUnit'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaBorrower
    	 ON bpcaBorrower."ASSOC_FinancialContract_FinancialContractID"	= dg."FinancialContract_FinancialContractID"
    	AND bpcaBorrower."ASSOC_FinancialContract_IDSystem"				= dg."FinancialContract_IDSystem"
    	AND bpcaBorrower."Role"											= 'Borrower'
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaLender
    	ON  bpcaLender."ASSOC_FinancialContract_FinancialContractID"	= dg."FinancialContract_FinancialContractID" 
    	AND bpcaLender."ASSOC_FinancialContract_IDSystem"				= dg."FinancialContract_IDSystem"
    	AND bpcaLender."Role"											= 'Lender'
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS pca
    	 ON pca."IDSystem"												= dg."FinancialContract_IDSystem" 
        AND pca."FinancialContractID"									= dg."FinancialContract_FinancialContractID"