VIEW "com.adweko.adapter.osx.inputdata.iro::InterestRateOption_Interest_View"(
	IN I_BUSINESS_DATE DATE,
	IN I_SYSTEM_TIMESTAMP TIMESTAMP,
	IN I_DATAGROUP_STRING NVARCHAR(128) DEFAULT '',
	IN I_CAP_FLOOR_COLLAR NVARCHAR(16),
	IN I_COLLAR_C_OR_F NVARCHAR(1) DEFAULT NULL
    )
AS
    SELECT
        inte."OptionOfReferenceRateSpecification_InterestRateOption_IDSystem" AS "FinancialContract_IDSystem",
        inte."OptionOfReferenceRateSpecification_InterestRateOption_FinancialContractID" AS "FinancialContract_FinancialContractID",
		dcm."dayCountMethod" AS "contractDayCountMethod",
		"com.adweko.adapter.osx.inputdata.common::get_interestPaymentType" (inte."InterestInAdvance", kv1."Value", kv2."Value") AS "interestPaymentType",
		"com.adweko.adapter.osx.inputdata.common::get_businessDayConvention" (bdc."OsxBusinessDayConventionCode") AS "businessDayConvention",
		cal."calendar",
		"com.adweko.adapter.osx.inputdata.common::get_eomConvention" (inte."DayOfMonthOfInterestPeriodEnd") AS "eomConvention",		
		CASE 
			WHEN inte."InterestSpecificationCategory" = 'FixedRateSpecification'
				THEN inte."FixedRate"
			WHEN inte."InterestSpecificationCategory" = 'FloatingRateSpecification'
				THEN appl."ApplicableInterestRate_Rate"
		END AS "currentNominalInterestRate",
		inte."InterestSpecificationCategory" AS "Interest_InterestSpecificationCategory",
		inte."FirstInterestPeriodEndDate" AS "Interest_FirstInterestPeriodEndDate",
		inte."InterestPeriodLength" AS "Interest_InterestPeriodLength",
		inte."InterestPeriodTimeUnit" AS "Interest_InterestPeriodTimeUnit",
		inte."FixingRateSpecificationCategory" AS "Interest_FixingRateSpecificationCategory",
		inte."FirstInterestPeriodStartDate" AS "Interest_FirstInterestPeriodStartDate",
        inte."FirstRegularFloatingRateResetDate" AS "Interest_FirstRegularFloatingRateResetDate",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
						inte."ResetCutoffLength",
						mcp."cyclePeriod")
		END AS "lookbackPeriod",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN 'MONETARY_REFERENCE'
		END AS "rateCappingFlooring",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", inte."FloatingRateMax", inte."VariableRateMax")
		END AS "rateAveragingCap",	
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", inte."FloatingRateMin", inte."VariableRateMin")
		END AS "rateAveragingFloor"

    FROM "com.adweko.adapter.osx.inputdata.iro::InterestRateOption_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING, :I_CAP_FLOOR_COLLAR) root
    
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvInterestType
		ON kvInterestType."KeyID" = 'InterestRateOption_InterestType'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvBankLegFlag
		ON	kvBankLegFlag."KeyID" = 'IROBankLeg4InterestFlag'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inteCheck
		ON inteCheck."OptionOfReferenceRateSpecification_ComponentNumber"								
			= MAP(:I_CAP_FLOOR_COLLAR, 
				'Cap',root."InterestRateOptionComponent_Cap_ComponentNumber",
				'Floor',root."InterestRateOptionComponent_Floor_ComponentNumber",
				'Collar', MAP(:I_COLLAR_C_OR_F,
							'C',root."InterestRateOptionComponent_Cap_ComponentNumber",
							'F',root."InterestRateOptionComponent_Floor_ComponentNumber")
			)
			AND inteCheck."OptionOfReferenceRateSpecification_InterestRateOption_FinancialContractID"	= root."FinancialContract_FinancialContractID"
			AND inteCheck."OptionOfReferenceRateSpecification_InterestRateOption_IDSystem" 				= root."FinancialContract_IDSystem"
			AND inteCheck."InterestType" 					= kvInterestType."Value"
			AND inteCheck."FirstInterestPeriodStartDate"	<= :I_BUSINESS_DATE 
			AND	inteCheck."LastInterestPeriodEndDate"		> :I_BUSINESS_DATE
			AND ( kvBankLegFlag."Value" = 'Off' 	OR		inteCheck."RoleOfPayer"	= root."BankLegFlag" )
			
	INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inte
		ON inte."OptionOfReferenceRateSpecification_ComponentNumber"								
			= MAP(:I_CAP_FLOOR_COLLAR, 
				'Cap',root."InterestRateOptionComponent_Cap_ComponentNumber",
				'Floor',root."InterestRateOptionComponent_Floor_ComponentNumber",
				'Collar', MAP(:I_COLLAR_C_OR_F,
							'C',root."InterestRateOptionComponent_Cap_ComponentNumber",
							'F',root."InterestRateOptionComponent_Floor_ComponentNumber")
			)
			AND inte."OptionOfReferenceRateSpecification_InterestRateOption_FinancialContractID"	= root."FinancialContract_FinancialContractID"
			AND inte."OptionOfReferenceRateSpecification_InterestRateOption_IDSystem" 				= root."FinancialContract_IDSystem"
			AND inte."InterestType" 						= kvInterestType."Value"
			AND inte."FirstInterestPeriodStartDate"			<= :I_BUSINESS_DATE
			AND (
				inteCheck."OptionOfReferenceRateSpecification_InterestRateOption_FinancialContractID" 		IS NOT NULL
				AND inte."LastInterestPeriodEndDate"	> :I_BUSINESS_DATE
				OR
				inteCheck."OptionOfReferenceRateSpecification_InterestRateOption_FinancialContractID" 		IS NULL
				AND inte."LastInterestPeriodEndDate"	IS NULL
			)
			AND ( kvBankLegFlag."Value" = 'Off' 	OR		inte."RoleOfPayer"	= root."BankLegFlag" )
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ApplicableInterestRate"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) appl
		ON appl."ApplicableInterestRate_FinancialContract_FinancialContractID"		= root."FinancialContract_FinancialContractID"
			AND appl."ApplicableInterestRate_FinancialContract_IDSystem"			= root."FinancialContract_IDSystem"
			AND appl."ApplicableInterestRate_InterestSequenceNumber"				= inte."SequenceNumber"

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS dcm 
		ON dcm."DayCountConvention" = inte."DayCountConvention"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP)  AS kv1
		ON  kv1."KeyID" = 'TRUE'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv2
		ON  kv2."KeyID" = 'FALSE'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_BDC_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS bdc
		ON bdc."CalculationDay" = inte."BusinessDayConvention"
		AND bdc."PaymentDay"	= inte."DueScheduleBusinessDayConvention"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_calendar_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) cal 
		ON cal."BusinessCalendar" = inte."InterestBusinessCalendar"

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" = inte."ResetCutoffTimeUnit"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS cfss
		ON cfss."KeyID" = 'CapFloorSwitch'