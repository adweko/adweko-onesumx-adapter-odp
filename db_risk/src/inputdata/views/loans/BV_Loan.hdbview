VIEW "com.adweko.adapter.osx.inputdata.loans::BV_Loan"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP, IN I_DATAGROUP_STRING NVARCHAR(128) )
AS
       	SELECT
      	/*-----fc = Identifier for FinancialContract ------*/
       	fc."IDSystem"															AS "FinancialContract_IDSystem",
		fc."FinancialContractID"												AS "FinancialContract_FinancialContractID",
		fc."BusinessValidFrom"								    				AS "FinancialContract_BusinessValidFrom",
		fc."BusinessValidTo"								    				AS "FinancialContract_BusinessValidTo",
		fc."SystemValidFrom"											        AS "FinancialContract_SystemValidFrom",
		fc."SystemValidTo"												        AS "FinancialContract_SystemValidTo",
		fc."FinancialContractCategory"										    AS "FinancialContract_FinancialContractCategory",
		fc."OriginalSigningDate"										        AS "FinancialContract_OriginalSigningDate",
		fc."PurposeType"												        AS "FinancialContract_PurposeType",
		fc."OfferValidityStartDate"										        AS "FinancialContract_OfferValidityStartDate",
		fc."OfferValidityEndDate"										        AS "FinancialContract_OfferValidityEndDate",
		fc."GracePeriod"									   					AS "FinancialContract_GracePeriod",
		fc."Purpose"                                                            AS "FinancialContract_Purpose",
		fc."ApprovedNominalAmount"									  			AS "FinancialContract_ApprovedNominalAmount",
		fc."NominalAmountCurrency"										        AS "FinancialContract_NominalAmountCurrency",
		fc."TotalRepayableAmount"										        AS "FinancialContract_TotalRepayableAmount",
		fc."TotalRepayableAmountCurrency"										AS "FinancialContract_TotalRepayableAmountCurrency",
		fc."Discount"													        AS "FinancialContract_Discount",
		fc."DiscountAmount"												        AS "FinancialContract_DiscountAmount",
		fc."DiscountAmountCurrency"										        AS "FinancialContract_DiscountAmountCurrency",
		fc."DiscountPeriodLength"										        AS "FinancialContract_DiscountPeriodLength",
		fc."DiscountPeriodTimeUnit"										        AS "FinancialContract_DiscountPeriodTimeUnit",
		fc."ExpectedMaturityDate"										        AS "FinancialContract_ExpectedMaturityDate",
		fc."Seniority"													        AS "FinancialContract_Seniority",
		fc."CurePeriodLength"											        AS "FinancialContract_CurePeriodLength",
		fc."CurePeriodTimeUnit"												    AS "FinancialContract_CurePeriodTimeUnit",
		fc."LoanInsuranceProtectionLevel"									    AS "FinancialContract_LoanInsuranceProtectionLevel",
		fc."InsuredLoanAmount"											        AS "FinancialContract_InsuredLoanAmount",
		fc."CommunicatedEffectiveInterestRate"									AS "FinancialContract_CommunicatedEffectiveInterestRate",
		fc."CommunicatedEffectiveInterestRateMethod"					        AS "FinancialContract_CommunicatedEffectiveInterestRateMethod",
		fc."Recourse"														    AS "FinancialContract_Recourse",
		fc."AmortizationType"											        AS "FinancialContract_AmortizationType",
		fc."SpecializedLendingType"										        AS "FinancialContract_SpecializedLendingType",
		fc."GoverningLawCountry"                                        		AS "FinancialContract_GoverningLawCountry",
		fc."ASSOC_FacilityOfDrawing_IDSystem"									AS "FinancialContract_FacilityOfDrawing_IDSystem",
        fc."ASSOC_FacilityOfDrawing_FinancialContractID"						AS "FinancialContract_FacilityOfDrawing_FinancialContractID",
		fc."LifecycleStatus"												    AS "FinancialContract_LifecycleStatus",
		fc."FixedPeriodEndDate"													AS "FinancialContract_FixedPeriodEndDate",
    	fc."FixedPeriodStartDate"												AS "FinancialContract_FixedPeriodStartDate",
    	fc."IntendedAssetLiability"                                     		AS "FinancialContract_IntendedAssetLiability",
    	fc."_TrancheInSyndication__SyndicationAgreement_IDSystem"               AS "FinancialContract_TrancheInSyndication_SyndicationAgreement_IDSystem",
		fc."_TrancheInSyndication__SyndicationAgreement_FinancialContractID"    AS "FinancialContract_TrancheInSyndication_SyndicationAgreement_FinancialContractID",
		fc."_TrancheInSyndication_TrancheSequenceNumber"                        AS "FinancialContract_TrancheInSyndication_TrancheSequenceNumber",		
		fc."PromotionalLoanProgramID"											AS "FinancialContract_PromotionalLoanProgramID",
		
		/*-----csd = Identifier for ContractStatus_View ------*/
		csd."ContractStatusType"										        AS "ContractStatus_DefaultStatus_ContractStatusType",
		csd."BusinessValidFrom"											        AS "ContractStatus_DefaultStatus_BusinessValidFrom",
		csd."BusinessValidTo"											        AS "ContractStatus_DefaultStatus_BusinessValidTo",
		csd."SystemValidFrom"											        AS "ContractStatus_DefaultStatus_SystemValidFrom",
		csd."SystemValidTo"												        AS "ContractStatus_DefaultStatus_SystemValidTo",
		csd."Status"														    AS "ContractStatus_DefaultStatus_Status",
		
		/*-----csp = Identifier for ContractStatus_View ------*/
		csp."ContractStatusType"                                        		AS "ContractStatus_PastDueStatus_ContractStatusType",
		csp."BusinessValidFrom"                                         		AS "ContractStatus_PastDueStatus_BusinessValidFrom",
		csp."BusinessValidTo"                                           		AS "ContractStatus_PastDueStatus_BusinessValidTo",
		csp."SystemValidFrom"                                           		AS "ContractStatus_PastDueStatus_SystemValidFrom",
		csp."SystemValidTo"                                             		AS "ContractStatus_PastDueStatus_SystemValidTo",
		csp."Status"                                                    		AS "ContractStatus_PastDueStatus_Status",
		csp."ChangeTimestampInSourceSystem"                             		AS "ContractStatus_PastDueStatus_ChangeTimestampInSourceSystem",
		
		/*-----pca = Identifier for ProductCatalogAssignment_View ------*/
		'FSDMStandard' 										  					AS "ProductCatalogAssignment_ProductCatalogItem_ProductCatalog_CatalogID",
		pca."StandardCatalog_ProductCatalogItem"			     				AS "ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",
		pca."BusinessValidFrom"											        AS "ProductCatalogAssignment_BusinessValidFrom",
		pca."BusinessValidTo"											        AS "ProductCatalogAssignment_BusinessValidTo",
		pca."StandardCatalog_ProductCatalogItem",
		pca."CustomCatalog_ProductCatalogItem",

		/*-----bpca = Identifier for BusinessPartnerContractAssignment_View ------*/
		bpca."ASSOC_PartnerInParticipation_BusinessPartnerID"     				AS "BusinessPartnerContractAssignment_Borrower_PartnerInParticipation_BusinessPartnerID",
		bpca."ContractDataOwner"												AS "BusinessPartnerContractAssignment_Borrower_ContractDataOwner",
		bpca."BusinessValidFrom"										        AS "BusinessPartnerContractAssignment_Borrower_BusinessValidFrom",
		bpca."BusinessValidTo"											        AS "BusinessPartnerContractAssignment_Borrower_BusinessValidTo",
		bpca."SystemValidFrom"											        AS "BusinessPartnerContractAssignment_Borrower_SystemValidFrom",
		bpca."SystemValidTo"											        AS "BusinessPartnerContractAssignment_Borrower_SystemValidTo",

		bpcac."ASSOC_PartnerInParticipation_BusinessPartnerID"              	AS "BusinessPartnerContractAssignment_ClearingMember_PartnerInParticipation_BusinessPartnerID",
		bpcac."BusinessValidFrom"                                       		AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidFrom",
		bpcac."BusinessValidTo"                                         		AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidTo",
		bpcac."SystemValidFrom"                                         		AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidFrom",
		bpcac."SystemValidTo"                                           		AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidTo",
		
		/*-----ouca = Identifier for OrganizationalUnitContractAssignment_View ------*/
		ouca."RoleOfOrganizationalUnit"     									AS "OrganizationalUnitContractAssignment_RoleOfOrganizationalUnit",
		ouca."ASSOC_OrgUnit_OrganizationalUnitID"								AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationalUnitID",
		ouca."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID"					        AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationHostingOrganizationalUnit_BusinessPartnerID",
		ouca."BusinessValidFrom"										        AS "OrganizationalUnitContractAssignment_BusinessValidFrom",
		ouca."BusinessValidTo"											        AS "OrganizationalUnitContractAssignment_BusinessValidTo",
		ouca."SystemValidFrom"											        AS "OrganizationalUnitContractAssignment_SystemValidFrom",
		ouca."SystemValidTo"											        AS "OrganizationalUnitContractAssignment_SystemValidTo",
		
		/*-----inte = Identifier for Interest_View ------*/
		inte."SequenceNumber"       											AS "Interest_SequenceNumber",
		inte."BusinessValidFrom"        										AS "Interest_BusinessValidFrom",
		inte."BusinessValidTo"      											AS "Interest_BusinessValidTo",
		inte."SystemValidFrom"      											AS "Interest_SystemValidFrom",
		inte."SystemValidTo"        											AS "Interest_SystemValidTo",
		inte."InterestType"     												AS "Interest_InterestType",
		inte."PayingOrReceiving"        										AS "Interest_PayingOrReceiving",
		inte."PreconditionApplies"      										AS "Interest_PreconditionApplies",
		inte."ScaleApplies"     												AS "Interest_ScaleApplies",
		inte."InterestCategory"     											AS "Interest_InterestCategory",
		inte."FirstInterestPeriodStartDate"     								AS "Interest_FirstInterestPeriodStartDate",
		inte."FirstInterestPeriodEndDate"       								AS "Interest_FirstInterestPeriodEndDate",
		inte."LastInterestPeriodEndDate"        								AS "Interest_LastInterestPeriodEndDate",
		inte."InterestCurrency"     											AS "Interest_InterestCurrency",
		inte."InterestPeriodLength"     										AS "Interest_InterestPeriodLength",
		inte."InterestPeriodTimeUnit"       									AS "Interest_InterestPeriodTimeUnit",
		inte."DayOfMonthOfInterestPeriodEnd"        							AS "Interest_DayOfMonthOfInterestPeriodEnd",
		inte."InterestBusinessCalendar"     									AS "Interest_InterestBusinessCalendar",
		inte."BusinessDayConvention"        									AS "Interest_BusinessDayConvention",
		inte."InterestPaymentPrecision"     									AS "Interest_InterestPaymentPrecision",
		inte."InterestPaymentRoundingMethod"        							AS "Interest_InterestPaymentRoundingMethod",
		inte."InterestInAdvance"        										AS "Interest_InterestInAdvance",
		inte."AnnuityAmount"													AS "Interest_AnnuityAmount",
		inte."AnnuityAmountCurrency"											AS "Interest_AnnuityAmountCurrency",
		inte."ASSOC_ReferenceRateID_ReferenceRateID"        					AS "Interest_ReferenceRateID",
		inte."PeriodEndDueDateLag"      										AS "Interest_PeriodEndDueDateLag",
		inte."PeriodEndDueDateLagTimeUnit"      								AS "Interest_PeriodEndDueDateLagTimeUnit",
		inte."FirstDueDate"     												AS "Interest_FirstDueDate",
		inte."DueDateScheduleIsIndependent"     								AS "Interest_DueDateScheduleIsIndependent",
		inte."DueSchedulePeriodLength"      									AS "Interest_DueSchedulePeriodLength",
		inte."DueSchedulePeriodTimeUnit"      									AS "Interest_DueScheduleTimeUnit",
		inte."DueScheduleBusinessCalendar"      								AS "Interest_DueScheduleBusinessCalendar",
		inte."DueScheduleBusinessDayConvention"     							AS "Interest_DueScheduleBusinessDayConvention",
		inte."DayOfMonthOfInterestPayment"      								AS "Interest_DayOfMonthOfInterestPayment",
		inte."InterestSpecificationCategory"       							    AS "Interest_InterestSpecificationCategory",
		inte."VariableRateMin"													AS "Interest_VariableRateMin",
		inte."VariableRateMax"													AS "Interest_VariableRateMax",
		inte."Spread"       													AS "Interest_Spread",
		inte."ReferenceRateFactor"      										AS "Interest_ReferenceRateFactor",
		inte."ResetCutoffLength"        										AS "Interest_ResetCutoffLength",
		inte."ResetCutoffTimeUnit"      										AS "Interest_ResetCutoffTimeUnit",
		inte."ResetPrecision"      												AS "Interest_ResetPrecision",
		inte."ResetRounding"       												AS "Interest_ResetRounding",
		inte."FixingRateSpecificationCategory"      							AS "Interest_FixingRateSpecificationCategory",
		inte."ResetPeriodLength"        										AS "Interest_ResetPeriodLength",
		inte."ResetPeriodTimeUnit"      										AS "Interest_ResetPeriodTimeUnit",
		inte."FirstRegularFloatingRateResetDate"        						AS "Interest_FirstRegularFloatingRateResetDate",
		inte."ResetBusinessCalendar"        									AS "Interest_ResetBusinessCalendar",
		inte."ResetBusinessDayConvention"       								AS "Interest_ResetBusinessDayConvention",
		inte."RelativeToInterestPeriodStartOrEnd"								AS "Interest_RelativeToInterestPeriodStartOrEnd",
		inte."ResetLagLength"       											AS "Interest_ResetLagLength",
		inte."ResetLagTimeUnit"     											AS "Interest_ResetLagTimeUnit",
		inte."DayCountConvention"       										AS "Interest_DayCountConvention",
		inte."FixedRate"        												AS "Interest_FixedRate",
		inte."InterestSubPeriodSpecificationCategory"							AS "Interest_InterestSubPeriodSpecificationCategory",
        inte."ResetInArrears"													AS "Interest_ResetInArrears",
		inte."ResetAtMonthUltimo"												AS "Interest_ResetAtMonthUltimo",
		inte."CutoffRelativeToDate"												AS "Interest_CutoffRelativeToDate",
		
		/*-----iscale = Identifier for ScaleInterval_View ------*/
		iscale."BusinessValidFrom"      										AS "Interest_ScaleInterval_BusinessValidFrom",
		iscale."BusinessValidTo"        										AS "Interest_ScaleInterval_BusinessValidTo",
		iscale."SystemValidFrom"        										AS "Interest_ScaleInterval_SystemValidFrom",
		iscale."SystemValidTo"      											AS "Interest_ScaleInterval_SystemValidTo",
		iscale."ScalingMethod"     												AS "Interest_ScaleInterval_ScalingMethod",
		iscale."ScaleBaseType"     												AS "Interest_ScaleInterval_ScaleBaseType",
		iscale."ScaleBaseAggregationMethod"        								AS "Interest_ScaleInterval_ScaleBaseAggregationMethod",
		iscale."LowerBoundaryAmount"       										AS "Interest_ScaleInterval_LowerBoundaryAmount",
		iscale."UpperBoundaryAmount"       										AS "Interest_ScaleInterval_UpperBoundaryAmount",
		iscale."BoundaryAmountCurrency"        									AS "Interest_ScaleInterval_BoundaryAmountCurrency",
		iscale."LowerBoundaryCount"     										AS "Interest_ScaleInterval_LowerBoundaryCount",
		iscale."UpperBoundaryCount"     										AS "Interest_ScaleInterval_UpperBoundaryCount",
	
		/*-----paym = Identifier for PaymentSchedule_View ------*/	
		paym."SequenceNumber"       											AS "PaymentSchedule_SequenceNumber",
		paym."BusinessValidFrom"        										AS "PaymentSchedule_BusinessValidFrom",
		paym."BusinessValidTo"      											AS "PaymentSchedule_BusinessValidTo",
		paym."SystemValidFrom"											        AS "PaymentSchedule_SystemValidFrom",
		paym."SystemValidTo"											        AS "PaymentSchedule_SystemValidTo",
		paym."PaymentScheduleStartDate"									        AS "PaymentSchedule_PaymentScheduleStartDate",
		paym."PaymentScheduleType"										        AS "PaymentSchedule_PaymentScheduleType",
		paym."BusinessDayConvention"									        AS "PaymentSchedule_BusinessDayConvention",
		paym."PaymentScheduleCategory"      									AS "PaymentSchedule_PaymentScheduleCategory",
		paym."FirstPaymentDate"											        AS "PaymentSchedule_FirstPaymentDate",
		paym."LastPaymentDate"      											AS "PaymentSchedule_LastPaymentDate",
		paym."InstallmentPeriodLength"										    AS "PaymentSchedule_InstallmentPeriodLength",
		paym."InstallmentPeriodTimeUnit"								        AS "PaymentSchedule_InstallmentPeriodTimeUnit",
		paym."DayOfMonthOfPayment"										        AS "PaymentSchedule_DayOfMonthOfPayment",
		paym."InstallmentAmount"										        AS "PaymentSchedule_InstallmentAmount",
		paym."InstallmentCurrency"										        AS "PaymentSchedule_InstallmentCurrency",
		paym."InstallmentsRelativeToInterestPaymentDates"				        AS "PaymentSchedule_InstallmentsRelativeToInterestPaymentDates",
		paym."InstallmentLagPeriodLength"								        AS "PaymentSchedule_InstallmentLagPeriodLength",
		paym."InstallmentLagTimeUnit"									        AS "PaymentSchedule_InstallmentLagTimeUnit",
		paym."BulletPaymentDate"										        AS "PaymentSchedule_BulletPaymentDate",
		paym."BulletPaymentAmount"										        AS "PaymentSchedule_BulletPaymentAmount",
		paym."BulletPaymentCurrency"									        AS "PaymentSchedule_BulletPaymentCurrency",

		/*-----agrd = Identifier for AgreedLimit_View ------*/	
		agrd."SequenceNumber"											        AS "AgreedLimit_SequenceNumber",
		agrd."BusinessValidFrom"										        AS "AgreedLimit_BusinessValidFrom",
		agrd."BusinessValidTo"											        AS "AgreedLimit_BusinessValidTo",
		agrd."SystemValidFrom"											        AS "AgreedLimit_SystemValidFrom",
		agrd."SystemValidTo"											        AS "AgreedLimit_SystemValidTo",
		agrd."LimitType"												        AS "AgreedLimit_LimitType",
		agrd."LimitValidFrom"											        AS "AgreedLimit_LimitValidFrom",
		agrd."LimitValidTo" 												    AS "AgreedLimit_LimitValidTo",
		agrd."LimitAmount"												        AS "AgreedLimit_LimitAmount",
		agrd."LimitCurrency"													AS "AgreedLimit_LimitCurrency",
		
		/*-----appl = Identifier for ApplicableInterestRate_View ------*/		
		appl."BusinessValidFrom"        										AS "ApplicableInterestRate_BusinessValidFrom",
		appl."BusinessValidTo"      											AS "ApplicableInterestRate_BusinessValidTo",
		appl."SystemValidFrom"      											AS "ApplicableInterestRate_SystemValidFrom",
		appl."SystemValidTo"        											AS "ApplicableInterestRate_SystemValidTo",
		appl."InterestType"     												AS "ApplicableInterestRate_InterestType",
		appl."Rate"     														AS "ApplicableInterestRate_Rate",
		appl."LastResetDate"        											AS "ApplicableInterestRate_LastResetDate",
		appl."NextResetDate"        											AS "ApplicableInterestRate_NextResetDate",

		/*-----canc = Identifier for CancellationOption_View ------*/
		canc."SequenceNumber"											        AS "CancellationOption_SequenceNumber",
		canc."BusinessValidFrom"										        AS "CancellationOption_BusinessValidFrom",
		canc."BusinessValidTo"													AS "CancellationOption_BusinessValidTo",
		canc."SystemValidFrom"											        AS "CancellationOption_SystemValidFrom",
		canc."SystemValidTo"											        AS "CancellationOption_SystemValidTo",
		canc."CancellationOptionCategory"								        AS "CancellationOption_CancellationOptionCategory",
		canc."CancellationOptionType"											AS "CancellationOption_CancellationOptionType",
		canc."CancellationOptionHolder"										    AS "CancellationOption_CancellationOptionHolder",
		canc."BeginOfExercisePeriod"									        AS "CancellationOption_BeginOfExercisePeriod",
		canc."EndOfExercisePeriod"											    AS "CancellationOption_EndOfExercisePeriod",
		canc."BusinessCalendar"											        AS "CancellationOption_BusinessCalendar",
		canc."BusinessDayConvention"									        AS "CancellationOption_BusinessDayConvention",
		canc."NoticePeriodLength"										        AS "CancellationOption_NoticePeriodLength",
		canc."NoticePeriodTimeUnit"											    AS "CancellationOption_NoticePeriodTimeUnit",

		/*-----fee = Identifier for Fee_View ------*/
		fee."SequenceNumber"        											AS "Fee_SequenceNumber",
		fee."BusinessValidFrom" 											    AS "Fee_BusinessValidFrom",
		fee."BusinessValidTo"											        AS "Fee_BusinessValidTo",
		fee."SystemValidFrom"											        AS "Fee_SystemValidFrom",
		fee."SystemValidTo"												        AS "Fee_SystemValidTo",
		fee."FeeType"													        AS "Fee_FeeType",
		fee."RoleOfPayer"													    AS "Fee_RoleOfPayer",
		fee."FeeChargingStartDate"										        AS "Fee_FeeChargingStartDate",
		fee."FeeChargingEndDate"										        AS "Fee_FeeChargingEndDate",
		fee."FeeCalculationMethod"										        AS "Fee_FeeCalculationMethod",
		fee."FeeAmount"													        AS "Fee_FeeAmount",
		fee."FeeCurrency"												        AS "Fee_FeeCurrency",
		fee."FeePercentage"												        AS "Fee_FeePercentage",
		fee."FeeAssessmentBase"											        AS "Fee_FeeAssessmentBaseAggregation",
		fee."FeeAssessmentBaseAggregationMethod"						        AS "Fee_FeeAssessmentBaseAggregationMethod",
		fee."FeeCategory"												        AS "Fee_FeeCategory",
		fee."FirstFeeBillingDate"										        AS "Fee_FirstFeeBillingDate",
		fee."FirstRegularFeeBillingDate"									    AS "Fee_FirstRegularFeeBillingDate",
		fee."LastFeeBillingDate"										        AS "Fee_LastFeeBillingDate",
		fee."RecurringFeePeriodLength"									        AS "Fee_RecurringFeePeriodLength",
		fee."ReccurringFeePeriodTimeUnit"										AS "Fee_ReccurringFeePeriodTimeUnit",
		fee."DayOfMonthOfFeeBillingDate"								        AS "Fee_DayOfMonthOfFeeBillingDate",
		fee."BusinessDayConvention"												AS "Fee_BusinessDayConvention",
		fee."BusinessCalendar"											        AS "Fee_BusinessCalendar",
		fee."EventTriggerType"											        AS "Fee_EventTriggerType",

		/*-----earl = Identifier for EarlyRepaymentRule_View ------*/
		earl."ASSOC_Loan_FinancialContractID"									AS "EarlyRepaymentRule_Loan_FinancialContractID",
		earl."SequenceNumber"											        AS "EarlyRepaymentRule_SequenceNumber",
		earl."BusinessValidFrom"										        AS "EarlyRepaymentRule_BusinessValidFrom",
		earl."BusinessValidTo"											        AS "EarlyRepaymentRule_BusinessValidTo",
		earl."SystemValidFrom"											        AS "EarlyRepaymentRule_SystemValidFrom",
		earl."SystemValidTo"											        AS "EarlyRepaymentRule_SystemValidTo",
		earl."EarlyRepaymentStartDate"								        	AS "EarlyRepaymentRule_EarlyRepaymentStartDate",
		earl."EarlyRepaymentEndDate"											AS "EarlyRepaymentRule_EarlyRepaymentEndDate",
		earl."NoticePeriodLength"										        AS "EarlyRepaymentRule_NoticePeriodLength",
		earl."NoticePeriodTimeUnit"									        	AS "EarlyRepaymentRule_NoticePeriodTimeUnit",
		earl."PrepaymentAmountCurrency"								        	AS "EarlyRepaymentRule_PrepaymentAmountCurrency",
		earl."MinimumPrepaymentRate"											AS "EarlyRepaymentRule_MinimumPrepaymentRate",
		earl."MaximumPrepaymentRate"											AS "EarlyRepaymentRule_MaximumPrepaymentRate",

		/*-----cov = Identifier for FinancialCovenant_View ------*/
		cov."SequenceNumber"											        AS "FinancialCovenant_SequenceNumber",
		cov."BusinessValidFrom"											        AS "FinancialCovenant_BusinessValidFrom",
		cov."BusinessValidTo"											        AS "FinancialCovenant_BusinessValidTo",
		cov."SystemValidFrom"											        AS "FinancialCovenant_SystemValidFrom",
		cov."SystemValidTo"												        AS "FinancialCovenant_SystemValidTo",
		cov."CovenantType"												        AS "FinancialCovenant_CovenantType",
		cov."Criterion"													        AS "FinancialCovenant_Criterion",
		cov."CriterionApplicableFrom"									        AS "FinancialCovenant_CriterionApplicableFrom",
		cov."CriterionApplicableTo"										        AS "FinancialCovenant_CriterionApplicableTo",
		cov."MonitoringPeriodLength"									        AS "FinancialCovenant_MonitoringPeriodLength",
		cov."MonitoringPeriodLengthTimeUnit"							        AS "FinancialCovenant_MonitoringPeriodLengthTimeUnit",
		cov."FirstRegularMonitoringDate"								        AS "FinancialCovenant_FirstRegularMonitoringDate",
		cov."CovenantBreachEffectType"									        AS "FinancialCovenant_CovenantBreachEffectType",
		cov."CovenantFulfilledEffectType"								        AS "FinancialCovenant_CovenantFulfilledEffectType",
		cov."LowerBoundary"												        AS "FinancialCovenant_LowerBoundary",
		cov."UpperBoundary"												        AS "FinancialCovenant_UpperBoundary",
		/*-----ref = Identifier for RefinancingRelation_View ------*/
		ref."RefinancingType"											        AS "RefinancingRelation_RefinancingType",
		ref."BusinessValidFrom"											        AS "RefinancingRelation_BusinessValidFrom",
		ref."BusinessValidTo"											        AS "RefinancingRelation_BusinessValidTo",
		ref."SystemValidFrom"											        AS "RefinancingRelation_SystemValidFrom",
		ref."SystemValidTo"												        AS "RefinancingRelation_SystemValidTo",
		ref."RefinancedAmountInPositionCurrency"						        AS "RefinancingRelation_RefinancedAmountInPositionCurrency",
		ref."PositionCurrency"											        AS "RefinancingRelation_PositionCurrency",

		/*-----net = Identifier for NettingSetContractAssignment_View ------*/
		net."_NettingSet_NettingSetID"                                  		AS "NettingSetContractAssignment_NettingSet_NettingSetID",
		net."BusinessValidFrom"                                         		AS "NettingSetContractAssignment_BusinessValidFrom",
		net."BusinessValidTo"                                           		AS "NettingSetContractAssignment_BusinessValidTo",
		net."SystemValidFrom"                                           		AS "NettingSetContractAssignment_SystemValidFrom",
		net."SystemValidTo"                                             		AS "NettingSetContractAssignment_SystemValidTo",

		/*-----ica = Identifier for IndustryClassAssignment_View ------*/
		ica."ASSOC_IndustryClass_Industry"										AS "IndustryClassAssignment_Industry",
		ica."ASSOC_IndustryClass_IndustryClassificationSystem"					AS "IndustryClassAssignment_IndustryClassificationSystem",
		
				/*----Indicator wheter loan is active or passive--*/
		CASE
		    WHEN bpca."ContractDataOwner" = true  --Borrower role
                THEN TO_NVARCHAR('FALSE')
		    WHEN bpcl."ContractDataOwner" = true  --Lender role
		        THEN TO_NVARCHAR('TRUE')
		    ELSE
		        TO_NVARCHAR('')
        END AS "IsActive"
        
    FROM "com.adweko.adapter.osx.inputdata.loans::Loan_DatagroupAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP , :I_DATAGROUP_STRING , 'Loan'  ) AS fc
    	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS csd ON 
                csd."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND csd."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND csd."ContractStatusCategory"								= 'DefaultStatus'
 
    	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS csp ON
    			csp."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND csp."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND csp."ContractStatusCategory"								= 'PastDueStatus'	
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS pca ON 
            	pca."IDSystem"							= fc."IDSystem" 
            AND pca."FinancialContractID"				= fc."FinancialContractID"     
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpca ON 
                bpca."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND bpca."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND bpca."Role"											        = 'Borrower'   
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcl ON 
                bpcl."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND bpcl."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND bpcl."Role"											        = 'Lender'   
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bpcac ON
				bpcac."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND bpcac."ASSOC_FinancialContract_IDSystem"					= fc."IDSystem"
			AND bpcac."Role"											    = 'ClearingMember'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS ouca ON 
                ouca."ASSOC_Contract_FinancialContractID"					= fc."FinancialContractID" 
            AND ouca."ASSOC_Contract_IDSystem"								= fc."IDSystem"
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Interest_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS inte ON 
	            inte."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"  
	        AND inte."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
	        AND inte."InterestType"											= 'LendingInterest'
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ScaleInterval_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS iscale ON 
				iscale."ASSOC_InterestSpecification_ASSOC_FinancialContract_IDSystem"				= inte."ASSOC_FinancialContract_IDSystem"
	        AND iscale."ASSOC_InterestSpecification_ASSOC_FinancialContract_FinancialContractID"	= inte."ASSOC_FinancialContract_FinancialContractID" 
	        AND iscale."ASSOC_InterestSpecification_SequenceNumber"  								= inte."SequenceNumber" 
	    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::PaymentSchedule_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS paym ON 
                paym."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"                    
            AND paym."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"   	 
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::AgreedLimit_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS agrd ON 
	            agrd."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem" 
	        AND agrd."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ApplicableInterestRate_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS appl ON 
	            appl."ASSOC_FinancialContract_IDSystem"				        = fc."IDSystem" 
	        AND appl."ASSOC_FinancialContract_FinancialContractID"          = fc."FinancialContractID"
	        AND appl."InterestType"											= 'LendingInterest'
	     --   AND appl."InterestSequenceNumber"								= inte."SequenceNumber"
	    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::CancellationOption_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS canc ON 
                canc."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem" 
            AND canc."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"  
       	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Fee_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS fee ON 
                fee."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem" 
            AND fee."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::EarlyRepaymentRule_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS earl ON 
                earl."ASSOC_Loan_IDSystem"									= fc."IDSystem" 
            AND earl."ASSOC_Loan_FinancialContractID"						= fc."FinancialContractID"          
       	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::FinancialCovenant_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS cov ON 
                cov."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem" 
            AND cov."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"                         
       	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::RefinancingRelation_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS ref ON 
                ref."ASSOC_RefinancedContract_IDSystem"						= fc."IDSystem" 
            AND ref."ASSOC_RefinancedContract_FinancialContractID"			= fc."FinancialContractID" 		
       	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::NettingSetContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS net ON
       		    net."_FinancialContract_IDSystem"							= fc."IDSystem"
            AND net."_FinancialContract_FinancialContractID"				= fc."FinancialContractID"
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::IndustryClassAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS ica ON
	    		ica."ASSOC_BusinessPartner_BusinessPartnerID"					= bpca."ASSOC_PartnerInParticipation_BusinessPartnerID"
	    			AND ica."ASSOC_IndustryClass_IndustryClassificationSystem"	= 'NACE'
							
    WHERE 		fc."FinancialContractCategory"		=  'Loan'
    AND fc."_TrancheInSyndication__SyndicationAgreement_FinancialContractID" IS NULL