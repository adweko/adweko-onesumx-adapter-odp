VIEW "com.adweko.adapter.osx.inputdata.loans::Loan_SecuredPercentage"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
AS
    SELECT
    	count(*)																AS "NumberOfRows",
       	BV_fc."FinancialContractID"												AS "FinancialContractID",
    	BV_fc."IDSystem"														AS "IDSystem",
       	"com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard"
       		(MAX(BV_cp."CollateralPortion_CoveredPercentageOfObligations")
       		, 'SecuredPercentage'
       		, kv_ps."Value") 
       	AS "SecuredPercentage"

	FROM "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP)  AS BV_fc

		INNER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralPortionContractAssignment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP)  AS BV_cpca ON
				BV_cpca."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"		= BV_fc."FinancialContractID"
			AND	BV_cpca."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem"					= BV_fc."IDSystem"

		INNER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralPortion"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP)  AS BV_cp ON
				BV_cp."CollateralPortion_PortionNumber" 							= BV_cpca."CollateralPortionContractAssignment_PortionNumber"
			AND BV_cp."CollateralPortion_CollateralAgreement_FinancialContractID"	= BV_cpca."CollateralPortionContractAssignment_CollateralAgreement_FinancialContractID"
			AND BV_cp."CollateralPortion_CollateralAgreement_IDSystem"				= BV_cpca."CollateralPortionContractAssignment_CollateralAgreement_IDSystem"
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv_ps
			ON  kv_ps."KeyID" = 'SAPPercentageStandard'

	GROUP BY BV_fc."FinancialContractID",
	BV_fc."IDSystem",
	kv_ps."Value"
	HAVING count(*) = 1

	UNION

	SELECT
    	count(*)																AS "NumberOfRows",
       	BV_fc."FinancialContractID"												AS "FinancialContractID",
    	BV_fc."IDSystem"														AS "IDSystem",
       	NULL																	AS "SecuredPercentage"

	FROM "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP)  AS BV_fc

		INNER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralPortionContractAssignment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP)  AS BV_cpca ON
				BV_cpca."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"		= BV_fc."FinancialContractID"
			AND	BV_cpca."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem"					= BV_fc."IDSystem"

		INNER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralPortion"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP)  AS BV_cp ON
				BV_cp."CollateralPortion_PortionNumber" 							= BV_cpca."CollateralPortionContractAssignment_PortionNumber"
			AND BV_cp."CollateralPortion_CollateralAgreement_FinancialContractID"	= BV_cpca."CollateralPortionContractAssignment_CollateralAgreement_FinancialContractID"
			AND BV_cp."CollateralPortion_CollateralAgreement_IDSystem"				= BV_cpca."CollateralPortionContractAssignment_CollateralAgreement_IDSystem"

	GROUP BY BV_fc."FinancialContractID", BV_fc."IDSystem"
	HAVING count(*) <> 1