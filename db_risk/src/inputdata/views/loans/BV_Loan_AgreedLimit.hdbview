view "com.adweko.adapter.osx.inputdata.loans::BV_Loan_AgreedLimit"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_AGREED_LIMIT_STREAM_NAME NVARCHAR(40)
    )
AS
	SELECT
		mb."FinancialContractID",
		mb."IDSystem",
		mb."ContractCurrency",
		agrd."SequenceNumber"									AS "AgreedLimit_SequenceNumber",
		agrd."IsUnconditionallyRevocable"						AS "AgreedLimit_IsUnconditionallyRevocable",
		agrd."AcceptanceDate"									AS "AgreedLimit_AcceptanceDate",
		agrd."LimitCurrency"									AS "AgreedLimit_LimitCurrency",
		agrd."LimitValidFrom"									AS "AgreedLimit_LimitValidFrom",
		agrd."LimitValidTo"										AS "AgreedLimit_LimitValidTo",
		agrd."LimitAmount"										AS "AgreedLimit_LimitAmount",
		agrd."IsRevolving"										AS "AgreedLimit_IsRevolving",
		"com.adweko.adapter.osx.inputdata.common::get_contractID"(
			'F', 
			'',
			mb."FinancialContractID" || '#LIM',
			mb."IDSystem",
			''
		) AS "contractID"
	
	FROM "com.adweko.adapter.osx.inputdata.common::get_PriorityMonetaryBalance"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_AGREED_LIMIT_STREAM_NAME) mb
	
	INNER JOIN "com.adweko.adapter.osx.synonyms::AgreedLimit_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) agrd
		ON	agrd."ASSOC_FinancialContract_FinancialContractID"	= mb."FinancialContractID"
		AND agrd."ASSOC_FinancialContract_IDSystem"				= mb."IDSystem"
		AND agrd."LimitType"									= 'LoanCommitment'
		AND agrd."LimitCurrency"								= mb."BalanceCurrency"
		AND agrd."LimitAmount"									> 0
		AND agrd."LimitValidFrom"								<= :I_BUSINESS_DATE
		AND agrd."LimitValidTo" 								> :I_BUSINESS_DATE
		
	WHERE 
		mb."MonetaryBalanceCategory"							= 'LoanBalance'