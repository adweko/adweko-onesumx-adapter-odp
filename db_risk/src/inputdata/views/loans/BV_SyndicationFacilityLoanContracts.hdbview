VIEW "com.adweko.adapter.osx.inputdata.loans::BV_SyndicationFacilityLoanContracts"(
	IN I_BUSINESS_DATE DATE,
	IN I_SYSTEM_TIMESTAMP TIMESTAMP,
	IN SumActiveOrPassiv NVARCHAR(10)
	    )
AS
    SELECT
    fc."FinancialContractID"	AS "Facility_FinancialContractID",
    fc."IDSystem"				AS	"Facility_IDSystem",
	--passiv."PassivLoan_ApprovedNominalAmount",
	SUM(ABS(activepassiv."PassivLoan_ApprovedNominalAmount")) AS "SUM_Loans_ApprovedNominalAmount",
	MAP(activepassiv."synMarginCalcSwitch",
		'ApprovedNominalAmount',SUM(ABS(activepassiv."PassivLoan_ApprovedNominalAmount")),
		'LimitAmount',SUM(ABS(activepassiv."PassivLoan_LimitAmount"))
	) AS "SUM_Loans_Amount"
    FROM "com.adweko.adapter.osx.synonyms::FinancialContract_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) as fc
    --join passiv loan on this Facility to get the Nominal Value
    LEFT JOIN(
    SELECT 
    fc."FinancialContractID" AS "PassivLoan_FinancialContractID",
    fc."IDSystem"			AS "PassivLoan_IDSystem",
    fc."ASSOC_FacilityOfDrawing_FinancialContractID",
    fc."ASSOC_FacilityOfDrawing_IDSystem",
    fc."ApprovedNominalAmount"	AS "PassivLoan_ApprovedNominalAmount",
    agrd."AgreedLimit_LimitAmount" AS "PassivLoan_LimitAmount",
    synMarginCalcSwitch."Value" AS "synMarginCalcSwitch"
	    
    FROM "com.adweko.adapter.osx.synonyms::FinancialContract_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) as fc		
	INNER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bp  ON 
		bp."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
	AND bp."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
	AND bp."ContractDataOwner"										= true
	AND bp."Role"													= MAP(:SumActiveOrPassiv,
																			'Passiv','Borrower',
																			'Active','Lender')
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS synMarginSwitch
		ON  synMarginSwitch."KeyID" = 'syndicatedMarginAtLoanOrAgreedLimitLevel'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS synMarginCalcSwitch
		ON  synMarginCalcSwitch."KeyID" = 'syndicatedMarginCalculationSwitch'
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::BV_Loan_AgreedLimit"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,'AgreedLimitSyndicatedLoan') agrd
    	ON agrd."FinancialContractID"	= fc."FinancialContractID" 
    	AND agrd."IDSystem"				= fc."IDSystem"
    	AND synMarginSwitch."Value" 	= '2'  -- applicable if syndicatedMargin at the level of AgreedLimitFacility
    	AND synMarginCalcSwitch."Value"	= 'LimitAmount'
    	
	WHERE fc."FinancialContractCategory" = 'Loan') as activepassiv
		ON activepassiv."ASSOC_FacilityOfDrawing_FinancialContractID" = fc."FinancialContractID"
		AND activepassiv."ASSOC_FacilityOfDrawing_IDSystem" = fc."IDSystem"
    WHERE fc."FinancialContractCategory" = 'Facility'
    GROUP BY fc."FinancialContractID",
    		fc."IDSystem",
    		activepassiv."synMarginCalcSwitch";