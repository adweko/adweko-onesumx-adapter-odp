VIEW "com.adweko.adapter.osx.inputdata.amortizingShortGuarantee::AmortizingShortGuaranteeUnderlying_DatagroupAssignment_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."IDSystem"															AS "FinancialContract_IDSystem",
        fc."FinancialContractID"												AS "FinancialContract_FinancialContractID",
        fc."OriginalSigningDate"												AS "FinancialContract_OriginalSigningDate",
        fc."TerminationDate"													AS "FinancialContract_TerminationDate",
        fc."LifecycleStatus"													AS "FinancialContract_LifecycleStatus",
        fc."LifecycleStatusChangeDate"											AS "FinancialContract_LifecycleStatusChangeDate",
        fc."FacilityEndDate"													AS "FinancialContract_FacilityEndDate",
        fc."GoverningLawCountry"												AS "FinancialContract_GoverningLawCountry",
		dg."dataGroup"

    FROM "com.adweko.adapter.osx.inputdata.amortizingShortGuarantee::AmortizingShortGuaranteeUnderlying_Datagroup_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) dg
    
    INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) fc
    	ON	fc."FinancialContractCategory"										= dg."FinancialContractCategory"
    	AND fc."FinancialContractType"											= dg."FinancialContractType"
    
    INNER JOIN "com.adweko.adapter.osx.synonyms::PaymentSchedule_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) paym
    	ON paym."ASSOC_FinancialContract_IDSystem"								= fc."IDSystem"
    	AND paym."ASSOC_FinancialContract_FinancialContractID"					= fc."FinancialContractID"
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ProductCatalogAssignment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pc
    	ON	pc."ProductCatalogAssignment_FinancialContract_FinancialContractID" = fc."FinancialContractID" 
    	AND pc."ProductCatalogAssignment_FinancialContract_IDSystem"			= fc."IDSystem"
   		AND pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem" = dg."ProductCatalogItem"
    	AND pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID"	= dg."ProductCatalog_CatalogID"
    	
   WHERE	IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",'')			= IFNULL(dg."ProductCatalogItem",'')
		AND IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID",'')	= IFNULL(dg."ProductCatalog_CatalogID",'')
    	AND (dg."IDSystem" IS NULL OR dg."IDSystem" = fc."IDSystem")
    
    GROUP BY fc."IDSystem",
    	fc."FinancialContractID",
    	fc."OriginalSigningDate",
    	fc."TerminationDate",
    	fc."LifecycleStatus",
    	fc."LifecycleStatusChangeDate",
    	fc."FacilityEndDate",
    	fc."GoverningLawCountry",
    	dg."dataGroup"