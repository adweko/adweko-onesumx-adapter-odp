view "com.adweko.adapter.osx.inputdata.account::BV_Account_InterestPrio_generalCase"
    ( IN I_BUSINESS_DATE DATE, 
      IN I_SYSTEM_TIMESTAMP TIMESTAMP)
as
        /*------ 1) GENERAL CASE ------*/
        select
        /*------ fc = Identifier for FinancialContract ------*/
        fc."IDSystem"									AS "FinancialContract_IDSystem",
    	fc."FinancialContractID"						AS "FinancialContract_FinancialContractID",
		
		/*------ inte = Identifier for Interest ------*/
		sinte."SequenceNumber"  						AS "Interest_SequenceNumber",
		sinte."BusinessValidFrom"						AS "Interest_BusinessValidFrom",
		sinte."BusinessValidTo" 						AS "Interest_BusinessValidTo",
		sinte."SystemValidFrom" 						AS "Interest_SystemValidFrom",
		sinte."SystemValidTo"							AS "Interest_SystemValidTo",
		sinte."InterestType"							AS "Interest_InterestType",
		sinte."PayingOrReceiving"						AS "Interest_PayingOrReceiving",		
		sinte."InterestCategory"						AS "Interest_InterestCategory",
		sinte."FirstInterestPeriodStartDate"			AS "Interest_FirstInterestPeriodStartDate",
		sinte."FirstInterestPeriodEndDate"				AS "Interest_FirstInterestPeriodEndDate",		
		sinte."LastInterestPeriodEndDate"				AS "Interest_LastInterestPeriodEndDate",		
		sinte."InterestCurrency"						AS "Interest_InterestCurrency",		
		sinte."InterestPeriodLength"					AS "Interest_InterestPeriodLength",
		sinte."InterestPeriodTimeUnit"					AS "Interest_InterestPeriodTimeUnit",
		sinte."DayOfMonthOfInterestPeriodEnd"			AS "Interest_DayOfMonthOfInterestPeriodEnd",
		sinte."InterestBusinessCalendar"				AS "Interest_InterestBusinessCalendar",
		sinte."BusinessDayConvention"					AS "Interest_BusinessDayConvention",
		sinte."InterestInAdvance"						AS "Interest_InterestInAdvance",
		sinte."InterestSpecificationCategory"			AS "Interest_InterestSpecificationCategory", 
		sinte."ASSOC_ReferenceRateID_ReferenceRateID"	AS "Interest_ReferenceRateID",
		sinte."VariableRateMin"							AS "Interest_VariableRateMin",
		sinte."VariableRateMax"							AS "Interest_VariableRateMax",
		sinte."FloatingRateMin"							AS "Interest_FloatingRateMin",
		sinte."FloatingRateMax"							AS "Interest_FloatingRateMax",
		sinte."Spread"									AS "Interest_Spread",
		sinte."ReferenceRateFactor" 					AS "Interest_ReferenceRateFactor",
		sinte."ResetCutoffLength" 						AS "Interest_ResetCutoffLength",
		sinte."ResetCutoffTimeUnit" 					AS "Interest_ResetCutoffTimeUnit",
		sinte."FixingRateSpecificationCategory" 		AS "Interest_FixingRateSpecificationCategory",
		sinte."ResetPeriodLength"						AS "Interest_ResetPeriodLength",
		sinte."ResetPeriodTimeUnit" 					AS "Interest_ResetPeriodTimeUnit",
		sinte."FirstRegularFloatingRateResetDate"		AS "Interest_FirstRegularFloatingRateResetDate",
		sinte."ResetBusinessCalendar"					AS "Interest_ResetBusinessCalendar",
		sinte."ResetBusinessDayConvention"				AS "Interest_ResetBusinessDayConvention",
		sinte."ResetLagLength"							AS "Interest_ResetLagLength",
		sinte."ResetLagTimeUnit"						AS "Interest_ResetLagTimeUnit",
		sinte."DayCountConvention"						AS "Interest_DayCountConvention",
		sinte."FixedRate"								AS "Interest_FixedRate",
		sinte."InterestIsCompounded"					AS "Interest_InterestIsCompounded",		
		sinte."CompoundingConvention"					AS "Interest_CompoundingConvention",
		sinte."ResetInArrears"							AS "Interest_ResetInArrears",
		sinte."ResetAtMonthUltimo"						AS "Interest_ResetAtMonthUltimo",
		sinte."CutoffRelativeToDate"					AS "Interest_CutoffRelativeToDate",
		
		/*------ iscale = Identifier for Interest ScaleInterval ------*/	
		NULL											AS "Interest_ScaleInterval_BusinessValidFrom",		
		NULL											AS "Interest_ScaleInterval_BusinessValidTo",	
		NULL											AS "Interest_ScaleInterval_SystemValidFrom",	
		NULL											AS "Interest_ScaleInterval_SystemValidTo",	
		NULL											AS "Interest_ScaleInterval_BoundaryAmountCurrency",	
		NULL											AS "Interest_ScaleInterval_LowerBoundaryAmount",	
		NULL											AS "Interest_ScaleInterval_LowerBoundaryCount",	
		NULL											AS "Interest_ScaleInterval_ScaleBaseAggregationMethod",	
		NULL											AS "Interest_ScaleInterval_ScaleBaseType",	
		NULL											AS "Interest_ScaleInterval_ScalingMethod",	
		NULL											AS "Interest_ScaleInterval_UpperBoundaryAmount",	
		NULL											AS "Interest_ScaleInterval_UpperBoundaryCount",

		/*------ appl = Identifier for ApplicableInterestRate ------*/
		sappl."BusinessValidFrom" 						AS "ApplicableInterestRate_BusinessValidFrom",
		sappl."BusinessValidTo"							AS "ApplicableInterestRate_BusinessValidTo",
		sappl."SystemValidFrom"							AS "ApplicableInterestRate_SystemValidFrom",
		sappl."SystemValidTo" 							AS "ApplicableInterestRate_SystemValidTo",
		sappl."InterestType"							AS "ApplicableInterestRate_InterestType",
		sappl."Rate" 									AS "ApplicableInterestRate_Rate",
		sappl."LastResetDate" 							AS "ApplicableInterestRate_LastResetDate",
		sappl."NextResetDate" 							AS "ApplicableInterestRate_NextResetDate"			

	FROM "com.adweko.adapter.osx.synonyms::FinancialContract_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) fc
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Interest_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) sinte 
        ON sinte."ASSOC_FinancialContract_IDSystem"					= fc."IDSystem"
        	AND sinte."ASSOC_FinancialContract_FinancialContractID" = fc."FinancialContractID"

	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ApplicableInterestRate_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) sappl 
		ON sappl."ASSOC_FinancialContract_IDSystem"					= fc."IDSystem"
	    	AND sappl."ASSOC_FinancialContract_FinancialContractID" = fc."FinancialContractID"
	    	AND sappl."InterestSequenceNumber"						= sinte."SequenceNumber"
    
    WHERE 		fc."FinancialContractCategory"		=  'BankAccount'
    		AND	fc."AccountCategory"				<> 'SecuritiesAccount'
    		AND NOT	
    		(	
    			fc."AccountCategory"			= 'SingleCurrencyAccount'
    			AND	 
    			(
    				fc."SingleCurrencyAccountCategory"	= 'CurrentAccount' 
    				OR
    				fc."SingleCurrencyAccountCategory"	= 'SavingsAccount'
    				OR
    				fc."SingleCurrencyAccountCategory" = 'FixedTermDeposit'
    			)
    		)