view "com.adweko.adapter.osx.inputdata.account::BV_Account"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
as
        select
        /*------ fc = Identifier for FinancialContract ------*/
        fc."IDSystem"															AS "FinancialContract_IDSystem",
    	fc."FinancialContractID"												AS "FinancialContract_FinancialContractID",
        fc."BusinessValidFrom"             										AS "FinancialContract_BusinessValidFrom",
        fc."BusinessValidTo"                               						AS "FinancialContract_BusinessValidTo",
        fc."SystemValidFrom"                               						AS "FinancialContract_SystemValidFrom",
        fc."SystemValidTo"                                 						AS "FinancialContract_SystemValidTo",
		fc."AccountCategory"													AS "FinancialContract_AccountCategory",
		fc."SingleCurrencyAccountCategory"										AS "FinancialContract_SingleCurrencyAccountCategory",
		fc."FinancialContractCategory"											AS "FinancialContract_FinancialContractCategory",
		fc."OriginalSigningDate"												AS "FinancialContract_OriginalSigningDate",
		fc."OpeningDate"														AS "FinancialContract_OpeningDate",
		fc."ClosingDate"														AS "FinancialContract_ClosingDate",
		fc."Purpose"                                        					AS "FinancialContract_Purpose",
		fc."DepositStartDate"													AS "FinancialContract_DepositStartDate",
		fc."CurrentDepositEndDate"												AS "FinancialContract_CurrentDepositEndDate",
		fc."LifecycleStatus"													AS "FinancialContract_LifecycleStatus",
		fc."IntendedAssetLiability"                                             AS "FinancialContract_IntendedAssetLiability",
		fc."ForClientTrades"													AS "FinancialContract_ForClientTrades",
		/*------ csd = Identifier for ContractStatus ------*/
		csd."ContractStatusType"												AS "ContractStatus_DefaultStatus_ContractStatusType",
		csd."BusinessValidFrom"													AS "ContractStatus_DefaultStatus_BusinessValidFrom",
		csd."BusinessValidTo"													AS "ContractStatus_DefaultStatus_BusinessValidTo",
		csd."SystemValidFrom"													AS "ContractStatus_DefaultStatus_SystemValidFrom",
		csd."SystemValidTo"														AS "ContractStatus_DefaultStatus_SystemValidTo",
		csd."Status"															AS "ContractStatus_DefaultStatus_Status",
		
		/*------ csp = Identifier for ContractStatus in Category PastDueStatus-----*/
		csp."BusinessValidFrom"                         						AS "ContractStatus_PastDueStatus_BusinessValidFrom",
		csp."BusinessValidTo"                           						AS "ContractStatus_PastDueStatus_BusinessValidTo",
		csp."SystemValidFrom"                           						AS "ContractStatus_PastDueStatus_SystemValidFrom",
		csp."SystemValidTo"                             						AS "ContractStatus_PastDueStatus_SystemValidTo",
		csp."Status"                                    						AS "ContractStatus_PastDueStatus_Status",
	
		/*------ bpca = Identifier for BusinessPartnerContractAssignment ------*/
		bpca."ASSOC_PartnerInParticipation_BusinessPartnerID"					AS "BusinessPartnerContractAssignment_AccountHolder_PartnerInParticipation_BusinessPartnerID",
		bpca."BusinessValidFrom"												AS "BusinessPartnerContractAssignment_AccountHolder_BusinessValidFrom",
		bpca."BusinessValidTo"							    					AS "BusinessPartnerContractAssignment_AccountHolder_BusinessValidTo",
		bpca."SystemValidFrom"													AS "BusinessPartnerContractAssignment_AccountHolder_SystemValidFrom",
		bpca."SystemValidTo"													AS "BusinessPartnerContractAssignment_AccountHolder_SystemValidTo",
		
		/*------ bpcac = Identifier for BusinessPartnerContractAssignment ------*/
		bpcac."ASSOC_PartnerInParticipation_BusinessPartnerID"     				AS "BusinessPartnerContractAssignment_ClearingMember_PartnerInParticipation_BusinessPartnerID",
		bpcac."BusinessValidFrom"                            					AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidFrom",
		bpcac."BusinessValidTo"                              					AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidTo",
		bpcac."SystemValidFrom"                              					AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidFrom",
		bpcac."SystemValidTo"                                					AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidTo",

		/*------ ouca = Identifier for OrganizationalUnitContractAssignment ------*/
		ouca."RoleOfOrganizationalUnit" 												    AS "OrganizationalUnitContractAssignment_RoleOfOrganizationalUnit",
		ouca."ASSOC_OrgUnit_OrganizationalUnitID"											AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationalUnitID",
		ouca."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID"  AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationHostingOrganizationalUnit_BusinessPartnerID",
		ouca."BusinessValidFrom"														    AS "OrganizationalUnitContractAssignment_BusinessValidFrom",
		ouca."BusinessValidTo"							    							    AS "OrganizationalUnitContractAssignment_BusinessValidTo",
		ouca."SystemValidFrom"															    AS "OrganizationalUnitContractAssignment_SystemValidFrom",
		ouca."SystemValidTo"															    AS "OrganizationalUnitContractAssignment_SystemValidTo",
		
		/*------ paym = Identifier for PaymentSchedule ------*/
		paym."SequenceNumber"													AS "PaymentSchedule_SequenceNumber",
		paym."BusinessValidFrom"												AS "PaymentSchedule_BusinessValidFrom",
		paym."BusinessValidTo"													AS "PaymentSchedule_BusinessValidTo",
		paym."SystemValidFrom"													AS "PaymentSchedule_SystemValidFrom",
		paym."SystemValidTo"													AS "PaymentSchedule_SystemValidTo",
		paym."PaymentScheduleType"												AS "PaymentSchedule_PaymentScheduleType",
		paym."BusinessDayConvention"											AS "PaymentSchedule_BusinessDayConvention",
		paym."PaymentScheduleCategory"											AS "PaymentSchedule_PaymentScheduleCategory",
		paym."FirstPaymentDate" 												AS "PaymentSchedule_FirstPaymentDate",
		paym."LastPaymentDate"													AS "PaymentSchedule_LastPaymentDate",
		paym."InstallmentPeriodLength"											AS "PaymentSchedule_InstallmentPeriodLength",
		paym."InstallmentPeriodTimeUnit"										AS "PaymentSchedule_InstallmentPeriodTimeUnit",
		paym."InstallmentAmount"												AS "PaymentSchedule_InstallmentAmount",
		paym."InstallmentCurrency"												AS "PaymentSchedule_InstallmentCurrency",
		paym."InstallmentsRelativeToInterestPaymentDates"						AS "PaymentSchedule_InstallmentsRelativeToInterestPaymentDates",
		paym."InstallmentLagPeriodLength"										AS "PaymentSchedule_InstallmentLagPeriodLength",
		paym."InstallmentLagTimeUnit"											AS "PaymentSchedule_InstallmentLagTimeUnit",
		paym."BulletPaymentDate"												AS "PaymentSchedule_BulletPaymentDate",
		paym."BulletPaymentAmount"												AS "PaymentSchedule_BulletPaymentAmount",
		paym."BulletPaymentCurrency"											AS "PaymentSchedule_BulletPaymentCurrency",
		
		/*------ canc= Identifier for CancellationOption ------*/
		canc."SequenceNumber"													AS "CancellationOption_SequenceNumber",
		canc."BusinessValidFrom"												AS "CancellationOption_BusinessValidFrom",
		canc."BusinessValidTo"													AS "CancellationOption_BusinessValidTo",
		canc."SystemValidFrom"													AS "CancellationOption_SystemValidFrom",
		canc."SystemValidTo"													AS "CancellationOption_SystemValidTo",
		canc."CancellationOptionCategory"										AS "CancellationOption_CancellationOptionCategory",
		canc."CancellationOptionType"											AS "CancellationOption_CancellationOptionType",
		canc."CancellationOptionHolder"											AS "CancellationOption_CancellationOptionHolder",
		canc."BeginOfExercisePeriod"											AS "CancellationOption_BeginOfExercisePeriod",
		canc."EndOfExercisePeriod"												AS "CancellationOption_EndOfExercisePeriod",
		canc."BusinessCalendar"													AS "CancellationOption_BusinessCalendar",
		canc."BusinessDayConvention"											AS "CancellationOption_BusinessDayConvention",
		canc."NoticePeriodLength"												AS "CancellationOption_NoticePeriodLength",
		canc."NoticePeriodTimeUnit"												AS "CancellationOption_NoticePeriodTimeUnit",
		
		/*------ fee = Identifier for Fee ------*/
		
		fee."SequenceNumber"													AS "Fee_SequenceNumber",
		fee."BusinessValidFrom" 												AS "Fee_BusinessValidFrom", 
		fee."BusinessValidTo"													AS "Fee_BusinessValidTo",  
		fee."FeeType"															AS "Fee_FeeType",
		fee."RoleOfPayer"														AS "Fee_RoleOfPayer",
		fee."FeeChargingStartDate"												AS "Fee_FeeChargingStartDate",
		fee."FeeChargingEndDate"												AS "Fee_FeeChargingEndDate",
		fee."FeeCalculationMethod"												AS "Fee_FeeCalculationMethod", 
		fee."FeeAmount"															AS "Fee_FeeAmount",
		fee."FeeCurrency"														AS "Fee_FeeCurrency",
		fee."FeePercentage"														AS "Fee_FeePercentage",
		fee."FeeAssessmentBaseAggregationMethod"								AS "Fee_FeeAssessmentBaseAggregationMethod",
		fee."FeeCategory"														AS "Fee_FeeCategory",
		fee."FirstFeeBillingDate"												AS "Fee_FirstFeeBillingDate",
		fee."FirstRegularFeeBillingDate"										AS "Fee_FirstRegularFeeBillingDate",
		fee."LastFeeBillingDate"												AS "Fee_LastFeeBillingDate",
		fee."RecurringFeePeriodLength"											AS "Fee_RecurringFeePeriodLength",
		fee."ReccurringFeePeriodTimeUnit"										AS "Fee_ReccurringFeePeriodTimeUnit",
		fee."DayOfMonthOfFeeBillingDate"										AS "Fee_DayOfMonthOfFeeBillingDate",
		fee."BusinessDayConvention"												AS "Fee_BusinessDayConvention",
		fee."BusinessCalendar"													AS "Fee_BusinessCalendar",
		
		/*-----pca = Identifier for ProductCatalogAssignment_View ------*/
		'FSDMStandard'										    						as "ProductCatalogAssignment_ProductCatalogItem_ProductCatalog_CatalogID",
		pca."StandardCatalog_ProductCatalogItem"        								as "ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",
		pca."StandardCatalog_ProductCatalogItem",
        pca."CustomCatalog_ProductCatalogItem",
		pca."BusinessValidFrom"     													as "ProductCatalogAssignment_BusinessValidFrom",
		pca."BusinessValidTo"       													as "ProductCatalogAssignment_BusinessValidTo",
		
		/*-----ccs = Identifier for ContractChannelStatus_View_View ------*/
		ccs."ContractChannelStatus"                                             AS "ContractChannelStatus_ContractChannelStatus",
		ccs."StatusChangeReason"                                                AS "ContractChannelStatus_StatusChangeReason",
		ccs."BusinessValidFrom"                                                 AS "ContractChannelStatus_BusinessValidFrom",
		ccs."BusinessValidTo"                                                   AS "ContractChannelStatus_BusinessValidTo",
		ccs."SystemValidFrom"                                                   AS "ContractChannelStatus_SystemValidFrom",
		ccs."SystemValidTo"                                                     AS "ContractChannelStatus_SystemValidTo",


		/*-----bch = Identifier for BankingChannel_View ------*/
		bch."BankingChannelID"                                                  AS "BankingChannel_BankingChannelID",
		bch."Description"                                                       AS "BankingChannel_Description",
		bch."BankingChannelCategory"                                            AS "BankingChannel_BankingChannelCategory",
		bch."BusinessValidFrom"                                                 AS "BankingChannel_BusinessValidFrom",
		bch."BusinessValidTo"                                                   AS "BankingChannel_BusinessValidTo",
		bch."SystemValidFrom"                                                   AS "BankingChannel_SystemValidFrom",
		bch."SystemValidTo"                                                     AS "BankingChannel_SystemValidTo",
		
		/*-----bch = Identifier for DigitalBankingPlatformAccess_View ------*/		
		dbpa."Status"															AS "DigitalBankingPlatform_Status",
 		
		
		/*---------------- Single-/Multi Currency Accounts----------------------*/
		
		/*------ pcmca = Identifier for PositionCurrencyOfMultiCurrencyContract ------*/

		case when pcmca."PositionCurrency" is NULL then fc."AccountCurrency"		else pcmca."PositionCurrency" end	AS "Calc_Account_PositionCurrency",
		
		/*------ agrd = Identifier for AgreedLimit ------*/
		
        case when pcmca."PositionCurrency" is NULL then sagrd."SequenceNumber"		else agrd."SequenceNumber" end		AS "AgreedLimit_SequenceNumber",
		case when pcmca."PositionCurrency" is NULL then sagrd."BusinessValidFrom"	else agrd."BusinessValidFrom" end	AS "AgreedLimit_BusinessValidFrom",
		case when pcmca."PositionCurrency" is NULL then sagrd."BusinessValidTo"		else agrd."BusinessValidTo" end 	AS "AgreedLimit_BusinessValidTo",
		case when pcmca."PositionCurrency" is NULL then sagrd."SystemValidFrom"		else agrd."SystemValidFrom" end 	AS "AgreedLimit_SystemValidFrom",
		case when pcmca."PositionCurrency" is NULL then sagrd."SystemValidTo"		else agrd."SystemValidTo" end		AS "AgreedLimit_SystemValidTo",
		case when pcmca."PositionCurrency" is NULL then sagrd."LimitType"			else agrd."LimitType" end			AS "AgreedLimit_LimitType",
		case when pcmca."PositionCurrency" is NULL then sagrd."LimitValidFrom"		else agrd."LimitValidFrom" end		AS "AgreedLimit_LimitValidFrom",
		case when pcmca."PositionCurrency" is NULL then sagrd."LimitValidTo" 		else agrd."LimitValidTo" end		AS "AgreedLimit_LimitValidTo",
		case when pcmca."PositionCurrency" is NULL then sagrd."LimitAmount"			else agrd."LimitAmount" end 		AS "AgreedLimit_LimitAmount",
		case when pcmca."PositionCurrency" is NULL then sagrd."LimitCurrency"		else agrd."LimitCurrency" end		AS "AgreedLimit_LimitCurrency",
		
		/*------ inte = Identifier for Interest ------*/
		
		case when pcmca."PositionCurrency" is NULL then sinte."SequenceNumber"  						else inte."SequenceNumber" end						AS "Interest_SequenceNumber",
		case when pcmca."PositionCurrency" is NULL then sinte."BusinessValidFrom"						else inte."BusinessValidFrom" end					AS "Interest_BusinessValidFrom",
		case when pcmca."PositionCurrency" is NULL then sinte."BusinessValidTo" 						else inte."BusinessValidTo" end 					AS "Interest_BusinessValidTo",
		case when pcmca."PositionCurrency" is NULL then sinte."SystemValidFrom" 						else inte."SystemValidFrom" end						AS "Interest_SystemValidFrom",
		case when pcmca."PositionCurrency" is NULL then sinte."SystemValidTo"							else inte."SystemValidTo" end						AS "Interest_SystemValidTo",
		case when pcmca."PositionCurrency" is NULL then sinte."InterestType"							else inte."InterestType" end 						AS "Interest_InterestType",
		case when pcmca."PositionCurrency" is NULL then sinte."PayingOrReceiving"						else inte."PayingOrReceiving" end 					AS "Interest_PayingOrReceiving",		
		case when pcmca."PositionCurrency" is NULL then sinte."InterestCategory"						else inte."InterestCategory" end 					AS "Interest_InterestCategory",
		case when pcmca."PositionCurrency" is NULL then sinte."FirstInterestPeriodStartDate"			else inte."FirstInterestPeriodStartDate" end 		AS "Interest_FirstInterestPeriodStartDate",
		case when pcmca."PositionCurrency" is NULL then sinte."FirstInterestPeriodEndDate"				else inte."FirstInterestPeriodEndDate" end 			AS "Interest_FirstInterestPeriodEndDate",		
		case when pcmca."PositionCurrency" is NULL then sinte."LastInterestPeriodEndDate"				else inte."LastInterestPeriodEndDate" end 			AS "Interest_LastInterestPeriodEndDate",		
		case when pcmca."PositionCurrency" is NULL then sinte."InterestCurrency"						else inte."InterestCurrency" end 					AS "Interest_InterestCurrency",		
		case when pcmca."PositionCurrency" is NULL then sinte."InterestPeriodLength"					else inte."InterestPeriodLength" end 				AS "Interest_InterestPeriodLength",
		case when pcmca."PositionCurrency" is NULL then sinte."InterestPeriodTimeUnit"					else inte."InterestPeriodTimeUnit" end				AS "Interest_InterestPeriodTimeUnit",
		case when pcmca."PositionCurrency" is NULL then sinte."DayOfMonthOfInterestPeriodEnd"			else inte."DayOfMonthOfInterestPeriodEnd" end		AS "Interest_DayOfMonthOfInterestPeriodEnd",
		case when pcmca."PositionCurrency" is NULL then sinte."InterestBusinessCalendar"				else inte."InterestBusinessCalendar" end			AS "Interest_InterestBusinessCalendar",
		case when pcmca."PositionCurrency" is NULL then sinte."BusinessDayConvention"					else inte."BusinessDayConvention" end				AS "Interest_BusinessDayConvention",
		case when pcmca."PositionCurrency" is NULL then sinte."InterestInAdvance"						else inte."InterestInAdvance" end					AS "Interest_InterestInAdvance",
		case when pcmca."PositionCurrency" is NULL then sinte."InterestSpecificationCategory"			else inte."InterestSpecificationCategory" end		AS "Interest_InterestSpecificationCategory", 
		case when pcmca."PositionCurrency" is NULL then sinte."ASSOC_ReferenceRateID_ReferenceRateID"	else inte."ASSOC_ReferenceRateID_ReferenceRateID" end AS "Interest_ReferenceRateID",
		case when pcmca."PositionCurrency" is NULL then sinte."VariableRateMin"							else inte."VariableRateMin" end						AS "Interest_VariableRateMin",
		case when pcmca."PositionCurrency" is NULL then sinte."VariableRateMax"							else inte."VariableRateMax" end 					AS "Interest_VariableRateMax",
		case when pcmca."PositionCurrency" is NULL then sinte."Spread"									else inte."Spread" end								AS "Interest_Spread",
		case when pcmca."PositionCurrency" is NULL then sinte."ReferenceRateFactor" 					else inte."ReferenceRateFactor" end					AS "Interest_ReferenceRateFactor",
		case when pcmca."PositionCurrency" is NULL then sinte."ResetCutoffLength" 						else inte."ResetCutoffLength" end					AS "Interest_ResetCutoffLength",
		case when pcmca."PositionCurrency" is NULL then sinte."ResetCutoffTimeUnit" 					else inte."ResetCutoffTimeUnit" end					AS "Interest_ResetCutoffTimeUnit",
		case when pcmca."PositionCurrency" is NULL then sinte."FixingRateSpecificationCategory" 		else inte."FixingRateSpecificationCategory" end		AS "Interest_FixingRateSpecificationCategory",
		case when pcmca."PositionCurrency" is NULL then sinte."ResetPeriodLength"						else inte."ResetPeriodLength" end					AS "Interest_ResetPeriodLength",
		case when pcmca."PositionCurrency" is NULL then sinte."ResetPeriodTimeUnit" 					else inte."ResetPeriodTimeUnit" end					AS "Interest_ResetPeriodTimeUnit",
		case when pcmca."PositionCurrency" is NULL then sinte."FirstRegularFloatingRateResetDate"		else inte."FirstRegularFloatingRateResetDate" end	AS "Interest_FirstRegularFloatingRateResetDate",
		case when pcmca."PositionCurrency" is NULL then sinte."ResetBusinessCalendar"					else inte."ResetBusinessCalendar" end				AS "Interest_ResetBusinessCalendar",
		case when pcmca."PositionCurrency" is NULL then sinte."ResetBusinessDayConvention"				else inte."ResetBusinessDayConvention" end			AS "Interest_ResetBusinessDayConvention",
		case when pcmca."PositionCurrency" is NULL then sinte."ResetLagLength"							else inte."ResetLagLength" end						AS "Interest_ResetLagLength",
		case when pcmca."PositionCurrency" is NULL then sinte."ResetLagTimeUnit"						else inte."ResetLagTimeUnit" end					AS "Interest_ResetLagTimeUnit",
		case when pcmca."PositionCurrency" is NULL then sinte."DayCountConvention"						else inte."DayCountConvention" end					AS "Interest_DayCountConvention",
		case when pcmca."PositionCurrency" is NULL then sinte."FixedRate"								else inte."FixedRate" end							AS "Interest_FixedRate",
		case when pcmca."PositionCurrency" is NULL then sinte."InterestIsCompounded"					else inte."InterestIsCompounded" end				AS "Interest_InterestIsCompounded",		
		case when pcmca."PositionCurrency" is NULL then sinte."CompoundingConvention"					else inte."CompoundingConvention" end				AS "Interest_CompoundingConvention",		
		
		/*------ iscale = Identifier for Interest ScaleInterval ------*/	

		case when pcmca."PositionCurrency" is NULL then siscale."BusinessValidFrom"						else iscale."BusinessValidFrom"	 end				AS "Interest_ScaleInterval_BusinessValidFrom",		
		case when pcmca."PositionCurrency" is NULL then siscale."BusinessValidTo"						else iscale."BusinessValidTo"	 end				AS "Interest_ScaleInterval_BusinessValidTo",	
		case when pcmca."PositionCurrency" is NULL then siscale."SystemValidFrom"						else iscale."SystemValidFrom"	 end				AS "Interest_ScaleInterval_SystemValidFrom",	
		case when pcmca."PositionCurrency" is NULL then siscale."SystemValidTo"							else iscale."SystemValidTo"	 end					AS "Interest_ScaleInterval_SystemValidTo",	
		case when pcmca."PositionCurrency" is NULL then siscale."BoundaryAmountCurrency"				else iscale."BoundaryAmountCurrency"	 end		AS "Interest_ScaleInterval_BoundaryAmountCurrency",	
		case when pcmca."PositionCurrency" is NULL then siscale."LowerBoundaryAmount"					else iscale."LowerBoundaryAmount"	 end			AS "Interest_ScaleInterval_LowerBoundaryAmount",	
		case when pcmca."PositionCurrency" is NULL then siscale."LowerBoundaryCount"					else iscale."LowerBoundaryCount"	 end			AS "Interest_ScaleInterval_LowerBoundaryCount",	
		case when pcmca."PositionCurrency" is NULL then siscale."ScaleBaseAggregationMethod"			else iscale."ScaleBaseAggregationMethod"	 end	AS "Interest_ScaleInterval_ScaleBaseAggregationMethod",	
		case when pcmca."PositionCurrency" is NULL then siscale."ScaleBaseType"							else iscale."ScaleBaseType"	 end					AS "Interest_ScaleInterval_ScaleBaseType",	
		case when pcmca."PositionCurrency" is NULL then siscale."ScalingMethod"							else iscale."ScalingMethod"	 end					AS "Interest_ScaleInterval_ScalingMethod",	
		case when pcmca."PositionCurrency" is NULL then siscale."UpperBoundaryAmount"					else iscale."UpperBoundaryAmount"	 end			AS "Interest_ScaleInterval_UpperBoundaryAmount",	
		case when pcmca."PositionCurrency" is NULL then siscale."UpperBoundaryCount"					else iscale."UpperBoundaryCount"	 end			AS "Interest_ScaleInterval_UpperBoundaryCount",	
		
		/*------ appl = Identifier for ApplicableInterestRate ------*/
		
		case when pcmca."PositionCurrency" is NULL then sappl."BusinessValidFrom"						else appl."BusinessValidFrom" end	AS "ApplicableInterestRate_BusinessValidFrom",
		case when pcmca."PositionCurrency" is NULL then sappl."BusinessValidTo" 						else appl."BusinessValidTo" end		AS "ApplicableInterestRate_BusinessValidTo",
		case when pcmca."PositionCurrency" is NULL then sappl."SystemValidFrom" 						else appl."SystemValidFrom" end		AS "ApplicableInterestRate_SystemValidFrom",
		case when pcmca."PositionCurrency" is NULL then sappl."SystemValidTo"							else appl."SystemValidTo" end		AS "ApplicableInterestRate_SystemValidTo",
		case when pcmca."PositionCurrency" is NULL then sappl."InterestType"							else appl."InterestType" end		AS "ApplicableInterestRate_InterestType",
		case when pcmca."PositionCurrency" is NULL then sappl."Rate"									else appl."Rate" end				AS "ApplicableInterestRate_Rate",
		case when pcmca."PositionCurrency" is NULL then sappl."LastResetDate"							else appl."LastResetDate" end		AS "ApplicableInterestRate_LastResetDate",
		case when pcmca."PositionCurrency" is NULL then sappl."NextResetDate"							else appl."NextResetDate" end		AS "ApplicableInterestRate_NextResetDate"		
		

	FROM  "com.adweko.adapter.osx.synonyms::FinancialContract_View"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) fc
    
    	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  csd ON 
                                csd."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
                            AND csd."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
							AND csd."ContractStatusCategory"								= 'DefaultStatus'

		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS csp ON
								csp."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"
							AND csp."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
							AND csp."ContractStatusCategory"								= 'PastDueStatus'

		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  bpca ON 
                                bpca."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
                            AND bpca."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
							AND bpca."Role"													= 'AccountHolder'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bpcac ON
								bpcac."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"
							AND bpcac."ASSOC_FinancialContract_IDSystem"					= fc."IDSystem"
							AND bpcac."Role"												= 'ClearingMember'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  ouca ON 
                                ouca."ASSOC_Contract_FinancialContractID"					= fc."FinancialContractID" 
                            AND ouca."ASSOC_Contract_IDSystem"								= fc."IDSystem"
       	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::PaymentSchedule_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  paym ON 
                                paym."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem" 
                            AND paym."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"                      
       	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::CancellationOption_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  canc ON 
                                canc."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem" 
                            AND canc."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"  
       	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Fee_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  fee ON 
                                fee."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem" 
                            AND fee."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"
        LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) pca ON
		   	pca."IDSystem" = fc."IDSystem"
		   	AND pca."FinancialContractID" = fc."FinancialContractID"  
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractChannelStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS ccs ON
	    						ccs."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
        				    AND ccs."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
       LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BankingChannel_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bch ON
								bch."BankingChannelID"										= ccs."ASSOC_BankingChannel_BankingChannelID" 
       LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::DigitalBankingPlatformAccess_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS dbpa ON
   							dbpa."ASSOC_DigitalBankingPlatform_BankingChannelID"			= ccs."ASSOC_BankingChannel_BankingChannelID"
 							
	/* -------------Multi Currency Account---------------*/  
	
			LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::PositionCurrencyOfMultiCurrencyContract_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  pcmca ON 
	                           pcmca."ASSOC_MultiCcyAccnt_IDSystem" 						= fc."IDSystem" AND
	                           pcmca."ASSOC_MultiCcyAccnt_FinancialContractID"				= fc."FinancialContractID" 
			LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::AgreedLimit_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  agrd ON 
	                           agrd."ASSOC_PositionCcyOfMultiCcyAccnt_ASSOC_MultiCcyAccnt_IDSystem"				= pcmca."ASSOC_MultiCcyAccnt_IDSystem" 
	                       AND agrd."ASSOC_PositionCcyOfMultiCcyAccnt_ASSOC_MultiCcyAccnt_FinancialContractID"	= pcmca."ASSOC_MultiCcyAccnt_FinancialContractID"
	                       AND agrd."ASSOC_PositionCcyOfMultiCcyAccnt_PositionCurrency"						    = pcmca."PositionCurrency"	     
			LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ApplicableInterestRate_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  appl ON 
	                           appl."ASSOC_PositionCurrencyForAccount_ASSOC_MultiCcyAccnt_IDSystem"										   = pcmca."ASSOC_MultiCcyAccnt_IDSystem" 
	                       AND appl."ASSOC_PositionCurrencyForAccount_ASSOC_MultiCcyAccnt_FinancialContractID"							   = pcmca."ASSOC_MultiCcyAccnt_FinancialContractID"
	                       AND appl."ASSOC_PositionCurrencyForAccount_PositionCurrency"					       = pcmca."PositionCurrency"	
			LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Interest_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  inte ON 
	                           inte."ASSOC_CcyOfMultiCcyAccnt_ASSOC_MultiCcyAccnt_IDSystem"											= pcmca."ASSOC_MultiCcyAccnt_IDSystem" 
	                       AND inte."ASSOC_CcyOfMultiCcyAccnt_ASSOC_MultiCcyAccnt_FinancialContractID"								= pcmca."ASSOC_MultiCcyAccnt_FinancialContractID"
	                       AND inte."ASSOC_CcyOfMultiCcyAccnt_PositionCurrency"									= pcmca."PositionCurrency"	
			LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ScaleInterval_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  iscale ON 
							   iscale."ASSOC_InterestSpecification_SequenceNumber"								= inte."SequenceNumber"
	                       AND iscale."ASSOC_InterestSpecification_ASSOC_CcyOfMultiCcyAccnt_ASSOC_MultiCcyAccnt_IDSystem"			 = pcmca."ASSOC_MultiCcyAccnt_IDSystem" 
	                       AND iscale."ASSOC_InterestSpecification_ASSOC_CcyOfMultiCcyAccnt_ASSOC_MultiCcyAccnt_FinancialContractID" = pcmca."ASSOC_MultiCcyAccnt_FinancialContractID"
	                       AND iscale."ASSOC_InterestSpecification_ASSOC_CcyOfMultiCcyAccnt_PositionCurrency"	= pcmca."PositionCurrency"		                    
	                    	
	/* -------------Single Currency Account---------------*/
	
    		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::AgreedLimit_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  sagrd ON 
	                           sagrd."ASSOC_FinancialContract_IDSystem"			   = fc."IDSystem"
	                       AND sagrd."ASSOC_FinancialContract_FinancialContractID" = fc."FinancialContractID"   
	                       AND pcmca."PositionCurrency"							is NULL	                       
			LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ApplicableInterestRate_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  sappl ON 	                            
	                           sappl."ASSOC_FinancialContract_IDSystem"			   = fc."IDSystem"
	                       AND sappl."ASSOC_FinancialContract_FinancialContractID" = fc."FinancialContractID"   
	                       AND pcmca."PositionCurrency"							is NULL    
			LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Interest_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  sinte ON 
    	                       sinte."ASSOC_FinancialContract_IDSystem"			   = fc."IDSystem"
	                       AND sinte."ASSOC_FinancialContract_FinancialContractID" = fc."FinancialContractID"   
	                       AND pcmca."PositionCurrency"							is NULL	    
			LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ScaleInterval_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  siscale ON 
							   siscale."ASSOC_InterestSpecification_SequenceNumber"	= sinte."SequenceNumber"
	                       AND siscale."ASSOC_InterestSpecification_ASSOC_CcyOfMultiCcyAccnt_ASSOC_MultiCcyAccnt_IDSystem"			  = fc."IDSystem"
	                       AND siscale."ASSOC_InterestSpecification_ASSOC_CcyOfMultiCcyAccnt_ASSOC_MultiCcyAccnt_FinancialContractID" = fc."FinancialContractID"  
	                       AND pcmca."PositionCurrency"							is NULL	   
    
    WHERE 		fc."FinancialContractCategory"		=  'BankAccount'
    		AND	fc. "AccountCategory"				<> 'SecuritiesAccount'