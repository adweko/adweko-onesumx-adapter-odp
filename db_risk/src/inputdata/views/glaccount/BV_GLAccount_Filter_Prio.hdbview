VIEW "com.adweko.adapter.osx.inputdata.glaccount::BV_GLAccount_Filter_Prio"
	( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
	AS
	SELECT
		"GLAccount_GLAccount",
		"GLAccount_ChartOfAccountId",
		"GLAccount_SourceSystemID",
		"GLAccount_GLAccountGroupID",
		"GLAccount_OpeningDate",
		"GLAccount_AssetLiabilityAssignment",
		"AccountingEntityGLAccount_Company_BusinessPartnerID",
		"FilterID"
	FROM ( 
		SELECT
	        gl."GLAccount"							AS "GLAccount_GLAccount",
	        gl."_ChartOfAccounts_ChartOfAccountId"	AS "GLAccount_ChartOfAccountId",
	        gl."SourceSystemID"						AS "GLAccount_SourceSystemID",
	        gl."_GLAccountGroup_GLAccountGroupID"	AS "GLAccount_GLAccountGroupID",
	        gl."OpeningDate"						AS "GLAccount_OpeningDate",
	        gl."AssetLiabilityAssignment"			AS "GLAccount_AssetLiabilityAssignment",
	        ae."_Company_BusinessPartnerID"			AS "AccountingEntityGLAccount_Company_BusinessPartnerID",
	        map_glgroup."FilterID"					AS "FilterID",
			ROW_NUMBER() OVER (
				PARTITION BY
					gl."GLAccount",
					gl."_ChartOfAccounts_ChartOfAccountId"
				ORDER BY map_glgroup."FilterID"
			) AS "Prio"
		
		FROM "com.adweko.adapter.osx.synonyms::GLAccount_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) gl
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::AccountingEntityGLAccount_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) ae
			ON	ae."_GLAccount_GLAccount"							= gl."GLAccount"
			AND ae."_GLAccount__ChartOfAccounts_ChartOfAccountId"	= gl."_ChartOfAccounts_ChartOfAccountId"
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::FinancialStatementSegment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) seg
			ON	seg."_GLAccount_GLAccount"							= gl."GLAccount"
			AND seg."_GLAccount__ChartOfAccounts_ChartOfAccountId"	= gl."_ChartOfAccounts_ChartOfAccountId"
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::FinancialStatementStructure_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) str
			ON	str."FinancialStatementStructureID"					= seg."_FinancialStatementStructure_FinancialStatementStructureID"	
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_GLAccountGroupID_Filter_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) map_glgroup
			ON	map_glgroup."StreamName" = 'GL Account - Equity Positions'
			AND	(
				map_glgroup."GLAccountGroupID" = gl."_GLAccountGroup_GLAccountGroupID"
				OR		
				IFNULL(TRIM(map_glgroup."GLAccountGroupID"),'') = ''
			)
			AND	(
				map_glgroup."GLAccount" = gl."GLAccount"
				OR		
				IFNULL(TRIM(map_glgroup."GLAccount"),'') = ''
			)
			AND	(
				map_glgroup."ChartOfAccountId" = gl."_ChartOfAccounts_ChartOfAccountId"
				OR		
				IFNULL(TRIM(map_glgroup."ChartOfAccountId"),'') = ''
			)
			AND ( 
				map_glgroup."CompanyCode" = ae."CompanyCode" 
				OR
				IFNULL(TRIM(map_glgroup."CompanyCode"),'') = ''
			)
			AND ( 
				map_glgroup."FinancialStatementStructureID" = str."FinancialStatementStructureID" 
				OR
				IFNULL(TRIM(map_glgroup."FinancialStatementStructureID"),'') = ''
			)
			AND ( 
				map_glgroup."FinancialStatementSegmentID" = seg."FinancialStatementSegmentID" 
				OR
				IFNULL(TRIM(map_glgroup."FinancialStatementSegmentID"),'') = ''
			)
		
		WHERE
			NOT EXISTS ( -- Option 1) StreamName not found in mapping = no filter for this stream at all => take all entries
				SELECT 1
					FROM "com.adweko.adapter.osx.mappings::map_GLAccountGroupID_Filter_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) check_mapping
					WHERE check_mapping."StreamName" = 'GL Account - Equity Positions' )
			OR -- Option 2) otherwise the filter entry must be found
			map_glgroup."StreamName" IS NOT NULL
				
		) WHERE "Prio" = 1 --in order to eliminate potential duplicates (from AccountingEntityGLAccount or map_GLAccountGroupID_Filter)