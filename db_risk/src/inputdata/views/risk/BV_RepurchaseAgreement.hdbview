VIEW "com.adweko.adapter.osx.inputdata.risk::BV_RepurchaseAgreement"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
AS
       	SELECT
      	/*-----fc = Identifier for FinancialContract ------*/
		fc."IDSystem"																		AS "FinancialContract_IDSystem",
		fc."FinancialContractID"															AS "FinancialContract_FinancialContractID",
		fc."BusinessValidFrom"																AS "FinancialContract_BusinessValidFrom",
		fc."BusinessValidTo"																AS "FinancialContract_BusinessValidTo",
		fc."SystemValidFrom"																AS "FinancialContract_SystemValidFrom",
		fc."SystemValidTo"																	AS "FinancialContract_SystemValidTo",
		fc."OriginalSigningDate"															AS "FinancialContract_OriginalSigningDate",
		fc."GoverningLawCountry"															AS "FinancialContract_GoverningLawCountry",
		fc."Purpose"																		AS "FinancialContract_Purpose",
		fc."LifecycleStatus"																AS "FinancialContract_LifecycleStatus",
		fc."LifecycleStatusReason"															AS "FinancialContract_LifecycleStatusReason",
		fc."LifecycleStatusChangeDate"														AS "FinancialContract_LifecycleStatusChangeDate",
		fc."FinancialContractType"															AS "FinancialContract_FinancialContractType",
		fc."Description"																	AS "FinancialContract_Description",
		fc."OfferValidityStartDate"															AS "FinancialContract_OfferValidityStartDate",
		fc."OfferValidityEndDate"															AS "FinancialContract_OfferValidityEndDate",
		fc."ApplicableLaw"																	AS "FinancialContract_ApplicableLaw",
		fc."PlaceOfJurisdiction"															AS "FinancialContract_PlaceOfJurisdiction",
		fc."PurposeType"																	AS "FinancialContract_PurposeType",
		fc."IntendedAssetLiability"															AS "FinancialContract_IntendedAssetLiability",
		fc."POCIAcquisitionStatus"															AS "FinancialContract_POCIAcquisitionStatus",
		fc."GracePeriod"																	AS "FinancialContract_GracePeriod",
		fc."PurchaseDate"																	AS "FinancialContract_PurchaseDate",
		fc."RepurchaseDate"																	AS "FinancialContract_RepurchaseDate",
		fc."OpenMaturity"																	AS "FinancialContract_OpenMaturity",
		fc."RepoToMaturity"																	AS "FinancialContract_RepoToMaturity",
		fc."PurchasePricePerInstrument"														AS "FinancialContract_PurchasePricePerInstrument",
		fc."TotalPurchasePrice"																AS "FinancialContract_TotalPurchasePrice",
		fc."RepurchasePricePerInstrument"													AS "FinancialContract_RepurchasePricePerInstrument",
		fc."TotalRepurchasePrice"															AS "FinancialContract_TotalRepurchasePrice",
		fc."RepoInterestAmount"																AS "FinancialContract_RepoInterestAmount",
		fc."ContractualCurrency"															AS "FinancialContract_ContractualCurrency",
		fc."QuantitySold"																	AS "FinancialContract_QuantitySold",
		fc."Unit"																			AS "FinancialContract_Unit",
		fc."NominalAmountOfInstrumentsSold"													AS "FinancialContract_NominalAmountOfInstrumentsSold",
		fc."DenominationCurrency"															AS "FinancialContract_DenominationCurrency",
		fc."ManufacturedPayments"															AS "FinancialContract_ManufacturedPayments",
		fc."ResaleAllowed"																	AS "FinancialContract_ResaleAllowed",
		fc."SettlementMethod"																AS "FinancialContract_SettlementMethod",
		fc."BuyerAllowedToRetainCollateral"													AS "FinancialContract_BuyerAllowedToRetainCollateral",
		fc."BrokerIsLiable"																	AS "FinancialContract_BrokerIsLiable",
		fc."_FinancialInstrument_FinancialInstrumentID"										AS "FinancialContract_FinancialInstrument_FinancialInstrumentID",
		fc."_SecuritiesAccountOfAssignedFinancialInstrument_FinancialContractID"			AS "FinancialContract_SecuritiesAccountOfAssignedFinancialInstrument_FinancialContractID",
		fc."_SecuritiesAccountOfAssignedFinancialInstrument_IDSystem"						AS "FinancialContract_SecuritiesAccountOfAssignedFinancialInstrument_IDSystem",
		
		fi."PreferredOrCommonStock"															AS "FinancialInstrument_PreferredOrCommonStock",
		fi."SecurityCategory"																AS "FinancialInstrument_SecurityCategory",

		csd."ContractStatusType"															AS "ContractStatus_DefaultStatus_ContractStatusType",
		csd."BusinessValidFrom"																AS "ContractStatus_DefaultStatus_BusinessValidFrom",
		csd."BusinessValidTo"																AS "ContractStatus_DefaultStatus_BusinessValidTo",
		csd."SystemValidFrom"																AS "ContractStatus_DefaultStatus_SystemValidFrom",
		csd."SystemValidTo"																	AS "ContractStatus_DefaultStatus_SystemValidTo",
		csd."Status"																		AS "ContractStatus_DefaultStatus_Status",
					
		csp."BusinessValidFrom"																AS "ContractStatus_PastDueStatus_BusinessValidFrom",
		csp."BusinessValidTo"																AS "ContractStatus_PastDueStatus_BusinessValidTo",
		csp."SystemValidFrom"																AS "ContractStatus_PastDueStatus_SystemValidFrom",
		csp."SystemValidTo"																	AS "ContractStatus_PastDueStatus_SystemValidTo",
		csp."Status"																		AS "ContractStatus_PastDueStatus_Status",
					
		/*-----pca = Identifier for ProductCatalogAssignment_View ------*/
		'FSDMStandard'										    						as "ProductCatalogAssignment_ProductCatalogItem_ProductCatalog_CatalogID",
		pca."StandardCatalog_ProductCatalogItem"        								as "ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",
		pca."StandardCatalog_ProductCatalogItem",
        pca."CustomCatalog_ProductCatalogItem",
		pca."BusinessValidFrom"     													as "ProductCatalogAssignment_BusinessValidFrom",
		pca."BusinessValidTo"       													as "ProductCatalogAssignment_BusinessValidTo",
		
					
		bpcab."ASSOC_PartnerInParticipation_BusinessPartnerID"								AS "BusinessPartnerContractAssignment_Borrower_PartnerInParticipation_BusinessPartnerID",
		bpcab."ContractDataOwner"															AS "BusinessPartnerContractAssignment_Borrower_ContractDataOwner",
		bpcab."BusinessValidFrom"															AS "BusinessPartnerContractAssignment_Borrower_BusinessValidFrom",
		bpcab."BusinessValidTo"																AS "BusinessPartnerContractAssignment_Borrower_BusinessValidTo",
		bpcab."SystemValidFrom"																AS "BusinessPartnerContractAssignment_Borrower_SystemValidFrom",
		bpcab."SystemValidTo"																AS "BusinessPartnerContractAssignment_Borrower_SystemValidTo",
				
		bpcacc."ASSOC_PartnerInParticipation_BusinessPartnerID"								AS "BusinessPartnerContractAssignment_CentralCounterparty_PartnerInParticipation_BusinessPartnerID",
		bpcacc."ContractDataOwner"															AS "BusinessPartnerContractAssignment_CentralCounterparty_ContractDataOwner",
		bpcacc."BusinessValidFrom"															AS "BusinessPartnerContractAssignment_CentralCounterparty_BusinessValidFrom",
		bpcacc."BusinessValidTo"															AS "BusinessPartnerContractAssignment_CentralCounterparty_BusinessValidTo",
		bpcacc."SystemValidFrom"															AS "BusinessPartnerContractAssignment_CentralCounterparty_SystemValidFrom",
		bpcacc."SystemValidTo"																AS "BusinessPartnerContractAssignment_CentralCounterparty_SystemValidTo",
					
		bpcal."ASSOC_PartnerInParticipation_BusinessPartnerID"								AS "BusinessPartnerContractAssignment_Lender_PartnerInParticipation_BusinessPartnerID",
		bpcal."ContractDataOwner"															AS "BusinessPartnerContractAssignment_Lender_ContractDataOwner",
		bpcal."BusinessValidFrom"															AS "BusinessPartnerContractAssignment_Lender_BusinessValidFrom",
		bpcal."BusinessValidTo"																AS "BusinessPartnerContractAssignment_Lender_BusinessValidTo",
		bpcal."SystemValidFrom"																AS "BusinessPartnerContractAssignment_Lender_SystemValidFrom",
		bpcal."SystemValidTo"																AS "BusinessPartnerContractAssignment_Lender_SystemValidTo",
					
		bpcac."ASSOC_PartnerInParticipation_BusinessPartnerID"								AS "BusinessPartnerContractAssignment_ClearingMember_PartnerInParticipation_BusinessPartnerID",
		bpcac."BusinessValidFrom"															AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidFrom",
		bpcac."BusinessValidTo"																AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidTo",
		bpcac."SystemValidFrom"																AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidFrom",
		bpcac."SystemValidTo"																AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidTo",
		
		/*------ bpcao = Identifier for BusinessPartnerContractAssignment ------*/
		bpcao."ASSOC_PartnerInParticipation_BusinessPartnerID"     							AS "BusinessPartnerContractAssignment_ContractDataOwner_PartnerInParticipation_BusinessPartnerID",
		bpcao."BusinessValidFrom"                            								AS "BusinessPartnerContractAssignment_ContractDataOwner_BusinessValidFrom",
		bpcao."BusinessValidTo"                              								AS "BusinessPartnerContractAssignment_ContractDataOwner_BusinessValidTo",
		bpcao."SystemValidFrom"                              								AS "BusinessPartnerContractAssignment_ContractDataOwner_SystemValidFrom",
		bpcao."SystemValidTo"                                								AS "BusinessPartnerContractAssignment_ContractDataOwner_SystemValidTo",	
					
		ouca."RoleOfOrganizationalUnit"														AS "OrganizationalUnitContractAssignment_RoleOfOrganizationalUnit",
		ouca."ASSOC_OrgUnit_OrganizationalUnitID"											AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationalUnitID",
		ouca."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID"	AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationHostingOrganizationalUnit_BusinessPartnerID",
		ouca."BusinessValidFrom"															AS "OrganizationalUnitContractAssignment_BusinessValidFrom",
		ouca."BusinessValidTo"																AS "OrganizationalUnitContractAssignment_BusinessValidTo",
		ouca."SystemValidFrom"																AS "OrganizationalUnitContractAssignment_SystemValidFrom",
		ouca."SystemValidTo"																AS "OrganizationalUnitContractAssignment_SystemValidTo",
					
		inte."SequenceNumber"																AS "Interest_SequenceNumber",
		inte."BusinessValidFrom"															AS "Interest_BusinessValidFrom",
		inte."BusinessValidTo"																AS "Interest_BusinessValidTo",
		inte."SystemValidFrom"																AS "Interest_SystemValidFrom",
		inte."SystemValidTo"																AS "Interest_SystemValidTo",
		inte."InterestType"																	AS "Interest_InterestType",
		inte."PayingOrReceiving"															AS "Interest_PayingOrReceiving",
		inte."PreconditionApplies"															AS "Interest_PreconditionApplies",
		inte."ScaleApplies"																	AS "Interest_ScaleApplies",
		inte."InterestCategory"																AS "Interest_InterestCategory",
		inte."FirstInterestPeriodStartDate"													AS "Interest_FirstInterestPeriodStartDate",
		inte."FirstInterestPeriodEndDate"													AS "Interest_FirstInterestPeriodEndDate",
		inte."LastInterestPeriodEndDate"													AS "Interest_LastInterestPeriodEndDate",
		inte."InterestCurrency"																AS "Interest_InterestCurrency",
		inte."InterestPeriodLength"															AS "Interest_InterestPeriodLength",
		inte."InterestPeriodTimeUnit"														AS "Interest_InterestPeriodTimeUnit",
		inte."DayOfMonthOfInterestPeriodEnd"												AS "Interest_DayOfMonthOfInterestPeriodEnd",
		inte."InterestBusinessCalendar"														AS "Interest_InterestBusinessCalendar",
		inte."BusinessDayConvention"														AS "Interest_BusinessDayConvention",
		inte."InterestPaymentPrecision"														AS "Interest_InterestPaymentPrecision",
		inte."InterestPaymentRoundingMethod"												AS "Interest_InterestPaymentRoundingMethod",
		inte."InterestInAdvance"															AS "Interest_InterestInAdvance",
		inte."ASSOC_ReferenceRateID_ReferenceRateID"										AS "Interest_ReferenceRateID",
		inte."PeriodEndDueDateLag"															AS "Interest_PeriodEndDueDateLag",
		inte."PeriodEndDueDateLagTimeUnit"													AS "Interest_PeriodEndDueDateLagTimeUnit",
		inte."FirstDueDate"																	AS "Interest_FirstDueDate",
		inte."DueDateScheduleIsIndependent"													AS "Interest_DueDateScheduleIsIndependent",
		inte."DueSchedulePeriodLength"														AS "Interest_DueSchedulePeriodLength",
		inte."DueSchedulePeriodTimeUnit"													AS "Interest_DueSchedulePeriodTimeUnit",
		inte."DueScheduleBusinessCalendar"													AS "Interest_DueScheduleBusinessCalendar",
		inte."DueScheduleBusinessDayConvention"												AS "Interest_DueScheduleBusinessDayConvention",
		inte."AnnuityAmount"																AS "Interest_AnnuityAmount",
		inte."AnnuityAmountCurrency"														AS "Interest_AnnuityAmountCurrency",
		inte."DayOfMonthOfInterestPayment"													AS "Interest_DayOfMonthOfInterestPayment",
		inte."InterestSpecificationCategory"												AS "Interest_InterestSpecificationCategory",
		inte."VariableRateMin"																AS "Interest_VariableRateMin",
		inte."VariableRateMax"																AS "Interest_VariableRateMax",
		inte."Spread"																		AS "Interest_Spread",
		inte."ReferenceRateFactor"															AS "Interest_ReferenceRateFactor",
		inte."ResetCutoffLength"															AS "Interest_ResetCutoffLength",
		inte."ResetCutoffTimeUnit"															AS "Interest_ResetCutoffTimeUnit",
		inte."ResetPrecision"																AS "Interest_ResetPrecision",
		inte."ResetRounding"																AS "Interest_ResetRounding",
		inte."FixingRateSpecificationCategory"												AS "Interest_FixingRateSpecificationCategory",
		inte."ResetPeriodLength"															AS "Interest_ResetPeriodLength",
		inte."ResetPeriodTimeUnit"															AS "Interest_ResetPeriodTimeUnit",
		inte."FirstRegularFloatingRateResetDate"											AS "Interest_FirstRegularFloatingRateResetDate",
		inte."ResetBusinessCalendar"														AS "Interest_ResetBusinessCalendar",
		inte."ResetBusinessDayConvention"													AS "Interest_ResetBusinessDayConvention",
		inte."ResetLagLength"																AS "Interest_ResetLagLength",
		inte."ResetLagTimeUnit"																AS "Interest_ResetLagTimeUnit",
		inte."DayCountConvention"															AS "Interest_DayCountConvention",
		inte."FixedRate"																	AS "Interest_FixedRate",
					
		riff."BusinessValidFrom"															AS "RegulatoryIndicatorForFinancialContract_BusinessValidFrom",
		riff."BusinessValidTo"																AS "RegulatoryIndicatorForFinancialContract_BusinessValidTo",
		riff."SystemValidFrom"																AS "RegulatoryIndicatorForFinancialContract_SystemValidFrom",
		riff."SystemValidTo"																AS "RegulatoryIndicatorForFinancialContract_SystemValidTo",
		riff."Criterion"																	AS "RegulatoryIndicatorForFinancialContract_Criterion",
		riff."IndicatorCharValue"															AS "RegulatoryIndicatorForFinancialContract_IndicatorCharValue",
		riff."Regulation"																	AS "RegulatoryIndicatorForFinancialContract_Regulation",
					
		ccs."ContractChannelStatus"															AS "ContractChannelStatus_ContractChannelStatus",
		ccs."StatusChangeReason"															AS "ContractChannelStatus_StatusChangeReason",
		ccs."BusinessValidFrom"																AS "ContractChannelStatus_BusinessValidFrom",
		ccs."BusinessValidTo"																AS "ContractChannelStatus_BusinessValidTo",
		ccs."SystemValidFrom"																AS "ContractChannelStatus_SystemValidFrom",
		ccs."SystemValidTo"																	AS "ContractChannelStatus_SystemValidTo",
					
		bch."BankingChannelID"																AS "BankingChannel_BankingChannelID",
		bch."Description"																	AS "BankingChannel_Description",
		bch."BankingChannelCategory"														AS "BankingChannel_BankingChannelCategory",
		bch."BusinessValidFrom"																AS "BankingChannel_BusinessValidFrom",
		bch."BusinessValidTo"																AS "BankingChannel_BusinessValidTo",
		bch."SystemValidFrom"																AS "BankingChannel_SystemValidFrom",
		bch."SystemValidTo"																	AS "BankingChannel_SystemValidTo"
        
    FROM "com.adweko.adapter.osx.synonyms::FinancialContract_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS fc
    	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS csd ON 
                csd."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND csd."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND csd."ContractStatusCategory"								= 'DefaultStatus'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS csp ON
				csp."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND csp."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND csp."ContractStatusCategory"								= 'PastDueStatus'	
        LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) pca ON
		   	pca."IDSystem" = fc."IDSystem"
		   	AND pca."FinancialContractID" = fc."FinancialContractID" 
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcab ON 
                bpcab."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND bpcab."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND bpcab."Role"											        = 'Borrower'

		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcacc ON 
                bpcacc."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND bpcacc."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND bpcacc."Role"											        = 'CentralCounterparty'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcal ON 
                bpcal."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND bpcal."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND bpcal."Role"											        = 'Lender'

		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bpcac ON
				bpcac."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND bpcac."ASSOC_FinancialContract_IDSystem"					= fc."IDSystem"
			AND bpcac."Role"											    = 'ClearingMember'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bpcao ON
				bpcao."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"
			AND bpcao."ASSOC_FinancialContract_IDSystem"					= fc."IDSystem"
			AND bpcao."ContractDataOwner"									= true		
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS ouca ON 
                ouca."ASSOC_Contract_FinancialContractID"					= fc."FinancialContractID" 
            AND ouca."ASSOC_Contract_IDSystem"								= fc."IDSystem"
            AND ouca."RoleOfOrganizationalUnit"								= 'ManagingUnit'

        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Interest_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS inte ON 
	            inte."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"  
	        AND inte."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 

        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::RegulatoryIndicatorForFinancialContract_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS riff ON
	    		riff."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
            AND riff."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 

        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractChannelStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS ccs ON
	    		ccs."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
            AND ccs."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 

        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BankingChannel_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bch ON
	    		bch."BankingChannelID"						= fc."ASSOC_BankingChannel_BankingChannelID"
	    		
	    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::FinancialInstrument_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS fi
	    	ON fi."FinancialInstrumentID"						= fc."_FinancialInstrument_FinancialInstrumentID"

    WHERE 		fc."FinancialContractCategory"		=  'RepurchaseAgreement'
    