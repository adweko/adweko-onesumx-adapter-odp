VIEW "com.adweko.adapter.osx.inputdata.risk::BV_CommodityPrices"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
AS
	
	SELECT
		"CommodityEndOfDayPriceObservation_Commodity_CommodityID",
		"CommodityEndOfDayPriceObservation_CommodityReferencePrice_ReferencePriceID",
		"CommodityEndOfDayPriceObservation_PriceDataProvider",
        "CommodityEndOfDayPriceObservation_PriceSeriesType",
        "CommodityEndOfDayPriceObservation_BusinessValidFrom",
        "CommodityEndOfDayPriceObservation_BusinessValidTo",
        "CommodityEndOfDayPriceObservation_SystemValidFrom",
        "CommodityEndOfDayPriceObservation_SystemValidTo",
        "CommodityEndOfDayPriceObservation_Open",
        "CommodityEndOfDayPriceObservation_Close",
        "CommodityEndOfDayPriceObservation_Currency",
        "CommodityEndOfDayPriceObservation_Quantity",
        "CommodityEndOfDayPriceObservation_Unit",
        "CommodityEndOfDayPriceObservation_SourceSystemID"
	FROM(
		SELECT DISTINCT
			com."_Commodity_CommodityID"                                                                        		  AS "CommodityEndOfDayPriceObservation_Commodity_CommodityID",
	        com."_CommodityReferencePrice_ReferencePriceID"                                                               AS "CommodityEndOfDayPriceObservation_CommodityReferencePrice_ReferencePriceID",
	        com."PriceDataProvider"                                                                                       AS "CommodityEndOfDayPriceObservation_PriceDataProvider",
	        com."PriceSeriesType"                                                                                         AS "CommodityEndOfDayPriceObservation_PriceSeriesType",
	        com."BusinessValidFrom"                                                                                       AS "CommodityEndOfDayPriceObservation_BusinessValidFrom",
	        com."BusinessValidTo"                                                                                         AS "CommodityEndOfDayPriceObservation_BusinessValidTo",
	        com."SystemValidFrom"                                                                                         AS "CommodityEndOfDayPriceObservation_SystemValidFrom",
	        com."SystemValidTo"                                                                                           AS "CommodityEndOfDayPriceObservation_SystemValidTo",
	        com."Open"                                                                                                    AS "CommodityEndOfDayPriceObservation_Open",
	        com."Close"                                                                                                   AS "CommodityEndOfDayPriceObservation_Close",
	        com."Currency"                                                                                                AS "CommodityEndOfDayPriceObservation_Currency",
	        com."Quantity"                                                                                                AS "CommodityEndOfDayPriceObservation_Quantity",
	        com."Unit"                                                                                                    AS "CommodityEndOfDayPriceObservation_Unit",
	        com."SourceSystemID"                                                                                          AS "CommodityEndOfDayPriceObservation_SourceSystemID",
	        ROW_NUMBER() OVER (
	        	PARTITION BY 
	        		com."_Commodity_CommodityID"
			 	ORDER BY
			 		dp."Priority" ASC
			) AS "Prio"
	
		FROM "com.adweko.adapter.osx.synonyms::CommodityEndOfDayPriceObservation_View" ( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP ) AS com
		
	    INNER JOIN "com.adweko.adapter.osx.mappings::map_DataProvider_View" ( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS dp
	        ON  dp."marketPriceKind"                		= 'CommodityPrices'
	        AND IFNULL(TRIM(dp."PriceDataProvider"), '')	= IFNULL(TRIM(com."PriceDataProvider"), '') 
	)
	WHERE "Prio" = 1
