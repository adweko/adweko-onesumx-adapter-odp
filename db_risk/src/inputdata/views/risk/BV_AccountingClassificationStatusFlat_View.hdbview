VIEW "com.adweko.adapter.osx.inputdata.risk::BV_AccountingClassificationStatusFlat"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
AS
	SELECT
        fc."IDSystem"															AS "AccountingClassificationStatus_FinancialContract_IDSystem",
    	fc."FinancialContractID"												AS "AccountingClassificationStatus_FinancialContract_FinancialContractID",
        acs_ifrs."_AccountingSystem_AccountingSystemID"							AS "AccountingClassificationStatus_IFRS_AccountingSystem_AccountingSystemID",
        acs_hgb."_AccountingSystem_AccountingSystemID"							AS "AccountingClassificationStatus_HGB_AccountingSystem_AccountingSystemID",
		acs_ifrs."AccountingClassificationCategory"								AS "AccountingClassificationStatus_IFRS_AccountingClassificationCategory",
        CASE
        	WHEN acs_ifrs."AccountingClassificationCategory" = 'HoldingCategoryStatus'
        		THEN acs_ifrs."HoldingCategoryStatus"
        END																		AS "AccountingClassificationStatus_IFRS_HoldingCategoryStatus",
        acs_hgb."AccountingClassificationCategory"								AS "AccountingClassificationStatus_HGB_AccountingClassificationCategory",
        CASE
        	WHEN acs_hgb."AccountingClassificationCategory" = 'HoldingCategoryStatus'
        		THEN acs_hgb."HoldingCategoryStatus"
        END																		AS "AccountingClassificationStatus_HGB_HoldingCategoryStatus"

    FROM "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) fc
    
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::AccountingClassificationStatus_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS acs_ifrs
		ON acs_ifrs."_FinancialContract_FinancialContractID"		= fc."FinancialContractID"
		AND acs_ifrs."_FinancialContract_IDSystem"				= fc."IDSystem"
		AND acs_ifrs."_AccountingSystem_AccountingSystemID"			= 'IFRS'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::AccountingClassificationStatus_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS acs_hgb
		ON acs_hgb."_FinancialContract_FinancialContractID"		= fc."FinancialContractID"
		AND acs_hgb."_FinancialContract_IDSystem"				= fc."IDSystem"
		AND acs_hgb."_AccountingSystem_AccountingSystemID"		= 'HGB'