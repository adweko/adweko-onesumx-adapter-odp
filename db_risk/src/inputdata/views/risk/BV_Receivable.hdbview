VIEW "com.adweko.adapter.osx.inputdata.risk::BV_Receivable"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
AS
    SELECT
		/*-----rece = Identifier for Receivable_View ------*/
		rece."ReceivableID"                                                                                           AS "Receivable_ReceivableID",
		rece."BusinessValidFrom"                                                                                      AS "Receivable_BusinessValidFrom",
		rece."BusinessValidTo"                                                                                        AS "Receivable_BusinessValidTo",
		rece."SystemValidFrom"                                                                                        AS "Receivable_SystemValidFrom",
		rece."SystemValidTo"                                                                                          AS "Receivable_SystemValidTo",
		rece."ApplicableLaw"                                                                                          AS "Receivable_ApplicableLaw",
		rece."LastDueDate"                                                                                            AS "Receivable_LastDueDate",
		rece."OriginalReceivableAmount"                                                                               AS "Receivable_OriginalReceivableAmount",
		rece."OriginalReceivableAmountCurrency"                                                                       AS "Receivable_OriginalReceivableAmountCurrency",
		rece."OutstandingReceivableAmount"                                                                            AS "Receivable_OutstandingReceivableAmount",
		rece."OutstandingReceivableAmountCurrency"                                                                    AS "Receivable_OutstandingReceivableAmountCurrency",
		rece."ReceivableType"                                                                                         AS "Receivable_ReceivableType",
		rece."PlaceOfJurisdiction"                                                                                    AS "Receivable_PlaceOfJurisdiction",
		
		/*-----bpca = Identifier for ReceivableBusinessPartnerAssignment_View ------*/
		bpca."BusinessValidFrom"                                                                                      AS "ReceivableBusinessPartnerAssignment_BusinessValidFrom",
		bpca."BusinessValidTo"                                                                                        AS "ReceivableBusinessPartnerAssignment_BusinessValidTo",
		bpca."SystemValidFrom"                                                                                        AS "ReceivableBusinessPartnerAssignment_SystemValidFrom",
		bpca."SystemValidTo"                                                                                          AS "ReceivableBusinessPartnerAssignment_SystemValidTo",
		bpca."Role"                                                                                                   AS "ReceivableBusinessPartnerAssignment_Role",
		bpca."ReceivableDataOwner"                                                                                    AS "ReceivableBusinessPartnerAssignment_ReceivableDataOwner",
		
		/*-----bp = Identifier for BusinessPartner_View ------*/
		bp."BusinessValidFrom"                                                                                        AS "BusinessPatner_BusinessValidFrom",
		bp."BusinessValidTo"                                                                                          AS "BusinessPatner_BusinessValidTo",
		bp."SystemValidFrom"                                                                                          AS "BusinessPatner_SystemValidFrom",
		bp."SystemValidTo"                                                                                            AS "BusinessPatner_SystemValidTo",
		bp."RiskCountry"                                                                                              AS "BusinessPatner_RiskCountry",
		bp."RelationshipStartDate"                                                                                    AS "BusinessPatner_RelationshipStartDate",
		
		/*-----ou = Identifier for OrganizationalUnit_View ------*/
		ou."OrganisationalUnitType"                                                                                   AS "OrganizationalUnit_administratedBy_OrganisationalUnitType",
		ou."OrganizationalUnitID"                                                                                     AS "OrganizationalUnit_administratedBy_OrganizationalUnitID",
		ou."OrganizationalUnitName"                                                                                   AS "OrganizationalUnit_administratedBy_OrganizationalUnitName",
		ou."OrganizationalUnitCategory"                                                                               AS "OrganizationalUnit_administratedBy_OrganizationalUnitCategory",
		ou."BusinessValidFrom"                                                                                        AS "OrganizationalUnit_administratedBy_BusinessValidFrom",
		ou."BusinessValidTo"                                                                                          AS "OrganizationalUnit_administratedBy_BusinessValidTo",
		ou."SystemValidFrom"                                                                                          AS "OrganizationalUnit_administratedBy_SystemValidFrom",
		ou."SystemValidTo"                                                                                            AS "OrganizationalUnit_administratedBy_SystemValidTo",
		
		/*-----paym = Identifier for PaymentSchedule_View ------*/
		paym."SequenceNumber"                                                                                         AS "PaymentSchedule_SequenceNumber",
		paym."BusinessValidFrom"                                                                                      AS "PaymentSchedule_BusinessValidFrom",
		paym."BusinessValidTo"                                                                                        AS "PaymentSchedule_BusinessValidTo",
		paym."SystemValidFrom"                                                                                        AS "PaymentSchedule_SystemValidFrom",
		paym."SystemValidTo"                                                                                          AS "PaymentSchedule_SystemValidTo",
		paym."PaymentScheduleStartDate"                                                                               AS "PaymentSchedule_PaymentScheduleStartDate",
		paym."PaymentScheduleType"                                                                                    AS "PaymentSchedule_PaymentScheduleType",
		paym."BusinessDayConvention"                                                                                  AS "PaymentSchedule_BusinessDayConvention",
		paym."PaymentScheduleCategory"                                                                                AS "PaymentSchedule_PaymentScheduleCategory",
		paym."FirstPaymentDate"                                                                                       AS "PaymentSchedule_FirstPaymentDate",
		paym."LastPaymentDate"                                                                                        AS "PaymentSchedule_LastPaymentDate",
		paym."InstallmentPeriodLength"                                                                                AS "PaymentSchedule_InstallmentPeriodLength",
		paym."InstallmentPeriodTimeUnit"                                                                              AS "PaymentSchedule_InstallmentPeriodTimeUnit",
		paym."DayOfMonthOfPayment"                                                                                    AS "PaymentSchedule_DayOfMonthOfPayment",
		paym."InstallmentAmount"                                                                                      AS "PaymentSchedule_InstallmentAmount",
		paym."InstallmentCurrency"                                                                                    AS "PaymentSchedule_InstallmentCurrency",
		paym."InstallmentsRelativeToInterestPaymentDates"                                                             AS "PaymentSchedule_InstallmentsRelativeToInterestPaymentDates",
		paym."InstallmentLagPeriodLength"                                                                             AS "PaymentSchedule_InstallmentLagPeriodLength",
		paym."InstallmentLagTimeUnit"                                                                                 AS "PaymentSchedule_InstallmentLagTimeUnit",
		paym."BulletPaymentDate"                                                                                      AS "PaymentSchedule_BulletPaymentDate",
		paym."BulletPaymentAmount"                                                                                    AS "PaymentSchedule_BulletPaymentAmount",
		paym."BulletPaymentCurrency"                                                                                  AS "PaymentSchedule_BulletPaymentCurrency"

    FROM "com.adweko.adapter.osx.synonyms::Receivable_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) rece
	    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ReceivableBusinessPartnerAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) bpca ON
	    		bpca."ASSOC_Receivable_ReceivableID"=rece."ReceivableID"
	    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartner_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) bp ON
	    		bp."BusinessPartnerID"=bpca."ASSOC_BusinessPartner_BusinessPartnerID"
	    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnit_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) ou ON
	    		ou."OrganizationalUnitID"=rece."ASSOC_OrganizationUnit_OrganizationalUnitID"
	    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::PaymentSchedule_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) paym ON
	    		paym."ASSOC_Receivable_ReceivableID"=rece."ReceivableID"	
	    