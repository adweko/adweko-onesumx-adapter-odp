
VIEW "com.adweko.adapter.osx.inputdata.risk::BV_OTCDerivative"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
AS
    SELECT
    
    	/*------ fc = Identifier for FinancialContract ------*/
    
    	fc."IDSystem"														AS "FinancialContract_IDSystem",
		fc."FinancialContractID"											AS "FinancialContract_FinancialContractID",
		fc."BusinessValidFrom"												AS "FinancialContract_BusinessValidFrom",
		fc."BusinessValidTo"												AS "FinancialContract_BusinessValidTo",
		fc."SystemValidFrom"												AS "FinancialContract_SystemValidFrom",
		fc."SystemValidTo"													AS "FinancialContract_SystemValidTo",
		fc."OTCDerivativeContractCategory"									AS "FinancialContract_OTCDerivativeContractCategory",
		fc."SwapCategory"													AS "FinancialContract_SwapCategory",
    	fc."InterestRateSwapCategory"										AS "FinancialContract_InterestRateSwapCategory",
    	fc."InterestRateOptionType"											AS "FinancialContract_InterestRateOptionType",
    	fc."SettlementCurrency"												AS "FinancialContract_SettlementCurrency",
		fc."OriginalSigningDate"											AS "FinancialContract_OriginalSigningDate",    	
		fc."EffectiveDate"			    									AS "FinancialContract_EffectiveDate",
	    fc."TerminationDate"												AS "FinancialContract_TerminationDate",
	    fc."NotionalAmount"													AS "FinancialContract_NotionalAmount",
	    fc."NotionalAmountCurrency"											AS "FinancialContract_NotionalAmountCurrency",
	    fc."Leg1NotionalAmount"												AS "FinancialContract_Leg1NotionalAmount",
		fc."Leg1NotionalAmountCurrency"										AS "FinancialContract_Leg1NotionalAmountCurrency",
		fc."Leg2NotionalAmount"												AS "FinancialContract_Leg2NotionalAmount",
		fc."Leg2NotionalAmountCurrency"										AS "FinancialContract_Leg2NotionalAmountCurrency",
		fc."PrincipalExchangeAtStart"										AS "FinancialContract_PrincipalExchangeAtStart",
		fc."PrincipalExchangeAtEnd"											AS "FinancialContract_PrincipalExchangeAtEnd",
		fc."PrincipalAdjustmentLagLength"									AS "FinancialContract_PrincipalAdjustmentLagLength",
		fc."LegWithAdjustedPrincipal"										AS "FinancialContract_LegWithAdjustedPrincipal",
		fc."PrincipalAdjustedForFXRate"										AS "FinancialContract_PrincipalAdjustedForFXRate",
		fc."Leg2AgainstLeg1FXRate"											AS "FinancialContract_Leg2AgainstLeg1FXRate",
		fc."TradedCurrency"													AS "FinancialContract_TradedCurrency",
		fc."QuoteCurrency"													AS "FinancialContract_QuoteCurrency",
		fc."AmountInTradedCurrency"											AS "FinancialContract_AmountInTradedCurrency",
		fc."FirstSettlementDate"											AS "FinancialContract_FirstSettlementDate",
		fc."PricePerTradedCurrencyUnitFirstSettlement"						AS "FinancialContract_PricePerTradedCurrencyUnitFirstSettlement",
		fc."SecondSettlementDate"											AS "FinancialContract_SecondSettlementDate",
		fc."PricePerTradedCurrencyUnitSecondSettlement"						AS "FinancialContract_PricePerTradedCurrencyUnitSecondSettlement",
		fc."SettlementMethod"												AS "FinancialContract_SettlementMethod",
		fc."SettlementDate"													AS "FinancialContract_SettlementDate",
		fc."PricePerTradedCurrencyUnit"										AS "FinancialContract_PricePerTradedCurrencyUnit",
		fc."MaturityDate"                                   				AS "FinancialContract_MaturityDate",
		fc."FirstCouponPaymentDate"                         				AS "FinancialContract_FirstAccrualStartDate",
		fc."FirstAccrualStartDate"                          				AS "FinancialContract_FirstCouponPaymnetDate",
		fc."InitialPaymentAmount"                           				AS "FinancialContract_InitialPaymentAmount",
		fc."InitialPaymentCurrency"                         				AS "FinancialContract_InitialPaymentCurrency",
		fc."InitialPaymentPayer"                            				AS "FinancialContract_InitialPaymentPayer",
--		fc."PurposeType"                                                    AS "FinancialContract_PurposeType",
--		fc."Purpose"                                                        AS "FinancialContract_Purpose",
		fc."CreditDefaultSwapCategory"                      				AS "FinancialContract_CreditDefaultSwapCategory",
		fc."OriginalNotionalAmount"                                         AS "FinancialContract_OriginalNotionalAmount",
		fc."MaximumNotionalAmount"                                          AS "FinancialContract_MaximumNotionalAmount",
		fc."TotalReturnSwapCategory"                                        AS "FinancialContract_TotalReturnSwapCategory",
		fc."GoverningLawCountry"                            				AS "FinancialContract_GoverningLawCountry",
		fc."LifecycleStatus"											    AS "FinancialContract_LifecycleStatus",
		
		fc."UnderlyingNotional"                                             AS "FinancialContract_UnderlyingNotional",
		fc."UnderlyingNotionalCurrency"                                     AS "FinancialContract_UnderlyingNotionalCurrency",
		fc."UnderlyingLegsHaveSameDirection"                                AS "FinancialContract_UnderlyingLegsHaveSameDirection",
		fc."UnderlyingEffectiveDate"                                        AS "FinancialContract_UnderlyingEffectiveDate",
		fc."UnderlyingTerminationDate"                                      AS "FinancialContract_UnderlyingTerminationDate",
		fc."UnderlyingSettlementCurrency"                                   AS "FinancialContract_UnderlyingSettlementCurrency",
		fc."UnderlyingNettedSettlement"                                     AS "FinancialContract_UnderlyingNettedSettlement",
		fc."CashSettlementMethod"                                           AS "FinancialContract_CashSettlementMethod",
		fc."CashSettlementDate"                                             AS "FinancialContract_CashSettlementDate",
		fc."CashSettlementTime"                                             AS "FinancialContract_CashSettlementTime",
		fc."CashSettlementTimeZone"                                         AS "FinancialContract_CashSettlementTimeZone",
		fc."SwaptionDirection"                                              AS "FinancialContract_SwaptionDirection",
		fc."ExerciseMethod"                                                 AS "FinancialContract_ExerciseMethod",
		fc."ExpirationDate"                                                 AS "FinancialContract_ExpirationDate",
		fc."ExerciseStyle"                                                  AS "FinancialContract_ExerciseStyle",
		fc."ExpirationTime"                                                 AS "FinancialContract_ExpirationTime",
		fc."ExpirationTimeZone"                                             AS "FinancialContract_ExpirationTimeZone",
		fc."ValuationBusinessCalendar"                                      AS "FinancialContract_ValuationBusinessCalendar",
		fc."ValuationBusinessDayConvention"                                 AS "FinancialContract_ValuationBusinessDayConvention",
		fc."SettlementBusinessCalendar"                                     AS "FinancialContract_SettlementBusinessCalendar",
		fc."SettlementBusinessDayConvention"                                AS "FinancialContract_SettlementBusinessDayConvention",
		fc."DeliveryDate"                                                   AS "FinancialContract_DeliveryDate",
		fc."OptionPremium"                                                  AS "FinancialContract_OptionPremium",
		fc."OptionPremiumCurrency"                                          AS "FinancialContract_OptionPremiumCurrency",
		fc."OptionPremiumPaymentDate"                                       AS "FinancialContract_OptionPremiumPaymentDate",
		fc."FRAFixingDate"                                                  AS "FinancialContract_FRAFixingDate",
		fc."IntendedAssetLiability"                                         AS "FinancialContract_IntendedAssetLiability",
		fc."FixedRate"                                      				AS "FinancialContract_FixedRate",
		fc."Interpolation"                                      			AS "FinancialContract_Interpolation",
		fc."FixingLagLength"                                        		AS "FinancialContract_FixingLagLength",
		fc."FixingLagTimeUnit"                                      		AS "FinancialContract_FixingLagTimeUnit",
		fc."SettlementInArrears"                                        	AS "FinancialContract_SettlementInArrears",
		fc."DiscountingMethod"                                      		AS "FinancialContract_DiscountingMethod",
		fc."RoundingMethodAmount"                                       	AS "FinancialContract_RoundingMethodAmount",
		fc."RoundingMethodReferenceRate"                                	AS "FinancialContract_RoundingMethodReferenceRate",
		fc."RateRoundingPrecision"                                      	AS "FinancialContract_RateRoundingPrecision",
		fc."RoundingMethodInterpolation"                                    AS "FinancialContract_RoundingMethodInterpolation",
		fc."InterpolationRoundingPrecision"                                 AS "FinancialContract_InterpolationRoundingPrecision",
		fc."DayCountConvention"                                         	AS "FinancialContract_DayCountConvention",
		fc."BusinessCalendar"                                       		AS "FinancialContract_BusinessCalendar",
		fc."_ReferenceRate_ReferenceRateID"                                 AS "FinancialContract_ReferenceRate_ReferenceRateID",

		/*------ csd = Identifier for ContractStatus in Category DefaultStatus ------*/
	
		csd."ContractStatusType"											AS "ContractStatus_DefaultStatus_ContractStatusType",
		csd."BusinessValidFrom"												AS "ContractStatus_DefaultStatus_BusinessValidFrom",
		csd."BusinessValidTo"												AS "ContractStatus_DefaultStatus_BusinessValidTo",
		csd."SystemValidFrom"												AS "ContractStatus_DefaultStatus_SystemValidFrom",
		csd."SystemValidTo"													AS "ContractStatus_DefaultStatus_SystemValidTo",
		csd."Status"														AS "ContractStatus_DefaultStatus_Status",
	
		/*------ csp = Identifier for ContractStatus in Category PastDueStatus-----*/
		csp."ContractStatusType"                            				AS "ContractStatus_PastDueStatus_ContractStatusType",
		csp."BusinessValidFrom"                             				AS "ContractStatus_PastDueStatus_BusinessValidFrom",
		csp."BusinessValidTo"                               				AS "ContractStatus_PastDueStatus_BusinessValidTo",
		csp."SystemValidFrom"                               				AS "ContractStatus_PastDueStatus_SystemValidFrom",
		csp."SystemValidTo"                                 				AS "ContractStatus_PastDueStatus_SystemValidTo",
		csp."Status"                                        				AS "ContractStatus_PastDueStatus_Status",
		csp."ChangeTimestampInSourceSystem"                 				AS "ContractStatus_PastDueStatus_ChangeTimestampInSourceSystem",
		
		/*------ bpca = Identifier for BusinessPartnerContractAssignment ------*/
	
		bpcaBuyer."ASSOC_PartnerInParticipation_BusinessPartnerID"   		AS "BusinessPartnerContractAssignment_Buyer_Counterparty_PartnerInParticipation_BusinessPartnerID",
		bpcaBuyer."BusinessValidFrom"										AS "BusinessPartnerContractAssignment_Buyer_Counterparty_BusinessValidFrom",
		bpcaBuyer."BusinessValidTo"							    			AS "BusinessPartnerContractAssignment_Buyer_Counterparty_BusinessValidTo",
		bpcaBuyer."SystemValidFrom"											AS "BusinessPartnerContractAssignment_Buyer_Counterparty_SystemValidFrom",
		bpcaBuyer."SystemValidTo"											AS "BusinessPartnerContractAssignment_Buyer_Counterparty_SystemValidTo",
		bpcaBuyer."Role"													AS "BusinessPartnerContractAssignment_Buyer_Counterparty_Role",
		bpcaBuyer."ContractDataOwner"										AS "BusinessPartnerContractAssignment_Buyer_Counterparty_ContractDataOwner",

		bpcaSeller."ASSOC_PartnerInParticipation_BusinessPartnerID"   		AS "BusinessPartnerContractAssignment_Seller_Counterparty_PartnerInParticipation_BusinessPartnerID",
		bpcaSeller."BusinessValidFrom"										AS "BusinessPartnerContractAssignment_Seller_Counterparty_BusinessValidFrom",
		bpcaSeller."BusinessValidTo"							    		AS "BusinessPartnerContractAssignment_Seller_Counterparty_BusinessValidTo",
		bpcaSeller."SystemValidFrom"										AS "BusinessPartnerContractAssignment_Seller_Counterparty_SystemValidFrom",
		bpcaSeller."SystemValidTo"											AS "BusinessPartnerContractAssignment_Seller_Counterparty_SystemValidTo",
		bpcaSeller."Role"													AS "BusinessPartnerContractAssignment_Seller_Counterparty_Role",
		bpcaSeller."ContractDataOwner"										AS "BusinessPartnerContractAssignment_Seller_Counterparty_ContractDataOwner",
		
		bpcac."ASSOC_PartnerInParticipation_BusinessPartnerID"     			AS "BusinessPartnerContractAssignment_ClearingMember_PartnerInParticipation_BusinessPartnerID",
		bpcac."BusinessValidFrom"                            				AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidFrom",
		bpcac."BusinessValidTo"                              				AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidTo",
		bpcac."SystemValidFrom"                              				AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidFrom",
		bpcac."SystemValidTo"                                				AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidTo",
		
		/*------ bpcao = Identifier for BusinessPartnerContractAssignment ------*/
		bpcao."ASSOC_PartnerInParticipation_BusinessPartnerID"     			AS "BusinessPartnerContractAssignment_ContractDataOwner_PartnerInParticipation_BusinessPartnerID",
		bpcao."BusinessValidFrom"                            				AS "BusinessPartnerContractAssignment_ContractDataOwner_BusinessValidFrom",
		bpcao."BusinessValidTo"                              				AS "BusinessPartnerContractAssignment_ContractDataOwner_BusinessValidTo",
		bpcao."SystemValidFrom"                              				AS "BusinessPartnerContractAssignment_ContractDataOwner_SystemValidFrom",
		bpcao."SystemValidTo"                                				AS "BusinessPartnerContractAssignment_ContractDataOwner_SystemValidTo",	
		
		bpLeg1Payer."ContractDataOwner"										AS "BusinessPartnerContractAssignment_Leg1Payer_ContractDataOwner",
		bpLeg2Payer."ContractDataOwner"										AS "BusinessPartnerContractAssignment_Leg2Payer_ContractDataOwner",
		
		bpSpotSeller."ContractDataOwner"									AS "BusinessPartnerContractAssignment_SpotSeller_ContractDataOwner",
		bpSpotBuyer."ContractDataOwner"										AS "BusinessPartnerContractAssignment_SpotBuyer_ContractDataOwner",


		/*------ ouca = Identifier for OrganizationalUnitContractAssignment ------*/
	
		ouca."ASSOC_OrgUnit_OrganizationalUnitID"										   AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationalUnitID",
		ouca."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID" AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationHostingOrganizationalUnit_BusinessPartnerID",
		ouca."BusinessValidFrom"														   AS "OrganizationalUnitContractAssignment_BusinessValidFrom",
		ouca."BusinessValidTo"							    							   AS "OrganizationalUnitContractAssignment_BusinessValidTo",
		ouca."SystemValidFrom"															   AS "OrganizationalUnitContractAssignment_SystemValidFrom",
		ouca."SystemValidTo"															   AS "OrganizationalUnitContractAssignment_SystemValidTo",
		ouca."RoleOfOrganizationalUnit"													   AS "OrganizationalUnitContractAssignment_RoleOfOrganizationalUnit",
		
		/*------ inte = Identifier for Interest ------*/	
		inte."SequenceNumber"												AS "Interest_SequenceNumber",
		inte."BusinessValidFrom"											AS "Interest_BusinessValidFrom",
		inte."BusinessValidTo"												AS "Interest_BusinessValidTo",
		inte."SystemValidFrom"												AS "Interest_SystemValidFrom",
		inte."SystemValidTo"												AS "Interest_SystemValidTo",
		inte."InterestType"													AS "Interest_InterestType",
		inte."PayingOrReceiving"											AS "Interest_PayingOrReceiving",
		inte."RoleOfPayer"													AS "Interest_RoleOfPayer",
		inte."InterestCategory"												AS "Interest_InterestCategory",
		inte."FirstInterestPeriodStartDate"									AS "Interest_FirstInterestPeriodStartDate",
		inte."FirstInterestPeriodEndDate"									AS "Interest_FirstInterestPeriodEndDate",
		inte."LastInterestPeriodEndDate"									AS "Interest_LastInterestPeriodEndDate",
		inte."InterestCurrency"												AS "Interest_InterestCurrency",	
		inte."InterestPeriodLength"											AS "Interest_InterestPeriodLength",
		inte."InterestPeriodTimeUnit"										AS "Interest_InterestPeriodTimeUnit",
		inte."DayOfMonthOfInterestPeriodEnd"								AS "Interest_DayOfMonthOfInterestPeriodEnd",
		inte."InterestBusinessCalendar"										AS "Interest_InterestBusinessCalendar",
		inte."BusinessDayConvention"										AS "Interest_BusinessDayConvention",		
		inte."InterestInAdvance"											AS "Interest_InterestInAdvance",
		inte."InterestSpecificationCategory"								AS "Interest_InterestSpecificationCategory", 
		inte."ASSOC_ReferenceRateID_ReferenceRateID"						AS "Interest_ReferenceRateID",
		inte."FloatingRateMin"												AS "Interest_FloatingRateMin",
		inte."FloatingRateMax"												AS "Interest_FloatingRateMax",
		inte."VariableRateMin"												AS "Interest_VariableRateMin",
		inte."VariableRateMax"												AS "Interest_VariableRateMax",
		inte."Spread"														AS "Interest_Spread",
		inte."ReferenceRateFactor"											AS "Interest_ReferenceRateFactor",
		inte."ResetCutoffLength" 											AS "Interest_ResetCutoffLength",
		inte."ResetCutoffTimeUnit" 											AS "Interest_ResetCutoffTimeUnit",
		inte."FixingRateSpecificationCategory"								AS "Interest_FixingRateSpecificationCategory",
		inte."ResetPeriodLength"											AS "Interest_ResetPeriodLength",
		inte."ResetPeriodTimeUnit"											AS "Interest_ResetPeriodTimeUnit",
		inte."FirstRegularFloatingRateResetDate"							AS "Interest_FirstRegularFloatingRateResetDate",
		inte."ResetBusinessCalendar"										AS "Interest_ResetBusinessCalendar",
		inte."ResetBusinessDayConvention"									AS "Interest_ResetBusinessDayConvention",
		inte."ResetLagLength"												AS "Interest_ResetLagLength",
		inte."ResetLagTimeUnit"												AS "Interest_ResetLagTimeUnit",
		inte."DayCountConvention"											AS "Interest_DayCountConvention",
		inte."FixedRate"													AS "Interest_FixedRate",
		inte."AnnuityAmount"												AS "Interest_AnnuityAmount",
		inte."AnnuityAmountCurrency"										AS "Interest_AnnuityAmountCurrency",
		inte."PreconditionApplies"                                          AS "Interest_PreconditionApplies",
		inte."ScaleApplies"                                                 AS "Interest_ScaleApplies",

		
		/*------ appl = Identifier for ApplicableInterestRate ------*/	
	
		appl."BusinessValidFrom"											AS "ApplicableInterestRate_BusinessValidFrom",
		appl."BusinessValidTo"												AS "ApplicableInterestRate_BusinessValidTo",
		appl."SystemValidFrom"												AS "ApplicableInterestRate_SystemValidFrom",
		appl."SystemValidTo"												AS "ApplicableInterestRate_SystemValidTo",
		appl."Rate" 														AS "ApplicableInterestRate_Rate",
		appl."LastResetDate"												AS "ApplicableInterestRate_LastResetDate",
		appl."NextResetDate"												AS "ApplicableInterestRate_NextResetDate",

		/*------ pea = Identifier for PrincipalExchangeSchedule ------*/	
	
		pea."BusinessValidFrom"												AS "PrincipalExchangeSchedule_BusinessValidFrom",
		pea."BusinessValidTo"												AS "PrincipalExchangeSchedule_BusinessValidTo",
		pea."SystemValidFrom"												AS "PrincipalExchangeSchedule_SystemValidFrom",
		pea."SystemValidTo"													AS "PrincipalExchangeSchedule_SystemValidTo",
		pea."BusinessCalendar"												AS "PrincipalExchangeSchedule_BusinessCalendar",
		pea."BusinessDayConvention"											AS "PrincipalExchangeSchedule_BusinessDayConvention",
		pea."PrincipalExchangeDate"											AS "PrincipalExchangeSchedule_PrincipalExchangeDate",
		pea."PrincipalExchangeAmount"										AS "PrincipalExchangeSchedule_PrincipalExchangeAmount",
		pea."PrincipalExchangeAmountCurrency"								AS "PrincipalExchangeSchedule_PrincipalExchangeAmountCurrency",
 
		/*-----pca = Identifier for ProductCatalogAssignment_View ------*/
		'FSDMStandard' 										  					AS "ProductCatalogAssignment_ProductCatalogItem_ProductCatalog_CatalogID",
		pca."StandardCatalog_ProductCatalogItem"			     				AS "ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",
		pca."BusinessValidFrom"											        AS "ProductCatalogAssignment_BusinessValidFrom",
		pca."BusinessValidTo"											        AS "ProductCatalogAssignment_BusinessValidTo",
		pca."StandardCatalog_ProductCatalogItem",
		pca."CustomCatalog_ProductCatalogItem",
     
    	/*------ cdsp = Identifier for CreditDefaultSwapProtectionLeg_View ------*/   

		cdsp."BusinessValidFrom"                                            AS "CreditDefaultSwapProtectionLeg_BusinessValidFrom",
		cdsp."BusinessValidTo"                                              AS "CreditDefaultSwapProtectionLeg_BusinessValidTo",
		cdsp."SystemValidFrom"                                              AS "CreditDefaultSwapProtectionLeg_SystemValidFrom",
		cdsp."SystemValidTo"                                                AS "CreditDefaultSwapProtectionLeg_SystemValidTo",
		cdsp."_CDSIndex_IndexID"                                            AS "CreditDefaultSwapProtectionLeg_CDSIndex_IndexID",
		cdsp."_Basket_FinancialInstrumentID"                                AS "CreditDefaultSwapProtectionLeg_Basket_FinancialInstrumentID",
		cdsp."_DebtInstrument_FinancialInstrumentID"                        AS "CreditDefaultSwapProtectionLeg_DebtInstrument_FinancialInstrumentID",
		cdsp."ProtectionStartDate"                                          AS "CreditDefaultSwapProtectionLeg_ProtectionStartDate",
		cdsp."ProtectionCurrency"                                           AS "CreditDefaultSwapProtectionLeg_ProtectionCurrency",
		cdsp."ReferencePrice"                                               AS "CreditDefaultSwapProtectionLeg_ReferencePrice",
		cdsp."CreditEventBackstopDate"                                      AS "CreditDefaultSwapProtectionLeg_CreditEventBackstopDate",
		cdsp."ReferenceEntityPairCode"                                      AS "CreditDefaultSwapProtectionLeg_ReferenceEntityPairCode",
		
		/*------ cdsre = Identifier for CreditDefaultSwapReferenceEntity_View ------*/	
		
		cdsre."_Basket_FinancialInstrumentID"        						AS "CreditDefaultSwapReferenceEntity_Basket_FinancialInstrumentID",
		cdsre."_ProtectionLeg__CreditDefaultSwap_FinancialContractID"       AS "CreditDefaultSwapReferenceEntity_ProtectionLeg_CreditDefaultSwap_FinancialContractID",
		cdsre."_ProtectionLeg__CreditDefaultSwap_IDSystem"        			AS "CreditDefaultSwapReferenceEntity_ProtectionLeg_CreditDefaultSwap_IDSystem",
		cdsre."_ReferenceEntity_BusinessPartnerID"       					AS "CreditDefaultSwapReferenceEntity_ReferenceEntity_BusinessPartnerID",
		cdsre."IsEntityExcluded"                                            AS "CreditDefaultSwapReferenceEntity_IsEntityExcluded",
		cdsre."ReferenceEntityPairCode"                                     AS "CreditDefaultSwapReferenceEntity_ReferenceEntityPairCode",
		
		/*------ bp = Identifier for BusinessPartner_View ------*/

		bp."RelationshipStartDate"                                          AS "BusinessPartner_RelationshipStartDate",
		bp."RiskCountry"                                                    AS "BusinessPartner_RiskCountry",

		/*------ trsrl = Identifier for TotalReturnSwapReturnLeg_View ------*/

		trsrl."_Underlying_FinancialInstrumentID"                           AS "TotalReturnSwapReturnLeg_Underlying_FinancialInstrumentID",
		trsrl."_UnderlyingIndex_IndexID"                                    AS "TotalReturnSwapReturnLeg_UnderlyingIndex_IndexID",
		trsrl."BusinessValidFrom"                                           AS "TotalReturnSwapReturnLeg_BusinessValidFrom",
		trsrl."BusinessValidTo"                                             AS "TotalReturnSwapReturnLeg_BusinessValidTo",
		trsrl."SystemValidFrom"                                             AS "TotalReturnSwapReturnLeg_SystemValidFrom",
		trsrl."SystemValidTo"                                               AS "TotalReturnSwapReturnLeg_SystemValidTo",
		
		/*-----paym = Identifier for PaymentSchedule_View ------*/	
		paym."SequenceNumber"       											AS "PaymentSchedule_SequenceNumber",
		paym."BusinessValidFrom"        										AS "PaymentSchedule_BusinessValidFrom",
		paym."BusinessValidTo"      											AS "PaymentSchedule_BusinessValidTo",
		paym."SystemValidFrom"											        AS "PaymentSchedule_SystemValidFrom",
		paym."SystemValidTo"											        AS "PaymentSchedule_SystemValidTo",
		paym."PaymentScheduleStartDate"									        AS "PaymentSchedule_PaymentScheduleStartDate",
		paym."PaymentScheduleType"										        AS "PaymentSchedule_PaymentScheduleType",
		paym."BusinessDayConvention"									        AS "PaymentSchedule_BusinessDayConvention",
		paym."PaymentScheduleCategory"      									AS "PaymentSchedule_PaymentScheduleCategory",
		paym."FirstPaymentDate"											        AS "PaymentSchedule_FirstPaymentDate",
		paym."LastPaymentDate"      											AS "PaymentSchedule_LastPaymentDate",
		paym."InstallmentPeriodLength"										    AS "PaymentSchedule_InstallmentPeriodLength",
		paym."InstallmentPeriodTimeUnit"								        AS "PaymentSchedule_InstallmentPeriodTimeUnit",
		paym."DayOfMonthOfPayment"										        AS "PaymentSchedule_DayOfMonthOfPayment",
		paym."InstallmentAmount"										        AS "PaymentSchedule_InstallmentAmount",
		paym."InstallmentCurrency"										        AS "PaymentSchedule_InstallmentCurrency",
		paym."InstallmentsRelativeToInterestPaymentDates"				        AS "PaymentSchedule_InstallmentsRelativeToInterestPaymentDates",
		paym."InstallmentLagPeriodLength"								        AS "PaymentSchedule_InstallmentLagPeriodLength",
		paym."InstallmentLagTimeUnit"									        AS "PaymentSchedule_InstallmentLagTimeUnit",
		paym."BulletPaymentDate"										        AS "PaymentSchedule_BulletPaymentDate",
		paym."BulletPaymentAmount"										        AS "PaymentSchedule_BulletPaymentAmount",
		paym."BulletPaymentCurrency"									        AS "PaymentSchedule_BulletPaymentCurrency",
		paym."BusinessCalendar"													AS "PaymentSchedule_BusinessCalendar",
		paym."PaymentToBeRequested"												AS "PaymentSchedule_PaymentToBeRequested",
		paym."PartialPaymentRequestsPermitted"									AS "PaymentSchedule_PartialPaymentRequestsPermitted",
		paym."RoleOfPayer"														AS "PaymentSchedule_RoleOfPayer",
	
		/*-----net = Identifier for NettingSetContractAssignment_View ------*/	
		net."_NettingSet_NettingSetID"                      					AS "NettingSetContractAssignment_NettingSet_NettingSetID",
		net."_NettingSet__MasterAgreement_FinancialContractID"					AS "NettingSetContractAssignment_NettingSet_MasterAgreement_FinancialContractID",
		net."_NettingSet__MasterAgreement_IDSystem"         					AS "NettingSetContractAssignment_NettingSet_MasterAgreement_IDSystem",
		net."BusinessValidFrom"                             					AS "NettingSetContractAssignment_BusinessValidFrom",
		net."BusinessValidTo"                               					AS "NettingSetContractAssignment_BusinessValidTo",
		net."SystemValidFrom"                               					AS "NettingSetContractAssignment_SystemValidFrom",
		net."SystemValidTo"                                 					AS "NettingSetContractAssignment_SystemValidTo",

		/*-----riff = Identifier for RegulatoryIndicatorForFinancialContract_View ------*/
		riff."BusinessValidFrom"                                                AS "RegulatoryIndicatorForFinancialContract_BusinessValidFrom",
		riff."BusinessValidTo"                                                  AS "RegulatoryIndicatorForFinancialContract_BusinessValidTo",
		riff."SystemValidFrom"                                                  AS "RegulatoryIndicatorForFinancialContract_SystemValidFrom",
		riff."SystemValidTo"                                                    AS "RegulatoryIndicatorForFinancialContract_SystemValidTo",
		riff."Criterion"                                                        AS "RegulatoryIndicatorForFinancialContract_Criterion",
		riff."IndicatorCharValue"                                               AS "RegulatoryIndicatorForFinancialContract_IndicatorCharValue",
		riff."Regulation"														AS "RegulatoryIndicatorForFinancialContract_Regulation",

		ep."StartDate"                                                          AS "ExercisePeriod_StartDate",
		ep."EndDate"                                                            AS "ExercisePeriod_EndDate",
		ep."BusinessValidFrom"                                                  AS "ExercisePeriod_BusinessValidFrom",
		ep."BusinessValidTo"                                                    AS "ExercisePeriod_BusinessValidTo",
		ep."SystemValidFrom"                                                    AS "ExercisePeriod_SystemValidFrom",
		ep."SystemValidTo"                                                      AS "ExercisePeriod_SystemValidTo"

	
 	
	FROM "com.adweko.adapter.osx.synonyms::FinancialContract_View"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS fc		

        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS csd ON 
                    csd."ASSOC_FinancialContract_FinancialContractID"				= fc."FinancialContractID" 
                AND csd."ASSOC_FinancialContract_IDSystem"							= fc."IDSystem"
				AND csd."ContractStatusCategory"									= 'DefaultStatus'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS csp ON 
                    csp."ASSOC_FinancialContract_FinancialContractID"				= fc."FinancialContractID" 
                AND csp."ASSOC_FinancialContract_IDSystem"							= fc."IDSystem"
				AND csp."ContractStatusCategory"									= 'PastDueStatus'
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS bpcaBuyer ON 
                    bpcaBuyer."ASSOC_FinancialContract_FinancialContractID"				= fc."FinancialContractID" 
                AND bpcaBuyer."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
				AND bpcaBuyer."Role"													= 'Buyer'   
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS bpcaSeller ON 
                    bpcaSeller."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
                AND bpcaSeller."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
				AND bpcaSeller."Role"													= 'Seller' 
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bpcac  ON 
					bpcac."ASSOC_FinancialContract_FinancialContractID"				= fc."FinancialContractID" 
				AND bpcac."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
				AND bpcac."Role"													= 'ClearingMember'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bpLeg1Payer  ON 
					bpLeg1Payer."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
				AND bpLeg1Payer."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
				AND bpLeg1Payer."Role"													= 'Leg1Payer'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bpLeg2Payer  ON 
					bpLeg2Payer."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
				AND bpLeg2Payer."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
				AND bpLeg2Payer."Role"													= 'Leg2Payer'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bpSpotSeller  ON 
					bpSpotSeller."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
				AND bpSpotSeller."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
				AND bpSpotSeller."Role"													= 'SpotSeller'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bpSpotBuyer  ON 
					bpSpotBuyer."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
				AND bpSpotBuyer."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
				AND bpSpotBuyer."Role"													= 'SpotBuyer'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartner_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS bp ON 
                    bp."BusinessPartnerID"											= bpcac."ASSOC_PartnerInParticipation_BusinessPartnerID" 
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bpcao ON
				bpcao."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"
			AND bpcao."ASSOC_FinancialContract_IDSystem"					= fc."IDSystem"
			AND bpcao."ContractDataOwner"									= true	            
                    
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS ouca ON 
                    ouca."ASSOC_Contract_FinancialContractID"						= fc."FinancialContractID" 
                AND ouca."ASSOC_Contract_IDSystem"									= fc."IDSystem"
                AND ouca."RoleOfOrganizationalUnit"									= 'ManagingUnit'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Interest_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS inte ON 
                    inte."ASSOC_FinancialContract_FinancialContractID"				= fc."FinancialContractID" 
                AND inte."ASSOC_FinancialContract_IDSystem"							= fc."IDSystem"                    
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ApplicableInterestRate_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS appl ON 
                    appl."ASSOC_FinancialContract_FinancialContractID"				= fc."FinancialContractID" 
                AND appl."ASSOC_FinancialContract_IDSystem"							= fc."IDSystem"   
                AND appl."RoleOfPayer"												= inte."RoleOfPayer"
                AND appl."InterestType"												= inte."InterestType"                
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::PrincipalExchangeSchedule_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS pea ON 
                    pea."_SwapForSchedule_IDSystem"									= fc."IDSystem" 
                AND pea."_SwapForSchedule_FinancialContractID"						= fc."FinancialContractID"
                AND pea."RoleOfPayer"												= inte."RoleOfPayer"             
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS pca ON 
            	pca."IDSystem"							= fc."IDSystem" 
            AND pca."FinancialContractID"				= fc."FinancialContractID"     
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::CreditDefaultSwapProtectionLeg_View"(ADD_Seconds(:I_BUSINESS_DATE, 24*60*60-1) , :I_SYSTEM_TIMESTAMP  )  AS cdsp ON 
                    cdsp."_CreditDefaultSwap_IDSystem"								= fc."IDSystem" 
                AND cdsp."_CreditDefaultSwap_FinancialContractID"					= fc."FinancialContractID"   
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::CreditDefaultSwapReferenceEntity_View"(ADD_Seconds(:I_BUSINESS_DATE, 24*60*60-1) , :I_SYSTEM_TIMESTAMP  )  AS cdsre ON 
                    cdsre."_ProtectionLeg__CreditDefaultSwap_IDSystem"				= cdsp."_CreditDefaultSwap_IDSystem" 
                AND cdsre."_ProtectionLeg__CreditDefaultSwap_FinancialContractID"	= cdsp."_CreditDefaultSwap_FinancialContractID"   
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::TotalReturnSwapReturnLeg_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS trsrl ON 
                    trsrl."_Swap_IDSystem"											= fc."IDSystem" 
                AND trsrl."_Swap_FinancialContractID"								= fc."FinancialContractID"   
	    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::PaymentSchedule_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  paym ON 
                    paym."ASSOC_FinancialContract_IDSystem"							= fc."IDSystem"                    
                AND paym."ASSOC_FinancialContract_FinancialContractID"				= fc."FinancialContractID"   
	    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::NettingSetContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS net ON
	    			net."_FinancialContract_IDSystem"								= fc."IDSystem"
                AND net."_FinancialContract_FinancialContractID"					= fc."FinancialContractID"
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::RegulatoryIndicatorForFinancialContract_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS riff ON
	    			 riff."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
                AND  riff."ASSOC_FinancialContract_FinancialContractID"				= fc."FinancialContractID"       
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ExercisePeriod_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS ep ON
	    			 (ep."_Swaption_IDSystem"										= fc."IDSystem"
                AND  ep."_Swaption_FinancialContractID"								= fc."FinancialContractID")
                OR
                	(ep."_Option_IDSystem"											= fc."IDSystem"
                AND  ep."_Option_FinancialContractID"								= fc."FinancialContractID")
                
	WHERE 	fc."FinancialContractCategory" =  'OTCDerivative'
