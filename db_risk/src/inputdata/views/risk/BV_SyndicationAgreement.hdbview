VIEW "com.adweko.adapter.osx.inputdata.risk::BV_SyndicationAgreement"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
AS
        SELECT
        /*------ fc = Identifier for FinancialContract ------*/
        fc."IDSystem"                                                                                                 AS "FinancialContract_IDSystem",
		fc."FinancialContractID"                                                                                      AS "FinancialContract_FinancialContractID",
		fc."BusinessValidFrom"                                                                                        AS "FinancialContract_BusinessValidFrom",
		fc."BusinessValidTo"                                                                                          AS "FinancialContract_BusinessValidTo",
		fc."SystemValidFrom"                                                                                          AS "FinancialContract_SystemValidFrom",
		fc."SystemValidTo"                                                                                            AS "FinancialContract_SystemValidTo",
		fc."OriginalSigningDate"                                                                                      AS "FinancialContract_OriginalSigningDate",
		fc."GoverningLawCountry"                                                                                      AS "FinancialContract_GoverningLawCountry",
		fc."Purpose"                                                                                                  AS "FinancialContract_Purpose",
		fc."FinancialContractCategory"                                                                                AS "FinancialContract_FinancialContractCategory",
		fc."PurposeType"                                                                                              AS "FinancialContract_PurposeType",
		fc."GracePeriod"                                                                                              AS "FinancialContract_GracePeriod",
		fc."LifecycleStatus"                                                                                          AS "FinancialContract_LifecycleStatus",
		fc."AgreementStartDate"                                                                                       AS "FinancialContract_AgreementStartDate",
		fc."AgreementEndDate"                                                                                         AS "FinancialContract_AgreementEndDate",
		fc."ApprovalDate"                                                                                             AS "FinancialContract_ApprovalDate",
		fc."TotalFinancingAmount"                                                                                     AS "FinancialContract_TotalFinancingAmount",
		fc."FinancingAmountCurrency"                                                                                  AS "FinancialContract_FinancingAmountCurrency",
		fc."RiskCountry"                                                                                              AS "FinancialContract_RiskCountry",
		fc."RiskStatus"                                                                                               AS "FinancialContract_RiskStatus",
		fc."SyndicatedContractIdentifier"                                                                             AS "FinancialContract_SyndicatedContractIdentifier",

		/*------ tra = Identifier for TrancheInSyndication ------*/
		tra."BusinessValidFrom"                                                                                       AS "TrancheInSyndication_BusinessValidFrom",
		tra."BusinessValidTo"                                                                                         AS "TrancheInSyndication_BusinessValidTo",
		tra."SystemValidFrom"                                                                                         AS "TrancheInSyndication_SystemValidFrom",
		tra."SystemValidTo"                                                                                           AS "TrancheInSyndication_SystemValidTo",
		tra."TrancheSequenceNumber"                                                                                   AS "TrancheInSyndication_TrancheSequenceNumber",
		tra."TrancheStartDate"                                                                                        AS "TrancheInSyndication_TrancheStartDate",
		tra."TrancheEndDate"                                                                                          AS "TrancheInSyndication_TrancheEndDate",
		tra."TrancheType"                                                                                             AS "TrancheInSyndication_TrancheType",
		tra."TrancheAmount"                                                                                           AS "TrancheInSyndication_TrancheAmount",
		tra."TrancheCurrency"                                                                                         AS "TrancheInSyndication_TrancheCurrency",
		tra."IsTrancheRevolving"                                                                                      AS "TrancheInSyndication_IsTrancheRevolving",
		tra."Status"                                                                                                  AS "TrancheInSyndication_Status",

		/*------ csd = Identifier for ContractStatus ------*/
		csd."ContractStatusType"                                                                                      AS "ContractStatus_DefaultStatus_ContractStatusType",
		csd."BusinessValidFrom"                                                                                       AS "ContractStatus_DefaultStatus_BusinessValidFrom",
		csd."BusinessValidTo"                                                                                         AS "ContractStatus_DefaultStatus_BusinessValidTo",
		csd."SystemValidFrom"                                                                                         AS "ContractStatus_DefaultStatus_SystemValidFrom",
		csd."SystemValidTo"                                                                                           AS "ContractStatus_DefaultStatus_SystemValidTo",
		csd."Status"                                                                                                  AS "ContractStatus_DefaultStatus_Status",

		/*------ csp = Identifier for ContractStatus in Category PastDueStatus-----*/
		csp."ContractStatusType"                                                                                      AS "ContractStatus_PastDueStatus_ContractStatusType",
		csp."BusinessValidFrom"                                                                                       AS "ContractStatus_PastDueStatus_BusinessValidFrom",
		csp."BusinessValidTo"                                                                                         AS "ContractStatus_PastDueStatus_BusinessValidTo",
		csp."SystemValidFrom"                                                                                         AS "ContractStatus_PastDueStatus_SystemValidFrom",
		csp."SystemValidTo"                                                                                           AS "ContractStatus_PastDueStatus_SystemValidTo",
		csp."Status"                                                                                                  AS "ContractStatus_PastDueStatus_Status",
		csp."ChangeTimestampInSourceSystem"                                                                           AS "ContractStatus_PastDueStatus_ChangeTimestampInSourceSystem",
	
		/*------ pca = Identifier for ProductCatalogAssignment ------*/
		pca."_ProductCatalogItem__ProductCatalog_CatalogID"                                                           AS "ProductCatalogAssignment_ProductCatalogItem_ProductCatalog_CatalogID",
		pca."_ProductCatalogItem_ProductCatalogItem"                                                                  AS "ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",
		pca."ProductCatalogAssignmentCategory"                                                                        AS "ProductCatalogAssignment_ProductCatalogAssignmentCategory",
		pca."BusinessValidFrom"                                                                                       AS "ProductCatalogAssignment_BusinessValidFrom",
		pca."BusinessValidTo"                                                                                         AS "ProductCatalogAssignment_BusinessValidTo",
		pca."SystemValidFrom"                                                                                         AS "ProductCatalogAssignment_SystemValidFrom",
		pca."SystemValidTo"                                                                                           AS "ProductCatalogAssignment_SystemValidTo",

		/*------ bpca = Identifier for BusinessPartnerContractAssignment ------*/
		bpca."ASSOC_PartnerInParticipation_BusinessPartnerID"                                                         AS "BusinessPartnerContractAssignment_Borrower_PartnerInParticipation_BusinessPartnerID",
		bpca."BusinessValidFrom"                                                                                      AS "BusinessPartnerContractAssignment_Borrower_BusinessValidFrom",
		bpca."BusinessValidTo"                                                                                        AS "BusinessPartnerContractAssignment_Borrower_BusinessValidTo",
		bpca."SystemValidFrom"                                                                                        AS "BusinessPartnerContractAssignment_Borrower_SystemValidFrom",
		bpca."SystemValidTo"                                                                                          AS "BusinessPartnerContractAssignment_Borrower_SystemValidTo",
		
		/*------ bpcl = Identifier for BusinessPartnerContractAssignment ------*/
		bpcl."ASSOC_PartnerInParticipation_BusinessPartnerID"                                                         AS "BusinessPartnerContractAssignment_Lender_PartnerInParticipation_BusinessPartnerID",
		bpcl."BusinessValidFrom"                                                                                      AS "BusinessPartnerContractAssignment_Lender_BusinessValidFrom",
		bpcl."BusinessValidTo"                                                                                        AS "BusinessPartnerContractAssignment_Lender_BusinessValidTo",
		bpcl."SystemValidFrom"                                                                                        AS "BusinessPartnerContractAssignment_Lender_SystemValidFrom",
		bpcl."SystemValidTo"                                                                                          AS "BusinessPartnerContractAssignment_Lender_SystemValidTo",

		
		/*------ bpcac = Identifier for BusinessPartnerContractAssignment ------*/
		bpcac."ASSOC_PartnerInParticipation_BusinessPartnerID"                                                        AS "BusinessPartnerContractAssignment_ClearingMember_PartnerInParticipation_BusinessPartnerID",
		bpcac."BusinessValidFrom"                                                                                     AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidFrom",
		bpcac."BusinessValidTo"                                                                                       AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidTo",
		bpcac."SystemValidFrom"                                                                                       AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidFrom",
		bpcac."SystemValidTo"                                                                                         AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidTo",

		
		/*------ ouca = Identifier for OrganizationalUnitContractAssignment ------*/
		ouca."RoleOfOrganizationalUnit"                                                                               AS "OrganizationalUnitContractAssignment_RoleOfOrganizationalUnit",
		ouca."ASSOC_OrgUnit_OrganizationalUnitID"                                                                     AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationalUnitID",
		ouca."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID"                            AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationHostingOrganizationalUnit_BusinessPartnerID",
		ouca."BusinessValidFrom"                                                                                      AS "OrganizationalUnitContractAssignment_BusinessValidFrom",
		ouca."BusinessValidTo"                                                                                        AS "OrganizationalUnitContractAssignment_BusinessValidTo",
		ouca."SystemValidFrom"                                                                                        AS "OrganizationalUnitContractAssignment_SystemValidFrom",
		ouca."SystemValidTo"                                                                                          AS "OrganizationalUnitContractAssignment_SystemValidTo",
		
		/*------ fee = Identifier for Fee ------*/
		fee."SequenceNumber"                                                                                          AS "Fee_SequenceNumber",
		fee."BusinessValidFrom"                                                                                       AS "Fee_BusinessValidFrom",
		fee."BusinessValidTo"                                                                                         AS "Fee_BusinessValidTo",
		fee."SystemValidFrom"                                                                                         AS "Fee_SystemValidFrom",
		fee."SystemValidTo"                                                                                           AS "Fee_SystemValidTo",
		fee."FeeType"                                                                                                 AS "Fee_FeeType",
		fee."RoleOfPayer"                                                                                             AS "Fee_RoleOfPayer",
		fee."FeeChargingStartDate"                                                                                    AS "Fee_FeeChargingStartDate",
		fee."FeeChargingEndDate"                                                                                      AS "Fee_FeeChargingEndDate",
		fee."FeeCalculationMethod"                                                                                    AS "Fee_FeeCalculationMethod",
		fee."FeeAmount"                                                                                               AS "Fee_FeeAmount",
		fee."FeeCurrency"                                                                                             AS "Fee_FeeCurrency",
		fee."FeePercentage"                                                                                           AS "Fee_FeePercentage",
		fee."FeeAssessmentBase"                                                                                       AS "Fee_FeeAssessmentBaseAggregation",
		fee."FeeAssessmentBaseAggregationMethod"                                                                      AS "Fee_FeeAssessmentBaseAggregationMethod",
		fee."FeeCategory"                                                                                             AS "Fee_FeeCategory",
		fee."FirstFeeBillingDate"                                                                                     AS "Fee_FirstFeeBillingDate",
		fee."FirstRegularFeeBillingDate"                                                                              AS "Fee_FirstRegularFeeBillingDate",
		fee."LastFeeBillingDate"                                                                                      AS "Fee_LastFeeBillingDate",
		fee."RecurringFeePeriodLength"                                                                                AS "Fee_RecurringFeePeriodLength",
		fee."ReccurringFeePeriodTimeUnit"                                                                             AS "Fee_ReccurringFeePeriodTimeUnit",
		fee."DayOfMonthOfFeeBillingDate"                                                                              AS "Fee_DayOfMonthOfFeeBillingDate",
		fee."BusinessDayConvention"                                                                                   AS "Fee_BusinessDayConvention",
		fee."BusinessCalendar"                                                                                        AS "Fee_BusinessCalendar",
		fee."EventTriggerType"                                                                                        AS "Fee_EventTriggerType",

		/*------ agrd = Identifier for AgreedLimit ------*/
		agrd."SequenceNumber"                                                                                         AS "AgreedLimit_SequenceNumber",
		agrd."BusinessValidFrom"                                                                                      AS "AgreedLimit_BusinessValidFrom",
		agrd."BusinessValidTo"                                                                                        AS "AgreedLimit_BusinessValidTo",
		agrd."SystemValidFrom"                                                                                        AS "AgreedLimit_SystemValidFrom",
		agrd."SystemValidTo"                                                                                          AS "AgreedLimit_SystemValidTo",
		agrd."LimitType"                                                                                              AS "AgreedLimit_LimitType",
		agrd."LimitValidFrom"                                                                                         AS "AgreedLimit_LimitValidFrom",
		agrd."LimitValidTo"                                                                                           AS "AgreedLimit_LimitValidTo",
		agrd."LimitAmount"                                                                                            AS "AgreedLimit_LimitAmount",
		agrd."LimitCurrency"                                                                                          AS "AgreedLimit_LimitCurrency"



	FROM  "com.adweko.adapter.osx.synonyms::FinancialContract_View"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) fc
    
    	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  csd ON 
                                csd."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
                            AND csd."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
							AND csd."ContractStatusCategory"								= 'DefaultStatus'

		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS csp ON
								csp."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"
							AND csp."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
							AND csp."ContractStatusCategory"								= 'PastDueStatus'

		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  bpca ON 
                                bpca."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
                            AND bpca."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
							AND bpca."Role"													= 'Borrower'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bpcac ON
								bpcac."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"
							AND bpcac."ASSOC_FinancialContract_IDSystem"					= fc."IDSystem"
							AND bpcac."Role"												= 'ClearingMember'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bpcl ON
								bpcac."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"
							AND bpcac."ASSOC_FinancialContract_IDSystem"					= fc."IDSystem"
							AND bpcac."Role"												= 'Lender'
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  ouca ON 
                                ouca."ASSOC_Contract_FinancialContractID"					= fc."FinancialContractID" 
                            AND ouca."ASSOC_Contract_IDSystem"								= fc."IDSystem"
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::TrancheInSyndication_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  tra ON 
                                tra."_SyndicationAgreement_IDSystem"						= fc."IDSystem"
                            AND tra."_SyndicationAgreement_FinancialContractID"				= fc."FinancialContractID"  
       	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Fee_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  fee ON 
                                fee."_TrancheInSyndication__SyndicationAgreement_IDSystem"	= tra."_SyndicationAgreement_IDSystem" 
                            AND fee."_TrancheInSyndication__SyndicationAgreement_FinancialContractID" = tra."_SyndicationAgreement_FinancialContractID"   
                            AND	fee."_TrancheInSyndication_TrancheSequenceNumber"			= tra."TrancheSequenceNumber"
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::AgreedLimit_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  agrd ON 
                                agrd."_TrancheInSyndication__SyndicationAgreement_IDSystem"	= tra."_SyndicationAgreement_IDSystem" 
                            AND agrd."_TrancheInSyndication__SyndicationAgreement_FinancialContractID"	= tra."_SyndicationAgreement_FinancialContractID"   
                            AND	agrd."_TrancheInSyndication_TrancheSequenceNumber"			= tra."TrancheSequenceNumber"
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ProductCatalogAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  pca ON 
                    			pca."_FinancialContract_IDSystem"							= fc."IDSystem" 
            				AND pca."_FinancialContract_FinancialContractID"				= fc."FinancialContractID"   
            				AND pca."_ProductCatalogItem__ProductCatalog_CatalogID"         = 'FSDMStandard'

    WHERE 		fc."FinancialContractCategory"		=  'SyndicationAgreement'
