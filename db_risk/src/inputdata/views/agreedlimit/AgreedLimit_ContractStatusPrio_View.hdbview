VIEW "com.adweko.adapter.osx.inputdata.agreedlimit::AgreedLimit_ContractStatusPrio_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_STREAM_NAME NVARCHAR(40)
    )
AS
	SELECT
			"FinancialContract_FinancialContractID",
			"FinancialContract_IDSystem",
			"MonetaryBalanceHistorical_AccountBalance",
			"MonetaryBalanceHistorical_DisbursedPrincipal",
			"PlannedEarlyDisbursement_PlannedDisbursementAmount"
		
	FROM (
		SELECT
			cs."ASSOC_FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			cs."ASSOC_FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			mbHist."MonetaryBalanceHistorical_AccountBalance",
			mbHist."MonetaryBalanceHistorical_DisbursedPrincipal",
			ped."PlannedDisbursementAmount" AS "PlannedEarlyDisbursement_PlannedDisbursementAmount",
			ROW_NUMBER() OVER(
					PARTITION BY "MonetaryBalanceHistorical_FinancialContract_FinancialContractID",
								"MonetaryBalanceHistorical_FinancialContract_IDSystem"
					ORDER BY map_dod."Priority" ASC,
							"MonetaryBalanceHistorical_BusinessValidFrom" DESC
			) AS "Prio"
		FROM "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cs
		
		INNER JOIN "com.adweko.adapter.osx.mappings::map_definitionOfDefaultPrio_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS map_dod
			 ON map_dod."ContractStatusCategory"					= cs."ContractStatusCategory"
			AND map_dod."Status"									= cs."Status"
			AND map_dod."DefinitionOfDefault"						= 'Default'
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMonetaryBalanceHistorical"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_STREAM_NAME) AS mbHist
			 ON mbHist."MonetaryBalanceHistorical_FinancialContract_FinancialContractID" = cs."ASSOC_FinancialContract_FinancialContractID"
			AND mbHist."MonetaryBalanceHistorical_FinancialContract_IDSystem" = cs."ASSOC_FinancialContract_IDSystem"
			AND mbHist."MonetaryBalanceHistorical_BusinessValidFrom" < cs."StatusChangeDate"
			AND mbHist."MonetaryBalanceHistorical_SystemValidFrom"	<= :I_SYSTEM_TIMESTAMP
			And mbHist."MonetaryBalanceHistorical_SystemValidTo"	> :I_SYSTEM_TIMESTAMP
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::PlannedEarlyDisbursement_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS ped
			 ON ped."_FinancialContract_FinancialContractID"		= cs."ASSOC_FinancialContract_FinancialContractID"
			AND ped."_FinancialContract_IDSystem"					= cs."ASSOC_FinancialContract_IDSystem"
			AND ped."PlannedDisbursementDate"						> cs."StatusChangeDate"
	) WHERE "Prio" = 1