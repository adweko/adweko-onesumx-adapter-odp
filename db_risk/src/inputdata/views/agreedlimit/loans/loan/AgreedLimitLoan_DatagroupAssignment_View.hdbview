view "com.adweko.adapter.osx.inputdata.agreedlimit.loans.loan::AgreedLimitLoan_DatagroupAssignment_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."IDSystem"															AS "FinancialContract_IDSystem",
    	fc."FinancialContractID"												AS "FinancialContract_FinancialContractID",
        fc."FinancialContractCategory"											AS "FinancialContract_FinancialContractCategory",
		fc."OriginalSigningDate"												AS "FinancialContract_OriginalSigningDate",
		fc."Seniority"															AS "FinancialContract_Seniority",
		fc."ASSOC_FacilityOfDrawing_FinancialContractID"						AS "FinancialContract_ASSOC_FacilityOfDrawing_FinancialContractID",
		fc."ASSOC_FacilityOfDrawing_IDSystem"									AS "FinancialContract_ASSOC_FacilityOfDrawing_IDSystem",
		dg."dataGroup"

    FROM "com.adweko.adapter.osx.inputdata.agreedlimit.loans.loan::AgreedLimitLoan_Datagroup_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) dg
    
    INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) fc
    	ON	fc."FinancialContractCategory"		= dg."FinancialContractCategory"
    	
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_AccountingClassificationStatusFlat"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS acs
		ON acs."AccountingClassificationStatus_FinancialContract_FinancialContractID" = fc."FinancialContractID"
		AND acs."AccountingClassificationStatus_FinancialContract_IDSystem"	= fc."IDSystem"
		AND dg."AccountingClassificationCategory"			IN (acs."AccountingClassificationStatus_IFRS_AccountingClassificationCategory", acs."AccountingClassificationStatus_HGB_AccountingClassificationCategory")
		AND dg."HoldingCategoryStatus"						IN (acs."AccountingClassificationStatus_IFRS_HoldingCategoryStatus", acs."AccountingClassificationStatus_HGB_HoldingCategoryStatus")

    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ProductCatalogAssignment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pc
    	ON	pc."ProductCatalogAssignment_FinancialContract_FinancialContractID" 		= fc."FinancialContractID" 
    	AND pc."ProductCatalogAssignment_FinancialContract_IDSystem"					= fc."IDSystem"
   		AND pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem" 		= dg."ProductCatalogItem"
    	AND pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID"	= dg."ProductCatalog_CatalogID"
    	
   WHERE	IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",'')			= IFNULL(dg."ProductCatalogItem",'')
		AND IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID",'')	= IFNULL(dg."ProductCatalog_CatalogID",'')
    	AND (dg."IDSystem" IS NULL OR dg."IDSystem" = fc."IDSystem")
    	AND IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",'') <> 'PassThroughLoan'
    	AND fc."_TrancheInSyndication__SyndicationAgreement_FinancialContractID" IS NULL
    	AND IFNULL(dg."_AccountingSystem_AccountingSystemID", '') IN (IFNULL(acs."AccountingClassificationStatus_IFRS_AccountingSystem_AccountingSystemID", ''), IFNULL(acs."AccountingClassificationStatus_HGB_AccountingSystem_AccountingSystemID", ''))
    	AND IFNULL(dg."AccountingClassificationCategory", '') IN (IFNULL(acs."AccountingClassificationStatus_IFRS_AccountingClassificationCategory", ''), IFNULL(acs."AccountingClassificationStatus_HGB_AccountingClassificationCategory", ''))
    	AND IFNULL(dg."HoldingCategoryStatus", '') IN (IFNULL(acs."AccountingClassificationStatus_IFRS_HoldingCategoryStatus", ''), IFNULL(acs."AccountingClassificationStatus_HGB_HoldingCategoryStatus", ''))