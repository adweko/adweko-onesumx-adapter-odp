VIEW "com.adweko.adapter.osx.inputdata.agreedlimit.creditCard::AgreedLimitCreditCard_Root_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."FinancialContract_IDSystem",
    	fc."FinancialContract_FinancialContractID",
        fc."FinancialContract_OriginalSigningDate",
		fc."FinancialContract_AcceptanceDate",
		fc."dataGroup",
		"com.adweko.adapter.osx.inputdata.common::get_contractID"(
			'F', 
			'',
			"FinancialContract_FinancialContractID" || '#' || ci."CardIssueID" || '#' || card."CardIssueSequenceID" || '#LIM',
			"FinancialContract_IDSystem",
			''
		) AS "contractID",
		"FinancialContract_FinancialContractID" || '#' || ci."CardIssueID" || '#' || card."CardIssueSequenceID" || '#LIM' AS "sourceSystemID",
		'0001' AS "node_No",
		agrd."AcceptanceDate"					AS "AgreedLimit_AcceptanceDate",
		agrd."LimitValidFrom"					AS "AgreedLimit_LimitValidFrom",
		agrd."LimitValidTo"						AS "AgreedLimit_LimitValidTo",
		agrd."IsRevolving"						AS "AgreedLimit_IsRevolving",
		agrd."IsUnconditionallyRevocable"		AS "AgreedLimit_IsUnconditionallyRevocable",
		agrd."LimitCurrency"					AS "AgreedLimit_LimitCurrency",
		agrd."LimitAmount"						AS "AgreedLimit_LimitAmount",
		ci."IsMainCard"							AS "CardIssue_IsMainCard",
		ci."ASSOC_CardHolder_BusinessPartnerID"	AS "CardIssue_ASSOC_CardHolder_BusinessPartnerID",
		CASE
			WHEN mb."AccountBalance" > 0 THEN 1
			WHEN mb."AccountBalance" <= 0 THEN -1
		END AS "isActiveSign",
		mb."AccountBalance"					AS "MonetaryBalance_AccountBalance",
		mb."BalanceCurrency"				AS "ContractCurrency",
		fl."FreeLine"						AS "FreeLine_FreeLine",
		fl."Utilization"					AS "FreeLine_Utilization",
		csd."Status"						AS "ContractStatus_DefaultStatus_Status",
		csd."StatusChangeDate"				AS "ContractStatus_DefaultStatus_StatusChangeDate",
		pc."StandardCatalog_ProductCatalogItem",
		pc."CustomCatalog_ProductCatalogItem"
		
    FROM "com.adweko.adapter.osx.inputdata.agreedlimit.creditCard::AgreedLimitCreditCard_DatagroupAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) fc
	
	INNER JOIN "com.adweko.adapter.osx.synonyms::CardIssue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS ci
		ON	ci."ASSOC_CardAgreement_FinancialContractID"		= fc."FinancialContract_FinancialContractID"
		AND	ci."ASSOC_CardAgreement_IDSystem"					= fc."FinancialContract_IDSystem"
		
	INNER JOIN "com.adweko.adapter.osx.synonyms::Card_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS card
		ON	card."ASSOC_CardIssue_CardIssueID"									= ci."CardIssueID"
		AND	card."ASSOC_CardIssue_ASSOC_CardAgreement_FinancialContractID"		= ci."ASSOC_CardAgreement_FinancialContractID"
		AND	card."ASSOC_CardIssue_ASSOC_CardAgreement_IDSystem"					= ci."ASSOC_CardAgreement_IDSystem"
	
	INNER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMonetaryBalance"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,'AgreedLimitCreditCard') mb
		ON	mb."CardIssue_CardIssueID"									= ci."CardIssueID"
		AND mb."CardIssue_ASSOC_CardAgreement_FinancialContractID"		= ci."ASSOC_CardAgreement_FinancialContractID"
		AND	mb."CardIssue_ASSOC_CardAgreement_IDSystem"					= ci."ASSOC_CardAgreement_IDSystem"
		AND mb."MonetaryBalanceCategory"								= 'CardAccountBalance'

	INNER JOIN "com.adweko.adapter.osx.synonyms::AgreedLimit_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS agrd
		ON	agrd."ASSOC_CardIssue_CardIssueID"									= ci."CardIssueID"
		AND	agrd."ASSOC_CardIssue_ASSOC_CardAgreement_FinancialContractID"		= ci."ASSOC_CardAgreement_FinancialContractID"
		AND	agrd."ASSOC_CardIssue_ASSOC_CardAgreement_IDSystem"					= ci."ASSOC_CardAgreement_IDSystem"
		AND agrd."LimitType"								= 'CardLimit'
		AND agrd."LimitCurrency"							= mb."BalanceCurrency"
		AND agrd."LimitAmount"								> 0
		AND agrd."LimitValidFrom"							<= :I_BUSINESS_DATE
		AND agrd."LimitValidTo" 							> :I_BUSINESS_DATE

 	 LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS csd
         ON csd."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContract_FinancialContractID"
        AND csd."ASSOC_FinancialContract_IDSystem"				= fc."FinancialContract_IDSystem"
        AND csd."ContractStatusCategory"						= 'DefaultStatus'
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::FreeLine_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS fl
		ON fl."ASSOC_AgreedLimit_ASSOC_FinancialContract_FinancialContractID"	= agrd."ASSOC_CardIssue_ASSOC_CardAgreement_FinancialContractID"
		AND fl."ASSOC_AgreedLimit_ASSOC_FinancialContract_IDSystem"				= agrd."ASSOC_CardIssue_ASSOC_CardAgreement_IDSystem"
		AND fl."ASSOC_AgreedLimit_SequenceNumber"								= agrd."SequenceNumber"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) pc
		ON pc."IDSystem"				= fc."FinancialContract_IDSystem"
		AND pc."FinancialContractID"	= fc."FinancialContract_FinancialContractID"