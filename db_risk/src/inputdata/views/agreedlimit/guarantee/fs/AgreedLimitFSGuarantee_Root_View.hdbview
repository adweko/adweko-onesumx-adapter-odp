VIEW "com.adweko.adapter.osx.inputdata.agreedlimit.guarantee.fs::AgreedLimitFSGuarantee_Root_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."FinancialContract_IDSystem",
    	fc."FinancialContract_FinancialContractID",
        fc."FinancialContract_OriginalSigningDate",
		fc."dataGroup",
		"com.adweko.adapter.osx.inputdata.common::get_contractID"(
			'F', 
			'',
			fc."FinancialContract_FinancialContractID" || '#LIM',
			fc."FinancialContract_IDSystem",
			''
		) AS "contractID",
		fc."FinancialContract_FinancialContractID" || '#LIM' AS "sourceSystemID",
		'0001' AS "node_No",
		fc."FinancialContract_FacilityOfDrawing_IDSystem",
		fc."FinancialContract_FacilityOfDrawing_FinancialContractID",
		agrd."AcceptanceDate"					AS "AgreedLimit_AcceptanceDate",
		agrd."LimitValidFrom"					AS "AgreedLimit_LimitValidFrom",
		agrd."LimitValidTo"						AS "AgreedLimit_LimitValidTo",
		agrd."IsRevolving"						AS "AgreedLimit_IsRevolving",
		agrd."IsUnconditionallyRevocable"		AS "AgreedLimit_IsUnconditionallyRevocable",
		agrd."LimitCurrency"					AS "AgreedLimit_LimitCurrency",
		agrd."LimitAmount"						AS "AgreedLimit_LimitAmount",
		fl."FreeLine"							AS "FreeLine_FreeLine",
		fl."Utilization"						AS "FreeLine_Utilization",
    	MAP(bpcda."Role",
    		'Lender', 1,
    		'Borrower', -1
    	) AS "isActiveSign",
		mb."BalanceCurrency"				AS "ContractCurrency",
		mb."DisbursedPrincipal"				AS "MonetaryBalance_DisbursedPrincipal",
		csd."Status"						AS "ContractStatus_DefaultStatus_Status",
		csd."StatusChangeDate"				AS "ContractStatus_DefaultStatus_StatusChangeDate",
		pc."StandardCatalog_ProductCatalogItem",
		pc."CustomCatalog_ProductCatalogItem"
		
    FROM "com.adweko.adapter.osx.inputdata.agreedlimit.guarantee.fs::AgreedLimitFSGuarantee_DatagroupAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) fc
    
    INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) synd
    	ON	synd."FinancialContractID"				= fc."FinancialContract_TrancheInSyndication_SyndicationAgreement_FinancialContractID"
    	AND synd."IDSystem"							= fc."FinancialContract_TrancheInSyndication_SyndicationAgreement_IDSystem"
    	AND synd."FinancialContractCategory"		= 'FinancingScheme'
    	
	INNER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMonetaryBalance"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,'AgreedLimitFSGuarantee') mb
		ON	mb."FinancialContractID"				= fc."FinancialContract_FinancialContractID"
		AND mb."IDSystem"							= fc."FinancialContract_IDSystem"
		AND mb."MonetaryBalanceCategory"			= 'FacilityBalance'

	INNER JOIN "com.adweko.adapter.osx.synonyms::AgreedLimit_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS agrd
		ON	agrd."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContract_FinancialContractID"
		AND agrd."ASSOC_FinancialContract_IDSystem"				= fc."FinancialContract_IDSystem"
		AND agrd."LimitType"									= 'FacilityLine'
		AND agrd."LimitCurrency"								= mb."BalanceCurrency"
		AND agrd."LimitAmount"									> 0
		AND agrd."LimitValidFrom"								<= :I_BUSINESS_DATE
		AND agrd."LimitValidTo" 								> :I_BUSINESS_DATE
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcda
    	 ON bpcda."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContract_FinancialContractID"
    	AND bpcda."ASSOC_FinancialContract_IDSystem" 			= fc."FinancialContract_IDSystem"
    	AND bpcda."ContractDataOwner"							= true
    	AND bpcda."Role"										IN ('Lender', 'Borrower')

 	 LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS csd
         ON csd."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContract_FinancialContractID"
        AND csd."ASSOC_FinancialContract_IDSystem"				= fc."FinancialContract_IDSystem"
        AND csd."ContractStatusCategory"						= 'DefaultStatus'

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) pc
		ON pc."IDSystem"				= fc."FinancialContract_IDSystem"
		AND pc."FinancialContractID"	= fc."FinancialContract_FinancialContractID"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::FreeLine_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS fl
		ON	fl."ASSOC_AgreedLimit_SequenceNumber"								= agrd."SequenceNumber"
		AND fl."ASSOC_AgreedLimit_ASSOC_FinancialContract_FinancialContractID"	= agrd."ASSOC_FinancialContract_FinancialContractID"
		AND fl."ASSOC_AgreedLimit_ASSOC_FinancialContract_IDSystem"				= agrd."ASSOC_FinancialContract_IDSystem"