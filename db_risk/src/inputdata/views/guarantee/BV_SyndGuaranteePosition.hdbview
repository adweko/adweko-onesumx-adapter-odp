VIEW "com.adweko.adapter.osx.inputdata.guarantee::BV_SyndGuaranteePosition"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
AS
       	SELECT
      		/*-----fc = Identifier for FinancialContract ------*/
       		fc."IDSystem"       												AS "FinancialContract_IDSystem",
			fc."FinancialContractID"        									AS "FinancialContract_FinancialContractID",
			fc."BusinessValidFrom"      										AS "FinancialContract_BusinessValidFrom",
			fc."BusinessValidTo"        										AS "FinancialContract_BusinessValidTo",
			fc."SystemValidFrom"        										AS "FinancialContract_SystemValidFrom",
			fc."SystemValidTo"      											AS "FinancialContract_SystemValidTo",
			fc."_TrancheInSyndication_TrancheSequenceNumber"					AS "FinancialContract_TrancheInSyndication_TrancheSequenceNumber",
    		fc."_TrancheInSyndication__SyndicationAgreement_FinancialContractID"AS "FinancialContract_TrancheInSyndication_SyndicationAgreement_FinancialContractID",
    		fc."_TrancheInSyndication__SyndicationAgreement_IDSystem"			AS "FinancialContract_TrancheInSyndication_SyndicationAgreement_IDSystem",
	
			/*-----mb = Identifier for MonetaryBalance_View ------*/
			mb."PostingOrValueDateCutoff"								        AS "MonetaryBalance_PostingOrValueDateCutoff",
			mb."RoleOfCurrency" 										        AS "MonetaryBalance_RoleOfCurrency",
			mb."BalanceCurrency"											    AS "MonetaryBalance_BalanceCurrency",
			mb."BusinessValidFrom"										        AS "MonetaryBalance_BusinessValidFrom",
			mb."BusinessValidTo"										        AS "MonetaryBalance_BusinessValidTo",
			mb."SystemValidFrom"										        AS "MonetaryBalance_SystemValidFrom",
			mb."SystemValidTo"											        AS "MonetaryBalance_SystemValidTo",
			mb."OutstandingPrincipal"									        AS "MonetaryBalance_OutstandingPrincipal",
			mb."OutstandingPrincipalCurrency"							        AS "MonetaryBalance_OutstandingPrincipalCurrency",
			mb."PrincipalPastDue"										        AS "MonetaryBalance_PrincipalPastDue",
			mb."PrincipalPastDueCurrency"								        AS "MonetaryBalance_PrincipalPastDueCurrency",
			mb."InterestPastDue"	        									AS "MonetaryBalance_InterestPastDue",
			mb."InterestPastDueCurrency"        								AS "MonetaryBalance_InterestPastDueCurrency",
			mb."FeesPastDue"        											AS "MonetaryBalance_FeesPastDue",
			mb."FeesPastDueCurrency"        									AS "MonetaryBalance_FeesPastDueCurrency",
			mb."TotalPastDue"       											AS "MonetaryBalance_TotalPastDue",
			mb."TotalPastDueCurrency"       									AS "MonetaryBalance_TotalPastDueCurrency",
			mb."DisbursedPrincipal"     										AS "MonetaryBalance_DisbursedPrincipal",
			mb."DisbursedPrincipalCurrency"     								AS "MonetaryBalance_DisbursedPrincipalCurrency",
			mb."CapitalizedInterestCurrency"									AS "MonetaryBalance_CapitalizedInterestCurrency",
			mb."CapitalizedInterest"											AS "MonetaryBalance_CapitalizedInterest",
			mb."ContractCurrency",
	
    		/*-----acc = Identifier for Accrual_View ------*/    
        	acc."AccrualType"											        AS "Accrual_AccrualType",
			acc."InterestType"											        AS "Accrual_InterestType",
			acc."BusinessValidFrom"										        AS "Accrual_BusinessValidFrom",
			acc."BusinessValidTo"										        AS "Accrual_BusinessValidTo",
			acc."SystemValidFrom"										        AS "Accrual_SystemValidFrom",
			acc."SystemValidTo"											        AS "Accrual_SystemValidTo",
			acc."AmountInPositionCurrency"								        AS "Accrual_AmountInPositionCurrency",
			acc."PositionCurrency"										        AS "Accrual_PositionCurrency",
			acc."AmountInPaymentCurrency"								        AS "Accrual_AmountInPaymentCurrency",
			acc."PaymentCurrency"										        AS "Accrual_PaymentCurrency"

    FROM "com.adweko.adapter.osx.synonyms::FinancialContract_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) fc
    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMonetaryBalance"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP, 'SyndGuarantee'  )  mb ON 
                mb."FinancialContractID"							= fc."FinancialContractID" 
            AND mb."IDSystem"										= fc."IDSystem"
            AND mb."MonetaryBalanceCategory"						= 'FacilityBalance'
            		
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Accrual_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  acc ON 
                acc."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND acc."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND acc."PositionCurrency"										= mb."ContractCurrency"

    	WHERE 		fc."FinancialContractCategory"		=  'Facility'
    		AND fc."FinancialContractType" IN ('AvalFacility', 'LetterOfCreditFacility')

