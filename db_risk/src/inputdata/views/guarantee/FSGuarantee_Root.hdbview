VIEW "com.adweko.adapter.osx.inputdata.guarantee::FSGuarantee_Root"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
AS
       	SELECT
      	/*-----fc = Identifier for FinancialContract ------*/
       	fc."IDSystem"															AS "FinancialContract_IDSystem",
		fc."FinancialContractID"												AS "FinancialContract_FinancialContractID",
		fc."BusinessValidFrom"								    				AS "FinancialContract_BusinessValidFrom",
		fc."BusinessValidTo"								    				AS "FinancialContract_BusinessValidTo",
		fc."SystemValidFrom"											        AS "FinancialContract_SystemValidFrom",
		fc."SystemValidTo"												        AS "FinancialContract_SystemValidTo",
		fc."FinancialContractCategory"										    AS "FinancialContract_FinancialContractCategory",
		fc."OriginalSigningDate"										        AS "FinancialContract_OriginalSigningDate",
		fc."PurposeType"												        AS "FinancialContract_PurposeType",
		fc."OfferValidityStartDate"										        AS "FinancialContract_OfferValidityStartDate",
		fc."OfferValidityEndDate"										        AS "FinancialContract_OfferValidityEndDate",
		fc."GracePeriod"									   					AS "FinancialContract_GracePeriod",
		fc."Purpose"                                                            AS "FinancialContract_Purpose",
		fc."ApprovedNominalAmount"									  			AS "FinancialContract_ApprovedNominalAmount",
		fc."NominalAmountCurrency"										        AS "FinancialContract_NominalAmountCurrency",
		fc."TotalRepayableAmount"										        AS "FinancialContract_TotalRepayableAmount",
		fc."TotalRepayableAmountCurrency"										AS "FinancialContract_TotalRepayableAmountCurrency",
		fc."Discount"													        AS "FinancialContract_Discount",
		fc."DiscountAmount"												        AS "FinancialContract_DiscountAmount",
		fc."DiscountAmountCurrency"										        AS "FinancialContract_DiscountAmountCurrency",
		fc."DiscountPeriodLength"										        AS "FinancialContract_DiscountPeriodLength",
		fc."DiscountPeriodTimeUnit"										        AS "FinancialContract_DiscountPeriodTimeUnit",
		fc."ExpectedMaturityDate"										        AS "FinancialContract_ExpectedMaturityDate",
		fc."Seniority"													        AS "FinancialContract_Seniority",
		fc."CurePeriodLength"											        AS "FinancialContract_CurePeriodLength",
		fc."CurePeriodTimeUnit"												    AS "FinancialContract_CurePeriodTimeUnit",
		fc."LoanInsuranceProtectionLevel"									    AS "FinancialContract_LoanInsuranceProtectionLevel",
		fc."InsuredLoanAmount"											        AS "FinancialContract_InsuredLoanAmount",
		fc."CommunicatedEffectiveInterestRate"									AS "FinancialContract_CommunicatedEffectiveInterestRate",
		fc."CommunicatedEffectiveInterestRateMethod"					        AS "FinancialContract_CommunicatedEffectiveInterestRateMethod",
		fc."Recourse"														    AS "FinancialContract_Recourse",
		fc."AmortizationType"											        AS "FinancialContract_AmortizationType",
		fc."SpecializedLendingType"										        AS "FinancialContract_SpecializedLendingType",
		fc."GoverningLawCountry"                                        		AS "FinancialContract_GoverningLawCountry",
		fc."ASSOC_FacilityOfDrawing_IDSystem"									AS "FinancialContract_FacilityOfDrawing_IDSystem",
        fc."ASSOC_FacilityOfDrawing_FinancialContractID"						AS "FinancialContract_FacilityOfDrawing_FinancialContractID",
		fc."LifecycleStatus"												    AS "FinancialContract_LifecycleStatus",
		fc."FixedPeriodEndDate"													AS "FinancialContract_FixedPeriodEndDate",
    	fc."FixedPeriodStartDate"												AS "FinancialContract_FixedPeriodStartDate",
    	fc."IntendedAssetLiability"                                     		AS "FinancialContract_IntendedAssetLiability",
    	fc."_TrancheInSyndication__SyndicationAgreement_IDSystem"               AS "FinancialContract_TrancheInSyndication_SyndicationAgreement_IDSystem",
		fc."_TrancheInSyndication__SyndicationAgreement_FinancialContractID"    AS "FinancialContract_TrancheInSyndication_SyndicationAgreement_FinancialContractID",
		fc."_TrancheInSyndication_TrancheSequenceNumber"                        AS "FinancialContract_TrancheInSyndication_TrancheSequenceNumber",		
		fc."PromotionalLoanProgramID"											AS "FinancialContract_PromotionalLoanProgramID",
		fc."FinancialContractType"												AS "FinancialContract_FinancialContractType",
		fc."Currency"															AS "FinancialContract_Currency",
		
		/*-----SyndAgre = Identifier for SyndicationAgreement ------*/
		SyndAgre."IDSystem"														AS "SyndicationAgreement_IDSystem",
		SyndAgre."FinancialContractID"											AS "SyndicationAgreement_FinancialContractID",
		SyndAgre."TotalFinancingAmount"											AS "SyndicationAgreement_TotalFinancingAmount",

      	/*-----tra = Identifier for TrancheInSyndication ------*/	
		tra."TrancheAmount"														AS "TrancheInSyndication_TrancheAmount",
      	
		/*-----csd = Identifier for ContractStatus_View ------*/
		csd."ContractStatusType"										        AS "ContractStatus_DefaultStatus_ContractStatusType",
		csd."BusinessValidFrom"											        AS "ContractStatus_DefaultStatus_BusinessValidFrom",
		csd."BusinessValidTo"											        AS "ContractStatus_DefaultStatus_BusinessValidTo",
		csd."SystemValidFrom"											        AS "ContractStatus_DefaultStatus_SystemValidFrom",
		csd."SystemValidTo"												        AS "ContractStatus_DefaultStatus_SystemValidTo",
		csd."Status"														    AS "ContractStatus_DefaultStatus_Status",
		
		/*-----csp = Identifier for ContractStatus_View ------*/
		csp."ContractStatusType"                                        		AS "ContractStatus_PastDueStatus_ContractStatusType",
		csp."BusinessValidFrom"                                         		AS "ContractStatus_PastDueStatus_BusinessValidFrom",
		csp."BusinessValidTo"                                           		AS "ContractStatus_PastDueStatus_BusinessValidTo",
		csp."SystemValidFrom"                                           		AS "ContractStatus_PastDueStatus_SystemValidFrom",
		csp."SystemValidTo"                                             		AS "ContractStatus_PastDueStatus_SystemValidTo",
		csp."Status"                                                    		AS "ContractStatus_PastDueStatus_Status",
		csp."ChangeTimestampInSourceSystem"                             		AS "ContractStatus_PastDueStatus_ChangeTimestampInSourceSystem",
		
		/*-----pca = Identifier for ProductCatalogAssignment_View ------*/
		'FSDMStandard' 										  					AS "ProductCatalogAssignment_ProductCatalogItem_ProductCatalog_CatalogID",
		pca."StandardCatalog_ProductCatalogItem"			     				AS "ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",
		pca."BusinessValidFrom"											        AS "ProductCatalogAssignment_BusinessValidFrom",
		pca."BusinessValidTo"											        AS "ProductCatalogAssignment_BusinessValidTo",
		pca."StandardCatalog_ProductCatalogItem",
		pca."CustomCatalog_ProductCatalogItem",

		/*-----bpca = Identifier for BusinessPartnerContractAssignment_View ------*/
		bpca."ASSOC_PartnerInParticipation_BusinessPartnerID"     				AS "BusinessPartnerContractAssignment_Borrower_PartnerInParticipation_BusinessPartnerID",
		bpca."ContractDataOwner"												AS "BusinessPartnerContractAssignment_Borrower_ContractDataOwner",
		bpca."BusinessValidFrom"										        AS "BusinessPartnerContractAssignment_Borrower_BusinessValidFrom",
		bpca."BusinessValidTo"											        AS "BusinessPartnerContractAssignment_Borrower_BusinessValidTo",
		bpca."SystemValidFrom"											        AS "BusinessPartnerContractAssignment_Borrower_SystemValidFrom",
		bpca."SystemValidTo"											        AS "BusinessPartnerContractAssignment_Borrower_SystemValidTo",
		
		/*-----bpcal = Identifier for BusinessPartnerContractAssignment_View ------*/
		bpcl."ASSOC_PartnerInParticipation_BusinessPartnerID"     				AS "BusinessPartnerContractAssignment_Lender_PartnerInParticipation_BusinessPartnerID",
		bpcl."ContractDataOwner"												AS "BusinessPartnerContractAssignment_Lender_ContractDataOwner",
		bpcl."BusinessValidFrom"										        AS "BusinessPartnerContractAssignment_Lender_BusinessValidFrom",
		bpcl."BusinessValidTo"											        AS "BusinessPartnerContractAssignment_Lender_BusinessValidTo",
		bpcl."SystemValidFrom"											        AS "BusinessPartnerContractAssignment_Lender_SystemValidFrom",
		bpcl."SystemValidTo"											        AS "BusinessPartnerContractAssignment_Lender_SystemValidTo",
		
		/*-----bpcr = Identifier for BusinessPartnerContractAssignment_View ------*/
		bpcr."ASSOC_PartnerInParticipation_BusinessPartnerID"     				AS "BusinessPartnerContractAssignment_RatingSubstitute_PartnerInParticipation_BusinessPartnerID",
		bpcr."ContractDataOwner"												AS "BusinessPartnerContractAssignment_RatingSubstitute_ContractDataOwner",
		bpcr."BusinessValidFrom"										        AS "BusinessPartnerContractAssignment_RatingSubstitute_BusinessValidFrom",
		bpcr."BusinessValidTo"											        AS "BusinessPartnerContractAssignment_RatingSubstitute_BusinessValidTo",
		bpcr."SystemValidFrom"											        AS "BusinessPartnerContractAssignment_RatingSubstitute_SystemValidFrom",
		bpcr."SystemValidTo"											        AS "BusinessPartnerContractAssignment_RatingSubstitute_SystemValidTo",

		bpcac."ASSOC_PartnerInParticipation_BusinessPartnerID"              	AS "BusinessPartnerContractAssignment_ClearingMember_PartnerInParticipation_BusinessPartnerID",
		bpcac."BusinessValidFrom"                                       		AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidFrom",
		bpcac."BusinessValidTo"                                         		AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidTo",
		bpcac."SystemValidFrom"                                         		AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidFrom",
		bpcac."SystemValidTo"                                           		AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidTo",
		
		/*------ bpcao = Identifier for BusinessPartnerContractAssignment ------*/
		bpcao."ASSOC_PartnerInParticipation_BusinessPartnerID"     							AS "BusinessPartnerContractAssignment_ContractDataOwner_PartnerInParticipation_BusinessPartnerID",
		bpcao."BusinessValidFrom"                            								AS "BusinessPartnerContractAssignment_ContractDataOwner_BusinessValidFrom",
		bpcao."BusinessValidTo"                              								AS "BusinessPartnerContractAssignment_ContractDataOwner_BusinessValidTo",
		bpcao."SystemValidFrom"                              								AS "BusinessPartnerContractAssignment_ContractDataOwner_SystemValidFrom",
		bpcao."SystemValidTo"                                								AS "BusinessPartnerContractAssignment_ContractDataOwner_SystemValidTo",	
					
		ouca."RoleOfOrganizationalUnit"														AS "OrganizationalUnitContractAssignment_RoleOfOrganizationalUnit",
		ouca."ASSOC_OrgUnit_OrganizationalUnitID"											AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationalUnitID",
		ouca."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID"	AS "OrganizationalUnitContractAssignment_OrgUnit_OrganizationHostingOrganizationalUnit_BusinessPartnerID",
		ouca."BusinessValidFrom"															AS "OrganizationalUnitContractAssignment_BusinessValidFrom",
		ouca."BusinessValidTo"																AS "OrganizationalUnitContractAssignment_BusinessValidTo",
		ouca."SystemValidFrom"																AS "OrganizationalUnitContractAssignment_SystemValidFrom",
		ouca."SystemValidTo"																AS "OrganizationalUnitContractAssignment_SystemValidTo",

		/*-----earl = Identifier for EarlyRepaymentRule_View ------*/
		earl."ASSOC_Loan_FinancialContractID"									AS "EarlyRepaymentRule_Loan_FinancialContractID",
		earl."SequenceNumber"											        AS "EarlyRepaymentRule_SequenceNumber",
		earl."BusinessValidFrom"										        AS "EarlyRepaymentRule_BusinessValidFrom",
		earl."BusinessValidTo"											        AS "EarlyRepaymentRule_BusinessValidTo",
		earl."SystemValidFrom"											        AS "EarlyRepaymentRule_SystemValidFrom",
		earl."SystemValidTo"											        AS "EarlyRepaymentRule_SystemValidTo",
		earl."EarlyRepaymentStartDate"								        	AS "EarlyRepaymentRule_EarlyRepaymentStartDate",
		earl."EarlyRepaymentEndDate"											AS "EarlyRepaymentRule_EarlyRepaymentEndDate",
		earl."NoticePeriodLength"										        AS "EarlyRepaymentRule_NoticePeriodLength",
		earl."NoticePeriodTimeUnit"									        	AS "EarlyRepaymentRule_NoticePeriodTimeUnit",
		earl."PrepaymentAmountCurrency"								        	AS "EarlyRepaymentRule_PrepaymentAmountCurrency",
		earl."MinimumPrepaymentRate"											AS "EarlyRepaymentRule_MinimumPrepaymentRate",
		earl."MaximumPrepaymentRate"											AS "EarlyRepaymentRule_MaximumPrepaymentRate",

		/*-----ref = Identifier for RefinancingRelation_View ------*/
		ref."RefinancingType"											        AS "RefinancingRelation_RefinancingType",
		ref."BusinessValidFrom"											        AS "RefinancingRelation_BusinessValidFrom",
		ref."BusinessValidTo"											        AS "RefinancingRelation_BusinessValidTo",
		ref."SystemValidFrom"											        AS "RefinancingRelation_SystemValidFrom",
		ref."SystemValidTo"												        AS "RefinancingRelation_SystemValidTo",
		ref."RefinancedAmountInPositionCurrency"						        AS "RefinancingRelation_RefinancedAmountInPositionCurrency",
		ref."PositionCurrency"											        AS "RefinancingRelation_PositionCurrency",


		/*-----ica = Identifier for IndustryClassAssignment_View ------*/
		ica."ASSOC_IndustryClass_Industry"										AS "IndustryClassAssignment_Industry",
		ica."ASSOC_IndustryClass_IndustryClassificationSystem"					AS "IndustryClassAssignment_IndustryClassificationSystem",
		
		/*----Indicator wheter loan is active or passive--*/
		CASE
		    WHEN bpca."ContractDataOwner" = true  --Borrower role
                THEN TO_NVARCHAR('FALSE')
		    WHEN bpcl."ContractDataOwner" = true  --Lender role
		        THEN TO_NVARCHAR('TRUE')
		    ELSE
		        TO_NVARCHAR('')
        END AS "IsActive"
        
    FROM "com.adweko.adapter.osx.synonyms::FinancialContract_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS fc
    INNER JOIN "com.adweko.adapter.osx.synonyms::FinancialContract_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS SyndAgre
    	ON SyndAgre."FinancialContractID" = fc."_TrancheInSyndication__SyndicationAgreement_FinancialContractID"
    	AND SyndAgre."IDSystem" 		= fc."_TrancheInSyndication__SyndicationAgreement_IDSystem"
    	
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::PaymentSchedule_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS paym ON
    			paym."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
    		AND paym."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"
    	
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::TrancheInSyndication_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  tra ON 
                tra."_SyndicationAgreement_IDSystem"						= fc."_TrancheInSyndication__SyndicationAgreement_IDSystem"
            AND tra."_SyndicationAgreement_FinancialContractID"				= fc."_TrancheInSyndication__SyndicationAgreement_FinancialContractID"
            AND tra."TrancheSequenceNumber"									= fc."_TrancheInSyndication_TrancheSequenceNumber"
    	
    	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS csd ON 
                csd."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND csd."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND csd."ContractStatusCategory"								= 'DefaultStatus'
 
    	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ContractStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS csp ON
    			csp."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND csp."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND csp."ContractStatusCategory"								= 'PastDueStatus'	
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS pca ON 
            	pca."IDSystem"							= fc."IDSystem" 
            AND pca."FinancialContractID"				= fc."FinancialContractID"     
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpca ON 
                bpca."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND bpca."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND bpca."Role"											        = 'Borrower'  
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcl ON 
                bpcl."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND bpcl."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND bpcl."Role"											        = 'Lender' 
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcr ON 
                bpcr."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND bpcr."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
			AND bpcr."Role"											        = 'RatingSubstitute' 
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bpcac ON
				bpcac."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
            AND bpcac."ASSOC_FinancialContract_IDSystem"					= fc."IDSystem"
			AND bpcac."Role"											    = 'ClearingMember'
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bpcao ON
				bpcao."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID"
			AND bpcao."ASSOC_FinancialContract_IDSystem"					= fc."IDSystem"
			AND bpcao."ContractDataOwner"									= true	
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS ouca ON 
                ouca."ASSOC_Contract_FinancialContractID"					= fc."FinancialContractID" 
            AND ouca."ASSOC_Contract_IDSystem"								= fc."IDSystem"
            AND ouca."RoleOfOrganizationalUnit"								= 'ManagingUnit'
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::EarlyRepaymentRule_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS earl ON 
                earl."ASSOC_Loan_IDSystem"									= fc."IDSystem" 
            AND earl."ASSOC_Loan_FinancialContractID"						= fc."FinancialContractID"          
       	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::RefinancingRelation_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS ref ON 
                ref."ASSOC_RefinancedContract_IDSystem"						= fc."IDSystem" 
            AND ref."ASSOC_RefinancedContract_FinancialContractID"			= fc."FinancialContractID" 		
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::IndustryClassAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS ica ON
	    		ica."ASSOC_BusinessPartner_BusinessPartnerID"					= bpca."ASSOC_PartnerInParticipation_BusinessPartnerID"
	    			AND ica."ASSOC_IndustryClass_IndustryClassificationSystem"	= 'NACE'
							
    WHERE 		fc."FinancialContractCategory"		=  'Facility'
    AND fc."FinancialContractType" IN ('AvalFacility', 'LetterOfCreditFacility')
    AND fc."_TrancheInSyndication__SyndicationAgreement_FinancialContractID"  IS NOT NULL
    AND SyndAgre."FinancialContractCategory" = 'FinancingScheme'
    AND (	
    		paym."ASSOC_FinancialContract_IDSystem" IS NULL
    	AND paym."ASSOC_FinancialContract_FinancialContractID" IS NULL
    )
    