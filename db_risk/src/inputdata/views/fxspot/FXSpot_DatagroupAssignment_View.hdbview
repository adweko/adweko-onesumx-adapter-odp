view "com.adweko.adapter.osx.inputdata.fxspot::FXSpot_DatagroupAssignment_View"
    (IN I_BUSINESS_DATE DATE
    , IN I_SYSTEM_TIMESTAMP TIMESTAMP
    , IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
	SELECT DISTINCT
	    fc."IDSystem",
	    fc."FinancialContractID",
	    fc."BusinessValidFrom",
	    fc."BusinessValidTo",
	    fc."SystemValidFrom",
	    fc."SystemValidTo",
	    fc."FinancialContractCategory",
	    fc."OTCDerivativeContractCategory",
	    fc."QuoteCurrency",
	    fc."TradedCurrency",
	    fc."Seniority",
	    fc."SettlementDate",
	    fc."OriginalSigningDate",
	    fc."PricePerTradedCurrencyUnit",
	    fc."AmountInTradedCurrency",
	    fc."_StructuredProduct_FinancialContractID",
	    dg."ProductCatalogItem",
	    dg."ProductCatalog_CatalogID",
	    dg."dataGroup"
	FROM "com.adweko.adapter.osx.inputdata.fxspot::FXSpot_Datagroup_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS dg
	
	INNER JOIN "com.adweko.adapter.osx.synonyms::FinancialContract_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fc
		 ON dg."FinancialContractCategory"						= fc."FinancialContractCategory"
		AND dg."OTCDerivativeContractCategory"					= fc."OTCDerivativeContractCategory"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::ProductCatalogAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pc
		 ON fc."FinancialContractID"						= pc."_FinancialContract_FinancialContractID"
		AND fc."IDSystem"									= pc."_FinancialContract_IDSystem"
		
	WHERE 
		(
			(		dg."IDSystem"							= fc."IDSystem"
				AND dg."ProductCatalog_CatalogID"			= pc."_ProductCatalogItem__ProductCatalog_CatalogID"
				AND dg."ProductCatalogItem"					= pc."_ProductCatalogItem_ProductCatalogItem"
			)
			OR
			(		dg."IDSystem" IS NULL
				AND dg."ProductCatalog_CatalogID"			= pc."_ProductCatalogItem__ProductCatalog_CatalogID"
				AND dg."ProductCatalogItem"					= pc."_ProductCatalogItem_ProductCatalogItem"
			)
			OR
			(		dg."IDSystem" IS NULL
				AND dg."ProductCatalog_CatalogID" IS NULL
				AND dg."ProductCatalogItem"	IS NULL
			)
			OR
			(		dg."IDSystem"							= fc."IDSystem"
				AND dg."ProductCatalog_CatalogID" IS NULL
				AND dg."ProductCatalogItem" IS NULL
			)
		)