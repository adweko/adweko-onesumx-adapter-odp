VIEW "com.adweko.adapter.osx.inputdata.fxspot::BV_EndOfDayExRateObsPrio"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
AS
        SELECT DISTINCT
            "BaseCurrency",
			"ExchangeRateType",
			"PriceDataProvider",
			"QuotationType",
			"QuoteCurrency",
			"BusinessValidFrom",
			"BusinessValidTo",
			"SystemValidFrom",
			"SystemValidTo",
			"Close",
			"High",
			"Low",
			"Open",
			"SourceSystemID",
			"ChangeTimestampInSourceSystem",
			"ChangingUserInSourceSystem",
			"ChangingProcessType",
			"ChangingProcessID"	
        FROM (
        	SELECT
        		"BaseCurrency",
				"ExchangeRateType",
				"PriceDataProvider",
				"QuotationType",
				"QuoteCurrency",
				"BusinessValidFrom",
				"BusinessValidTo",
				"SystemValidFrom",
				"SystemValidTo",
				"Close",
				"High",
				"Low",
				"Open",
				"SourceSystemID",
				"ChangeTimestampInSourceSystem",
				"ChangingUserInSourceSystem",
				"ChangingProcessType",
				"ChangingProcessID",
        		ROW_NUMBER() OVER(
        			PARTITION BY
        				"BaseCurrency",
        				"QuoteCurrency"
        			ORDER BY
        				MAP("PriceDataProvider",
        					'Bloomberg', 1,
        					'EuropeanCentralBank', 2,
        					'Reuteres', 3
        				)
        		) AS "Prio"
        	FROM (
        		SELECT
        			eod."BaseCurrency",
					eod."ExchangeRateType",
					eod."PriceDataProvider",
					eod."QuotationType",
					eod."QuoteCurrency",
					eod."BusinessValidFrom",
					eod."BusinessValidTo",
					eod."SystemValidFrom",
					eod."SystemValidTo",
					eod."Close",
					eod."High",
					eod."Low",
					eod."Open",
					eod."SourceSystemID",
					eod."ChangeTimestampInSourceSystem",
					eod."ChangingUserInSourceSystem",
					eod."ChangingProcessType",
					eod."ChangingProcessID",
        			ROW_NUMBER() OVER(
        				PARTITION BY
        					eod."BaseCurrency",
        					eod."QuoteCurrency"
        				ORDER BY
       						MAP(eod."ExchangeRateType",
       							'BuyingRate', 1,
       							'AverageRate', 2,
       							'HistoricalExchangeRate', 3,
       							'KeyDateExchangeRate', 4,
       							'SellingRate', 5
       						)
        			) AS "Prio"
        		FROM "com.adweko.adapter.osx.synonyms::EndOfDayExchangeRateObservation_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) eod
        		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::FinancialContract_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fc
        			 ON eod."BaseCurrency"									= fc."TradedCurrency"
        			AND eod."QuoteCurrency"									= fc."QuoteCurrency"
        		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bpca
        			 ON bpca."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContractID"
        			AND bpca."ASSOC_FinancialContract_IDSystem"				= fc."IDSystem"
        			
        		WHERE bpca."ContractDataOwner" = 'true'
        			AND bpca."Role" = 'Buyer'
        			AND eod."QuotationType" = 'Direct'

        		UNION ALL
        		
        		SELECT
        			eod."BaseCurrency",
					eod."ExchangeRateType",
					eod."PriceDataProvider",
					eod."QuotationType",
					eod."QuoteCurrency",
					eod."BusinessValidFrom",
					eod."BusinessValidTo",
					eod."SystemValidFrom",
					eod."SystemValidTo",
					eod."Close",
					eod."High",
					eod."Low",
					eod."Open",
					eod."SourceSystemID",
					eod."ChangeTimestampInSourceSystem",
					eod."ChangingUserInSourceSystem",
					eod."ChangingProcessType",
					eod."ChangingProcessID",
        			ROW_NUMBER() OVER(
        				PARTITION BY
        					eod."BaseCurrency",
        					eod."QuoteCurrency"
        				ORDER BY
       						MAP(eod."ExchangeRateType",
       							'SellingRate', 1,
       							'AverageRate', 2,
       							'HistoricalExchangeRate', 3,
       							'KeyDateExchangeRate', 4,
       							'BuyingRate', 5
       						)
        			) AS "Prio"
        		FROM "com.adweko.adapter.osx.synonyms::EndOfDayExchangeRateObservation_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS eod
        		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::FinancialContract_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fc
        			 ON eod."BaseCurrency"							= fc."TradedCurrency"
        			AND eod."QuoteCurrency"							= fc."QuoteCurrency"
        		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View" (:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpca
        			 ON bpca."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContractID"
        			AND bpca."ASSOC_FinancialContract_IDSystem"				= fc."IDSystem"
        			
        		WHERE bpca."ContractDataOwner" = 'true'
        			AND bpca."Role"	= 'Seller'
        			AND eod."QuotationType" = 'Direct'
        	)
        	WHERE "Prio" = 1
        )