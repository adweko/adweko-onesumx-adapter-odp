view "com.adweko.adapter.osx.inputdata.sft.repo::RepurchaseAgreementCash_DatagroupAssignment_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."IDSystem"															AS "FinancialContract_IDSystem",
    	fc."FinancialContractID"												AS "FinancialContract_FinancialContractID",
        fc."FinancialContractCategory"											AS "FinancialContract_FinancialContractCategory",
		fc."OriginalSigningDate"												AS "FinancialContract_OriginalSigningDate",
		fc."PurchaseDate"														AS "FinancialContract_PurchaseDate",
		fc."RepurchaseDate"														AS "FinancialContract_RepurchaseDate",
		fc."ContractualCurrency"												AS "FinancialContract_ContractualCurrency",
		fc."TotalPurchasePrice"													AS "FinancialContract_TotalPurchasePrice",
		dg."dataGroup",
		crep."CreditRiskExposure_ExposureClass"									AS "CreditRiskExposurePrio_CreditRiskExposure_ExposureClass",
		crep."CreditRiskExposure_CreditRiskCalculationMethod"					AS "CreditRiskExposurePrio_CreditRiskExposure_CreditRiskCalculationMethod",
		crep."CreditRiskExposure_CreditConversionFactorClass"					AS "CreditRiskExposurePrio_CreditRiskExposure_CreditConversionFactorClass",
		crep."exposureClass"

    FROM "com.adweko.adapter.osx.inputdata.sft.repo::RepurchaseAgreementCash_Datagroup_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) dg
    
    INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) fc
    	ON	fc."FinancialContractCategory"		= dg."FinancialContractCategory"

    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ProductCatalogAssignment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pc
    	ON	pc."ProductCatalogAssignment_FinancialContract_FinancialContractID" 		= fc."FinancialContractID" 
    	AND pc."ProductCatalogAssignment_FinancialContract_IDSystem"					= fc."IDSystem"
   		AND pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem" 		= dg."ProductCatalogItem"
    	AND pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID"	= dg."ProductCatalog_CatalogID"
    	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Repo') crep
		ON crep."CreditRiskExposure_FinancialContract_FinancialContractID" = fc."FinancialContractID"
		AND crep."CreditRiskExposure_FinancialContract_IDSystem"						= fc."IDSystem"
    	
   WHERE	IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",'')			= IFNULL(dg."ProductCatalogItem",'')
		AND IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID",'')	= IFNULL(dg."ProductCatalog_CatalogID",'')
    	AND (dg."IDSystem" IS NULL OR dg."IDSystem" = fc."IDSystem");
