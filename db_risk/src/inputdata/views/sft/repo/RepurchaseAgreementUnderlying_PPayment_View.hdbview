view "com.adweko.adapter.osx.inputdata.sft.repo::RepurchaseAgreementUnderlying_PPayment_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."FinancialContract_IDSystem",
    	fc."FinancialContract_FinancialContractID",
		fc."FinancialInstrument_FinancialInstrumentID",
		fc."dataGroup",
		'C-_-' 
			|| fc."FinancialContract_FinancialContractID" 
			|| '#REPO' 
			|| fc."FinancialInstrument_FinancialInstrumentID" 
			|| fc."FinancialContract_IDSystem"
		AS "contractID",
		'0002' AS "node_No",
		CASE 
			WHEN
				paym."PaymentScheduleCategory" = 'Installment'
				AND paym."InstallmentsRelativeToInterestPaymentDates" = true
					THEN 1	
			WHEN
				paym."PaymentScheduleCategory" = 'Installment'
				AND paym."InstallmentsRelativeToInterestPaymentDates" = false
					THEN 2
			WHEN 
				paym."PaymentScheduleCategory" = 'PaymentList'
				AND 
				(
					paym."InstallmentsRelativeToInterestPaymentDates" = false
					OR paym."InstallmentsRelativeToInterestPaymentDates" IS NULL
				)
					THEN 3
		END AS "PaymentCase",
		paym."InstallmentAmount"					AS "PaymentSchedule_InstallmentAmount",
		paym."FirstPaymentDate"						AS "PaymentSchedule_FirstPaymentDate",
		paym."InstallmentLagPeriodLength"			AS "PaymentSchedule_InstallmentLagPeriodLength",
		paym."InstallmentPeriodLength"				AS "PaymentSchedule_InstallmentPeriodLength",
		paym."InstallmentLagTimeUnit"				AS "PaymentSchedule_InstallmentLagTimeUnit",
		paym."InstallmentPeriodTimeUnit"			AS "PaymentSchedule_InstallmentPeriodTimeUnit",
		pl."PaymentAmount"							AS "PaymentList_PaymentAmount",
		pl."PaymentDate"							AS "PaymentList_PaymentDate"
    FROM "com.adweko.adapter.osx.inputdata.sft.repo::RepurchaseAgreementUnderlying_DatagroupAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) fc

    INNER JOIN "com.adweko.adapter.osx.synonyms::PaymentSchedule_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) paym 
    	ON paym."_DebtInstrument_FinancialInstrumentID"	= fc."FinancialInstrument_FinancialInstrumentID"
    		AND paym."PaymentScheduleType"	= 'Payout'
    		AND paym."PaymentScheduleCategory" IN ('Installment','PaymentList')

	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::PaymentListEntry_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) pl
    	ON pl."ASSOC_PaymentList__DebtInstrument_FinancialInstrumentID" = paym."_DebtInstrument_FinancialInstrumentID"
    		AND	pl."ASSOC_PaymentList_SequenceNumber" = paym."SequenceNumber"
    		AND paym."PaymentScheduleCategory" = 'PaymentList'
