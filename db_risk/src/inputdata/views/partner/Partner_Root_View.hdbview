VIEW "com.adweko.adapter.osx.inputdata.partner::Partner_Root_View"(IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP, IN I_DATAGROUP_STRING NVARCHAR(128))
AS
    SELECT
    
		bpdg."BusinessPartner_SourceSystemID",
		bpdg."BusinessPartner_BusinessPartnerID",
		bpdg."BusinessPartner_BusinessPartnerCategory",
		bpdg."BusinessPartner_Name",
		bpdg."BusinessPartner_GivenName",
		bpdg."BusinessPartner_MiddleName",
		bpdg."BusinessPartner_LastName",
		bpdg."BusinessPartner_GroupCategory",
		bpdg."BusinessPartner_CountryOfIncorporation",
		bpdg."BusinessPartner_ResidingCountry",
		bpdg."BusinessPartner_Register",
		bpdg."BusinessPartner_RegulatedFinancialInstitution",
		bpdg."BusinessPartner_LegalEntityUnderPublicLawType",
		bpdg."BusinessPartner_RepresentedGeographicalUnit_GeographicalStructureID",
		bpdg."BusinessPartner_RepresentedGeographicalUnit_GeographicalUnitID",
		bpdg."BusinessPartner_CountryOfMainEconomicActivities",
		bpdg."BusinessPartner_InstitutionalProtectionScheme",
		bpdg."BusinessPartner_OrganizationCategory",
		bpdg."dataGroup",
		
		defaultStatus."Status"                                         											  AS "BusinessPartnerStatus_DefaultStatus_Status",
		
		nace."IndustryClassAssignment_IndustryClassificationSystem",
		nace."IndustryClassAssignment_Industry",
		
		errbp."EuropeanRegulatoryReportingForBusinessPartner_InternationalOrganizationCode",
		errbp."EuropeanRegulatoryReportingForBusinessPartner_PublicSectorEntityTreatedAsCentralGovernment",
		errbp."EuropeanRegulatoryReportingForBusinessPartner_PublicSectorEntityTreatedAsRegionalGovernmentOrLocalAuthority",
		errbp."EuropeanRegulatoryReportingForBusinessPartner_LegalFormCode",
		errbp."EuropeanRegulatoryReportingForBusinessPartner_RegionalGovernmentOrLocalAuthorityTreatedAsCentralGovernment",
		
		abpilei."PartnerIdentifier"																				  AS "AlternativeBusinessPartnerIdentification_LegalEntityIdentifier_PartnerIdentifier",
		abpibbk."PartnerIdentifier"																				  AS "AlternativeBusinessPartnerIdentification_BBKBorrowerUnitNumber_PartnerIdentifier",
		
		roc."RoleOfCurrency"																					  AS "CurrencyRoles_RoleOfCurrency",
		roc."CurrencyCode"																						  AS "CurrencyRoles_CurrencyCode"

    FROM "com.adweko.adapter.osx.inputdata.partner::Partner_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS bpdg
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerStatus_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS defaultStatus 
        	ON  defaultStatus."ASSOC_Partner_BusinessPartnerID"							= bpdg."BusinessPartner_BusinessPartnerID"
        	AND defaultStatus."PartnerStatusCategory" 									= 'DefaultStatus'
        LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_IndustryClassAssignment"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS nace 
        	ON  nace."IndustryClassAssignment_BusinessPartnerID"						= bpdg."BusinessPartner_BusinessPartnerID"
        	AND nace."IndustryClassAssignment_IndustryClassificationSystem" 			= 'NACE'
        LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_EuropeanRegulatoryReportingForBusinessPartner"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS errbp
        	ON 	errbp."EuropeanRegulatoryReportingForBusinessPartner_BusinessPartnerID" = bpdg."BusinessPartner_BusinessPartnerID"
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::AlternativeBusinessPartnerIdentification_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS abpilei 
        	ON	abpilei."ASSOC_BusinessPartnerID_BusinessPartnerID"						= bpdg."BusinessPartner_BusinessPartnerID"
        	AND abpilei."IDSystem"														= 'LegalEntityIdentifier' --key fields authority and country are beeing ignored on purpose because they are not considered relevant (ONESUMX-8518)
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::AlternativeBusinessPartnerIdentification_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS abpibbk 
        	ON	abpibbk."ASSOC_BusinessPartnerID_BusinessPartnerID"						= bpdg."BusinessPartner_BusinessPartnerID"
        	AND abpibbk."IDSystem"														= 'BBKBorrowerUnitNumber' --key fields authority and country are beeing ignored on purpose because they are not considered relevant (ONESUMX-8518)

        --currency
        LEFT OUTER JOIN  "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS key1 
        	ON key1."KeyID" = 'counterpartyCurrencyRole'
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::CurrencyRoles_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS roc 
        	ON 	roc."_BusinessPartner_BusinessPartnerID"									= bpdg."BusinessPartner_BusinessPartnerID"
    		AND roc."RoleOfCurrency" = key1."Value"
				 								 
        
        