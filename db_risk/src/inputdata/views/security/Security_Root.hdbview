VIEW "com.adweko.adapter.osx.inputdata.security::Security_Root"
    ( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP, IN I_DATAGROUP_STRING NVARCHAR(128)  )
AS
        SELECT
        /*------ dg = Identifier for Security_DatagroupAssignment_View ------*/
    	dg."SecuritiesBalance__FinancialInstrument_FinancialInstrumentID"  			AS "SecuritiesBalance_FinancialInstrument_FinancialInstrumentID",
		dg."SecuritiesBalance__Account_IDSystem"        							AS "SecuritiesBalance_Account_IDSystem",
		dg."SecuritiesBalance__Account_FinancialContractID"     					AS "SecuritiesBalance_Account_FinancialContractID",
		dg."SecuritiesBalance_BusinessValidFrom"        							AS "SecuritiesBalance_BusinessValidFrom",
		dg."SecuritiesBalance_BusinessValidTo"      								AS "SecuritiesBalance_BusinessValidTo",
		dg."SecuritiesBalance_SystemValidFrom"      								AS "SecuritiesBalance_SystemValidFrom",
		dg."SecuritiesBalance_SystemValidTo"    									AS "SecuritiesBalance_SystemValidTo",
		dg."SecuritiesBalance_MarketValue"      									AS "SecuritiesBalance_MarketValue",
		dg."SecuritiesBalance_MarketValueCurrency"      							AS "SecuritiesBalance_MarketValueCurrency",
		dg."SecuritiesBalance_Quantity"     										AS "SecuritiesBalance_Quantity",
		dg."SecuritiesBalance_Unit"     											AS "SecuritiesBalance_Unit",
		dg."SecuritiesBalance_NominalAmount"        								AS "SecuritiesBalance_NominalAmount",
		dg."SecuritiesBalance_NominalAmountCurrency"        						AS "SecuritiesBalance_NominalAmountCurrency",
		dg."SecuritiesBalance_PurchasePrice"        								AS "SecuritiesBalance_PurchasePrice",
		dg."SecuritiesBalance_PurchasePriceCurrency"        						AS "SecuritiesBalance_PurchasePriceCurrency",
		dg."FinancialInstrument_FinancialInstrumentCategory"						AS "FinancialInstrument_FinancialInstrumentCategory",

		
		/*------ bpca = Identifier for BusinessPartnerContractAssignment_View ------*/
		bpca."ASSOC_PartnerInParticipation_BusinessPartnerID"     					AS "BusinessPartnerContractAssignment_AccountHolder_PartnerInParticipation_BusinessPartnerID",
		bpca."BusinessValidFrom"        											AS "BusinessPartnerContractAssignment_AccountHolder_BusinessValidFrom",
		bpca."BusinessValidTo"      												AS "BusinessPartnerContractAssignment_AccountHolder_BusinessValidTo",
		bpca."SystemValidFrom"      												AS "BusinessPartnerContractAssignment_AccountHolder_SystemValidFrom",
		bpca."SystemValidTo"        												AS "BusinessPartnerContractAssignment_AccountHolder_SystemValidTo",

		bpcac."ASSOC_PartnerInParticipation_BusinessPartnerID"              		AS "BusinessPartnerContractAssignment_ClearingMember_PartnerInParticipation_BusinessPartnerID",
		bpcac."BusinessValidFrom"                                       			AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidFrom",
		bpcac."BusinessValidTo"                                         			AS "BusinessPartnerContractAssignment_ClearingMember_BusinessValidTo",
		bpcac."SystemValidFrom"                                         			AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidFrom",
		bpcac."SystemValidTo"                                           			AS "BusinessPartnerContractAssignment_ClearingMember_SystemValidTo",
		
		/*------ iacc = Identifier for InstrumentAccruedInterest_View ------*/
		iacc."AccrualType"      													AS "InstrumentAccruedInterest_AccrualType",
		iacc."_SecuritiesAccount_FinancialContractID"								AS "InstrumentAccruedInterest_SecuritiesAccount_FinancialContractID",
		iacc."_SecuritiesAccount_IDSystem"											AS "InstrumentAccruedInterest_SecuritiesAccount_IDSystem",
		iacc."BusinessValidFrom"        											AS "InstrumentAccruedInterest_BusinessValidFrom",
		iacc."BusinessValidTo"      												AS "InstrumentAccruedInterest_BusinessValidTo",
		iacc."SystemValidFrom"      												AS "InstrumentAccruedInterest_SystemValidFrom",
		iacc."SystemValidTo"        												AS "InstrumentAccruedInterest_SystemValidTo",
		iacc."AmountInPositionCurrency"     										AS "InstrumentAccruedInterest_AmountInPositionCurrency",
		iacc."PositionCurrency"     												AS "InstrumentAccruedInterest_PositionCurrency",
		iacc."AmountInPaymentCurrency"      										AS "InstrumentAccruedInterest_AmountInPaymentCurrency",
		iacc."PaymentCurrency"      												AS "InstrumentAccruedInterest_PaymentCurrency",
		iacc."Quantity"     														AS "InstrumentAccruedInterest_Quantity"

	FROM "com.adweko.adapter.osx.inputdata.security::Security_DatagroupAssignment_View"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING  ) AS dg

    	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  bpca ON 
    			bpca."ASSOC_FinancialContract_FinancialContractID"			= dg."SecuritiesBalance__Account_FinancialContractID"
    		AND bpca."ASSOC_FinancialContract_IDSystem" 					= dg."SecuritiesBalance__Account_IDSystem"
    		AND bpca."Role"													= 'AccountHolder'

		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bpcac ON
				bpcac."ASSOC_FinancialContract_FinancialContractID"			= dg."SecuritiesBalance__Account_FinancialContractID" 
            AND bpcac."ASSOC_FinancialContract_IDSystem"					= dg."SecuritiesBalance__Account_IDSystem"
			AND bpcac."Role"											    = 'ClearingMember'
    						
    	/*Attention: The parameter "Businessdate" for "InstrumentAccruedInterest_View" needs to be a timestamp instead of a date. 
		---this is necessary to get all rows including those valid from 00:00 to 23:59*/						
    	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::InstrumentAccruedInterest_View"(ADD_Seconds(:I_BUSINESS_DATE, 24*60*60-1) , :I_SYSTEM_TIMESTAMP  )  iacc ON 
    			iacc."_FinancialInstrument_FinancialInstrumentID"			= dg."SecuritiesBalance__FinancialInstrument_FinancialInstrumentID"
    		AND iacc."_SecuritiesAccount_FinancialContractID"				= dg."SecuritiesBalance__Account_FinancialContractID"
    		AND iacc."_SecuritiesAccount_IDSystem"							= dg."SecuritiesBalance__Account_IDSystem"
    	
