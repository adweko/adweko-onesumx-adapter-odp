view "com.adweko.adapter.osx.inputdata.fxswap::FXSwap_Root_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."FinancialContract_IDSystem",
    	fc."FinancialContract_FinancialContractID",
        fc."FinancialContract_FinancialContractCategory",
        fc."FinancialContract_OTCDerivativeContractCategory",
        fc."FinancialContract_OriginalSigningDate",
        fc."FinancialContract_QuoteCurrency",
        fc."FinancialContract_TradedCurrency",
		fc."FinancialContract_FirstSettlementDate",
		fc."FinancialContract_SecondSettlementDate",
		fc."FinancialContract_TerminationDate",
		fc."FinancialContract_AmountInTradedCurrency",
		fc."FinancialContract_AmountInTradedCurrencySecondSettlement",
		fc."FinancialContract_PricePerTradedCurrencyUnitFirstSettlement",
		fc."FinancialContract_PricePerTradedCurrencyUnitSecondSettlement",
		fc."FinancialContract_FXSwapCategory",
		fc."FinancialContract_StructuredProduct_FinancialContractID",
		fc."dataGroup",
		"com.adweko.adapter.osx.inputdata.common::get_contractID"(
			'F', 
			'',
			fc."FinancialContract_FinancialContractID",
			fc."FinancialContract_IDSystem",
			''
		) AS "contractID",
		'0001' AS "node_No",
		bpcaBroker."ASSOC_PartnerInParticipation_BusinessPartnerID" AS "Broker_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bpcaBroker."ContractDataOwner" AS "Broker_BusinessPartnerContractAssignment_ContractDataOwner",
		bpcaSpotSeller."ASSOC_PartnerInParticipation_BusinessPartnerID" AS "SpotSeller_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bpcaSpotSeller."ContractDataOwner" AS "SpotSeller_BusinessPartnerContractAssignment_ContractDataOwner",
		bpcaSpotBuyer."ASSOC_PartnerInParticipation_BusinessPartnerID" AS "SpotBuyer_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bpcaSpotBuyer."ContractDataOwner" AS "SpotBuyer_BusinessPartnerContractAssignment_ContractDataOwner",
		MAP(kvActiveFlagSwitch."Value",
			'RoleAndAmount',
				CASE
					WHEN bpcaSpotBuyer."ContractDataOwner" = 'true' AND fc."FinancialContract_AmountInTradedCurrency" > 0
						THEN -1
					WHEN bpcaSpotBuyer."ContractDataOwner" = 'true' AND fc."FinancialContract_AmountInTradedCurrency" <= 0
						THEN 1
					WHEN bpcaSpotSeller."ContractDataOwner" = 'true' AND fc."FinancialContract_AmountInTradedCurrency" > 0
						THEN -1
					WHEN bpcaSpotSeller."ContractDataOwner" = 'true' AND fc."FinancialContract_AmountInTradedCurrency" <= 0
						THEN 1
				END,
			'Amount',
				CASE
					WHEN fc."FinancialContract_AmountInTradedCurrency" > 0
						THEN -1
					WHEN fc."FinancialContract_AmountInTradedCurrency" <= 0
						THEN 1
				END
		) AS "isActiveSign",
		bp_chouse."BusinessPartnerID" AS "BusinessPartner_ClearingHouse_BusinessPartnerID",
		pc."StandardCatalog_ProductCatalogItem",
		pc."CustomCatalog_ProductCatalogItem",
		oucam."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID" AS "OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",
		IFNULL(keylegalEntity."Value",'OrganizationalUnitContractAssignment') AS "legalEntitySwitch"
		
    FROM "com.adweko.adapter.osx.inputdata.fxswap::FXSwap_DatagroupAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) fc
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvBroker
		ON kvBroker."KeyID" = 'FXSwap_RoleBroker'

    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaBroker
    	ON  bpcaBroker."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContract_FinancialContractID" 
    	AND bpcaBroker."ASSOC_FinancialContract_IDSystem"				= fc."FinancialContract_IDSystem"
    	AND bpcaBroker."Role"											= kvBroker."Value"
    	
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaSpotSeller
    	ON  bpcaSpotSeller."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContract_FinancialContractID" 
    	AND bpcaSpotSeller."ASSOC_FinancialContract_IDSystem"				= fc."FinancialContract_IDSystem"
    	AND bpcaSpotSeller."Role"											= 'SpotSeller'
    	
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaSpotBuyer
    	ON  bpcaSpotBuyer."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContract_FinancialContractID" 
    	AND bpcaSpotBuyer."ASSOC_FinancialContract_IDSystem"			= fc."FinancialContract_IDSystem"
    	AND bpcaSpotBuyer."Role"										= 'SpotBuyer'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartner_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) bp_chouse
		ON bp_chouse."BusinessPartnerID" = 
			CASE 
				WHEN bpcaSpotSeller."ContractDataOwner" = false THEN bpcaSpotSeller."ASSOC_PartnerInParticipation_BusinessPartnerID"
				WHEN bpcaSpotBuyer."ContractDataOwner" = false THEN bpcaSpotBuyer."ASSOC_PartnerInParticipation_BusinessPartnerID" 
			END
		AND bp_chouse."RoleInClearing" IN ('ClearingHouse','CentralCounterparty')	
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) pc
		ON pc."IDSystem"				= fc."FinancialContract_IDSystem"
		AND pc."FinancialContractID"	= fc."FinancialContract_FinancialContractID" 
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitContractAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) oucam
		ON oucam."ASSOC_Contract_FinancialContractID"	= fc."FinancialContract_FinancialContractID" 
        AND oucam."ASSOC_Contract_IDSystem"				= fc."FinancialContract_IDSystem"
        AND oucam."RoleOfOrganizationalUnit"			= 'ManagingUnit'
        
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS keylegalEntity
		ON keylegalEntity."KeyID" = 'legalEntitySwitch'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvActiveFlagSwitch
		ON kvActiveFlagSwitch."KeyID" = 'DerivativeActiveFlagSwitch'