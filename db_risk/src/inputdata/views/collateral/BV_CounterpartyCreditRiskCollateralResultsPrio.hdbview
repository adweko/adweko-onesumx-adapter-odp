VIEW "com.adweko.adapter.osx.inputdata.collateral::BV_CounterpartyCreditRiskCollateralResultsPrio"
	( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP )
AS
		SELECT
			"MarginedNettingSet"															AS "CounterpartyCreditRiskCollateralResults_MarginedNettingSet",
			"_CollateralPortion_PortionNumber"												AS "CounterpartyCreditRiskCollateralResults_CollateralPortion_PortionNumber",
			"_CollateralPortion_ASSOC_CollateralAgreement_FinancialContractID"				AS "CounterpartyCreditRiskCollateralResults_CollateralPortion.ASSOC_CollateralAgreement_FinancialContractID",
			"_CollateralPortion_ASSOC_CollateralAgreement_IDSystem"							AS "CounterpartyCreditRiskCollateralResults_CollateralPortion.ASSOC_CollateralAgreement_IDSystem",
			"_ExchangeTradedNettingSet_FinancialInstrumentID"								AS "CounterpartyCreditRiskCollateralResults_ExchangeTradedNettingSet_FinancialInstrumentID",
			"_FinancialContractNettingSet_FinancialContractID"								AS "CounterpartyCreditRiskCollateralResults_FinancialContractNettingSet_FinancialContractID",
			"_FinancialContractNettingSet_IDSystem"											AS "CounterpartyCreditRiskCollateralResults_FinancialContractNettingSet_IDSystem",
			"_ResultGroup_ResultDataProvider"												AS "CounterpartyCreditRiskCollateralResults_ResultGroup_ResultDataProvider",
			"_ResultGroup_ResultGroupID"													AS "CounterpartyCreditRiskCollateralResults_ResultGroup_ResultGroupID",
			"_SecuritiesAccountOfExchangeTradedNettingSet_FinancialContractID"				AS "CounterpartyCreditRiskCollateralResults_SecuritiesAccountOfExchangeTradedNettingSet_FinancialContractID",
			"_SecuritiesAccountOfExchangeTradedNettingSet_IDSystem"							AS "CounterpartyCreditRiskCollateralResults_SecuritiesAccountOfExchangeTradedNettingSet_IDSystem",
			"BusinessValidFrom"																AS "CounterpartyCreditRiskCollateralResults_BusinessValidFrom",
			"BusinessValidTo"																AS "CounterpartyCreditRiskCollateralResults_BusinessValidTo",
			"SystemValidFrom"																AS "CounterpartyCreditRiskCollateralResults_SystemValidFrom",
			"SystemValidTo"																	AS "CounterpartyCreditRiskCollateralResults_SystemValidTo",
			"CollateralAmount"																AS "CounterpartyCreditRiskCollateralResults_CollateralAmount",
			"CollateralAmountCurrency"														AS "CounterpartyCreditRiskCollateralResults_CollateralAmountCurrency",
			"Eligible"																		AS "CounterpartyCreditRiskCollateralResults_Eligible",
			"MarginType"																	AS "CounterpartyCreditRiskCollateralResults_MarginType",
			"PostedOrReceived"																AS "CounterpartyCreditRiskCollateralResults_PostedOrReceived",
			"SegregatedAccount"																AS "CounterpartyCreditRiskCollateralResults_SegregatedAccount",
			"SourceSystemID"																AS "CounterpartyCreditRiskCollateralResults_SourceSystemID",
			"ChangeTimestampInSourceSystem"													AS "CounterpartyCreditRiskCollateralResults_ChangeTimestampInSourceSystem",
			"ChangingUserInSourceSystem"													AS "CounterpartyCreditRiskCollateralResults_ChangingUserInSourceSystem",
			"ChangingProcessType"															AS "CounterpartyCreditRiskCollateralResults_ChangingProcessType",
			"ChangingProcessID"																AS "CounterpartyCreditRiskCollateralResults_ChangingProcessID"


		FROM (
			SELECT
				"MarginedNettingSet",
                "_CollateralPortion_PortionNumber",
                "_CollateralPortion_ASSOC_CollateralAgreement_FinancialContractID",
                "_CollateralPortion_ASSOC_CollateralAgreement_IDSystem",
                "_ExchangeTradedNettingSet_FinancialInstrumentID",
                "_FinancialContractNettingSet_FinancialContractID",
                "_FinancialContractNettingSet_IDSystem",
                "_ResultGroup_ResultDataProvider",
                "_ResultGroup_ResultGroupID",
                "_SecuritiesAccountOfExchangeTradedNettingSet_FinancialContractID",
                "_SecuritiesAccountOfExchangeTradedNettingSet_IDSystem",
                "BusinessValidFrom",
                "BusinessValidTo",
                "SystemValidFrom",
                "SystemValidTo",
                "CollateralAmount",
                "CollateralAmountCurrency",
                "Eligible",
                "MarginType",
                "PostedOrReceived",
                "SegregatedAccount",
                "SourceSystemID",
                "ChangeTimestampInSourceSystem",
                "ChangingUserInSourceSystem",
                "ChangingProcessType",
                "ChangingProcessID",

				ROW_NUMBER() OVER (
					PARTITION BY
						"_CollateralPortion_ASSOC_CollateralAgreement_FinancialContractID",
						"_CollateralPortion_ASSOC_CollateralAgreement_IDSystem",
						"_CollateralPortion_PortionNumber"
					ORDER BY
						MAP("MarginedNettingSet", 
							true, 1,
							false, 2
						),
						MAP("SegregatedAccount",
							true, 1,
							false, 2
						)
					) AS "Prio"
			FROM  "com.adweko.adapter.osx.synonyms::CounterpartyCreditRiskCollateralResults_View" (:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )
		)
	WHERE "Prio" = 1