VIEW "com.adweko.adapter.osx.inputdata.collateral.collateralCash::CollateralCash_DatagroupAssignment_View"(
	IN I_BUSINESS_DATE DATE,
	IN I_SYSTEM_TIMESTAMP TIMESTAMP,
	IN I_DATAGROUP_STRING NVARCHAR(128) DEFAULT ''
    )
AS
	SELECT
		
	    /*------ dg = Identifier for CollateralCash_DatagroupAssignment_View ------*/
		fc_marginAgr."FinancialContractID"																	AS "FinancialContract_MarginAgreement_FinancialContractID",
		fc_marginAgr."IDSystem"																				AS "FinancialContract_MarginAgreement_IDSystem",
		fc_cashAcc."FinancialContractID"																	AS "FinancialContract_CA_FinancialContractID",
		MAP(kvMC."Value",
			'With', 1,
			'Without', 0
		) AS "withMarginCall",
		MAP(kvMC."Value",
			'With',		fc_cashAcc."FinancialContractID",
			'Without',	fc_marginAgr."FinancialContractID"
		) AS "FinancialContract_MAorCA_FinancialContractID",
		MAP(kvMC."Value",
			'With',		fc_cashAcc."IDSystem",
			'Without',	fc_marginAgr."IDSystem"
		) AS "FinancialContract_MAorCA_IDSystem",
		MAP(kvMC."Value",	
			'With',		fc_cashAcc."OriginalSigningDate",
			'Without',	fc_marginAgr."OriginalSigningDate"
		) AS "FinancialContract_MAorCA_OriginalSigningDate",
		MAP(kvMC."Value",
			'With',		fc_cashAcc."MaximumCollateralAmountCurrency",
			'Without',	fc_marginAgr."MaximumCollateralAmountCurrency"
		) AS "FinancialContract_MAorCA_MaximumCollateralAmountCurrency",
		dg."dataGroup",
		
	    /*------ pc = Identifier for BV_ProductCatalogAssignment ------*/
		pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem" 								AS "ProductCatalogAssignment_ProductCatalogItem"
	
	FROM "com.adweko.adapter.osx.inputdata.collateral.collateralCash::CollateralCash_Datagroup_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS dg
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) kvMC
		ON kvMC."KeyID" = 'cashCollateralMarginCallSwitch'
	
    INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fc_marginAgr
		 ON fc_marginAgr."FinancialContractCategory"												= dg."FinancialContractCategory"
		AND fc_marginAgr."CollateralAgreementCategory"												= dg."MarginAgreement_CollateralAgreementCategory"
    	AND fc_marginAgr."CollateralType"															= dg."MarginAgreement_CollateralType"

    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fc_cashAcc
    	 ON kvMC."Value" = 'With'
    	AND fc_cashAcc."ASSOC_CombinedCollateralAgreement_FinancialContractID"						= fc_marginAgr."FinancialContractID"
    	AND fc_cashAcc."ASSOC_CombinedCollateralAgreement_IDSystem" 								= fc_marginAgr."IDSystem"
    	AND fc_cashAcc."FinancialContractCategory"													= dg."FinancialContractCategory"
    	AND fc_cashAcc."CollateralAgreementCategory"												= dg."CashAccounts_CollateralAgreementCategory"
    	AND fc_cashAcc."CollateralType"																= dg."CashAccounts_CollateralType"
		
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ProductCatalogAssignment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pc
    	 ON	pc."ProductCatalogAssignment_FinancialContract_FinancialContractID" 
    		= 
    		MAP(kvMC."Value",
				'With',		fc_cashAcc."FinancialContractID",
				'Without',	fc_marginAgr."FinancialContractID")
		AND pc."ProductCatalogAssignment_FinancialContract_IDSystem"
    		=
    		MAP(kvMC."Value",
			'With',		fc_cashAcc."IDSystem",
			'Without',	fc_marginAgr."IDSystem")

    WHERE 
    	(
    		kvMC."Value" = 'With'
    		AND fc_cashAcc."ASSOC_CombinedCollateralAgreement_FinancialContractID" IS NOT NULL
    	OR
    		kvMC."Value" = 'Without'
    	)
    	AND IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",'')			= IFNULL(dg."ProductCatalogItem",'')
		AND IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID",'')	= IFNULL(dg."ProductCatalog_CatalogID",'')
    	AND (
    		dg."IDSystem" IS NULL 
    		OR 
    		dg."IDSystem" 
    		= 
    		MAP(kvMC."Value",
				'With',		fc_cashAcc."IDSystem",
				'Without',	fc_marginAgr."IDSystem")
    		)