view "com.adweko.adapter.osx.inputdata.collateral.collateralLCA::CollateralLandChargeArrangement_DatagroupAssignment_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."IDSystem"															AS "FinancialContract_IDSystem",
    	fc."FinancialContractID"												AS "FinancialContract_FinancialContractID",
        fc."FinancialContractCategory"											AS "FinancialContract_FinancialContractCategory",
        fc."CollateralAgreementCategory"										AS "FinancialContract_CollateralAgreementCategory",
		fc."SimpleCollateralCategory"											AS "FinancialContract_SimpleCollateralCategory",
		fc."CollateralType"														AS "FinancialContract_CollateralType",
		fc."GuaranteeCategory"													AS "FinancialContract_GuaranteeCategory",
		fc."OriginalSigningDate"												AS "FinancialContract_OriginalSigningDate",
		fc."Seniority"													    	AS "FinancialContract_Seniority",
		fc."ASSOC_PhysicalAsset_PhysicalAssetID"								AS "FinancialContract_PhysicalAsset_PhysicalAssetID",
		fc."MaximumCollateralAmountCurrency"									AS "FinancialContract_MaximumCollateralAmountCurrency",
		cp."CollateralPortion_CollateralAgreement_FinancialContractID",
		cp."CollateralPortion_CollateralAgreement_IDSystem",
		cp."CollateralPortion_PortionNumber",
		cp."CollateralPortion_MaximumCollateralPortionAmount",
		cp."CollateralPortion_MaximumCollateralPortionAmountCurrency",
		cp."CollateralPortion_AgreedCoverageStartDate",
		cp."CollateralPortion_AgreedCoverageEndDate",
		cp."CollateralPortion_Counter",
		cp. "CollateralPortion_CoveredPercentageOfObligations",
		'PledgeOfPhysicalAsset' AS "PledgedObject_PledgedObjectCategory",
		'PA' AS "PledgedObjectCategoryID",
		bp."CollateralProvider_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bp."CollateralProvider_BusinessPartnerContractAssignment_ContractDataOwner",
		bp."Pledger_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bp."Pledger_BusinessPartnerContractAssignment_ContractDataOwner",
		bp."CollateralPortionProvider_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bp."CollateralPortionProvider_BusinessPartnerContractAssignment_ContractDataOwner",
		bp."CollateralReceiver_BusinessPartnerContractAssignment_MarginPeriod",
		bp."CollateralReceiver_BusinessPartnerContractAssignment_MarginPeriodTimeUnit",
		bp."CollateralReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bp."CollateralReceiver_BusinessPartnerContractAssignment_ContractDataOwner",
		bp."Pledgee_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bp."Pledgee_BusinessPartnerContractAssignment_ContractDataOwner",
		bp."CollateralPortionReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bp."CollateralPortionReceiver_BusinessPartnerContractAssignment_ContractDataOwner",
		dg."dataGroup"
   
    FROM "com.adweko.adapter.osx.inputdata.collateral.collateralLCA::CollateralLandChargeArrangement_Datagroup_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) dg
    
    INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) fc
    	ON	fc."FinancialContractCategory"		= dg."FinancialContractCategory"
    	AND fc."CollateralAgreementCategory"	= dg."CollateralAgreementCategory"
    	AND fc."SimpleCollateralCategory"		= dg."SimpleCollateralCategory"
	
	INNER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralPortion"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) cp
		ON	cp."CollateralPortion_CollateralAgreement_FinancialContractID"			= fc."FinancialContractID" 
		AND cp."CollateralPortion_CollateralAgreement_IDSystem"						= fc."IDSystem"
		AND IFNULL(cp."CollateralPortion_CollateralPortionCoverageCategory",'')		<> 'CollateralPortionCoveringForeignLiabilities'
		
	INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_BusinessPartnerContractAssignmentFlat"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bp
		ON	bp."BusinessPartnerContractAssignment_FinancialContract_FinancialContractID"	= fc."FinancialContractID"
		AND	bp."BusinessPartnerContractAssignment_FinancialContract_IDSystem"				= fc."IDSystem" 
		AND (	(
				bp."CollateralReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
			AND bp."CollateralReceiver_BusinessPartnerContractAssignment_ContractDataOwner" = true )
			OR	(	
				bp."Pledgee_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
			AND bp."Pledgee_BusinessPartnerContractAssignment_ContractDataOwner" = true )
			OR	(	
				bp."CollateralPortionReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
			AND bp."CollateralPortionReceiver_BusinessPartnerContractAssignment_ContractDataOwner" = true )
		)
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ProductCatalogAssignment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pc
    	ON	pc."ProductCatalogAssignment_FinancialContract_FinancialContractID" 		= fc."FinancialContractID" 
    	AND pc."ProductCatalogAssignment_FinancialContract_IDSystem"					= fc."IDSystem"
   		AND pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem" 		= dg."ProductCatalogItem"
    	AND pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID"	= dg."ProductCatalog_CatalogID"
    	
   WHERE	IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",'')			= IFNULL(dg."ProductCatalogItem",'')
		AND IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID",'')	= IFNULL(dg."ProductCatalog_CatalogID",'')
    	AND (dg."IDSystem" IS NULL OR dg."IDSystem" = fc."IDSystem")