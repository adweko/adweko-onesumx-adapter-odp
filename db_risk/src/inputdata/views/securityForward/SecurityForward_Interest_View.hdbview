VIEW "com.adweko.adapter.osx.inputdata.securityForward::SecurityForward_Interest_View"(
	IN I_BUSINESS_DATE DATE,
	IN I_SYSTEM_TIMESTAMP TIMESTAMP,
	IN I_DATAGROUP_STRING NVARCHAR(128) DEFAULT ''
    )
AS
    SELECT
        root."FinancialContract_IDSystem",
        root."FinancialContract_FinancialContractID",
		dcm."dayCountMethod" AS "contractDayCountMethod",
		"com.adweko.adapter.osx.inputdata.common::get_businessDayConvention" (bdc."OsxBusinessDayConventionCode") AS "businessDayConvention",
		cal."calendar",
		"com.adweko.adapter.osx.inputdata.common::get_eomConvention" (inte."DayOfMonthOfInterestPeriodEnd") AS "eomConvention",		
		CASE 
			WHEN inte."InterestSpecificationCategory" = 'FixedRateSpecification'
				THEN inte."FixedRate"
			WHEN inte."InterestSpecificationCategory" = 'FloatingRateSpecification'
				THEN appl."ApplicableInterestRate_Rate"
		END AS "currentNominalInterestRate",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
						inte."ResetCutoffLength",
						mcp."cyclePeriod")
		END AS "lookbackPeriod",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN 'MONETARY_REFERENCE'
		END AS "rateCappingFlooring",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", inte."FloatingRateMax", inte."VariableRateMax")
		END AS "rateAveragingCap",	
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", inte."FloatingRateMin", inte."VariableRateMin")
		END AS "rateAveragingFloor"

    FROM "com.adweko.adapter.osx.inputdata.securityForward::SecurityForward_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) root
    
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvInterestType
		ON kvInterestType."KeyID" = 'SecurityForward_InterestType'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inteCheck
		ON inteCheck."FinancialContractID"					= root."FinancialContract_FinancialContractID"
			AND inteCheck."IDSystem" 						= root."FinancialContract_IDSystem"
			AND inteCheck."InterestType" 					= kvInterestType."Value"
			AND inteCheck."RoleOfPayer"						= root."BankLegFlag"
			AND inteCheck."FirstInterestPeriodStartDate"	<= :I_BUSINESS_DATE 
			AND	inteCheck."LastInterestPeriodEndDate"		> :I_BUSINESS_DATE			
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inte
		ON inte."FinancialContractID"						= root."FinancialContract_FinancialContractID"
			AND inte."IDSystem" 							= root."FinancialContract_IDSystem"
			AND inte."InterestType" 						= kvInterestType."Value"
			AND inte."RoleOfPayer"							= root."BankLegFlag"
			AND inte."FirstInterestPeriodStartDate"			<= :I_BUSINESS_DATE
			AND (
				inteCheck."FinancialContractID" 		IS NOT NULL
				AND inte."LastInterestPeriodEndDate"	> :I_BUSINESS_DATE
				OR
				inteCheck."FinancialContractID" 		IS NULL
				AND inte."LastInterestPeriodEndDate"	IS NULL
			)
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ApplicableInterestRate"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) appl
		ON appl."ApplicableInterestRate_FinancialContract_FinancialContractID"		= root."FinancialContract_FinancialContractID"
			AND appl."ApplicableInterestRate_FinancialContract_IDSystem"			= root."FinancialContract_IDSystem"
			AND appl."ApplicableInterestRate_InterestSequenceNumber"				= inte."SequenceNumber"

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS dcm 
		ON dcm."DayCountConvention" = inte."DayCountConvention"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_BDC_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS bdc
		ON bdc."CalculationDay" = inte."BusinessDayConvention"
		AND bdc."PaymentDay"	= inte."DueScheduleBusinessDayConvention"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_calendar_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) cal 
		ON cal."BusinessCalendar" = inte."InterestBusinessCalendar"

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" = inte."ResetCutoffTimeUnit"
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS cfss
		ON cfss."KeyID" = 'CapFloorSwitch'