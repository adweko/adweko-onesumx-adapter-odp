VIEW "com.adweko.adapter.osx.inputdata.securityForward::SecurityForward_Root_View"(
	IN I_BUSINESS_DATE DATE,
	IN I_SYSTEM_TIMESTAMP TIMESTAMP,
	IN I_DATAGROUP_STRING NVARCHAR(128) DEFAULT ''
    )
AS
    SELECT
        fc."FinancialContract_IDSystem",
        fc."FinancialContract_FinancialContractID",
        fc."FinancialContract_FinancialContractCategory",
        fc."FinancialContract_OTCDerivativeContractCategory",
		fc."FinancialContract_OriginalSigningDate",
		fc."FinancialContract_ForwardPrice",
		fc."FinancialContract_ForwardPriceCurrency",
		fc."FinancialContract_SettlementDate",
		fc."FinancialContract_SettlementMethod",
		fc."FinancialContract_Quantity",
		fc."FinancialContract_MaturityDate",
		fc."FinancialContract_NominalAmount",
		fc."FinancialContract_AccrualAmount",
		fc."FinancialContract_Underlying_FinancialInstrumentID",
		fc."dataGroup",
		"com.adweko.adapter.osx.inputdata.common::get_contractID"(
			'F', 
			'',
			fc."FinancialContract_FinancialContractID",
			fc."FinancialContract_IDSystem",
			''
		) AS "contractID",
		'0001' AS "node_No",
		fc."FinancialContract_StructuredProduct_FinancialContractID",
		bpcaBroker."ASSOC_PartnerInParticipation_BusinessPartnerID" 		AS "Broker_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bpcaBroker."ContractDataOwner"										AS "Broker_BusinessPartnerContractAssignment_ContractDataOwner",
		bpcaSeller."ASSOC_PartnerInParticipation_BusinessPartnerID"			AS "Seller_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bpcaSeller."ContractDataOwner"										AS "Seller_BusinessPartnerContractAssignment_ContractDataOwner",
		bpcaBuyer."ASSOC_PartnerInParticipation_BusinessPartnerID"			AS "Buyer_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bpcaBuyer."ContractDataOwner"										AS "Buyer_BusinessPartnerContractAssignment_ContractDataOwner",
		CASE 
			WHEN bpcaBroker."ContractDataOwner" = true
				THEN bpcaBroker."Role"
			WHEN bpcaSeller."ContractDataOwner" = true
				THEN bpcaSeller."Role"
			WHEN bpcaBuyer."ContractDataOwner" = true
				THEN bpcaBuyer."Role"
		END AS "ContractDataOwner_Role",
		CASE 
			WHEN bpcaBroker."ContractDataOwner" = true
				THEN bpcaBroker."ASSOC_PartnerInParticipation_BusinessPartnerID"
			WHEN bpcaSeller."ContractDataOwner" = true
				THEN bpcaSeller."ASSOC_PartnerInParticipation_BusinessPartnerID"
			WHEN bpcaBuyer."ContractDataOwner" = true
				THEN bpcaBuyer."ASSOC_PartnerInParticipation_BusinessPartnerID"
		END AS "ContractDataOwner_BusinessPartnerID",
		CASE 
			WHEN bpcaBuyer."ContractDataOwner" = true
				AND bpcaSeller."ContractDataOwner" = false
					THEN bpcaSeller."ASSOC_PartnerInParticipation_BusinessPartnerID"
			WHEN bpcaSeller."ContractDataOwner" = true
				AND bpcaBuyer."ContractDataOwner" = false
					THEN bpcaBuyer."ASSOC_PartnerInParticipation_BusinessPartnerID"
		END AS "counterparty",
		CASE 
			WHEN bpcaSeller."ContractDataOwner" = true
				THEN bpcaSeller."Role"
			WHEN bpcaBuyer."ContractDataOwner" = true
				THEN bpcaBuyer."Role"
		END AS "BankLegFlag",
		CASE 
			WHEN bpcaSeller."ContractDataOwner" = true
				THEN -1
			WHEN bpcaBuyer."ContractDataOwner" = true
				THEN 1
		END AS "isActiveFlagSign",
		bp_chouse."BusinessPartnerID" AS "BusinessPartner_ClearingHouse_BusinessPartnerID",
		oucam."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID" AS "OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",
		IFNULL(keylegalEntity."Value",'OrganizationalUnitContractAssignment') AS "legalEntitySwitch",
		pc."StandardCatalog_ProductCatalogItem",
		pc."CustomCatalog_ProductCatalogItem"

    FROM "com.adweko.adapter.osx.inputdata.securityForward::SecurityForward_DatagroupAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) fc

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvBroker
		ON kvBroker."KeyID" = 'SecurityForward_RoleBroker'

    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaBroker
    	ON  bpcaBroker."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContract_FinancialContractID" 
    	AND bpcaBroker."ASSOC_FinancialContract_IDSystem"				= fc."FinancialContract_IDSystem"
    	AND bpcaBroker."Role"											= kvBroker."Value"
    	
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaSeller
    	ON  bpcaSeller."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContract_FinancialContractID" 
    	AND bpcaSeller."ASSOC_FinancialContract_IDSystem"				= fc."FinancialContract_IDSystem"
    	AND bpcaSeller."Role"											= 'Seller'
    	
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaBuyer
    	ON  bpcaBuyer."ASSOC_FinancialContract_FinancialContractID"		= fc."FinancialContract_FinancialContractID" 
    	AND bpcaBuyer."ASSOC_FinancialContract_IDSystem"				= fc."FinancialContract_IDSystem"
    	AND bpcaBuyer."Role"											= 'Buyer'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitContractAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) oucam
		ON oucam."ASSOC_Contract_FinancialContractID"	= fc."FinancialContract_FinancialContractID" 
        AND oucam."ASSOC_Contract_IDSystem"				= fc."FinancialContract_IDSystem"
        AND oucam."RoleOfOrganizationalUnit"			= 'ManagingUnit'
        
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartner_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) bp_chouse
		ON bp_chouse."BusinessPartnerID" = 
			CASE 
				WHEN bpcaSeller."ContractDataOwner" = false 
					THEN bpcaBuyer."ASSOC_PartnerInParticipation_BusinessPartnerID"
				WHEN bpcaBuyer."ContractDataOwner" = false 
					THEN bpcaSeller."ASSOC_PartnerInParticipation_BusinessPartnerID"
			END
		AND bp_chouse."RoleInClearing" IN ('ClearingHouse','CentralCounterparty')	
        
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS keylegalEntity
		ON keylegalEntity."KeyID" = 'legalEntitySwitch'
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) pc
		ON pc."IDSystem"				= fc."FinancialContract_IDSystem"
		AND pc."FinancialContractID"	= fc."FinancialContract_FinancialContractID" 