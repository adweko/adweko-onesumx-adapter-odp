VIEW "com.adweko.adapter.osx.inputdata.fxforward::FXForward_Root_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
    
        /*------ dg = Identifier for Security_DatagroupAssignment_View ------*/
        dg."FinancialContract_IDSystem",
    	dg."FinancialContract_FinancialContractID",
        dg."FinancialContract_FinancialContractCategory",
		dg."FinancialContract_OriginalSigningDate",
		dg."FinancialContract_SettlementDate",
		dg."FinancialContract_MaturityDate",
		dg."FinancialContract_QuoteCurrency",
		dg."FinancialContract_TradedCurrency",
		dg."FinancialContract_PricePerTradedCurrencyUnit",
		dg."FinancialContract_AmountInTradedCurrency",
		dg."dataGroup",
		dg."FinancialContract_StructuredProduct_FinancialContractID",
		
		/*------ bpcax = Identifier for BusinessPartnerContractAssignment_View ------*/
		bpcaSeller."ASSOC_PartnerInParticipation_BusinessPartnerID" 										AS "Seller_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bpcaSeller."ContractDataOwner"																		AS "Seller_BusinessPartnerContractAssignment_ContractDataOwner",
		bpcaBuyer."ASSOC_PartnerInParticipation_BusinessPartnerID"											AS "Buyer_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bpcaBuyer."ContractDataOwner"																		AS "Buyer_BusinessPartnerContractAssignment_ContractDataOwner",
		bpcaBroker."ASSOC_PartnerInParticipation_BusinessPartnerID" 										AS "Broker_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bpcaBroker."ContractDataOwner"																		AS "Broker_BusinessPartnerContractAssignment_ContractDataOwner",
		
		/*------ bpcax = Identifier for OrganizationalUnitContractAssignment_View ------*/
		oucam."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID" AS "OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",
		
		/*------ bp_chouse = Identifier for BusinessPartner_View ------*/
		bp_chouse."RoleInClearing"																			AS "BusinessPartner_RoleInClearing",
		CAST("com.adweko.adapter.osx.inputdata.common::get_contractID"(
			'F',
			'',
			dg."FinancialContract_FinancialContractID",
			dg."FinancialContract_IDSystem",
			''
		) AS NVARCHAR(128)) "contractID",
		'0001'																								AS "node_No",

		MAP(kvActiveFlagSwitch."Value",
			'RoleAndAmount',
				CASE
					WHEN bpcaBuyer."ContractDataOwner" = 'true' AND dg."FinancialContract_AmountInTradedCurrency" > 0
						THEN -1
					WHEN bpcaBuyer."ContractDataOwner" = 'true' AND dg."FinancialContract_AmountInTradedCurrency" <= 0
						THEN 1
					WHEN bpcaSeller."ContractDataOwner" = 'true' AND dg."FinancialContract_AmountInTradedCurrency" > 0
						THEN -1
					WHEN bpcaSeller."ContractDataOwner" = 'true' AND dg."FinancialContract_AmountInTradedCurrency" <= 0
						THEN 1
				END,
			'Amount',
				CASE
					WHEN dg."FinancialContract_AmountInTradedCurrency" > 0
						THEN -1
					WHEN dg."FinancialContract_AmountInTradedCurrency" <= 0
						THEN 1
				END
		) AS "isActiveSign",
		
	pca."StandardCatalog_ProductCatalogItem",
	pca."CustomCatalog_ProductCatalogItem"

    FROM "com.adweko.adapter.osx.inputdata.fxforward::FXForward_DatagroupAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) dg

    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaSeller
    	ON  bpcaSeller."ASSOC_FinancialContract_FinancialContractID"	= dg."FinancialContract_FinancialContractID" 
    	AND bpcaSeller."ASSOC_FinancialContract_IDSystem"				= dg."FinancialContract_IDSystem"
    	AND bpcaSeller."Role"											= 'Seller'
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaBuyer
    	ON  bpcaBuyer."ASSOC_FinancialContract_FinancialContractID"	= dg."FinancialContract_FinancialContractID" 
    	AND bpcaBuyer."ASSOC_FinancialContract_IDSystem"				= dg."FinancialContract_IDSystem"
    	AND bpcaBuyer."Role"											= 'Buyer'
	
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaBroker
    	ON  bpcaBroker."ASSOC_FinancialContract_FinancialContractID"	= dg."FinancialContract_FinancialContractID" 
    	AND bpcaBroker."ASSOC_FinancialContract_IDSystem"				= dg."FinancialContract_IDSystem"
    	AND bpcaBroker."Role" = 'Broker'
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitContractAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) oucam
		ON oucam."ASSOC_Contract_FinancialContractID"	= dg."FinancialContract_FinancialContractID" 
        AND oucam."ASSOC_Contract_IDSystem"				= dg."FinancialContract_IDSystem"
        AND oucam."RoleOfOrganizationalUnit"			= 'ManagingUnit'
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS pca ON 
            	pca."IDSystem"							= dg."FinancialContract_IDSystem" 
            AND pca."FinancialContractID"				= dg."FinancialContract_FinancialContractID" 
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartner_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) bp_chouse
		ON bp_chouse."BusinessPartnerID" = 
			CASE 
				WHEN bpcaSeller."ContractDataOwner" = false THEN bpcaSeller."ASSOC_PartnerInParticipation_BusinessPartnerID"
				WHEN bpcaBuyer."ContractDataOwner" = false THEN bpcaBuyer."ASSOC_PartnerInParticipation_BusinessPartnerID" 
			END
            
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvActiveFlagSwitch
		ON kvActiveFlagSwitch."KeyID" = 'DerivativeActiveFlagSwitch'
				
	WHERE (dg."FinancialContract_FinancialContractCategory" = 'OTCDerivative' OR dg."FinancialContract_FinancialContractCategory" = 'StructuredProductSubleg')
			AND dg."FinancialContract_OTCDerivativeContractCategory" = 'FXForward'