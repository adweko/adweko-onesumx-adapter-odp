VIEW "com.adweko.adapter.osx.inputdata.leaseAgreement::LeaseAgreement_Root_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
    
    	fc."FinancialContract_IDSystem"											AS "FinancialContract_IDSystem",
    	fc."FinancialContract_FinancialContractID"								AS "FinancialContract_FinancialContractID",
    	fc."FinancialContract_OriginalSigningDate"								AS "FinancialContract_OriginalSigningDate",
    	fc."FinancialContract_LeaseTermStart"									AS "FinancialContract_LeaseTermStart",
    	fc."FinancialContract_LeaseTermEnd"										AS "FinancialContract_LeaseTermEnd",
    	fc. "PhysicalAsset_PhysicalAssetID"										AS "PhysicalAsset_PhysicalAssetID",
		fc."dataGroup",
		
		"com.adweko.adapter.osx.inputdata.common::get_contractID"(
			'F',
			NULL,
			fc."FinancialContract_FinancialContractID",
			fc."FinancialContract_IDSystem",
			''
		)																		AS "contractID",
		"com.adweko.adapter.osx.inputdata.common::get_contractID"(
			'F',
			NULL,
			fc."FinancialContract_FinancialContractID" || '#' || fc."PhysicalAsset_PhysicalAssetID",
			fc."FinancialContract_IDSystem",
			''
		)																		AS "contractID_Collateral",
		'0001'																	AS "node_No",

		bpcaLessee."ContractDataOwner"											AS "BusinessPartnerContractAssignment_Lessee_ContractDataOwner",
		bpcaLessee."ASSOC_PartnerInParticipation_BusinessPartnerID"				AS "BusinessPartnerContractAssignment_Lessee_BusinessPartnerID",
		
		bpcaLessor."ContractDataOwner"											AS "BusinessPartnerContractAssignment_Lessor_ContractDataOwner",
		bpcaLessor."ASSOC_PartnerInParticipation_BusinessPartnerID"				AS "BusinessPartnerContractAssignment_Lessor_BusinessPartnerID",
		
		bpcaadmin."ContractDataOwner"											AS "BusinessPartnerContractAssignment_Administrator_ContractDataOwner",
		bpcaadmin."ASSOC_PartnerInParticipation_BusinessPartnerID"				AS "BusinessPartnerContractAssignment_Administrator_BusinessPartnerID",
		
		pca."StandardCatalog_ProductCatalogItem",
		pca."CustomCatalog_ProductCatalogItem",

		oucam."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID" AS "OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",

		IFNULL(keylegalEntity."Value",'OrganizationalUnitContractAssignment') AS "legalEntitySwitch",

		CASE
			WHEN bpcaLessor."ContractDataOwner" = true
				THEN 1
			WHEN bpcaLessee."ContractDataOwner" = true
				THEN -1
			WHEN bpcaadmin."ContractDataOwner" = true
				THEN 1
		END AS "isActiveSign",
		val."NominalAmountAtTimeOfValuation" AS  "Valuation_NominalAmountAtTimeOfValuation",
		val."NominalAmountAtTimeOfValuationCurrency" AS "Valuation_NominalAmountAtTimeOfValuationCurrency"
	
    FROM "com.adweko.adapter.osx.inputdata.leaseAgreement::LeaseAgreement_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS fc
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Valuation_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS val
    	ON fc."FinancialContract_FinancialContractID" = val."ASSOC_FinancialContract_FinancialContractID"
		AND fc."FinancialContract_IDSystem" = val."ASSOC_FinancialContract_IDSystem"
		AND val."ValuationMethod" = 'MarkToMarket'
    
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcaLessee
		 ON bpcaLessee."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContract_FinancialContractID" 
		AND bpcaLessee."ASSOC_FinancialContract_IDSystem"						= fc."FinancialContract_IDSystem"
		AND bpcaLessee."Role"													= 'Lessee'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcaLessor
		 ON bpcaLessor."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContract_FinancialContractID" 
		AND bpcaLessor."ASSOC_FinancialContract_IDSystem"						= fc."FinancialContract_IDSystem"
		AND bpcaLessor."Role"													= 'Lessor'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcaadmin
		 ON bpcaadmin."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContract_FinancialContractID" 
		AND bpcaadmin."ASSOC_FinancialContract_IDSystem"						= fc."FinancialContract_IDSystem"
		AND bpcaadmin."Role"													= 'Administrator'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS pca 
         ON pca."IDSystem"														= fc."FinancialContract_IDSystem" 
        AND pca."FinancialContractID"											= fc."FinancialContract_FinancialContractID"    
        
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitContractAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS oucam
	     ON oucam."ASSOC_Contract_FinancialContractID"	= fc."FinancialContract_FinancialContractID" 
        AND oucam."ASSOC_Contract_IDSystem"				= fc."FinancialContract_IDSystem"
        AND oucam."RoleOfOrganizationalUnit"			= 'ManagingUnit'
        
    LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS keylegalEntity
		ON keylegalEntity."KeyID" = 'legalEntitySwitch'
      
