VIEW "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos::InstrumentOptions_Interest_View"(
	IN I_BUSINESS_DATE DATE,
	IN I_SYSTEM_TIMESTAMP TIMESTAMP,
	IN I_DATAGROUP_STRING NVARCHAR(128) DEFAULT ''
    )
AS
    SELECT
        root."contractID",
		CASE 
			WHEN inte."InterestSpecificationCategory" = 'FixedRateSpecification'
				THEN inte."FixedRate"
			WHEN inte."InterestSpecificationCategory" = 'FloatingRateSpecification'
				THEN appl."ApplicableInterestRate_Rate"
		END AS "currentNominalInterestRate"

    FROM "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos::InstrumentOptions_Root_Common_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) root
    
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvInterestType
		ON kvInterestType."KeyID" = 'InstrumentETDerivative_InterestType'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inteCheck
		ON inteCheck."_DebtInstrument_FinancialInstrumentID"	= root."FinancialInstrument_Underlying_FinancialInstrumentID"
			AND inteCheck."InterestType" 						= kvInterestType."Value"
			AND inteCheck."FirstInterestPeriodStartDate"		<= :I_BUSINESS_DATE 
			AND	inteCheck."LastInterestPeriodEndDate"			> :I_BUSINESS_DATE			
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inte
		ON inte."_DebtInstrument_FinancialInstrumentID"		= root."FinancialInstrument_Underlying_FinancialInstrumentID"
			AND inte."InterestType" 						= kvInterestType."Value"
			AND inte."FirstInterestPeriodStartDate"			<= :I_BUSINESS_DATE
			AND (
				inteCheck."_DebtInstrument_FinancialInstrumentID"	IS NOT NULL
				AND inte."LastInterestPeriodEndDate"				> :I_BUSINESS_DATE
				OR
				inteCheck."_DebtInstrument_FinancialInstrumentID" 	IS NULL
				AND inte."LastInterestPeriodEndDate"				IS NULL
			)
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ApplicableInterestRate"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) appl
		ON appl."ApplicableInterestRate_FinancialInstrument_FinancialInstrumentID"	= root."FinancialInstrument_Underlying_FinancialInstrumentID"
			AND appl."ApplicableInterestRate_InterestSequenceNumber"				= inte."SequenceNumber"
			
	WHERE
		root."FinancialInstrument_Underlying_SecurityCategory"	= 'DebtInstrument'