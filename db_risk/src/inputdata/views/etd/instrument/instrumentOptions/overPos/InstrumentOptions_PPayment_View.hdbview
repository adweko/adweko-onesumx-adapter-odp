VIEW "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos::InstrumentOptions_PPayment_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        root."FinancialContract_IDSystem",
		root."FinancialContract_FinancialContractID",
		root."dataGroup",
		root."contractID",
		root."node_No",
		root."isActiveSign",
		CASE 
			WHEN	paym."PaymentScheduleCategory" = 'Installment'
				AND paym."InstallmentsRelativeToInterestPaymentDates" = true
					THEN 1	
			WHEN	paym."PaymentScheduleCategory" = 'Installment'
				AND paym."InstallmentsRelativeToInterestPaymentDates" = false
					THEN 2
			WHEN 
				paym."PaymentScheduleCategory" = 'PaymentList'
				AND 
				(
					paym."InstallmentsRelativeToInterestPaymentDates" = false
					OR paym."InstallmentsRelativeToInterestPaymentDates" IS NULL
				)
					THEN 3
		END AS "PaymentCase",
		paym."InstallmentAmount"					AS "PaymentSchedule_InstallmentAmount",
		paym."FirstPaymentDate"						AS "PaymentSchedule_FirstPaymentDate",
		paym."InstallmentLagPeriodLength"			AS "PaymentSchedule_InstallmentLagPeriodLength",
		paym."InstallmentPeriodLength"				AS "PaymentSchedule_InstallmentPeriodLength",
		paym."InstallmentLagTimeUnit"				AS "PaymentSchedule_InstallmentLagTimeUnit",
		paym."InstallmentPeriodTimeUnit"			AS "PaymentSchedule_InstallmentPeriodTimeUnit",
		paym."PaymentScheduleType"					AS "PaymentSchedule_PaymentScheduleType",
		pl."PaymentAmount"							AS "PaymentList_PaymentAmount",
		pl."PaymentDate"							AS "PaymentList_PaymentDate",
		IFNULL(
			root."withPayout",
			ROW_NUMBER() OVER (
				PARTITION BY
					root."contractID",
					root."node_No",
					root."dataGroup",
					paym."PaymentScheduleType"
				ORDER BY
					paym."FirstPaymentDate"
			)
		) AS "Prio"
		
    FROM "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos::InstrumentOptions_Root_Common_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) root

    INNER JOIN "com.adweko.adapter.osx.synonyms::PaymentSchedule_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) paym 
    	ON paym."_DebtInstrument_FinancialInstrumentID"	= root."FinancialInstrument_Underlying_FinancialInstrumentID"
    		AND (
	    				root."withPayout"				IS NULL
	    			AND	paym."PaymentScheduleType"		IN ('Repayment','Disbursement')
	    			AND	paym."PaymentScheduleCategory"	= 'Installment'
    			OR
	    				root."withPayout"				= 1
	    			AND	paym."PaymentScheduleType"		= 'Payout'	
	    			AND (
	    					paym."PaymentScheduleCategory"	= 'Installment'
	    				OR
	    					paym."PaymentScheduleCategory"	= 'PaymentList'
		    				AND (
		    						paym."InstallmentsRelativeToInterestPaymentDates" = false
		    						OR
		    						paym."InstallmentsRelativeToInterestPaymentDates" IS NULL
    							)
	    			)
    		)
    		
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::PaymentListEntry_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) pl
    	ON pl."ASSOC_PaymentList__DebtInstrument_FinancialInstrumentID" = paym."_DebtInstrument_FinancialInstrumentID"
    		AND	pl."ASSOC_PaymentList_SequenceNumber" = paym."SequenceNumber"
    		AND paym."PaymentScheduleCategory" = 'PaymentList'
    		
	WHERE
		root."FinancialInstrument_Underlying_SecurityCategory"	= 'DebtInstrument'
		AND (
			paym."PaymentScheduleCategory"	<> 'PaymentList'
			OR
			pl."ASSOC_PaymentList__DebtInstrument_FinancialInstrumentID" IS NOT NULL
		)