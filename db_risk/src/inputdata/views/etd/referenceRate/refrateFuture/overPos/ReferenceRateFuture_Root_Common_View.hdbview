VIEW "com.adweko.adapter.osx.inputdata.refrateFuture.overPos::ReferenceRateFuture_Root_Common_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."FinancialContract_IDSystem",
		fc."FinancialContract_FinancialContractID",
		fc."dataGroup",
		fi."FinancialInstrumentID"				AS "FinancialInstrument_FinancialInstrumentID",
		fi."IssueDate"							AS "FinancialInstrument_IssueDate",
		fi."NotionalPerContract"				AS "FinancialInstrument_NotionalPerContract",
		fi."NotionalPerContractCurrency"		AS "FinancialInstrument_NotionalPerContractCurrency",
		fi."_ReferenceRate_ReferenceRateID"		AS "FinancialInstrument_ReferenceRate_ReferenceRateID",
	    COALESCE(fi."ISIN",
			fi."CUSIP",
			fi."SEDOL",
			fi."WKN",
			fi."FIGI")
		AS "isin",
		pos."Position_ExpirationDate",
		pos."Position_LongPosition",
		pos."Position_ShortPosition",
		pos."Position_NetPosition",
		pos."Position_FuturesPrice",
		'I-_-' 
			|| fc."FinancialContract_FinancialContractID"
			|| '#'
			|| fi."FinancialInstrumentID"
			|| '#'
			|| pos."Position_PositionNumber"
			|| '-_-'
			|| fc."FinancialContract_IDSystem"
		AS "contractID",
		fc."FinancialContract_FinancialContractID" 
			|| '#'
			|| fi."FinancialInstrumentID"
		AS "sourceSystemID",
		'0001' AS "node_No",
		CASE
			WHEN pos."Position_LongPosition" IS NOT NULL	AND pos."Position_ShortPosition" IS 	NULL	AND pos."Position_NetPosition" IS NULL	THEN 1
			WHEN pos."Position_LongPosition" IS		NULL	AND pos."Position_ShortPosition" IS NOT NULL	AND pos."Position_NetPosition" IS NULL	THEN -1
			WHEN pos."Position_LongPosition" IS 	NULL	AND pos."Position_ShortPosition" IS		NULL	AND pos."Position_NetPosition" > 0		THEN 1
			WHEN pos."Position_LongPosition" IS 	NULL	AND pos."Position_ShortPosition" IS		NULL	AND pos."Position_NetPosition" < 0		THEN -1
		END AS "isActiveSign",
		CASE
			WHEN pos."Position_LongPosition" IS NOT NULL	AND pos."Position_ShortPosition" IS 	NULL	AND pos."Position_NetPosition" IS NULL		THEN pos."Position_LongPosition"
			WHEN pos."Position_LongPosition" IS		NULL	AND pos."Position_ShortPosition" IS NOT NULL	AND pos."Position_NetPosition" IS NULL		THEN pos."Position_ShortPosition"
			WHEN pos."Position_LongPosition" IS 	NULL	AND pos."Position_ShortPosition" IS		NULL	AND pos."Position_NetPosition" IS NOT NULL	THEN pos."Position_NetPosition"
		END AS "Position_LSN"
		
    FROM "com.adweko.adapter.osx.inputdata.refrateFuture.overPos::ReferenceRateFuture_DatagroupAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) fc
	
	INNER JOIN "com.adweko.adapter.osx.inputdata.common::get_PositionPriority"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,'ReferenceRateFuture') pos
		ON	
		(		pos."Position_DerivativesAccount_FinancialContractID"		= fc."FinancialContract_FinancialContractID"
			AND pos."Position_DerivativesAccount_IDSystem"					= fc."FinancialContract_IDSystem"
			AND fc."FinancialContract_FinancialContractCategory"			= 'DerivativesAccount'
		)
		OR
		(		pos."Position_SecuritiesAccount_FinancialContractID"		= fc."FinancialContract_FinancialContractID"
			AND pos."Position_SecuritiesAccount_IDSystem"					= fc."FinancialContract_IDSystem"
			AND fc."FinancialContract_FinancialContractCategory"			= 'BankAccount'
		)
		
    INNER JOIN "com.adweko.adapter.osx.synonyms::FinancialInstrument_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) fi
		ON	fi."FinancialInstrumentID"				= pos."Position_FinancialInstrument_FinancialInstrumentID"
		AND	fi."FinancialInstrumentCategory"		= 'DerivativeContractSpecification'
		AND	fi."ExchangeTradedDerivativeCategory"	= 'FuturesContractSpecification'
		AND	fi."FuturesCategory"					= 'ReferenceRateFutureSpecification'
		
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ProductCatalogAssignment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pc
    	ON	pc."ProductCatalogAssignment_FinancialInstrument_FinancialInstrumentID" 	= fi."FinancialInstrumentID"
   		AND pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem" 		= fc."ProductCatalogAssignment_FI_ProductCatalogItem"
    	AND pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID"	= fc."ProductCatalogAssignment_FI_ProductCatalog_CatalogID"
    	
	WHERE	IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",'')			= IFNULL(fc."ProductCatalogAssignment_FI_ProductCatalogItem",'')
		AND IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID",'')	= IFNULL(fc."ProductCatalogAssignment_FI_ProductCatalog_CatalogID",'')