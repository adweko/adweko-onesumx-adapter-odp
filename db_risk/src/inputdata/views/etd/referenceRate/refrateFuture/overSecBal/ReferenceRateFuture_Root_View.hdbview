VIEW "com.adweko.adapter.osx.inputdata.refrateFuture.overSecBal::ReferenceRateFuture_Root_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        root."FinancialContract_IDSystem",
		root."FinancialContract_FinancialContractID",
		root."FinancialInstrument_FinancialInstrumentID",
		root."dataGroup",
		root."FinancialInstrument_IssueDate",
		root."FinancialInstrument_NotionalPerContract",
		root."FinancialInstrument_NotionalPerContractCurrency",
		root."FinancialInstrument_ExpirationDate",
	    root."isin",
		root."SecuritiesBalance_Quantity",
		root."contractID",
		root."sourceSystemID",
		root."node_No",
		root."isActiveSign",
		rr."EndOfDayRateObservation_Close",
		drHist."EndOfDayListedDerivativePriceObservation_Close" AS "EndOfDayListedDerivativePriceObservation_Close4IssueDate",
		CASE 
			WHEN bpcaAccountProvider."ContractDataOwner" = true
				THEN bpcaAccountProvider."ASSOC_PartnerInParticipation_BusinessPartnerID"
			WHEN bpcaAccountHolder."ContractDataOwner" = true
				THEN bpcaAccountHolder."ASSOC_PartnerInParticipation_BusinessPartnerID"
		END AS "ContractDataOwner_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		CASE 
			WHEN bpcaAccountProvider."ContractDataOwner" = true
				THEN bpcaAccountProvider."ASSOC_PartnerInParticipation_BusinessPartnerID"
			WHEN bpcaAccountHolder."ContractDataOwner" = true
				THEN bpcaAccountHolder."ASSOC_PartnerInParticipation_BusinessPartnerID"
		END AS "ContractDataOwner_BusinessPartnerID",
		CASE 
			WHEN bpcaAccountHolder."ContractDataOwner" = false
				THEN bpcaAccountHolder."ASSOC_PartnerInParticipation_BusinessPartnerID"
		END AS "counterparty",
		bp_chouse."BusinessPartnerID" AS "clearingHouse",
		pc."StandardCatalog_ProductCatalogItem",
		pc."CustomCatalog_ProductCatalogItem",
		oucam."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID" AS "OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",
		IFNULL(keylegalEntity."Value",'OrganizationalUnitContractAssignment') AS "legalEntitySwitch"
		
    FROM "com.adweko.adapter.osx.inputdata.refrateFuture.overSecBal::ReferenceRateFuture_Root_Common_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) root
        
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS keyRRDP
		ON keyRRDP."KeyID" = 'ReferenceRate_RateDataProvider'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ReferenceRate"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) rr
		ON	rr."ReferenceRate_ReferenceRateID"				= root."FinancialInstrument_ReferenceRate_ReferenceRateID"
		AND rr."EndOfDayRateObservation_RateDataProvider"	= keyRRDP."Value"
        
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS keyDRPDP
		ON keyDRPDP."KeyID" = 'DerivativeRate_PriceDataProvider'
		
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_DerivativeRate_SQLView" drHist
		ON	drHist."EndOfDayListedDerivativePriceObservation_FinancialInstrument_FinancialInstrumentID"			= root."FinancialInstrument_FinancialInstrumentID"
		AND drHist."EndOfDayListedDerivativePriceObservation_PriceDataProvider"									= keyDRPDP."Value"
		AND drHist."EndOfDayListedDerivativePriceObservation_EndOfDayListedDerivativePriceObservationCategory"	= 'EndOfDayFuturesPriceObservation'
		AND drHist."EndOfDayListedDerivativePriceObservation_ExpirationDate"									= root."FinancialInstrument_ExpirationDate"
		AND drHist."EndOfDayListedDerivativePriceObservation_BusinessValidFrom" 								= root."FinancialInstrument_IssueDate"
		AND drHist."EndOfDayListedDerivativePriceObservation_SystemValidFrom"									<= :I_SYSTEM_TIMESTAMP
		AND drHist."EndOfDayListedDerivativePriceObservation_SystemValidTo"										> :I_SYSTEM_TIMESTAMP
    	
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaAccountHolder
    	ON  bpcaAccountHolder."ASSOC_FinancialContract_FinancialContractID"		= root."FinancialContract_FinancialContractID" 
    	AND bpcaAccountHolder."ASSOC_FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"
    	AND bpcaAccountHolder."Role"											= 'AccountHolder'
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaAccountProvider
    	ON  bpcaAccountProvider."ASSOC_FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID" 
    	AND bpcaAccountProvider."ASSOC_FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"
    	AND bpcaAccountProvider."Role"											= 'AccountProvider'
    	
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaRatingSubstitute
    	ON  bpcaRatingSubstitute."ASSOC_FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID" 
    	AND bpcaRatingSubstitute."ASSOC_FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"
    	AND bpcaRatingSubstitute."Role"											= 'RatingSubstitute'
    	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartner_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) bp_chouse
		ON bp_chouse."RoleInClearing" IN ('ClearingHouse','CentralCounterparty')
		AND (
					bpcaAccountHolder."ContractDataOwner" = false
				AND bp_chouse."BusinessPartnerID" = bpcaAccountHolder."ASSOC_PartnerInParticipation_BusinessPartnerID"
			OR
					bpcaRatingSubstitute."ContractDataOwner" = false
				AND bp_chouse."BusinessPartnerID" = bpcaRatingSubstitute."ASSOC_PartnerInParticipation_BusinessPartnerID"
		)
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitContractAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) oucam
		ON oucam."ASSOC_Contract_FinancialContractID"	= root."FinancialContract_FinancialContractID" 
        AND oucam."ASSOC_Contract_IDSystem"				= root."FinancialContract_IDSystem"
        AND oucam."RoleOfOrganizationalUnit"			= 'ManagingUnit'
        
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS keylegalEntity
		ON keylegalEntity."KeyID" = 'legalEntitySwitch'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) pc
		ON pc."IDSystem"				= root."FinancialContract_IDSystem"
		AND pc."FinancialContractID"	= root."FinancialContract_FinancialContractID"