VIEW "com.adweko.adapter.osx.inputdata.refrateOptions.overSecBal::ReferenceRateOptions_Root_Common_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."FinancialContract_IDSystem",
		fc."FinancialContract_FinancialContractID",
		fc."dataGroup",
		fi."FinancialInstrumentID"				AS "FinancialInstrument_FinancialInstrumentID",
		fi."IssueDate"							AS "FinancialInstrument_IssueDate",
		fi."NotionalPerContract"				AS "FinancialInstrument_NotionalPerContract",
		fi."NotionalPerContractCurrency"		AS "FinancialInstrument_NotionalPerContractCurrency",
		fi."_ReferenceRate_ReferenceRateID"		AS "FinancialInstrument_ReferenceRate_ReferenceRateID",
		fi."ExpirationDate"						AS "FinancialInstrument_ExpirationDate",
		fi."ExerciseStyle"						AS "FinancialInstrument_ExerciseStyle",
		fi."SettlementMethod"						AS "FinancialInstrument_SettlementMethod",
	    COALESCE(fi."ISIN",
			fi."CUSIP",
			fi."SEDOL",
			fi."WKN",
			fi."FIGI")
		AS "isin",
		secbal."SecuritiesBalance_Quantity",
		secbal."SecuritiesBalance_MarketValue",
		secbal."SecuritiesBalance_BusinessValidFrom",
		secbal."SecuritiesBalance_PurchasePrice",
		"com.adweko.adapter.osx.inputdata.common::get_contractID"(
			'I', 
			fi."FinancialInstrumentID",
			fc."FinancialContract_FinancialContractID",
			fc."FinancialContract_IDSystem",
			''
		) AS "contractID",
		fc."FinancialContract_FinancialContractID" 
			|| '#'
			|| fi."FinancialInstrumentID"
		AS "sourceSystemID",
		'0001' AS "node_No",
		CASE
			WHEN secbal."SecuritiesBalance_MarketValue" > 0 THEN 1
			WHEN secbal."SecuritiesBalance_MarketValue" < 0 THEN -1
		END AS "isActiveSign",
		CASE
			WHEN secbal."SecuritiesBalance_MarketValue" > 0 THEN '1'
			WHEN secbal."SecuritiesBalance_MarketValue" < 0 THEN '2'
		END AS "optionHolder"	
		
    FROM "com.adweko.adapter.osx.inputdata.refrateOptions.overSecBal::ReferenceRateOptions_DatagroupAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) fc
	
	INNER JOIN "com.adweko.adapter.osx.inputdata.common::get_SecuritiesBalancePriority"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,'ReferenceRateOptions') secbal
		ON	secbal."SecuritiesBalance_Account_FinancialContractID"		= fc."FinancialContract_FinancialContractID"
		AND	secbal."SecuritiesBalance_Account_IDSystem"					= fc."FinancialContract_IDSystem"
		
    INNER JOIN "com.adweko.adapter.osx.synonyms::FinancialInstrument_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) fi
		ON	fi."FinancialInstrumentID"					= secbal."SecuritiesBalance_FinancialInstrument_FinancialInstrumentID"
		AND	fi."FinancialInstrumentCategory"			= 'DerivativeContractSpecification'
		AND	fi."ExchangeTradedDerivativeCategory"		= 'OptionsContractSpecification'
		AND	fi."OptionContractSpecificationCategory"	= 'ReferenceRateOptionsSpecification'
		
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ProductCatalogAssignment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pc
    	ON	pc."ProductCatalogAssignment_FinancialInstrument_FinancialInstrumentID" 	= fi."FinancialInstrumentID"
   		AND pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem" 		= fc."ProductCatalogAssignment_FI_ProductCatalogItem"
    	AND pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID"	= fc."ProductCatalogAssignment_FI_ProductCatalog_CatalogID"
    	
	WHERE	IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",'')			= IFNULL(fc."ProductCatalogAssignment_FI_ProductCatalogItem",'')
		AND IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID",'')	= IFNULL(fc."ProductCatalogAssignment_FI_ProductCatalog_CatalogID",'')