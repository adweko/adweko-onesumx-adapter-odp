view "com.adweko.adapter.osx.inputdata.interestBearingSwap.irswap::InterestRateSwap_RootCommon_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."FinancialContract_IDSystem",
    	fc."FinancialContract_FinancialContractID",
    	fc."FinancialContract_StructuredProduct_FinancialContractID",
        fc."FinancialContract_FinancialContractCategory",
		fc."FinancialContract_OriginalSigningDate",
		fc."FinancialContract_InterestRateSwapCategory",
		fc."FinancialContract_NotionalAmount",
		fc."FinancialContract_NotionalAmountCurrency",
		fc."FinancialContract_TerminationDate",
		fc."FinancialContract_MaturityDate",
		fc."FinancialContract_EffectiveDate",
		fc."dataGroup",
		bpcaBroker."ASSOC_PartnerInParticipation_BusinessPartnerID" 		AS "Broker_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bpcaBroker."ContractDataOwner"										AS "Broker_BusinessPartnerContractAssignment_ContractDataOwner",
		bpcaLeg1Payer."ASSOC_PartnerInParticipation_BusinessPartnerID"		AS "Leg1Payer_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bpcaLeg1Payer."ContractDataOwner"									AS "Leg1Payer_BusinessPartnerContractAssignment_ContractDataOwner",
		bpcaLeg2Payer."ASSOC_PartnerInParticipation_BusinessPartnerID"		AS "Leg2Payer_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
		bpcaLeg2Payer."ContractDataOwner"									AS "Leg2Payer_BusinessPartnerContractAssignment_ContractDataOwner",
		CASE 
			WHEN bpcaBroker."ContractDataOwner" = true
				THEN 'Broker'
			WHEN bpcaLeg1Payer."ContractDataOwner" = true
				THEN bpcaLeg1Payer."Role"
			WHEN bpcaLeg2Payer."ContractDataOwner" = true
				THEN bpcaLeg2Payer."Role"
		END AS "ContractDataOwner_Role",
		pc."StandardCatalog_ProductCatalogItem",
		pc."CustomCatalog_ProductCatalogItem"

    FROM "com.adweko.adapter.osx.inputdata.interestBearingSwap.irswap::InterestRateSwap_DatagroupAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) fc

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvBroker
		ON kvBroker."KeyID" = 'InterestRateSwap_RoleBroker'

    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaBroker
    	ON  bpcaBroker."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContract_FinancialContractID" 
    	AND bpcaBroker."ASSOC_FinancialContract_IDSystem"				= fc."FinancialContract_IDSystem"
    	AND bpcaBroker."Role"											= kvBroker."Value"
    	
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaLeg1Payer
    	ON  bpcaLeg1Payer."ASSOC_FinancialContract_FinancialContractID"		= fc."FinancialContract_FinancialContractID" 
    	AND bpcaLeg1Payer."ASSOC_FinancialContract_IDSystem"				= fc."FinancialContract_IDSystem"
    	AND bpcaLeg1Payer."Role"											= 'Leg1Payer'
    	
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcaLeg2Payer
    	ON  bpcaLeg2Payer."ASSOC_FinancialContract_FinancialContractID"		= fc."FinancialContract_FinancialContractID" 
    	AND bpcaLeg2Payer."ASSOC_FinancialContract_IDSystem"				= fc."FinancialContract_IDSystem"
    	AND bpcaLeg2Payer."Role"											= 'Leg2Payer'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) pc
		ON pc."IDSystem"				= fc."FinancialContract_IDSystem"
		AND pc."FinancialContractID"	= fc."FinancialContract_FinancialContractID" 