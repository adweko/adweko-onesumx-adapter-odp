view "com.adweko.adapter.osx.inputdata.interestBearingSwap.ccswap::CrossCurrencySwap_DatagroupAssignment_View"(
	IN I_BUSINESS_DATE DATE,
    IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    IN I_DATAGROUP_STRING NVARCHAR(128)
    )
AS
    SELECT
        fc."IDSystem"															AS "FinancialContract_IDSystem",
    	fc."FinancialContractID"												AS "FinancialContract_FinancialContractID",
    	fc."_StructuredProduct_FinancialContractID"								AS "FinancialContract_StructuredProduct_FinancialContractID",
        fc."FinancialContractCategory"											AS "FinancialContract_FinancialContractCategory",
        fc."OTCDerivativeContractCategory"										AS "FinancialContract_OTCDerivativeContractCategory",
		fc."OriginalSigningDate"												AS "FinancialContract_OriginalSigningDate",
		fc."TerminationDate"													AS "FinancialContract_TerminationDate",
		fc."MaturityDate"														AS "FinancialContract_MaturityDate",
		fc."EffectiveDate"														AS "FinancialContract_EffectiveDate",
		fc."Leg1NotionalAmount"													AS "FinancialContract_Leg1NotionalAmount",
		fc."Leg2NotionalAmount"													AS "FinancialContract_Leg2NotionalAmount",
		fc."Leg1NotionalAmountCurrency"											AS "FinancialContract_Leg1NotionalAmountCurrency",
		fc."Leg2NotionalAmountCurrency"											AS "FinancialContract_Leg2NotionalAmountCurrency",
		fc."PrincipalExchangeAtStart"											AS "FinancialContract_PrincipalExchangeAtStart",
		fc."PrincipalExchangeAtEnd"												AS "FinancialContract_PrincipalExchangeAtEnd",
		fc."InterimPrincipalExchange"											AS "FinancialContract_InterimPrincipalExchange",
		dg."dataGroup"

    FROM "com.adweko.adapter.osx.inputdata.interestBearingSwap.ccswap::CrossCurrencySwap_Datagroup_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) dg
    
    INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) fc
    	ON	fc."FinancialContractCategory"				= dg."FinancialContractCategory"
    	AND fc."OTCDerivativeContractCategory"			= dg."OTCDerivativeContractCategory"
    	AND fc."SwapCategory"							= dg."SwapCategory"
    	AND	fc."_BasedOnSwaption_FinancialContractID"	IS NULL
		AND fc."_BasedOnSwaption_IDSystem"				IS NULL

    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ProductCatalogAssignment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pc
    	ON	pc."ProductCatalogAssignment_FinancialContract_FinancialContractID" 		= fc."FinancialContractID" 
    	AND pc."ProductCatalogAssignment_FinancialContract_IDSystem"					= fc."IDSystem"
   		AND pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem" 		= dg."ProductCatalogItem"
    	AND pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID"	= dg."ProductCatalog_CatalogID"
    	
   WHERE	IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem",'')			= IFNULL(dg."ProductCatalogItem",'')
		AND IFNULL(pc."ProductCatalogAssignment_ProductCatalogItem__ProductCatalog_CatalogID",'')	= IFNULL(dg."ProductCatalog_CatalogID",'')
    	AND (dg."IDSystem" IS NULL OR dg."IDSystem" = fc."IDSystem")