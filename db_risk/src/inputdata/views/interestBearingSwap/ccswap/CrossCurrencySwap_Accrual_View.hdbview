VIEW "com.adweko.adapter.osx.inputdata.interestBearingSwap.ccswap::CrossCurrencySwap_Accrual_View"(
	IN I_BUSINESS_DATE DATE,
	IN I_SYSTEM_TIMESTAMP TIMESTAMP,
	IN I_DATAGROUP_STRING NVARCHAR(128) DEFAULT '',
	IN I_LEG NVARCHAR(4)
    )
AS
	SELECT 
		fc."FinancialContract_FinancialContractID",
		fc."FinancialContract_IDSystem",
		TO_DECIMAL(
			IFNULL(
				MAP(kvPaymentOrPosition."Value",
					'Payment',	SUM( accr."AmountInPaymentCurrency"		* "com.adweko.adapter.osx.inputdata.common::get_accrualIncomeOrExpenseSign"( accr."AccrualType") ),
					'Position', SUM( accr."AmountInPositionCurrency"	* "com.adweko.adapter.osx.inputdata.common::get_accrualIncomeOrExpenseSign"( accr."AccrualType") ) ),
				0 ),
			38,
			11
	) AS "accruedInterest"

	FROM "com.adweko.adapter.osx.inputdata.interestBearingSwap.ccswap::CrossCurrencySwap_RootCommon_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP , :I_DATAGROUP_STRING) fc
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Accrual_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) accr 
		ON accr."ASSOC_FinancialContract_IDSystem"               		= fc."FinancialContract_IDSystem" 
			AND accr."ASSOC_FinancialContract_FinancialContractID"		= fc."FinancialContract_FinancialContractID"
			AND accr."RoleOfPayer"										= :I_LEG || 'Payer'
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) kvPaymentOrPosition
		ON kvPaymentOrPosition."KeyID" = 'AccrualsPaymentOrPosition'

    GROUP BY 
		fc."FinancialContract_FinancialContractID",
		fc."FinancialContract_IDSystem",
		kvPaymentOrPosition."Value"