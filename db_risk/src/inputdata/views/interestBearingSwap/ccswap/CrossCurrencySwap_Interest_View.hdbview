VIEW "com.adweko.adapter.osx.inputdata.interestBearingSwap.ccswap::CrossCurrencySwap_Interest_View"(
	IN I_BUSINESS_DATE DATE,
	IN I_SYSTEM_TIMESTAMP TIMESTAMP,
	IN I_DATAGROUP_STRING NVARCHAR(128) DEFAULT '',
	IN I_LEG NVARCHAR(4)
    )
AS
    SELECT
        root."FinancialContract_IDSystem",
        root."FinancialContract_FinancialContractID",
		dcm."dayCountMethod" AS "contractDayCountMethod",
		"com.adweko.adapter.osx.inputdata.common::get_interestPaymentType" (inte."InterestInAdvance", kv1."Value", kv2."Value") AS "interestPaymentType",
		"com.adweko.adapter.osx.inputdata.common::get_businessDayConvention" (bdc."OsxBusinessDayConventionCode") AS "businessDayConvention",
		cal."calendar",
		"com.adweko.adapter.osx.inputdata.common::get_eomConvention" (inte."DayOfMonthOfInterestPeriodEnd") AS "eomConvention",		
		
		MAP(inte."InterestSpecificationCategory",
			'FixedRateSpecification', "com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (inte."FixedRate", 'rateAdd', kv."Value"),
			'FloatingRateSpecification', "com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (appl."ApplicableInterestRate_Rate", 'rateAdd', kv."Value")
		) AS "currentNominalInterestRate",
		
		inte."InterestSpecificationCategory" AS "Interest_InterestSpecificationCategory",
		inte."FirstInterestPeriodEndDate" AS "Interest_FirstInterestPeriodEndDate",
		inte."InterestPeriodLength" AS "Interest_InterestPeriodLength",
		inte."InterestPeriodTimeUnit" AS "Interest_InterestPeriodTimeUnit",
		inte."FixingRateSpecificationCategory" AS "Interest_FixingRateSpecificationCategory",
		inte."FirstInterestPeriodStartDate" AS "Interest_FirstInterestPeriodStartDate",
        inte."FirstRegularFloatingRateResetDate" AS "Interest_FirstRegularFloatingRateResetDate",
        "com.adweko.adapter.osx.inputdata.interestBearingSwap::get_activeFlagSign"(
			I_Leg					=> :I_LEG,
			I_DORole				=> root."ContractDataOwner_Role",
			I_PayingOrReceiving		=> inte_dataOwner."PayingOrReceiving"
        ) AS "isActiveFlagLegSign",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
						inte."ResetCutoffLength",
						mcp."cyclePeriod")
		END AS "lookbackPeriod",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN 'MONETARY_REFERENCE'
		END AS "rateCappingFlooring",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", inte."FloatingRateMax", inte."VariableRateMax")
		END AS "rateAveragingCap",	
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", inte."FloatingRateMin", inte."VariableRateMin")
		END AS "rateAveragingFloor"

    FROM "com.adweko.adapter.osx.inputdata.interestBearingSwap.ccswap::CrossCurrencySwap_RootCommon_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) root
    
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvInterestType
		ON kvInterestType."KeyID" = 'CrossCurrencySwap_InterestType'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inteCheck
		ON inteCheck."FinancialContractID"					= root."FinancialContract_FinancialContractID"
			AND inteCheck."IDSystem" 						= root."FinancialContract_IDSystem"
			AND inteCheck."InterestType" 					= kvInterestType."Value"
			AND inteCheck."RoleOfPayer"						= :I_LEG || 'Payer'
			AND inteCheck."FirstInterestPeriodStartDate"	<= :I_BUSINESS_DATE 
			AND	inteCheck."LastInterestPeriodEndDate"		> :I_BUSINESS_DATE			
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inte
		ON inte."FinancialContractID"						= root."FinancialContract_FinancialContractID"
			AND inte."IDSystem" 							= root."FinancialContract_IDSystem"
			AND inte."InterestType" 						= kvInterestType."Value"
			AND inte."RoleOfPayer"							= :I_LEG || 'Payer'
			AND inte."FirstInterestPeriodStartDate"			<= :I_BUSINESS_DATE
			AND (
				inteCheck."FinancialContractID" 		IS NOT NULL
				AND inte."LastInterestPeriodEndDate"	> :I_BUSINESS_DATE
				OR
				inteCheck."FinancialContractID" 		IS NULL
				AND inte."LastInterestPeriodEndDate"	IS NULL
			)
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inteCheck_dataOwner
		ON inteCheck_dataOwner."FinancialContractID"				= root."FinancialContract_FinancialContractID"
			AND inteCheck_dataOwner."IDSystem" 						= root."FinancialContract_IDSystem"
			AND inteCheck_dataOwner."InterestType" 					= kvInterestType."Value"
			AND inteCheck_dataOwner."RoleOfPayer"					= root."ContractDataOwner_Role"
			AND inteCheck_dataOwner."FirstInterestPeriodStartDate"	<= :I_BUSINESS_DATE 
			AND	inteCheck_dataOwner."LastInterestPeriodEndDate"		> :I_BUSINESS_DATE			
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inte_dataOwner
		ON inte_dataOwner."FinancialContractID"						= root."FinancialContract_FinancialContractID"
			AND inte_dataOwner."IDSystem" 							= root."FinancialContract_IDSystem"
			AND inte_dataOwner."InterestType" 						= kvInterestType."Value"
			AND inte_dataOwner."RoleOfPayer"						= root."ContractDataOwner_Role"
			AND inte_dataOwner."FirstInterestPeriodStartDate"		<= :I_BUSINESS_DATE
			AND (
				inteCheck_dataOwner."FinancialContractID" 		IS NOT NULL
				AND inte_dataOwner."LastInterestPeriodEndDate"	> :I_BUSINESS_DATE
				OR
				inteCheck_dataOwner."FinancialContractID" 		IS NULL
				AND inte_dataOwner."LastInterestPeriodEndDate"	IS NULL
			)
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ApplicableInterestRate"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) appl
		ON appl."ApplicableInterestRate_FinancialContract_FinancialContractID"		= root."FinancialContract_FinancialContractID"
			AND appl."ApplicableInterestRate_FinancialContract_IDSystem"			= root."FinancialContract_IDSystem"
			AND appl."ApplicableInterestRate_InterestSequenceNumber"				= inte."SequenceNumber"

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS dcm 
		ON dcm."DayCountConvention" = inte."DayCountConvention"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP)  AS kv1
		ON  kv1."KeyID" = 'TRUE'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv2
		ON  kv2."KeyID" = 'FALSE'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_BDC_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS bdc
		ON bdc."CalculationDay" = inte."BusinessDayConvention"
		AND bdc."PaymentDay"	= inte."DueScheduleBusinessDayConvention"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_calendar_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) cal 
		ON cal."BusinessCalendar" = inte."InterestBusinessCalendar"

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" = inte."ResetCutoffTimeUnit"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS cfss
			ON cfss."KeyID" = 'CapFloorSwitch'
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
			ON  kv."KeyID" = 'SAPPercentageStandard'