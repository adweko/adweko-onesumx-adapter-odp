FUNCTION "com.adweko.adapter.osx.inputdata.guarantee.SyndGuarantee::SyndGuarantee_get_Payment" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP
		)
RETURNS TABLE (
		"FinancialContract_IDSystem" NVARCHAR(40),
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"PaymentSchedule_PaymentScheduleType" NVARCHAR(40),
		"ppAmount" DECIMAL(34, 6),
		"cycleStartDateOrig" DATE,
		"cycleTimeUnit" NVARCHAR(12),
		"cycleLength" DECIMAL(34, 6),
		"cycleEndDate" DATE,
		"FinancialContract_AmortizationType" NVARCHAR(40)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/**************************************************
        Select Payment Data of a Loan
    *************************************************/
	RETURN
	
	SELECT 
		fc."IDSystem" AS "FinancialContract_IDSystem",
		fc."FinancialContractID" AS "FinancialContract_FinancialContractID",
		ps."PaymentSchedule_PaymentScheduleType" AS "PaymentSchedule_PaymentScheduleType",
		--ps."PaymentSchedule_InstallmentAmount" AS "ppAmount",
		
		CASE WHEN ps."PaymentSchedule_PaymentScheduleType" = 'Repayment' AND ps."PaymentSchedule_InstallmentAmount" IS NOT NULL THEN
		    CASE
    		    WHEN bpcl."ContractDataOwner" = TRUE /*Active loan*/ THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" (ps."PaymentSchedule_InstallmentAmount") 
    		    WHEN bpca."ContractDataOwner" = TRUE /*Passive loan*/ THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" (ps."PaymentSchedule_InstallmentAmount") 
		        ELSE NULL
		    END
		WHEN ps."PaymentSchedule_PaymentScheduleType" = 'Disbursement' AND ps."PaymentSchedule_InstallmentAmount" IS NOT NULL THEN
		    CASE 
    		    WHEN bpcl."ContractDataOwner" = TRUE /*Active loan*/ THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" (ps."PaymentSchedule_InstallmentAmount") 
    		    WHEN bpca."ContractDataOwner" = TRUE /*Passive loan*/ THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" (ps."PaymentSchedule_InstallmentAmount") 
    		    ELSE NULL
		    END
		ELSE NULL
		END AS "ppAmount",
		"com.adweko.adapter.osx.inputdata.common::get_date_add" (
			ps."PaymentSchedule_FirstPaymentDate",
			ps."PaymentSchedule_InstallmentLagPeriodLength",
			ps."PaymentSchedule_InstallmentLagTimeUnit"
		) AS "cycleStartDateOrig",
		CAST(ps."PaymentSchedule_InstallmentPeriodTimeUnit" AS NVARCHAR(12)) AS "cycleTimeUnit",
		ps."PaymentSchedule_InstallmentPeriodLength" AS "cycleLength",
		ps."PaymentSchedule_LastPaymentDate" AS "cycleEndDate",
		fc."AmortizationType" AS "FinancialContract_AmortizationType"
	FROM "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fc

	INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_PaymentSchedule"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS ps
		ON ps."PaymentSchedule_FinancialContractID" = fc."FinancialContractID"
		AND ps."PaymentSchedule_IDSystem" = fc."IDSystem"
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpca ON 
            bpca."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
        AND bpca."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
		AND bpca."Role"											        = 'Borrower'     
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcl ON 
            bpcl."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
        AND bpcl."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
		AND bpcl."Role"											        = 'Lender'   
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMonetaryBalance"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP, 'Loan' )  mb ON --filter for currency
            mb."FinancialContractID"									= fc."FinancialContractID" 
        AND mb."IDSystem"												= fc."IDSystem"
        AND mb."MonetaryBalanceCategory"								= 'LoanBalance'		
		
	WHERE
		fc."FinancialContractCategory" = 'Facility'
		AND (
			ps."PaymentSchedule_PaymentScheduleType" = 'Repayment'
			OR (
				ps."PaymentSchedule_PaymentScheduleType" = 'Disbursement'
				AND ps."PaymentSchedule_InstallmentAmount" IS NOT NULL
			)
		)
		AND ps."PaymentSchedule_PaymentScheduleCategory" != 'PaymentList'
		AND ps."PaymentSchedule_FirstPaymentDate" IS NOT NULL
		AND fc."_TrancheInSyndication__SyndicationAgreement_FinancialContractID" IS NOT NULL
		AND mb."ContractCurrency" = ps."PaymentSchedule_InstallmentCurrency" --filter for currency
	
	UNION ALL	
		
	SELECT 
		fc."IDSystem" AS "FinancialContract_IDSystem",
		fc."FinancialContractID" AS "FinancialContract_FinancialContractID",
		ps."PaymentSchedule_PaymentScheduleType" AS "PaymentSchedule_PaymentScheduleType",
		--pl."PaymentListEntry_PaymentAmount" AS "ppAmount",
		CASE WHEN ps."PaymentSchedule_PaymentScheduleType" = 'Repayment' AND pl."PaymentListEntry_PaymentAmount" IS NOT NULL THEN
		    CASE
    		    WHEN bpcl."ContractDataOwner" = TRUE /*Active loan*/ THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" (pl."PaymentListEntry_PaymentAmount") 
    		    WHEN bpca."ContractDataOwner" = TRUE /*Passive loan*/ THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" (pl."PaymentListEntry_PaymentAmount") 
		        ELSE NULL
		    END
		WHEN ps."PaymentSchedule_PaymentScheduleType" = 'Disbursement' AND pl."PaymentListEntry_PaymentAmount" IS NOT NULL THEN
		    CASE 
    		    WHEN bpcl."ContractDataOwner" = TRUE /*Active loan*/ THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" (pl."PaymentListEntry_PaymentAmount") 
    		    WHEN bpca."ContractDataOwner" = TRUE /*Passive loan*/ THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" (pl."PaymentListEntry_PaymentAmount") 
    		    ELSE NULL
		    END
		ELSE NULL
		END AS "ppAmount",
		pl."PaymentListEntry_PaymentDate" AS "cycleStartDateOrig",
		'Year' AS "cycleTimeUnit",
		999 AS "cycleLength",
		NULL AS "cycleEndDate",
		fc."AmortizationType" AS "FinancialContract_AmortizationType"
	FROM "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fc

	INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_PaymentSchedule"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS ps
		ON ps."PaymentSchedule_FinancialContractID" = fc."FinancialContractID"
		AND ps."PaymentSchedule_IDSystem" = fc."IDSystem"
		
	LEFT JOIN "com.adweko.adapter.osx.inputdata.risk::BV_PaymentList"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pl
		ON ps."PaymentSchedule_FinancialContractID" = pl."PaymentListEntry_FinancialContractID"
		AND ps."PaymentSchedule_IDSystem" = pl."PaymentListEntry_IDSystem"
		AND ps."PaymentSchedule_SequenceNumber" = pl."PaymentListEntry_SequenceNumber"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMonetaryBalance"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP, 'Loan' )  mb ON --filter for currency
            mb."FinancialContractID"									= fc."FinancialContractID" 
        AND mb."IDSystem"												= fc."IDSystem"
        AND mb."MonetaryBalanceCategory"								= 'LoanBalance'			
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpca ON 
            bpca."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
        AND bpca."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
		AND bpca."Role"											        = 'Borrower'     
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcl ON 
            bpcl."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
        AND bpcl."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
		AND bpcl."Role"											        = 'Lender'   
		
	WHERE
		fc."FinancialContractCategory" = 'Facility'
		AND (
			ps."PaymentSchedule_PaymentScheduleType" = 'Repayment'
			OR (
				ps."PaymentSchedule_PaymentScheduleType" = 'Disbursement'
				AND ps."PaymentSchedule_InstallmentAmount" IS NOT NULL
			)
		)
		AND ps."PaymentSchedule_PaymentScheduleCategory" = 'PaymentList'
		AND fc."_TrancheInSyndication__SyndicationAgreement_FinancialContractID" IS NOT NULL
		AND mb."ContractCurrency" = pl."PaymentListEntry_PaymentCurrency" --filter for currency
		
	UNION ALL

	SELECT 
		fc."IDSystem" AS "FinancialContract_IDSystem",
		fc."FinancialContractID" AS "FinancialContract_FinancialContractID",
		'InterestTable' AS "PaymentSchedule_PaymentScheduleType",
		--inte."AnnuityAmount" AS "ppAmount",

         CASE
    		    WHEN bpcl."ContractDataOwner" = TRUE /*Active loan*/ THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" (inte."AnnuityAmount") 
    		    WHEN bpca."ContractDataOwner" = TRUE /*Passive loan*/ THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" (inte."AnnuityAmount") 
		    ELSE NULL
		 END AS "ppAmount",
		inte."FirstInterestPeriodStartDate" AS "cycleStartDateOrig",
		CAST(inte."InterestPeriodTimeUnit" AS NVARCHAR(12)) AS "cycleTimeUnit",
		inte."InterestPeriodLength" AS "cycleLength",
		inte."LastInterestPeriodEndDate" AS "cycleEndDate",
		fc."AmortizationType" AS "FinancialContract_AmortizationType"
	FROM "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fc

	INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS inte
		ON inte."FinancialContractID" = fc."FinancialContractID"
		AND inte."IDSystem" = fc."IDSystem"
		AND inte."InterestType" = 'LendingInterest'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMonetaryBalance"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP, 'Loan' )  mb ON --filter for currency
            mb."FinancialContractID"									= fc."FinancialContractID" 
        AND mb."IDSystem"												= fc."IDSystem"
        AND mb."MonetaryBalanceCategory"								= 'LoanBalance'		
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpca ON 
            bpca."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
        AND bpca."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
		AND bpca."Role"											        = 'Borrower'     
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS bpcl ON 
            bpcl."ASSOC_FinancialContract_FinancialContractID"			= fc."FinancialContractID" 
        AND bpcl."ASSOC_FinancialContract_IDSystem"						= fc."IDSystem"
		AND bpcl."Role"											        = 'Lender'   
		
	WHERE 
		fc."FinancialContractCategory" = 'Facility'
		AND fc."AmortizationType" = 'FixedAmortizationSchedule'
		AND fc."_TrancheInSyndication__SyndicationAgreement_FinancialContractID" IS NOT NULL
		AND mb."ContractCurrency" = COALESCE(inte."AnnuityAmountCurrency",inte."InterestCurrency") --filter for currency
		AND NOT EXISTS (
    				SELECT 'DUMMY'
    					FROM "com.adweko.adapter.osx.inputdata.risk::BV_PaymentSchedule"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) as ps
    					WHERE	
	    						ps."PaymentSchedule_FinancialContractID"		= fc."FinancialContractID" 
	    					AND ps."PaymentSchedule_IDSystem"					= fc."IDSystem"
	    					AND ps."PaymentSchedule_PaymentScheduleCategory" = 'PaymentList' )    
	    ;
END;