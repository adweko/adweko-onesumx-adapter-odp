FUNCTION "com.adweko.adapter.osx.inputdata.guarantee.SyndGuarantee::SyndGuarantee_get_Optionality" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP
		)
RETURNS TABLE (
		"FinancialContract_IDSystem" NVARCHAR(40),
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"optionHolder" NVARCHAR(1),
		"optionType" NVARCHAR(1),
		"exerciseBeginDate" DATE,
		"exerciseEndDate" DATE,
		"optionQuantity" DECIMAL(34, 6),
		"optionStrikeCall" DECIMAL(34, 11),
		"optionStrikePut" DECIMAL(34, 11),
		"CancellationOption_SequenceNumber" INT,
		"CancellationOption_BeginOfExercisePeriod" DATE,
		"CancellationOption_EndOfExercisePeriod" DATE,
		"xDayNotice" INT
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/**************************************************
        Select Payment Data of a Loan
    *************************************************/
	RETURN

	SELECT DISTINCT "FinancialContract_IDSystem",
		"FinancialContract_FinancialContractID",
		CASE 
			WHEN (
					"Interest_VariableRateMin" IS NULL
					AND "Interest_VariableRateMax" IS NOT NULL
					)
				THEN '1' --BUY
			WHEN (
					"Interest_VariableRateMin" IS NOT NULL
					AND "Interest_VariableRateMax" IS NULL
					)
				OR (
					"Interest_VariableRateMin" IS NOT NULL
					AND "Interest_VariableRateMax" IS NOT NULL
					)
				OR "EarlyRepaymentRule_Loan_FinancialContractID" IS NOT NULL
				THEN '2' --SELL
			ELSE NULL
			END AS "optionHolder",
		CASE 
			WHEN (
					"Interest_VariableRateMin" IS NOT NULL
					AND "Interest_VariableRateMax" IS NULL
					)
				OR "EarlyRepaymentRule_Loan_FinancialContractID" IS NOT NULL
				THEN '1' --CALL
			WHEN (
					"Interest_VariableRateMin" IS NULL
					AND "Interest_VariableRateMax" IS NOT NULL
					)
				THEN '2' --PUT
			WHEN (
					"Interest_VariableRateMin" IS NOT NULL
					AND "Interest_VariableRateMax" IS NOT NULL
					)
				THEN '3' --CALL AND PUT
			ELSE NULL
			END AS "optionType",
		"EarlyRepaymentRule_EarlyRepaymentStartDate" AS "exerciseBeginDate",
		"EarlyRepaymentRule_EarlyRepaymentEndDate" AS "exerciseEndDate",
		CASE 
			WHEN "EarlyRepaymentRule_EarlyRepaymentStartDate" IS NOT NULL
				THEN 1
			ELSE NULL
			END AS "optionQuantity",
		"EarlyRepaymentRule_MaximumPrepaymentRate" AS "optionStrikeCall",
		"EarlyRepaymentRule_MinimumPrepaymentRate" AS "optionStrikePut",
		NULL AS "CancellationOption_SequenceNumber",
		NULL AS "CancellationOption_BeginOfExercisePeriod",
		NULL AS "CancellationOption_EndOfExercisePeriod",
		NULL AS "xDayNotice"
	FROM "com.adweko.adapter.osx.inputdata.guarantee::Guarantee_Optionality" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP
			)
	WHERE "FinancialContract_TrancheInSyndication_SyndicationAgreement_FinancialContractID" IS NOT NULL
	
	UNION ALL
	
	SELECT DISTINCT "FinancialContract_IDSystem",
		"FinancialContract_FinancialContractID",
		NULL AS "optionHolder",
		NULL AS "optionType",
		NULL AS "exerciseBeginDate",
		NULL AS "exerciseEndDate",
		NULL AS "optionQuantity",
		NULL AS "optionStrikeCall",
		NULL AS "optionStrikePut",
		"CancellationOption_SequenceNumber",
		"CancellationOption_BeginOfExercisePeriod",
		"CancellationOption_EndOfExercisePeriod",
		CASE 
			WHEN "CancellationOption_NoticePeriodTimeUnit" = 'Days'
				AND "CancellationOption_NoticePeriodLength" <= 14
				THEN "CancellationOption_NoticePeriodLength"
			WHEN "CancellationOption_NoticePeriodTimeUnit" = 'Week'
				AND "CancellationOption_NoticePeriodLength" * 7 <= 14
				THEN "CancellationOption_NoticePeriodLength" * 7
			ELSE NULL
			END AS "xDayNotice"
	FROM "com.adweko.adapter.osx.inputdata.guarantee::Guarantee_Optionality" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP
			)
	WHERE "CancellationOption_CancellationOptionType" = 'OrdinaryTerminationRight'
		AND "CancellationOption_CancellationOptionCategory" = 'TerminationOption'
		AND "CancellationOption_NoticePeriodTimeUnit" IN (
			'Days',
			'Week'
			)--if period is 'Month' or 'Year' than it is not relevant because we only need values <= 14 days for OSX
		AND "FinancialContract_TrancheInSyndication_SyndicationAgreement_FinancialContractID" IS NOT NULL
		;
END;
