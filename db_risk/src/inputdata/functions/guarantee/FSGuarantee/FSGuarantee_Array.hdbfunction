FUNCTION "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_Array" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"refid" NVARCHAR(32),
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),
		"pricingMarketObject" NVARCHAR(40),
		"repricingType" NVARCHAR(1),
		"rateAdd" DECIMAL(15, 11),
		"rateMultiplier" DECIMAL(15, 11),
		"rateTerm" NVARCHAR(10),
		"fixingDays" NVARCHAR(10),
		"ppAmount" DECIMAL(34, 6),
		"cap" DECIMAL(15, 11),
		"floor" DECIMAL(15, 11),
		"gracePeriodStartDate" DATE,
		"gracePeriodEndDate" DATE,
		"graceType" NVARCHAR(1),
/*  ---------------------------------
	Attributes for Credit Risk:
*/ ---------------------------------
		"anchor" DATE,
		"amount" DECIMAL(34, 11),
		"ratingScale" NVARCHAR(75),
		"ratingClass" NVARCHAR(10),
		"cashFlowKind" NVARCHAR(1),
		"dataGroup" NVARCHAR(128)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	SELECT CASE 
			WHEN "arraySpecKind" = 'CRTNG'
				THEN "com.adweko.adapter.osx.inputdata.common::get_array_id" (
						"com.adweko.adapter.osx.inputdata.common::get_id" (
							"contractID",
							'0001',
							COALESCE(dtg."dataGroup", dtgdef."defaultName")
							),
						"arraySpecKind",
						"rating_id"
						)
			WHEN "arraySpecKind" = 'AMP'
				THEN "com.adweko.adapter.osx.inputdata.common::get_array_id" (
						"com.adweko.adapter.osx.inputdata.common::get_id" (
							"contractID",
							'0001',
							COALESCE(dtg."dataGroup", dtgdef."defaultName")
							),
						"arraySpecKind",
						"cycleDate"
						)
			WHEN "arraySpecKind" IN (
					'LOSS',
					'PDEF'
					)
				THEN "com.adweko.adapter.osx.inputdata.common::get_array_id" (
						"com.adweko.adapter.osx.inputdata.common::get_id" (
							"contractID",
							'0001',
							COALESCE(dtg."dataGroup", dtgdef."defaultName")
							),
						"arraySpecKind",
						"anchor"
						)
			WHEN "arraySpecKind"='CFS'
				THEN "com.adweko.adapter.osx.inputdata.common::get_array_id" (
						"com.adweko.adapter.osx.inputdata.common::get_id" (
							"contractID",
							'000'||"cashFlowKind",
							COALESCE(dtg."dataGroup", dtgdef."defaultName")
							),
						"arraySpecKind",
						"anchor"
						)
			ELSE "com.adweko.adapter.osx.inputdata.common::get_array_id" (
					"com.adweko.adapter.osx.inputdata.common::get_id" (
						"contractID",
						'0001',
						COALESCE(dtg."dataGroup", dtgdef."defaultName")
						),
					"arraySpecKind",
					"cycleDate"
					)
			END AS "id",
		"com.adweko.adapter.osx.inputdata.common::get_id" (
			"contractID",
			'0001',
			COALESCE(dtg."dataGroup", dtgdef."defaultName")
			) AS "refid", 
		"arraySpecKind",
		"cycleDate",
		"cyclePeriod",
		"pricingMarketObject",
		"repricingType",
		"rateAdd",
		"rateMultiplier",
		"rateTerm",
		"fixingDays",
		CASE 
			WHEN "IsActive" = 'TRUE'  -- aktiv
				THEN 
					CASE 
						WHEN "arraySpecKind" = 'AMP'
							THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue"("ppAmount")
						WHEN "arraySpecKind" = 'PP'
							THEN "ppAmount"
						ELSE "ppAmount"
					END
			WHEN "IsActive" = 'FALSE' -- passiv
				THEN 
					CASE 
						WHEN "arraySpecKind" = 'AMP'
							THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue"("ppAmount")
						WHEN "arraySpecKind" = 'PP'
							THEN "ppAmount"
						ELSE "ppAmount"
					END
				ELSE "ppAmount"
		END AS "ppAmount",
		"cap",
		"floor",
		"gracePeriodStartDate",
		"gracePeriodEndDate",
		"graceType",
		"anchor",
		CASE 
			WHEN "IsActive" = 'TRUE'  -- aktiv
				THEN 
					CASE 
						WHEN "arraySpecKind" = 'CFS'-- repayment
							THEN TO_DECIMAL("com.adweko.adapter.osx.inputdata.common::get_absolutedValue"("amount"), 34, 11)
						ELSE TO_DECIMAL("amount", 34, 11)
					END
			WHEN "IsActive" = 'FALSE' -- passiv
				THEN 
					CASE 
						WHEN "arraySpecKind" = 'CFS'-- repayment
							THEN TO_DECIMAL("com.adweko.adapter.osx.inputdata.common::get_negatedValue"("amount"), 34, 11)
						ELSE TO_DECIMAL("amount", 34, 11)
					END
				ELSE TO_DECIMAL("amount", 34, 11)
		END AS "amount",
		"ratingScale",
		"ratingClass",
		"cashFlowKind",
		-- datagroup
		COALESCE(dtg."dataGroup", dtgdef."defaultName") AS "dataGroup"
	FROM (
		SELECT 
			"arraySpecKind",
			"cycleDate",
			"cyclePeriod",
			"pricingMarketObject",
			"repricingType",
			CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" ("rateAdd", 'rateAdd', kv."Value") AS DECIMAL (15,11)) AS "rateAdd",
			"rateMultiplier",
			"rateTerm",
			"fixingDays",
			NULL AS "ppAmount",
			CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" ("cap", 'cap', kv."Value") AS DECIMAL(15,11)) AS "cap",
			CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" ("floor", 'floor', kv."Value") AS DECIMAL(15,11)) AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "anchor",
			NULL AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			inte."FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			inte."FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive" 
		FROM "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Interest" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP
				) AS inte
		INNER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Root" (
				:I_BUSINESS_DATE, 
				:I_SYSTEM_TIMESTAMP
				) AS root 
			ON (
				root."FinancialContract_IDSystem"				= inte."FinancialContract_IDSystem"
				AND root."FinancialContract_FinancialContractID" = inte."FinancialContract_FinancialContractID"
				AND ("repricingType"<>'4' OR "repricingType" IS NULL)
			)
			OR
			(
				root."FinancialContract_IDSystem"					= inte."FinancialContract_IDSystem"
				AND root."FinancialContract_FinancialContractID"	= inte."FinancialContract_FinancialContractID"
				AND (inte."repricingType"='4' AND inte."Interest_FirstInterestPeriodStartDate"  > :I_BUSINESS_DATE)
			) -- only Fixed Interest Rates in future periods
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
			ON  kv."KeyID" = 'SAPPercentageStandard'
		
		UNION
		
		SELECT 
			'PP' AS "arraySpecKind",
			"cycleStartDateOrig" AS "cycleDate",
			"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
				"cycleLength",
				mcp."cyclePeriod"
			) AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			"ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "anchor",
			NULL AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			paym."FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			paym."FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive" 
		FROM "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Payment" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS paym
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = paym."cycleTimeUnit"

		INNER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Root" (
				:I_BUSINESS_DATE, 
				:I_SYSTEM_TIMESTAMP
				) AS root 
			ON	root."FinancialContract_IDSystem"				= paym."FinancialContract_IDSystem"
			AND root."FinancialContract_FinancialContractID"	= paym."FinancialContract_FinancialContractID"
			AND ("cycleEndDate" > :I_BUSINESS_DATE OR ("cycleStartDateOrig" >= :I_BUSINESS_DATE AND "cycleEndDate" IS NULL AND "cycleLength" = 999 AND "cycleTimeUnit" = 'Year') )
		WHERE	paym."FinancialContract_AmortizationType" != 'FixedAmortizationSchedule'
		
		UNION
		
		SELECT 
			'AMP' AS "arraySpecKind",
			"cycleStartDateOrig" AS "cycleDate",
			"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
				"cycleLength",
				mcp."cyclePeriod"
			) AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			"ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "anchor",
			NULL AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			paym."FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			paym."FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive"
		FROM "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Payment" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP
				) AS paym
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = paym."cycleTimeUnit"
		INNER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Root" (
				:I_BUSINESS_DATE, 
				:I_SYSTEM_TIMESTAMP
				) AS root
			ON	root."FinancialContract_IDSystem"				= paym."FinancialContract_IDSystem"
			AND root."FinancialContract_FinancialContractID"	= paym."FinancialContract_FinancialContractID"
			AND ("cycleEndDate" > :I_BUSINESS_DATE OR ("cycleStartDateOrig" >= :I_BUSINESS_DATE AND "cycleEndDate" IS NULL AND "cycleLength" = 999 AND "cycleTimeUnit" = 'Year') )
		WHERE 	paym."FinancialContract_AmortizationType" = 'FixedAmortizationSchedule'

		UNION

		SELECT 'GP' AS "arraySpecKind",
			NULL AS "cycleDate",
			NULL AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			"gracePeriodStartDate",
			"gracePeriodEndDate",
			"graceType",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "anchor",
			NULL AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			grace."FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			grace."FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive"
		FROM "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_GracePeriod" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				:I_DATAGROUP_STRING
				) AS grace
		INNER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Root" (
				:I_BUSINESS_DATE, 
				:I_SYSTEM_TIMESTAMP
				) AS root  
			ON	root."FinancialContract_IDSystem"				= grace."FinancialContract_IDSystem"
			AND root."FinancialContract_FinancialContractID"	= grace."FinancialContract_FinancialContractID"
		
		UNION
		
		SELECT 'PDEF' AS "arraySpecKind",
			NULL AS "cycleDate",
			NULL AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			pd."probabilityOfDefaultDate" AS "anchor",
			CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (pd."probabilityOfDefaultValue", 'probabilityOfDefaultValue', kv."Value") AS DECIMAL(34,11)) AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			pd."CreditRiskProbabilityOfDefault_FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			pd."CreditRiskProbabilityOfDefault_FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive"
		FROM "com.adweko.adapter.osx.inputdata.common::get_pd" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				'FSGuarantee'
				) AS pd
		INNER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Root" (
				:I_BUSINESS_DATE, 
				:I_SYSTEM_TIMESTAMP
				) AS root
			ON	root."FinancialContract_FinancialContractID"	= pd."CreditRiskProbabilityOfDefault_FinancialContract_FinancialContractID"
			AND root."FinancialContract_IDSystem"				= pd."CreditRiskProbabilityOfDefault_FinancialContract_IDSystem"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
		ON  kv."KeyID" = 'SAPPercentageStandard'
		
		UNION
		
		SELECT 'LOSS' AS "arraySpecKind",
			NULL AS "cycleDate",
			NULL AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			lgd."lossGivenDefaultDate" AS "anchor",
			CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (lgd."lossGivenDefaultValue", 'lossGivenDefaultValue', kv."Value") AS DECIMAL(34,11)) AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			lgd."CreditRiskLossGivenDefault_FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			lgd."CreditRiskLossGivenDefault_FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive"
		FROM "com.adweko.adapter.osx.inputdata.common::get_lgd" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				'FSGuarantee'
				) AS lgd
		INNER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Root" (
				:I_BUSINESS_DATE, 
				:I_SYSTEM_TIMESTAMP
				) AS root
			ON	root."FinancialContract_FinancialContractID"	= lgd."CreditRiskLossGivenDefault_FinancialContract_FinancialContractID"
			AND root."FinancialContract_IDSystem"				= lgd."CreditRiskLossGivenDefault_FinancialContract_IDSystem"

		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
			ON  kv."KeyID" = 'SAPPercentageStandard'
		
		UNION
		
		SELECT --CFS
			"arraySpecKind",
			NULL AS "cycleDate",
			NULL AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			cf."anchor" AS "anchor",
			TO_DECIMAL(cf."amount", 34, 11) AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			cf."cashFlowKind" AS "cashFlowKind",	
			cf."IDSystem" AS "FinancialContract_IDSystem",
			cf."FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive"
		FROM "com.adweko.adapter.osx.inputdata.loans::get_cashflow"(
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP
				) AS cf
		INNER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Root" (
				:I_BUSINESS_DATE, 
				:I_SYSTEM_TIMESTAMP
				) AS root
			ON	root."FinancialContract_FinancialContractID"	= cf."FinancialContractID"
			AND	root."FinancialContract_IDSystem"				= cf."IDSystem"
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMonetaryBalance"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP, 'SyndicatedLoan' )  mb ON --filter for currency
            	mb."FinancialContractID"						= cf."FinancialContractID" 
    		AND mb."IDSystem"									= cf."IDSystem"
        	AND mb."MonetaryBalanceCategory"					= 'LoanBalance'		
        WHERE   mb."ContractCurrency"	 						= cf."currency"		
		
		UNION
		
		SELECT 'CRTNG' AS "arraySpecKind",
			NULL AS "cycleDate",
			NULL AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "anchor",
			NULL AS "amount",
			rat."ratingScale",
			rat."ratingClass",
			NULL AS "cashFlowKind",
			rat."Rating_FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			rat."Rating_FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			"rating_id",
			root."IsActive"
		FROM "com.adweko.adapter.osx.inputdata.common::get_RatingFinancialContract"(
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				'FSGuarantee'
				) AS rat
		INNER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Root" (
				:I_BUSINESS_DATE, 
				:I_SYSTEM_TIMESTAMP
				) AS root
			ON	root."FinancialContract_FinancialContractID"	= rat."Rating_FinancialContract_FinancialContractID"
			AND root."FinancialContract_IDSystem"				= rat."Rating_FinancialContract_IDSystem"
		) AS innerselect
-- Datagroup
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_DataGroup" (
    			:I_BUSINESS_DATE,
    			:I_SYSTEM_TIMESTAMP
    			) AS dtg ON dtg."FinancialContract_IDSystem" = innerselect."FinancialContract_IDSystem"
    		AND dtg."FinancialContract_FinancialContractID" = innerselect."FinancialContract_FinancialContractID"
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_datagroupDefaultValue" (:I_BUSINESS_DATE) AS dtgdef
		ON 1 = 1 ;
END;
