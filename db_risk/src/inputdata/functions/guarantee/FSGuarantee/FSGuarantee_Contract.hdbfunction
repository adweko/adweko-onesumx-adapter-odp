FUNCTION "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_Contract" (
		I_BUSINESS_DATE DATE, 
		I_SYSTEM_TIMESTAMP TIMESTAMP
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"contractID" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"valueDate" DATE,
		"maturityDate" DATE,
		"amortizationDate" DATE,
		"counterparty" NVARCHAR(128),
		"legalEntity" NVARCHAR(128),
		"currentPrincipal" DECIMAL(34, 6),
		"linkID" NVARCHAR(256),
		"linkType" NVARCHAR(3),
		"originalTotalPrincipal" DECIMAL(34, 6),
		"referencePrincipal" DECIMAL(34, 6),
		"amortizationType" INT,
		"currency" NVARCHAR(3),
		"currentNominalInterestRate" DECIMAL(15, 11),
		"nominalValueOfGuarantee" DECIMAL(34, 6),
		"optionHolder" NVARCHAR(1),
		"optionType" NVARCHAR(1),
		"referenceEntity" NVARCHAR(128),
		"exerciseBeginDate" DATE,
		"exerciseEndDate" DATE,
		"contractDayCountMethod" NVARCHAR(2),
		"interestPaymentType" NVARCHAR(1),
		"businessDayConvention" NVARCHAR(2),
		"calendar" NVARCHAR(75),
		"eomConvention" NVARCHAR(1),
    	"capitalizationEndDate" DATE,
		"xDayNotice" INT,
		"accruedInterest" DECIMAL(38, 11),
		"strippedPaymentType" NVARCHAR(1),
		"dataGroup" NVARCHAR(128),
		"ProductCatalogItemCode" NVARCHAR(128),
		"ProductCatalogID" NVARCHAR(128),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
/*  ---------------------------------
	Attributes for Credit Risk:
*/ ---------------------------------
		"pastDueAmount" DECIMAL(34, 6),
		"nonPerformingDate" DATE,
		"clearingHouse" NVARCHAR(128),
		"creditLineKey" NVARCHAR(256),
		"productType" NVARCHAR(75),
		"countryTransfer" NVARCHAR(2),
		"creditRiskProvision" DECIMAL(34, 6),
		"netRecoveryRate" DECIMAL (15,11),
/*  ---------------------------------
	User Defined Attributes :
*/ ---------------------------------
		"frtbCsrClass" NVARCHAR(75),
		"frtbEqClass" NVARCHAR(75),
		"frtbDrcClass" NVARCHAR(75),
		"contractFSType" NVARCHAR(256),
		"contractFSSubType" NVARCHAR(256),
		"productFSClass" NVARCHAR(128),
		"annuityCalculationType" NVARCHAR(128),
		"lmEntityName" NVARCHAR(128),
		"lmEntityDetail" NVARCHAR(128),
		"instrumentType" NVARCHAR(128),
		"financialInstrumentType" INT, 
		"isUnderLiquidityManagementControl" INT,
		"ebaLcrContractEligibility" NVARCHAR(255),
        "portfolioType"	NVARCHAR(255),
		"cadCrConversionFactor" NVARCHAR(75),
		"crRiskWeight" DECIMAL(15,11),
		"commitmentType" NVARCHAR(75),
		"lookbackPeriod" NVARCHAR(10),
		"rateCappingFlooring" NVARCHAR(40),
		"rateAveragingCap" DECIMAL(15, 11),
		"rateAveragingFloor" DECIMAL(15, 11),
		"writeOffAmount" DECIMAL(34, 6),
		"bookValue" DECIMAL(34, 6),
		"exposureClass" NVARCHAR(255)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN

	SELECT DISTINCT
		"com.adweko.adapter.osx.inputdata.common::get_id" (
			root."contractID",
			'0001',
			COALESCE(dtg."dataGroup", dtgdef."defaultName")
		) AS "id",
		root."contractID" AS "contractID",
		root."FinancialContract_FinancialContractID" AS "sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		'0001' AS "node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		root."dealDate",
		root."valueDate",
		NULL AS "maturityDate",
		CASE
			WHEN root."FinancialContract_AmortizationType" = 'FixedAmortizationSchedule'
				AND inte2."AnnuityAmount" IS NULL THEN
					root."maturityDate"
			ELSE NULL
		END AS "amortizationDate",
		root."counterparty",
		root."legalEntity",
		pos."currentPrincipal",
		NULL AS "linkID",
		NULL AS "linkType",
		
		root."originalTotalPrincipal",

		CASE 
		    WHEN inte."Interest_InterestSubPeriodSpecificationCategory" = 'FlatInterestSpecification'
			AND root."FinancialContract_AmortizationType" = 'French' THEN
			    CASE
        		    WHEN root."IsActive" = 'TRUE' THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" ("originalTotalPrincipal") 
        		    WHEN root."IsActive" = 'FALSE' THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" ("originalTotalPrincipal") 
    		        ELSE NULL
    		    END
            ELSE
		        NULL
		END AS "referencePrincipal",

        CASE
			WHEN inte."Interest_InterestSubPeriodSpecificationCategory" = 'FlatInterestSpecification'
					AND root."FinancialContract_AmortizationType" = 'French'
				THEN 1
		END AS "amortizationType",
		COALESCE(pos."ContractCurrency", root."FinancialContract_Currency") AS "currency",
		COALESCE(CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (inte."currentNominalInterestRate", 'currentNominalInterestRate', kv."Value") AS DECIMAL(15,11)), 0) AS "currentNominalInterestRate",
		pos."currentPrincipal" AS "nominalValueOfGuarantee",
		'2' AS "optionHolder",
		NULL AS "optionType",
		root."referenceEntity",
		NULL AS "exerciseBeginDate",
		NULL AS "exerciseEndDate",
		inte."contractDayCountMethod",
		inte."interestPaymentType",
		inte."businessDayConvention",
		inte."calendar",
		inte."eomConvention",
		inte."capitalizationEndDate",
		CASE 
			WHEN root."maturityDate" IS NULL
				THEN cancellation."xDayNotice"
			ELSE NULL
		END AS "xDayNotice",
		pos."accruedInterest",
		NULL AS "strippedPaymentType",  
		--Datagroup
		COALESCE(dtg."dataGroup", dtgdef."defaultName") AS "dataGroup",
		root."ProductCatalogItemCode",
		root."ProductCatalogID",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",

        CASE
		    WHEN root."IsActive" = 'TRUE' THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" ("pastDueAmount") 
		    WHEN root."IsActive" = 'FALSE' THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" ("pastDueAmount") 
	        ELSE NULL
	    END AS "pastDueAmount",

		root."nonPerformingDate",
		NULL AS "clearingHouse",
		root."creditLineKey",
		expo."CreditRiskExposure_ExposureClass" AS "productType",
		CASE 
			WHEN expo."CreditRiskExposure_Currency" <> pos."ContractCurrency"
				THEN root."FinancialContract_GoverningLawCountry"
			ELSE NULL
		END AS "countryTransfer",
		adj."CreditRiskAdjustment_RiskProvisionInPositionCurrency" AS "creditRiskProvision",
		TO_DECIMAL(root."netRecoveryRate", 15, 11) AS "netRecoveryRate",
		NULL AS "frtbCsrClass",
		NULL AS "frtbEqClass",
		NULL AS "frtbDrcClass",
		MAP(root."FinancialContract_FinancialContractType", 
				'AvalFacility', 'GUARANTEE',
				'LetterOfCreditFacility', 'LETTER_OF_CREDIT') AS "contractFSType",
		NULL AS "contractFSSubType",
		'TRADE_FINANCE_INSTRUMENT' AS "productFSClass",
		CASE
			WHEN root."FinancialContract_AmortizationType" = 'FixedAmortizationSchedule' THEN
				CASE
					WHEN inte2."AnnuityAmount" IS NOT NULL THEN
						'paymentFixed'
					ELSE 'dateFixed'
				END
			ELSE NULL
		END AS "annuityCalculationType",
		'GuaranteeAgreement' AS "lmEntityName",
		CASE
            WHEN optionality."optionQuantity" IS NOT NULL
                THEN 'BondOptionality' 
        END AS "lmEntityDetail",
    	NULL AS "instrumentType",
    	root."financialInstrumentType",
        1 AS "isUnderLiquidityManagementControl",
		"com.adweko.adapter.osx.inputdata.common::get_ebaLcrContractEligibility"(
			i_ProductCatalogItemCode			=> NULL,
			i_LiquidityClassificationOfAsset	=> EURegRepForContract."EuropeanRegulatoryReportingForContract_LiquidityClassificationOfAsset",
			i_CountryEU 						=> NULL,
			i_CountryNonEU						=> NULL,
			i_SecurityCategory					=> NULL,
			i_TreatedAsUCITS					=> NULL,
			i_FinancialContractCategory 		=> root."FinancialContract_FinancialContractCategory",
			i_DenominationCurrency				=> NULL,
			i_CurrencyInCountry 				=> NULL ) AS "ebaLcrContractEligibility",
		root."portfolioType",
		CASE
			WHEN expo."CreditRiskExposure_CreditRiskCalculationMethod" = 'IntRatingBasedAppr'
				THEN expo."CreditRiskExposure_CreditConversionFactorClass"
			ELSE NULL
		END AS "cadCrConversionFactor",
		CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard"(expo."CreditRiskExposure_RiskWeight", 'crRiskWeight', kv."Value") AS DECIMAL(15,11)) AS "crRiskWeight",
		root."commitmentType",
		inte."lookbackPeriod",
		inte."rateCappingFlooring",
		inte."rateAveragingCap",
		inte."rateAveragingFloor",
		MAP(writeOffKey."Value",
			'WriteDownAmountInPositionCurrency', writeOff."WriteDownAmountInPositionCurrency",
			'WriteDownAmount', writeOff."WriteDownAmount"
		) AS "writeOffAmount",
		bok."BookValue_BookValue" AS "bookValue",
		expo."exposureClass"
	FROM "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Root" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS root

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Position" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pos
		ON pos."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND pos."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Optionality" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS optionality
		ON optionality."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND optionality."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND optionality."CancellationOption_SequenceNumber" IS NULL

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Optionality" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cancellation
		ON cancellation."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND cancellation."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND cancellation."xDayNotice" IS NOT NULL
		AND cancellation."CancellationOption_BeginOfExercisePeriod" <= :I_BUSINESS_DATE
		AND cancellation."CancellationOption_EndOfExercisePeriod" > :I_BUSINESS_DATE

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Interest" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS inte
		ON inte."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND inte."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND inte."Interest_FirstInterestPeriodStartDate" <= :I_BUSINESS_DATE
		AND inte."Interest_LastInterestPeriodEndDate" > :I_BUSINESS_DATE
		AND inte."arraySpecKind" = 'IP'

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_Interest" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS inte1
		ON inte1."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND inte1."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND inte1."Interest_SequenceNumber" = 1
		AND inte1."arraySpecKind" = 'RPS'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS inte2
		ON inte2."FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND inte2."IDSystem" = root."FinancialContract_IDSystem"
		AND inte2."FirstInterestPeriodStartDate" <= :I_BUSINESS_DATE
		AND inte2."LastInterestPeriodEndDate" > :I_BUSINESS_DATE
		AND inte2."InterestType" = 'LendingInterest'

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
		ON bok."BookValue_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND bok."BookValue_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND bok."BookValue_BookValueCurrency" = COALESCE(pos."ContractCurrency", root."FinancialContract_Currency")

-- Datagroup
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.guarantee.FSGuarantee::FSGuarantee_get_DataGroup" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS dtg
		ON dtg."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
    	AND dtg."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"

    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_datagroupDefaultValue" (:I_BUSINESS_DATE) AS dtgdef
		ON 1 = 1

--	CreditRisk
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposureAtDefault" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cread
		ON cread."CreditRiskExposureAtDefault_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND cread."CreditRiskExposureAtDefault_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND cread."CreditRiskExposureAtDefault_EADCurrency" = pos."ContractCurrency"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP,'Loan') AS expo
		ON expo."CreditRiskExposure_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND expo."CreditRiskExposure_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskAdjustmentPrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Provision', 'Loan') AS adj
		ON adj."CreditRiskAdjustment_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND adj."CreditRiskAdjustment_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND adj."CreditRiskAdjustment_PositionCurrency" = pos."ContractCurrency"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
		ON  kv."KeyID" = 'SAPPercentageStandard'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_EuropeanRegulatoryReportingForContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS EURegRepForContract
		ON EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialContract_IDSystem"	= root."FinancialContract_IDSystem"

--  writeOffAmount
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS writeOffKey
		ON writeOffKey."KeyID" = 'WriteDownPositionCurrenyOrAmount'

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityWriteOffFC"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Loan') AS writeOff
		ON writeOff."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND writeOff."FinancialContract_IDSystem" = root."FinancialContract_IDSystem";
END;
