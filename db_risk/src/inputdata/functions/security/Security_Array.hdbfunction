FUNCTION "com.adweko.adapter.osx.inputdata.security::Security_Array" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"refid" NVARCHAR(32),
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),
		"pricingMarketObject" NVARCHAR(40),
		"repricingType" NVARCHAR(1),
		"rateAdd" DECIMAL(15, 11),
		"rateTerm" NVARCHAR(10),
		"rateMultiplier" DECIMAL(15, 11),
		"fixingDays" NVARCHAR(10),
		"ppAmount" DECIMAL(34, 6),
		"cap" DECIMAL(15, 11),
		"floor" DECIMAL(15, 11),
		"indexBeta" DECIMAL(15, 11), 
		"cashFlowKind" NVARCHAR(1),
		"capitalized" INT,
		"StrikePriceCall" DECIMAL(34, 6),
		"StrikePricePut" DECIMAL(34, 6),
		"FirstInterestPeriodStartDate" DATE,
		"FirstInterestPeriodEndDate" DATE,
		"FirstInterestDueDate" DATE,
		"LastInterestPeriodEndDate" DATE,
		"DayOfMonthOfInterestPayment" INTEGER,
		"DayOfMonthOfInterestPeriodEnd" INTEGER,
		"InterestScheduleType" NVARCHAR(100),
/*  ---------------------------------
	Attributes for Credit Risk:
*/ ---------------------------------
		"anchor" DATE,
		"amount" DECIMAL(34, 11),
		"ratingScale" NVARCHAR(75),
		"ratingClass" NVARCHAR(10),
		"dataGroup" NVARCHAR(128)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	SELECT 
	MAP("arraySpecKind",
		'CRTNG',"com.adweko.adapter.osx.inputdata.common::get_array_id" (
					"com.adweko.adapter.osx.inputdata.common::get_id" (root."contractID",'0001',root."dataGroup"),
					"arraySpecKind",
					"rating_id"),
		'LOSS',"com.adweko.adapter.osx.inputdata.common::get_array_id" (
					"com.adweko.adapter.osx.inputdata.common::get_id" (root."contractID",'0001',root."dataGroup"),
					"arraySpecKind",
					"anchor"),
		'PDEF',"com.adweko.adapter.osx.inputdata.common::get_array_id" (
					"com.adweko.adapter.osx.inputdata.common::get_id" (root."contractID",'0001',root."dataGroup"),
					"arraySpecKind",
					"anchor"),
		'CFS', "com.adweko.adapter.osx.inputdata.common::get_array_id" (
					"com.adweko.adapter.osx.inputdata.common::get_id" (root."contractID",'000'||"cashFlowKind",root."dataGroup"),
					"arraySpecKind",
					"anchor"),
		'SCIX', "com.adweko.adapter.osx.inputdata.common::get_array_id" (
					"com.adweko.adapter.osx.inputdata.common::get_id" (root."contractID",'0001',root."dataGroup"),
					"arraySpecKind",''),
		"com.adweko.adapter.osx.inputdata.common::get_array_id" (
			"com.adweko.adapter.osx.inputdata.common::get_id" (root."contractID",'0001',root."dataGroup"),
			"arraySpecKind",
			"cycleDate")) AS "id",
		"com.adweko.adapter.osx.inputdata.common::get_id" (root."contractID",'0001',root."dataGroup") AS "refid",
		"arraySpecKind",
		"cycleDate",
		"cyclePeriod",
		"pricingMarketObject",
		"repricingType",
		"rateAdd",
		"rateTerm",
		"rateMultiplier",
		"fixingDays",
		"ppAmount",
		"cap",
		"floor",
		"indexBeta",
		"cashFlowKind",
		"capitalized",
		"StrikePriceCall",
		"StrikePricePut",
		"FirstInterestPeriodStartDate",
		"FirstInterestPeriodEndDate",
		"FirstInterestDueDate",
		"LastInterestPeriodEndDate",
		"DayOfMonthOfInterestPayment",
		"DayOfMonthOfInterestPeriodEnd",
		"InterestScheduleType",
		"anchor",
		"amount",
		"ratingScale",
		"ratingClass",
		root."dataGroup" AS "dataGroup"
	FROM (
		SELECT * FROM "com.adweko.adapter.osx.inputdata.security.securityArray::PDEF"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP)
		UNION ALL
		SELECT * FROM "com.adweko.adapter.osx.inputdata.security.securityArray::LOSS"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP)
		UNION ALL
		SELECT * FROM "com.adweko.adapter.osx.inputdata.security.securityArray::CRTNG"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP)
		UNION ALL
		SELECT * FROM  "com.adweko.adapter.osx.inputdata.security.securityArray::InterestAndResetPeriod"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
		UNION ALL
		SELECT * FROM "com.adweko.adapter.osx.inputdata.security.securityArray::FixedInterestRatesInFuturePeriod"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
		UNION ALL
		SELECT * FROM "com.adweko.adapter.osx.inputdata.security.securityArray::IndexFieldsForSTK"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
		UNION ALL
		SELECT * FROM "com.adweko.adapter.osx.inputdata.security.securityArray::POOLF"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP)
	) AS innerselect
-- Datagroup
	INNER JOIN "com.adweko.adapter.osx.inputdata.security::Security_get_Root" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root 
			ON  root."SecuritiesBalance_FinancialInstrument_FinancialInstrumentID" = innerselect."FinancialInstrument_FinancialInstrumentID"
		AND ((IFNULL(innerselect."repricingType",0) <> 4) OR (innerselect."repricingType"=4 and innerselect."cycleDate" > :I_BUSINESS_DATE))

	UNION ALL

	SELECT
		MAP("arraySpecKind",
			'PP', "com.adweko.adapter.osx.inputdata.common::get_array_id" (
					"com.adweko.adapter.osx.inputdata.common::get_id" (root."contractID",'0001',root."dataGroup"),
					"arraySpecKind",
					"cycleDate"),
			'OE', "com.adweko.adapter.osx.inputdata.common::get_array_id" (
					"com.adweko.adapter.osx.inputdata.common::get_id" (root."contractID",'0001',root."dataGroup"),
					"arraySpecKind",
					"cycleDate")
		) AS "id",
		"com.adweko.adapter.osx.inputdata.common::get_id" (root."contractID",'0001',root."dataGroup") AS "refid",
		"arraySpecKind",
		"cycleDate",
		"cyclePeriod",
		"pricingMarketObject",
		"repricingType",
		"rateAdd",
		"rateTerm",
		"rateMultiplier",
		"fixingDays",
		CASE
			WHEN root."nominalAmount" > 0
				THEN "ppAmount"
			WHEN root."nominalAmount" <= 0
				THEN ABS("ppAmount")*(-1)
		END AS "ppAmount",
		"cap",
		"floor",
		"indexBeta",
		"cashFlowKind",
		"capitalized",
		"StrikePriceCall",
		"StrikePricePut",
		"FirstInterestPeriodStartDate",
		"FirstInterestPeriodEndDate",
		"FirstInterestDueDate",
		"LastInterestPeriodEndDate",
		"DayOfMonthOfInterestPayment",
		NULL AS "DayOfMonthOfInterestPeriodEnd",
		"InterestScheduleType",
		"anchor",
		"amount",
		"ratingScale",
		"ratingClass",
		root."dataGroup" AS "dataGroup"
	FROM (
		SELECT * FROM "com.adweko.adapter.osx.inputdata.security.securityArray::PrincipalPayments"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
		UNION ALL
		SELECT * FROM "com.adweko.adapter.osx.inputdata.security.securityArray::OE"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
		) AS innerselect
-- Datagroup
	INNER JOIN "com.adweko.adapter.osx.inputdata.security::Security_get_Root" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
			ON  root."SecuritiesBalance_FinancialInstrument_FinancialInstrumentID" = innerselect."FinancialInstrument_FinancialInstrumentID"
		AND root."SecuritiesBalance_Account_FinancialContractID" = innerselect."FinancialContract_FinancialContractID"
		AND root."SecuritiesBalance_Account_IDSystem" = innerselect."FinancialContract_IDSystem"
		AND ((IFNULL(innerselect."repricingType",0) <> 4) OR (innerselect."repricingType"=4 and innerselect."cycleDate" > :I_BUSINESS_DATE))
	;
END;
