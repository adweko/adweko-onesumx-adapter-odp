FUNCTION "com.adweko.adapter.osx.inputdata.security::Security_get_Payment" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"FinancialInstrument_FinancialInstrumentID" NVARCHAR(128),
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"FinancialContract_IDSystem" NVARCHAR(40),
		"PaymentList_PaymentAmount" DECIMAL(34, 6),
		"PaymentSchedule_InstallmentAmount" DECIMAL(34, 6),
		"PaymentSchedule_PaymentScheduleType" NVARCHAR(40),
		"PaymentSchedule_SequenceNumber" INTEGER,
		"PaymentSchedule_InstallmentsRelativeToInterestPaymentDates" BOOLEAN,
		"ppAmount" DECIMAL(34, 6),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/**************************************************
        Select Payment Data of a Security
    *************************************************/
	RETURN
	
	SELECT DISTINCT 
		paym."FinancialInstrument_FinancialInstrumentID",
		paym."FinancialContract_FinancialContractID",
		paym."FinancialContract_IDSystem",
		paym."PaymentList_PaymentAmount",
		paym."PaymentSchedule_InstallmentAmount",
		paym."PaymentSchedule_PaymentScheduleType",
		paym."PaymentSchedule_SequenceNumber",
		paym."PaymentSchedule_InstallmentsRelativeToInterestPaymentDates",
		paym."PaymentSchedule_InstallmentAmount" * paym."scalingFactor" AS "ppAmount",
		paym."PaymentSchedule_FirstPaymentDate" AS "cycleDate",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"("PaymentSchedule_InstallmentLagPeriodLength",mcp."cyclePeriod") AS "cyclePeriod"
	FROM "com.adweko.adapter.osx.inputdata.security::BV_SecurityPayment" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) as paym
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" = paym."PaymentSchedule_InstallmentLagTimeUnit"
			 
	WHERE "PaymentSchedule_PaymentScheduleType" = 'Payout'
		AND "PaymentSchedule_PaymentScheduleCategory" = 'Installment'
		AND "PaymentSchedule_InstallmentsRelativeToInterestPaymentDates" = 'true'

	UNION ALL
	
	SELECT DISTINCT 
		paym."FinancialInstrument_FinancialInstrumentID",
		paym."FinancialContract_FinancialContractID",
		paym."FinancialContract_IDSystem",
		paym."PaymentList_PaymentAmount",
		paym."PaymentSchedule_InstallmentAmount",
		paym."PaymentSchedule_PaymentScheduleType",
		paym."PaymentSchedule_SequenceNumber",
		paym."PaymentSchedule_InstallmentsRelativeToInterestPaymentDates",
		paym."PaymentSchedule_InstallmentAmount" * paym."scalingFactor" AS "ppAmount",
		paym."PaymentSchedule_FirstPaymentDate" AS "cycleDate",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"("PaymentSchedule_InstallmentPeriodLength",mcp."cyclePeriod") AS "cyclePeriod"
	FROM "com.adweko.adapter.osx.inputdata.security::BV_SecurityPayment" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) as paym
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" = paym."PaymentSchedule_InstallmentPeriodTimeUnit"
	WHERE "PaymentSchedule_PaymentScheduleType" = 'Payout'
		AND "PaymentSchedule_PaymentScheduleCategory" = 'Installment'
		AND "PaymentSchedule_InstallmentsRelativeToInterestPaymentDates" = 'false'
		
	UNION ALL

	SELECT DISTINCT 
		paym."FinancialInstrument_FinancialInstrumentID",
		paym."FinancialContract_FinancialContractID",
		paym."FinancialContract_IDSystem",
		paym."PaymentList_PaymentAmount",
		paym."PaymentSchedule_InstallmentAmount",
		paym."PaymentSchedule_PaymentScheduleType",
		paym."PaymentSchedule_SequenceNumber",
		paym."PaymentSchedule_InstallmentsRelativeToInterestPaymentDates",
		paym."PaymentList_PaymentAmount" * paym."scalingFactor" AS "ppAmount",
		paym."PaymentList_PaymentDate" AS "cycleDate",
		'999Y' AS "cyclePeriod"
	FROM "com.adweko.adapter.osx.inputdata.security::BV_SecurityPayment" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) as paym
	WHERE "PaymentSchedule_PaymentScheduleType" = 'Payout'
		AND "PaymentSchedule_PaymentScheduleCategory" = 'PaymentList'
		AND ("PaymentSchedule_InstallmentsRelativeToInterestPaymentDates" = 'false'
			OR "PaymentSchedule_InstallmentsRelativeToInterestPaymentDates" IS NULL)
	;
END;
