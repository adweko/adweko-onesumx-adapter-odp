FUNCTION "com.adweko.adapter.osx.inputdata.security::Security_get_Optionality"
 	( I_BUSINESS_DATE DATE, I_SYSTEM_TIMESTAMP TIMESTAMP, I_DATAGROUP_STRING NVARCHAR(128) )
       RETURNS TABLE (
			"FinancialInstrument_FinancialInstrumentID" NVARCHAR(128),
			"optionType" NVARCHAR(1),
			"optionHolder" NVARCHAR(1),
			"exerciseBeginDate" DATE,
			"exerciseEndDate" DATE,
			"optionExecutionType" NVARCHAR(1),
			"optionExerciseFrequency" NVARCHAR(32),
			"optionQuantity" DECIMAL(34, 6),
			"optionStrikeCall" DECIMAL(34, 11),
			"optionStrikePut" DECIMAL(34, 11),
			"node_No" NVARCHAR(4),
			"ConversionRight_PrespecifiedConversionRatio" DECIMAL(34,6),
			"SecuritiesBalance_NominalAmount" DECIMAL(34,6),
			"SecuritiesBalance_Account_FinancialContractID" NVARCHAR (128),
			"SecuritiesBalance_Account_IDSystem" NVARCHAR (40)
       ) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN 
    /**************************************************
        Select Payment Data of a Security
    *************************************************/

RETURN
    SELECT DISTINCT
		sec."FinancialInstrument_FinancialInstrumentID",
		CASE
			WHEN fin."SecuritiesBalance_NominalAmount" > 0
				THEN MAP(redr."RedemptionRightHolder",
					'Issuer', 1,
					'Bearer', 2
				)
			WHEN fin."SecuritiesBalance_NominalAmount" <= 0
				THEN MAP(redr."RedemptionRightHolder",
					'Issuer', 1,
					'Bearer', 2
				)
		END AS "optionType",
		CASE
			WHEN fin."SecuritiesBalance_NominalAmount" > 0
				THEN MAP(redr."RedemptionRightHolder",
					'Issuer', 2,
					'Bearer', 1
				)
			WHEN fin."SecuritiesBalance_NominalAmount" <= 0
				THEN MAP(redr."RedemptionRightHolder",
					'Issuer', 1,
					'Bearer', 2
				)
		END AS "optionHolder",
		redr."RedemptionPeriodStart" AS "exerciseBeginDate",
		redr."RedemptionPeriodEnd" AS "exerciseEndDate",
		CASE
			WHEN redr."DeriveExerciseStyle" = 'American' AND redr."count" = 1
				THEN 2
			WHEN redr."DeriveExerciseStyle" = 'European' AND redr."count" = 1
				THEN 1
			WHEN redr."DeriveExerciseStyle" = 'Bermudan' AND redr."count" > 1
				THEN 3
			ELSE NULL
		END AS "optionExecutionType",
		NULL AS "optionExerciseFrequency",
		CASE
			WHEN redr."_DebtInstrument_FinancialInstrumentID" IS NOT NULL
				THEN 1
		END AS "optionQuantity",
		CASE
			WHEN fin."SecuritiesBalance_NominalAmount" IS NOT NULL AND redr."RedemptionRightHolder" = 'Issuer'
				THEN redr."RedemptionPriceInPercent"
		END AS "optionStrikeCall",
		CASE
			WHEN fin."SecuritiesBalance_NominalAmount" IS NOT NULL AND redr."RedemptionRightHolder" = 'Bearer'
				THEN redr."RedemptionPriceInPercent"
		END AS "optionStrikePut",
		'0001' AS "node_No",
		NULL AS "ConversionRight_PrespecifiedConversionRatio",
		fin."SecuritiesBalance_NominalAmount",
		fin."SecuritiesBalance_Account_FinancialContractID",
		fin."SecuritiesBalance_Account_IDSystem"
	FROM "com.adweko.adapter.osx.inputdata.risk::BV_Security"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS sec
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.security::BV_FinancialInstrumentPosition"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING ) AS fin
		ON fin."SecuritiesBalance_FinancialInstrument_FinancialInstrumentID" = sec."FinancialInstrument_FinancialInstrumentID"
    
    LEFT OUTER JOIN (
    	SELECT
    		redr."_DebtInstrument_FinancialInstrumentID",
    		redr."RedemptionRightHolder",
    		redr."RedemptionRightType",
    		AVG(redr."RedemptionPriceInPercent") AS "RedemptionPriceInPercent",
    		MIN(redr."RedemptionPeriodStart") AS "RedemptionPeriodStart",
    		MAX(redr."RedemptionPeriodEnd") AS "RedemptionPeriodEnd",
    		rel."DeriveExerciseStyle",
    		COUNT(redr."_DebtInstrument_FinancialInstrumentID") AS "count"
    	FROM "com.adweko.adapter.osx.synonyms::RedemptionRight_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS redr
    	
    	INNER JOIN "com.adweko.adapter.osx.mappings::map_redemptionRightRelevance_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS rel
			ON rel."RedemptionRightType" = redr."RedemptionRightType"
			AND rel."IsRelevant" = true
		
		GROUP BY redr."_DebtInstrument_FinancialInstrumentID", redr."RedemptionRightHolder", redr."RedemptionRightType", rel."DeriveExerciseStyle", rel."RedemptionRightType", rel."IsRelevant"
    ) AS redr
		ON redr."_DebtInstrument_FinancialInstrumentID" = sec."FinancialInstrument_FinancialInstrumentID"
	
	UNION ALL

	SELECT DISTINCT
		sec."FinancialInstrument_FinancialInstrumentID",
		CASE
			WHEN fin."SecuritiesBalance_NominalAmount" > 0
				THEN '1' -- Call
			WHEN fin."SecuritiesBalance_NominalAmount" < 0
				THEN '2' -- PUT
			ELSE NULL
		END AS "optionType",
		CASE
			WHEN sec."ConversionRight_ConversionRightHolder" = 'Issuer' AND fin."SecuritiesBalance_NominalAmount" > 0
				THEN '2' -- SELL
			WHEN sec."ConversionRight_ConversionRightHolder" = 'Issuer' AND fin."SecuritiesBalance_NominalAmount" < 0
				THEN '1' -- BUY
			WHEN sec."ConversionRight_ConversionRightHolder" = 'Bearer' AND fin."SecuritiesBalance_NominalAmount" > 0
				THEN '1' -- BUY
			WHEN sec."ConversionRight_ConversionRightHolder" = 'Bearer' AND fin."SecuritiesBalance_NominalAmount" < 0
				THEN '2' -- SELL
			ELSE NULL
		END AS "optionHolder",
		sec."ConversionRight_ConversionPeriodStartDate" AS "exerciseBeginDate",
		sec."ConversionRight_ConversionPeriodEndDate" AS "exerciseEndDate",
		NULL AS "OptionExecutionType",
		NULL AS "OptionExerciseFrequency",
		1 AS "optionQuantity",
		NULL AS "optionStrikeCall",
		NULL AS "optionStrikePut",
		'0002' AS "node_No",
		NULL AS "ConversionRight_PrespecifiedConversionRatio",
		fin."SecuritiesBalance_NominalAmount",
		fin."SecuritiesBalance_Account_FinancialContractID",
		fin."SecuritiesBalance_Account_IDSystem"
	FROM "com.adweko.adapter.osx.inputdata.risk::BV_Security"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING ) AS sec
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.security::BV_FinancialInstrumentPosition"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING ) AS fin
		ON fin."SecuritiesBalance_FinancialInstrument_FinancialInstrumentID" = sec."FinancialInstrument_FinancialInstrumentID"
		
	WHERE "ConversionRightDebtInstrument_FinancialInstrumentID" IS NOT NULL

	UNION ALL

	SELECT DISTINCT
		"FinancialInstrument_FinancialInstrumentID",
		NULL AS "optionType",
		NULL AS "optionHolder",
		NULL AS "exerciseBeginDate",
		NULL AS "exerciseEndDate",
		NULL AS "OptionExecutionType",
		NULL AS "OptionExerciseFrequency",
		NULL AS "optionQuantity",
		NULL AS "optionStrikeCall",
		NULL AS "optionStrikePut",
		'0003' AS "node_No",
		sec."ConversionRight_PrespecifiedConversionRatio",
		fin."SecuritiesBalance_NominalAmount",
		fin."SecuritiesBalance_Account_FinancialContractID",
		fin."SecuritiesBalance_Account_IDSystem"
	FROM "com.adweko.adapter.osx.inputdata.risk::BV_Security"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING ) AS sec
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.security::BV_FinancialInstrumentPosition"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING ) AS fin
		ON fin."SecuritiesBalance_FinancialInstrument_FinancialInstrumentID" = sec."FinancialInstrument_FinancialInstrumentID"
		
	WHERE "ConversionRightDebtInstrument_FinancialInstrumentID" IS NOT NULL
		;
END;
