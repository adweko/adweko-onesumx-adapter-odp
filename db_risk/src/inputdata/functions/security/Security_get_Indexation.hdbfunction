FUNCTION "com.adweko.adapter.osx.inputdata.security::Security_get_Indexation" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
		
RETURNS TABLE (
		"FinancialInstrumentID" NVARCHAR(128),
		"consumerPriceIndexDefinition" NVARCHAR(128),
		"originalConsumerPriceIndex" DECIMAL(34,6),
		"indexationRule" NVARCHAR(5),
		"referenceIndexRule" NVARCHAR(5),
		"indexationLag" NVARCHAR(8)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/***************************************
        Select Master data for Securities
    **************************************/
	RETURN

	SELECT DISTINCT
		sec."FinancialInstrument_FinancialInstrumentID" AS "FinancialInstrumentID",
		CASE 
			WHEN sec."FinancialInstrument_SecurityCategory" = 'DebtInstrument' AND sec."FinancialInstrument_DebtSecurityCategory" = 'IndexLinkedInstrument' AND ind."Index_IndexCategory" = 'PriceIndex'
				THEN COALESCE(
					pil."PriceIndexObservation_PriceIndex_IndexID"
						|| MAP(kvCPIPriceIndexPeriod."Value",
								'On', '-_-' || pil."PriceIndexObservation_PriceIndexPeriod",
								'Off', '')
						|| '-_-BP' 
						|| pil."PriceIndexObservation_BasePeriod",
					osxid."osxID")
			ELSE NULL
		END AS "consumerPriceIndexDefinition",
		CASE 
			WHEN sec."FinancialInstrument_SecurityCategory" = 'DebtInstrument' AND sec."FinancialInstrument_DebtSecurityCategory" = 'IndexLinkedInstrument' AND ind."Index_IndexCategory" = 'PriceIndex'
				THEN pil."PriceIndexObservation_PriceIndexValue"
			ELSE NULL
		END AS "originalConsumerPriceIndex",
		CASE
			WHEN sec."FinancialInstrument_PrincipalInflationAdjustmentIndicator" = TRUE 
				THEN '2'
			WHEN sec."FinancialInstrument_InterestPayoutInflationAdjustmentIndicator" = TRUE
				THEN '1'
			WHEN sec."FinancialInstrument_DeflationFloorAmount" <> 0
				THEN '3' 
			ELSE NULL
		END AS "indexationRule",
		CASE 
			WHEN sec."FinancialInstrument_DebtSecurityCategory" = 'IndexLinkedInstrument'
				THEN
					MAP(sec."FinancialInstrument_PriceIndexCalculationMethod",
						kv_calcMonthly."Value",'1',
						kv_calcInterpolation."Value",'2') 
		END AS "referenceIndexRule",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			ABS(sec."FinancialInstrument_IndexationLagPeriodLength") * -1,
			mcp."cyclePeriod"
		) AS "indexationLag"

	FROM "com.adweko.adapter.osx.inputdata.risk::BV_Security" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS sec
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Index" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS ind
			ON ind."Index_IndexID" = sec."FinancialInstrument_Index_IndexID"
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = sec."FinancialInstrument_IndexationLagPeriodTimeUnit"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_PriceIndexLevel" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pil 
			ON pil."PriceIndexObservation_PriceIndex_IndexID" = ind."Index_IndexID"
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS kv_calcMonthly
			ON kv_calcMonthly."KeyID" = 'IndexCalcMethodMonthly'
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS kv_calcInterpolation
			ON kv_calcInterpolation."KeyID" = 'IndexCalcMethodInterpolation'
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_marketdataIdentifierCode_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS osxid
			ON osxid."marketPriceKind" = 'ConsumerPriceIndex'
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvCPIPriceIndexPeriod
			ON kvCPIPriceIndexPeriod."KeyID" = 'ConsumerPriceIndexDefinition_PriceIndexPeriodSwitch';
			
END;