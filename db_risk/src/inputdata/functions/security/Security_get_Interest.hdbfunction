FUNCTION "com.adweko.adapter.osx.inputdata.security::Security_get_Interest" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"FinancialInstrument_FinancialInstrumentID" NVARCHAR(128),
		"FinancialInstrument_ZeroBond" BOOLEAN,
		"Interest_InterestType" NVARCHAR(40),
		"Interest_FirstInterestPeriodStartDate" DATE,
		"Interest_FirstInterestPeriodEndDate" DATE,	
		"Interest_LastInterestPeriodEndDate" DATE,
		"Interest_FirstDueDate" DATE,
		"Interest_DayOfMonthOfInterestPayment" INTEGER,
		"Interest_DayOfMonthOfInterestPeriodEnd" INTEGER,
		"Interest_InterestScheduleType" NVARCHAR(100),
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),
		"currentNominalInterestRate" DECIMAL(15, 11),
		"repricingType" NVARCHAR(1),
		"pricingMarketObject" NVARCHAR(40),
		"rateAdd" DECIMAL(15, 11),
		"rateTerm" NVARCHAR(10),
		"rateMultiplier" DECIMAL(15, 11),
		"fixingDays" NVARCHAR(10),
		"globalCap" DECIMAL(15, 11),
		"globalFloor" DECIMAL(15, 11),
		"cap" DECIMAL(15, 11),
		"floor" DECIMAL(15, 11),
		"contractDayCountMethod" NVARCHAR(2),
		"interestPaymentType" NVARCHAR(1),
		"businessDayConvention" NVARCHAR(2),
		"calendar" NVARCHAR(75),
		"eomConvention" NVARCHAR(1),
		"Interest_SequenceNumber" INT,
		"PreferredOrCommonStock" NVARCHAR(10),
		"Interest_InterestIsCompounded" INT,
		"lookbackPeriod" NVARCHAR(10),
		"rateCappingFlooring" NVARCHAR(40),
		"rateAveragingCap" DECIMAL(15, 11),
		"rateAveragingFloor" DECIMAL(15, 11)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/***********************************
        Select and map Interest Data
    ***********************************/
	RETURN

	SELECT DISTINCT --Regular IP Periods after Stub
		"FinancialInstrument_FinancialInstrumentID",
		"FinancialInstrument_ZeroBond",
		"Interest_InterestType",
		"Interest_FirstInterestPeriodStartDate",
		"Interest_FirstInterestPeriodEndDate",
		"Interest_LastInterestPeriodEndDate",
		"Interest_FirstDueDate",
		"Interest_DayOfMonthOfInterestPayment",
		"Interest_DayOfMonthOfInterestPeriodEnd",
		"Interest_InterestScheduleType",
		'IP' AS "arraySpecKind",
		"Interest_FirstInterestPeriodEndDate" AS "cycleDate",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
				"Interest_InterestPeriodLength",
				mcp."cyclePeriod"
			) AS "cyclePeriod",
		CASE 
			WHEN "Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
				THEN "Interest_FixedRate"
			ELSE "ApplicableInterestRate_Rate"
		END AS "currentNominalInterestRate",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		NULL AS "rateAdd",
		NULL AS "rateTerm",
		NULL AS "rateMultiplier",
		NULL AS "fixingDays",
		NULL AS "globalCap",
		NULL AS "globalFloor",
		NULL AS "cap",
		NULL AS "floor",
		dcm."dayCountMethod" AS "contractDayCountMethod",
		"com.adweko.adapter.osx.inputdata.common::get_interestPaymentType" ("Interest_InterestInAdvance", 2, 1) AS "interestPaymentType",
		"com.adweko.adapter.osx.inputdata.common::get_businessDayConvention" (bdc."OsxBusinessDayConventionCode") AS "businessDayConvention",
		cal."calendar" AS "calendar",
		"com.adweko.adapter.osx.inputdata.common::get_eomConvention" ("Interest_DayOfMonthOfInterestPeriodEnd") AS "eomConvention",
		"Interest_SequenceNumber",
		"FinancialInstrument_PreferredOrCommonStock" AS "PreferredOrCommonStock",
		MAP("Interest_InterestIsCompounded",
			true, 1,
			false, 0
		) AS "Interest_InterestIsCompounded",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND sec."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
						sec."Interest_ResetCutoffLength",
						mcpre."cyclePeriod")
		END AS "lookbackPeriod",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND sec."Interest_InterestIsCompounded" = true
			THEN 'MONETARY_REFERENCE'
		END AS "rateCappingFlooring",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND sec."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", sec."Interest_FloatingRateMax", sec."Interest_VariableRateMax")
		END AS "rateAveragingCap",	
		CASE WHEN kvMonetaryRef."Value" = 'On' AND sec."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", sec."Interest_FloatingRateMin", sec."Interest_VariableRateMin")
		END AS "rateAveragingFloor"
		FROM "com.adweko.adapter.osx.inputdata.risk::BV_Security" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) sec
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_BDC_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bdc
		ON bdc."CalculationDay" = sec."Interest_BusinessDayConvention"
		AND bdc."PaymentDay" = sec."Interest_DueScheduleBusinessDayConvention"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS dcm ON (
			dcm."DayCountConvention" = sec."Interest_DayCountConvention" )
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_calendar_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) cal ON (
			cal."BusinessCalendar" = sec."Interest_InterestBusinessCalendar"
			)
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = sec."Interest_InterestPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcpre
		ON  mcpre."BusinesscyclePeriod" = sec."Interest_ResetCutoffTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS cfss
			ON cfss."KeyID" = 'CapFloorSwitch'
	
	WHERE sec."Interest_InterestCategory" = 'InterestPeriodSpecification'
	AND "Interest_FirstInterestPeriodEndDate" IS NOT NULL 
	AND "Interest_LastInterestPeriodEndDate">"Interest_FirstInterestPeriodEndDate"
	
	UNION ALL
	
	SELECT DISTINCT --Stub Period and regular IP Periods
		"FinancialInstrument_FinancialInstrumentID",
		"FinancialInstrument_ZeroBond",
		"Interest_InterestType",
		"Interest_FirstInterestPeriodStartDate",
		"Interest_FirstInterestPeriodEndDate",
		"Interest_LastInterestPeriodEndDate",
		"Interest_FirstDueDate",
		"Interest_DayOfMonthOfInterestPayment",
		"Interest_DayOfMonthOfInterestPeriodEnd",
		"Interest_InterestScheduleType",
		'IP' AS "arraySpecKind",
		"Interest_FirstInterestPeriodStartDate" AS "cycleDate",
		CASE 
			WHEN "Interest_FirstInterestPeriodEndDate" IS NOT NULL 
				THEN '999Y' 
			ELSE
				"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
					"Interest_InterestPeriodLength",
					mcp."cyclePeriod")
		END AS "cyclePeriod",
		CASE 
			WHEN "Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
				THEN "Interest_FixedRate"
			ELSE "ApplicableInterestRate_Rate"
		END AS "currentNominalInterestRate",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		NULL AS "rateAdd",
		NULL AS "rateTerm",
		NULL AS "rateMultiplier",
		NULL AS "fixingDays",
		NULL AS "globalCap",
		NULL AS "globalFloor",
		NULL AS "cap",
		NULL AS "floor",
		dcm."dayCountMethod" AS "contractDayCountMethod",
		"com.adweko.adapter.osx.inputdata.common::get_interestPaymentType" ("Interest_InterestInAdvance", 2, 1) AS "interestPaymentType",
		"com.adweko.adapter.osx.inputdata.common::get_businessDayConvention" (bdc."OsxBusinessDayConventionCode") AS "businessDayConvention",
		cal."calendar" AS "calendar",
		"com.adweko.adapter.osx.inputdata.common::get_eomConvention" ("Interest_DayOfMonthOfInterestPeriodEnd") AS "eomConvention",
		"Interest_SequenceNumber",
		"FinancialInstrument_PreferredOrCommonStock" AS "PreferredOrCommonStock",
		MAP("Interest_InterestIsCompounded",
			true, 1,
			false, 0
		) AS "Interest_InterestIsCompounded",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND sec."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
						sec."Interest_ResetCutoffLength",
						mcpre."cyclePeriod")
		END AS "lookbackPeriod",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND sec."Interest_InterestIsCompounded" = true
			THEN 'MONETARY_REFERENCE'
		END AS "rateCappingFlooring",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND sec."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", sec."Interest_FloatingRateMax", sec."Interest_VariableRateMax")
		END AS "rateAveragingCap",	
		CASE WHEN kvMonetaryRef."Value" = 'On' AND sec."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", sec."Interest_FloatingRateMin", sec."Interest_VariableRateMin")
		END AS "rateAveragingFloor"
		FROM "com.adweko.adapter.osx.inputdata.risk::BV_Security" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) sec
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_BDC_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bdc
		ON bdc."CalculationDay" = sec."Interest_BusinessDayConvention"
		AND bdc."PaymentDay" =  sec."Interest_DueScheduleBusinessDayConvention"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS dcm ON (
			dcm."DayCountConvention" = sec."Interest_DayCountConvention" )
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_calendar_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) cal ON (
			cal."BusinessCalendar" = sec."Interest_InterestBusinessCalendar"
			)
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = sec."Interest_InterestPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcpre
		ON  mcpre."BusinesscyclePeriod" = sec."Interest_ResetCutoffTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS cfss
			ON cfss."KeyID" = 'CapFloorSwitch'
	WHERE sec."Interest_InterestCategory" = 'InterestPeriodSpecification'
	AND sec."Interest_FirstInterestPeriodStartDate" != IFNULL(sec."Interest_FirstInterestPeriodEndDate",'1900-01-01')

	UNION ALL
	
	SELECT DISTINCT --regular DependentFloatingRateSpecification RP Period after Stub 
		"FinancialInstrument_FinancialInstrumentID",
		"FinancialInstrument_ZeroBond",
		"Interest_InterestType",
		"Interest_FirstInterestPeriodStartDate",
		"Interest_FirstInterestPeriodEndDate",
		"Interest_LastInterestPeriodEndDate",
		"Interest_FirstDueDate",
		"Interest_DayOfMonthOfInterestPayment",
		"Interest_DayOfMonthOfInterestPeriodEnd",
		"Interest_InterestScheduleType",
		CASE 
			WHEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", "Interest_FloatingRateMax", "Interest_VariableRateMax") IS NOT NULL
				OR "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", "Interest_FloatingRateMin", "Interest_VariableRateMin") IS NOT NULL
				THEN 'RPS'
			ELSE 'RP'
		END AS "arraySpecKind",
		"Interest_FirstInterestPeriodEndDate" AS "cycleDate",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			"Interest_InterestPeriodLength",
			mcp2."cyclePeriod"
		)
		AS "cyclePeriod",
		NULL AS "currentNominalInterestRate",
		"com.adweko.adapter.osx.inputdata.common::get_repricingType"(
			ReferenceRateType => sec."ReferenceRate_ReferenceRateType",
			InterestSpecificationCategory => sec."Interest_InterestSpecificationCategory",
			FixingRateSpecificationCategory => sec."Interest_FixingRateSpecificationCategory",
			DependentFloatingRateSpecification_ResetInArrears => sec."Interest_ResetInArrears",
			IndependentFloatingRateSpecification_ResetAtMonthUltimo => sec."Interest_ResetAtMonthUltimo",
			CutOffRelativeToDateCode => sec."Interest_CutoffRelativeToDate"
		) AS "repricingType",
		MAP(kvMonetaryRef."Value",
			'On', CASE WHEN sec."Interest_InterestIsCompounded" = true
					THEN COALESCE(sec."FinancialInstrument_NominalAmountCurrency", sec."FinancialInstrument_IssuePriceCurrency")
							|| '-' 
							|| pmo."pricingMarketObject"
							|| '-'
							|| "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
									"Interest_InterestPeriodLength",
									mcp2."cyclePeriod")
							|| '-MR'
					ELSE pmo."pricingMarketObject" END,
			'Off', pmo."pricingMarketObject") AS "pricingMarketObject",
		"Interest_Spread" AS "rateAdd",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			sec."ReferenceRate_FixingIntervalPeriodLength",
			mcp_rr."cyclePeriod"
		) AS "rateTerm", 
		"Interest_ReferenceRateFactor" AS "rateMultiplier",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			"Interest_ResetLagLength",
			mcp."cyclePeriod"
			) AS "fixingDays",
		"com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", "Interest_FloatingRateMax", "Interest_VariableRateMax") 
		AS "globalCap",
		"com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", "Interest_FloatingRateMin", "Interest_VariableRateMin")
		AS "globalFloor",
		"com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", "Interest_FloatingRateMax", "Interest_VariableRateMax") AS "cap",
		"com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", "Interest_FloatingRateMin", "Interest_VariableRateMin") AS "floor",
		NULL AS "contractDayCountMethod",
		NULL AS "interestPaymentType",
		NULL AS "businessDayConvention",
		NULL AS "calendar",
		NULL AS "eomConvention",
		"Interest_SequenceNumber",
		"FinancialInstrument_PreferredOrCommonStock" AS "PreferredOrCommonStock",
		NULL AS "Interest_InterestIsCompounded",
		NULL AS "lookbackPeriod",
		NULL AS "rateCappingFlooring",
		NULL AS "rateAveragingCap",
		NULL AS "rateAveragingFloor"
		FROM "com.adweko.adapter.osx.inputdata.risk::BV_Security" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) sec
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_pricingMarketObject_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pmo ON (
			pmo."ReferenceRateID" = "Interest_ReferenceRateID"
			)
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = sec."Interest_ResetLagTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp2
			ON  mcp2."BusinesscyclePeriod" = sec."Interest_InterestPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS mcp_rr
		ON  mcp_rr."BusinesscyclePeriod" = sec."ReferenceRate_FixingIntervalTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS cfss
			ON cfss."KeyID" = 'CapFloorSwitch'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
	WHERE (
			"Interest_InterestSpecificationCategory" = 'FloatingRateSpecification' 
			AND "Interest_FixingRateSpecificationCategory" ='DependentFloatingRateSpecification'
			AND "Interest_LastInterestPeriodEndDate" IS NOT NULL
			AND "Interest_LastInterestPeriodEndDate" > "Interest_FirstInterestPeriodEndDate"
			)
			
	UNION ALL
	
	SELECT DISTINCT --regular RP Periods and Stub Periods
		"FinancialInstrument_FinancialInstrumentID",
		"FinancialInstrument_ZeroBond",
		"Interest_InterestType",
		"Interest_FirstInterestPeriodStartDate",
		"Interest_FirstInterestPeriodEndDate",
		"Interest_LastInterestPeriodEndDate",
		"Interest_FirstDueDate",
		"Interest_DayOfMonthOfInterestPayment",
		"Interest_DayOfMonthOfInterestPeriodEnd",
		"Interest_InterestScheduleType",
		CASE 
			WHEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", "Interest_FloatingRateMax", "Interest_VariableRateMax") IS NOT NULL
				OR "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", "Interest_FloatingRateMin", "Interest_VariableRateMin") IS NOT NULL
				THEN 'RPS'
			ELSE 'RP'
		END AS "arraySpecKind",
		CASE 
			WHEN "Interest_FixingRateSpecificationCategory" = 'IndependentFloatingRateSpecification'
				AND "Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
				THEN "Interest_FirstRegularFloatingRateResetDate"
			WHEN "Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification'
				THEN "Interest_FirstInterestPeriodStartDate"
			WHEN "Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
				THEN "Interest_FirstInterestPeriodStartDate"
		END AS "cycleDate",
 		CASE 
			WHEN "Interest_FixingRateSpecificationCategory" = 'IndependentFloatingRateSpecification' --(HF00939599 - currently reworked)
			AND "Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
				THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
					"Interest_ResetPeriodLength",
					mcp3."cyclePeriod"
					)
			WHEN 
			("Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification'
			AND "Interest_LastInterestPeriodEndDate" IS NULL) OR
			"Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
				THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
					"Interest_InterestPeriodLength",
					mcp2."cyclePeriod"
					)
			WHEN "Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification'
			AND "Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
				AND "Interest_LastInterestPeriodEndDate" IS NOT NULL
				THEN '999Y' 
		END AS "cyclePeriod",
		NULL AS "currentNominalInterestRate",
		"com.adweko.adapter.osx.inputdata.common::get_repricingType"(
			ReferenceRateType => sec."ReferenceRate_ReferenceRateType",
			InterestSpecificationCategory => sec."Interest_InterestSpecificationCategory",
			FixingRateSpecificationCategory => sec."Interest_FixingRateSpecificationCategory",
			DependentFloatingRateSpecification_ResetInArrears => sec."Interest_ResetInArrears",
			IndependentFloatingRateSpecification_ResetAtMonthUltimo => sec."Interest_ResetAtMonthUltimo",
			CutOffRelativeToDateCode => sec."Interest_CutoffRelativeToDate"
		) AS "repricingType",
		MAP(kvMonetaryRef."Value",
			'On', CASE WHEN sec."Interest_InterestIsCompounded" = true
					THEN COALESCE(sec."FinancialInstrument_NominalAmountCurrency", sec."FinancialInstrument_IssuePriceCurrency")
							|| '-' 
							|| pmo."pricingMarketObject"
							|| '-'
							|| CASE 
									WHEN "Interest_FixingRateSpecificationCategory" = 'IndependentFloatingRateSpecification' --(HF00939599 - currently reworked)
									AND "Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
										THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
											"Interest_ResetPeriodLength",
											mcp3."cyclePeriod"
											)
									WHEN 
									("Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification'
									AND "Interest_LastInterestPeriodEndDate" IS NULL) OR
									"Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
										THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
											"Interest_InterestPeriodLength",
											mcp2."cyclePeriod"
											)
									WHEN "Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification'
									AND "Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
										AND "Interest_LastInterestPeriodEndDate" IS NOT NULL
										THEN '999Y' 
								END
							|| '-MR'
					ELSE pmo."pricingMarketObject" END,
			'Off', pmo."pricingMarketObject") AS "pricingMarketObject",
		MAP("Interest_InterestSpecificationCategory",
			'FloatingRateSpecification', "Interest_Spread",
			'FixedRateSpecification', "Interest_FixedRate"
			) AS "rateAdd",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			sec."ReferenceRate_FixingIntervalPeriodLength",
			mcp_rr."cyclePeriod"
		) AS "rateTerm",
		MAP("Interest_InterestSpecificationCategory",
			'FloatingRateSpecification', "Interest_ReferenceRateFactor",
			NULL
			) AS "rateMultiplier",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			"Interest_ResetLagLength",
			mcp."cyclePeriod"
		) AS "fixingDays",
		"com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", "Interest_FloatingRateMax", "Interest_VariableRateMax")
		AS "globalCap",
		"com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", "Interest_FloatingRateMin", "Interest_VariableRateMin")
		AS "globalFloor",
		"com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", "Interest_FloatingRateMax", "Interest_VariableRateMax") AS "cap",
		"com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", "Interest_FloatingRateMin", "Interest_VariableRateMin") AS "floor",
		NULL AS "contractDayCountMethod",
		NULL AS "interestPaymentType",
		NULL AS "businessDayConvention",
		NULL AS "calendar",
		NULL AS "eomConvention",
		"Interest_SequenceNumber",
		"FinancialInstrument_PreferredOrCommonStock" AS "PreferredOrCommonStock",
		NULL AS "Interest_InterestIsCompounded",
		NULL AS "lookbackPeriod",
		NULL AS "rateCappingFlooring",
		NULL AS "rateAveragingCap",
		NULL AS "rateAveragingFloor"
		FROM "com.adweko.adapter.osx.inputdata.risk::BV_Security" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) sec
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_pricingMarketObject_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pmo ON (
			pmo."ReferenceRateID" = "Interest_ReferenceRateID"
			)
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = sec."Interest_ResetLagTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp2
			ON  mcp2."BusinesscyclePeriod" = sec."Interest_InterestPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp3
			ON  mcp3."BusinesscyclePeriod" = sec."Interest_ResetPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS mcp_rr
		ON  mcp_rr."BusinesscyclePeriod" = sec."ReferenceRate_FixingIntervalTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS cfss
			ON cfss."KeyID" = 'CapFloorSwitch'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
	WHERE (
			("Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
			AND "Interest_FixingRateSpecificationCategory" = 'IndependentFloatingRateSpecification'
			) OR
			("Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
			AND "Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification'
			AND "Interest_FirstInterestPeriodStartDate" != "Interest_FirstInterestPeriodEndDate"
			)
			OR (
				"Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
			)
			)
			;
END;
