FUNCTION "com.adweko.adapter.osx.inputdata.fxspot::FXSpot_Contract"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"contractID" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"contractedFxRate" DECIMAL(38, 11),
		"currency" NVARCHAR(3),
		"currentPrincipal" DECIMAL(34, 6),
		"dealDate" DATE,
		"discriminator" NVARCHAR(32),
		"foreignCurrency" NVARCHAR(3),
		"futureQuantity" INT,
		"settlementDate" DATE,
		"commitmentType" NVARCHAR(1),
		"productType" NVARCHAR(128),
		"contractRole" NVARCHAR(1),
		"portfolioType" NVARCHAR(255),
		"grossRecoveryRate" DECIMAL(15, 11),
		"counterparty" NVARCHAR(128),
		"dataGroup" NVARCHAR(128),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
		"grossPayableAmountAtDefault" DECIMAL(34, 6),
		"nonPerformingDate" DATE,
		"clearingHouse" NVARCHAR(128),
		"netRecoveryRate" DECIMAL(15, 11),
		"contractFSType" NVARCHAR(256),
		"contractFSSubType" NVARCHAR(256),
		"productFSClass" NVARCHAR(128),
		"instrumentType" NVARCHAR(128),
		"isUnderLiquidityManagementControl" INT,
		"cadCrConversionFactor" NVARCHAR(75),
		"structuredProductName" NVARCHAR(255),
		"writeOffAmount" DECIMAL(34, 6),
		"bookValue" DECIMAL(34, 6),
		"exposureClass" NVARCHAR(255)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
    RETURN
    
    SELECT
		"com.adweko.adapter.osx.inputdata.common::get_id" (
			root."contractID",
			root."node_No",
			root."dataGroup"
		) AS "id",
		root."contractID",
		root."FinancialContract_FinancialContractID" AS "sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		root."node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		--TODO "contractSoftLinkKey",
		root."contractedFxRate",
		root."currency",
		root."currentPrincipal",
		root."dealDate",
		'FWDFX' AS "discriminator",
		root."foreignCurrency",
		null AS "futureQuantity",
		root."settlementDate" AS "settlementDate",
		'2' AS "commitmentType",
		root."productType",
		'1' AS "contractRole",
		root."portfolioType",
		rec_before."CreditRiskRecoveryRate_RecoveryRate" AS "grossRecoveryRate",
		root."counterparty",
		root."dataGroup" AS "dataGroup",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",
		gpamount."Valuation_EstimatedMarketValue" AS "grossPayableAmountAtDefault",
		root."nonPerformingDate",
		root."clearingHouse",
		rec_after."CreditRiskRecoveryRate_RecoveryRate" AS "netRecoveryRate",
		'DRV_FOREX' AS "contractFSType",
		'DRV_FORWARD' AS "contractFSSubType",
		'DERIVATIVE' AS "productFSClass",
		'ForwardCurrency' AS "instrumentType",
		1 AS "isUnderLiquidityManagementControl",
		root."cadCrConversionFactor",
		root."structuredProductName",
		MAP(writeOffKey."Value",
			'WriteDownAmountInPositionCurrency', writeOff."WriteDownAmountInPositionCurrency",
			'WriteDownAmount', writeOff."WriteDownAmount"
		) AS "writeOffAmount",
		bok."BookValue_BookValue" AS "bookValue",
		expo."exposureClass"

    FROM "com.adweko.adapter.osx.inputdata.fxspot::FXSpot_get_Root"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'FXSpot') AS expo
			ON expo."CreditRiskExposure_FinancialContract_FinancialContractID"				= root."FinancialContract_FinancialContractID"
			AND expo."CreditRiskExposure_FinancialContract_IDSystem"						= root."FinancialContract_IDSystem"

    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.fxspot::FXSpot_get_grossPayableAmountAtDefault"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS gpamount
    	 ON gpamount."FinancialContract_FinancialContractID"								= root."FinancialContract_FinancialContractID"
    	AND gpamount."FinancialContract_IDSystem"											= root."FinancialContract_IDSystem"
       	
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.fxspot::FXSpot_get_RecoveryRate"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS rec_before
    	 ON rec_before."CreditRiskRecoveryRate_FinancialContract_FinancialContractID"		= root."FinancialContract_FinancialContractID"
    	AND rec_before."CreditRiskRecoveryRate_FinancialContract_IDSystem"					= root."FinancialContract_IDSystem"
    	AND rec_before."CreditRiskRecoveryRate_RecoveryRateEstimationMethod"				= 'BeforeCreditEnhancements'
    	
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.fxspot::FXSpot_get_RecoveryRate"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS rec_after
    	 ON rec_after."CreditRiskRecoveryRate_FinancialContract_FinancialContractID"		= root."FinancialContract_FinancialContractID"
    	AND rec_after."CreditRiskRecoveryRate_FinancialContract_IDSystem"					= root."FinancialContract_IDSystem"
    	AND rec_after."CreditRiskRecoveryRate_RecoveryRateEstimationMethod"					= 'AfterCreditEnhancements'

    --writeOffAmount
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS writeOffKey
		ON writeOffKey."KeyID" = 'WriteDownPositionCurrenyOrAmount'

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityWriteOffFC"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Loan') AS writeOff
		ON writeOff."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND writeOff."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"

    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
		 ON bok."BookValue_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND bok."BookValue_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND bok."BookValue_BookValueCurrency" = root."currency";
END;