FUNCTION "com.adweko.adapter.osx.inputdata.marketdata.derivativePrice::Derivative_MarketData" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP
		)
RETURNS TABLE (
		"marketPriceKind" NVARCHAR(40),
		"riskFactorDefinition" NVARCHAR(256),
		"observationDate" DATE,
		"value" DECIMAL(34, 11)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN

    SELECT
        "marketPriceKind",
        "riskFactorDefinition",
        "observationDate",
        "value"
    FROM (
        SELECT DISTINCT 'Indices' AS "marketPriceKind",
            "EndOfDayListedDerivativePriceObservation_FinancialInstrument_FinancialInstrumentID" AS "riskFactorDefinition",
            "EndOfDayListedDerivativePriceObservation_BusinessValidFrom" AS "observationDate",
            CAST("EndOfDayListedDerivativePriceObservation_Close" AS DECIMAL(34, 11)) AS "value",
            ROW_NUMBER() OVER (
                PARTITION BY
                    "EndOfDayListedDerivativePriceObservation_ExpirationDate"
                ORDER BY
                    dp."Priority" ASC
            ) AS "Prio"

        FROM "com.adweko.adapter.osx.inputdata.risk::BV_DerivativeRate" (
                :I_BUSINESS_DATE,
                :I_SYSTEM_TIMESTAMP
                )

        INNER JOIN "com.adweko.adapter.osx.mappings::map_DataProvider_View"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS dp
            ON  dp."marketPriceKind"    					= 'TradableContractPrices'
            AND IFNULL(TRIM(dp."PriceDataProvider"), '')	= IFNULL(TRIM("EndOfDayListedDerivativePriceObservation_PriceDataProvider"), '')
    )
    WHERE "Prio" = 1;
END;
