FUNCTION "com.adweko.adapter.osx.inputdata.marketdata.volatility::VolatilitySurfaceObservationFilter_Marketdata" (
		I_VOLATILITY_SURFACE_NAME NVARCHAR(128),
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP DEFAULT '9999-01-01'
		)
RETURNS TABLE (
		"valuationDate" DATE,
		"interestRateModelType" NVARCHAR(40),
		"shift" DECIMAL(15,11),
		"strike" DECIMAL(34,6),
		"timeToMaturityPeriod" NVARCHAR(8),
		"distanceToATM" DECIMAL(15,11),
		"tenorPeriod" NVARCHAR(8),
		"swapPeriod" NVARCHAR(8),
		"volatility" DECIMAL(15,11)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN
		SELECT 
			"valuationDate",
			"interestRateModelType",
			"shift",
			"strike",
			"timeToMaturityPeriod",
			"distanceToATM",
			"tenorPeriod",
			"swapPeriod",
			"volatility"
			FROM (
				SELECT DISTINCT
					"valuationDate",
					"interestRateModelType",
					"shift",
					"strike",
					"timeToMaturityPeriod",
					"distanceToATM",
					"tenorPeriod",
					"swapPeriod",
					"volatility",
					ROW_NUMBER() OVER (
					 	PARTITION BY
					 		"volatilityID",
					 		"timeToMaturityPeriod",
					 		"tenorPeriod",
					 		"swapPeriod"
					  	ORDER BY
		                    "Priority" ASC
	            	) AS "Prio"
				FROM "com.adweko.adapter.osx.inputdata.marketdata.volatility::VolatilitySurfaceObservation_Marketdata" (
						:I_BUSINESS_DATE,
						:I_SYSTEM_TIMESTAMP
					)
				WHERE :I_VOLATILITY_SURFACE_NAME = "volatilityID"
			)
		WHERE "Prio" = 1	
	;
END;
