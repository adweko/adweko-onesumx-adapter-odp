FUNCTION "com.adweko.adapter.osx.inputdata.marketdata.productrate::ProductRate_Marketdata" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP
		)
RETURNS TABLE (
		"riskFactorDefinition" NVARCHAR(256),
		"observationDate" DATE,
		"value" DECIMAL(34, 11)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN

	SELECT
		"riskFactorDefinition",
		"observationDate",
		"value"
	FROM(
		SELECT DISTINCT 
			COALESCE(osxid."osxID", ref."ReferenceRate_ReferenceRateID") AS "riskFactorDefinition",
			ref."EndOfDayRateObservation_BusinessValidFrom" AS "observationDate",
			CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard"(
				"EndOfDayRateObservation_Close",
				'Close',
				kv."Value"
				) AS DECIMAL(34, 11)) AS "value",
            ROW_NUMBER() OVER (
                PARTITION BY
                    ref."ReferenceRate_ReferenceRateID"
                ORDER BY
                    dp."Priority" ASC
            ) AS "Prio"
				
		FROM "com.adweko.adapter.osx.inputdata.risk::BV_ReferenceRate" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP
				) AS ref
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_marketdataIdentifierCode_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) osxid
			ON osxid."fsdmID" = ref."ReferenceRate_ReferenceRateID"
			AND osxid."marketPriceKind" = 'ProductRate'
		INNER JOIN "com.adweko.adapter.osx.mappings::map_DataProvider_View"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS dp
            ON  dp."marketPriceKind" = 'ReferenceRate'
            AND IFNULL(TRIM(dp."PriceDataProvider"), '') = IFNULL(TRIM(ref."ReferenceRate_ProviderOfReferenceRate"), '')
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
				ON  kv."KeyID" = 'SAPPercentageStandard'
		WHERE "ReferenceRate_ReferenceRateType" = 'ProductRate'
	)
	WHERE "Prio" = 1;
END;
