FUNCTION "com.adweko.adapter.osx.inputdata.marketdata.yieldcurve::YieldCurve_Marketdata" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP
		)
RETURNS TABLE (
		"riskFactorDefinition" NVARCHAR(256),
		"interpolation" NVARCHAR(40),
		"rateAdaption" NVARCHAR(40),
		"zeroCoupon" NVARCHAR(128),
		"observationDate" DATE,
		"term" NVARCHAR(8),
		"rate" DECIMAL(15, 11),
		"dayCountMethod" NVARCHAR(2),
		"frequency" NVARCHAR(40)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN

	/*********************************
    	End of Day Rate Observation
	*********************************/
	SELECT
    		"riskFactorDefinition",
    		"interpolation",
    		"rateAdaption",
    		"zeroCoupon",
    		"observationDate",
    		"term",
    		"rate",
    		"dayCountMethod",
    		"frequency"
    FROM(
        SELECT DISTINCT
            COALESCE(osxid."osxID", yc."YieldCurve_Name") AS "riskFactorDefinition",
            MAP(yc."YieldCurve_InterpolationMethod",
                'LinearInterpolation',	'Linear',
                'CubicSplineInterpolation',	'CubicSpline',
                'ForwardMonotoneConvexInterpolation',	'ForwardMonotoneConvex',
                'LogLinearInterpolation',	'LogLinear',
                'StaircaseInterpolation',	'Staircase',
                'Linear'
            ) AS "interpolation",
            'NONE' AS "rateAdaption",
            'TRUE' AS "zeroCoupon",
            "EndOfDayRateObservation_BusinessValidFrom" AS "observationDate",
            "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
                "ReferenceRate_TimeToMaturity",
                mcp."cyclePeriod"
            ) AS "term",
            TO_DECIMAL("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard"(
                ref."EndOfDayRateObservation_Close",
                'Close',
                kv."Value"
                ), 15, 11) AS "rate",
            dcm."dayCountMethod" AS "dayCountMethod",
            MAP(ref."ReferenceRate_CompoundingFrequency",
                'Yearly',	'Annually',
                'SemiYearly',	'SemiAnnually',
                'Quarterly',	'Quarterly',
                'Monthly',	'Monthly',
                'Daily',	'Continuous',
                'Annually'
            ) AS "frequency",
            ROW_NUMBER() OVER (
            	PARTITION BY
            		yc."YieldCurve_YieldCurveID",
            		CONCAT("ReferenceRate_TimeToMaturity", ref."ReferenceRate_TimeToMaturityUnit")
                ORDER BY
                    dp."Priority"
            ) AS "Prio"

        FROM "com.adweko.adapter.osx.inputdata.risk::BV_YieldCurve" (
                :I_BUSINESS_DATE,
                :I_SYSTEM_TIMESTAMP
                ) yc
        INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ReferenceRate" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) ref
            ON ref."ReferenceRate_ReferenceRateID" = yc."SimpleYieldCurveReferenceRateAssignment_ReferenceRate_ReferenceRateID"
            AND IFNULL(yc."SimpleYieldCurveReferenceRateAssignment_SimpleYieldCurve_ProviderOfYieldCurve", '')	= IFNULL(ref."EndOfDayRateObservation_RateDataProvider", '')
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp ON
            mcp."BusinesscyclePeriod" = ref."ReferenceRate_TimeToMaturityUnit"
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv ON
            kv."KeyID" = 'SAPPercentageStandard'
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) dcm ON
            dcm."DayCountConvention" = ref."ReferenceRate_DayCountConvention"
        INNER JOIN "com.adweko.adapter.osx.mappings::map_DataProvider_View"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS dp
            ON  dp."marketPriceKind"     = 'YieldCurve'
            AND IFNULL(TRIM(dp."PriceDataProvider"), '') = IFNULL(TRIM(yc."YieldCurve_ProviderOfYieldCurve"), '')
            AND IFNULL(TRIM(dp."PriceDataProvider"), '') = IFNULL(TRIM(yc."SimpleYieldCurveReferenceRateAssignment_SimpleYieldCurve_ProviderOfYieldCurve"), '')
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_marketdataIdentifierCode_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) osxid
            ON osxid."fsdmID" = yc."YieldCurve_Name"
            AND osxid."marketPriceKind" = 'YieldCurve'
        INNER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS kv_hm
            ON kv_hm."KeyID" = 'YieldCurveRead'
            AND kv_hm."Value" = '1' --EndOfDayRateObservation
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_unitFilter_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS uf
            ON uf."StreamName" = 'YieldCurve'
        WHERE ("YieldCurve_IsSpreadCurve" = FALSE OR "YieldCurve_YieldCurveCategory"='SimpleYieldCurve')
        	AND (uf."UnitFilter" = ref."ReferenceRate_TimeToMaturityUnit" OR uf."UnitFilter" IS NULL)
    )
    WHERE "Prio" = 1

	UNION ALL

	/*****************************
        Curve Data Points
    ****************************/
    SELECT
            "riskFactorDefinition",
            "interpolation",
            "rateAdaption",
            "zeroCoupon",
            "observationDate",
            "term",
            "rate",
            "dayCountMethod",
            "frequency"
    FROM(
        SELECT DISTINCT
            COALESCE(osxid."osxID", yc."YieldCurve_Name") AS "riskFactorDefinition",
            MAP(yc."YieldCurve_InterpolationMethod",
                'LinearInterpolation',	'Linear',
                'CubicSplineInterpolation',	'CubicSpline',
                'ForwardMonotoneConvexInterpolation',	'ForwardMonotoneConvex',
                'LogLinearInterpolation',	'LogLinear',
                'StaircaseInterpolation',	'Staircase',
                'Linear'
            ) AS "interpolation",
            'NONE' AS "rateAdaption",
            'TRUE' AS "zeroCoupon",
            cdp."BusinessValidFrom" AS "observationDate",
            "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
                cdp."TimeToMaturity",
                mcp."cyclePeriod"
                ) AS "term",
            TO_DECIMAL(cdp."Rate", 15, 11) AS "rate",
            dcm."dayCountMethod" AS "dayCountMethod",
            MAP(ref."ReferenceRate_CompoundingFrequency",
                'Yearly',	'Annually',
                'SemiYearly',	'SemiAnnually',
                'Quarterly',	'Quarterly',
                'Monthly',	'Monthly',
                'Daily',	'Continuous',
                'Annually'
            ) AS "frequency",
            ROW_NUMBER() OVER (
            	PARTITION BY
            		yc."YieldCurve_YieldCurveID",
            		CONCAT(cdp."TimeToMaturity", cdp."TimeToMaturityUnit")
                ORDER BY
                    dp."Priority"
            ) AS "Prio"

        FROM "com.adweko.adapter.osx.inputdata.risk::BV_YieldCurve"(
                :I_BUSINESS_DATE ,
                :I_SYSTEM_TIMESTAMP
                ) yc
        INNER JOIN "com.adweko.adapter.osx.synonyms::CurveDataPoint_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) cdp ON
            cdp."ASSOC_YieldCurve_YieldCurveID"=yc."YieldCurve_YieldCurveID"
        LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ReferenceRate" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) ref ON
            ref."ReferenceRate_ReferenceRateID"=yc."SimpleYieldCurveReferenceRateAssignment_ReferenceRate_ReferenceRateID"
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp ON
            mcp."BusinesscyclePeriod" = cdp."TimeToMaturityUnit"
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv ON
            kv."KeyID" = 'SAPPercentageStandard'
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) dcm ON
            dcm."DayCountConvention" = yc."YieldCurve_DayCountConvention"
        INNER JOIN "com.adweko.adapter.osx.mappings::map_DataProvider_View"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS dp
            ON  dp."marketPriceKind" = 'YieldCurve'
            AND IFNULL(TRIM(dp."PriceDataProvider"), '') = IFNULL(TRIM(yc."YieldCurve_ProviderOfYieldCurve"), '')
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_marketdataIdentifierCode_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) osxid
            ON osxid."fsdmID" = yc."YieldCurve_Name"
            AND osxid."marketPriceKind" = 'YieldCurve'
        INNER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS kv_hm
            ON kv_hm."KeyID" = 'YieldCurveRead'
            AND kv_hm."Value" = '2' --CurveDataPoint
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_unitFilter_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS uf
            ON uf."StreamName" = 'YieldCurve'
        WHERE (yc."YieldCurve_IsSpreadCurve" = FALSE OR "YieldCurve_YieldCurveCategory"='SimpleYieldCurve')
            AND (uf."UnitFilter" = ref."ReferenceRate_TimeToMaturityUnit" OR uf."UnitFilter" IS NULL)
    )
    WHERE "Prio" = 1;
END;
