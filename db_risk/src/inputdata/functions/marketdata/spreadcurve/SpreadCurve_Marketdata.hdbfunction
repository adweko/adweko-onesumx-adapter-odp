FUNCTION "com.adweko.adapter.osx.inputdata.marketdata.spreadcurve::SpreadCurve_Marketdata" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP
		)
RETURNS TABLE (
		"riskFactorDefinition" NVARCHAR(256),
		"interpolation" NVARCHAR(40),
		"observationDate" DATE,
		"term" NVARCHAR(8),
		"rate" DECIMAL(15, 11)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN

	/*********************************
    	End of Day Spread Observation
	*********************************/
    SELECT
		"riskFactorDefinition",
		"interpolation",
		"observationDate",
		"term",
		"rate"
    FROM (
        SELECT DISTINCT
            COALESCE(osxid."osxID", sc."SpreadCurveID") AS "riskFactorDefinition",
            MAP(sc."InterpolationMethod",
                'LinearInterpolation',	'Linear',
                'CubicSplineInterpolation',	'CubicSpline',
                'ForwardMonotoneConvexInterpolation',	'ForwardMonotoneConvex',
                'LogLinearInterpolation',	'LogLinear',
                'StaircaseInterpolation',	'Staircase',
                'Linear'
            ) AS "interpolation",
            eodso."BusinessValidFrom" AS "observationDate",
            "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
                spread."TimeToMaturity",
                mcp."cyclePeriod"
                ) AS "term",
            TO_DECIMAL("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard"(
                eodso."Close",
                'Close',
                kv."Value"
                ), 15, 11) AS "rate",
            ROW_NUMBER() OVER (
            	PARTITION BY
            		sc."SpreadCurveID",
            		CONCAT(spread."TimeToMaturity", spread."TimeToMaturityUnit")
                ORDER BY
                    dp."Priority"
            ) AS "Prio"

        FROM "com.adweko.adapter.osx.synonyms::SpreadCurve_View" (:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) sc

        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::SpreadCurveToSpreadAssignment_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) sctsa
                ON sctsa."_SpreadCurve_SpreadCurveID" = sc."SpreadCurveID"
        LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::Spread_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) spread
                ON spread."SpreadID" = sctsa."_Spread_SpreadID"
        INNER JOIN "com.adweko.adapter.osx.synonyms::EndOfDaySpreadObservation_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) eodso
                ON eodso."_Spread_SpreadID" = spread."SpreadID"
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
                ON  mcp."BusinesscyclePeriod" = spread."TimeToMaturityUnit"
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
                ON  kv."KeyID" = 'SAPPercentageStandard'
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_DataProvider_View"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS dp
                ON dp."marketPriceKind" = 'SpreadCurve'
                AND  IFNULL(TRIM(dp."PriceDataProvider"),'') = IFNULL(TRIM(eodso."SpreadDataProvider"),'')
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_marketdataIdentifierCode_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) osxid
                ON osxid."fsdmID" = sc."SpreadCurveID"
                AND osxid."marketPriceKind" = 'SpreadCurve'
        INNER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS kv_hm
                ON kv_hm."KeyID" = 'SpreadCurveRead'
                AND kv_hm."Value" = '1' --EndOfDaySpreadObservation
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_unitFilter_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS uf
                ON uf."StreamName" = 'SpreadCurve'
        WHERE IFNULL(TRIM(dp."PriceDataProvider"), '')	= IFNULL(TRIM(eodso."SpreadDataProvider"), '')
                AND IFNULL(TRIM(dp."PriceDataProvider"), '') = IFNULL(TRIM(sc."ProviderOfSpreadCurve"), '')
                AND IFNULL(TRIM(sctsa."_SpreadCurve_ProviderOfSpreadCurve"), '') = IFNULL(TRIM(sc."ProviderOfSpreadCurve"), '')
                AND (uf."UnitFilter" = spread."TimeToMaturityUnit" OR uf."UnitFilter" IS NULL)
    )
    WHERE "Prio" = 1
			
	UNION ALL

	/*********************************
        EOD Spread Curve Data Points
    *********************************/
	SELECT
		"riskFactorDefinition",
		"interpolation",
		"observationDate",
		"term",
		"rate"
    FROM (
        SELECT DISTINCT
            COALESCE(osxid."osxID", sc."SpreadCurveID") AS "riskFactorDefinition",
            MAP(sc."InterpolationMethod",
                'LinearInterpolation',	'Linear',
                'CubicSplineInterpolation',	'CubicSpline',
                'ForwardMonotoneConvexInterpolation',	'ForwardMonotoneConvex',
                'LogLinearInterpolation',	'LogLinear',
                'StaircaseInterpolation',	'Staircase',
                'Linear'
            ) AS "interpolation",
            eoddp."BusinessValidFrom" AS "observationDate",
            "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
                eoddp."TimeToMaturity",
                mcp."cyclePeriod"
                ) AS "term",
            TO_DECIMAL("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard"(
                eoddp."Close",
                'Close',
                kv."Value"
                ), 15, 11) AS "rate",
            ROW_NUMBER() OVER (
            	PARTITION BY
            		sc."SpreadCurveID",
            		CONCAT(eoddp."TimeToMaturity", eoddp."TimeToMaturityUnit")
                ORDER BY
                    dp."Priority"
            ) AS "Prio"

        FROM "com.adweko.adapter.osx.synonyms::SpreadCurve_View" (:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) sc

        INNER JOIN "com.adweko.adapter.osx.synonyms::EndOfDaySpreadCurveDataPoint_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) eoddp ON
                eoddp."_SpreadCurve_SpreadCurveID" = sc."SpreadCurveID"
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
                ON  mcp."BusinesscyclePeriod" = eoddp."TimeToMaturityUnit"
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
                ON  kv."KeyID" = 'SAPPercentageStandard'
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_marketdataIdentifierCode_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) osxid
                ON osxid."fsdmID" = sc."SpreadCurveID"
                AND osxid."marketPriceKind" = 'SpreadCurve'
        INNER JOIN "com.adweko.adapter.osx.mappings::map_DataProvider_View"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS dp
                ON  dp."marketPriceKind" = 'SpreadCurve'
                AND	IFNULL(TRIM(dp."PriceDataProvider"), '') = IFNULL(TRIM(sc."ProviderOfSpreadCurve"), '')
                AND IFNULL(TRIM(dp."PriceDataProvider"), '') = IFNULL(TRIM(eoddp."_SpreadCurve_ProviderOfSpreadCurve"), '')
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_unitFilter_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS uf
                ON uf."StreamName" = 'SpreadCurve'
        INNER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS kv_hm
                ON kv_hm."KeyID" = 'SpreadCurveRead'
                AND kv_hm."Value" = '2' --EndOfDaySpreadCurveDataPoint;
                AND (uf."UnitFilter" = eoddp."TimeToMaturityUnit" OR uf."UnitFilter" IS NULL)
	)
    WHERE "Prio" = 1;
END;
