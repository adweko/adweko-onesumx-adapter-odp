FUNCTION "com.adweko.adapter.osx.inputdata.marketdata.macroeconomicfactors::MacroEconomicFactor_Marketdata" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP
		)
RETURNS TABLE (
		"riskFactorDefinition" NVARCHAR(75),
		"observationDate" DATE,
		"value" DECIMAL(34,11)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN
	SELECT	
		CAST((COALESCE(osxid."osxID", "mfID")) AS NVARCHAR(75)) AS "riskFactorDefinition",
		"observationDate",
		"value"
	FROM(
		SELECT DISTINCT
			mf."Type" || '-' || mf."CalculationPeriodID" AS "mfID",
	    	mf."PublicationDate" AS "observationDate",
	    	TO_DECIMAL(COALESCE("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard"(mf."Rate", 'Rate', kv."Value"), mf."Amount"), 34, 11) AS "value"
	    	
    	FROM "com.adweko.adapter.osx.synonyms::MacroeconomicFactor_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mf
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS kv
    		ON kv."KeyID" = 'SAPPercentageStandard'
   		
   		WHERE IFNULL(TRIM(mf."_Segment_SegmentID"), '') = ''
   			AND IFNULL(TRIM(mf."Provider"), '') = ''
    )
    LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_marketdataIdentifierCode_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) osxid
        ON osxid."fsdmID" = "mfID"
        AND osxid."marketPriceKind" = 'MacroEconomicFactor';
END;
	
