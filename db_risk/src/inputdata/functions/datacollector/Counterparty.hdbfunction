FUNCTION "com.adweko.adapter.osx.datacollector::Counterparty" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP DEFAULT '9999-01-01',
		I_DATAGROUP_STRING NVARCHAR (128) DEFAULT ''
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"partyKey" NVARCHAR(256),
		"sourceSystemName" NVARCHAR(128),
		"dataGroup" NVARCHAR(128),
		"partyLegalName" NVARCHAR(256),
		"description" NVARCHAR(40),
		"probabilityOfDefault" DECIMAL(15, 11),
		"isSovereign" INT,
		"countryOfLegalDomicileType" NVARCHAR(2),
		"currency" NVARCHAR(3),
		"yearlyRevenue" DECIMAL(34, 6),
		"totalSales" DECIMAL(34, 6),
		"segment" NVARCHAR(40),
		"riskSensitivityProfile" NVARCHAR(40),
		"crCadCounterpartyClass" NVARCHAR(40),
		"mrCadCounterpartyClass" NVARCHAR(40),
		"countryOperation" NVARCHAR(2),
		"isSovereignEquivalent" NVARCHAR(5),
		"sectorCode" NVARCHAR(40),
		"specialCounterpartyType" NVARCHAR(40),
		"isRegulatedInstitution" INT,
		"liquiditySubType" NVARCHAR(40),
		"isEstablishedRelationship" INT,
		"selSystemTimestamp" TIMESTAMP,
		"hasFiscalAutonomy" INT,
		"leiCode" NVARCHAR(256),
		"ultimateParent" NVARCHAR(128),
		"nationalIdentifierCode" NVARCHAR(128),
		"isNonPerforming" INT,
		"totalAssets" DECIMAL(34, 6),
		"eligibleType" NVARCHAR(75),
		"counterpartySubClass" NVARCHAR(255),
		"tier1LeverageRatio" DECIMAL(15, 11),
		"commonEquityTier1Ratio" DECIMAL(15, 11),
		"isInvestmentGrade" NVARCHAR(75),
		"sourceOfIncomeCurrency" NVARCHAR(3),
		"transactor" NVARCHAR(5)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	SELECT "id",
		"partyKey",
		"sourceSystemName",
		"dataGroup",
		"partyLegalName",
		"description",
		"probabilityOfDefault",
		"isSovereign",
		"countryOfLegalDomicileType",
		"currency",
		"yearlyRevenue",
		"totalSales",
		"segment",
		"riskSensitivityProfile",
		"crCadCounterpartyClass",
		"mrCadCounterpartyClass",
		"countryOperation",
		"isSovereignEquivalent",
		"sectorCode",
		"specialCounterpartyType",
		"isRegulatedInstitution",
		"liquiditySubType",
		"isEstablishedRelationship",
		CASE 
			WHEN :I_SYSTEM_TIMESTAMP = '9999-01-01'
				THEN now()
			ELSE :I_SYSTEM_TIMESTAMP
			END AS "selSystemTimestamp",
		"hasFiscalAutonomy",
		"leiCode",
		"ultimateParent",
		"nationalIdentifierCode",
		"isNonPerforming",
		"totalAssets",
		"eligibleType",
		"counterpartySubClass",
		"tier1LeverageRatio",
		"commonEquityTier1Ratio",
		"isInvestmentGrade",
		"sourceOfIncomeCurrency",
		"transactor"
	FROM "com.adweko.adapter.osx.inputdata.partner::Counterparty" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			);
END;
