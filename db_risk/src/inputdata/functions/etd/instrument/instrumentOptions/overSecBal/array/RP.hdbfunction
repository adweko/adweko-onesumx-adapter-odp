FUNCTION "com.adweko.adapter.osx.inputdata.instrumentOptions.overSecBal.array::RP" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"refid" NVARCHAR(32),
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),
		"dataGroup" NVARCHAR(128),
		"repricingType" NVARCHAR(1),
		"pricingMarketObject" NVARCHAR(40),
		"rateAdd" DECIMAL(15, 11),
		"rateMultiplier" DECIMAL(15, 11),
		"rateTerm" NVARCHAR(10),
		"fixingDays" NVARCHAR(10),
		"cap" DECIMAL(15, 11),
		"floor" DECIMAL(15, 11),
		"Prio" INT

		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	SELECT
		"com.adweko.adapter.osx.inputdata.common::get_array_id" (
			"com.adweko.adapter.osx.inputdata.common::get_id" (
				root."contractID",
				root."node_No",
				root."dataGroup"
			),
			"com.adweko.adapter.osx.inputdata.common::get_RP_or_RPS" (
				cfss."Value", inte."FloatingRateMax", inte."FloatingRateMin", inte."VariableRateMax", inte."VariableRateMin"
			),
			"com.adweko.adapter.osx.inputdata.common::get_RP_cycleDate" (
				i_InterestSpecificationCategory			=> inte."InterestSpecificationCategory",
				i_FixingRateSpecificationCategory		=> inte."FixingRateSpecificationCategory",
				i_ResetAtMonthUltimo					=> inte."ResetAtMonthUltimo",
				i_RelativeToInterestPeriodStartOrEnd	=> inte."RelativeToInterestPeriodStartOrEnd",
				i_FirstInterestPeriodEndDate			=> inte."FirstInterestPeriodEndDate",
				i_FirstRegularFloatingRateResetDate		=> inte."FirstRegularFloatingRateResetDate",
				i_FirstInterestPeriodStartDate			=> inte."FirstInterestPeriodStartDate"
			)	-- =cycleDate"
		) AS "id",
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			root."node_No",
			root."dataGroup"
		) AS "refid",
		"com.adweko.adapter.osx.inputdata.common::get_RP_or_RPS" (
			cfss."Value", inte."FloatingRateMax", inte."FloatingRateMin", inte."VariableRateMax", inte."VariableRateMin"
		) AS "arraySpecKind",
		"com.adweko.adapter.osx.inputdata.common::get_RP_cycleDate" (
				i_InterestSpecificationCategory			=> inte."InterestSpecificationCategory",
				i_FixingRateSpecificationCategory		=> inte."FixingRateSpecificationCategory",
				i_ResetAtMonthUltimo					=> inte."ResetAtMonthUltimo",
				i_RelativeToInterestPeriodStartOrEnd	=> inte."RelativeToInterestPeriodStartOrEnd",
				i_FirstInterestPeriodEndDate			=> inte."FirstInterestPeriodEndDate",
				i_FirstRegularFloatingRateResetDate		=> inte."FirstRegularFloatingRateResetDate",
				i_FirstInterestPeriodStartDate			=> inte."FirstInterestPeriodStartDate"
		) AS "cycleDate",
		
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
				MAP(inte."InterestSpecificationCategory",
					'FixedRateSpecification', inte."InterestPeriodLength",
					'FloatingRateSpecification', 
						MAP(inte."FixingRateSpecificationCategory",
							'IndependentFloatingRateSpecification',	inte."ResetPeriodLength",
							'DependentFloatingRateSpecification',	inte."InterestPeriodLength"
						)
				),
				mcp."cyclePeriod"
		) AS "cyclePeriod",
		root."dataGroup",
		"com.adweko.adapter.osx.inputdata.common::get_repricingType"(
			'',
			inte."InterestSpecificationCategory",
			inte."FixingRateSpecificationCategory",
			inte."ResetInArrears",
			inte."ResetAtMonthUltimo",
			inte."CutoffRelativeToDate"
			) AS "repricingType",
		MAP(kvMonetaryRef."Value",
			'On', CASE WHEN inte."InterestIsCompounded" = true
					THEN root."currency"
							|| '-' 
							|| pmo."pricingMarketObject"
							|| '-'
							|| "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
									MAP(inte."InterestSpecificationCategory",
										'FixedRateSpecification', inte."InterestPeriodLength",
										'FloatingRateSpecification', 
											MAP(inte."FixingRateSpecificationCategory",
												'IndependentFloatingRateSpecification',	inte."ResetPeriodLength",
												'DependentFloatingRateSpecification',	inte."InterestPeriodLength"
										)
									),
									mcp."cyclePeriod"
								)
							|| '-MR'
					ELSE pmo."pricingMarketObject" END,
			'Off', pmo."pricingMarketObject") AS "pricingMarketObject",
		MAP(inte."InterestSpecificationCategory",
			'FloatingRateSpecification', inte."Spread",
			'FixedRateSpecification', inte."FixedRate"
		) AS "rateAdd",
		MAP(inte."InterestSpecificationCategory",
			'FloatingRateSpecification', inte."ReferenceRateFactor",
			NULL
		) AS "rateMultiplier",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			referenceRate."ReferenceRate_TimeToMaturity",
			mcp_rr."cyclePeriod"
		) AS "rateTerm",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			inte."ResetLagLength",
			mcp_reset."cyclePeriod"
		) AS "fixingDays",
		"com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", inte."FloatingRateMax", inte."VariableRateMax") AS "cap",
		"com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", inte."FloatingRateMin", inte."VariableRateMin") AS "floor",
		ROW_NUMBER() OVER (
			PARTITION BY
				"com.adweko.adapter.osx.inputdata.common::get_id"(
					root."contractID",
					root."node_No",
					root."dataGroup") --="refid"
			ORDER BY
				"com.adweko.adapter.osx.inputdata.common::get_RP_cycleDate" (
					i_InterestSpecificationCategory			=> inte."InterestSpecificationCategory",
					i_FixingRateSpecificationCategory		=> inte."FixingRateSpecificationCategory",
					i_ResetAtMonthUltimo					=> inte."ResetAtMonthUltimo",
					i_RelativeToInterestPeriodStartOrEnd	=> inte."RelativeToInterestPeriodStartOrEnd",
					i_FirstInterestPeriodEndDate			=> inte."FirstInterestPeriodEndDate",
					i_FirstRegularFloatingRateResetDate		=> inte."FirstRegularFloatingRateResetDate",
					i_FirstInterestPeriodStartDate			=> inte."FirstInterestPeriodStartDate"
				) ASC -- =cycleDate" 
		) AS "Prio"
		
	FROM "com.adweko.adapter.osx.inputdata.instrumentOptions.overSecBal::InstrumentOptions_Root_Common_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS root
    
    INNER JOIN "com.adweko.adapter.osx.synonyms::FinancialInstrument_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) ufi
		ON	ufi."FinancialInstrumentID"		= root."FinancialInstrument_Underlying_FinancialInstrumentID"
		AND	ufi."SecurityCategory"			= 'DebtInstrument'

	INNER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvInterestType
		ON kvInterestType."KeyID" = 'InstrumentETDerivative_InterestType'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP)  AS cfss
		ON cfss."KeyID" = 'CapFloorSwitch'
		
	INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inte
        ON inte."_DebtInstrument_FinancialInstrumentID"	= ufi."FinancialInstrumentID"
			AND inte."InterestCategory"					= 'InterestPeriodSpecification'
			AND inte."InterestType" 					= kvInterestType."Value"
			AND ( 
				inte."LastInterestPeriodEndDate"		> :I_BUSINESS_DATE
				OR
				inte."LastInterestPeriodEndDate"		IS NULL
			)
			AND (
					inte."InterestSpecificationCategory"	= 'FixedRateSpecification'
				AND inte."InterestPeriodLength" 			IS NOT NULL
				AND inte."InterestPeriodTimeUnit"			IS NOT NULL
				
				OR	inte."InterestSpecificationCategory"	= 'FloatingRateSpecification'
				AND inte."FixingRateSpecificationCategory"	= 'DependentFloatingRateSpecification'
				AND inte."InterestPeriodLength" 			IS NOT NULL
				AND inte."InterestPeriodTimeUnit"			IS NOT NULL
				
				OR	inte."InterestSpecificationCategory"	= 'FloatingRateSpecification'
				AND inte."FixingRateSpecificationCategory"	= 'IndependentFloatingRateSpecification'
				AND inte."ResetPeriodLength"				IS NOT NULL
				AND inte."ResetPeriodTimeUnit"				IS NOT NULL
			)
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ReferenceRate_Simple"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS referenceRate 
			ON referenceRate."ReferenceRate_ReferenceRateID" = inte."ReferenceRateID"
				
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" 
			= 
			MAP(inte."InterestSpecificationCategory",
				'FixedRateSpecification', inte."InterestPeriodTimeUnit",
				'FloatingRateSpecification', 
					MAP(inte."FixingRateSpecificationCategory",
						'IndependentFloatingRateSpecification',	inte."ResetPeriodTimeUnit",
						'DependentFloatingRateSpecification',	inte."InterestPeriodTimeUnit"
					)
			)
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS mcp_rr
		ON  mcp_rr."BusinesscyclePeriod" = referenceRate."ReferenceRate_TimeToMaturityUnit"
			
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS mcp_reset
		ON  mcp_reset."BusinesscyclePeriod" = inte."ResetLagTimeUnit"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_pricingMarketObject_View" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS pmo 
		ON pmo."ReferenceRateID" = inte."ReferenceRateID"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
	
	WHERE
			root."FinancialInstrument_Underlying_SecurityCategory"	= 'DebtInstrument'
		AND root."withPayout" IS NULL
	;
END;