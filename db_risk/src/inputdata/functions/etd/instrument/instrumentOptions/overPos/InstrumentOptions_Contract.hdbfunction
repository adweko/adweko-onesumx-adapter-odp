FUNCTION "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos::InstrumentOptions_Contract"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP, 
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"settlementDate" DATE,
		"counterparty" NVARCHAR(128),
		"currentPrincipal" DECIMAL(34, 6),
		"futureQuantity" DECIMAL(34, 6),
		"currency" NVARCHAR(3),
		"isin" NVARCHAR(12),
		"priceAtCDD" DECIMAL(34, 6),
		"optionDeliverySettlement" NVARCHAR(1),
		"legalEntity" NVARCHAR(128),
		"currentNominalInterestRate" DECIMAL(15, 11),
		"clearingHouse" NVARCHAR(128),
		"isUnderLiquidityManagementControl" INT,
		"lmEntityName" NVARCHAR(128),
		"lmEntityDetail" NVARCHAR(128),
		"dataGroup" NVARCHAR(128),
		"priceAtTD" DECIMAL(34, 6),
		"optionHolder" NVARCHAR(1),
		"optionExecutionType" NVARCHAR(1),
		"optionType" NVARCHAR(1),
		"exerciseBeginDate" DATE,
		"exerciseEndDate" DATE,
		"optionQuantity" DECIMAL(34, 6),
		"optionStrikePut" DECIMAL(34, 11),
		"optionStrikeCall" DECIMAL(34, 11),
		"marketValueObserved" DECIMAL(34, 6),
		"underlyingMarketValueObserved" DECIMAL(34, 6),
		"marketValueDate" DATE,
		"issuer" NVARCHAR(128),
		"quantity" DECIMAL(34, 6),
		"valueDate" DATE,
		"conversionFactor" DECIMAL(38, 11),
		"futureDeliverySettlement" NVARCHAR(1),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128)
		
    	) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	 SELECT 
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			root."node_No",
			root."dataGroup"
			) AS "id",
		root."contractID" AS "sourceSystemRecordNumber",
		root."sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		root."node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		root."FinancialInstrument_IssueDate" AS "dealDate",
		root."Position_ExpirationDate" AS "settlementDate",
		root."counterparty",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'DebtInstrument',
				ABS(
					root."Position_LSN" * root."EndOfDayPriceObservation_Underlying_Close" * root."FinancialInstrument_NotionalPerContract" * root."FinancialInstrument_NumberOfInstrumentsPerContract"
				) * root."isActiveSign"
		) AS "currentPrincipal",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'DebtInstrument', root."FinancialInstrument_NumberOfInstrumentsPerContract"
		) AS "futureQuantity",
		root."currency",
		root."isin",
		ABS(root."EndOfDayListedDerivativePriceObservation_Close4IssueDate") * root."isActiveSign" AS "priceAtCDD",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'EquityInstrument',
				MAP(root."FinancialInstrument_SettlementMethod",
					'PhysicalDelivery','1',
					'CashSettlement','2',
					'Auction','2')
		) AS "optionDeliverySettlement",
		MAP(root."legalEntitySwitch",
			'OrganizationalUnitContractAssignment', root."OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",
			'BusinessPartnerContractAssignment',  root."ContractDataOwner_BusinessPartnerID"
		) AS "legalEntity",
		inte."currentNominalInterestRate",
		root."clearingHouse",
		1 AS "isUnderLiquidityManagementControl",
		'Option' AS "lmEntityName",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'DebtInstrument',
				MAP(root."withPayout",
					1,'Bond',
					'Future'),
			'EquityInstrument','Equity'
		) AS "lmEntityDetail",
		root."dataGroup",
		root."Position_OptionPremium" AS "priceAtTD",
		MAP(root."isActiveSign",
			1,'1',
			-1,'2'
		) AS "optionHolder",
		MAP(root."FinancialInstrument_ExerciseStyle",
			'European','1',
			'American','2',
			'Bermudan','3'
		) AS "optionExecutionType",
		MAP(root."Position_PutOrCall",
			'Put','2',
			'Call','1'
		) AS "optionType",
		root."Position_BusinessValidFrom" AS "exerciseBeginDate",
		root."Position_ExpirationDate" AS "exerciseEndDate",
		root."Position_LSN" AS "optionQuantity",
		MAP(root."Position_PutOrCall",'Put',root."Position_StrikePrice") AS "optionStrikePut",
		MAP(root."Position_PutOrCall",'Call',root."Position_StrikePrice") AS "optionStrikeCall",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'EquityInstrument',
				ABS(
					root."Position_LSN" * root."EndOfDayPriceObservation_Close" * root."FinancialInstrument_NotionalPerContract"
				) * root."isActiveSign"
		) AS "marketValueObserved",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'EquityInstrument',
				ABS(
					root."EndOfDayPriceObservation_Underlying_Close" * root."FinancialInstrument_NotionalPerContract" * root."FinancialInstrument_NumberOfInstrumentsPerContract"
				) * root."isActiveSign"
		) AS "underlyingMarketValueObserved",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'EquityInstrument',root."EndOfDayPriceObservation_BusinessValidFrom" 
		) AS "marketValueDate",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'EquityInstrument', root."FinancialInstrument_Underlying_IssuerOfSecurity_BusinessPartnerID"
		) AS "issuer",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'EquityInstrument', root."FinancialInstrument_NumberOfInstrumentsPerContract"
		) AS "quantity",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'DebtInstrument',root."FinancialInstrument_Underlying_IssueDate" 
		) AS "valueDate",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'DebtInstrument',1.00000000000 
		) AS "conversionFactor",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'DebtInstrument',
				MAP(root."FinancialInstrument_SettlementMethod",
					'PhysicalDelivery','1',
					'CashSettlement','2')
		) AS "futureDeliverySettlement",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem"

    FROM "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos::InstrumentOptions_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS root
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos::InstrumentOptions_Interest_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS inte
		ON  inte."contractID"	= root."contractID"
	;
END;