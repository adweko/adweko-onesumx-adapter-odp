FUNCTION "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos::InstrumentOptions_Array"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP, 
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"refid" NVARCHAR(32),
		"arraySpecKind" NVARCHAR(5),
		"dataGroup" NVARCHAR(128),
		"ratingClass" NVARCHAR(10),
		"ratingScale" NVARCHAR(75),
		"amount" DECIMAL(34, 11),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),
		"repricingType" NVARCHAR(1),
		"pricingMarketObject" NVARCHAR(40),
		"rateAdd" DECIMAL(15, 11),
		"rateMultiplier" DECIMAL(15, 11),
		"rateTerm" NVARCHAR(10),
		"fixingDays" NVARCHAR(10),
		"cap" DECIMAL(15, 11),
		"floor" DECIMAL(15, 11),
		"indexBeta" DECIMAL(15, 11),
		"ppAmount" DECIMAL(34, 6)
		
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS 
BEGIN 

RETURN

	SELECT
		"id",
		"refid",
		"arraySpecKind",
		"dataGroup",
		"ratingClass",
		"ratingScale",
		TO_DECIMAL(NULL, 34, 11) AS "amount",
		NULL AS "cycleDate",
		NULL AS "cyclePeriod",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		TO_DECIMAL(NULL, 15, 11) AS "rateAdd",
		TO_DECIMAL(NULL, 15, 11) AS "rateMultiplier",
		NULL AS "rateTerm",
		NULL AS "fixingDays",
		TO_DECIMAL(NULL, 15, 11) AS "cap",
		TO_DECIMAL(NULL, 15, 11) AS "floor",
		TO_DECIMAL(NULL, 15, 11) AS "indexBeta",
		TO_DECIMAL(NULL, 34, 6) AS "ppAmount"
		
	FROM "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos.array::CRTNG"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
	
	UNION ALL
	
	SELECT
		"id",
		"refid",
		"arraySpecKind",
		"dataGroup",
		NULL AS "ratingClass",
		NULL AS "ratingScale",
		"amount",
		NULL AS "cycleDate",
		NULL AS "cyclePeriod",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		TO_DECIMAL(NULL, 15, 11) AS "rateAdd",
		TO_DECIMAL(NULL, 15, 11) AS "rateMultiplier",
		NULL AS "rateTerm",
		NULL AS "fixingDays",
		TO_DECIMAL(NULL, 15, 11) AS "cap",
		TO_DECIMAL(NULL, 15, 11) AS "floor",
		TO_DECIMAL(NULL, 15, 11) AS "indexBeta",
		TO_DECIMAL(NULL, 34, 6) AS "ppAmount"
	FROM "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos.array::PDEF"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
	
	UNION ALL
	
	SELECT
		"id",
		"refid",
		"arraySpecKind",
		"dataGroup",
		NULL AS "ratingClass",
		NULL AS "ratingScale",
		"amount",
		NULL AS "cycleDate",
		NULL AS "cyclePeriod",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		TO_DECIMAL(NULL, 15, 11) AS "rateAdd",
		TO_DECIMAL(NULL, 15, 11) AS "rateMultiplier",
		NULL AS "rateTerm",
		NULL AS "fixingDays",
		TO_DECIMAL(NULL, 15, 11) AS "cap",
		TO_DECIMAL(NULL, 15, 11) AS "floor",
		TO_DECIMAL(NULL, 15, 11) AS "indexBeta",
		TO_DECIMAL(NULL, 34, 6) AS "ppAmount"
	FROM "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos.array::LOSS"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
	
	UNION ALL
	
	SELECT
		"id",
		"refid",
		"arraySpecKind",
		"dataGroup",
		NULL AS "ratingClass",
		NULL AS "ratingScale",
		TO_DECIMAL(NULL, 34, 11) AS "amount",
		"cycleDate",
		"cyclePeriod",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		TO_DECIMAL(NULL, 15, 11) AS "rateAdd",
		TO_DECIMAL(NULL, 15, 11) AS "rateMultiplier",
		NULL AS "rateTerm",
		NULL AS "fixingDays",
		TO_DECIMAL(NULL, 15, 11) AS "cap",
		TO_DECIMAL(NULL, 15, 11) AS "floor",
		TO_DECIMAL(NULL, 15, 11) AS "indexBeta",
		TO_DECIMAL(NULL, 34, 6) AS "ppAmount"
	FROM "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos.array::IP_afterStub"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
	WHERE "Prio" = 1
	
	UNION ALL
	
	SELECT
		"id",
		"refid",
		"arraySpecKind",
		"dataGroup",
		NULL AS "ratingClass",
		NULL AS "ratingScale",
		TO_DECIMAL(NULL, 34, 11) AS "amount",
		"cycleDate",
		"cyclePeriod",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		TO_DECIMAL(NULL, 15, 11) AS "rateAdd",
		TO_DECIMAL(NULL, 15, 11) AS "rateMultiplier",
		NULL AS "rateTerm",
		NULL AS "fixingDays",
		TO_DECIMAL(NULL, 15, 11) AS "cap",
		TO_DECIMAL(NULL, 15, 11) AS "floor",
		TO_DECIMAL(NULL, 15, 11) AS "indexBeta",
		TO_DECIMAL(NULL, 34, 6) AS "ppAmount"
	FROM "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos.array::IP_stubAndRegular"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
	WHERE "Prio" = 1

	UNION ALL
	
	SELECT
		"id",
		"refid",
		"arraySpecKind",
		"dataGroup",
		NULL AS "ratingClass",
		NULL AS "ratingScale",
		TO_DECIMAL(NULL, 34, 11) AS "amount",
		"cycleDate",
		"cyclePeriod",
		"repricingType",
		"pricingMarketObject",
		"rateAdd",
		"rateMultiplier",
		"rateTerm",
		"fixingDays",
		"cap",
		"floor",
		TO_DECIMAL(NULL, 15, 11) AS "indexBeta",
		TO_DECIMAL(NULL, 34, 6) AS "ppAmount"
	FROM "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos.array::RP"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
	WHERE "Prio" = 1
	
	UNION ALL
	
	SELECT
		"id",
		"refid",
		"arraySpecKind",
		"dataGroup",
		NULL AS "ratingClass",
		NULL AS "ratingScale",
		TO_DECIMAL(NULL, 34, 11) AS "amount",
		NULL AS "cycleDate",
		NULL AS "cyclePeriod",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		TO_DECIMAL(NULL, 15, 11) AS "rateAdd",
		TO_DECIMAL(NULL, 15, 11) AS "rateMultiplier",
		NULL AS "rateTerm",
		NULL AS "fixingDays",
		TO_DECIMAL(NULL, 15, 11) AS "cap",
		TO_DECIMAL(NULL, 15, 11) AS "floor",
		"indexBeta",
		TO_DECIMAL(NULL, 34, 6) AS "ppAmount"
		
	FROM "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos.array::SCIX"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
	
	UNION ALL
	
	SELECT
		"id",
		"refid",
		"arraySpecKind",
		"dataGroup",
		NULL AS "ratingClass",
		NULL AS "ratingScale",
		TO_DECIMAL(NULL, 34, 11) AS "amount",
		"cycleDate",
		"cyclePeriod",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		TO_DECIMAL(NULL, 15, 11) AS "rateAdd",
		TO_DECIMAL(NULL, 15, 11) AS "rateMultiplier",
		NULL AS "rateTerm",
		NULL AS "fixingDays",
		TO_DECIMAL(NULL, 15, 11) AS "cap",
		TO_DECIMAL(NULL, 15, 11) AS "floor",
		TO_DECIMAL(NULL, 15, 11) AS "indexBeta",
		"ppAmount"
		
	FROM "com.adweko.adapter.osx.inputdata.instrumentOptions.overPos.array::PP"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING)
	;
END;