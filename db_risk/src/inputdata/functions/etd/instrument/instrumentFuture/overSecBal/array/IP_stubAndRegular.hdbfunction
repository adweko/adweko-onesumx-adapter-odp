FUNCTION "com.adweko.adapter.osx.inputdata.instrumentFuture.overSecBal.array::IP_stubAndRegular" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"refid" NVARCHAR(32),
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),
		"dataGroup" NVARCHAR(128),
		"Prio" INT
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	SELECT
		"com.adweko.adapter.osx.inputdata.common::get_array_id" (
			"com.adweko.adapter.osx.inputdata.common::get_id" (
				root."contractID",
				root."node_No",
				root."dataGroup"
			),
			'IP',
			inte."FirstInterestPeriodStartDate" -- =cycleDate"
		) AS "id",
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			root."node_No",
			root."dataGroup"
		) AS "refid",
		'IP' AS "arraySpecKind",
		inte."FirstInterestPeriodStartDate" AS "cycleDate",
		CASE 
			WHEN inte."FirstInterestPeriodEndDate" = inte."FirstInterestPeriodStartDate"
				THEN
					"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
							inte."InterestPeriodLength",
							mcp."cyclePeriod")
			ELSE '999Y' 
		END AS "cyclePeriod",
		root."dataGroup",
		ROW_NUMBER() OVER (
			PARTITION BY
				"com.adweko.adapter.osx.inputdata.common::get_id"(
					root."contractID",
					root."node_No",
					root."dataGroup") --="refid"
			ORDER BY
				inte."FirstInterestPeriodStartDate" ASC -- =cycleDate" 
		) AS "Prio"

	FROM "com.adweko.adapter.osx.inputdata.instrumentFuture.overSecBal::InstrumentFuture_Root_Common_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS root
    
	INNER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvInterestType
		ON kvInterestType."KeyID" = 'InstrumentETDerivative_InterestType'
		
	INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inte
        ON inte."_DebtInstrument_FinancialInstrumentID"	= root."FinancialInstrument_Underlying_FinancialInstrumentID"
			AND inte."InterestCategory"					= 'InterestPeriodSpecification'
			AND inte."InterestType" 					= kvInterestType."Value"
			AND	inte."FirstInterestPeriodEndDate"		IS NOT NULL
			AND	(
				inte."LastInterestPeriodEndDate"		IS NULL
				OR
				inte."LastInterestPeriodEndDate"		> :I_BUSINESS_DATE
				AND inte."LastInterestPeriodEndDate"    > inte."FirstInterestPeriodEndDate"
			)
			AND inte."InterestPeriodLength"				IS NOT NULL
			AND inte."InterestPeriodTimeUnit"			IS NOT NULL
				
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" = inte."InterestPeriodTimeUnit"
	
	WHERE
		root."FinancialInstrument_Underlying_SecurityCategory"	= 'DebtInstrument'
	;
END;