FUNCTION "com.adweko.adapter.osx.inputdata.instrumentFuture.overSecBal::InstrumentFuture_Contract"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP, 
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"settlementDate" DATE,
		"counterparty" NVARCHAR(128),
		"currentPrincipal" DECIMAL(34, 6),
		"futureQuantity" DECIMAL(34, 6),
		"currency" NVARCHAR(3),
		"isin" NVARCHAR(12),
		"priceAtCDD" DECIMAL(34, 6),
		"legalEntity" NVARCHAR(128),
		"currentNominalInterestRate" DECIMAL(15, 11),
		"clearingHouse" NVARCHAR(128),
		"isUnderLiquidityManagementControl" INT,
		"lmEntityName" NVARCHAR(128),
		"lmEntityDetail" NVARCHAR(128),
		"discountRate" DECIMAL(15, 11),
		"dataGroup" NVARCHAR(128),
		"priceAtTD" DECIMAL(34, 6),
		"marketValueObserved" DECIMAL(34, 6),
		"underlyingMarketValueObserved" DECIMAL(34, 6),
		"marketValueDate" DATE,
		"issuer" NVARCHAR(128),
		"quantity" DECIMAL(34, 6),
		"valueDate" DATE,
		"conversionFactor" DECIMAL(38, 11),
		"futureDeliverySettlement" NVARCHAR(1),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128)
		
    	) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	 SELECT 
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			root."node_No",
			root."dataGroup"
			) AS "id",
		root."contractID" AS "sourceSystemRecordNumber",
		root."sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		root."node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		root."FinancialInstrument_IssueDate" AS "dealDate",
		root."FinancialInstrument_ExpirationDate" AS "settlementDate",
		root."counterparty",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'DebtInstrument',
				ABS(
					root."SecuritiesBalance_Quantity" * root."EndOfDayPriceObservation_Underlying_Close" * root."FinancialInstrument_NotionalPerContract" * root."FinancialInstrument_NumberOfInstrumentsPerContract"
				) * root."isActiveSign"
		) AS "currentPrincipal",
		root."SecuritiesBalance_Quantity" AS "futureQuantity",
		root."currency",
		root."isin",
		ABS(root."EndOfDayListedDerivativePriceObservation_Close4IssueDate") * root."isActiveSign" AS "priceAtCDD",
		MAP(root."legalEntitySwitch",
			'OrganizationalUnitContractAssignment', root."OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",
			'BusinessPartnerContractAssignment',  root."ContractDataOwner_BusinessPartnerID"
		) AS "legalEntity",
		inte."currentNominalInterestRate",
		root."clearingHouse",
		1 AS "isUnderLiquidityManagementControl",
		'Future' AS "lmEntityName",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'DebtInstrument','Bond',
			'EquityInstrument','Equity'
		) AS "lmEntityDetail",
		0.00000000000 AS "discountRate",
		root."dataGroup",
		NULL AS "priceAtTD",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'EquityInstrument',
				ABS(
					root."SecuritiesBalance_Quantity" * root."EndOfDayPriceObservation_Close" * root."FinancialInstrument_NotionalPerContract"
				) * root."isActiveSign"
		) AS "marketValueObserved",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'EquityInstrument',
				ABS(
					root."EndOfDayPriceObservation_Underlying_Close" * root."FinancialInstrument_NotionalPerContract" * root."FinancialInstrument_NumberOfInstrumentsPerContract"
				) * root."isActiveSign"
		) AS "underlyingMarketValueObserved",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'EquityInstrument',root."EndOfDayPriceObservation_BusinessValidFrom" 
		) AS "marketValueDate",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'EquityInstrument', root."FinancialInstrument_Underlying_IssuerOfSecurity_BusinessPartnerID"
		) AS "issuer",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'EquityInstrument', root."FinancialInstrument_NumberOfInstrumentsPerContract"
		) AS "quantity",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'DebtInstrument',root."FinancialInstrument_Underlying_IssueDate" 
		) AS "valueDate",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'DebtInstrument',1.00000000000 
		) AS "conversionFactor",
		MAP(root."FinancialInstrument_Underlying_SecurityCategory",
			'DebtInstrument',
				MAP(root."FinancialContract_SettlementMethod",
					'PhysicalDelivery','1',
					'CashSettlement','2')
		) AS "futureDeliverySettlement",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem"

    FROM "com.adweko.adapter.osx.inputdata.instrumentFuture.overSecBal::InstrumentFuture_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS root
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.instrumentFuture.overSecBal::InstrumentFuture_Interest_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS inte
		ON  inte."contractID"	= root."contractID"
	;
END;