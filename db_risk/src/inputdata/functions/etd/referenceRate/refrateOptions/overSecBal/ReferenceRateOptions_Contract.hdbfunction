FUNCTION "com.adweko.adapter.osx.inputdata.refrateOptions.overSecBal::ReferenceRateOptions_Contract"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP, 
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"settlementDate" DATE,
		"counterparty" NVARCHAR(128),
		"currentPrincipal" DECIMAL(34, 6),
		"futureQuantity" DECIMAL(34, 6),
		"currency" NVARCHAR(3),
		"isin" NVARCHAR(12),
		"optionDeliverySettlement" NVARCHAR(1),
		"legalEntity" NVARCHAR(128),
		"clearingHouse" NVARCHAR(128),
		"isUnderLiquidityManagementControl" INT,
		"lmEntityName" NVARCHAR(128),
		"lmEntityDetail" NVARCHAR(128),
		"dataGroup" NVARCHAR(128),
		"priceAtTD" DECIMAL(34, 6),
		"optionHolder" NVARCHAR(1),
		"optionExecutionType" NVARCHAR(1),
		"optionType" NVARCHAR(1),
		"exerciseBeginDate" DATE,
		"exerciseEndDate" DATE,
		"optionQuantity" DECIMAL(34, 6),
		"optionStrikePut" DECIMAL(34, 11),
		"optionStrikeCall" DECIMAL(34, 11),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
		"bookValue" DECIMAL(34, 6),
		"exposureClass" NVARCHAR(255)
		
    	) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	 SELECT 
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			root."node_No",
			root."dataGroup"
			) AS "id",
		root."contractID" AS "sourceSystemRecordNumber",
		root."sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		root."node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		root."FinancialInstrument_IssueDate" AS "dealDate",
		root."FinancialInstrument_ExpirationDate" AS "settlementDate",
		root."counterparty",
		ABS(
			root."SecuritiesBalance_Quantity" * root."EndOfDayRateObservation_Close" * root."FinancialInstrument_NotionalPerContract"
		) * root."isActiveSign" AS "currentPrincipal",
		root."SecuritiesBalance_Quantity" AS "futureQuantity",
		root."FinancialInstrument_NotionalPerContractCurrency" AS "currency",
		root."isin",
		MAP(root."FinancialInstrument_SettlementMethod",
			'PhysicalDelivery','1',
			'CashSettlement','2',
			'Auction','2'
		) AS "optionDeliverySettlement",
		MAP(root."legalEntitySwitch",
			'OrganizationalUnitContractAssignment', root."OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",
			'BusinessPartnerContractAssignment',  root."ContractDataOwner_BusinessPartnerID"
		) AS "legalEntity",
		root."clearingHouse",
		1 AS "isUnderLiquidityManagementControl",
		'Option' AS "lmEntityName",
		'Future' AS "lmEntityDetail",
		root."dataGroup",
		NULL AS "priceAtTD",
		root."optionHolder",
		MAP(root."FinancialInstrument_ExerciseStyle",
			'European','1',
			'American','2',
			'Bermudan','3'
		) AS "optionExecutionType",
		CASE
			WHEN root."BusinessPartnerContractAssignment_AccountHolder_ContractDataOwner" = true
				THEN root."optionHolder"
			WHEN root."BusinessPartnerContractAssignment_AccountHolder_ContractDataOwner" = false
				THEN 	
					MAP(root."optionHolder",
						'1','2',
						'2','1')
		END	AS "optionType",
		root."SecuritiesBalance_BusinessValidFrom" AS "exerciseBeginDate",
		root."FinancialInstrument_ExpirationDate" AS "exerciseEndDate",
		root."SecuritiesBalance_Quantity" AS "optionQuantity",
		CASE
			WHEN (
					root."BusinessPartnerContractAssignment_AccountHolder_ContractDataOwner" = true
				AND	root."SecuritiesBalance_PurchasePrice" < 0
			) OR (
					root."BusinessPartnerContractAssignment_AccountHolder_ContractDataOwner" = false
				AND	root."SecuritiesBalance_PurchasePrice" > 0
			)
				THEN root."SecuritiesBalance_PurchasePrice"
		END AS "optionStrikePut",
		CASE
			WHEN (
					root."BusinessPartnerContractAssignment_AccountHolder_ContractDataOwner" = true
				AND	root."SecuritiesBalance_PurchasePrice" > 0
			) OR (
					root."BusinessPartnerContractAssignment_AccountHolder_ContractDataOwner" = false
				AND	root."SecuritiesBalance_PurchasePrice" < 0
			)
				THEN root."SecuritiesBalance_PurchasePrice"
		END AS "optionStrikeCall",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",
		bok."BookValue_BookValue" AS "bookValue",
		expop."exposureClass"

    FROM "com.adweko.adapter.osx.inputdata.refrateOptions.overSecBal::ReferenceRateOptions_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS root
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP,'ReferenceRateOptions') AS expop 
		ON expop."CreditRiskExposure_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND expop."CreditRiskExposure_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
	
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
		ON bok."BookValue_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND bok."BookValue_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND bok."BookValue_FinancialInstrument_FinancialInstrumentID" = root."FinancialInstrument_FinancialInstrumentID"
		AND bok."BookValue_BookValueCurrency" = root."FinancialInstrument_NotionalPerContractCurrency"
	;
END;