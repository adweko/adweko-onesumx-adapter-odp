FUNCTION "com.adweko.adapter.osx.inputdata.account::Account_get_Root" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"FinancialContract_IDSystem" NVARCHAR(40),
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"dataGroup" NVARCHAR(128),
		"FinancialContract_SingleCurrencyAccountCategory" NVARCHAR (40),
		"FinancialContract_AccountCategory" NVARCHAR (40),
		"BusinessPartnerContractAssignment_AccountHolder_PartnerInParticipation_BusinessPartnerID" NVARCHAR(128),
		"ContractCurrency" NVARCHAR(3),
		"dealDate" DATE,
		"valueDate" DATE,
		"maturityDate" DATE,
		"terminationDate" DATE,
		"nonPerformingDate" DATE,
		"clearingHouse" NVARCHAR(128),
		"netRecoveryRate" DECIMAL (15,11),
		"ContractStatus_DefaultStatus_Status" NVARCHAR(100),
		"impairmentType" INT,
		"contractFSType" NVARCHAR(256),
		"contractFSSubType" NVARCHAR(256),
		"depositDistributionChannel" NVARCHAR(128),
		"productFSClass" NVARCHAR(128),
		"instrumentType" NVARCHAR(128),
		"financialInstrumentType" INT, 
		"operationalPercentage" DECIMAL(15, 11),
		"FinancialContract_SegregatedAccountBeneficiaryID" NVARCHAR(128),
		"FinancialContract_SegregatedAccountBeneficiaryName" NVARCHAR(256),
		"legalEntity" NVARCHAR(128),
		"isPerforming" INT,
		"FinancialContract_FixedDepositRepayableAmount" Decimal(34, 6),
		"FinancialContract_OriginalInvestmentAmount" Decimal(34, 6),
		"portfolioType"	NVARCHAR(255),
		"contractID" NVARCHAR(256),
        "currentPrincipal" DECIMAL(34, 6),
		"recoveredAmount" DECIMAL(34, 6),
		"pastDueAmount" DECIMAL(34, 6),
		"accruedInterest" DECIMAL(38, 11),
		"AccountBalance" DECIMAL(34, 6),
		"isGoldBullion" INT,
		"productType" NVARCHAR(128),
		"exposureClass" NVARCHAR(255)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************************************
        Build Account Granularity and map Master Fields
    ******************************************************/
	RETURN

	SELECT innerselect1."FinancialContract_IDSystem",
		innerselect1."FinancialContract_FinancialContractID",
		innerselect1."dataGroup",
		"FinancialContract_SingleCurrencyAccountCategory",
		"FinancialContract_AccountCategory",
		"BusinessPartnerContractAssignment_AccountHolder_PartnerInParticipation_BusinessPartnerID",
		"ContractCurrency",
		"dealDate",
		"valueDate",
		"maturityDate",
		"terminationDate",
		"nonPerformingDate",
		"clearingHouse",
		"netRecoveryRate",
		"ContractStatus_DefaultStatus_Status",
		"impairmentType",
		contractFSType."contractFSType" AS "contractFSType",
		NULL AS "contractFSSubType",
		"depositDistributionChannel",
		"com.adweko.adapter.osx.inputdata.account::Account_getProductFSClass"(
			productCatalogAssignment."StandardCatalog_ProductCatalogItem",
			innerselect1."AccountBalance",
			"FinancialContract_ForClientTrades"
		) AS "productFSClass",
		NULL AS "instrumentType",
		"financialInstrumentType", 
		CASE
			WHEN IFNULL (innerselect1."OperationalAmount",0) <> 0
			 AND IFNULL (innerselect1."AccountBalance",0) <> 0
				THEN TO_DECIMAL(ABS(innerselect1."OperationalAmount" / innerselect1."AccountBalance"), 15, 11)
			WHEN IFNULL (innerselect1."OperationalAmount",0) <> 0
			 AND IFNULL (innerselect1."AccountBalance",0) = 0
				THEN 1
		END AS "operationalPercentage",
		"FinancialContract_SegregatedAccountBeneficiaryID",
		"FinancialContract_SegregatedAccountBeneficiaryName",
		"legalEntity",
		"isPerforming",
		"FinancialContract_FixedDepositRepayableAmount",
		"FinancialContract_OriginalInvestmentAmount",
		portfolioType."portfolioType",
		"com.adweko.adapter.osx.inputdata.common::get_contractID"(
				'F',
				'',
				innerselect1."FinancialContract_FinancialContractID",
				innerselect1."FinancialContract_IDSystem",
				innerselect1."ContractCurrency"
			) AS "contractID",
		innerselect1."currentPrincipal",
		innerselect1."recoveredAmount",
		innerselect1."pastDueAmount",
		innerselect1."accruedInterest",
		innerselect1."AccountBalance",
		"isGoldBullion",
		"productType",
		"exposureClass"
		FROM (
		(
		SELECT DISTINCT
			    acc."FinancialContract_IDSystem",
				acc."FinancialContract_FinancialContractID",
				acc."dataGroup",
				"FinancialContract_SingleCurrencyAccountCategory",
				"FinancialContract_AccountCategory",
				"BusinessPartnerContractAssignment_AccountHolder_PartnerInParticipation_BusinessPartnerID",
				pos."ContractCurrency",
				"FinancialContract_ForClientTrades",
				"FinancialContract_OriginalSigningDate" AS "dealDate",
				CASE 
					WHEN "FinancialContract_AccountCategory" = 'MultiCurrencyAccount'
						THEN "FinancialContract_OpeningDate"
					WHEN "FinancialContract_AccountCategory" = 'SingleCurrencyAccount'
						AND "FinancialContract_SingleCurrencyAccountCategory" = 'FixedTermDeposit'
						THEN "FinancialContract_DepositStartDate"
					WHEN "FinancialContract_AccountCategory" = 'SingleCurrencyAccount'
						THEN "FinancialContract_OpeningDate"
					END AS "valueDate",
				CASE 
					WHEN "FinancialContract_AccountCategory" = 'SingleCurrencyAccount'
						AND "FinancialContract_SingleCurrencyAccountCategory" = 'FixedTermDeposit'
						THEN "FinancialContract_CurrentDepositEndDate"
					END AS "maturityDate",
				CASE 
					WHEN "FinancialContract_AccountCategory" = 'MultiCurrencyAccount'
						THEN "FinancialContract_ClosingDate"
					WHEN "FinancialContract_AccountCategory" = 'SingleCurrencyAccount'
						AND "FinancialContract_SingleCurrencyAccountCategory" <> 'FixedTermDeposit'
						THEN "FinancialContract_ClosingDate"
					END AS "terminationDate",
				CASE 
					WHEN "ContractStatus_PastDueStatus_Status" IN (
							'PastDueLongerThanDefaultRelevantThreshold',
							'PastDueNotLongerThanDefaultRelevantThreshold'
							)
						THEN "ContractStatus_PastDueStatus_BusinessValidFrom"
					ELSE NULL
					END AS "nonPerformingDate",
				"BusinessPartnerContractAssignment_ClearingMember_PartnerInParticipation_BusinessPartnerID" AS "clearingHouse",
-- CreditRisk
				CAST(
					CASE
						WHEN rec."CreditEnhancementStep" = 'After'
	    		        	THEN rec."CreditRiskRecoveryRate_RecoveryRate"
						ELSE NULL
					END 
					AS DECIMAL(15,11)
				) AS "netRecoveryRate",
				"ContractStatus_DefaultStatus_Status",
				MAP(cra_imp."CreditRiskAdjustment_CalculationMethod",
				'IndividuallyAssessed', 1,
				'CollectivelyAssessed', 2 ) AS "impairmentType",
			MAP("BankingChannel_BankingChannelCategory",
					'Terminal',					'DIRECT',
					'DigitalBankingPlatform',	MAP("DigitalBankingPlatform_Status",
													'Approved', 'INTERNET'),
					'FinancialAPI',				'INTERNET',
					'VoiceAccess',				'OTHER REMOTE',
					'Branch',					'POST OFFICE'
					) AS "depositDistributionChannel",
			NULL AS "instrumentType",
			"financialInstrumentType",
			"FinancialContract_SegregatedAccountBeneficiaryID",
			"FinancialContract_SegregatedAccountBeneficiaryName",
			CASE 
				WHEN (keylegalEntity."Value"='OrganizationalUnitContractAssignment' 
					AND "OrganizationalUnitContractAssignment_OrgUnit_OrganizationHostingOrganizationalUnit_BusinessPartnerID"<>' ')
					OR keylegalEntity."Value" IS NULL
						THEN "OrganizationalUnitContractAssignment_OrgUnit_OrganizationHostingOrganizationalUnit_BusinessPartnerID"
				WHEN keylegalEntity."Value"='BusinessPartnerContractAssignment'
					AND "BusinessPartnerContractAssignment_ContractDataOwner_PartnerInParticipation_BusinessPartnerID"<>' '
						THEN "BusinessPartnerContractAssignment_ContractDataOwner_PartnerInParticipation_BusinessPartnerID"
				ELSE
					NULL
			END AS "legalEntity",
			CASE
				WHEN acc."FinancialContract_FinancialContractCategory" = 'BankAccount'
					AND acc."FinancialContract_AccountCategory" = 'SingleCurrencyAccount'
					AND acc."FinancialContract_SingleCurrencyAccountCategory" = 'CurrentAccount'
						THEN "com.adweko.adapter.osx.inputdata.common::get_isPerforming"(acc."ContractStatus_PastDueStatus_Status")
				ELSE NULL
			END AS "isPerforming",
			"FinancialContract_FixedDepositRepayableAmount",
			"FinancialContract_OriginalInvestmentAmount",
            pos."currentPrincipal",
			pos."recoveredAmount",
			pos."pastDueAmount",
			pos."accruedInterest",
			pos."AccountBalance",
			pos."OperationalAmount",
			CASE
				WHEN acc."FinancialContract_AccountCurrency" = 'XAU'
					--AND "BusinessPartnerContractAssignment_AccountProvider_ContractDataOwner" = true
					AND  expop."CreditRiskExposure_ExposureClass"='Gold'
						THEN 1
				ELSE 0
			END AS "isGoldBullion",
			expop."CreditRiskExposure_ExposureClass" AS "productType",
			expop."exposureClass"
			
			FROM "com.adweko.adapter.osx.inputdata.account::Account_Root_View" (
					:I_BUSINESS_DATE,
					:I_SYSTEM_TIMESTAMP,
					:I_DATAGROUP_STRING
					) AS acc
			LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.account::Account_get_Position" (
					:I_BUSINESS_DATE,
					:I_SYSTEM_TIMESTAMP,
					:I_DATAGROUP_STRING
					) AS pos ON pos."FinancialContract_IDSystem" = acc."FinancialContract_IDSystem"
				AND pos."FinancialContract_FinancialContractID" = acc."FinancialContract_FinancialContractID"
				AND (pos."ContractCurrency" = acc."PositionCurrency" --multiy currency Account
					OR acc."PositionCurrency" IS NULL) --single currency Account
				AND pos."dataGroup" = acc."dataGroup"
			LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS keylegalEntity 
				 ON keylegalEntity."KeyID" = 'legalEntitySwitch'
				 
			LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_holdingCategory"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS ghc
				ON	ghc."_FinancialContract_IDSystem" = acc."FinancialContract_IDSystem"
				AND	ghc."_FinancialContract_FinancialContractID" = acc."FinancialContract_FinancialContractID"	 
		
-- CreditRisk 
			LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_recoveryRate" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Account') AS rec 
				ON rec."CreditRiskRecoveryRate_FinancialContract_IDSystem" = acc."FinancialContract_IDSystem"
				AND rec."CreditRiskRecoveryRate_FinancialContract_FinancialContractID" = acc."FinancialContract_FinancialContractID"
			
			LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskAdjustmentPrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Impairment', 'Account') AS cra_imp
				ON cra_imp."CreditRiskAdjustment_FinancialContract_IDSystem" = acc."FinancialContract_IDSystem"
				AND cra_imp."CreditRiskAdjustment_FinancialContract_FinancialContractID" = acc."FinancialContract_FinancialContractID"
				AND "FinancialContract_AccountCategory" = 'SingleCurrencyAccount'
				AND "FinancialContract_SingleCurrencyAccountCategory" = 'FixedTermDeposit'
				
			LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP, 'Account') AS expop ON 
					expop."CreditRiskExposure_FinancialContract_IDSystem" = acc."FinancialContract_IDSystem"
					AND expop."CreditRiskExposure_FinancialContract_FinancialContractID" = acc."FinancialContract_FinancialContractID"
				
			) AS innerselect1
			

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.account::Account_ProductType"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS productCatalogAssignment
			ON productCatalogAssignment."FinancialContract_FinancialContractID"				= innerselect1."FinancialContract_FinancialContractID"
				AND productCatalogAssignment."FinancialContract_IDSystem"						= innerselect1."FinancialContract_IDSystem"
				AND productCatalogAssignment."dataGroup" = innerselect1."dataGroup"
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_contractFSType_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS contractFSType
			ON contractFSType."ProductCatalogItem" = productCatalogAssignment."StandardCatalog_ProductCatalogItem"
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_portfolioType" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS portfolioType
			ON  portfolioType."ASSOC_FinancialContract_FinancialContractID" = innerselect1."FinancialContract_FinancialContractID"
			AND portfolioType."ASSOC_FinancialContract_IDSystem"            = innerselect1."FinancialContract_IDSystem"
	);
END;
