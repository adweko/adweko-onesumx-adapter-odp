FUNCTION "com.adweko.adapter.osx.inputdata.account::Account_get_Interest" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"FinancialContract_IDSystem" NVARCHAR(40),
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"Interest_InterestCurrency" NVARCHAR(3),
		"Interest_InterestType" NVARCHAR(40),
		"Interest_FirstInterestPeriodStartDate" DATE,
		"Interest_FirstInterestPeriodEndDate" DATE,	
		"Interest_LastInterestPeriodEndDate" DATE,
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),		
		"currentNominalInterestRate" DECIMAL(15, 11),
		"repricingType" NVARCHAR(1),
		"pricingMarketObject" NVARCHAR(40),
		"pricingMarketObjectSimple" NVARCHAR(40),
		"rateAdd" DECIMAL(15, 11),
		"rateMultiplier" DECIMAL(15, 11),
		"rateTerm" NVARCHAR(10),
		"fixingDays" NVARCHAR(10),
		"contractDayCountMethod" NVARCHAR(2),
		"interestPaymentType" NVARCHAR(1),
		"businessDayConvention" NVARCHAR(2),
		"calendar" NVARCHAR(75),
		"eomConvention" NVARCHAR(1),
		"Interest_InterestIsCompounded" INT,
		"dataGroup" NVARCHAR(128),
		"lookbackPeriod" NVARCHAR(10),
		"rateCappingFlooring" NVARCHAR(40),
		"rateAveragingCap" DECIMAL(15, 11),
		"rateAveragingFloor" DECIMAL(15, 11)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/************************************************
        Select and map Interest Data
    *************************************************/
	OUTPUT =

	SELECT DISTINCT --Regular IP Periods after Stub
		acc."FinancialContract_IDSystem",
		acc."FinancialContract_FinancialContractID",
		acc."Interest_InterestCurrency",
		acc."Interest_InterestType",
		acc."Interest_FirstInterestPeriodStartDate",
		acc."Interest_FirstInterestPeriodEndDate",
		acc."Interest_LastInterestPeriodEndDate",
		'IP' AS "arraySpecKind",
		acc."Interest_FirstInterestPeriodEndDate" AS "cycleDate",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
				"Interest_InterestPeriodLength",
				mcp."cyclePeriod"
		) AS "cyclePeriod",
		CASE 
			WHEN acc."Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
				THEN acc."Interest_FixedRate"
			ELSE acc."ApplicableInterestRate_Rate"
		END AS "currentNominalInterestRate",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		NULL AS "pricingMarketObjectSimple",
		NULL AS "rateAdd",
		NULL AS "rateMultiplier",
		NULL AS "rateTerm",
		NULL AS "fixingDays",
		dcm."dayCountMethod" AS "contractDayCountMethod",
		"com.adweko.adapter.osx.inputdata.common::get_interestPaymentType" ("Interest_InterestInAdvance", 2, 1) AS "interestPaymentType",
		"com.adweko.adapter.osx.inputdata.common::get_businessDayConvention" (bdc."OsxBusinessDayConventionCode") AS "businessDayConvention",
		cal."calendar" AS "calendar",
		"com.adweko.adapter.osx.inputdata.common::get_eomConvention" ("Interest_DayOfMonthOfInterestPeriodEnd") AS "eomConvention",
		MAP("Interest_InterestIsCompounded",
			true, 1,
			false, 0
		) AS "Interest_InterestIsCompounded",
		"dataGroup",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND acc."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
						acc."Interest_ResetCutoffLength",
						mcpr."cyclePeriod")
		END AS "lookbackPeriod",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND acc."Interest_InterestIsCompounded" = true
			THEN 'MONETARY_REFERENCE'
		END AS "rateCappingFlooring",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND acc."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", acc."Interest_FloatingRateMax", acc."Interest_VariableRateMax")
		END AS "rateAveragingCap",	
		CASE WHEN kvMonetaryRef."Value" = 'On' AND acc."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", acc."Interest_FloatingRateMin", acc."Interest_VariableRateMin")
		END AS "rateAveragingFloor"
	FROM "com.adweko.adapter.osx.inputdata.account::BV_Account_EXT" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) AS acc
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_BDC_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bdc
			ON bdc."CalculationDay" = 'NoAdjustment'
			AND bdc."PaymentDay" = acc."Interest_BusinessDayConvention"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS dcm 
			ON	dcm."DayCountConvention" = acc."Interest_DayCountConvention"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_calendar_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS cal 
			ON  cal."BusinessCalendar" = acc."Interest_InterestBusinessCalendar"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = acc."Interest_InterestPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcpr
		ON  mcpr."BusinesscyclePeriod" = acc."Interest_ResetCutoffTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS cfss
		ON cfss."KeyID" = 'CapFloorSwitch'

	WHERE	acc."Interest_InterestCategory" = 'InterestPeriodSpecification' AND 
			acc."Interest_FirstInterestPeriodEndDate" IS NOT NULL AND
			(acc."Interest_LastInterestPeriodEndDate" > :I_BUSINESS_DATE OR acc."Interest_LastInterestPeriodEndDate" IS NULL) AND			
			acc."Interest_LastInterestPeriodEndDate" > acc."Interest_FirstInterestPeriodEndDate" AND
			acc."Interest_InterestPeriodLength" IS NOT NULL AND
			acc."Interest_InterestPeriodTimeUnit" IS NOT NULL
			
	UNION ALL

	SELECT DISTINCT --Stub Period and regular IP Periods
		acc."FinancialContract_IDSystem",
		acc."FinancialContract_FinancialContractID",
		acc."Interest_InterestCurrency",
		acc."Interest_InterestType",
		acc."Interest_FirstInterestPeriodStartDate",
		acc."Interest_FirstInterestPeriodEndDate",
		acc."Interest_LastInterestPeriodEndDate",
		'IP' AS "arraySpecKind",
		acc."Interest_FirstInterestPeriodStartDate" AS "cycleDate",
		CASE 
			WHEN "Interest_FirstInterestPeriodEndDate" IS NOT NULL 
				THEN '999Y' 
			ELSE
				"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
					"Interest_InterestPeriodLength",
					mcp."cyclePeriod")
		END AS "cyclePeriod",
		CASE 
			WHEN acc."Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
				THEN acc."Interest_FixedRate"
			ELSE acc."ApplicableInterestRate_Rate"
		END AS "currentNominalInterestRate",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		NULL AS "pricingMarketObjectSimple",
		NULL AS "rateAdd",
		NULL AS "rateMultiplier",
		NULL AS "rateTerm",
		NULL AS "fixingDays",
		dcm."dayCountMethod" AS "contractDayCountMethod",
		"com.adweko.adapter.osx.inputdata.common::get_interestPaymentType" ("Interest_InterestInAdvance", kv1."Value", kv2."Value") AS "interestPaymentType",
		"com.adweko.adapter.osx.inputdata.common::get_businessDayConvention" (bdc."OsxBusinessDayConventionCode") AS "businessDayConvention",
		cal."calendar" AS "calendar",
		"com.adweko.adapter.osx.inputdata.common::get_eomConvention" ("Interest_DayOfMonthOfInterestPeriodEnd") AS "eomConvention",
		MAP("Interest_InterestIsCompounded",
			true, 1,
			false, 0
		) AS "Interest_InterestIsCompounded",
		"dataGroup",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND acc."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
						acc."Interest_ResetCutoffLength",
						mcpr."cyclePeriod")
		END AS "lookbackPeriod",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND acc."Interest_InterestIsCompounded" = true
			THEN 'MONETARY_REFERENCE'
		END AS "rateCappingFlooring",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND acc."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", acc."Interest_FloatingRateMax", acc."Interest_VariableRateMax")
		END AS "rateAveragingCap",	
		CASE WHEN kvMonetaryRef."Value" = 'On' AND acc."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", acc."Interest_FloatingRateMin", acc."Interest_VariableRateMin")
		END AS "rateAveragingFloor"
	FROM "com.adweko.adapter.osx.inputdata.account::BV_Account_EXT" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) AS acc
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv1
			ON  kv1."KeyID" = 'TRUE'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv2
			ON  kv2."KeyID" = 'FALSE'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_BDC_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bdc
			ON bdc."CalculationDay" = 'NoAdjustment'
			AND bdc."PaymentDay" = acc."Interest_BusinessDayConvention"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS dcm 
			ON	dcm."DayCountConvention" = acc."Interest_DayCountConvention"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_calendar_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS cal 
			ON	cal."BusinessCalendar" = acc."Interest_InterestBusinessCalendar"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = acc."Interest_InterestPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcpr
		ON  mcpr."BusinesscyclePeriod" = acc."Interest_ResetCutoffTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS cfss
		ON cfss."KeyID" = 'CapFloorSwitch'

	WHERE	acc."Interest_InterestCategory" = 'InterestPeriodSpecification' AND
			(acc."Interest_LastInterestPeriodEndDate" > :I_BUSINESS_DATE OR acc."Interest_LastInterestPeriodEndDate" IS NULL) AND
			acc."Interest_InterestPeriodLength" IS NOT NULL AND
			acc."Interest_InterestPeriodTimeUnit" IS NOT NULL
	
	UNION ALL

	SELECT DISTINCT --regular DependentFloatingRateSpecification RP Period after Stub 
		acc."FinancialContract_IDSystem",
		acc."FinancialContract_FinancialContractID",
		acc."Interest_InterestCurrency",
		acc."Interest_InterestType",
		acc."Interest_FirstInterestPeriodStartDate",
		acc."Interest_FirstInterestPeriodEndDate",
		acc."Interest_LastInterestPeriodEndDate",
		'RP' AS "arraySpecKind",
		"Interest_FirstInterestPeriodEndDate" AS "cycleDate",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			"Interest_InterestPeriodLength",
			mcp3."cyclePeriod" )
		AS "cyclePeriod",
		NULL AS "currentNominalInterestRate",
		NULL AS "repricingType",
		MAP(kvMonetaryRef."Value",
			'On', CASE WHEN acc."Interest_InterestIsCompounded" = true
					THEN acc."Calc_Account_PositionCurrency"
							|| '-' 
							|| pmo."pricingMarketObject"
							|| '-'
							|| "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
									"Interest_InterestPeriodLength",
									mcp3."cyclePeriod" )
							|| '-MR'
					ELSE pmo."pricingMarketObject" END,
			'Off', pmo."pricingMarketObject") AS "pricingMarketObject",
		pmo."pricingMarketObject" AS "pricingMarketObjectSimple",
		"Interest_Spread" AS "rateAdd",
		"Interest_ReferenceRateFactor" AS "rateMultiplier",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			referenceRate."ReferenceRate_TimeToMaturity",
			mcp1."cyclePeriod"
		) AS "rateTerm",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			"Interest_ResetLagLength",
			mcp2."cyclePeriod"
		) AS "fixingDays",
		NULL AS "contractDayCountMethod",
		NULL AS "interestPaymentType",
		NULL AS "businessDayConvention",
		NULL AS "calendar",
		NULL AS "eomConvention",
		NULL AS "Interest_InterestIsCompounded",
		"dataGroup",
		NULL AS "lookbackPeriod",
		NULL AS "rateCappingFlooring",
		NULL AS "rateAveragingCap",
		NULL AS "rateAveragingFloor"
	FROM "com.adweko.adapter.osx.inputdata.account::BV_Account_EXT" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) AS acc
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ReferenceRate" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP
			) AS referenceRate 
			ON referenceRate."ReferenceRate_ReferenceRateID" = acc."Interest_ReferenceRateID"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp1
			ON  mcp1."BusinesscyclePeriod" = referenceRate."ReferenceRate_TimeToMaturityUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp2
			ON  mcp2."BusinesscyclePeriod" = acc."Interest_ResetLagTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp3
			ON  mcp3."BusinesscyclePeriod" = acc."Interest_InterestPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_pricingMarketObject_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pmo 
			ON	pmo."ReferenceRateID" = acc."Interest_ReferenceRateID"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
			
	WHERE	acc."Interest_InterestSpecificationCategory" = 'FloatingRateSpecification' AND
			acc."Interest_FixingRateSpecificationCategory" ='DependentFloatingRateSpecification' AND
			acc."Interest_LastInterestPeriodEndDate" IS NOT NULL AND
			acc."Interest_FirstInterestPeriodEndDate" IS NOT NULL
			
	UNION ALL
			
	SELECT DISTINCT --regular RP Periods and Stub Periods
		acc."FinancialContract_IDSystem",
		acc."FinancialContract_FinancialContractID",
		acc."Interest_InterestCurrency",
		acc."Interest_InterestType",
		acc."Interest_FirstInterestPeriodStartDate",
		acc."Interest_FirstInterestPeriodEndDate",
		acc."Interest_LastInterestPeriodEndDate",
		'RP' AS "arraySpecKind",
		CASE 
			WHEN "Interest_FixingRateSpecificationCategory" = 'IndependentFloatingRateSpecification'
				AND "Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
				THEN "Interest_FirstRegularFloatingRateResetDate"
			WHEN "Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification'
				THEN "Interest_FirstInterestPeriodStartDate"
			WHEN "Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
				THEN "Interest_FirstInterestPeriodStartDate"
		END AS "cycleDate",
 		CASE 
			--WHEN 
			--"Interest_FixingRateSpecificationCategory" = 'IndependentFloatingRateSpecification'
			--	AND "Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
			--	THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			--		"Interest_ResetPeriodLength",
			--		mcp4."cyclePeriod" )
			WHEN 
			--"Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification'
			--	AND "Interest_LastInterestPeriodEndDate" IS NULL
					"Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
				OR 	"Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
				
				THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
					"Interest_InterestPeriodLength",
					mcp3."cyclePeriod" )
			WHEN "Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification'
				AND "Interest_LastInterestPeriodEndDate" IS NOT NULL
				THEN '999Y'
		END AS "cyclePeriod",
		NULL AS "currentNominalInterestRate", 
		"com.adweko.adapter.osx.inputdata.common::get_repricingType"(
			ReferenceRateType => '',
			InterestSpecificationCategory => "Interest_InterestSpecificationCategory",
			FixingRateSpecificationCategory => "Interest_FixingRateSpecificationCategory",
			DependentFloatingRateSpecification_ResetInArrears => "Interest_ResetInArrears",
			IndependentFloatingRateSpecification_ResetAtMonthUltimo => "Interest_ResetAtMonthUltimo",
			CutOffRelativeToDateCode => "Interest_CutoffRelativeToDate"
		) AS "repricingType",
		MAP(kvMonetaryRef."Value",
			'On', CASE WHEN acc."Interest_InterestIsCompounded" = true
					THEN acc."Calc_Account_PositionCurrency"
							|| '-' 
							|| pmo."pricingMarketObject"
							|| '-'
							|| CASE WHEN 
										"Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
									OR 	"Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
									
									THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
										"Interest_InterestPeriodLength",
										mcp3."cyclePeriod" )
								WHEN "Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification'
									AND "Interest_LastInterestPeriodEndDate" IS NOT NULL
									THEN '999Y'
							END
							|| '-MR'
					ELSE pmo."pricingMarketObject" END,
			'Off', pmo."pricingMarketObject") AS "pricingMarketObject",
		pmo."pricingMarketObject" AS "pricingMarketObjectSimple",
		MAP("Interest_InterestSpecificationCategory",
			'FloatingRateSpecification', "Interest_Spread",
			'FixedRateSpecification', "Interest_FixedRate"
		) AS "rateAdd",
		MAP("Interest_InterestSpecificationCategory",
			'FloatingRateSpecification', "Interest_ReferenceRateFactor",
			NULL
		) AS "rateMultiplier",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			referenceRate."ReferenceRate_TimeToMaturity",
			mcp1."cyclePeriod"
		) AS "rateTerm",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			"Interest_ResetLagLength",
			mcp2."cyclePeriod"
		) AS "fixingDays",
		NULL AS "contractDayCountMethod",
		NULL AS "interestPaymentType",
		NULL AS "businessDayConvention",
		NULL AS "calendar",
		NULL AS "eomConvention",
		NULL AS "Interest_InterestIsCompounded",
		"dataGroup",
		NULL AS "lookbackPeriod",
		NULL AS "rateCappingFlooring",
		NULL AS "rateAveragingCap",
		NULL AS "rateAveragingFloor"
	FROM "com.adweko.adapter.osx.inputdata.account::BV_Account_EXT" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) AS acc
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ReferenceRate" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP
			) AS referenceRate 
			ON referenceRate."ReferenceRate_ReferenceRateID" = acc."Interest_ReferenceRateID"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp1
			ON  mcp1."BusinesscyclePeriod" = referenceRate."ReferenceRate_TimeToMaturityUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp2
			ON  mcp2."BusinesscyclePeriod" = acc."Interest_ResetLagTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp3
			ON  mcp3."BusinesscyclePeriod" = acc."Interest_InterestPeriodTimeUnit"
--	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp4
--			ON  mcp4."BusinesscyclePeriod" = acc."Interest_ResetPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_pricingMarketObject_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pmo 
			ON	pmo."ReferenceRateID" = acc."Interest_ReferenceRateID"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'

	WHERE acc."Interest_InterestSpecificationCategory" IN ('FloatingRateSpecification','FixedRateSpecification') ;

	RETURN :output;
END;
