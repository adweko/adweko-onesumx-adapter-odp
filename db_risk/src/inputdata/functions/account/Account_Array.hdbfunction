FUNCTION "com.adweko.adapter.osx.inputdata.account::Account_Array" (
	IN	I_BUSINESS_DATE DATE,
	IN	I_SYSTEM_TIMESTAMP TIMESTAMP,
	IN  I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"refid" NVARCHAR(32),
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),
		"pricingMarketObject" NVARCHAR(40),
		"repricingType" NVARCHAR(1),
		"rateAdd" DECIMAL(15, 11),
		"rateMultiplier" DECIMAL(15, 11),
		"rateTerm" NVARCHAR(10),
		"fixingDays" NVARCHAR(10),
		"cashFlowKind" NVARCHAR(1),
		"capitalized" INT,
/*  ---------------------------------
	Attributes for Credit Risk:
*/ ---------------------------------
		"anchor" DATE,
		"amount" DECIMAL(34, 11),
		"ppAmount" DECIMAL(34, 6),
		"dataGroup" NVARCHAR(128)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	SELECT DISTINCT CASE 
			WHEN (innerselect."FinancialContract_SingleCurrencyAccountCategory" IN ('SavingsAccount', 'CurrentAccount') AND "arraySpecKind"='RP')
				OR (innerselect."FinancialContract_AccountCategory" = 'MultiCurrencyAccount' AND innerselect."maturityDate" IS NULL AND "arraySpecKind"='RP')
				THEN "com.adweko.adapter.osx.inputdata.common::get_array_id" (
						"com.adweko.adapter.osx.inputdata.common::get_id" (
							"contractID",
							'0001',
							innerselect."dataGroup"
							),
						"arraySpecKind",
						''
						)
			ELSE "com.adweko.adapter.osx.inputdata.common::get_array_id" (
					"com.adweko.adapter.osx.inputdata.common::get_id" (
						"contractID",
						'0001',
						innerselect."dataGroup"
						),
					"arraySpecKind",
					"cycleDate"
					)
			END AS "id",
		"com.adweko.adapter.osx.inputdata.common::get_id" (
			"contractID",
			'0001',
			innerselect."dataGroup"
			) AS "refid",
		"arraySpecKind",
		"cycleDate",
		"cyclePeriod",
		"pricingMarketObject",
		"repricingType",
		"rateAdd",
		"rateMultiplier",
		"rateTerm",
		"fixingDays",
		"cashFlowKind",
		"capitalized",
		"anchor",
		"amount",
		"ppAmount",
		--Datagroup
		innerselect."dataGroup" AS "dataGroup"
		-- COALESCE(dtg."Name", dtgdef."defaultName") AS "dataGroup"
	FROM (
		SELECT 
			"arraySpecKind",
			CASE 
				WHEN root."maturityDate" IS NOT NULL --exclude UMP
					THEN inte."cycleDate"
				ELSE NULL
			END AS "cycleDate",
			CASE 
				WHEN root."maturityDate" IS NOT NULL --exclude UMP
					THEN inte."cyclePeriod"
				ELSE NULL 
			END AS "cyclePeriod",
			CASE 
				WHEN root."maturityDate" IS NOT NULL --exclude UMP
					THEN inte."pricingMarketObject"
				ELSE inte."pricingMarketObjectSimple"
			END AS "pricingMarketObject",
			"repricingType",
			CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" ("rateAdd", 'rateAdd', kv."Value") AS DECIMAL (15,11)) AS "rateAdd",
			CASE 
				WHEN root."maturityDate" IS NOT NULL --exclude UMP
					THEN inte."rateMultiplier"
				ELSE NULL 
			END AS "rateMultiplier",
			CASE 
				WHEN root."maturityDate" IS NOT NULL --exclude UMP
					THEN inte."rateTerm"
				ELSE NULL
			END AS "rateTerm",
			CASE 
				WHEN root."maturityDate" IS NOT NULL --exclude UMP
					THEN inte."fixingDays"
				ELSE NULL
			END AS "fixingDays",
			:I_BUSINESS_DATE,
			root."contractID" AS "contractID",
			NULL AS "anchor",
			NULL AS "amount",
			NULL AS "ppAmount",
			NULL AS "cashFlowKind",
			NULL AS "capitalized",
			inte."FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			inte."FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			root."FinancialContract_SingleCurrencyAccountCategory",
			root."FinancialContract_AccountCategory",
			root."maturityDate",
			inte."dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.account::Account_get_Interest" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				:I_DATAGROUP_STRING
				) AS inte
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
			ON  kv."KeyID" = 'SAPPercentageStandard'
		INNER JOIN "com.adweko.adapter.osx.inputdata.account::Account_get_Root" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				:I_DATAGROUP_STRING
			) AS root
			ON	root."FinancialContract_IDSystem" 					= inte."FinancialContract_IDSystem"
				AND root."FinancialContract_FinancialContractID"	= inte."FinancialContract_FinancialContractID"
				AND inte."Interest_InterestCurrency"				= root."ContractCurrency"
				AND inte."dataGroup"								= root."dataGroup"
		WHERE (
			(
				"repricingType"<>'4' OR "repricingType" IS NULL)
				AND root."FinancialContract_SingleCurrencyAccountCategory" NOT IN ('SavingsAccount', 'CurrentAccount') --Exclude UMP single Currency
				AND root."FinancialContract_AccountCategory" <> 'MultiCurrencyAccount' --Exclude UMP Multiple Currency Account
				AND root."maturityDate" IS NOT NULL
			)
			OR
			(
				"repricingType"='4'
				AND inte."Interest_FirstInterestPeriodStartDate" > :I_BUSINESS_DATE
				AND root."FinancialContract_SingleCurrencyAccountCategory" NOT IN ('SavingsAccount', 'CurrentAccount')
				AND root."FinancialContract_AccountCategory" <> 'MultiCurrencyAccount' --Exclude UMP Multiple Currency Account
				AND root."maturityDate" IS NOT NULL
			) -- only Fixed Interest Rates in future periods
		
			OR
			(
				(root."FinancialContract_SingleCurrencyAccountCategory" IN ('SavingsAccount', 'CurrentAccount')
				OR
				(root."FinancialContract_AccountCategory" = 'MultiCurrencyAccount'
				AND root."maturityDate" IS NULL AND inte."Interest_InterestCurrency" = root."ContractCurrency") ---multyCurrencyAccoun
				)--UMP RP Event as of BVD
				AND "cycleDate" <= :I_BUSINESS_DATE
				AND inte."Interest_LastInterestPeriodEndDate" >= :I_BUSINESS_DATE
				AND "arraySpecKind" = 'RP'
			)
/*

-- It is relevant, for the FixedTermdeposit, as a new Stream was created for the FixedTermDeposit, this part should be avoid

		UNION
	
		SELECT --PP
			'PP' AS "arraySpecKind",
			payment."cycleStartDateOrig" AS "cycleDate",
			"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
				payment."cycleLength",
				mcp."cyclePeriod"
			) AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "anchor",
			NULL AS "amount",
			payment."ppAmount",
			NULL AS "cashFlowKind",
			NULL AS "capitalized",
			payment."FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			payment."FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "FinancialContract_SingleCurrencyAccountCategory",
			NULL AS "FinancialContract_AccountCategory",
			NULL AS "maturityDate",
			root."dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.account::Account_get_Payment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS payment
		INNER JOIN "com.adweko.adapter.osx.inputdata.account::Account_get_Root" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				:I_DATAGROUP_STRING
			) AS root
			ON	payment."FinancialContract_FinancialContractID" 	= root."FinancialContract_FinancialContractID"
			AND payment."FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"
			AND ("cycleEndDate" > :I_BUSINESS_DATE OR ("cycleStartDateOrig" >= :I_BUSINESS_DATE AND "cycleEndDate" IS NULL AND "cycleLength" = 999 AND "cycleTimeUnit" = 'Year') )
			AND payment."dataGroup" 								= root."dataGroup"
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = payment."cycleTimeUnit"			
		WHERE
			payment."cycleLength" IS NOT NULL
			AND payment."cycleTimeUnit" IS NOT NULL
			AND payment."cycleStartDateOrig" IS NOT NULL*/
	) AS innerselect
	;
END;
