FUNCTION "com.adweko.adapter.osx.inputdata.swaption.array::2C_IP_afterStub" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128),
		I_LEG NVARCHAR(4)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"refid" NVARCHAR(32),
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),
		"dataGroup" NVARCHAR(128)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	SELECT
		"com.adweko.adapter.osx.inputdata.common::get_array_id" (
			"com.adweko.adapter.osx.inputdata.common::get_id" (
				root."contractID",
				root."node_No",
				root."dataGroup"
			),
			'IP',
			inte."FirstInterestPeriodEndDate" -- =cycleDate"
		) AS "id",
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			root."node_No",
			root."dataGroup"
		) AS "refid",
		'IP' AS "arraySpecKind",
		inte."FirstInterestPeriodEndDate" AS "cycleDate",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
				inte."InterestPeriodLength",
				mcp."cyclePeriod"
		) AS "cyclePeriod",
		root."dataGroup"

	FROM "com.adweko.adapter.osx.inputdata.swaption::Swaption2C_RootLeg_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING,:I_LEG) AS root
	
	INNER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvInterestType
		ON kvInterestType."KeyID" = 'Swaption_InterestType'
		
	INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inte
        ON inte."FinancialContractID"					= root."InterestBearingSwap_FinancialContractID"
       		AND inte."IDSystem" 						= root."InterestBearingSwap_IDSystem"
       		AND inte."RoleOfPayer"						= :I_LEG || 'Payer'
			AND inte."InterestCategory"					= 'InterestPeriodSpecification'
			AND inte."InterestType" 					= kvInterestType."Value"
			AND inte."FirstInterestPeriodEndDate"		<> inte."FirstInterestPeriodStartDate"
			AND	(
				inte."LastInterestPeriodEndDate"		IS NULL
				OR
				inte."LastInterestPeriodEndDate"		> :I_BUSINESS_DATE
				AND inte."LastInterestPeriodEndDate"    > inte."FirstInterestPeriodEndDate"
			)
			AND inte."InterestPeriodLength"				IS NOT NULL
			AND inte."InterestPeriodTimeUnit"			IS NOT NULL
				
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" = inte."InterestPeriodTimeUnit"
	
	;
END;
