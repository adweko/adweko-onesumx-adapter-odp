FUNCTION "com.adweko.adapter.osx.inputdata.agreedlimit.creditCard::AgreedLimitCreditCard_Contract"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP, 
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"maturityDate" DATE,
		"currency" NVARCHAR(3),
		"dataGroup" NVARCHAR(128),
		"grossPayableAmountAtDefault" DECIMAL(34, 6),
		"netRecoveryRate" DECIMAL (15,11),
    	"isUnderLiquidityManagementControl" INT,
    	"isRevolving" NVARCHAR(10),
    	"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
    	"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
    	"creditLineTypeID" INT,
    	"usedAtDefault" DECIMAL(34, 6),
    	"bookValue" DECIMAL(34, 6),
    	"exposureClass" NVARCHAR(255)
    	) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	 SELECT 
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			root."node_No",
			root."dataGroup"
			) AS "id",
		root."contractID" AS "sourceSystemRecordNumber",
		root."sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		root."node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		CASE
			WHEN root."AgreedLimit_AcceptanceDate" IS NOT NULL THEN
				CASE 
					WHEN (root."AgreedLimit_LimitValidFrom" < root."AgreedLimit_AcceptanceDate") THEN root."AgreedLimit_LimitValidFrom"
					WHEN (root."AgreedLimit_LimitValidFrom" >= root."AgreedLimit_AcceptanceDate") THEN root."AgreedLimit_AcceptanceDate"
				END
			ELSE root."FinancialContract_OriginalSigningDate"
		END AS "dealDate",
		CASE 
			WHEN root."AgreedLimit_LimitValidTo"  = '9999-12-31' THEN NULL
			ELSE root."AgreedLimit_LimitValidTo"
		END AS "maturityDate",
		root."AgreedLimit_LimitCurrency" AS "currency",
		root."dataGroup",
		gpaad."Valuation_EstimatedMarketValue" AS "grossPayableAmountAtDefault",
		CAST(
			CASE
				WHEN rec."CreditEnhancementStep" = 'After'
	            	THEN rec."CreditRiskRecoveryRate_RecoveryRate"
				ELSE NULL
			END 
			AS DECIMAL(15,11)
		) AS "netRecoveryRate",
		1 AS "isUnderLiquidityManagementControl",
		UPPER(CAST(root."AgreedLimit_IsRevolving" AS NVARCHAR(10))) AS "isRevolving",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",
		CASE 
			WHEN root."AgreedLimit_IsRevolving" = true 
			 AND root."AgreedLimit_IsUnconditionallyRevocable" = true THEN 1
			WHEN root."AgreedLimit_IsRevolving" = true 
			 AND root."AgreedLimit_IsUnconditionallyRevocable" = false THEN 2
			WHEN root."AgreedLimit_IsRevolving" = false 
			 AND root."AgreedLimit_IsUnconditionallyRevocable" = true THEN 4
			WHEN root."AgreedLimit_IsRevolving" = false 
			 AND root."AgreedLimit_IsUnconditionallyRevocable" = false THEN 5
		END AS "creditLineTypeID",
		"com.adweko.adapter.osx.inputdata.common::get_usedAtDefault"(
			SUM(root."MonetaryBalance_AccountBalance" - cs."MonetaryBalanceHistorical_AccountBalance"),
			root."FreeLine_FreeLine",
			root."FreeLine_Utilization"
		) AS "usedAtDefault",
		bok."BookValue_BookValue" AS "bookValue",
		expop."exposureClass"

    FROM "com.adweko.adapter.osx.inputdata.agreedlimit.creditCard::AgreedLimitCreditCard_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS root

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.agreedlimit::AgreedLimit_ContractStatusPrio_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'AgreedLimitCurrentAccount') AS cs
			ON cs."FinancialContract_IDSystem"										= root."FinancialContract_IDSystem"
			AND cs."FinancialContract_FinancialContractID"							= root."FinancialContract_FinancialContractID"
			AND cs."MonetaryBalanceHistorical_AccountBalance"						< root."MonetaryBalance_AccountBalance"

    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP, 'AgreedLimitCreditCard') AS expop
			ON expop."CreditRiskExposure_FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"
			AND expop."CreditRiskExposure_FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_recoveryRate" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'AgreedLimitCreditCard') AS rec
            ON rec."CreditRiskRecoveryRate_FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"
            AND rec."CreditRiskRecoveryRate_FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"
            
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kv
			ON  kv."KeyID" = 'SAPPercentageStandard'
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.agreedlimit.creditCard::get_grossPayableAmountAtDefault"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS gpaad
			ON gpaad."FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"
			AND gpaad."FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"
			AND root."CardIssue_IsMainCard"						= true

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
			ON bok."BookValue_FinancialContract_IDSystem"		= root."FinancialContract_IDSystem"
			AND bok."BookValue_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
			AND bok."BookValue_BookValueCurrency"				= root."AgreedLimit_LimitCurrency"

	GROUP BY root."contractID",
		root."node_No",
		root."dataGroup",
		root."sourceSystemID",
		root."FinancialContract_IDSystem",
		root."AgreedLimit_AcceptanceDate",
		root."AgreedLimit_LimitValidFrom",
		root."FinancialContract_OriginalSigningDate",
		root."AgreedLimit_LimitValidTo" ,
		root."AgreedLimit_LimitCurrency",
		gpaad."Valuation_EstimatedMarketValue",
		rec."CreditEnhancementStep",
		rec."CreditRiskRecoveryRate_RecoveryRate",
		root."AgreedLimit_IsRevolving",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",
		root."AgreedLimit_IsRevolving",
		root."AgreedLimit_IsUnconditionallyRevocable",
		root."FreeLine_FreeLine",
		root."FreeLine_Utilization",
		bok."BookValue_BookValue",
		expop."exposureClass"
    ;
END;
