FUNCTION "com.adweko.adapter.osx.inputdata.agreedlimit.creditCard::get_grossPayableAmountAtDefault"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"FinancialContract_IDSystem" NVARCHAR(40),
		"Valuation_EstimatedMarketValue" DECIMAL(34, 6)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
    /*****************************
        Write your function logic
    ****************************/
    RETURN
	
	SELECT
		"FinancialContract_FinancialContractID",
		"FinancialContract_IDSystem",
		"Valuation_EstimatedMarketValue"
	FROM (
		SELECT
			root."FinancialContract_FinancialContractID",
			root."FinancialContract_IDSystem",
			val."Valuation_EstimatedMarketValue",
			ROW_NUMBER() OVER(
				PARTITION BY 
					"FinancialContract_FinancialContractID",
    				"FinancialContract_IDSystem"
    			ORDER BY
    				val."Valuation_ValuationDate" DESC,
    				MAP(val."Valuation_ValuationMethod",
    					'MarkToMarket', 1,
    					'ThirdPartyValuation', 2,
    					'CreditorValuation', 3,
    					'ProviderEstimation', 4,
    					'MarkToModel', 5
    				)
			) AS "Prio"
		FROM "com.adweko.adapter.osx.inputdata.agreedlimit.creditCard::AgreedLimitCreditCard_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS root
    	
    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Valuation"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS val
    		 ON val."Valuation_ASSOC_FinancialContract_FinancialContractID"			= root."FinancialContract_FinancialContractID"
    		AND val."Valuation_ASSOC_FinancialContract_IDSystem"					= root."FinancialContract_IDSystem"
	
    	WHERE root."ContractStatus_DefaultStatus_Status" = 'Defaulted'
    		AND root."CardIssue_IsMainCard" = true
    		AND val."Valuation_ValuationDate" <= root."ContractStatus_DefaultStatus_StatusChangeDate"
    ) WHERE "Prio" = 1
   	;
END;