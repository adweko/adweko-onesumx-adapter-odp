FUNCTION "com.adweko.adapter.osx.inputdata.agreedlimit.facility::AgreedLimitFacility_Contract"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"productFSClass" NVARCHAR(128),
		"isRevolving" NVARCHAR(10),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"maturityDate" DATE,
		"currency" NVARCHAR(3),
		"grossPayableAmountAtDefault" DECIMAL(34, 6),
		"creditRiskProvision" DECIMAL(34, 6),
		"netRecoveryRate" DECIMAL(15, 11),
		"grossRecoveryRate" DECIMAL(15, 11),
		"syndicatedMargin" DECIMAL (15,11),
		"dataGroup" NVARCHAR(128),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
/*  ---------------------------------
	User Defined Attributes:
*/ ----------------------------------
		"isUnderLiquidityManagementControl" INT,
		"ebaLcrContractEligibility" NVARCHAR(255),
		"isGoldBullion" INT,
		"commitmentType" NVARCHAR(75),
		"crRiskWeight" DECIMAL(15, 11),
		"eligibleType" NVARCHAR(75),
		"productType" NVARCHAR(128),
		"contractRole" NVARCHAR(1),
		"portfolioType" NVARCHAR(255),
		"seniority" NVARCHAR(75),
		"creditLineTypeID" INT,
		"UDA_BRUTTOMARGE" DECIMAL(34, 6),
		"counterparty" NVARCHAR(128),
		"ftpCommitmentSpread" DECIMAL(15, 11),
		"globalCap" DECIMAL(15, 11),
		"globalFloor" DECIMAL(15, 11),
		"bookValue" DECIMAL(34, 6),
		"cycleOfFeeDate" DATE,
		"cycleOfFeePeriod" NVARCHAR(10),
		"feeRate" DECIMAL(15, 11),
        "exposureClass" NVARCHAR(255)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN

	SELECT
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			"node_No",
			root."dataGroup"
		) AS "id",
		root."contractID" AS "sourceSystemRecordNumber",
		root."FinancialContract_FinancialContractID" || '#LIM' AS "sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		root."node_No",
		MAP(root."LenderOrBorrower_BusinessPartnerContractAssignment_ContractDataOwner_Role",
			'Lender', 'FACILITY_PROVIDED',
			'Borrower', 'FACILITY_RECEIVED'
		) AS "productFSClass",
		UPPER(CAST(root."AgreedLimit_IsRevolving" AS NVARCHAR(10))) AS "isRevolving",
		:I_BUSINESS_DATE AS "bookValueDate",
		CASE
			WHEN root."AgreedLimit_AcceptanceDate" IS NOT NULL AND root."AgreedLimit_AcceptanceDate" <> '' THEN
				CASE 
					WHEN (root."AgreedLimit_LimitValidFrom" < root."AgreedLimit_AcceptanceDate") THEN root."AgreedLimit_LimitValidFrom"
					WHEN (root."AgreedLimit_LimitValidFrom" >= root."AgreedLimit_AcceptanceDate") THEN root."AgreedLimit_AcceptanceDate"
				END
			ELSE COALESCE (root."AgreedLimit_LimitValidFrom", root."FinancialContract_OriginalSigningDate")
		END AS "dealDate",
		CASE 
			WHEN root."FinancialContract_FacilityEndDate"  = '9999-12-31' THEN NULL
			ELSE root."FinancialContract_FacilityEndDate"
		END AS "maturityDate",
		root."AgreedLimit_LimitCurrency" AS "currency",
		gpamount."Valuation_EstimatedMarketValue" AS "grossPayableAmountAtDefault",
		cra."CreditRiskAdjustment_RiskProvisionInPositionCurrency" AS "creditRiskProvision",
		CAST(
			CASE
				WHEN rec."CreditEnhancementStep" = 'After'
	            	THEN rec."CreditRiskRecoveryRate_RecoveryRate"
				ELSE NULL
			END 
			AS DECIMAL(15,11)
		) AS "netRecoveryRate",
        CAST(
			CASE
				WHEN rec."CreditEnhancementStep" = 'Before'
	            	THEN rec."CreditRiskRecoveryRate_RecoveryRate"
				ELSE NULL
			END 
			AS DECIMAL(15,11)
		) AS "grossRecoveryRate",
		CASE
			WHEN synMarginSwitch."Value" = '2'
				THEN
					CASE
						WHEN passiv."SUM_Loans_Amount" IS NULL AND active."SUM_Loans_Amount" > 0
							THEN TO_DECIMAL(1,15,11)
						WHEN ABS(active."SUM_Loans_Amount") <= passiv."SUM_Loans_Amount"
							THEN TO_DECIMAL(0,15,11)
						WHEN ABS(active."SUM_Loans_Amount") > passiv."SUM_Loans_Amount"
							THEN TO_DECIMAL(1 -(passiv."SUM_Loans_Amount" / 
							ABS(active."SUM_Loans_Amount")),15,11)
					END
				ELSE NULL
		END AS "syndicatedMargin",
		root."dataGroup",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",
		1 AS "isUnderLiquidityManagementControl",
		MAP(root."EuropeanRegulatoryReportingForContract_LiquidityClassificationOfAsset",
			NULL, 'NOT_ELIGIBLE',
			'ELIGIBLE_FAC'
		) AS "ebaLcrContractEligibility",
		0 AS "isGoldBullion",
		'6' AS "commitmentType",
		expo."CreditRiskExposure_RiskWeight" AS "crRiskWeight",
		eligibleType."eligibleType",
		root."CreditRiskExposure_ExposureClass" AS "productType",
		'1' AS "contractRole",
		pt."portfolioType",
		root."FinancialContract_Seniority" AS "seniority",
		MAP(root."AgreedLimit_IsRevolving",
			true, MAP(root."AgreedLimit_IsUnconditionallyRevocable",
				true, 1, --COMMITTED
				false, 2 --UNCOMMITTED
			),
			false, MAP(root."AgreedLimit_IsUnconditionallyRevocable",
				true, 4, --REVOLVING_COMMITTED
				false, 5 --REVOLVING_UNCOMMITTED
			)
		)AS "creditLineTypeID",
		MAP(marginKey."Value",
			'Rate', COALESCE(margin."MarginComponent_MarginComponentRate", margin."Margin_MarginRate"),
			'Amount', COALESCE(margin."MarginComponent_MarginComponentAmount",  margin."Margin_MarginAmount")
		) AS "UDA_BRUTTOMARGE",
		root."LenderOrBorrower_BusinessPartnerContractAssignment_Counterparty__BusinessPartnerID" AS "counterparty",
		TO_DECIMAL("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard"(inte."Interest_FixedRate", 'Interest_FixedRate', kv."Value"), 15, 11) AS "ftpCommitmentSpread",
		CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (sideAgreementsInterest."RateMax", 'globalCap', kv."Value") AS DECIMAL(15,11)) AS "globalCap",
		CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (sideAgreementsInterest."RateMin", 'globalFloor', kv."Value") AS DECIMAL(15,11)) AS "globalFloor",
		bok."BookValue_BookValue" AS "bookValue",
		MAP(root."Fee_FeeCategory",
			'RecurringFee', root."Fee_FirstFeeDueDate",
			'AgreedOneTimeFee', root."Fee_FeeChargingStartDate"
		) AS "cycleOfFeeDate",
		
		MAP(root."Fee_FeeCategory",
			'RecurringFee', "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
									root."Fee_RecurringFeeLength",
									mcp."cyclePeriod"),
			'AgreedOneTimeFee','999Y'
		) AS "cycleOfFeePeriod",

    	MAP(root."kvFeePercentageOrAmountSwitch",
    		'Percentage', "com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard"(root."kvFeePercentageOrAmountSwitch", 'Percentage', kv."Value"),
    		'Amount', root."Fee_FeeAmount"
    	) AS "feeRate",
        expo."exposureClass"

	FROM "com.adweko.adapter.osx.inputdata.agreedlimit.facility::AgreedLimitFacility_Root_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskAdjustmentPrio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Provision', 'AgredLimitFacility') AS cra
		 ON cra."CreditRiskAdjustment_FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"
		AND cra."CreditRiskAdjustment_FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.agreedlimit.facility::AgreedLimitFacility_get_grossPayableAmountAtDefault"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS gpamount
    	 ON gpamount."FinancialContract_FinancialContractID"					= root."FinancialContract_FinancialContractID"
    	AND gpamount."FinancialContract_IDSystem"								= root."FinancialContract_IDSystem"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_recoveryRate" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'AgreddLimitFacility') AS rec
         ON rec."CreditRiskRecoveryRate_FinancialContract_IDSystem" 			= root."FinancialContract_IDSystem"
        AND rec."CreditRiskRecoveryRate_FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_portfolioType"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pt
		 ON pt."ASSOC_FinancialContract_FinancialContractID"					= root."FinancialContract_FinancialContractID"
		AND pt."ASSOC_FinancialContract_IDSystem"								= root."FinancialContract_IDSystem"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_eligibleType" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS eligibleType
		 ON eligibleType."FinancialContractID"									= root."FinancialContract_FinancialContractID"
		AND eligibleType."IDSystem"												= root."FinancialContract_IDSystem"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Facility') AS expo
    	 ON expo."CreditRiskExposure_FinancialContract_FinancialContractID"		= root."FinancialContract_FinancialContractID"
    	AND expo."CreditRiskExposure_FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kv
		 ON kv."KeyID" = 'SAPPercentageStandard'
	
	--UDA_BRUTTOMARGE
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS marginKey
		ON marginKey."KeyID" = 'marginSwitch'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMargin"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'AgreedLimitFacility') AS margin
		ON margin."Margin_FinancialContract_FinancialContractID"				= root."FinancialContract_FinancialContractID"
		AND margin."Margin_FinancialContract_IDSystem"							= root."FinancialContract_IDSystem"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS synMarginSwitch
		ON  synMarginSwitch."KeyID" = 'syndicatedMarginAtLoanOrAgreedLimitLevel'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::BV_SyndicationFacilityLoanContracts"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP,'Passiv') AS passiv
		ON root."FinancialContract_IDSystem" = passiv."Facility_IDSystem"
		AND root."FinancialContract_FinancialContractID" = passiv."Facility_FinancialContractID"
		AND synMarginSwitch."Value" = '2'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::BV_SyndicationFacilityLoanContracts"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP,'Active') AS active
		ON root."FinancialContract_IDSystem" = active."Facility_IDSystem"
		AND root."FinancialContract_FinancialContractID" = active."Facility_FinancialContractID"
		AND synMarginSwitch."Value" = '2'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.agreedlimit.facility::AgreedLimitFacility_get_InterestPrio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS inte
		ON inte."Interest_IDSystem" = root."FinancialContract_IDSystem"
		AND inte."Interest_FinancialContractID" = root."FinancialContract_FinancialContractID"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.agreedlimit.facility::AgreeedLimitFacility_SideAgreements_get_Interest"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS sideAgreementsInterest
		ON sideAgreementsInterest."FinancialContractID"	= root."FinancialContract_FinancialContractID"
		AND sideAgreementsInterest."IDSystem" = root."FinancialContract_IDSystem"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
    	ON bok."BookValue_FinancialContract_IDSystem"		= root."FinancialContract_IDSystem"
    	AND bok."BookValue_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
    	AND bok."BookValue_BookValueCurrency"				= root."AgreedLimit_LimitCurrency"
    			
    LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" = root."Fee_RecurringFeePeriodTimeUnit";
		

END;