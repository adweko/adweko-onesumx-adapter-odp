FUNCTION "com.adweko.adapter.osx.inputdata.iro::InterestRateOptionCap_Contract"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP, 
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"legalEntity" NVARCHAR(128),
		"counterparty" NVARCHAR(128),
		"clearingHouse" NVARCHAR(128),
		"currency" NVARCHAR(3),
		"productFSClass" NVARCHAR(128),
		"contractFSType" NVARCHAR(256),
		"contractFSSubType" NVARCHAR(256),
		"instrumentType" NVARCHAR(128),
		"isUnderLiquidityManagementControl" INT,
		"optionHolder" NVARCHAR(1), 
		"optionExecutionType" NVARCHAR(1), 
		"optionDeliverySettlement" NVARCHAR(1), 
		"exerciseBeginDate" DATE,
		"exerciseEndDate" DATE,
		"optionType" NVARCHAR(1),
		"valueDate" DATE,
		"maturityDate" DATE,
		"currentPrincipal" DECIMAL(34, 6),
		"currentNominalInterestRate" DECIMAL(15, 11),
		"contractDayCountMethod" NVARCHAR(2),
		"calendar" NVARCHAR(75),
		"eomConvention" NVARCHAR(1),
		"accruedInterest" DECIMAL(38, 11),
		"grossPayableAmountAtDefault" DECIMAL(34, 6),
		"lmEntityName" NVARCHAR(128),
		"lmEntityDetail" NVARCHAR(128),
		"netRecoveryRate" DECIMAL (15,11),
		"terminationDate" DATE,
		"optionStrikeCall" DECIMAL(34, 11),
		"optionStrikePut" DECIMAL(34, 11),
		"lookbackPeriod" NVARCHAR(10),
		"rateCappingFlooring" NVARCHAR(40),
		"rateAveragingCap" DECIMAL(15, 11),
		"rateAveragingFloor" DECIMAL(15, 11),
		"dataGroup" NVARCHAR(128),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
		"structuredProductName" NVARCHAR(255),
		"writeOffAmount" DECIMAL(34,6),
		"bookValue" DECIMAL(34, 6),
		"exposureClass" NVARCHAR(255)

    	) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	 SELECT 
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			root."node_No",
			root."dataGroup"
			) AS "id",
		root."contractID" AS "sourceSystemRecordNumber",
		root."FinancialContract_FinancialContractID" AS "sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		root."node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		root."FinancialContract_OriginalSigningDate" AS "dealDate",
		MAP(root."legalEntitySwitch",
			'OrganizationalUnitContractAssignment', root."OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",
			'BusinessPartnerContractAssignment',  root."ContractDataOwner_BusinessPartnerID"
		) AS "legalEntity",
		root."counterparty",
		root."BusinessPartner_ClearingHouse_BusinessPartnerID" AS "clearingHouse",
		root."currency",
		productFSClass."productFSClass",
		contractFSType."contractFSType",
		contractFSSubType."contractFSSubType",
		"com.adweko.adapter.osx.inputdata.common::get_instrumentType"(root."StandardCatalog_ProductCatalogItem",
																		NULL,
																		NULL
		) AS "instrumentType",
		1 AS "isUnderLiquidityManagementControl",
		CASE
			WHEN root."Seller_BusinessPartnerContractAssignment_ContractDataOwner" = true THEN '2'
			WHEN root."Buyer_BusinessPartnerContractAssignment_ContractDataOwner" = true THEN '1'
		END AS "optionHolder",
		MAP(root."FinancialContract_ExerciseStyle",
			'European','1',
			'American','2',
			'Bermudan','3'
		) AS "optionExecutionType",
		MAP(root."FinancialContract_SettlementMethod",
			'PhysicalDelivery','1',
			'CashSettlement','2',
			'Auction','2'
		) AS "optionDeliverySettlement",
		root."ExercisePeriod_StartDate" AS "exerciseBeginDate",
		root."ExercisePeriod_EndDate" AS "exerciseEndDate",
		'1' AS "optionType",
		root."FinancialContract_EffectiveDate" AS "valueDate",
		root."FinancialContract_ExpirationDate" AS "maturityDate",
		ABS(	
				IFNULL(root."NotionalSchedule_NotionalAmount", root."FinancialContract_NotionalAmount")
			) * root."isActiveFlagLegSign"
		AS "currentPrincipal",
		inte."currentNominalInterestRate",
		inte."contractDayCountMethod",
		inte."calendar",
		inte."eomConvention",
		accr."accruedInterest",
		gpamount."Valuation_EstimatedMarketValue" AS "grossPayableAmountAtDefault",
		'Option' AS "lmEntityName",
		'isInterestRateOption' AS "lmEntityDetail",
		CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard"(rec_after."CreditRiskRecoveryRate_RecoveryRate", 'netRecoveryRate', kv_perc."Value") AS DECIMAL(15,11)
		) AS "netRecoveryRate",
		MAP(root."FinancialContract_LifecycleStatus",
			'Cancelled',"FinancialContract_LifecycleStatusChangeDate",
			root."FinancialContract_TerminationDate"
		) AS "terminationDate",
		inteSRS."StrikeCallOrPut" AS "optionStrikeCall",
		NULL  AS "optionStrikePut",
		inte."lookbackPeriod",
		inte."rateCappingFlooring",
		inte."rateAveragingCap",
		inte."rateAveragingFloor",
		root."dataGroup",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",
		CASE
			WHEN root."FinancialContract_FinancialContractCategory" = 'StructuredProductSubleg'
				THEN root."FinancialContract_StructuredProduct_FinancialContractID"
		END AS "structuredProductName",
		MAP(writeOffKey."Value",
                			'WriteDownAmountInPositionCurrency', writeOff."WriteDownAmountInPositionCurrency",
                			'WriteDownAmount', writeOff."WriteDownAmount"
        ) AS "writeOffAmount",
		bok."BookValue_BookValue" AS "bookValue",
		expo."exposureClass"

    FROM "com.adweko.adapter.osx.inputdata.iro::InterestRateOption_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING, 'Cap') AS root
    
    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.iro::InterestRateOption_Interest_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING, 'Cap') AS inte
            ON  inte."FinancialContract_IDSystem"					= root."FinancialContract_IDSystem"
        		AND inte."FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"

    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.iro::InterestRateOption_InterestStrikeRS_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING, 'Cap') AS inteSRS
            ON  inteSRS."FinancialContract_IDSystem"					= root."FinancialContract_IDSystem"
        		AND inteSRS."FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"

    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.iro::InterestRateOption_Accrual_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING, 'Cap') AS accr
            ON  accr."FinancialContract_IDSystem"					= root."FinancialContract_IDSystem"
        		AND accr."FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"

        LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP,'InterestRateOptionCap') AS expo
			ON expo."CreditRiskExposure_FinancialContract_IDSystem"	= root."FinancialContract_IDSystem"
			AND expo."CreditRiskExposure_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"

    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.iro::get_grossPayableAmountAtDefault"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING, 'Cap') AS gpamount
    		ON gpamount."FinancialContract_FinancialContractID"		= root."FinancialContract_FinancialContractID"
    			AND gpamount."FinancialContract_IDSystem"			= root."FinancialContract_IDSystem"
    	
    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.iro::get_RecoveryRate"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING, 'Cap') AS rec_after
    		ON rec_after."CreditRiskRecoveryRate_FinancialContract_FinancialContractID"		= root."FinancialContract_FinancialContractID"
    			AND rec_after."CreditRiskRecoveryRate_FinancialContract_IDSystem"			= root."FinancialContract_IDSystem"
    			AND rec_after."CreditRiskRecoveryRate_RecoveryRateEstimationMethod"			= 'AfterCreditEnhancements'

        LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
			ON bok."BookValue_FinancialContract_IDSystem"			= root."FinancialContract_IDSystem"
				AND bok."BookValue_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
				AND bok."BookValue_BookValueCurrency"				= IFNULL(root."NotionalSchedule_NotionalAmountCurrency",root."FinancialContract_NotionalAmountCurrency")


        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_productFSClass_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS productFSClass
            ON productFSClass."ProductCatalogItem" = root."StandardCatalog_ProductCatalogItem"

        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_contractFSType_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS contractFSType
            ON contractFSType."ProductCatalogItem"    = root."StandardCatalog_ProductCatalogItem"
            
        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_contractFSSubType_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS contractFSSubType
            ON contractFSSubType."ProductCatalogItem"    = root."StandardCatalog_ProductCatalogItem"
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv_perc
			ON  kv_perc."KeyID" = 'SAPPercentageStandard'

		--writeOffAmount
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS writeOffKey
			ON writeOffKey."KeyID" = 'WriteDownPositionCurrenyOrAmount'

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityWriteOffFC"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Loan') AS writeOff
			ON writeOff."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
			AND writeOff."FinancialContract_IDSystem" = root."FinancialContract_IDSystem";


END;