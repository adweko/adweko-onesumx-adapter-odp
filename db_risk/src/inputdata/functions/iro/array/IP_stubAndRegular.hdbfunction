FUNCTION "com.adweko.adapter.osx.inputdata.iro.array::IP_stubAndRegular" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128),
		I_CAP_FLOOR_COLLAR NVARCHAR(16),
		I_COLLAR_C_OR_F NVARCHAR(1) DEFAULT ''
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"refid" NVARCHAR(32),
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),
		"dataGroup" NVARCHAR(128)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	SELECT
		"com.adweko.adapter.osx.inputdata.common::get_array_id" (
			"com.adweko.adapter.osx.inputdata.common::get_id" (
				root."contractID",
				root."node_No",
				root."dataGroup"
			),
			'IP',
			inte."FirstInterestPeriodStartDate" -- =cycleDate"
			|| :I_COLLAR_C_OR_F
		) AS "id",
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			root."node_No",
			root."dataGroup"
		) AS "refid",
		'IP' AS "arraySpecKind",
		inte."FirstInterestPeriodStartDate" AS "cycleDate",
		CASE 
			WHEN inte."FirstInterestPeriodEndDate" = inte."FirstInterestPeriodStartDate"
				THEN
					"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
							inte."InterestPeriodLength",
							mcp."cyclePeriod")
			ELSE '999Y' 
		END AS "cyclePeriod",
		root."dataGroup"

	FROM "com.adweko.adapter.osx.inputdata.iro::InterestRateOption_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING,:I_CAP_FLOOR_COLLAR) AS root
	
	INNER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvInterestType
		ON kvInterestType."KeyID" = 'InterestRateOption_InterestType'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvBankLegFlag
		ON	kvBankLegFlag."KeyID" = 'IROBankLeg4InterestFlag'
		
	INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inte
		ON inte."OptionOfReferenceRateSpecification_ComponentNumber"								
			= MAP(:I_CAP_FLOOR_COLLAR, 
				'Cap',root."InterestRateOptionComponent_Cap_ComponentNumber",
				'Floor',root."InterestRateOptionComponent_Floor_ComponentNumber",
				'Collar', MAP(:I_COLLAR_C_OR_F,
							'C',root."InterestRateOptionComponent_Cap_ComponentNumber",
							'F',root."InterestRateOptionComponent_Floor_ComponentNumber")
			)
			AND inte."OptionOfReferenceRateSpecification_InterestRateOption_FinancialContractID"	= root."FinancialContract_FinancialContractID"
			AND inte."OptionOfReferenceRateSpecification_InterestRateOption_IDSystem" 				= root."FinancialContract_IDSystem"
			AND inte."InterestCategory"					= 'InterestPeriodSpecification'
			AND inte."InterestType" 					= kvInterestType."Value"
			AND	inte."FirstInterestPeriodEndDate"		IS NOT NULL
			AND	(
				inte."LastInterestPeriodEndDate"		IS NULL
				OR
				inte."LastInterestPeriodEndDate"		> :I_BUSINESS_DATE
				AND inte."LastInterestPeriodEndDate"    > inte."FirstInterestPeriodEndDate"
			)
			AND inte."InterestPeriodLength"				IS NOT NULL
			AND inte."InterestPeriodTimeUnit"			IS NOT NULL
			AND ( kvBankLegFlag."Value" = 'Off' 	OR	inte."RoleOfPayer"	= root."BankLegFlag" )
				
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" = inte."InterestPeriodTimeUnit"
	
	;
END;
