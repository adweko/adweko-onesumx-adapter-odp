FUNCTION "com.adweko.adapter.osx.inputdata.interestBearingSwap.ccswap::CrossCurrencySwapMain_Contract"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP, 
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"legalEntity" NVARCHAR(128),
		"counterparty" NVARCHAR(128),
		"clearingHouse" NVARCHAR(128),
		"currency" NVARCHAR(3),
		"productFSClass" NVARCHAR(128),
		"contractFSType" NVARCHAR(256),
		"instrumentType" NVARCHAR(128),
		"isUnderLiquidityManagementControl" INT,
		"valueDate" DATE,
		"maturityDate" DATE,
		"currentPrincipal" DECIMAL(34, 6),
		"currentNominalInterestRate" DECIMAL(15, 11),
		"contractDayCountMethod" NVARCHAR(2),
		"calendar" NVARCHAR(75),
		"eomConvention" NVARCHAR(1),
		"interestPaymentType" NVARCHAR(1),
		"businessDayConvention" NVARCHAR(2),
		"terminationDate" DATE,
		"notionalExchangeType" NVARCHAR(1),
		"accruedInterest" DECIMAL(38, 11),
		"lookbackPeriod" NVARCHAR(10),
		"rateCappingFlooring" NVARCHAR(40),
		"rateAveragingCap" DECIMAL(15, 11),
		"rateAveragingFloor" DECIMAL(15, 11),
		"dataGroup" NVARCHAR(128),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
		"structuredProductName" NVARCHAR(255),
		"bookValue" DECIMAL(34, 6),
		"exposureClass" NVARCHAR(255)
		
    	) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	 SELECT 
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			root."node_No",
			root."dataGroup"
			) AS "id",
		root."contractID" AS "sourceSystemRecordNumber",
		root."FinancialContract_FinancialContractID" AS "sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		root."node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		root."FinancialContract_OriginalSigningDate" AS "dealDate",
		MAP(root."legalEntitySwitch",
			'OrganizationalUnitContractAssignment', root."OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",
			'BusinessPartnerContractAssignment',  root."ContractDataOwner_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
		) AS "legalEntity",
		root."counterparty",
		root."BusinessPartner_ClearingHouse_BusinessPartnerID" AS "clearingHouse",
		root."currency",
		productFSClass."productFSClass",
		contractFSType."contractFSType",
		"com.adweko.adapter.osx.inputdata.common::get_instrumentType"(root."StandardCatalog_ProductCatalogItem",
																		NULL,
																		NULL
		) AS "instrumentType",
		1 AS "isUnderLiquidityManagementControl",
		NULL AS "valueDate",
		NULL AS "maturityDate",
		NULL AS "currentPrincipal",
		NULL AS "currentNominalInterestRate",
		NULL AS "contractDayCountMethod",
		NULL AS "calendar",
		NULL AS "eomConvention",
		NULL AS "interestPaymentType",
		NULL AS "businessDayConvention",
		root."FinancialContract_TerminationDate" AS "terminationDate",
		"com.adweko.adapter.osx.inputdata.common::get_notionalExchangeType" (
			"FinancialContract_PrincipalExchangeAtStart",
			"FinancialContract_PrincipalExchangeAtEnd",
			"FinancialContract_InterimPrincipalExchange"
		) AS "notionalExchangeType",
		NULL AS "accruedInterest",
		NULL AS "lookbackPeriod",
		NULL AS "rateCappingFlooring",
		NULL AS "rateAveragingCap",
		NULL AS "rateAveragingFloor",
		root."dataGroup",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",
		CASE
			WHEN root."FinancialContract_FinancialContractCategory" = 'StructuredProductSubleg'
				THEN root."FinancialContract_StructuredProduct_FinancialContractID"
		END AS "structuredProductName",
		bok."BookValue_BookValue" AS "bookValue",
		expo."exposureClass"

    FROM "com.adweko.adapter.osx.inputdata.interestBearingSwap.ccswap::CrossCurrencySwap_RootMain_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS root

    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP,'CrossCurrencySwap') AS expo
			ON expo."CreditRiskExposure_FinancialContract_IDSystem"	= root."FinancialContract_IDSystem"
			AND expo."CreditRiskExposure_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"

    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
			ON bok."BookValue_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
			AND bok."BookValue_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
			AND bok."BookValue_BookValueCurrency" = root."currency"

        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_productFSClass_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS productFSClass
            ON productFSClass."ProductCatalogItem" = root."StandardCatalog_ProductCatalogItem"

        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_contractFSType_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS contractFSType
            ON contractFSType."ProductCatalogItem"    = root."StandardCatalog_ProductCatalogItem"

        ;
END;
