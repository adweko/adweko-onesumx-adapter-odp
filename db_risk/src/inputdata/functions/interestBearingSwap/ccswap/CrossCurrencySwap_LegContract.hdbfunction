FUNCTION "com.adweko.adapter.osx.inputdata.interestBearingSwap.ccswap::CrossCurrencySwapLeg_Contract"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP, 
		I_DATAGROUP_STRING NVARCHAR(128),
		I_LEG NVARCHAR(4)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"legalEntity" NVARCHAR(128),
		"counterparty" NVARCHAR(128),
		"clearingHouse" NVARCHAR(128),
		"currency" NVARCHAR(3),
		"productFSClass" NVARCHAR(128),
		"contractFSType" NVARCHAR(256),
		"instrumentType" NVARCHAR(128),
		"isUnderLiquidityManagementControl" INT,
		"valueDate" DATE,
		"maturityDate" DATE,
		"currentPrincipal" DECIMAL(34, 6),
		"currentNominalInterestRate" DECIMAL(15, 11),
		"contractDayCountMethod" NVARCHAR(2),
		"calendar" NVARCHAR(75),
		"eomConvention" NVARCHAR(1),
		"interestPaymentType" NVARCHAR(1),
		"businessDayConvention" NVARCHAR(2),
		"terminationDate" DATE,
		"notionalExchangeType" NVARCHAR(1),
		"accruedInterest" DECIMAL(38, 11),
		"lookbackPeriod" NVARCHAR(10),
		"rateCappingFlooring" NVARCHAR(40),
		"rateAveragingCap" DECIMAL(15, 11),
		"rateAveragingFloor" DECIMAL(15, 11),
		"dataGroup" NVARCHAR(128),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
		"bookValue" DECIMAL(34, 6),
		"exposureClass" NVARCHAR(255)

    	) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	 SELECT 
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			root."node_No",
			root."dataGroup"
			) AS "id",
		root."contractID" AS "sourceSystemRecordNumber",
		root."FinancialContract_FinancialContractID" AS "sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		root."node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		root."FinancialContract_OriginalSigningDate" AS "dealDate",
		NULL AS "legalEntity",
		root."counterparty",
		NULL AS "clearingHouse",
		root."currency",
		productFSClass."productFSClass",
		contractFSType."contractFSType",
		"com.adweko.adapter.osx.inputdata.common::get_instrumentType"(root."StandardCatalog_ProductCatalogItem",
																		NULL,
																		NULL
		) AS "instrumentType",
		1 AS "isUnderLiquidityManagementControl",
		root."FinancialContract_EffectiveDate" AS "valueDate",
		root."FinancialContract_MaturityDate" AS "maturityDate",
		(	ABS(	
				IFNULL(root."NotionalSchedule_NotionalAmount",
					MAP(:I_LEG,
						'Leg1', root."FinancialContract_Leg1NotionalAmount",
						'Leg2', root."FinancialContract_Leg2NotionalAmount"
					)
				)
			) * inte."isActiveFlagLegSign"
		) AS "currentPrincipal",
		inte."currentNominalInterestRate",
		inte."contractDayCountMethod",
		inte."calendar",
		inte."eomConvention",
		inte."interestPaymentType",
		inte."businessDayConvention",
		root."FinancialContract_TerminationDate" AS "terminationDate",
		NULL AS "notionalExchangeType",
		accr."accruedInterest",
		inte."lookbackPeriod",
		inte."rateCappingFlooring",
		inte."rateAveragingCap",
		inte."rateAveragingFloor",
		root."dataGroup",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",
		bok."BookValue_BookValue" AS "bookValue",
		expo."exposureClass"

    FROM "com.adweko.adapter.osx.inputdata.interestBearingSwap.ccswap::CrossCurrencySwap_RootLeg_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING, :I_LEG) AS root
    
    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.interestBearingSwap.ccswap::CrossCurrencySwap_Interest_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING, :I_LEG) AS inte
            ON  inte."FinancialContract_IDSystem"					= root."FinancialContract_IDSystem"
        		AND inte."FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"
        		
    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.interestBearingSwap.ccswap::CrossCurrencySwap_Accrual_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING, :I_LEG) AS accr
            ON  accr."FinancialContract_IDSystem"					= root."FinancialContract_IDSystem"
        		AND accr."FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"

        LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP,'CrossCurrencySwap') AS expo
			ON expo."CreditRiskExposure_FinancialContract_IDSystem"	= root."FinancialContract_IDSystem"
			AND expo."CreditRiskExposure_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"

        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_productFSClass_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS productFSClass
            ON productFSClass."ProductCatalogItem" = root."StandardCatalog_ProductCatalogItem"

        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_contractFSType_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS contractFSType
            ON contractFSType."ProductCatalogItem"    = root."StandardCatalog_ProductCatalogItem"

        LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
			ON bok."BookValue_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
			AND bok."BookValue_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
			AND bok."BookValue_BookValueCurrency" = root."currency"

        ;
END;
