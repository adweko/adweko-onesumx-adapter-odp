FUNCTION "com.adweko.adapter.osx.inputdata.partner::Counterparty" (
		IN I_BUSINESS_DATE DATE,
		IN I_SYSTEM_TIMESTAMP TIMESTAMP,
		IN I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"partyKey" NVARCHAR(256),
		"sourceSystemName" NVARCHAR(128),
		"dataGroup" NVARCHAR(128),
		"partyLegalName" NVARCHAR(256),
		"description" NVARCHAR(40),
		"probabilityOfDefault" DECIMAL(15, 11),
		"isSovereign" INT,
		"countryOfLegalDomicileType" NVARCHAR(2),
		"currency" NVARCHAR(3),
		"yearlyRevenue" DECIMAL(34, 6),
		"totalSales" DECIMAL(34, 6),
		"segment" NVARCHAR(40),
		"riskSensitivityProfile" NVARCHAR(40),
		"crCadCounterpartyClass" NVARCHAR(40),
		"mrCadCounterpartyClass" NVARCHAR(40),
/*  ---------------------------------
	User Defined Attributes :
*/ ---------------------------------		
		"countryOperation" NVARCHAR(2),
		"isSovereignEquivalent" NVARCHAR(5),
		"sectorCode" NVARCHAR(40),
		"specialCounterpartyType" NVARCHAR(40),
		"isRegulatedInstitution" INT,
		"liquiditySubType" NVARCHAR(40),
		"isEstablishedRelationship" INT,
		"hasFiscalAutonomy" INT,
		"leiCode" NVARCHAR(256),
		"ultimateParent" NVARCHAR(128),
		"nationalIdentifierCode" NVARCHAR(128),
		"isNonPerforming" INT,
		"totalAssets" DECIMAL(34, 6),
		"eligibleType" NVARCHAR(75),
		"counterpartySubClass" NVARCHAR(255),
		"tier1LeverageRatio" DECIMAL(15, 11),
		"commonEquityTier1Ratio" DECIMAL(15, 11),
		"isInvestmentGrade" NVARCHAR(75),
		"sourceOfIncomeCurrency" NVARCHAR(3),
		"transactor" NVARCHAR(5)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN

	SELECT 
		"com.adweko.adapter.osx.inputdata.common::get_counterparty_id" (
			root."partnerID",
			root."dataGroup"
			) AS "id",
		root."partnerID" AS "partyKey",
		root."sourceSystemName",
		root."dataGroup",
		root."partyLegalName",
		root."description",
		root."probabilityOfDefault",
		root."isSovereign",
		root."countryOfLegalDomicileType",
		root."currency",
		root."yearlyRevenue",
		root."totalSales",
		root."segment",
		root."riskSensitivityProfile",
		root."crCadCounterpartyClass",
		root."mrCadCounterpartyClass",
		root."countryOperation",
		root."isSovereignEquivalent",
		root."sectorCode",
		root."specialCounterpartyType",
		root."isRegulatedInstitution",
		root."liquiditySubType",
		root."isEstablishedRelationship",
		root."hasFiscalAutonomy",
		root."leiCode",
		root."ultimateParent",
		root."nationalIdentifierCode",
		root."isNonPerforming",
		root."totalAssets",
		root."eligibleType",
		root."counterpartySubClass",
		root."tier1LeverageRatio",
		root."commonEquityTier1Ratio",
		root."isInvestmentGrade",
		root."sourceOfIncomeCurrency",
		root."transactor"
	FROM "com.adweko.adapter.osx.inputdata.partner::Counterparty_get_Root" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) AS root
	;
END;
