FUNCTION "com.adweko.adapter.osx.inputdata.loans::get_cashflow" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP
		)
RETURNS TABLE (
	"IDSystem" NVARCHAR(40),
	"FinancialContractID" NVARCHAR(128),
	"FinancialInstrumentID" NVARCHAR(128),
	"SequenceNumber" INT,
	"arraySpecKind" NVARCHAR(5),
	"anchor" DATE,
	"amount" DECIMAL(34,6),
	"currency" NVARCHAR(3),
	"cashFlowKind" NVARCHAR(1)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*************************************************
					get cashflow for financial contract
	**************************************************/
RETURN

	SELECT DISTINCT 
		cfs."ASSOC_Contract_IDSystem"							AS "IDSystem",
		cfs."ASSOC_Contract_FinancialContractID"				AS "FinancialContractID",
		cfs."_Instrument_FinancialInstrumentID"					AS "FinancialInstrumentID",
		cfs."ItemNumber"										AS "SequenceNumber",
		'CFS'													AS "arraySpecKind",
		cfs."ValueDate"											AS "anchor",
		COALESCE(cfs."AmountInPositionCurrency", cfs."AmountInTransactionCurrency") AS "amount",
		CASE
			WHEN cfs."AmountInPositionCurrency" IS NOT NULL 
				THEN cfs."PositionCurrency"
			ELSE cfs."TransactionCurrency" 
		END AS "currency",
		'1'													AS "cashFlowKind"
	FROM "com.adweko.adapter.osx.synonyms::CashFlowStream_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) as cfs
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
		ON  kv."KeyID" = 'LoanCFSScenario'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv2
		ON  kv2."KeyID" = 'LoanCFSSourceSystem'
	WHERE "CashFlowItemType" = 'PrincipalRepayment'
		AND "CashFlowScenario" = kv."Value"
		AND "SourceSystemID" = kv2."Value"

	UNION ALL
	
	SELECT DISTINCT 
		cfs."ASSOC_Contract_IDSystem"							AS "IDSystem",
		cfs."ASSOC_Contract_FinancialContractID"				AS "FinancialContractID",
		cfs."_Instrument_FinancialInstrumentID"					AS "FinancialInstrumentID",
		cfs."ItemNumber"										AS "SequenceNumber",
		'CFS'													AS "arraySpecKind",
		cfs."ValueDate"											AS "anchor",
		COALESCE(cfs."AmountInPositionCurrency", cfs."AmountInTransactionCurrency") AS "amount",
		CASE
			WHEN cfs."AmountInPositionCurrency" IS NOT NULL 
				THEN cfs."PositionCurrency"
			ELSE cfs."TransactionCurrency" 
		END AS "currency",
		'2'													AS "cashFlowKind"
	FROM "com.adweko.adapter.osx.synonyms::CashFlowStream_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) as cfs
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
		ON  kv."KeyID" = 'LoanCFSScenario'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv2
		ON  kv2."KeyID" = 'LoanCFSSourceSystem'
	WHERE "CashFlowItemType" = 'InterestPayments' 
		AND "CashFlowScenario" = kv."Value"
		AND "SourceSystemID" = kv2."Value";
END;