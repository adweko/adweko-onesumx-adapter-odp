FUNCTION "com.adweko.adapter.osx.inputdata.loans.syndicatedLoan::Loan_get_Position" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"FinancialContract_IDSystem" NVARCHAR(40),
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"accruedInterest" DECIMAL(38, 11),
		"currentPrincipal" DECIMAL(34, 6),
		"MonetaryBalance_RequestedNotDisbursedPrincipal" DECIMAL(34, 6),
		"recoveredAmount" DECIMAL(34, 6),
		"pastDueAmount" DECIMAL(34, 6),
		"FirstDisbursementDate" DATE,
		"residualAmount" DECIMAL(34, 6),
		"ContractCurrency" NVARCHAR(3)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN

	SELECT DISTINCT pos."FinancialContract_IDSystem",
		pos."FinancialContract_FinancialContractID",
		CAST(pos."Accrual_AccruedInterest" AS DECIMAL(38, 11)) AS "accruedInterest",
	    CASE
		    WHEN loa."IsActive" = 'TRUE' THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" ("MonetaryBalance_OutstandingPrincipal") 
		    WHEN loa."IsActive" = 'FALSE' THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" ("MonetaryBalance_OutstandingPrincipal") 
		    ELSE NULL
		END AS "currentPrincipal",
		CAST(pos."MonetaryBalance_RequestedNotDisbursedPrincipal" AS DECIMAL(34, 6)) AS "MonetaryBalance_RequestedNotDisbursedPrincipal",
		pos."MonetaryBalance_RepaidPrincipalSinceInception" AS "recoveredAmount",
		pos."MonetaryBalance_TotalPastDue" AS "pastDueAmount",
		pos."MonetaryBalance_FirstDisbursementDate" AS "FirstDisbursementDate",
		CASE
		    WHEN loa."IsActive" = 'TRUE' THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" (pos."RefinancingRelation_RefinancedAmountInPositionCurrency") 
		    WHEN loa."IsActive" = 'FALSE' THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" (pos."RefinancingRelation_RefinancedAmountInPositionCurrency") 
		    ELSE NULL
		END AS "residualAmount",
		pos."ContractCurrency"

	FROM "com.adweko.adapter.osx.inputdata.loans::BV_LoanPosition" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING,
			'SyndicatedLoan'
			) pos
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::SyndicatedLoan_Root" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) loa ON pos."FinancialContract_IDSystem" = loa."FinancialContract_IDSystem"
		AND pos."FinancialContract_FinancialContractID" = loa."FinancialContract_FinancialContractID"
		AND pos."FinancialContract_TrancheInSyndication_SyndicationAgreement_FinancialContractID" IS NOT NULL;
END;