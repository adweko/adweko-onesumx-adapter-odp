FUNCTION "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_InterestPrio_Contract"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128),
		I_FILTER_INTEREST_WITH_STUB NVARCHAR(10)
		)
		RETURNS TABLE(
       	"FinancialContract_IDSystem" NVARCHAR(40),
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"Interest_InterestType" NVARCHAR(40),
		"Interest_FirstInterestPeriodStartDate" DATE,
		"Interest_FirstInterestPeriodEndDate" DATE,
		"Interest_ConditionFixedPeriodEndDate" DATE,
		"Interest_LastInterestPeriodEndDate" DATE,
		"Interest_FirstDueDate" DATE,
		"Interest_InterestSubPeriodSpecificationCategory" NVARCHAR(100),
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),		
		"currentNominalInterestRate" DECIMAL(15, 11),
		"repricingType" NVARCHAR(1),
		"pricingMarketObject" NVARCHAR(40),
		"rateAdd" DECIMAL(15, 11),
		"rateMultiplier" DECIMAL(15, 11),
		"rateTerm" NVARCHAR(10),
		"fixingDays" NVARCHAR(10),
		"contractDayCountMethod" NVARCHAR(2),
		"interestPaymentType" NVARCHAR(1),
		"businessDayConvention" NVARCHAR(2),
		"calendar" NVARCHAR(75),
		"eomConvention" NVARCHAR(1),
		"gracePeriodStartDate" DATE,
		"gracePeriodEndDate" DATE,
		"capitalizationEndDate" DATE,
		"Interest_SequenceNumber" INT,
		"MonetaryBalance_CapitalizedInterest" DECIMAL(34, 6),
		"lookbackPeriod" NVARCHAR(10),
		"rateCappingFlooring" NVARCHAR(40),
		"rateAveragingCap" DECIMAL(15, 11),
		"rateAveragingFloor" DECIMAL(15, 11)
       )
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 

RETURN
	SELECT
		"FinancialContract_IDSystem",
		"FinancialContract_FinancialContractID",
		"Interest_InterestType",
		"Interest_FirstInterestPeriodStartDate",
		"Interest_FirstInterestPeriodEndDate",
		"Interest_ConditionFixedPeriodEndDate",
		"Interest_LastInterestPeriodEndDate",
		"Interest_FirstDueDate",
		"Interest_InterestSubPeriodSpecificationCategory",
		"arraySpecKind",
		"cycleDate",
		"cyclePeriod",	
		"currentNominalInterestRate",
		"repricingType",
		"pricingMarketObject",
		"rateAdd",
		"rateMultiplier",
		"rateTerm",
		"fixingDays",
		"contractDayCountMethod",
		"interestPaymentType",
		"businessDayConvention",
		"calendar",
		"eomConvention",
		"gracePeriodStartDate",
		"gracePeriodEndDate",
		"capitalizationEndDate" ,
		"Interest_SequenceNumber",
		"MonetaryBalance_CapitalizedInterest",
		"lookbackPeriod",
		"rateCappingFlooring",
		"rateAveragingCap",
		"rateAveragingFloor"
	FROM
		(SELECT 
				"FinancialContract_IDSystem",
				"FinancialContract_FinancialContractID",
				"Interest_InterestType",
				"Interest_FirstInterestPeriodStartDate",
				"Interest_FirstInterestPeriodEndDate",
				"Interest_ConditionFixedPeriodEndDate",
				"Interest_LastInterestPeriodEndDate",
				"Interest_FirstDueDate",
				"Interest_InterestSubPeriodSpecificationCategory",
				"arraySpecKind",
				"cycleDate",
				"cyclePeriod",	
				"currentNominalInterestRate",
				"repricingType",
				"pricingMarketObject",
				"rateAdd",
				"rateMultiplier",
				"rateTerm",
				"fixingDays",
				"contractDayCountMethod",
				"interestPaymentType",
				"businessDayConvention",
				"calendar",
				"eomConvention",
				"gracePeriodStartDate",
				"gracePeriodEndDate",
				"capitalizationEndDate" ,
				"Interest_SequenceNumber",
				"MonetaryBalance_CapitalizedInterest",
				"lookbackPeriod",
				"rateCappingFlooring",
				"rateAveragingCap",
				"rateAveragingFloor",
				ROW_NUMBER() OVER	(	PARTITION BY	"FinancialContract_FinancialContractID"
												ORDER BY "Interest_FirstInterestPeriodStartDate", "OrderPrio"
												)
												AS "Priority"
		FROM (
			SELECT 
				"FinancialContract_IDSystem",
				"FinancialContract_FinancialContractID",
				"Interest_InterestType",
				"Interest_FirstInterestPeriodStartDate",
				"Interest_FirstInterestPeriodEndDate",
				"Interest_ConditionFixedPeriodEndDate",
				"Interest_LastInterestPeriodEndDate",
				"Interest_FirstDueDate",
				"Interest_InterestSubPeriodSpecificationCategory",
				"arraySpecKind",
				"cycleDate",
				"cyclePeriod",	
				"currentNominalInterestRate",
				"repricingType",
				"pricingMarketObject",
				"rateAdd",
				"rateMultiplier",
				"rateTerm",
				"fixingDays",
				"contractDayCountMethod",
				"interestPaymentType",
				"businessDayConvention",
				"calendar",
				"eomConvention",
				"gracePeriodStartDate",
				"gracePeriodEndDate",
				"capitalizationEndDate" ,
				"Interest_SequenceNumber",
				"MonetaryBalance_CapitalizedInterest",
				"lookbackPeriod",
				"rateCappingFlooring",
				"rateAveragingCap",
				"rateAveragingFloor",
				CASE
					WHEN "cyclePeriod" != '999Y'
						THEN 1
					ELSE 2
				END AS "OrderPrio"
			FROM "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Interest"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) as inte
			WHERE "arraySpecKind" = 'IP'
		--	AND inte."Interest_FirstInterestPeriodStartDate" <= :I_BUSINESS_DATE
			AND	 inte."Interest_LastInterestPeriodEndDate" > :I_BUSINESS_DATE
			AND (:I_FILTER_INTEREST_WITH_STUB = 'false' OR (:I_FILTER_INTEREST_WITH_STUB = 'true' AND inte."Interest_FirstInterestPeriodEndDate" != inte."Interest_FirstInterestPeriodStartDate"))
		))
	WHERE "Priority" = 1;
END;
