FUNCTION "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"FinancialContract_IDSystem" NVARCHAR(40),
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"dealDate" DATE,
		"valueDate" DATE,
		"maturityDate" DATE,
		"FinancialContract_AmortizationType" NVARCHAR (40),
		"counterparty" NVARCHAR(128),
		"legalEntity" NVARCHAR(128),
		"syndicationName" NVARCHAR(255),
		"FinancialContract_ApprovedNominalAmount" DECIMAL(34, 6),
		"nonPerformingDate" DATE,
		"clearingHouse" NVARCHAR(128),
		"FinancialContract_FacilityOfDrawing_IDSystem" NVARCHAR(40),
		"FinancialContract_FacilityOfDrawing_FinancialContractID" NVARCHAR(128),
		"impairmentType" INT,
		"netRecoveryRate" DECIMAL (15,11),
		"grossRecoveryRate" DECIMAL (15,11),
		"ContractStatus_DefaultStatus_Status" NVARCHAR(100),
		"contractFSType" NVARCHAR(256),
		"contractFSSubType" NVARCHAR(256),
		"productFSClass" NVARCHAR(128),
		"instrumentType" NVARCHAR(128),
		"financialInstrumentType" INT, 
		"ProductCatalogItemCode" NVARCHAR(128),
		"ProductCatalogID" NVARCHAR(128),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
        "spreadCurveDefinition" NVARCHAR(75),
		"IsActive" NVARCHAR(5),
		"isPerforming" INT,
		"BusinessPartnerContractAssignment_Borrower_ContractDataOwner" BOOLEAN,
		"BusinessPartnerContractAssignment_Lender_ContractDataOwner" BOOLEAN,
		"isin" NVARCHAR(12),
		"commitmentType" NVARCHAR(75),
		"productType"  NVARCHAR(75),
		"portfolioType" NVARCHAR(255),
		"seniority" NVARCHAR(75),
		"contractRole" NVARCHAR(1),
		"eligibleType" NVARCHAR(75),
		"crRiskWeight" DECIMAL (15,11),
		"cadCrConversionFactor" NVARCHAR(75),--data type jsut assumed
		"UDA_IS_RETAIL_LOWER_RW_COMPLIANT" INT,
		"UDA_NOMINAL_COVERAGE_PERCENT" DECIMAL(15, 11),
		"contractID" NVARCHAR(256),
		"exposureClass" NVARCHAR(255),
		"dataGroup" NVARCHAR(128)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN

	SELECT DISTINCT
		loa."FinancialContract_IDSystem",
		loa."FinancialContract_FinancialContractID",
		"FinancialContract_OriginalSigningDate" AS "dealDate",
		COALESCE("FinancialContract_FixedPeriodStartDate","FinancialContract_OriginalSigningDate") AS "valueDate",
		"FinancialContract_ExpectedMaturityDate" AS "maturityDate",
		"FinancialContract_AmortizationType",
		
		CASE
		    WHEN loa."BusinessPartnerContractAssignment_Borrower_ContractDataOwner" = false
		    AND loa."BusinessPartnerContractAssignment_Borrower_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
		        THEN loa."BusinessPartnerContractAssignment_Borrower_PartnerInParticipation_BusinessPartnerID"
		    WHEN loa."BusinessPartnerContractAssignment_Lender_ContractDataOwner" = false
		    AND loa."BusinessPartnerContractAssignment_Lender_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
		        THEN loa."BusinessPartnerContractAssignment_Lender_PartnerInParticipation_BusinessPartnerID"
		    ELSE
		        NULL
	       END AS "counterparty",
	       CASE 
				WHEN (keylegalEntity."Value"='OrganizationalUnitContractAssignment' 
					AND "OrganizationalUnitContractAssignment_OrgUnit_OrganizationHostingOrganizationalUnit_BusinessPartnerID"<>' ')
					OR keylegalEntity."Value" IS NULL
						THEN "OrganizationalUnitContractAssignment_OrgUnit_OrganizationHostingOrganizationalUnit_BusinessPartnerID"
				WHEN keylegalEntity."Value"='BusinessPartnerContractAssignment'
					AND "BusinessPartnerContractAssignment_ContractDataOwner_PartnerInParticipation_BusinessPartnerID"<>' '
						THEN "BusinessPartnerContractAssignment_ContractDataOwner_PartnerInParticipation_BusinessPartnerID"
				ELSE
					NULL
			END AS "legalEntity",
			"FinancialContract_FacilityOfDrawing_FinancialContractID" AS "syndicationName",
			CAST("FinancialContract_ApprovedNominalAmount" AS DECIMAL(34, 6)) AS "FinancialContract_ApprovedNominalAmount",
			CASE 
				WHEN "ContractStatus_PastDueStatus_Status" IN (
					'PastDueLongerThanDefaultRelevantThreshold',
					'PastDueNotLongerThanDefaultRelevantThreshold'
					)
					THEN "ContractStatus_PastDueStatus_BusinessValidFrom"
				ELSE NULL
			END AS "nonPerformingDate",
			"BusinessPartnerContractAssignment_ClearingMember_PartnerInParticipation_BusinessPartnerID" AS "clearingHouse",
			"FinancialContract_FacilityOfDrawing_IDSystem",
			"FinancialContract_FacilityOfDrawing_FinancialContractID",

			MAP(cra_imp."CreditRiskAdjustment_CalculationMethod",
			'IndividuallyAssessed', 1,
			'CollectivelyAssessed', 2 ) AS "impairmentType",
            CAST(
			CASE
				WHEN rec."CreditEnhancementStep" = 'After'
	            	THEN rec."CreditRiskRecoveryRate_RecoveryRate"
				ELSE NULL
			END AS DECIMAL(15,11)
			) AS "netRecoveryRate",
            CAST(
			CASE
				WHEN rec."CreditEnhancementStep" = 'Before'
	            	THEN rec."CreditRiskRecoveryRate_RecoveryRate"
				ELSE NULL
			END AS DECIMAL(15,11)
			) AS "grossRecoveryRate",
			"ContractStatus_DefaultStatus_Status",
			contractFSType."contractFSType" AS "contractFSType",
			"com.adweko.adapter.osx.inputdata.loans.loan::Loan_get_ContractFSSubtype"(
				loa."StandardCatalog_ProductCatalogItem",
				"FinancialContract_PromotionalLoanProgramID",
				"FinancialContract_PartOfPromotionalLoanProgram",
				"FinancialContract_PurposeType",
				"BusinessPartnerContractAssignment_Borrower_ContractDataOwner",
				"IndustryClassAssignment_Industry",
				"IndustryClassAssignment_IndustryClassificationSystem") AS "contractFSSubType",
			productFSClass."productFSClass"	AS "productFSClass",
			CASE -- filter for ps."PaymentSchedule_SequenceNumber" = max_seq."Max_SequenceNumber" added in where condition underneath
				WHEN ps."PaymentSchedule_PaymentToBeRequested" = true and (ps."PaymentSchedule_PartialPaymentRequestsPermitted" = false or ps."PaymentSchedule_PartialPaymentRequestsPermitted" IS NULL)
					THEN 'LoanCall'
				WHEN ps."PaymentSchedule_PaymentToBeRequested" = true and ps."PaymentSchedule_PartialPaymentRequestsPermitted" = true
					THEN 'LoanCall'
				WHEN (ps."PaymentSchedule_PaymentToBeRequested" = false or ps."PaymentSchedule_PaymentToBeRequested" IS NULL) and ps."PaymentSchedule_PartialPaymentRequestsPermitted" = true
					THEN 'LoanCall'
				ELSE 'LoanTerm'
			END AS "instrumentType",
			ghc."financialInstrumentType"			AS 	"financialInstrumentType",	
			loa."ProductCatalogAssignment_ProductCatalogItem_ProductCatalogItem" AS "ProductCatalogItemCode",
			loa."ProductCatalogAssignment_ProductCatalogItem_ProductCatalog_CatalogID" AS "ProductCatalogID",
			loa."StandardCatalog_ProductCatalogItem",
			loa."CustomCatalog_ProductCatalogItem",
             spc."spreadCurveDefinition",
			loa."IsActive",
			"com.adweko.adapter.osx.inputdata.common::get_isPerforming"(loa."ContractStatus_PastDueStatus_Status") AS "isPerforming",
			loa."BusinessPartnerContractAssignment_Borrower_ContractDataOwner",
			loa."BusinessPartnerContractAssignment_Lender_ContractDataOwner",
			COALESCE(loa."FinancialInstrument_ISIN",loa."FinancialInstrument_SEDOL",loa."FinancialInstrument_FIGI",loa."FinancialInstrument_CUSIP",loa."FinancialInstrument_WKN") AS "isin",
			'2' AS "commitmentType",
			expop."CreditRiskExposure_ExposureClass" AS "productType",
			portfolioType."portfolioType",
			loa."FinancialContract_Seniority" AS "seniority",
			'1' AS "contractRole",
			NULL AS "eligibleType", --but provisonary mapping rdy (Probem: duplicates data) loa."eligibleType",
			COALESCE("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard"(expop."CreditRiskExposure_RiskWeight", 'crRiskWeight', kv."Value"), c_expo."C_CreditRiskExposure_RiskWeight") AS "crRiskWeight",
			CASE
				WHEN expop."CreditRiskExposure_CreditRiskCalculationMethod" = 'IntRatingBasedAppr'
					THEN expop."CreditRiskExposure_CreditConversionFactorClass"
				ELSE NULL
			END AS "cadCrConversionFactor",
			loa."UDA_IS_RETAIL_LOWER_RW_COMPLIANT",
			CASE
				WHEN mb."OutstandingPrincipal" > 0 THEN
					CASE
						WHEN loa."CollateralPoolAssetAssignment_AmountAssignedToPool" IS NOT NULL
							THEN LEAST((loa."CollateralPoolAssetAssignment_AmountAssignedToPool" / mb."OutstandingPrincipal")*100, 100)
						ELSE 0
					END
				ELSE 0
			END AS "UDA_NOMINAL_COVERAGE_PERCENT",
			"com.adweko.adapter.osx.inputdata.common::get_contractID" (
				'F',
				'',
				loa."FinancialContract_FinancialContractID",
				loa."FinancialContract_IDSystem",
				''
			) AS "contractID",
			expop."exposureClass",
			loa."FinancialContract_DataGroup" AS "dataGroup"

		FROM "com.adweko.adapter.osx.inputdata.loans::ComplexLoan_Root" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS loa
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS keylegalEntity 
			ON keylegalEntity."KeyID" = 'legalEntitySwitch'
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS keyCreditRisk 
			ON keyCreditRisk."KeyID" = 'CreditRiskCustomerSwitch'
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
			ON  kv."KeyID" = 'SAPPercentageStandard'
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP,'Loan') AS expop 
			ON expop."CreditRiskExposure_FinancialContract_IDSystem" = loa."FinancialContract_IDSystem"
			AND expop."CreditRiskExposure_FinancialContract_FinancialContractID" = loa."FinancialContract_FinancialContractID"
			AND keyCreditRisk."Value" = 'Normal'
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::C_BV_CreditRiskExposure" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS c_expo 
			ON c_expo."C_CreditRiskExposure_FinancialContract_IDSystem" = loa."FinancialContract_IDSystem"
			AND c_expo."C_CreditRiskExposure_FinancialContract_FinancialContractID" = loa."FinancialContract_FinancialContractID"
			AND keyCreditRisk."Value" = 'Customer'

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskAdjustmentPrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Impairment', 'Loan') AS cra_imp
			ON cra_imp."CreditRiskAdjustment_FinancialContract_IDSystem" = loa."FinancialContract_IDSystem"
			AND cra_imp."CreditRiskAdjustment_FinancialContract_FinancialContractID" = loa."FinancialContract_FinancialContractID"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_recoveryRate" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'ComplexLoan') AS rec
             ON rec."CreditRiskRecoveryRate_FinancialContract_IDSystem" = loa."FinancialContract_IDSystem"
             AND rec."CreditRiskRecoveryRate_FinancialContract_FinancialContractID" = loa."FinancialContract_FinancialContractID"
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_productFSClass_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS productFSClass
		ON productFSClass."ProductCatalogItem" = loa."StandardCatalog_ProductCatalogItem"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_contractFSType_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS contractFSType
		ON contractFSType."ProductCatalogItem" = loa."StandardCatalog_ProductCatalogItem"

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_spreadCurveDefinition"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS spc
		ON spc."_ReferenceRate_ReferenceRateID" = loa."FinancialContract_ReferenceRateID"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_holdingCategory"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS ghc
		ON	ghc."_FinancialContract_IDSystem" = loa."FinancialContract_IDSystem"	AND
			ghc."_FinancialContract_FinancialContractID" = loa."FinancialContract_FinancialContractID"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_PaymentSchedule" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS ps
         	ON ps."PaymentSchedule_IDSystem" = loa."FinancialContract_IDSystem"
         	AND ps."PaymentSchedule_FinancialContractID" = loa."FinancialContract_FinancialContractID"
         
		LEFT OUTER JOIN  "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_maxSequenceNumber" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS max_seq
			ON  max_seq."FinancialContract_IDSystem"			 = loa."FinancialContract_IDSystem"
			AND max_seq."FinancialContract_FinancialContractID"	 = loa."FinancialContract_FinancialContractID"

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_portfolioType" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS portfolioType
			ON  portfolioType."ASSOC_FinancialContract_FinancialContractID" = loa."FinancialContract_FinancialContractID"
			AND portfolioType."ASSOC_FinancialContract_IDSystem"            = loa."FinancialContract_IDSystem"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMonetaryBalance"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP, 'Loan' ) AS mb
            ON	mb."FinancialContractID"									= loa."FinancialContract_FinancialContractID" 
            AND mb."IDSystem"												= loa."FinancialContract_IDSystem"
            AND mb."MonetaryBalanceCategory"								= 'LoanBalance'	

	WHERE	(ps."PaymentSchedule_SequenceNumber" = max_seq."Max_SequenceNumber" or max_seq."Max_SequenceNumber" IS NULL);
END;