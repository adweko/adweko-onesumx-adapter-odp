FUNCTION "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_Contract" (
		I_BUSINESS_DATE DATE, 
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"contractID" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"valueDate" DATE,
		"maturityDate" DATE,
		"amortizationDate" DATE,
		"counterparty" NVARCHAR(128),
		"legalEntity" NVARCHAR(128),
		"currentPrincipal" DECIMAL(34, 6),
		"linkID" NVARCHAR(256),
		"linkType" NVARCHAR(3),
		"syndicationName" NVARCHAR(255),
		"originalTotalPrincipal" DECIMAL(34, 6),
		"remainingPrincipalDue" DECIMAL(34, 6),
		"referencePrincipal" DECIMAL(34, 6),
		"amortizationType" INT,
		"residualAmount" DECIMAL(34, 6),
		"currency" NVARCHAR(3),
		"currentNominalInterestRate" DECIMAL(15, 11),
		"optionHolder" NVARCHAR(1),
		"optionType" NVARCHAR(1),
		"exerciseBeginDate" DATE,
		"exerciseEndDate" DATE,
		"optionQuantity" DECIMAL(34, 6),
		"optionStrikeCall" DECIMAL(34, 11),
		"optionExecutionType" NVARCHAR(1),
		"globalCap" DECIMAL(15, 11),
		"globalFloor" DECIMAL(15, 11),
		"contractDayCountMethod" NVARCHAR(2),
		"interestPaymentType" NVARCHAR(1),
		"businessDayConvention" NVARCHAR(2),
		"calendar" NVARCHAR(75),
		"eomConvention" NVARCHAR(1),
    	"capitalizationEndDate" DATE,
		"xDayNotice" INT,
		"noticePeriod" NVARCHAR(75),
		"accruedInterest" DECIMAL(38, 11),
		"strippedPaymentType" NVARCHAR(1),
		"dataGroup" NVARCHAR(128),
		"ProductCatalogItemCode" NVARCHAR(128),
		"ProductCatalogID" NVARCHAR(128),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
        "spreadCurveDefinition" NVARCHAR(75) ,
		"isPerforming" INT,
		"capitalizedInterest" DECIMAL(34, 6),
		"isin" NVARCHAR(12), ---for SSD
		"lookbackPeriod" NVARCHAR(10),
		"rateCappingFlooring" NVARCHAR(40),
		"rateAveragingCap" DECIMAL(15, 11),
		"rateAveragingFloor" DECIMAL(15, 11),
/*  ---------------------------------
	Attributes for Credit Risk:
*/ ---------------------------------
		"grossPayableAmountAtDefault" DECIMAL(34, 6),
		"recoveredAmount" DECIMAL(34, 6),
		"pastDueAmount" DECIMAL(34, 6),
		"nonPerformingDate" DATE,
		"clearingHouse" NVARCHAR(128),
		"creditLineKey" NVARCHAR(256),
		"productType" NVARCHAR(75),
		"countryTransfer" NVARCHAR(2),
		"creditRiskProvision" DECIMAL(34, 6),
		"impairmentType" INT,
		"netRecoveryRate" DECIMAL (15,11),
		"grossRecoveryRate" DECIMAL (15,11),
/*  ---------------------------------
	User Defined Attributes :
*/ ---------------------------------
		"frtbCsrClass" NVARCHAR(75),
		"frtbEqClass" NVARCHAR(75),
		"frtbDrcClass" NVARCHAR(75),
		"contractFSType" NVARCHAR(256),
		"contractFSSubType" NVARCHAR(256),
		"productFSClass" NVARCHAR(128),
		"annuityCalculationType" NVARCHAR(128),
		"lmEntityName" NVARCHAR(128),
		"lmEntityDetail" NVARCHAR(128),
		"instrumentType" NVARCHAR(128),
		"financialInstrumentType" INT, 
		"isUnderLiquidityManagementControl" INT,
		"isCollateralised" INT,
		"cbHaircutPercentage" DECIMAL(15,11),
		"securedPercentage"  DECIMAL (15,11),
        "commitmentType" NVARCHAR(75),
		"portfolioType" NVARCHAR(255),
		"seniority" NVARCHAR(75),
		"contractRole" NVARCHAR(1),
		"eligibleType" NVARCHAR(75),
		"crRiskWeight" DECIMAL (15,11),
		"cadCrConversionFactor" NVARCHAR(75),--data type jsut assumed
		"UDA_IS_RETAIL_LOWER_RW_COMPLIANT" INT,
		"UDA_NOMINAL_COVERAGE_PERCENT" DECIMAL(15, 11),
		"conditionEndDate" DATE,
		"UDA_BRUTTOMARGE" DECIMAL(34,6),
		"writeOffAmount" DECIMAL(34, 6),
		"bookValue" DECIMAL(34, 6),
		"exposureClass" NVARCHAR(255)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS
BEGIN
	RETURN

	SELECT DISTINCT
		"com.adweko.adapter.osx.inputdata.common::get_id" (
			root."contractID",
			'0001',
			COALESCE(root."dataGroup", dtgdef."defaultName")
		) AS "id",
		root."contractID",
		root."FinancialContract_FinancialContractID" AS "sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		'0001' AS "node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		root."dealDate",
		"com.adweko.adapter.osx.inputdata.loans::get_valueDate_output"(valueDate."valueDate",pos."FirstDisbursementDate",root."valueDate") AS "valueDate",
		"com.adweko.adapter.osx.inputdata.loans::get_maturityDate_output"(root."maturityDate",mtd."LastPaymentDate",inte."capitalizationEndDate") AS "maturityDate",
		CASE
			WHEN amp."FinancialContract_FinancialContractID" is not NULL THEN --amortizationDate should only be filled, if there are AMP arrays
					GREATEST(root."maturityDate", IFNULL(mtd."LastPaymentDate",'1900-01-01'))
			ELSE NULL
		END AS "amortizationDate",
		root."counterparty",
		root."legalEntity",
		pos."currentPrincipal",
		CASE
			WHEN root."ProductCatalogItemCode" = 'MortgageLoan' 
				THEN  root."contractID"
			ELSE NULL
		END AS "linkID",
		CASE
			WHEN root."ProductCatalogItemCode" = 'MortgageLoan' 
				THEN  '3' --HYPO_MORTGAGE
			ELSE NULL
		END AS "linkType",
		root."syndicationName",

		CASE
        		    WHEN root."IsActive" = 'TRUE' THEN TO_DECIMAL(("com.adweko.adapter.osx.inputdata.common::get_absolutedValue" (IFNULL(pos."MonetaryBalance_RequestedNotDisbursedPrincipal",0)) + "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" (IFNULL(pos."currentPrincipal",0))), 34, 6)
        		    WHEN root."IsActive" = 'FALSE' THEN TO_DECIMAL(("com.adweko.adapter.osx.inputdata.common::get_negatedValue" (IFNULL(pos."MonetaryBalance_RequestedNotDisbursedPrincipal",0)) + "com.adweko.adapter.osx.inputdata.common::get_negatedValue" (IFNULL(pos."currentPrincipal",0))), 34, 6)
    		        ELSE NULL
    	END AS "originalTotalPrincipal",
		CASE
        		    WHEN root."IsActive" = 'TRUE' THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" (pos."MonetaryBalance_RequestedNotDisbursedPrincipal") 
        		    WHEN root."IsActive" = 'FALSE' THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" (pos."MonetaryBalance_RequestedNotDisbursedPrincipal")
    		        ELSE NULL
    	END AS "remainingPrincipalDue",

		CASE 
		    WHEN inte."Interest_InterestSubPeriodSpecificationCategory" = 'FlatInterestSpecification'
			AND root."FinancialContract_AmortizationType" IN ('French','German','Balloon') THEN
			    CASE
        		    WHEN root."IsActive" = 'TRUE' THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" ("FinancialContract_ApprovedNominalAmount") 
        		    WHEN root."IsActive" = 'FALSE' THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" ("FinancialContract_ApprovedNominalAmount") 
    		        ELSE NULL
    		    END
            ELSE
		        NULL
		END AS "referencePrincipal",

        CASE
			WHEN inte."Interest_InterestSubPeriodSpecificationCategory" = 'FlatInterestSpecification'
					AND root."FinancialContract_AmortizationType" IN ('French','German','Balloon')
				THEN 1
		END AS "amortizationType",
		pos."residualAmount",
		pos."ContractCurrency" AS "currency",
		COALESCE(CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (inte."currentNominalInterestRate", 'currentNominalInterestRate', kv."Value") AS DECIMAL(15,11)), 0) AS "currentNominalInterestRate",
		"com.adweko.adapter.osx.inputdata.loans::Loan_SideAgreements_get_OptionHolder"(
			root."BusinessPartnerContractAssignment_Borrower_ContractDataOwner",
			root."BusinessPartnerContractAssignment_Lender_ContractDataOwner",
			sideAgreementsOptionRight."OptionRight_OptionHolder"
		) AS "optionHolder",
		CASE WHEN sideAgreementsOptionRight."NumberOf" >= 1 THEN '1' END AS "optionType",
		CASE 
			WHEN sideAgreementsExercisePeriod."NumberOf" = 1
				THEN 
					GREATEST(sideAgreementsExercisePeriod."ExercisePeriod_StartDate",
							"com.adweko.adapter.osx.inputdata.loans::get_valueDate_output"(valueDate."valueDate",pos."FirstDisbursementDate",root."valueDate")
					)
			ELSE 
				CASE
					WHEN sideAgreementsOptionRight."NumberOf" = 1
						THEN
							GREATEST(sideAgreementsOptionRight."OptionRight_BeginofExercisePeriod",
									"com.adweko.adapter.osx.inputdata.loans::get_valueDate_output"(valueDate."valueDate",pos."FirstDisbursementDate",root."valueDate")
						)
				END
		END AS "exerciseBeginDate",
		CASE 
			WHEN sideAgreementsExercisePeriod."NumberOf" >= 1
				THEN
					LEAST(sideAgreementsExercisePeriod."ExercisePeriod_EndDate",
						"com.adweko.adapter.osx.inputdata.loans::get_maturityDate_output"(root."maturityDate",mtd."LastPaymentDate",inte."capitalizationEndDate")
					)
			ELSE 
				CASE
					WHEN sideAgreementsOptionRight."NumberOf" >= 1
						THEN
							LEAST(sideAgreementsOptionRight."OptionRight_EndofExercisePeriod",
								"com.adweko.adapter.osx.inputdata.loans::get_maturityDate_output"(root."maturityDate",mtd."LastPaymentDate",inte."capitalizationEndDate")
							)
				END
		END AS "exerciseEndDate",
		CASE
			WHEN (sideAgreementsExercisePeriod."NumberOf" >= 1 OR sideAgreementsOptionRight."NumberOf" >= 1)
				THEN 1
		END AS "optionQuantity",
		CASE WHEN sideAgreementsOptionRight."NumberOf" >= 1 THEN 100.00000000000 END AS "optionStrikeCall",
		CASE 
			WHEN sideAgreementsExercisePeriod."NumberOf" >= 2
				THEN '3'
			WHEN sideAgreementsExercisePeriod."NumberOf" = 1
				THEN 
					CASE 
						WHEN sideAgreementsExercisePeriod."ExercisePeriod_DaysBetween" IN (0,1)
							THEN '1'
						WHEN sideAgreementsExercisePeriod."ExercisePeriod_DaysBetween" > 1
							THEN '2'
					END
			ELSE
				CASE
					WHEN sideAgreementsOptionRight."NumberOf" >= 2
						THEN '3'
					WHEN sideAgreementsOptionRight."NumberOf" = 1
						THEN 
							CASE 
								WHEN sideAgreementsOptionRight."OptionRight_DaysBetween" IN (0,1)
									THEN '1'
								WHEN sideAgreementsOptionRight."OptionRight_DaysBetween" > 1
									THEN '2'
							END
				END
		END AS "optionExecutionType",
		CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (sideAgreementsInterest."RateMax", 'globalCap', kv."Value") AS DECIMAL(15,11)) AS "globalCap",
		CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (sideAgreementsInterest."RateMin", 'globalFloor', kv."Value") AS DECIMAL(15,11)) AS "globalFloor",
		inte."contractDayCountMethod",
		inte."interestPaymentType",
		inte."businessDayConvention",
		inte."calendar",
		inte."eomConvention",
		inte."capitalizationEndDate",
		CASE 
			WHEN sideAgreementsExercisePeriod."_Canc_Cnt" >= 1
				THEN "com.adweko.adapter.osx.inputdata.loans::get_xDayNotice_output"(
							i_Canc_Cnt			=> sideAgreementsExercisePeriod."_Canc_Cnt",
							i_DaysToBegin 		=> sideAgreementsExercisePeriod."_Canc_DaysToBegin",
							i_DaysToEnd			=> sideAgreementsExercisePeriod."_Canc_DaysToEnd",
							i_InInterval		=> sideAgreementsExercisePeriod."_Canc_InInterval",
							i_AfterInterval		=> sideAgreementsExercisePeriod."_Canc_AfterInterval",
							i_DaysToNextBegin	=> sideAgreementsExercisePeriod."_Canc_DaysToNextBegin"
						)
			ELSE 
				CASE
					WHEN sideAgreementsOptionRight."_Canc_Cnt" >= 1
						THEN "com.adweko.adapter.osx.inputdata.loans::get_xDayNotice_output"(
									i_Canc_Cnt			=> sideAgreementsOptionRight."_Canc_Cnt",
									i_DaysToBegin 		=> sideAgreementsOptionRight."_Canc_DaysToBegin",
									i_DaysToEnd			=> sideAgreementsOptionRight."_Canc_DaysToEnd",
									i_InInterval		=> sideAgreementsOptionRight."_Canc_InInterval",
									i_AfterInterval		=> sideAgreementsOptionRight."_Canc_AfterInterval",
									i_DaysToNextBegin	=> sideAgreementsOptionRight."_Canc_DaysToNextBegin"
								)
				END
		END AS "xDayNotice",
		CONCAT(
			CASE 
				WHEN sideAgreementsExercisePeriod."_Canc_Cnt" >= 1
					THEN "com.adweko.adapter.osx.inputdata.loans::get_xDayNotice_output"(
								i_Canc_Cnt			=> sideAgreementsExercisePeriod."_Canc_Cnt",
								i_DaysToBegin 		=> sideAgreementsExercisePeriod."_Canc_DaysToBegin",
								i_DaysToEnd			=> sideAgreementsExercisePeriod."_Canc_DaysToEnd",
								i_InInterval		=> sideAgreementsExercisePeriod."_Canc_InInterval",
								i_AfterInterval		=> sideAgreementsExercisePeriod."_Canc_AfterInterval",
								i_DaysToNextBegin	=> sideAgreementsExercisePeriod."_Canc_DaysToNextBegin"
							)
				ELSE 
					CASE
						WHEN sideAgreementsOptionRight."_Canc_Cnt" >= 1
							THEN "com.adweko.adapter.osx.inputdata.loans::get_xDayNotice_output"(
										i_Canc_Cnt			=> sideAgreementsOptionRight."_Canc_Cnt",
										i_DaysToBegin 		=> sideAgreementsOptionRight."_Canc_DaysToBegin",
										i_DaysToEnd			=> sideAgreementsOptionRight."_Canc_DaysToEnd",
										i_InInterval		=> sideAgreementsOptionRight."_Canc_InInterval",
										i_AfterInterval		=> sideAgreementsOptionRight."_Canc_AfterInterval",
										i_DaysToNextBegin	=> sideAgreementsOptionRight."_Canc_DaysToNextBegin"
									)
					END
			END,
			'd'
		) AS "noticePeriod",
		pos."accruedInterest",
		NULL AS "strippedPaymentType",  
		--Datagroup
		COALESCE(root."dataGroup", dtgdef."defaultName") AS "dataGroup",
		root."ProductCatalogItemCode",
		root."ProductCatalogID",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",
        root."spreadCurveDefinition",
		root."isPerforming",
		CASE
        	WHEN root."IsActive" = 'TRUE' THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" (inte."MonetaryBalance_CapitalizedInterest") 
        	WHEN root."IsActive" = 'FALSE' THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" (inte."MonetaryBalance_CapitalizedInterest") 
    		ELSE NULL
    	END AS "capitalizedInterest",
        root."isin" AS "isin",
		inte."lookbackPeriod",
		inte."rateCappingFlooring",
		inte."rateAveragingCap",
		inte."rateAveragingFloor",
        CASE 
            WHEN root."ContractStatus_DefaultStatus_Status" IS NOT NULL THEN
                CASE
        		    WHEN root."IsActive" = 'TRUE' THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" ("CreditRiskExposureAtDefault_ExposureAtDefault") 
        		    WHEN root."IsActive" = 'FALSE' THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" ("CreditRiskExposureAtDefault_ExposureAtDefault") 
    		        ELSE NULL
    		    END
            ELSE
                NULL
        END AS "grossPayableAmountAtDefault",

		CASE 
		    WHEN root."ContractStatus_DefaultStatus_Status" = 'Recovered' THEN
		         CASE
        		    WHEN root."IsActive" = 'TRUE' THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" ("recoveredAmount") 
        		    WHEN root."IsActive" = 'FALSE' THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" ("recoveredAmount") 
    		        ELSE NULL
    		    END
		    ELSE
		        NULL
		END AS "recoveredAmount",
        
        CASE
		    WHEN root."IsActive" = 'TRUE' THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue" ("pastDueAmount") 
		    WHEN root."IsActive" = 'FALSE' THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue" ("pastDueAmount") 
	        ELSE NULL
	    END AS "pastDueAmount",

		root."nonPerformingDate",
		root."clearingHouse",
	--	agrd."contractID" AS "creditLineKey",
		agrd."contractID" AS "creditLineKey",
		root."productType",
		bp."RiskCountry" AS "countryTransfer",
		adj."CreditRiskAdjustment_RiskProvisionInPositionCurrency" AS "creditRiskProvision",
		root."impairmentType",
		TO_DECIMAL(root."netRecoveryRate", 15, 11) AS "netRecoveryRate",
        TO_DECIMAL(root."grossRecoveryRate", 15, 11) AS "grossRecoveryRate",
        NULL AS "frtbCsrClass",
		NULL AS "frtbEqClass",
		NULL AS "frtbDrcClass",
		root."contractFSType",
		root."contractFSSubType",
		"com.adweko.adapter.osx.inputdata.loans::Loan_get_ProductFSClass"(root."IsActive", root."StandardCatalog_ProductCatalogItem") AS "productFSClass",
		CASE
			WHEN amp."FinancialContract_FinancialContractID" is not NULL THEN --annuityCalculationType should only be filled, if there are AMP arrays
				'dateFixed'
			ELSE NULL
		END AS "annuityCalculationType",
		'Loan' AS "lmEntityName",
		CASE
			WHEN sideAgreementsOptionRight."NumberOf" >= 1 THEN 'BondOptionality' 
		END AS "lmEntityDetail",
        root."instrumentType",
        root."financialInstrumentType",
        1 AS "isUnderLiquidityManagementControl",
        CASE WHEN root."IsActive" = 'TRUE' THEN
			CASE
				WHEN EURegRepForContract."EuropeanRegulatoryReportingForContract_LiquidityClassificationOfAsset" IN ('ExtremlyHighLiquid', 'HighLiquidLevelA', 'HighLiquidLevelB')
					THEN 1
				ELSE 0
			END
        END AS "isCollateralised",
        IFNULL(cbHaircut."cbHaircutPercentage", 1) AS "cbHaircutPercentage",
        "com.adweko.adapter.osx.inputdata.loans::Loan_get_securedPercentage"(root."IsActive", securedPercentage."SecuredPercentage") AS "securedPercentage",
    	root."commitmentType",
		root."portfolioType",
		root."seniority",
		root."contractRole",
		root."eligibleType",
		CAST(root."crRiskWeight" AS DECIMAL(15,11)) AS "crRiskWeight",
		root."cadCrConversionFactor",
		root."UDA_IS_RETAIL_LOWER_RW_COMPLIANT",
		root."UDA_NOMINAL_COVERAGE_PERCENT",
		inte."Interest_ConditionFixedPeriodEndDate" AS "conditionEndDate",
		MAP(marginKey."Value",
			'Rate', COALESCE(margin."MarginComponent_MarginComponentRate", margin."Margin_MarginRate"),
			'Amount', COALESCE(margin."MarginComponent_MarginComponentAmount",  margin."Margin_MarginAmount")
		) AS "UDA_BRUTTOMARGE",
		MAP(writeOffKey."Value",
			'WriteDownAmountInPositionCurrency', writeOff."WriteDownAmountInPositionCurrency",
			'WriteDownAmount', writeOff."WriteDownAmount"
		) AS "writeOffAmount",
		bok."BookValue_BookValue" AS "bookValue",
		root."exposureClass"
	FROM "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::Loan_MaturityDate"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING, 'ComplexLoan') as mtd
		ON mtd."IDSystem" = root."FinancialContract_IDSystem"
		AND mtd."FinancialContractID" = root."FinancialContract_FinancialContractID"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Position" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS pos
		ON pos."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND pos."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::Loan_SideAgreements_get_Interest"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS sideAgreementsInterest
		ON sideAgreementsInterest."FinancialContractID"	= root."FinancialContract_FinancialContractID"
		AND sideAgreementsInterest."IDSystem"			= root."FinancialContract_IDSystem"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_SideAgreements_get_OptionRight_Prio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS sideAgreementsOptionRight
		ON sideAgreementsOptionRight."OptionRight_FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"
		AND sideAgreementsOptionRight."OptionRight_FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_SideAgreements_get_ExercisePeriod_Prio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS sideAgreementsExercisePeriod
		ON sideAgreementsExercisePeriod."ExercisePeriod_OptionRight_FinancialContractID"	= root."FinancialContract_FinancialContractID"
		AND sideAgreementsExercisePeriod."ExercisePeriod_OptionRight_IDSystem"				= root."FinancialContract_IDSystem"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_InterestPrio_Contract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING,'false') AS inte
		ON inte."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND inte."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::BV_Loan_AgreedLimit"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,'AgreedLimitComplexLoan') agrd
    	ON agrd."FinancialContractID"		= root."FinancialContract_FinancialContractID"
    	AND agrd."IDSystem"					= root."FinancialContract_IDSystem"

    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_datagroupDefaultValue" (:I_BUSINESS_DATE) AS dtgdef
		ON 1 = 1
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartner_View"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) bp
		ON bp."BusinessPartnerID" = root."counterparty" 

--	CreditRisk
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposureAtDefault" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cread
		ON cread."CreditRiskExposureAtDefault_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND cread."CreditRiskExposureAtDefault_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
		ON  kv."KeyID" = 'SAPPercentageStandard'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskAdjustmentPrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Provision', 'Loan') AS adj
		ON adj."CreditRiskAdjustment_FinancialContract_IDSystem" = pos."FinancialContract_IDSystem"
		AND adj."CreditRiskAdjustment_FinancialContract_FinancialContractID" = pos."FinancialContract_FinancialContractID"	
		AND adj. "CreditRiskAdjustment_PositionCurrency" = pos."ContractCurrency"

--  valueDate		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::get_valueDate" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING, 'ComplexLoan') AS valueDate
		ON valueDate."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND valueDate."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		
--  isCollateralised
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::Loan_get_CollateralAgreementFC"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS collateralAgreementFC
		ON collateralAgreementFC."FinancialContractID"	= root."FinancialContract_FinancialContractID"
			AND collateralAgreementFC."IDSystem"		= root."FinancialContract_IDSystem"
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::Loan_get_PledgedObject"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pledgedObject
		ON pledgedObject."Pledge_FinancialContractID"				= collateralAgreementFC."CollateralAgreementFC_FinancialContractID"
			AND pledgedObject."Pledge_IDSystem"						= collateralAgreementFC."CollateralAgreementFC_IDSystem"
			AND pledgedObject."PledgedObjectCategory"				IN ('PledgeOfFinancialContract','PledgeOfAssetsHeldAtThirdParty') 
			AND collateralAgreementFC."CollateralAgreementFC_SimpleCollateralCategory"	= 'Pledge'
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::Loan_get_PledgedFI"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pledgedFI
		ON pledgedFI."FinancialContractID"							= pledgedObject."Pledge_FinancialContractID"
			AND pledgedFI."IDSystem"								= pledgedObject."Pledge_IDSystem"
			AND collateralAgreementFC."CollateralAgreementFC_SimpleCollateralCategory"	= 'Pledge'
			AND (
				pledgedObject."PledgedObjectCategory"				= 'PledgeOfFinancialContract' AND
				pledgedObject."PledgeOfFinancialContractCategory"	= 'PledgeOfInstrumentsHeldInOwnAccount' 
				OR
				pledgedObject."PledgedObjectCategory"				= 'PledgeOfAssetsHeldAtThirdParty'
			)	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::Loan_get_AssignedRightAsCollateral"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS assignedRightAsCollateral
		ON assignedRightAsCollateral."SimpleCollateralAgreement_FinancialContractID"	= collateralAgreementFC."CollateralAgreementFC_FinancialContractID"
			AND assignedRightAsCollateral."SimpleCollateralAgreement_IDSystem"			= collateralAgreementFC."CollateralAgreementFC_IDSystem"
			AND collateralAgreementFC."CollateralAgreementFC_SimpleCollateralCategory"	= 'AssignmentOfRightsAsCollateral'
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_EuropeanRegulatoryReportingForContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS EURegRepForContract
		ON (	(EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialContract_FinancialContractID"	= pledgedObject."PledgedFinancialContract_FinancialContractID"
					AND EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialContract_IDSystem"		= pledgedObject."PledgedFinancialContract_IDSystem"
					AND collateralAgreementFC."CollateralAgreementFC_SimpleCollateralCategory"						= 'Pledge'
					AND pledgedObject."PledgedObjectCategory"														= 'PledgeOfFinancialContract'
					AND pledgedObject."PledgeOfFinancialContractCategory"											IS NULL )
			OR	(EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialInstrument_FinancialInstrumentID"	= pledgedFI."FinancialInstrumentID"
					AND collateralAgreementFC."CollateralAgreementFC_SimpleCollateralCategory"							= 'Pledge'
					AND pledgedObject."PledgedObjectCategory"															= 'PledgeOfFinancialContract'
					AND pledgedObject."PledgeOfFinancialContractCategory"												= 'PledgeOfInstrumentsHeldInOwnAccount')
			OR	(EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialInstrument_FinancialInstrumentID"	= pledgedFI."FinancialInstrumentID"
					AND collateralAgreementFC."CollateralAgreementFC_SimpleCollateralCategory"						= 'Pledge'
					AND pledgedObject."PledgedObjectCategory"														= 'PledgeOfAssetsHeldAtThirdParty')
			OR	(EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialContract_FinancialContractID"	= assignedRightAsCollateral."AssignedFinancialContract_FinancialContractID"
					AND EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialContract_IDSystem"		= assignedRightAsCollateral."AssignedFinancialContract_IDSystem"
					AND collateralAgreementFC."CollateralAgreementFC_SimpleCollateralCategory"						= 'AssignmentOfRightsAsCollateral'
					AND assignedRightAsCollateral."AssignmentOfRightCategory"										= 'AssignmentOfFinancialContractAsCollateral') 
		)
--	cbHaircutPercentage
	LEFT OUTER JOIN  "com.adweko.adapter.osx.inputdata.common::get_cbHaircutPercentage"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'ComplexLoan') AS cbHaircut
		ON cbHaircut."FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"
		AND cbHaircut."FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::Loan_SecuredPercentage" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS securedPercentage
		ON securedPercentage."IDSystem"				= root."FinancialContract_IDSystem"
		AND securedPercentage."FinancialContractID"	= root."FinancialContract_FinancialContractID"
		
	-- check whether there are principal payments	
	LEFT OUTER JOIN (
			select distinct "FinancialContract_FinancialContractID", "FinancialContract_IDSystem"
			from "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Payment" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS paym
			WHERE 
			(paym."cycleEndDate" > :I_BUSINESS_DATE OR (paym."cycleStartDateOrig" >= :I_BUSINESS_DATE AND paym."cycleEndDate" IS NULL AND paym."cycleLength" = 999 AND paym."cycleTimeUnit" = 'Year') )
			AND (paym."FinancialContract_AmortizationType" IN ('French','German') OR (paym."AnnuityDerivationCase"=1 AND paym."FinancialContract_AmortizationType" = 'Balloon'))
			AND paym."cycleLength" IS NOT NULL
			AND paym."cycleTimeUnit" IS NOT NULL
			AND paym."cycleStartDateOrig" IS NOT NULL 
	) as amp
			on amp."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
			AND amp."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
			
	--UDA_BRUTTOMARGE
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS marginKey
		ON marginKey."KeyID" = 'marginSwitch'

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMargin"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'ComplexLoan') AS margin
		ON margin."Margin_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND margin."Margin_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"

	--writeOffAmount
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS writeOffKey
		ON writeOffKey."KeyID" = 'WriteDownPositionCurrenyOrAmount'

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityWriteOffFC"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Loan') AS writeOff
		ON writeOff."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND writeOff."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
		ON bok."BookValue_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
		AND bok."BookValue_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND bok."BookValue_BookValueCurrency" = pos."ContractCurrency";

END;
