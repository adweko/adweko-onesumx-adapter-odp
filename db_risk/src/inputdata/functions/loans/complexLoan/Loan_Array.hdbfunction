FUNCTION "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_Array" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"refid" NVARCHAR(32),
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),
		"pricingMarketObject" NVARCHAR(40),
		"repricingType" NVARCHAR(1),
		"rateAdd" DECIMAL(15, 11),
		"rateMultiplier" DECIMAL(15, 11),
		"rateTerm" NVARCHAR(10),
		"fixingDays" NVARCHAR(10),
		"ppAmount" DECIMAL(34, 6),
		"cap" DECIMAL(15, 11),
		"floor" DECIMAL(15, 11),
		"gracePeriodStartDate" DATE,
		"gracePeriodEndDate" DATE,
		"graceType" NVARCHAR(1),
		"capitalized" INT,
		"FirstInterestPeriodStartDate" DATE,
		"FirstInterestPeriodEndDate" DATE,
		"FirstInterestDueDate" DATE,
		"LastInterestPeriodEndDate" DATE,
		"DayOfMonthOfInterestPayment" INTEGER,
		"InterestScheduleType" NVARCHAR(100),
/*  ---------------------------------
	Attributes for Credit Risk:
*/ ---------------------------------
		"anchor" DATE,
		"amount" DECIMAL(34, 11),
		"ratingScale" NVARCHAR(75),
		"ratingClass" NVARCHAR(10),
		"cashFlowKind" NVARCHAR(1),
		"dataGroup" NVARCHAR(128)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	SELECT CASE 
			WHEN "arraySpecKind" = 'CRTNG'
				THEN "com.adweko.adapter.osx.inputdata.common::get_array_id" (
						"com.adweko.adapter.osx.inputdata.common::get_id" (
							"contractID",
							'0001',
							COALESCE("dataGroup", dtgdef."defaultName")
							),
						"arraySpecKind",
						"rating_id"
						)
			WHEN "arraySpecKind" = 'AMP'
				THEN "com.adweko.adapter.osx.inputdata.common::get_array_id" (
						"com.adweko.adapter.osx.inputdata.common::get_id" (
							"contractID",
							'0001',
							COALESCE("dataGroup", dtgdef."defaultName")
							),
						"arraySpecKind",
						"cycleDate"
						)
			WHEN "arraySpecKind" IN (
					'LOSS',
					'PDEF'
					)
				THEN "com.adweko.adapter.osx.inputdata.common::get_array_id" (
						"com.adweko.adapter.osx.inputdata.common::get_id" (
							"contractID",
							'0001',
							COALESCE("dataGroup", dtgdef."defaultName")
							),
						"arraySpecKind",
						"anchor"
						)
			WHEN "arraySpecKind"='CFS'
				THEN "com.adweko.adapter.osx.inputdata.common::get_array_id" (
						"com.adweko.adapter.osx.inputdata.common::get_id" (
							"contractID",
							'000'||"cashFlowKind",
							COALESCE("dataGroup", dtgdef."defaultName")
							),
						"arraySpecKind",
						"anchor"
						)
			ELSE "com.adweko.adapter.osx.inputdata.common::get_array_id" (
					"com.adweko.adapter.osx.inputdata.common::get_id" (
						"contractID",
						'0001',
						COALESCE("dataGroup", dtgdef."defaultName")
						),
					"arraySpecKind",
					"cycleDate"
					)
			END AS "id",
		"com.adweko.adapter.osx.inputdata.common::get_id" (
			"contractID",
			'0001',
			COALESCE("dataGroup", dtgdef."defaultName")
			) AS "refid", 
		"arraySpecKind",
		"cycleDate",
		"cyclePeriod",
		"pricingMarketObject",
		"repricingType",
		"rateAdd",
		"rateMultiplier",
		"rateTerm",
		"fixingDays",
--		CASE 
--			WHEN "IsActive" = 'TRUE'  -- aktiv
--				THEN 
--					CASE 
--						WHEN "arraySpecKind" = 'AMP'
--							THEN "com.adweko.adapter.osx.inputdata.common::get_absolutedValue"("ppAmount")
--						WHEN "arraySpecKind" = 'PP' -- Vorzeichen wird in der get_payment function gedreht
--							THEN "ppAmount"
--					ELSE "ppAmount"
--					END
--			WHEN "IsActive" = 'FALSE' -- passiv
--				THEN 
--					CASE 
--						WHEN "arraySpecKind" = 'AMP'
--							THEN "com.adweko.adapter.osx.inputdata.common::get_negatedValue"("ppAmount")
--						WHEN "arraySpecKind" = 'PP'
--							THEN "ppAmount"
--						ELSE "ppAmount"
--					END
--			ELSE "ppAmount"
--		END AS "ppAmount",
		"ppAmount",
		"cap",
		"floor",
		"gracePeriodStartDate",
		"gracePeriodEndDate",
		"graceType",
		"capitalized",
		"FirstInterestPeriodStartDate",
		"FirstInterestPeriodEndDate",
		"FirstInterestDueDate",
		"LastInterestPeriodEndDate",
		"DayOfMonthOfInterestPayment",
		"InterestScheduleType",
		"anchor",
		CASE 
			WHEN "IsActive" = 'TRUE'  -- aktiv
				THEN 
					CASE 
						WHEN "arraySpecKind" = 'CFS'-- repayment
							THEN TO_DECIMAL("com.adweko.adapter.osx.inputdata.common::get_absolutedValue"("amount"), 34, 11)
						ELSE "amount"
					END
			WHEN "IsActive" = 'FALSE' -- passiv
				THEN 
					CASE 
						WHEN "arraySpecKind" = 'CFS'-- repayment
							THEN TO_DECIMAL("com.adweko.adapter.osx.inputdata.common::get_negatedValue"("amount"), 34, 11)
						ELSE "amount"
					END
				ELSE "amount"
		END AS "amount",
		"ratingScale",
		"ratingClass",
		"cashFlowKind",
		COALESCE("dataGroup", dtgdef."defaultName") AS "dataGroup"
	FROM (
		SELECT 
			CASE 
				WHEN	"arraySpecKind" = 'RP' 
					AND ((sideAgreementsInterest."RateMax" IS NOT NULL OR sideAgreementsInterest."RateMax" <> 0)
					OR (sideAgreementsInterest."RateMin" IS NOT NULL OR sideAgreementsInterest."RateMin" <> 0))
					THEN 'RPS' 
				ELSE "arraySpecKind" 
			END AS "arraySpecKind",
			"cycleDate",
			"cyclePeriod",
			"pricingMarketObject",
			"repricingType",
			CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" ("rateAdd", 'rateAdd', kv."Value") AS DECIMAL (15,11)) AS "rateAdd",
			"rateMultiplier",
			"rateTerm",
			"fixingDays",
			NULL AS "ppAmount",
			CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (sideAgreementsInterest."RateMax", 'cap', kv."Value") AS DECIMAL(15,11)) AS "cap",
			CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (sideAgreementsInterest."RateMin", 'floor', kv."Value") AS DECIMAL(15,11)) AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			"capitalized",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
		--	CASE
		--		WHEN inte."interestPaymentType" = 2 OR inte."Interest_FirstInterestPeriodEndDate" != inte."Interest_FirstDueDate"
		--			THEN inte."Interest_FirstInterestPeriodStartDate"
		--	END AS "FirstInterestPeriodStartDate",
			inte."Interest_FirstInterestPeriodStartDate" AS "FirstInterestPeriodStartDate",
		--	CASE
		--		WHEN inte."interestPaymentType" = 2 OR inte."Interest_FirstInterestPeriodEndDate" != inte."Interest_FirstDueDate"
		--			THEN inte."Interest_FirstInterestPeriodEndDate"
		--	END AS "FirstInterestPeriodEndDate",
			inte."Interest_FirstInterestPeriodEndDate" AS "FirstInterestPeriodEndDate",
		--	CASE
		--		WHEN inte."interestPaymentType" = 2 OR inte."Interest_FirstInterestPeriodEndDate" != inte."Interest_FirstDueDate"
		--			THEN inte."Interest_FirstDueDate"
		--	END AS "FirstInterestDueDate",
			inte."Interest_FirstDueDate" AS "FirstInterestDueDate",
		--	CASE
		--		WHEN inte."interestPaymentType" = 2 OR inte."Interest_FirstInterestPeriodEndDate" != inte."Interest_FirstDueDate"
		--			THEN inte."Interest_LastInterestPeriodEndDate"
		--	END AS "LastInterestPeriodEndDate",
			inte."Interest_LastInterestPeriodEndDate" AS "LastInterestPeriodEndDate",
		--	CASE
		--		WHEN inte."interestPaymentType" = 2 OR inte."Interest_FirstInterestPeriodEndDate" != inte."Interest_FirstDueDate"
		--			THEN inte."Interest_DayOfMonthOfInterestPayment"
		--	END AS "DayOfMonthOfInterestPayment",
			inte."Interest_DayOfMonthOfInterestPayment" AS "DayOfMonthOfInterestPayment",
		--	CASE
		--		WHEN inte."interestPaymentType" = 2 OR inte."Interest_FirstInterestPeriodEndDate" != inte."Interest_FirstDueDate"
		--			THEN inte."Interest_InterestScheduleType"
		--	END AS "InterestScheduleType",
			inte."Interest_InterestScheduleType" AS "InterestScheduleType",
			NULL AS "anchor",
			NULL AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			inte."FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			inte."FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive",
			root."dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Interest" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				:I_DATAGROUP_STRING
				) AS inte
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::Loan_SideAgreements_get_Interest"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS sideAgreementsInterest
			ON sideAgreementsInterest."FinancialContractID"	= inte."FinancialContract_FinancialContractID"
			AND sideAgreementsInterest."IDSystem"			= inte."FinancialContract_IDSystem"	
			AND "arraySpecKind" != 'IP'
		
		INNER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				:I_DATAGROUP_STRING
				) AS root ON (root."FinancialContract_IDSystem" = inte."FinancialContract_IDSystem"
			AND root."FinancialContract_FinancialContractID" = inte."FinancialContract_FinancialContractID"
			AND ("repricingType"<>'4' OR "repricingType" IS NULL)
			)
			OR
			(root."FinancialContract_IDSystem" = inte."FinancialContract_IDSystem"
			AND root."FinancialContract_FinancialContractID" = inte."FinancialContract_FinancialContractID"
			AND (inte."repricingType"='4' AND inte."Interest_FirstInterestPeriodStartDate"  > :I_BUSINESS_DATE)
			) -- only Fixed Interest Rates in future periods
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
			ON  kv."KeyID" = 'SAPPercentageStandard'
		
		UNION
		
		SELECT 
			'PP' AS "arraySpecKind",
			"cycleDate",
			"cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			"ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			NULL AS "capitalized",
			:I_BUSINESS_DATE AS "bookValueDate",
			"contractID",
			NULL AS "FirstInterestPeriodStartDate",
			NULL AS "FirstInterestPeriodEndDate",
			NULL AS "FirstInterestDueDate",
			NULL AS "LastInterestPeriodEndDate",
			NULL AS "DayOfMonthOfInterestPayment",
			NULL AS "InterestScheduleType",
			NULL AS "anchor",
			NULL AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			"FinancialContract_IDSystem",
			"FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			"IsActive",
			"dataGroup"
		FROM (
			SELECT
				paym."cycleStartDateOrig" AS "cycleDate",
				"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"("cycleLength", mcp."cyclePeriod") AS "cyclePeriod",
				paym."ppAmount" AS "ppAmount",
				root."contractID" AS "contractID",
				paym."FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
				paym."FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
				root."IsActive",
				root."dataGroup"
			FROM "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Payment" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS paym
			
			LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
				ON  mcp."BusinesscyclePeriod" = paym."cycleTimeUnit"
			
			INNER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root 
				ON root."FinancialContract_IDSystem" = paym."FinancialContract_IDSystem"
				AND root."FinancialContract_FinancialContractID" = paym."FinancialContract_FinancialContractID"
				AND ("cycleEndDate" > :I_BUSINESS_DATE OR ("cycleStartDateOrig" >= :I_BUSINESS_DATE AND "cycleEndDate" IS NULL AND "cycleLength" = 999 AND "cycleTimeUnit" = 'Year') )
				
			WHERE (paym."FinancialContract_AmortizationType" NOT IN ('French','German','Balloon')
				OR (paym."FinancialContract_AmortizationType" ='Balloon' AND "AnnuityDerivationCase"=0))
				AND paym."cycleLength" IS NOT NULL
				AND paym."cycleTimeUnit" IS NOT NULL
				AND paym."cycleStartDateOrig" IS NOT NULL
			
			UNION
			
			SELECT
				settm."cycleDate" AS "cycleDate",
				settm."cyclePeriod" AS "cyclePeriod",
				"com.adweko.adapter.osx.inputdata.loans::get_ppAmountForSettlement"(settm."Settlement_PostingDirection", root."IsActive",settm."ppAmountWithoutPostingDirection") AS "ppAmount",
				root."contractID" AS "contractID",
				settm."Settlement_FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
				settm."Settlement_FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
				root."IsActive",
				root."dataGroup"
			FROM "com.adweko.adapter.osx.inputdata.loans::Loan_get_Settlement"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS settm
			
			LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS settlementSwitch
				ON settlementSwitch."KeyID" = 'settlementOn'
			
			INNER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
				ON root."FinancialContract_IDSystem" = settm."Settlement_FinancialContract_IDSystem"
				AND root."FinancialContract_FinancialContractID" = settm."Settlement_FinancialContract_FinancialContractID"
				AND settm."cycleDate" >= :I_BUSINESS_DATE

			LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::get_AnnuityDerivationCase"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS badc ON
	    		badc."FinancialContract_FinancialContractID"			= settm."Settlement_FinancialContract_FinancialContractID"
        		AND badc."FinancialContract_IDSystem"					= settm."Settlement_FinancialContract_IDSystem"	
		

			WHERE settlementSwitch."Value" = 'true'
				AND (root."FinancialContract_AmortizationType" NOT IN ('French','German','Balloon')
							OR (root."FinancialContract_AmortizationType" ='Balloon' AND badc."AnnuityDerivationCase"=0))
		)
		
		UNION
		
		SELECT 
			'AMP' AS "arraySpecKind",
			"cycleStartDateOrig" AS "cycleDate",
			"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
				"cycleLength",
				mcp."cyclePeriod"
			) AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			"ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			NULL AS "capitalized",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "FirstInterestPeriodStartDate",
			NULL AS "FirstInterestPeriodEndDate",
			NULL AS "FirstInterestDueDate",
			NULL AS "LastInterestPeriodEndDate",
			NULL AS "DayOfMonthOfInterestPayment",
			NULL AS "InterestScheduleType",
			NULL AS "anchor",
			NULL AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			paym."FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			paym."FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive",
			root."dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Payment" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP
				) AS paym
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = paym."cycleTimeUnit"
		INNER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				:I_DATAGROUP_STRING
				) AS root ON root."FinancialContract_IDSystem" = paym."FinancialContract_IDSystem"
			AND root."FinancialContract_FinancialContractID" = paym."FinancialContract_FinancialContractID"
			AND ("cycleEndDate" > :I_BUSINESS_DATE OR ("cycleStartDateOrig" >= :I_BUSINESS_DATE AND "cycleEndDate" IS NULL AND "cycleLength" = 999 AND "cycleTimeUnit" = 'Year') )
		WHERE 	(paym."FinancialContract_AmortizationType" IN ('French','German')
				OR (paym."FinancialContract_AmortizationType" ='Balloon' AND "AnnuityDerivationCase"=1))
			AND paym."cycleLength" IS NOT NULL
			AND paym."cycleTimeUnit" IS NOT NULL
			AND paym."cycleStartDateOrig" IS NOT NULL

		UNION

		SELECT 'GP' AS "arraySpecKind",
			NULL AS "cycleDate",
			NULL AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			"gracePeriodStartDate",
			"gracePeriodEndDate",
			"graceType",
			NULL AS "capitalized",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "FirstInterestPeriodStartDate",
			NULL AS "FirstInterestPeriodEndDate",
			NULL AS "FirstInterestDueDate",
			NULL AS "LastInterestPeriodEndDate",
			NULL AS "DayOfMonthOfInterestPayment",
			NULL AS "InterestScheduleType",
			NULL AS "anchor",
			NULL AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			grace."FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			grace."FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive",
			root."dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_GracePeriod" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				:I_DATAGROUP_STRING
				) AS grace
		INNER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				:I_DATAGROUP_STRING
				) AS root ON root."FinancialContract_IDSystem" = grace."FinancialContract_IDSystem"
			AND root."FinancialContract_FinancialContractID" = grace."FinancialContract_FinancialContractID"
		
		UNION
		
		SELECT 'PDEF' AS "arraySpecKind",
			NULL AS "cycleDate",
			NULL AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			NULL AS "capitalized",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "FirstInterestPeriodStartDate",
			NULL AS "FirstInterestPeriodEndDate",
			NULL AS "FirstInterestDueDate",
			NULL AS "LastInterestPeriodEndDate",
			NULL AS "DayOfMonthOfInterestPayment",
			NULL AS "InterestScheduleType",
			pd."probabilityOfDefaultDate" AS "anchor",
			CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (pd."probabilityOfDefaultValue", 'probabilityOfDefaultValue', kv."Value") AS DECIMAL(34,11)) AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			pd."CreditRiskProbabilityOfDefault_FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			pd."CreditRiskProbabilityOfDefault_FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive",
			root."dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.common::get_pd" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				'ComplexLoan'
				) AS pd
		INNER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				:I_DATAGROUP_STRING
				) AS root ON root."FinancialContract_FinancialContractID" = pd."CreditRiskProbabilityOfDefault_FinancialContract_FinancialContractID"
			AND root."FinancialContract_IDSystem" = pd."CreditRiskProbabilityOfDefault_FinancialContract_IDSystem"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
		ON  kv."KeyID" = 'SAPPercentageStandard'
		
		UNION
		
		SELECT 'LOSS' AS "arraySpecKind",
			NULL AS "cycleDate",
			NULL AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			NULL AS "capitalized",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "FirstInterestPeriodStartDate",
			NULL AS "FirstInterestPeriodEndDate",
			NULL AS "FirstInterestDueDate",
			NULL AS "LastInterestPeriodEndDate",
			NULL AS "DayOfMonthOfInterestPayment",
			NULL AS "InterestScheduleType",
			lgd."lossGivenDefaultDate" AS "anchor",
			CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (lgd."lossGivenDefaultValue", 'lossGivenDefaultValue', kv."Value") AS DECIMAL(34,11)) AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			lgd."CreditRiskLossGivenDefault_FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			lgd."CreditRiskLossGivenDefault_FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive",
			root."dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.common::get_lgd" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				'ComplexLoan'
				) AS lgd
		INNER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				:I_DATAGROUP_STRING
				) AS root ON root."FinancialContract_FinancialContractID" = lgd."CreditRiskLossGivenDefault_FinancialContract_FinancialContractID"
			AND root."FinancialContract_IDSystem" = lgd."CreditRiskLossGivenDefault_FinancialContract_IDSystem"

		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
			ON  kv."KeyID" = 'SAPPercentageStandard'
		
		UNION
		
		SELECT --CFS
			"arraySpecKind",
			NULL AS "cycleDate",
			NULL AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			NULL AS "capitalized",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "FirstInterestPeriodStartDate",
			NULL AS "FirstInterestPeriodEndDate",
			NULL AS "FirstInterestDueDate",
			NULL AS "LastInterestPeriodEndDate",
			NULL AS "DayOfMonthOfInterestPayment",
			NULL AS "InterestScheduleType",
			cf."anchor" AS "anchor",
			TO_DECIMAL(cf."amount", 34, 11) AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			cf."cashFlowKind" AS "cashFlowKind",	
			cf."IDSystem" AS "FinancialContract_IDSystem",
			cf."FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			root."IsActive",
			root."dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.loans::get_cashflow"(
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP
				) AS cf
		INNER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (
				:I_BUSINESS_DATE,
				:I_SYSTEM_TIMESTAMP,
				:I_DATAGROUP_STRING
				) AS root ON root."FinancialContract_FinancialContractID" = cf."FinancialContractID"
			AND root."FinancialContract_IDSystem" = cf."IDSystem"
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityMonetaryBalance"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP, 'SyndicatedLoan' )  mb ON --filter for currency
            	mb."FinancialContractID"				= cf."FinancialContractID" 
    		AND mb."IDSystem"							= cf."IDSystem"
        	AND mb."MonetaryBalanceCategory"			= 'LoanBalance'		
        WHERE	cf."currency" 							= mb."ContractCurrency"
        
		UNION
		
		SELECT 'CRTNG' AS "arraySpecKind",
			NULL AS "cycleDate",
			NULL AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			NULL AS "capitalized",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "FirstInterestPeriodStartDate",
			NULL AS "FirstInterestPeriodEndDate",
			NULL AS "FirstInterestDueDate",
			NULL AS "LastInterestPeriodEndDate",
			NULL AS "DayOfMonthOfInterestPayment",
			NULL AS "InterestScheduleType",
			NULL AS "anchor",
			NULL AS "amount",
			rat."ratingScale",
			rat."ratingClass",
			NULL AS "cashFlowKind",
			rat."Rating_FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			rat."Rating_FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			"rating_id",
			root."IsActive",
			root."dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.common::get_RatingFinancialContract"(
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			'ComplexLoan'
			) AS rat
		INNER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) AS root ON root."FinancialContract_FinancialContractID" = rat."Rating_FinancialContract_FinancialContractID"
		AND root."FinancialContract_IDSystem" = rat."Rating_FinancialContract_IDSystem"
		
		UNION
		
		SELECT 'CRTNG' AS "arraySpecKind",
			NULL AS "cycleDate",
			NULL AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			NULL AS "capitalized",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "FirstInterestPeriodStartDate",
			NULL AS "FirstInterestPeriodEndDate",
			NULL AS "FirstInterestDueDate",
			NULL AS "LastInterestPeriodEndDate",
			NULL AS "DayOfMonthOfInterestPayment",
			NULL AS "InterestScheduleType",
			NULL AS "anchor",
			NULL AS "amount",
			rat."ratingScale",
			rat."ratingClass",
			NULL AS "cashFlowKind",
			rat."CreditRiskExposure_FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			rat."CreditRiskExposure_FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			"rating_id",
			root."IsActive",
			root."dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.common::get_ContractLevelRating"(
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			'ComplexLoan'
			) AS rat
		INNER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) AS root ON root."FinancialContract_FinancialContractID" = rat."CreditRiskExposure_FinancialContract_FinancialContractID"
		AND root."FinancialContract_IDSystem" = rat."CreditRiskExposure_FinancialContract_IDSystem"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE ,:I_SYSTEM_TIMESTAMP)  AS kv
			ON  kv."KeyID" = 'CustomSpecificRatingMapping'
        WHERE	kv."Value" 							= 1
        
        UNION
        
        SELECT 'CRTNG' AS "arraySpecKind",
			NULL AS "cycleDate",
			NULL AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			NULL AS "capitalized",
			:I_BUSINESS_DATE AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "FirstInterestPeriodStartDate",
			NULL AS "FirstInterestPeriodEndDate",
			NULL AS "FirstInterestDueDate",
			NULL AS "LastInterestPeriodEndDate",
			NULL AS "DayOfMonthOfInterestPayment",
			NULL AS "InterestScheduleType",
			NULL AS "anchor",
			NULL AS "amount",
			rat."ratingScale",
			rat."ratingClass",
			NULL AS "cashFlowKind",
			rat."Rating_FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			rat."Rating_FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			"rating_id",
			root."IsActive",
			root."dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.common::C_get_RatingFinancialContract"(
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP
			) AS rat
		INNER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING
			) AS root ON root."FinancialContract_FinancialContractID" = rat."Rating_FinancialContract_FinancialContractID"
		AND root."FinancialContract_IDSystem" = rat."Rating_FinancialContract_IDSystem"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE ,:I_SYSTEM_TIMESTAMP)  AS kv
			ON  kv."KeyID" = 'CustomSpecificRatingMapping'
        WHERE	kv."Value" 							= 2
        
        UNION
        
        SELECT 'OE' AS "arraySpecKind",
			CASE 
				WHEN sideAgreementsExercisePeriodCNT."NumberOf" >= 2
					THEN sideAgreementsExercisePeriod."ExercisePeriod_StartDate"
				ELSE sideAgreementsOptionRight."OptionRight_BeginofExercisePeriod"
			END AS "cycleDate",
			'999Y' AS "cyclePeriod",
			NULL AS "pricingMarketObject",
			NULL AS "repricingType",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			NULL AS "ppAmount",
			NULL AS "cap",
			NULL AS "floor",
			NULL AS "gracePeriodStartDate",
			NULL AS "gracePeriodEndDate",
			NULL AS "graceType",
			NULL AS "capitalized",
			NULL AS "bookValueDate",
			root."contractID" AS "contractID",
			NULL AS "FirstInterestPeriodStartDate",
			NULL AS "FirstInterestPeriodEndDate",
			NULL AS "FirstInterestDueDate",
			NULL AS "LastInterestPeriodEndDate",
			NULL AS "DayOfMonthOfInterestPayment",
			NULL AS "InterestScheduleType",
			NULL AS "anchor",
			NULL AS "amount",
			NULL AS "ratingScale",
			NULL AS "ratingClass",
			NULL AS "cashFlowKind",
			root."FinancialContract_IDSystem" AS "FinancialContract_IDSystem",
			root."FinancialContract_FinancialContractID" AS "FinancialContract_FinancialContractID",
			NULL AS "rating_id",
			NULL AS "IsActive",
			root."dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Root"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) root

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_SideAgreements_get_OptionRight_Prio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS sideAgreementsOptionRightCNT
			ON	sideAgreementsOptionRightCNT."OptionRight_FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"
			AND sideAgreementsOptionRightCNT."OptionRight_FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"		
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_SideAgreements_get_ExercisePeriod_Prio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS sideAgreementsExercisePeriodCNT
			ON	sideAgreementsExercisePeriodCNT."ExercisePeriod_OptionRight_FinancialContractID"	= root."FinancialContract_FinancialContractID"
			AND sideAgreementsExercisePeriodCNT."ExercisePeriod_OptionRight_IDSystem"				= root."FinancialContract_IDSystem"		
		
		INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_OptionRight"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) sideAgreementsOptionRight
			ON	sideAgreementsOptionRight."OptionRight_FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"
			AND sideAgreementsOptionRight."OptionRight_FinancialContract_IDSystem"				= root."FinancialContract_IDSystem"
			AND (
				sideAgreementsExercisePeriodCNT."NumberOf" >= 2
				OR
				sideAgreementsExercisePeriodCNT."ExercisePeriod_OptionRight_FinancialContractID" IS NULL
				AND sideAgreementsOptionRightCNT."NumberOf" >= 2
			)
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ExercisePeriod"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) sideAgreementsExercisePeriod
			ON	sideAgreementsExercisePeriod."ExercisePeriod_OptionRight_FinancialContractID"	= sideAgreementsOptionRight."OptionRight_FinancialContract_FinancialContractID"
			AND	sideAgreementsExercisePeriod."ExercisePeriod_OptionRight_IDSystem"				= sideAgreementsOptionRight."OptionRight_FinancialContract_IDSystem"
			AND sideAgreementsExercisePeriod."ExercisePeriod_OptionRight_SequenceNumber"		= sideAgreementsOptionRight."OptionRight_SequenceNumber"	
			AND sideAgreementsExercisePeriodCNT."NumberOf" >= 2	
		
		) AS innerselect
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_datagroupDefaultValue" (:I_BUSINESS_DATE) AS dtgdef
		ON 1 = 1 ;
END;
