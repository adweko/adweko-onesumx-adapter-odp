FUNCTION "com.adweko.adapter.osx.inputdata.loans.complexLoan::Loan_get_Interest" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"FinancialContract_IDSystem" NVARCHAR(40),
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"Interest_InterestType" NVARCHAR(40),
		"Interest_FirstInterestPeriodStartDate" DATE,
		"Interest_FirstInterestPeriodEndDate" DATE,
		"Interest_ConditionFixedPeriodEndDate" DATE,
		"Interest_LastInterestPeriodEndDate" DATE,
		"Interest_FirstDueDate" DATE,
		"Interest_InterestSubPeriodSpecificationCategory" NVARCHAR(100),
		"Interest_DayOfMonthOfInterestPayment" INTEGER,
		"Interest_InterestScheduleType" NVARCHAR(100),
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),		
		"currentNominalInterestRate" DECIMAL(15, 11),
		"repricingType" NVARCHAR(1),
		"pricingMarketObject" NVARCHAR(40),
		"rateAdd" DECIMAL(15, 11),
		"rateMultiplier" DECIMAL(15, 11),
		"rateTerm" NVARCHAR(10),
		"fixingDays" NVARCHAR(10),
		"contractDayCountMethod" NVARCHAR(2),
		"interestPaymentType" NVARCHAR(1),
		"businessDayConvention" NVARCHAR(2),
		"calendar" NVARCHAR(75),
		"eomConvention" NVARCHAR(1),
		"gracePeriodStartDate" DATE,
		"gracePeriodEndDate" DATE,
		"capitalizationEndDate" DATE,
		"capitalized" INT,
		"Interest_SequenceNumber" INT,
		"MonetaryBalance_CapitalizedInterest" DECIMAL(34 ,6),
		"lookbackPeriod" NVARCHAR(10),
		"rateCappingFlooring" NVARCHAR(40),
		"rateAveragingCap" DECIMAL(15, 11),
		"rateAveragingFloor" DECIMAL(15, 11)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/************************************************
        Select and map Interest Data
    *************************************************/
	OUTPUT =

	SELECT DISTINCT --Regular IP Periods after Stub
		loan."FinancialContract_IDSystem",
		loan."FinancialContract_FinancialContractID",
		"Interest_InterestType",
		"Interest_FirstInterestPeriodStartDate",
		"Interest_FirstInterestPeriodEndDate",
		"Interest_ConditionFixedPeriodEndDate",
		"Interest_LastInterestPeriodEndDate",
		"Interest_FirstDueDate",
		"Interest_InterestSubPeriodSpecificationCategory",
		"Interest_DayOfMonthOfInterestPayment",
		"Interest_InterestScheduleType",
		'IP' AS "arraySpecKind",
		"Interest_FirstInterestPeriodEndDate" AS "cycleDate",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
				"Interest_InterestPeriodLength",
				mcp."cyclePeriod" ) 
		AS "cyclePeriod",		
		CASE 
			WHEN "Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
				THEN "Interest_FixedRate"
			WHEN "Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
				THEN "ApplicableInterestRate_Rate"
			ELSE NULL
		END AS "currentNominalInterestRate",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		NULL AS "rateAdd",
		NULL AS "rateMultiplier",
		NULL AS "rateTerm",
		NULL AS "fixingDays",
		dcm."dayCountMethod" AS "contractDayCountMethod",
		"com.adweko.adapter.osx.inputdata.common::get_interestPaymentType" ("Interest_InterestInAdvance", 2, 1) AS "interestPaymentType",
		"com.adweko.adapter.osx.inputdata.common::get_businessDayConvention" (bdc."OsxBusinessDayConventionCode") AS "businessDayConvention",
		cal."calendar" AS "calendar",
		"com.adweko.adapter.osx.inputdata.common::get_eomConvention" ("Interest_DayOfMonthOfInterestPeriodEnd") AS "eomConvention",
		NULL AS "gracePeriodStartDate",
		NULL AS "gracePeriodEndDate",
		MAP("Interest_InterestType",
			'Amortized',"Interest_LastInterestPeriodEndDate",
			NULL
		) AS "capitalizationEndDate",
		MAP("Interest_InterestType",
			'Amortized', 1,
			0
		) AS "capitalized",
		"Interest_SequenceNumber",
		pos."MonetaryBalance_CapitalizedInterest",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND loan."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
						loan."Interest_ResetCutoffLength",
						mcpr."cyclePeriod")
		END AS "lookbackPeriod",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND loan."Interest_InterestIsCompounded" = true
			THEN 'MONETARY_REFERENCE'
		END AS "rateCappingFlooring",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND loan."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", loan."Interest_FloatingRateMax", loan."Interest_VariableRateMax")
		END AS "rateAveragingCap",	
		CASE WHEN kvMonetaryRef."Value" = 'On' AND loan."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", loan."Interest_FloatingRateMin", loan."Interest_VariableRateMin")
		END AS "rateAveragingFloor"
	FROM "com.adweko.adapter.osx.inputdata.loans::Loan_Interest" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING, 'ComplexLoan') AS loan

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_BDC_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bdc
		ON bdc."CalculationDay" = loan."Interest_BusinessDayConvention"
		AND bdc."PaymentDay" = loan."Interest_DueScheduleBusinessDayConvention"
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::BV_LoanPosition" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING, 'ComplexLoan') AS pos 
		ON pos."FinancialContract_IDSystem" = loan."FinancialContract_IDSystem"
		AND pos."FinancialContract_FinancialContractID" = loan."FinancialContract_FinancialContractID"
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::get_valueDate" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING, 'ComplexLoan') AS valueDate
		ON valueDate."FinancialContract_IDSystem" = loan."FinancialContract_IDSystem"
		AND valueDate."FinancialContract_FinancialContractID" = loan."FinancialContract_FinancialContractID"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS dcm 
		ON dcm."DayCountConvention" = loan."Interest_DayCountConvention"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_calendar_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS cal 
		ON cal."BusinessCalendar" = loan."Interest_InterestBusinessCalendar"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = loan."Interest_InterestPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcpr
		ON  mcpr."BusinesscyclePeriod" = loan."Interest_ResetCutoffTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS cfss
		ON cfss."KeyID" = 'CapFloorSwitch'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS switchIPFromValueDate
		ON switchIPFromValueDate."KeyID" = 'SwitchIPFromValueDate'
		
	WHERE	loan."Interest_InterestCategory" = 'InterestPeriodSpecification' AND
			loan."Interest_FirstInterestPeriodEndDate" IS NOT NULL AND
			(((loan."Interest_LastInterestPeriodEndDate" > :I_BUSINESS_DATE OR loan."Interest_LastInterestPeriodEndDate" IS NULL) AND switchIPFromValueDate."Value" = 'Off')
			OR ((loan."Interest_LastInterestPeriodEndDate" > valueDate."valueDate" OR loan."Interest_LastInterestPeriodEndDate" IS NULL) AND switchIPFromValueDate."Value" = 'On')
			) AND
			loan."Interest_LastInterestPeriodEndDate" > loan."Interest_FirstInterestPeriodEndDate" AND
			loan."FinancialContract_TrancheInSyndication_SyndicationAgreement_FinancialContractID" IS NOT NULL AND
			loan."Interest_InterestPeriodLength" IS NOT NULL AND
			loan."Interest_InterestPeriodTimeUnit" IS NOT NULL
	
	UNION ALL

	SELECT DISTINCT --Stub Period and regular IP Periods
		loan."FinancialContract_IDSystem",
		loan."FinancialContract_FinancialContractID",
		"Interest_InterestType",
		"Interest_FirstInterestPeriodStartDate",
		"Interest_FirstInterestPeriodEndDate",
		"Interest_ConditionFixedPeriodEndDate",
		"Interest_LastInterestPeriodEndDate",
		"Interest_FirstDueDate",
		"Interest_InterestSubPeriodSpecificationCategory",
		"Interest_DayOfMonthOfInterestPayment",
		"Interest_InterestScheduleType",
		'IP' AS "arraySpecKind",
		CASE 
			WHEN row_number() over(partition by loan."FinancialContract_FinancialContractID", loan."FinancialContract_IDSystem" order by loan."Interest_FirstInterestPeriodStartDate" asc) = 1 THEN
				CASE "com.adweko.adapter.osx.inputdata.loans::get_valueDateCase"(pos."MonetaryBalance_FirstDisbursementDate", "Interest_FirstInterestPeriodStartDate", "Interest_FirstInterestPeriodEndDate")
					WHEN 1 THEN pos."MonetaryBalance_FirstDisbursementDate"
					ELSE "Interest_FirstInterestPeriodStartDate"
				END
			ELSE "Interest_FirstInterestPeriodStartDate"
		END AS "cycleDate",
		CASE 
			WHEN "Interest_FirstInterestPeriodEndDate" IS NOT NULL 
				THEN '999Y' 
			ELSE
				"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
					"Interest_InterestPeriodLength",
					mcp."cyclePeriod" )
		END AS "cyclePeriod",
		CASE 
			WHEN "Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
				THEN "Interest_FixedRate"
			WHEN "Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
				THEN "ApplicableInterestRate_Rate"
			ELSE NULL
		END AS "currentNominalInterestRate",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		NULL AS "rateAdd",
		NULL AS "rateMultiplier",
		NULL AS "rateTerm",
		NULL AS "fixingDays",
		dcm."dayCountMethod" AS "contractDayCountMethod",
		"com.adweko.adapter.osx.inputdata.common::get_interestPaymentType" ("Interest_InterestInAdvance", 2, 1) AS "interestPaymentType",
		"com.adweko.adapter.osx.inputdata.common::get_businessDayConvention" (bdc."OsxBusinessDayConventionCode") AS "businessDayConvention",
		cal."calendar" AS "calendar",
		"com.adweko.adapter.osx.inputdata.common::get_eomConvention" ("Interest_DayOfMonthOfInterestPeriodEnd") AS "eomConvention",
		NULL AS "gracePeriodStartDate",
		NULL AS "gracePeriodEndDate",
		MAP("Interest_InterestType",
			'Amortized',"Interest_LastInterestPeriodEndDate",
			NULL
		) AS "capitalizationEndDate",
		MAP("Interest_InterestType",
			'Amortized', 1,
			0
		) AS "capitalized",
		"Interest_SequenceNumber",
		pos."MonetaryBalance_CapitalizedInterest",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND loan."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
						loan."Interest_ResetCutoffLength",
						mcpr."cyclePeriod")
		END AS "lookbackPeriod",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND loan."Interest_InterestIsCompounded" = true
			THEN 'MONETARY_REFERENCE'
		END AS "rateCappingFlooring",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND loan."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", loan."Interest_FloatingRateMax", loan."Interest_VariableRateMax")
		END AS "rateAveragingCap",	
		CASE WHEN kvMonetaryRef."Value" = 'On' AND loan."Interest_InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", loan."Interest_FloatingRateMin", loan."Interest_VariableRateMin")
		END AS "rateAveragingFloor"
	FROM "com.adweko.adapter.osx.inputdata.loans::Loan_Interest" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING, 'ComplexLoan') AS loan
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_BDC_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bdc
		ON bdc."CalculationDay" = 'NoAdjustment'
		AND bdc."PaymentDay" = loan."Interest_BusinessDayConvention"
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::BV_LoanPosition" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING, 'ComplexLoan') AS pos 
		ON pos."FinancialContract_IDSystem" = loan."FinancialContract_IDSystem"
		AND pos."FinancialContract_FinancialContractID" = loan."FinancialContract_FinancialContractID"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS dcm 
		ON dcm."DayCountConvention" = loan."Interest_DayCountConvention"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_calendar_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS cal 
		ON cal."BusinessCalendar" = loan."Interest_InterestBusinessCalendar"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
			ON  mcp."BusinesscyclePeriod" = loan."Interest_InterestPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::get_valueDate" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING, 'ComplexLoan') AS valueDate
		ON valueDate."FinancialContract_IDSystem" = loan."FinancialContract_IDSystem"
		AND valueDate."FinancialContract_FinancialContractID" = loan."FinancialContract_FinancialContractID"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
			ON  kv."KeyID" = 'LoanProcessBulletInterest'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcpr
		ON  mcpr."BusinesscyclePeriod" = loan."Interest_ResetCutoffTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS cfss
		ON cfss."KeyID" = 'CapFloorSwitch'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS switchIPFromValueDate
		ON switchIPFromValueDate."KeyID" = 'SwitchIPFromValueDate'
		
	WHERE loan."Interest_InterestCategory" = 'InterestPeriodSpecification' AND
		 (((loan."Interest_LastInterestPeriodEndDate" > :I_BUSINESS_DATE OR loan."Interest_LastInterestPeriodEndDate" IS NULL) AND switchIPFromValueDate."Value" = 'Off') OR
			((loan."Interest_LastInterestPeriodEndDate" > valueDate."valueDate" OR loan."Interest_LastInterestPeriodEndDate" IS NULL) AND switchIPFromValueDate."Value" = 'On')
			)AND
		  loan."FinancialContract_TrancheInSyndication_SyndicationAgreement_FinancialContractID" IS NOT NULL AND
		  loan."Interest_FirstInterestPeriodStartDate" != IFNULL(loan."Interest_FirstInterestPeriodEndDate",'1900-01-01') AND
		  (loan."Interest_InterestPeriodLength" IS NOT NULL AND loan."Interest_InterestPeriodTimeUnit" IS NOT NULL OR
		  ("Interest_InterestType" IN ('LendingInterest', 'Amortized') AND "Interest_InterestScheduleType" = 'Bullet' AND kv."Value" = 1
		  	AND loan."Interest_LastInterestPeriodEndDate" IS NOT NULL
		  )) AND
		  NOT ( "Interest_FirstInterestPeriodEndDate" IS NOT NULL AND IFNULL(valueDate."valueDateCase",0) = 1 )
	
	UNION ALL
	
	SELECT DISTINCT --regular RP Periods and Stub Periods / regular DependentFloatingRateSpecification RP Period after Stub 
		loan."FinancialContract_IDSystem",
		loan."FinancialContract_FinancialContractID",
		"Interest_InterestType",
		"Interest_FirstInterestPeriodStartDate",
		"Interest_FirstInterestPeriodEndDate",
		"Interest_ConditionFixedPeriodEndDate",
		"Interest_LastInterestPeriodEndDate",
		"Interest_FirstDueDate",
		"Interest_InterestSubPeriodSpecificationCategory",
		"Interest_DayOfMonthOfInterestPayment",
		"Interest_InterestScheduleType",
		'RP' AS "arraySpecKind",
		CASE 
			WHEN 	"Interest_InterestSpecificationCategory" = 'FixedRateSpecification' -- Fixed Rate
					THEN	
							CASE 
								WHEN row_number() over(partition by loan."FinancialContract_FinancialContractID", loan."FinancialContract_IDSystem" order by loan."Interest_FirstInterestPeriodStartDate" asc) = 1 THEN
										CASE "com.adweko.adapter.osx.inputdata.loans::get_valueDateCase"(pos."MonetaryBalance_FirstDisbursementDate", "Interest_FirstInterestPeriodStartDate", "Interest_FirstInterestPeriodEndDate")
											WHEN 1 THEN pos."MonetaryBalance_FirstDisbursementDate"
											ELSE "Interest_FirstInterestPeriodStartDate"
											END
								ELSE "Interest_FirstInterestPeriodStartDate"
							END
			WHEN	"Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
				AND "Interest_FixingRateSpecificationCategory" = 'IndependentFloatingRateSpecification' -- Independent Floating Rate
					THEN
						CASE 
							WHEN "Interest_ResetAtMonthUltimo" = true
								THEN LAST_DAY("Interest_FirstRegularFloatingRateResetDate")
							ELSE "Interest_FirstRegularFloatingRateResetDate"
						END 
			WHEN	"Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
				AND "Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification' -- Dependent Floating Rate 
				AND	( "Interest_RelativeToInterestPeriodStartOrEnd" = 'Start' OR
				"Interest_RelativeToInterestPeriodStartOrEnd" IS NULL )
					THEN 
					CASE 
						WHEN row_number() over(partition by loan."FinancialContract_FinancialContractID", loan."FinancialContract_IDSystem" order by loan."Interest_FirstInterestPeriodStartDate" asc) = 1 THEN
								CASE "com.adweko.adapter.osx.inputdata.loans::get_valueDateCase"(pos."MonetaryBalance_FirstDisbursementDate", "Interest_FirstInterestPeriodStartDate", "Interest_FirstInterestPeriodEndDate")
									WHEN 1 THEN pos."MonetaryBalance_FirstDisbursementDate"
									ELSE "Interest_FirstInterestPeriodStartDate"
								END
						ELSE "Interest_FirstInterestPeriodStartDate"
					END
			WHEN	"Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
				AND "Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification' -- Dependent Floating Rate 
				AND	"Interest_RelativeToInterestPeriodStartOrEnd" = 'End'
				THEN "Interest_FirstInterestPeriodEndDate"
		END AS "cycleDate",
		CASE 
			WHEN 	"Interest_InterestSpecificationCategory" = 'FixedRateSpecification' -- Fixed Rate
				THEN CASE 
						WHEN ("Interest_InterestScheduleType" = 'Bullet' AND loan."Interest_InterestPeriodLength" IS NULL AND loan."Interest_InterestPeriodTimeUnit" IS NULL)
						THEN '999Y'
						ELSE
						"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
						"Interest_InterestPeriodLength",
						mcp3."cyclePeriod"
						)
					END
			WHEN	"Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
				AND "Interest_FixingRateSpecificationCategory" = 'IndependentFloatingRateSpecification' -- Independent Floating Rate
			THEN	"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
					"Interest_ResetPeriodLength",
					mcp4."cyclePeriod"
					)
			WHEN	"Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
				AND "Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification' -- Dependent Floating Rate
				   THEN CASE 
						WHEN ("Interest_InterestScheduleType" = 'Bullet' AND loan."Interest_InterestPeriodLength" IS NULL AND loan."Interest_InterestPeriodTimeUnit" IS NULL)
						THEN '999Y'
						ELSE	
						"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
						"Interest_InterestPeriodLength",
						mcp3."cyclePeriod" )	
					END
			ELSE '999Y'
		END AS "cyclePeriod", 

		NULL AS "currentNominalInterestRate",
		"com.adweko.adapter.osx.inputdata.common::get_repricingType"(
			'',
			"Interest_InterestSpecificationCategory",
			"Interest_FixingRateSpecificationCategory",
			"Interest_ResetInArrears",
			"Interest_ResetAtMonthUltimo",
			"Interest_CutoffRelativeToDate"
			) AS "repricingType",
		MAP(kvMonetaryRef."Value",
			'On', CASE WHEN loan."Interest_InterestIsCompounded" = true
					THEN pos."ContractCurrency"
							|| '-' 
							|| pmo."pricingMarketObject"
							|| '-'
							|| CASE 
									WHEN 	"Interest_InterestSpecificationCategory" = 'FixedRateSpecification' -- Fixed Rate
										THEN CASE 
												WHEN ("Interest_InterestScheduleType" = 'Bullet' AND loan."Interest_InterestPeriodLength" IS NULL AND loan."Interest_InterestPeriodTimeUnit" IS NULL)
												THEN '999Y'
												ELSE
												"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
												"Interest_InterestPeriodLength",
												mcp3."cyclePeriod"
												)
											END
									WHEN	"Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
										AND "Interest_FixingRateSpecificationCategory" = 'IndependentFloatingRateSpecification' -- Independent Floating Rate
									THEN	"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
											"Interest_ResetPeriodLength",
											mcp4."cyclePeriod"
											)
									WHEN	"Interest_InterestSpecificationCategory" = 'FloatingRateSpecification'
										AND "Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification' -- Dependent Floating Rate
										   THEN CASE 
												WHEN ("Interest_InterestScheduleType" = 'Bullet' AND loan."Interest_InterestPeriodLength" IS NULL AND loan."Interest_InterestPeriodTimeUnit" IS NULL)
												THEN '999Y'
												ELSE	
												"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
												"Interest_InterestPeriodLength",
												mcp3."cyclePeriod" )	
											END
									ELSE '999Y'
								END
							|| '-MR'
					ELSE pmo."pricingMarketObject" END,
			'Off', pmo."pricingMarketObject") AS "pricingMarketObject",
		MAP("Interest_InterestSpecificationCategory",
			'FloatingRateSpecification', "Interest_Spread",
			'FixedRateSpecification', "Interest_FixedRate"
			) AS "rateAdd",
		MAP("Interest_InterestSpecificationCategory",
			'FloatingRateSpecification', "Interest_ReferenceRateFactor",
			NULL
			) AS "rateMultiplier",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			referenceRate."ReferenceRate_TimeToMaturity",
			mcp1."cyclePeriod"
			) AS "rateTerm",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			"Interest_ResetLagLength",
			mcp2."cyclePeriod"
			) AS "fixingDays", 
		NULL AS "contractDayCountMethod",
		NULL AS "interestPaymentType",
		NULL AS "businessDayConvention",
		NULL AS "calendar",
		NULL AS "eomConvention",
		NULL AS "gracePeriodStartDate",
		NULL AS "gracePeriodEndDate",
		NULL AS "capitalizationEndDate",
		NULL AS "capitalized",
		"Interest_SequenceNumber",
		NULL AS "MonetaryBalance_CapitalizedInterest",
		NULL AS "lookbackPeriod",
		NULL AS "rateCappingFlooring",
		NULL AS "rateAveragingCap",
		NULL AS "rateAveragingFloor"
	FROM "com.adweko.adapter.osx.inputdata.loans::Loan_Interest" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP,
			:I_DATAGROUP_STRING,
			'ComplexLoan'
			) AS loan
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_pricingMarketObject_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pmo 
			ON	pmo."ReferenceRateID" = loan."Interest_ReferenceRateID"
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ReferenceRate" (
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP
			) AS referenceRate 
			ON referenceRate."ReferenceRate_ReferenceRateID" = loan."Interest_ReferenceRateID"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp1
			ON  mcp1."BusinesscyclePeriod" = referenceRate."ReferenceRate_TimeToMaturityUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp2
			ON  mcp2."BusinesscyclePeriod" = loan."Interest_ResetLagTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp3
			ON  mcp3."BusinesscyclePeriod" = loan."Interest_InterestPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp4
			ON  mcp4."BusinesscyclePeriod" = loan."Interest_ResetPeriodTimeUnit"
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.loans::BV_LoanPosition" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING, 'ComplexLoan') AS pos 
		ON pos."FinancialContract_IDSystem" = loan."FinancialContract_IDSystem"
		AND pos."FinancialContract_FinancialContractID" = loan."FinancialContract_FinancialContractID"
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
		ON  kv."KeyID" = 'LoanProcessBulletInterest'
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'

	WHERE loan."FinancialContract_TrancheInSyndication_SyndicationAgreement_FinancialContractID" IS NOT NULL AND
	(loan."Interest_LastInterestPeriodEndDate" > :I_BUSINESS_DATE OR loan."Interest_LastInterestPeriodEndDate" IS NULL)
	--AND loan."Interest_InterestScheduleType" = 'RegularPeriodic'
	AND
			(
				(loan."Interest_InterestSpecificationCategory" IN ('FixedRateSpecification') 
				AND (loan."Interest_InterestPeriodLength" IS NOT NULL AND loan."Interest_InterestPeriodTimeUnit" IS NOT NULL OR
					("Interest_InterestType" IN ('LendingInterest', 'Amortized') AND "Interest_InterestScheduleType" = 'Bullet' AND kv."Value" = 1
		  			AND loan."Interest_LastInterestPeriodEndDate" IS NOT NULL))) 
			OR
				(loan."Interest_InterestSpecificationCategory" IN ('FloatingRateSpecification') 
				AND "Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification' 
				AND (loan."Interest_InterestPeriodLength" IS NOT NULL AND loan."Interest_InterestPeriodTimeUnit" IS NOT NULL OR
					("Interest_InterestType" IN ('LendingInterest', 'Amortized') AND "Interest_InterestScheduleType" = 'Bullet' AND kv."Value" = 1
		  			AND loan."Interest_LastInterestPeriodEndDate" IS NOT NULL))) 
			OR 
				(loan."Interest_InterestSpecificationCategory" IN ('FloatingRateSpecification') 
				AND "Interest_FixingRateSpecificationCategory" = 'IndependentFloatingRateSpecification' 
				AND loan."Interest_ResetPeriodLength" IS NOT NULL 
				AND loan."Interest_ResetPeriodTimeUnit" IS NOT NULL)
			);

	RETURN :output;
END;