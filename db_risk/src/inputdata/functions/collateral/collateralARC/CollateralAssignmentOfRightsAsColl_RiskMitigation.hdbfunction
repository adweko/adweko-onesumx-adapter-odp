FUNCTION "com.adweko.adapter.osx.inputdata.collateral.collateralARC::CollateralAssignmentOfRightsAsColl_RiskMitigation" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"mitigationGroupID" NVARCHAR(75),
		"contractRefid" NVARCHAR(256),
		"partnerRefid" NVARCHAR(256),
		"crmRole" NVARCHAR(3),
		"dataGroup" NVARCHAR(128),
		"contractRefidOrig" NVARCHAR(400)  --for internal use / tests
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN
	
	SELECT 
		CAST((HASH_MD5(to_binary("mitigationGroupID" || coalesce("contractRefid", '') || coalesce("partnerRefid", '')))) AS NVARCHAR(32)) AS "id",
		"mitigationGroupID",
		"contractRefid",
		"partnerRefid",
		"crmRole",
		"dataGroup",
		"contractRefidOrig"
	FROM (
		SELECT 
			"com.adweko.adapter.osx.inputdata.common::get_fcID"(
				i_FinancialContractID		=> root."FinancialContract_FinancialContractID",
				i_IDSystem					=> root."FinancialContract_IDSystem"
			) AS "mitigationGroupID",
			CAST("com.adweko.adapter.osx.inputdata.common::get_collateralPortionID"(
				i_FinancialContractID														=> root."FinancialContract_FinancialContractID",
				i_IDSystem																	=> IFNULL(root."FinancialContract_IDSystem",''),
				i_ObjectCategoryID															=> root."AssignmentOfRightCategoryID",
				i_CollateralPortionNumber													=> root."CollateralPortion_PortionNumber",
				i_PhysicalAsset_PhysicalAssetID												=> NULL,
				i_FinancialContract_FinancialContractID										=> root."AssignedRightAsCollateral_AssignedFinancialContract_FinancialContractID",
				i_Receivable_ReceivableID													=> root."AssignedRightAsCollateral_DerivedReceivableID",
				i_ThirdPartyBankAccountID													=> NULL,
				i_BusinessInterest_TargetInBusinessPartnerRelation_BusinessPartnerID		=> root."AssignedRightAsCollateral_AssignedBusinessInterest_ASSOC_TargetInBusinessPartnerRelation_BusinessPartnerID",
				i_prefix																	=> 'C',
				i_hash																		=> false
			) AS NVARCHAR(256)) AS "contractRefid",
			COALESCE(
				"CollateralProvider_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
				"Pledger_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID",
				"CollateralPortionProvider_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
			) AS "partnerRefid",
			'ENH' AS "crmRole",
			root."dataGroup",
			"com.adweko.adapter.osx.inputdata.common::get_collateralPortionID"(
				i_FinancialContractID														=> root."FinancialContract_FinancialContractID",
				i_IDSystem																	=> IFNULL(root."FinancialContract_IDSystem",''),
				i_ObjectCategoryID															=> root."AssignmentOfRightCategoryID",
				i_CollateralPortionNumber													=> root."CollateralPortion_PortionNumber",
				i_PhysicalAsset_PhysicalAssetID												=> NULL,
				i_FinancialContract_FinancialContractID										=> root."AssignedRightAsCollateral_AssignedFinancialContract_FinancialContractID",
				i_Receivable_ReceivableID													=> root."AssignedRightAsCollateral_DerivedReceivableID",
				i_ThirdPartyBankAccountID													=> NULL,
				i_BusinessInterest_TargetInBusinessPartnerRelation_BusinessPartnerID		=> root."AssignedRightAsCollateral_AssignedBusinessInterest_ASSOC_TargetInBusinessPartnerRelation_BusinessPartnerID",
				i_prefix																	=> NULL,
				i_hash																		=> false
			) AS "contractRefidOrig"
		FROM "com.adweko.adapter.osx.inputdata.collateral.collateralARC::CollateralAssignmentOfRightsAsColl_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
	
	
		UNION
		
		
		SELECT DISTINCT
			"com.adweko.adapter.osx.inputdata.common::get_fcID"(
				i_FinancialContractID		=> root."FinancialContract_FinancialContractID",
				i_IDSystem					=> root."FinancialContract_IDSystem"
			) AS "mitigationGroupID",
			collaterizedFC."contractID" AS "contractRefid",
			bp."Borrower_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" AS "partnerRefid",
			'EXP' AS "crmRole",
			root."dataGroup",
			collaterizedFC."contractID" || '0001' || collaterizedFC."dataGroup" AS "contractRefidOrig"
			
		FROM "com.adweko.adapter.osx.inputdata.collateral.collateralARC::CollateralAssignmentOfRightsAsColl_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
		
		INNER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralPortionContractAssignment"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cpca
			ON cpca."CollateralPortionContractAssignment_CollateralAgreement_FinancialContractID"	= root."FinancialContract_FinancialContractID"
			AND cpca."CollateralPortionContractAssignment_CollateralAgreement_IDSystem" 			= root."FinancialContract_IDSystem"
			AND cpca."CollateralPortionContractAssignment_PortionNumber"							= root."CollateralPortion_PortionNumber"
	    
	    INNER JOIN "com.adweko.adapter.osx.inputdata.collateral.common::Collateral_RiskMitigationEXP"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) collaterizedFC
	    	ON	collaterizedFC."FinancialContract_FinancialContractID"	= cpca."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"
	    	AND collaterizedFC."FinancialContract_IDSystem"				= cpca."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem"
	
		INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_BusinessPartnerContractAssignmentFlat"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bp
			ON	bp."BusinessPartnerContractAssignment_FinancialContract_FinancialContractID"	= collaterizedFC."FinancialContract_FinancialContractID"
			AND	bp."BusinessPartnerContractAssignment_FinancialContract_IDSystem"				= collaterizedFC."FinancialContract_IDSystem"
			AND bp."Borrower_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
			AND bp."Borrower_BusinessPartnerContractAssignment_ContractDataOwner" = false

		GROUP BY
			root."FinancialContract_FinancialContractID",
			root."FinancialContract_IDSystem",
			root."dataGroup",
			collaterizedFC."FinancialContract_FinancialContractID",
			collaterizedFC."FinancialContract_IDSystem",
			collaterizedFC."contractID",
			collaterizedFC."dataGroup",
			bp."Borrower_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
			
		);
END;
