FUNCTION "com.adweko.adapter.osx.inputdata.collateral.collateralCash::CollateralCash_Contract" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4) ,
		"bookValueDate" DATE,
		"dealDate" DATE,
		"valueDate" DATE,
		"maturityDate" DATE,
		"counterparty" NVARCHAR(256),
		"clearingHouse" NVARCHAR(256),
		"marketValueObserved" DECIMAL(34,6),
		"marketValueDate" DATE,
		"currency" NVARCHAR(3),
		"currentPrincipal" DECIMAL(34, 6),
		"contractRole" NVARCHAR(1),
		"optionHolder" NVARCHAR(1),
		"pledgedEndDate" DATE,
		"assetOwnerCode" NVARCHAR(255),
		"isUnderLiquidityManagementControl" INT,
		"dataGroup" NVARCHAR(128),
		"legalEntity" NVARCHAR(128),
		"lmEntityName" NVARCHAR(128),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
		"bookValue" DECIMAL(34, 6),
		"exposureClass" NVARCHAR(255)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN

	SELECT 
		CAST("com.adweko.adapter.osx.inputdata.common::get_collateralPortionID"(
			i_FinancialContractID														=> root."FinancialContract_MarginAgreement_FinancialContractID",
			i_IDSystem																	=> NULL,
			i_ObjectCategoryID															=> 'CASH',
			i_CollateralPortionNumber													=> IFNULL(root."CollateralPortion_PortionNumber",1),
			i_PhysicalAsset_PhysicalAssetID												=> NULL,
			i_FinancialContract_FinancialContractID										=> IFNULL('#' || root."FinancialContract_CA_FinancialContractID",''),
			i_Receivable_ReceivableID													=> NULL,
			i_ThirdPartyBankAccountID													=> NULL,
			i_BusinessInterest_TargetInBusinessPartnerRelation_BusinessPartnerID		=> NULL,
			i_prefix																	=> NULL,
			i_hash																		=> true
		) AS NVARCHAR(32)) AS "id", 
		CAST("com.adweko.adapter.osx.inputdata.common::get_collateralPortionID"(
			i_FinancialContractID														=> root."FinancialContract_MarginAgreement_FinancialContractID",
			i_IDSystem																	=> root."FinancialContract_MAorCA_IDSystem",
			i_ObjectCategoryID															=> 'CASH',
			i_CollateralPortionNumber													=> IFNULL(root."CollateralPortion_PortionNumber",1),
			i_PhysicalAsset_PhysicalAssetID												=> NULL,
			i_FinancialContract_FinancialContractID										=> IFNULL('#' || root."FinancialContract_CA_FinancialContractID",''),
			i_Receivable_ReceivableID													=> NULL,
			i_ThirdPartyBankAccountID													=> NULL,
			i_BusinessInterest_TargetInBusinessPartnerRelation_BusinessPartnerID		=> NULL,
			i_prefix																	=> 'C',
			i_hash																		=> false
		) AS NVARCHAR(256)) AS "sourceSystemRecordNumber",
		CAST("com.adweko.adapter.osx.inputdata.common::get_collateralPortionID"(
			i_FinancialContractID														=> root."FinancialContract_MarginAgreement_FinancialContractID",
			i_IDSystem																	=> NULL,
			i_ObjectCategoryID															=> 'CASH',
			i_CollateralPortionNumber													=> IFNULL(root."CollateralPortion_PortionNumber",1),
			i_PhysicalAsset_PhysicalAssetID												=> NULL,
			i_FinancialContract_FinancialContractID										=> IFNULL('#' || root."FinancialContract_CA_FinancialContractID",''),
			i_Receivable_ReceivableID													=> NULL,
			i_ThirdPartyBankAccountID													=> NULL,
			i_BusinessInterest_TargetInBusinessPartnerRelation_BusinessPartnerID		=> NULL,
			i_prefix																	=> NULL,
			i_hash																		=> false
		) AS NVARCHAR(128)) AS "sourceSystemID",
		root."FinancialContract_MAorCA_IDSystem" AS "sourceSystemName",
		'0001' AS "node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		root."FinancialContract_MAorCA_OriginalSigningDate" AS "dealDate",
		root."BusinessPartnerContractAssignment_ContractDataOwner_BusinessValidFrom" AS "valueDate",
		NULL AS "maturityDate",
		root."BusinessPartnerContractAssignment_Counterparty_BusinessPartnerID" AS "counterparty",
		CASE 
			WHEN (keyclearingHouse."Value"='BusinessPartner' OR keyclearingHouse."Value" IS NULL)
				AND root."isClearingHouseOeCentralCounterParty" = true
					THEN root."BusinessPartnerContractAssignment_Counterparty_BusinessPartnerID"
			WHEN keyclearingHouse."Value"='BusinessPartnerContractAssignment'
					THEN root."RatingSubstitute_BusinessPartnerContractAssignment_Counterparty_BusinessPartnerID"
		END AS "clearingHouse",
		CASE
			WHEN keymarketValueSwitch."Value" = 'MonetaryBalance' OR keymarketValueSwitch."Value" IS NULL
					THEN ABS(root."MonetaryBalance_CollateralBalance") * root."isActiveSign"
 			WHEN keymarketValueSwitch."Value" = 'CollateralDistributionResult' 
					THEN ABS(prioDistMethod."CollateralDistributionResult_AssignedCollateralAmount") * root."isActiveSign"
		END AS "marketValueObserved",
		CASE
			WHEN keymarketValueSwitch."Value" = 'MonetaryBalance' OR keymarketValueSwitch."Value" IS NULL
					THEN root."MonetaryBalance_BusinessValidFrom"
			WHEN keymarketValueSwitch."Value" = 'CollateralDistributionResult'
					THEN prioDistMethod."CollateralDistributionResult_BusinessValidFrom"
		END AS "marketValueDate",
		CASE 
			WHEN keycurrency."Value"='BusinessPartnerContractAssignment' OR keycurrency."Value" IS NULL
					THEN root."BusinessPartnerContractAssignment_ContractDataOwner_ThresholdAmountCurrency"
			WHEN keycurrency."Value"='FinancialContract'
					THEN root."FinancialContract_MAorCA_MaximumCollateralAmountCurrency"
		END AS "currency",
		CASE
			WHEN keymarketValueSwitch."Value" = 'MonetaryBalance' OR keymarketValueSwitch."Value" IS NULL
					THEN ABS(root."MonetaryBalance_CollateralBalance") * root."isActiveSign"
 			WHEN keymarketValueSwitch."Value" = 'CollateralDistributionResult' 
					THEN ABS(prioDistMethod."CollateralDistributionResult_AssignedCollateralAmount") * root."isActiveSign"
		END AS "currentPrincipal",
		'2' AS "contractRole",
		'1' AS "optionHolder",
		MAP(root."BusinessPartnerContractAssignment_ContractDataOwner_MarginPeriodTimeUnit",
			'Day', ADD_DAYS(root."BusinessPartnerContractAssignment_ContractDataOwner_BusinessValidFrom", root."BusinessPartnerContractAssignment_ContractDataOwner_MarginPeriod"),
			'Week', ADD_DAYS(root."BusinessPartnerContractAssignment_ContractDataOwner_BusinessValidFrom", root."BusinessPartnerContractAssignment_ContractDataOwner_MarginPeriod" * 7),
			'Month', ADD_MONTHS(root."BusinessPartnerContractAssignment_ContractDataOwner_BusinessValidFrom", root."BusinessPartnerContractAssignment_ContractDataOwner_MarginPeriod"),
			'Quarter', ADD_MONTHS(root."BusinessPartnerContractAssignment_ContractDataOwner_BusinessValidFrom", root."BusinessPartnerContractAssignment_ContractDataOwner_MarginPeriod" * 3),
			'Year', ADD_YEARS(root."BusinessPartnerContractAssignment_ContractDataOwner_BusinessValidFrom", root."BusinessPartnerContractAssignment_ContractDataOwner_MarginPeriod")
		) AS "pledgedEndDate",
		CASE
			WHEN root."BusinessPartnerContractAssignment_Provider_ContractDataOwner_BusinessPartnerID" IS NOT NULL
				THEN 'THEN FIRM'
			WHEN root."BusinessPartnerContractAssignment_Receiver_ContractDataOwner_BusinessPartnerID" IS NOT NULL
				THEN 'OTHER'
		END AS "assetOwnerCode",
		1 AS "isUnderLiquidityManagementControl",
		root."dataGroup" AS "dataGroup",
		root."BusinessPartnerContractAssignment_ContractDataOwner_BusinessPartnerID" AS "legalEntity",
		'CustomerAccount' AS "lmEntityName",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",
		bok."BookValue_BookValue" AS "bookValue",
		cre."exposureClass"
	
	FROM "com.adweko.adapter.osx.inputdata.collateral.collateralCash::CollateralCash_Root_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.collateral.collateralCash::CollateralCash_get_PrioDistMethod"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS prioDistMethod
		ON prioDistMethod."CollateralPortion_CollateralAgreement_FinancialContractID" = root."FinancialContract_MAorCA_FinancialContractID"
		AND prioDistMethod."CollateralPortion_CollateralAgreement_IDSystem" = root."FinancialContract_MAorCA_IDSystem"
		AND prioDistMethod."CollateralPortionContractAssignment_PortionNumber" = root."CollateralPortion_PortionNumber"		

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Collateral') AS cre
		ON cre."CreditRiskExposure_CollateralPortion_CollateralAgreement_FinancialContractID"	= root."CollateralPortion_CollateralAgreement_FinancialContractID"
		AND cre."CreditRiskExposure_FinancialContract_IDSystem" 								= root."CollateralPortion_CollateralAgreement_IDSystem"
		AND cre."CreditRiskExposure_CollateralPortion_PortionNumber"							= root."CollateralPortion_PortionNumber"

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS keyclearingHouse 
		 ON keyclearingHouse."KeyID" = 'clearingHouseSwitch'
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS keycurrency 
		 ON keycurrency."KeyID" = 'currencySwitch'
		 
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS keymarketValueSwitch 
		 ON keymarketValueSwitch."KeyID" = 'cashCollateralMarketValueSwitch'

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
		ON bok."BookValue_FinancialContract_IDSystem" = root."FinancialContract_MarginAgreement_IDSystem"
		AND bok."BookValue_FinancialContract_FinancialContractID" = root."FinancialContract_MarginAgreement_FinancialContractID"
		AND bok."BookValue_BookValueCurrency"	= 	CASE
														WHEN keycurrency."Value"='BusinessPartnerContractAssignment' OR keycurrency."Value" IS NULL
																THEN root."BusinessPartnerContractAssignment_ContractDataOwner_ThresholdAmountCurrency"
														WHEN keycurrency."Value"='FinancialContract'
																THEN root."FinancialContract_MAorCA_MaximumCollateralAmountCurrency"
													END
	;
END;