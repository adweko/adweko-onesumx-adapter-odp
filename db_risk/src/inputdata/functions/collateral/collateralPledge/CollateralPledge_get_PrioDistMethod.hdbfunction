FUNCTION "com.adweko.adapter.osx.inputdata.collateral.collateralPledge::CollateralPledge_get_PrioDistMethod"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"CollateralPortion_CollateralAgreement_FinancialContractID" NVARCHAR(128),
		"CollateralPortion_CollateralAgreement_IDSystem" NVARCHAR(40),
		"CollateralPortionContractAssignment_PortionNumber" INTEGER,
		"CollateralDistributionResult_AssignedCollateralAmount" DECIMAL(34, 6),
		"CollateralDistributionResult_BusinessValidFrom" DATE
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN

	SELECT DISTINCT
		"CollateralPortion_CollateralAgreement_FinancialContractID",
		"CollateralPortion_CollateralAgreement_IDSystem",
		"CollateralPortionContractAssignment_PortionNumber",
		"CollateralDistributionResult_AssignedCollateralAmount",
		"CollateralDistributionResult_BusinessValidFrom"
	
	FROM (
		SELECT
			root."CollateralPortion_CollateralAgreement_FinancialContractID",
			root."CollateralPortion_CollateralAgreement_IDSystem",
			collport."CollateralPortionContractAssignment_PortionNumber",
			colldist."CollateralDistributionResult_AssignedCollateralAmount",
			colldist."CollateralDistributionResult_BusinessValidFrom",
			ROW_NUMBER() OVER(
				PARTITION BY
					root."CollateralPortion_CollateralAgreement_FinancialContractID",
					root."CollateralPortion_CollateralAgreement_IDSystem",
					collport."CollateralPortionContractAssignment_PortionNumber"
				ORDER BY
					prio."Priority" ASC
			) AS "Prio"
		FROM "com.adweko.adapter.osx.inputdata.collateral.collateralPledge::CollateralPledge_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_PhysicalAsset"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pledgedPA
			ON pledgedPA."PhysicalAsset_PhysicalAssetID" = root."PledgedObject_PledgedPhysicalAsset_PhysicalAssetID"
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralPortionContractAssignment"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS collport
			ON collport."CollateralPortionContractAssignment_CollateralAgreement_FinancialContractID" = root."CollateralPortion_CollateralAgreement_FinancialContractID"
			AND collport."CollateralPortionContractAssignment_CollateralAgreement_IDSystem" = root."CollateralPortion_CollateralAgreement_IDSystem"
			AND collport."CollateralPortionContractAssignment_PortionNumber" = root."CollateralPortion_PortionNumber"
			
		INNER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralDistributionResult"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS colldist
			ON (
					colldist."CollateralDistributionResult_CollateralPortion_ASSOC_CollateralAgreement_FinancialContractID" = root."FinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_CollateralPortion_ASSOC_CollateralAgreement_IDSystem" = root."FinancialContract_IDSystem"
				AND colldist."CollateralDistributionResult_CollateralPortion_PortionNumber" = collport."CollateralPortionContractAssignment_PortionNumber"
				AND colldist."CollateralDistributionResult_FinancialContract_FinancialContractID" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_FinancialContract_IDSystem" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem"
				AND (colldist."CollateralDistributionResult_PhysicalAssetUsedAsCollateral_PhysicalAssetID" = pledgedPA."PhysicalAsset_PhysicalAssetID"
					OR pledgedPA."PhysicalAsset_PhysicalAssetID" IS NULL
					OR trim(colldist."CollateralDistributionResult_PhysicalAssetUsedAsCollateral_PhysicalAssetID") = ''
				)
			)
			OR
			(
					colldist."CollateralDistributionResult_SimpleCollateralAgreement_FinancialContractID" = root."FinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_SimpleCollateralAgreement_IDSystem" = root."FinancialContract_IDSystem"
				AND colldist."CollateralDistributionResult_FinancialContract_FinancialContractID" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_FinancialContract_IDSystem" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem"
				AND (colldist."CollateralDistributionResult_PhysicalAssetUsedAsCollateral_PhysicalAssetID" = pledgedPA."PhysicalAsset_PhysicalAssetID"
					OR pledgedPA."PhysicalAsset_PhysicalAssetID" IS NULL
					OR trim(colldist."CollateralDistributionResult_PhysicalAssetUsedAsCollateral_PhysicalAssetID") = ''
				)
			)
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_CollateralDistributionMethod_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS prio
			ON prio."CollateralDistributionMethod" = colldist."CollateralDistributionResult_CollateralDistributionMethod"
		
		WHERE root."FinancialContract_SimpleCollateralCategory" = 'Pledge'
			AND root."PledgedObject_PledgedObjectCategory" = 'PledgeOfPhysicalAsset'

		UNION ALL
		
		SELECT
			root."CollateralPortion_CollateralAgreement_FinancialContractID",
			root."CollateralPortion_CollateralAgreement_IDSystem",
			collport."CollateralPortionContractAssignment_PortionNumber",
			colldist."CollateralDistributionResult_AssignedCollateralAmount",
			colldist."CollateralDistributionResult_BusinessValidFrom",
			ROW_NUMBER() OVER(
				PARTITION BY
					root."CollateralPortion_CollateralAgreement_FinancialContractID",
					root."CollateralPortion_CollateralAgreement_IDSystem",
					collport."CollateralPortionContractAssignment_PortionNumber"
				ORDER BY
					prio."Priority" ASC
			) AS "Prio"
		FROM "com.adweko.adapter.osx.inputdata.collateral.collateralPledge::CollateralPledge_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
		
    	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fc
    		ON fc."FinancialContractID" = root."PledgedObject_PledgedFinancialContract_FinancialContractID"
    		AND fc."IDSystem" = root."PledgedObject_PledgedFinancialContract_IDSystem"
    	
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralPortionContractAssignment"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS collport
			ON collport."CollateralPortionContractAssignment_CollateralAgreement_FinancialContractID" = root."CollateralPortion_CollateralAgreement_FinancialContractID"
			AND collport."CollateralPortionContractAssignment_CollateralAgreement_IDSystem" = root."CollateralPortion_CollateralAgreement_IDSystem"
			AND collport."CollateralPortionContractAssignment_PortionNumber" = root."CollateralPortion_PortionNumber"
		
		INNER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralDistributionResult"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS colldist
			ON (
					colldist."CollateralDistributionResult_CollateralPortion_ASSOC_CollateralAgreement_FinancialContractID" = root."FinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_CollateralPortion_ASSOC_CollateralAgreement_IDSystem" = root."FinancialContract_IDSystem"
				AND colldist."CollateralDistributionResult_CollateralPortion_PortionNumber" = collport."CollateralPortionContractAssignment_PortionNumber"
				AND colldist."CollateralDistributionResult_FinancialContract_FinancialContractID" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_FinancialContract_IDSystem" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem"
				AND ((colldist."CollateralDistributionResult_FinancialContractUsedAsCollateral_FinancialContractID" = fc."FinancialContractID" AND colldist."CollateralDistributionResult_FinancialContractUsedAsCollateral_IDSystem" = fc."IDSystem")
					OR (fc."FinancialContractID" IS NULL AND fc."IDSystem" IS NULL)
					OR (trim(colldist."CollateralDistributionResult_FinancialContractUsedAsCollateral_FinancialContractID") = '' AND trim(colldist."CollateralDistributionResult_FinancialContractUsedAsCollateral_IDSystem") = '')
					)
			)
			OR
			(
					colldist."CollateralDistributionResult_SimpleCollateralAgreement_FinancialContractID" = root."FinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_SimpleCollateralAgreement_IDSystem" = root."FinancialContract_IDSystem"
				AND colldist."CollateralDistributionResult_FinancialContract_FinancialContractID" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_FinancialContract_IDSystem" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem"
				AND ((colldist."CollateralDistributionResult_FinancialContractUsedAsCollateral_FinancialContractID" = fc."FinancialContractID" AND colldist."CollateralDistributionResult_FinancialContractUsedAsCollateral_IDSystem" = fc."IDSystem")
					OR (fc."FinancialContractID" IS NULL AND fc."IDSystem" IS NULL)
					OR (trim(colldist."CollateralDistributionResult_FinancialContractUsedAsCollateral_FinancialContractID") = '' AND trim(colldist."CollateralDistributionResult_FinancialContractUsedAsCollateral_IDSystem") = '')
					)
			)
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_CollateralDistributionMethod_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS prio
			ON prio."CollateralDistributionMethod" = colldist."CollateralDistributionResult_CollateralDistributionMethod"
	
		WHERE root."FinancialContract_SimpleCollateralCategory" = 'Pledge'
			AND root."PledgedObject_PledgedObjectCategory" = 'PledgeOfFinancialContract'
		
		UNION ALL
		
		SELECT
			root."CollateralPortion_CollateralAgreement_FinancialContractID",
			root."CollateralPortion_CollateralAgreement_IDSystem",
			collport."CollateralPortionContractAssignment_PortionNumber",
			colldist."CollateralDistributionResult_AssignedCollateralAmount",
			colldist."CollateralDistributionResult_BusinessValidFrom",
			ROW_NUMBER() OVER(
				PARTITION BY
					root."CollateralPortion_CollateralAgreement_FinancialContractID",
					root."CollateralPortion_CollateralAgreement_IDSystem",
					collport."CollateralPortionContractAssignment_PortionNumber"
				ORDER BY
					prio."Priority" ASC
			) AS "Prio"
		FROM "com.adweko.adapter.osx.inputdata.collateral.collateralPledge::CollateralPledge_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Receivable"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) rece
			ON rece."Receivable_ReceivableID" = root."PledgedObject_PledgedReceivable_ReceivableID"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralPortionContractAssignment"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS collport
			ON collport."CollateralPortionContractAssignment_CollateralAgreement_FinancialContractID" = root."CollateralPortion_CollateralAgreement_FinancialContractID"
			AND collport."CollateralPortionContractAssignment_CollateralAgreement_IDSystem" = root."CollateralPortion_CollateralAgreement_IDSystem"
			AND collport."CollateralPortionContractAssignment_PortionNumber" = root."CollateralPortion_PortionNumber"
		
		INNER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralDistributionResult"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS colldist
			ON (
					colldist."CollateralDistributionResult_CollateralPortion_ASSOC_CollateralAgreement_FinancialContractID" = root."FinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_CollateralPortion_ASSOC_CollateralAgreement_IDSystem" = root."FinancialContract_IDSystem"
				AND colldist."CollateralDistributionResult_CollateralPortion_PortionNumber" = collport."CollateralPortionContractAssignment_PortionNumber"
				AND colldist."CollateralDistributionResult_FinancialContract_FinancialContractID" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_FinancialContract_IDSystem" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem"
				AND (colldist."CollateralDistributionResult_ReceivableUsedAsCollateral_ReceivableID" = rece."Receivable_ReceivableID"
					OR rece."Receivable_ReceivableID" IS NULL
					OR trim(colldist."CollateralDistributionResult_ReceivableUsedAsCollateral_ReceivableID") = ''
				)
			)
			OR
			(
					colldist."CollateralDistributionResult_SimpleCollateralAgreement_FinancialContractID" = root."FinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_SimpleCollateralAgreement_IDSystem" = root."FinancialContract_IDSystem"
				AND colldist."CollateralDistributionResult_FinancialContract_FinancialContractID" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_FinancialContract_IDSystem" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem"
				AND (colldist."CollateralDistributionResult_ReceivableUsedAsCollateral_ReceivableID" = rece."Receivable_ReceivableID"
					OR rece."Receivable_ReceivableID" IS NULL
					OR trim(colldist."CollateralDistributionResult_ReceivableUsedAsCollateral_ReceivableID") = ''
				)
			)
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_CollateralDistributionMethod_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS prio
			ON prio."CollateralDistributionMethod" = colldist."CollateralDistributionResult_CollateralDistributionMethod"
	
		WHERE root."FinancialContract_SimpleCollateralCategory" = 'Pledge'
			AND root."PledgedObject_PledgedObjectCategory" = 'PledgeOfReceivable'
		
		UNION ALL
		
		SELECT
			root."CollateralPortion_CollateralAgreement_FinancialContractID",
			root."CollateralPortion_CollateralAgreement_IDSystem",
			collport."CollateralPortionContractAssignment_PortionNumber",
			colldist."CollateralDistributionResult_AssignedCollateralAmount",
			colldist."CollateralDistributionResult_BusinessValidFrom",
			ROW_NUMBER() OVER(
				PARTITION BY
					root."CollateralPortion_CollateralAgreement_FinancialContractID",
					root."CollateralPortion_CollateralAgreement_IDSystem",
					collport."CollateralPortionContractAssignment_PortionNumber"
				ORDER BY
					prio."Priority" ASC
			) AS "Prio"
		FROM "com.adweko.adapter.osx.inputdata.collateral.collateralPledge::CollateralPledge_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralPortionContractAssignment"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS collport
			ON collport."CollateralPortionContractAssignment_CollateralAgreement_FinancialContractID" = root."CollateralPortion_CollateralAgreement_FinancialContractID"
			AND collport."CollateralPortionContractAssignment_CollateralAgreement_IDSystem" = root."CollateralPortion_CollateralAgreement_IDSystem"
			AND collport."CollateralPortionContractAssignment_PortionNumber" = root."CollateralPortion_PortionNumber"
		
		INNER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralDistributionResult"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS colldist
			ON (
					colldist."CollateralDistributionResult_CollateralPortion_ASSOC_CollateralAgreement_FinancialContractID" = root."FinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_CollateralPortion_ASSOC_CollateralAgreement_IDSystem" = root."FinancialContract_IDSystem"
				AND colldist."CollateralDistributionResult_CollateralPortion_PortionNumber" = collport."CollateralPortionContractAssignment_PortionNumber"
				AND colldist."CollateralDistributionResult_FinancialContract_FinancialContractID" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_FinancialContract_IDSystem" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem"
				AND (colldist."CollateralDistributionResult_FinancialContractUsedAsCollateral_FinancialContractID" = root."PledgedObject_ThirdPartyBankAccountID"
					OR root."PledgedObject_ThirdPartyBankAccountID" IS NULL
					OR trim(colldist."CollateralDistributionResult_FinancialContractUsedAsCollateral_FinancialContractID") = ''
				)
			)
			OR
			(
					colldist."CollateralDistributionResult_SimpleCollateralAgreement_FinancialContractID" = root."FinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_SimpleCollateralAgreement_IDSystem" = root."FinancialContract_IDSystem"
				AND colldist."CollateralDistributionResult_FinancialContract_FinancialContractID" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_FinancialContract_IDSystem" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem"
				AND (colldist."CollateralDistributionResult_FinancialContractUsedAsCollateral_FinancialContractID" = root."PledgedObject_ThirdPartyBankAccountID"
					OR root."PledgedObject_ThirdPartyBankAccountID" IS NULL
					OR trim(colldist."CollateralDistributionResult_FinancialContractUsedAsCollateral_FinancialContractID") = ''
				)
			)
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_CollateralDistributionMethod_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP) AS prio
			ON prio."CollateralDistributionMethod" = colldist."CollateralDistributionResult_CollateralDistributionMethod"
		
		WHERE root."FinancialContract_SimpleCollateralCategory" = 'Pledge'
			AND root."PledgedObject_PledgedObjectCategory" = 'PledgeOfAssetsHeldAtThirdParty'
		
		UNION ALL
		
		SELECT
			root."CollateralPortion_CollateralAgreement_FinancialContractID",
			root."CollateralPortion_CollateralAgreement_IDSystem",
			collport."CollateralPortionContractAssignment_PortionNumber",
			colldist."CollateralDistributionResult_AssignedCollateralAmount",
			colldist."CollateralDistributionResult_BusinessValidFrom",
			ROW_NUMBER() OVER(
				PARTITION BY
					root."CollateralPortion_CollateralAgreement_FinancialContractID",
					root."CollateralPortion_CollateralAgreement_IDSystem",
					collport."CollateralPortionContractAssignment_PortionNumber"
				ORDER BY
					prio."Priority" ASC
			) AS "Prio"
		FROM "com.adweko.adapter.osx.inputdata.collateral.collateralPledge::CollateralPledge_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralPortionContractAssignment"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS collport
			ON collport."CollateralPortionContractAssignment_CollateralAgreement_FinancialContractID" = root."CollateralPortion_CollateralAgreement_FinancialContractID"
			AND collport."CollateralPortionContractAssignment_CollateralAgreement_IDSystem" = root."CollateralPortion_CollateralAgreement_IDSystem"
			AND collport."CollateralPortionContractAssignment_PortionNumber" = root."CollateralPortion_PortionNumber"
		
		INNER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CollateralDistributionResult"( :I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  ) AS colldist
			ON (
					colldist."CollateralDistributionResult_CollateralPortion_ASSOC_CollateralAgreement_FinancialContractID" = root."FinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_CollateralPortion_ASSOC_CollateralAgreement_IDSystem" = root."FinancialContract_IDSystem"
				AND colldist."CollateralDistributionResult_CollateralPortion_PortionNumber" = collport."CollateralPortionContractAssignment_PortionNumber"
				AND (colldist."CollateralDistributionResult_FinancialContract_FinancialContractID" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"
						AND colldist."CollateralDistributionResult_FinancialContract_IDSystem" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem")
			)
			OR
			(
					colldist."CollateralDistributionResult_SimpleCollateralAgreement_FinancialContractID" = root."FinancialContract_FinancialContractID"
				AND colldist."CollateralDistributionResult_SimpleCollateralAgreement_IDSystem" = root."FinancialContract_IDSystem"
				AND (colldist."CollateralDistributionResult_FinancialContract_FinancialContractID" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_FinancialContractID"
						AND colldist."CollateralDistributionResult_FinancialContract_IDSystem" = collport."CollateralPortionContractAssignment_CollateralizedFinancialContract_IDSystem")
			)
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_CollateralDistributionMethod_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS prio
			ON prio."CollateralDistributionMethod" = colldist."CollateralDistributionResult_CollateralDistributionMethod"
		
		WHERE root."FinancialContract_SimpleCollateralCategory" = 'Pledge'
			AND root."PledgedObject_PledgedObjectCategory" = 'PledgeOfBusinessInterest'
	)
	WHERE "Prio" = 1;
END;