FUNCTION "com.adweko.adapter.osx.inputdata.collateral.collateralPledge::CollateralPledge_Contract" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4) ,
		"bookValueDate" DATE,
		"dealDate" DATE,
		"valueDate" DATE,
		"maturityDate" DATE,
		"counterparty" NVARCHAR(128),
		"marketValueDate" DATE,
		"marketValueObserved" DECIMAL(34,6),
		"currency" NVARCHAR(3),
		"contractRole" NVARCHAR(1),
		"optionHolder" NVARCHAR(1),
		"nominalValueOfGuarantee" DECIMAL(34, 6),
		"pledgedEndDate" DATE,
		"lmEntityName" NVARCHAR(128),
		"assetOwnerCode" NVARCHAR(255),
		"isUnderLiquidityManagementControl" INT,
		"dataGroup" NVARCHAR(128),
		"legalEntity" NVARCHAR(128),
		/*UDAs*/
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
		"commitmentType" NVARCHAR(75),
		"revaluationFrequency" NVARCHAR(255),
		"remarginingPeriod"  NVARCHAR(255),
		"portfolioType" NVARCHAR(255),
		"isSegregated" INT,
		"isOperationallyEligible" NVARCHAR(255),
		"UDA_PAY_FROM_GUARANTOR_CST" DECIMAL(15,11),
		"cadCrCollateralType" NVARCHAR(75),
		"bookValue" DECIMAL(34, 6),
		"exposureClass" NVARCHAR(255)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	RETURN

	SELECT 
		CAST("com.adweko.adapter.osx.inputdata.common::get_collateralPortionID"(
			i_FinancialContractID														=> root."FinancialContract_FinancialContractID",
			i_IDSystem																	=> IFNULL(root."FinancialContract_IDSystem",''),
			i_ObjectCategoryID															=> root."PledgedObjectCategoryID",
			i_CollateralPortionNumber													=> root."CollateralPortion_PortionNumber",
			i_PhysicalAsset_PhysicalAssetID												=> root."PledgedObject_PledgedPhysicalAsset_PhysicalAssetID",
			i_FinancialContract_FinancialContractID										=> root."PledgedObject_PledgedFinancialContract_FinancialContractID",
			i_Receivable_ReceivableID													=> root."PledgedObject_PledgedReceivable_ReceivableID",
			i_ThirdPartyBankAccountID													=> root."PledgedObject_ThirdPartyBankAccountID",
			i_BusinessInterest_TargetInBusinessPartnerRelation_BusinessPartnerID		=> root."PledgedObject_PledgedBusinessInterest_TargetInBusinessPartnerRelation_BusinessPartnerID",
			i_prefix																	=> NULL,
			i_hash																		=> true
		) AS NVARCHAR(32)) AS "id", 
		CAST("com.adweko.adapter.osx.inputdata.common::get_collateralPortionID"(
			i_FinancialContractID														=> root."FinancialContract_FinancialContractID",
			i_IDSystem																	=> IFNULL(root."FinancialContract_IDSystem",''),
			i_ObjectCategoryID															=> root."PledgedObjectCategoryID",
			i_CollateralPortionNumber													=> root."CollateralPortion_PortionNumber",
			i_PhysicalAsset_PhysicalAssetID												=> root."PledgedObject_PledgedPhysicalAsset_PhysicalAssetID",
			i_FinancialContract_FinancialContractID										=> root."PledgedObject_PledgedFinancialContract_FinancialContractID",
			i_Receivable_ReceivableID													=> root."PledgedObject_PledgedReceivable_ReceivableID",
			i_ThirdPartyBankAccountID													=> root."PledgedObject_ThirdPartyBankAccountID",
			i_BusinessInterest_TargetInBusinessPartnerRelation_BusinessPartnerID		=> root."PledgedObject_PledgedBusinessInterest_TargetInBusinessPartnerRelation_BusinessPartnerID",
			i_prefix																	=> 'C',
			i_hash																		=> false
		) AS NVARCHAR(256)) AS "sourceSystemRecordNumber",
		CAST("com.adweko.adapter.osx.inputdata.common::get_collateralPortionID"(
			i_FinancialContractID														=> root."FinancialContract_FinancialContractID",
			i_IDSystem																	=> NULL,
			i_ObjectCategoryID															=> root."PledgedObjectCategoryID",
			i_CollateralPortionNumber													=> root."CollateralPortion_PortionNumber",
			i_PhysicalAsset_PhysicalAssetID												=> root."PledgedObject_PledgedPhysicalAsset_PhysicalAssetID",
			i_FinancialContract_FinancialContractID										=> root."PledgedObject_PledgedFinancialContract_FinancialContractID",
			i_Receivable_ReceivableID													=> root."PledgedObject_PledgedReceivable_ReceivableID",
			i_ThirdPartyBankAccountID													=> root."PledgedObject_ThirdPartyBankAccountID",
			i_BusinessInterest_TargetInBusinessPartnerRelation_BusinessPartnerID		=> root."PledgedObject_PledgedBusinessInterest_TargetInBusinessPartnerRelation_BusinessPartnerID",
			i_prefix																	=> NULL,
			i_hash																		=> false
		) AS NVARCHAR(128)) AS "sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		'0001' AS "node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		root."FinancialContract_OriginalSigningDate" AS "dealDate",
		root."CollateralPortion_AgreedCoverageStartDate" AS "valueDate",
		root."CollateralPortion_AgreedCoverageEndDate" AS "maturityDate",
		CASE 
			WHEN
				root."CollateralProvider_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
				AND root."CollateralProvider_BusinessPartnerContractAssignment_ContractDataOwner" = false
					THEN root."CollateralProvider_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
			WHEN
				root."Pledger_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
				AND root."Pledger_BusinessPartnerContractAssignment_ContractDataOwner" = false
					THEN root."Pledger_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
			WHEN
				root."CollateralPortionProvider_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
				AND root."CollateralPortionProvider_BusinessPartnerContractAssignment_ContractDataOwner" = false
					THEN root."CollateralPortionProvider_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
		END AS "counterparty",
		CASE
			WHEN keyMarketValue."Value" = 'Valuation'
				THEN val."Valuation_ValuationDate"
			WHEN keyMarketValue."Value" = 'CollateralDistributionResult'
				THEN dist."CollateralDistributionResult_BusinessValidFrom"
		END AS "marketValueDate",
		CASE
			WHEN keyMarketValue."Value" = 'Valuation' THEN
				CASE 
					WHEN root."PledgedObject_Counter" = 1 THEN val."Valuation_EstimatedMarketValue"
					WHEN root."PledgedObject_Counter" > 1 THEN 
						MAP( kv_mode."Value",
							'1', TO_DECIMAL(root."PledgedObject_QuoteMedium"		* val."Valuation_EstimatedMarketValue", 34, 6),
							'2', TO_DECIMAL(root."PledgedObject_PledgedPercentage"	* val."Valuation_EstimatedMarketValue", 34, 6))
				END
			WHEN keyMarketValue."Value" = 'CollateralDistributionResult'
				THEN dist."CollateralDistributionResult_AssignedCollateralAmount"
		END AS "marketValueObserved",
		COALESCE(root."CollateralPortion_MaximumCollateralPortionAmountCurrency",root."FinancialContract_MaximumCollateralAmountCurrency") AS "currency",
		'2' AS "contractRole",
		'1' AS "optionHolder",
		CASE 
			WHEN root."PledgedObject_Counter" = 1 THEN root."CollateralPortion_MaximumCollateralPortionAmount"
			WHEN root."PledgedObject_Counter" > 1 THEN
				MAP( kv_mode."Value",
					'1', TO_DECIMAL(root."PledgedObject_QuoteMedium"		* root."CollateralPortion_MaximumCollateralPortionAmount", 34, 6),
					'2', TO_DECIMAL(root."PledgedObject_PledgedPercentage"	* root."CollateralPortion_MaximumCollateralPortionAmount", 34, 6))
		END AS "nominalValueOfGuarantee",
		root."CollateralPortion_AgreedCoverageEndDate" AS "pledgedEndDate",
		IFNULL("com.adweko.adapter.osx.inputdata.collateral.common::Collateral_get_lmEntityName" (
			i_ObjectCategory						=> root."PledgedObject_PledgedObjectCategory",
			i_PA_PhysicalAssetCategory				=> pledgedPA."PhysicalAsset_PhysicalAssetCategory",
			i_FC_FinancialContractCategory			=> pledgedFC."FinancialContractCategory",
			i_FC_OTCDerivativeContractCategory		=> pledgedFC."OTCDerivativeContractCategory",
			i_FC_SwapCategory						=> pledgedFC."SwapCategory",
			i_FC_SingleCurrencyAccountCategory		=> pledgedFC."SingleCurrencyAccountCategory"
		),'IntangibleAsset') AS "lmEntityName",
		'CLIENT' AS "assetOwnerCode",
		1 AS "isUnderLiquidityManagementControl",
		root."dataGroup" AS "dataGroup",
		CASE 
			WHEN
				root."CollateralReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
				AND root."CollateralReceiver_BusinessPartnerContractAssignment_ContractDataOwner" = true
					THEN root."CollateralReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
			WHEN
				root."Pledgee_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
				AND root."Pledgee_BusinessPartnerContractAssignment_ContractDataOwner" = true
					THEN root."Pledgee_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
			WHEN
				root."CollateralPortionReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
				AND root."CollateralPortionReceiver_BusinessPartnerContractAssignment_ContractDataOwner" = true
					THEN root."CollateralPortionReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
		END AS "legalEntity",
		pc."StandardCatalog_ProductCatalogItem",
		pc."CustomCatalog_ProductCatalogItem",
		'4' AS "commitmentType",
		CAST(ABS(days_between(val."Valuation_ScheduledNextValuationDate",val."Valuation_ValuationDate")) AS NVARCHAR(255)) AS "revaluationFrequency",
		CAST(CASE
			WHEN
				(root."CollateralReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
				AND root."CollateralReceiver_BusinessPartnerContractAssignment_ContractDataOwner" = true
				AND root."CollateralReceiver_BusinessPartnerContractAssignment_MarginPeriodTimeUnit" = 'Day')
					THEN root."CollateralReceiver_BusinessPartnerContractAssignment_MarginPeriod"
			WHEN
				(root."CollateralReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
				AND root."CollateralReceiver_BusinessPartnerContractAssignment_ContractDataOwner" = true
				AND root."CollateralReceiver_BusinessPartnerContractAssignment_MarginPeriodTimeUnit" = 'Week')
					THEN root."CollateralReceiver_BusinessPartnerContractAssignment_MarginPeriod"*7
			WHEN
				(root."CollateralReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
				AND root."CollateralReceiver_BusinessPartnerContractAssignment_ContractDataOwner" = true
				AND root."CollateralReceiver_BusinessPartnerContractAssignment_MarginPeriodTimeUnit" = 'Month')
					THEN root."CollateralReceiver_BusinessPartnerContractAssignment_MarginPeriod"*30
			WHEN
				(root."CollateralReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
				AND root."CollateralReceiver_BusinessPartnerContractAssignment_ContractDataOwner" = true
				AND root."CollateralReceiver_BusinessPartnerContractAssignment_MarginPeriodTimeUnit" = 'Quarter')
					THEN root."CollateralReceiver_BusinessPartnerContractAssignment_MarginPeriod"*7
			WHEN
				(root."CollateralReceiver_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" IS NOT NULL
				AND root."CollateralReceiver_BusinessPartnerContractAssignment_ContractDataOwner" = true
				AND root."CollateralReceiver_BusinessPartnerContractAssignment_MarginPeriodTimeUnit" = 'Year')
					THEN root."CollateralReceiver_BusinessPartnerContractAssignment_MarginPeriod"*360
				ELSE NULL
		END AS NVARCHAR(255)) AS "remarginingPeriod",
        portfolioType."portfolioType",
		CASE
			WHEN cccr."CounterpartyCreditRiskCollateralResults_MarginedNettingSet" = TRUE AND cccr."CounterpartyCreditRiskCollateralResults_SegregatedAccount" = TRUE
				THEN 1
			WHEN cccr."CounterpartyCreditRiskCollateralResults_MarginedNettingSet" = TRUE AND cccr."CounterpartyCreditRiskCollateralResults_SegregatedAccount" = FALSE
				THEN 0
			WHEN cccr."CounterpartyCreditRiskCollateralResults_MarginedNettingSet" = FALSE AND cccr."CounterpartyCreditRiskCollateralResults_SegregatedAccount" = TRUE
				THEN 1
			WHEN cccr."CounterpartyCreditRiskCollateralResults_MarginedNettingSet" = FALSE AND cccr."CounterpartyCreditRiskCollateralResults_SegregatedAccount" = FALSE
				THEN 0
		END AS "isSegregated",
		CASE
			WHEN cccr."CounterpartyCreditRiskCollateralResults_MarginedNettingSet" = TRUE AND cccr."CounterpartyCreditRiskCollateralResults_Eligible" = TRUE
				THEN 'TRUE'
			WHEN cccr."CounterpartyCreditRiskCollateralResults_MarginedNettingSet" = TRUE AND cccr."CounterpartyCreditRiskCollateralResults_Eligible" = FALSE
				THEN 'FALSE'
			WHEN cccr."CounterpartyCreditRiskCollateralResults_MarginedNettingSet" = FALSE AND cccr."CounterpartyCreditRiskCollateralResults_Eligible" = TRUE
				THEN 'TRUE'
			WHEN cccr."CounterpartyCreditRiskCollateralResults_MarginedNettingSet" = FALSE AND cccr."CounterpartyCreditRiskCollateralResults_Eligible" = FALSE
				THEN 'FALSE'
		END AS "isOperationallyEligible",
     	CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (root."CollateralPortion_CoveredPercentageOfObligations", 'CollateralPortion_CoveredPercentageOfObligations', kv_mode_perc."Value") AS DECIMAL(15,11)) AS "UDA_PAY_FROM_GUARANTOR_CST",
     	cre."CreditRiskExposure_CreditRiskExposureCollateralType" AS "cadCrCollateralType",
		bok."BookValue_BookValue" AS "bookValue",
		cre."exposureClass"

	FROM "com.adweko.adapter.osx.inputdata.collateral.collateralPledge::CollateralPledge_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.collateral.common::Collateral_get_PortionValuation"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS val
		ON val."Valuation_CollateralPortion_PortionNumber"								= root."CollateralPortion_PortionNumber"
		AND val."Valuation_CollateralPortion_CollateralAgreement_FinancialContractID"	= root."FinancialContract_FinancialContractID"
		AND val."Valuation_CollateralPortion_CollateralAgreement_IDSystem"				= root."FinancialContract_IDSystem"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS kv_mode
		ON kv_mode."KeyID" = 'CollateralPledgeMode'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS kv_mode_perc
		ON kv_mode_perc."KeyID" = 'SAPPercentageStandard'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_PhysicalAsset"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pledgedPA
		ON root."PledgedObject_PledgedObjectCategory" = 'PledgeOfPhysicalAsset'
		AND pledgedPA."PhysicalAsset_PhysicalAssetID" = root."PledgedObject_PledgedPhysicalAsset_PhysicalAssetID"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_FinancialContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pledgedFC
		ON root."PledgedObject_PledgedObjectCategory" = 'PledgeOfFinancialContract'
		AND pledgedFC."FinancialContractID"	= root."PledgedObject_PledgedFinancialContract_FinancialContractID"
		AND pledgedFC."IDSystem"			= root."PledgedObject_PledgedFinancialContract_IDSystem"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalog"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) pc
		ON pc."IDSystem"				= root."FinancialContract_IDSystem"
		AND pc."FinancialContractID"	= root."FinancialContract_FinancialContractID"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.collateral.collateralPledge::CollateralPledge_get_PrioDistMethod"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING ) dist
		ON dist."CollateralPortion_CollateralAgreement_IDSystem"				= root."FinancialContract_IDSystem"
		AND dist."CollateralPortion_CollateralAgreement_FinancialContractID"	= root."FinancialContract_FinancialContractID"
		AND dist."CollateralPortionContractAssignment_PortionNumber"			= root."CollateralPortion_PortionNumber"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.collateral::BV_CounterpartyCreditRiskCollateralResultsPrio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP  ) AS cccr
		ON  cccr."CounterpartyCreditRiskCollateralResults_CollateralPortion.ASSOC_CollateralAgreement_FinancialContractID"	= root."FinancialContract_FinancialContractID"
		AND cccr."CounterpartyCreditRiskCollateralResults_CollateralPortion.ASSOC_CollateralAgreement_IDSystem"	 			= root."FinancialContract_IDSystem"
		AND cccr."CounterpartyCreditRiskCollateralResults_CollateralPortion_PortionNumber"									= root."CollateralPortion_PortionNumber"

   	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Collateral') AS cre
		ON cre."CreditRiskExposure_CollateralPortion_CollateralAgreement_FinancialContractID"	= root."CollateralPortion_CollateralAgreement_FinancialContractID"
		AND cre."CreditRiskExposure_FinancialContract_IDSystem" 								= root."CollateralPortion_CollateralAgreement_IDSystem"
		AND cre."CreditRiskExposure_CollateralPortion_PortionNumber"							= root."CollateralPortion_PortionNumber"

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS keyMarketValue
		ON keyMarketValue."KeyID" = 'marketValueSwitch'	

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_portfolioType" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS portfolioType
		ON  portfolioType."ASSOC_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND portfolioType."ASSOC_FinancialContract_IDSystem"            = root."FinancialContract_IDSystem"

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
		ON bok."BookValue_FinancialContract_IDSystem"					= root."FinancialContract_IDSystem"
		AND bok."BookValue_FinancialContract_FinancialContractID"		= root."FinancialContract_FinancialContractID"
		AND bok."BookValue_BookValueCurrency"							= COALESCE(root."CollateralPortion_MaximumCollateralPortionAmountCurrency",root."FinancialContract_MaximumCollateralAmountCurrency")
	;
END;