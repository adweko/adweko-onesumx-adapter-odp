FUNCTION "com.adweko.adapter.osx.inputdata.leaseAgreement::LeaseAgreementCollateral_Contract"(
       	IN I_BUSINESS_DATE DATE,
    	IN I_SYSTEM_TIMESTAMP TIMESTAMP,
    	IN I_DATAGROUP_STRING NVARCHAR(128)
    	)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"isUnderLiquidityManagementControl" INT,
		"dataGroup" NVARCHAR(128),
		"lmEntityName" NVARCHAR(128),
		"assetOwnerCode" NVARCHAR(255),
		"contractRole" NVARCHAR(1),
		"optionHolder" NVARCHAR(1),
		"linkID" NVARCHAR(256),
		"linkType" NVARCHAR(3),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"legalEntity" NVARCHAR(128),
		"counterparty" NVARCHAR(128),
		"marketValueObserved" DECIMAL (34, 6),
		"marketValueDate" DATE,
		"writeOffAmount" DECIMAL(34, 6),
		"exposureClass" NVARCHAR(255)
		
       ) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN

	SELECT
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID_Collateral",
			root."node_No",
			root."dataGroup"
		) AS "id",
		root."contractID_Collateral" AS "sourceSystemRecordNumber",
		root."FinancialContract_FinancialContractID" || '#' || root."PhysicalAsset_PhysicalAssetID" AS "sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		root."node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		root."FinancialContract_OriginalSigningDate" AS "dealDate",
		1 AS "isUnderLiquidityManagementControl",
		root."dataGroup",
		'PhysicalAsset' AS "lmEntityName",
		'CLIENT' AS "assetOwnerCode",
		'2' AS "contractRole",
		'1' AS "optionHolder",
		root."FinancialContract_FinancialContractID" AS "linkID",
		'2' AS "linkType", 
		root."CustomCatalog_ProductCatalogItem",
		root."StandardCatalog_ProductCatalogItem",
		MAP(root."legalEntitySwitch",
			'OrganizationalUnitContractAssignment', root."OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",
			'BusinessPartnerContractAssignment', CASE
													WHEN root."BusinessPartnerContractAssignment_Lessor_ContractDataOwner" = true
														THEN root."BusinessPartnerContractAssignment_Lessor_BusinessPartnerID"
													WHEN root."BusinessPartnerContractAssignment_Lessee_ContractDataOwner" = true
														THEN root."BusinessPartnerContractAssignment_Lessee_BusinessPartnerID"
												 END
		) AS "legalEntity", 
		
		CASE
			WHEN root."BusinessPartnerContractAssignment_Lessor_ContractDataOwner" = false
				THEN root."BusinessPartnerContractAssignment_Lessor_BusinessPartnerID"
			WHEN root."BusinessPartnerContractAssignment_Lessee_ContractDataOwner" = false
				THEN root."BusinessPartnerContractAssignment_Lessee_BusinessPartnerID"
		END AS "counterparty",
		ABS(marketValue."Valuation_BaseValue")*root."isActiveSign" AS "marketValueObserved",
		marketValue."Valuation_ValuationDate" AS "marketValueDate",
		MAP(writeOffKey."Value",
			'WriteDownAmountInPositionCurrency', writeOff."WriteDownAmountInPositionCurrency",
			'WriteDownAmount', writeOff."WriteDownAmount"
		) AS "writeOffAmount",
		expo."exposureClass"
		
		FROM "com.adweko.adapter.osx.inputdata.leaseAgreement::LeaseAgreement_Root_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.leaseAgreement::LeaseAgreementCollateral_MarketValue_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS marketValue
			ON marketValue."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"

		--writeOffAmount
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS writeOffKey
			ON writeOffKey."KeyID" = 'WriteDownPositionCurrenyOrAmount'

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityWriteOffFC"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Loan') AS writeOff
			ON writeOff."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
			AND writeOff."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP,'LeaseAgreementCollateral') AS expo
			ON expo."CreditRiskExposure_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
			AND expo."CreditRiskExposure_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"

;
END;