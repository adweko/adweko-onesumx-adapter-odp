FUNCTION "com.adweko.adapter.osx.inputdata.common::get_PriorityWriteOffFC"( IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP, I_STREAM_NAME NVARCHAR(40))
       RETURNS TABLE(
		"WriteDownSubtype"															NVARCHAR(100),
		"WriteDownType" 															NVARCHAR(40),
		"AccountingSystem_AccountingSystemID" 										NVARCHAR(128),
		"FinancialContract_FinancialContractID"										NVARCHAR(128),
		"FinancialContract_IDSystem"												NVARCHAR(40),
		"FinancialInstrument"														NVARCHAR(128),
		"BusinessValidFrom" 														DATE,
		"BusinessValidTo"															DATE,
		"SystemValidFrom"															TIMESTAMP,
		"SystemValidTo" 															TIMESTAMP,
		"PositionCurrency"															NVARCHAR(3),
		"WriteDownAmount"															DECIMAL(34,6),
		"WriteDownAmountInPositionCurrency" 										DECIMAL(34,6)
       )
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN
	SELECT
		"WriteDownSubtype",
		"WriteDownType",
		"AccountingSystem_AccountingSystemID",
		"FinancialContract_FinancialContractID",
		"FinancialContract_IDSystem",
		"FinancialInstrument",
		"BusinessValidFrom",
		"BusinessValidTo",
		"SystemValidFrom",
		"SystemValidTo",
		"PositionCurrency",
		"WriteDownAmount",
		"WriteDownAmountInPositionCurrency"
		
	FROM
		(
			SELECT
			"WriteDownAmount_WriteDownSubtype"									AS "WriteDownSubtype",
			"WriteDownAmount_WriteDownType" 									AS "WriteDownType",
			"WriteDownAmount_AccountingSystem_AccountingSystemID" 				AS "AccountingSystem_AccountingSystemID",
			"WriteDownAmount_FinancialContract_FinancialContractID"				AS "FinancialContract_FinancialContractID",
			"WriteDownAmount_FinancialContract_IDSystem"						AS "FinancialContract_IDSystem",
			"WriteDownAmount_FinancialInstrument"								AS "FinancialInstrument",
			"WriteDownAmount_BusinessValidFrom" 								AS "BusinessValidFrom",
			"WriteDownAmount_BusinessValidTo"									AS "BusinessValidTo",
			"WriteDownAmount_SystemValidFrom"									AS "SystemValidFrom",
			"WriteDownAmount_SystemValidTo" 									AS "SystemValidTo",
			"WriteDownAmount_PositionCurrency"									AS "PositionCurrency",
			"WriteDownAmount_WriteDownAmount"									AS "WriteDownAmount",
			"WriteDownAmount_WriteDownAmountInPositionCurrency" 				AS "WriteDownAmountInPositionCurrency",
			ROW_NUMBER() OVER (PARTITION BY "WriteDownAmount_FinancialContract_FinancialContractID",
											"WriteDownAmount_FinancialContract_IDSystem"
							ORDER BY map_wda."Priority"
						) AS "Prio"
						
			FROM "com.adweko.adapter.osx.inputdata.risk::BV_WriteDownAmount"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS wda 
			INNER JOIN "com.adweko.adapter.osx.mappings::map_WriteOff" AS map_wda
				 ON TRIM(map_wda."ASSOC_AccountingSystem")		= TRIM(wda."WriteDownAmount_AccountingSystem_AccountingSystemID")
				AND TRIM(map_wda."WriteDownType")				= TRIM(wda."WriteDownAmount_WriteDownType")
				AND TRIM(map_wda."WriteDownSubtype")			= TRIM(wda."WriteDownAmount_WriteDownSubtype")
				AND (map_wda."StreamName"						= :I_STREAM_NAME
					OR TRIM(map_wda."StreamName")				= '')
		)
			WHERE "Prio"=1;
END;