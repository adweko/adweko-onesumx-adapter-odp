FUNCTION "com.adweko.adapter.osx.inputdata.common::get_ebaLcrContractEligibility" (
		i_ProductCatalogItemCode				NVARCHAR(128) DEFAULT NULL, 
		i_LiquidityClassificationOfAsset		NVARCHAR(40) DEFAULT NULL,
		i_CountryEU 							BOOLEAN DEFAULT NULL,
		i_CountryNonEU							BOOLEAN DEFAULT NULL,
		i_SecurityCategory						NVARCHAR(40) DEFAULT NULL,
		i_TreatedAsUCITS						BOOLEAN DEFAULT NULL,
		i_FinancialContractCategory 			NVARCHAR(50) DEFAULT NULL,
		i_DenominationCurrency					NVARCHAR(3) DEFAULT NULL,
		i_CurrencyInCountry 					NVARCHAR(3) DEFAULT NULL
)

RETURNS e_ebaLcrContractEligibility NVARCHAR(255) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN

	e_ebaLcrContractEligibility = 'NOT_ELIGIBLE';

	IF	i_ProductCatalogItemCode IN (
			'AssetBackedSecurities',
			'ABCPProgram',
			'MortgageBackedSecurities',
			'ResidentialMortgageBackedSecurities',
			'CommercialMortgageBackedSecurities' )
		
		THEN e_ebaLcrContractEligibility = 'ELIGIBLE_ABS';
	
	ELSEIF i_ProductCatalogItemCode = 'CoveredBond'
		
		THEN e_ebaLcrContractEligibility =
			MAP(i_LiquidityClassificationOfAsset,
				'ExtremlyHighLiquid', 'ELIGIBLE_CB_EHQ_1',
				'HighLiquidLevelA', CASE 
					WHEN i_CountryEU = true 	THEN 'ELIGIBLE_CB_HQ_2A'
					WHEN i_CountryNonEU = true	THEN 'ELIGIBLE_CB_HQ_2A_3RDCOUNTRY' END,
				'HighLiquidLevelB', 'ELIGIBLE_CB_HQ_2B'
				
			);
	
	ELSEIF i_TreatedAsUCITS = true 
		THEN e_ebaLcrContractEligibility = 'ELIGIBLE_CIU';
	
	ELSEIF i_SecurityCategory = 'EquityInstrument'
		THEN 
			IF	i_DenominationCurrency = i_CurrencyInCountry 
				OR i_LiquidityClassificationOfAsset = 'HighLiquidLevelB'
					
				THEN e_ebaLcrContractEligibility = 'ELIGIBLE_EQU'; 
			END IF;
	
	ELSEIF i_FinancialContractCategory = 'Facility'
		AND i_LiquidityClassificationOfAsset = 'HighLiquidLevelB' 
		
			THEN e_ebaLcrContractEligibility = 'ELIGIBLE_FAC';	
	END IF;

END;
