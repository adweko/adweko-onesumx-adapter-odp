FUNCTION "com.adweko.adapter.osx.inputdata.common::get_sectorCode"(
	IN i_IndustryClassificationSystem_NACE	NVARCHAR(128),
	IN i_Industry_NACE						NVARCHAR(40),
	IN i_OrganizationCategory 				NVARCHAR(40),
	IN i_LegalEntityUnderPublicLawType		NVARCHAR(256),
	IN i_InternationalOrganizationCode		NVARCHAR(60),
	IN i_GovernmentBacked					BOOLEAN
	)
    RETURNS sectorCode NVARCHAR(40) 
    LANGUAGE SQLSCRIPT 
    SQL SECURITY INVOKER AS 
BEGIN 
    /****************************
        Write your function logic
    ****************************/
    
    sectorCode = 
    		CASE
    			WHEN :i_OrganizationCategory = 'LegalEntityUnderPublicLaw' AND :i_LegalEntityUnderPublicLawType = 'SovereignState'
    				THEN '1001' --Sovereigns
    			WHEN :i_InternationalOrganizationCode IN ('1L', '4C', '5C', '5D', '5E', '5F', '5H', '5J', '5K', '5L', '5N', '5O', '5T', '5W', '5X', '6A5', '6A8', '7B', '7C', '7E', '7G', '7L', '7N', '7O')
    				THEN '1003' --Multilateral development banks
    			WHEN :i_Industry_NACE IS NULL AND :i_LegalEntityUnderPublicLawType = 'LegalEntityUnderNationalPublicLaw'
    				THEN '1004' --Local government 
    			WHEN :i_IndustryClassificationSystem_NACE = 'NACE' THEN
    				CASE
						WHEN LEFT(:i_Industry_NACE, 2) IN ('64', '65', '66') THEN
							CASE
    							WHEN :i_OrganizationCategory = 'Company' AND :i_GovernmentBacked = true
    								THEN '1009' --Government-backed financials
    							WHEN :i_Industry_NACE = '64.11'
    								THEN '1002' --Central banks
    							ELSE '1008' --Financials
    						END
						WHEN LEFT(:i_Industry_NACE, 2) IN ('10', '11', '12', '13', '14', '15', '31')
    						OR LEFT(:i_Industry_NACE, 4) IN ('20.4', '26.4', '26.5', '26.6', '26.7', '26.8')
    						OR :i_Industry_NACE IN ('17.22', '17.23', '17.24', '17.29')
    						THEN '1017' --Consumer goods and services
						WHEN LEFT(:i_Industry_NACE, 2) IN ('18', '19', '21', '22', '23', '25', '27', '28', '29', '30', '32', '33')
    						OR LEFT(:i_Industry_NACE, 4) IN ('17.1', '20.1', '20.2', '20.3', '20.5', '20.6', '26.1', '26.2', '26.3')
    						OR :i_Industry_NACE = '17.21'
    						THEN '1014' --Manufacturing
    					WHEN LEFT(:i_Industry_NACE, 2) IN ('05', '06', '07', '08.1', '08.9', '09')
    						OR LEFT(:i_Industry_NACE, 4) IN ('08.1', '08.9')
    						THEN '1015' --Mining and Quarrying
    					WHEN LEFT(:i_Industry_NACE, 2) = '84'
    						THEN '1007' --Public administration
    					WHEN LEFT(:i_Industry_NACE, 2) = '24'
    						THEN '1010' --Basic materials
    					WHEN LEFT(:i_Industry_NACE, 2) = '35'
    						THEN '1011' --Energy
    					WHEN LEFT(:i_Industry_NACE, 2) IN ('01', '02', '03')
    						THEN '1013' --Agriculture
    					WHEN LEFT(:i_Industry_NACE, 2) IN ('49', '50', '51', '52', '53')
    						THEN '1018' --Transportation and storage
    					WHEN LEFT(:i_Industry_NACE, 2) IN ('77', '78', '79', '80', '81')
    						THEN '1019' --Administrative and support activities
    					WHEN LEFT(:i_Industry_NACE, 2) IN ('62', '63')
    						THEN '1020' --Technology
    					WHEN LEFT(:i_Industry_NACE, 2) = '61'
    						THEN '1021' --Telecommunications
    					WHEN LEFT(:i_Industry_NACE, 2) IN ('86', '87', '88')
    						THEN '1022' --Health care
    					WHEN LEFT(:i_Industry_NACE, 2) IN ('36', '37', '38', '39')
    						THEN '1023' --Utilities
    					WHEN LEFT(:i_Industry_NACE, 2) IN ('69', '70', '71', '72', '73', '74', '75')
    						THEN '1024' --Professional and technical activities
    					WHEN LEFT(:i_Industry_NACE, 2) = '68'
    						THEN '1025' --Real estate activities
    					WHEN LEFT(:i_Industry_NACE, 2) = '85' THEN
    						CASE
    							WHEN :i_OrganizationCategory = 'LegalEntityUnderPublicLaw' AND :i_LegalEntityUnderPublicLawType IN ('LegalEntityUnderPublicLaw', 'LegalEntityUnderInternationalLaw') 
    								THEN '1006' --Public education
    							WHEN :i_OrganizationCategory = 'Company'
    								THEN '1027' --Private Education
    							ELSE
    								'9999'
    						END
						WHEN :i_OrganizationCategory = 'LegalEntityUnderPublicLaw' AND LEFT(:i_Industry_NACE, 2) <> '85' 
							AND :i_Industry_NACE IS NOT NULL AND :i_LegalEntityUnderPublicLawType = 'LegalEntityUnderNationalPublicLaw'
    						THEN '1026' --Other public sector entities
    					WHEN LEFT(:i_Industry_NACE, 2) NOT IN ('64', '65', '66') THEN
							CASE 
								WHEN :i_OrganizationCategory = 'Company' AND :i_GovernmentBacked = true
									THEN '1005' --Government-backed non-financials
								ELSE '9999'
							END
						ELSE '9999'
    				END
    		END;
END;