FUNCTION "com.adweko.adapter.osx.inputdata.common::get_PriorityMargin"(IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP, StreamName NVARCHAR(40))
    	RETURNS TABLE (
    		"Margin_MarginCategory" NVARCHAR(100),
			"Margin_RoleOfCurrency" NVARCHAR(40),
			"Margin_Scenario" NVARCHAR(100),
			"Margin_SplittingTranche" NVARCHAR(10),
			"Margin_BusinessPartner_BusinessPartnerID" NVARCHAR(128),
			"Margin_FinancialContract_FinancialContractID" NVARCHAR(128),
			"Margin_FinancialContract_IDSystem" NVARCHAR(40),
			"Margin_FinancialInstrument_FinancialInstrumentID" NVARCHAR(128),
			"Margin_ResultGroup_ResultGroupID" NVARCHAR(128),
			"Margin_RiskReportingNode_RiskReportingNodeID" NVARCHAR(128),
			"Margin_SecuritiesAccount_FinancialContractID" NVARCHAR(128),
			"Margin_SecuritiesAccount_IDSystem" NVARCHAR(40),
			"Margin_TimeBucket_TimeBucketID" NVARCHAR(128),
			"Margin_BusinessValidFrom" DATE,
			"Margin_BusinessValidTo" DATE,
			"Margin_SystemValidFrom" TIMESTAMP,
			"Margin_SystemValidTo" TIMESTAMP,
			"Margin_MarginAmount"  DECIMAL(34, 6),
			"Margin_MarginAmountCurrency" NVARCHAR(3),
			"Margin_MarginRate" DECIMAL(15, 11),
			"MarginComponent_MarginComponentAmount" DECIMAL(34, 6),
			"MarginComponent_MarginComponentRate" DECIMAL(15, 11)
    	)
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN

	SELECT
	
		"Margin_MarginCategory",
		"Margin_RoleOfCurrency",
		"Margin_Scenario",
		"Margin_SplittingTranche",
		"Margin_BusinessPartner_BusinessPartnerID",
		"Margin_FinancialContract_FinancialContractID",
		"Margin_FinancialContract_IDSystem",
		"Margin_FinancialInstrument_FinancialInstrumentID",
		"Margin_ResultGroup_ResultGroupID",
		"Margin_RiskReportingNode_RiskReportingNodeID",
		"Margin_SecuritiesAccount_FinancialContractID",
		"Margin_SecuritiesAccount_IDSystem",
		"Margin_TimeBucket_TimeBucketID",
		"Margin_BusinessValidFrom",
		"Margin_BusinessValidTo",
		"Margin_SystemValidFrom",
		"Margin_SystemValidTo",
		"Margin_MarginAmount",
		"Margin_MarginAmountCurrency",
		"Margin_MarginRate",
		
		"MarginComponent_MarginComponentAmount",
		"MarginComponent_MarginComponentRate"
	
	FROM (
		
		SELECT
			margin."MarginCategory" AS "Margin_MarginCategory",
			margin."RoleOfCurrency" AS "Margin_RoleOfCurrency",
			margin."Scenario" AS "Margin_Scenario",
			margin."SplittingTranche" AS "Margin_SplittingTranche",
			margin."_BusinessPartner_BusinessPartnerID" AS "Margin_BusinessPartner_BusinessPartnerID",
			margin."_FinancialContract_FinancialContractID" AS "Margin_FinancialContract_FinancialContractID",
			margin."_FinancialContract_IDSystem" AS "Margin_FinancialContract_IDSystem",
			margin."_FinancialInstrument_FinancialInstrumentID" AS "Margin_FinancialInstrument_FinancialInstrumentID",
			margin."_ResultGroup_ResultGroupID" AS "Margin_ResultGroup_ResultGroupID",
			margin."_RiskReportingNode_RiskReportingNodeID" AS "Margin_RiskReportingNode_RiskReportingNodeID",
			margin."_SecuritiesAccount_FinancialContractID" AS "Margin_SecuritiesAccount_FinancialContractID",
			margin."_SecuritiesAccount_IDSystem" AS "Margin_SecuritiesAccount_IDSystem",
			margin."_TimeBucket_TimeBucketID" AS "Margin_TimeBucket_TimeBucketID",
			margin."BusinessValidFrom" AS "Margin_BusinessValidFrom",
			margin."BusinessValidTo" AS "Margin_BusinessValidTo",
			margin."SystemValidFrom" AS "Margin_SystemValidFrom",
			margin."SystemValidTo" AS "Margin_SystemValidTo",
			margin."MarginAmount" AS "Margin_MarginAmount",
			margin."MarginAmountCurrency" AS "Margin_MarginAmountCurrency",
			margin."MarginRate" AS "Margin_MarginRate",
			
			marginComp."MarginComponentAmount" AS "MarginComponent_MarginComponentAmount",
			marginComp."MarginComponentRate" AS "MarginComponent_MarginComponentRate",
			
			ROW_NUMBER() OVER (PARTITION BY 	margin."_FinancialContract_FinancialContractID",
												margin."_FinancialContract_IDSystem"
									ORDER BY	mappingMargin."Priority"
			) AS "Prio"
		FROM "com.adweko.adapter.osx.synonyms::Margin_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS margin
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::MarginComponent_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS marginComp
			 ON marginComp."_Margin__FinancialContract_FinancialContractID" = margin."_FinancialContract_FinancialContractID"
			AND marginComp."_Margin__FinancialContract_IDSystem" = margin."_FinancialContract_IDSystem"
		
		INNER JOIN "com.adweko.adapter.osx.mappings::map_margin_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mappingMargin
			 ON mappingMargin."StreamName" = :streamName
			AND (mappingMargin."MarginCategory" = margin."MarginCategory" OR mappingMargin."MarginCategory" IS NULL)
			AND (mappingMargin."RoleOfCurrency" = margin."RoleOfCurrency" OR mappingMargin."RoleOfCurrency" IS NULL)
			AND (mappingMargin."Scenario" = margin."Scenario" OR mappingMargin."Scenario" IS NULL)
			AND (mappingMargin."SplittingTranche" = margin."SplittingTranche" OR mappingMargin."SplittingTranche" IS NULL)
			AND (mappingMargin."_ResultGroup" = margin."_ResultGroup_ResultGroupID" OR mappingMargin."_ResultGroup" IS NULL)
			AND (mappingMargin."_RiskReportingNode" = margin."_RiskReportingNode_RiskReportingNodeID" OR mappingMargin."_RiskReportingNode" IS NULL)
			AND (mappingMargin."_TimeBucket" = margin."_TimeBucket_TimeBucketID" OR mappingMargin."_TimeBucket" IS NULL)
			AND (mappingMargin."MarginComponentType" = marginComp."MarginComponentType" OR mappingMargin."MarginComponentType" IS NULL OR marginComp."MarginComponentType" IS NULL)
	) WHERE "Prio" = 1
	;
END;