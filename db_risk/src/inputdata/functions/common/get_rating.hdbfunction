FUNCTION "com.adweko.adapter.osx.inputdata.common::get_rating" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_STREAM_NAME NVARCHAR(128)
		)
RETURNS TABLE (
		"Rating_BusinessPartner_BusinessPartnerID" NVARCHAR(128),
		"Rating_FinancialContract_IDSystem" NVARCHAR(40),
		"Rating_FinancialContract_FinancialContractID" NVARCHAR(128),
		"Rating_Security_FinancialInstrumentID" NVARCHAR(128),
		"Rating_BusinessValidFrom" DATE,
		"rating_id" NVARCHAR(32),
		"ratingScale" NVARCHAR(75),
		"ratingClass" NVARCHAR(10)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*************************************************
	get rating for contract instrument or business partner
	**************************************************/
	RETURN

	SELECT DISTINCT "Rating_BusinessPartner_BusinessPartnerID",
		"Rating_FinancialContract_IDSystem",
		"Rating_FinancialContract_FinancialContractID",
		"Rating_Security_FinancialInstrumentID",
		"Rating_BusinessValidFrom",
			cast(HASH_MD5(to_binary(
				COALESCE("Rating_BusinessPartner_BusinessPartnerID",'') || 
				COALESCE("Rating_FinancialContract_IDSystem",'') ||
				COALESCE("Rating_FinancialContract_FinancialContractID",'') ||
				COALESCE("Rating_Security_FinancialInstrumentID",'') ||
				"Rating_BusinessValidFrom" ||
				COALESCE("Rating_RatingAgency",'') ||
				COALESCE("Rating_TimeHorizon",'') ||
				COALESCE("Rating_RatingMethod",'') ||
				COALESCE("Rating_RatingStatus",'')||
				COALESCE("Rating_IsFxRating",'')
			)) as NVARCHAR(32))
			AS "rating_id",
		rat_map."OSXScaleName" AS "ratingScale",
		rat."Rating_Rating" AS "ratingClass"
	FROM "com.adweko.adapter.osx.inputdata.risk::BV_Rating"(
			:I_BUSINESS_DATE,
			:I_SYSTEM_TIMESTAMP
			)  AS rat
			
	/*Join to mapping table in order to derive the scaleName for OSX
	In theory all fields for join criteria below are mandatory key fields in both tables that have a default set to ''
	and shoudn't be NULL
	Inner Join is required, because a rating without scale name is useless*/
	INNER JOIN "com.adweko.adapter.osx.mappings::map_rating_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS rat_map 
	ON (rat."Rating_RatingAgency" = rat_map."RatingAgency" OR trim(rat_map."RatingAgency") = '')
	AND	(rat."Rating_IsFxRating" = rat_map."IsFxRating" OR trim(rat_map."IsFxRating") = '')
	AND	(rat."Rating_RatingCategory" = rat_map."RatingCategory" OR trim(rat_map."RatingCategory") = '')
	AND	(rat."Rating_RatingMethod" = rat_map."RatingMethod" OR trim(rat_map."RatingMethod") = '')
	AND	(rat."Rating_RatingStatus" = rat_map."RatingStatus" OR trim(rat_map."RatingStatus") = '')
	AND	(rat."Rating_TimeHorizon" = rat_map."TimeHorizon" OR trim(rat_map."TimeHorizon") = '')
	AND (rat_map."StreamName" = :I_STREAM_NAME OR trim(rat_map."StreamName") = '');
END;