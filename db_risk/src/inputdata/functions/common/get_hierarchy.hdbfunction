FUNCTION "com.adweko.adapter.osx.inputdata.common::get_hierarchy"(
 	IN I_BUSINESS_DATE    DATE, 
	IN I_SYSTEM_TIMESTAMP TIMESTAMP
)
       RETURNS TABLE(
       	"Ide_Hierarchy_Rank"	NVARCHAR(32),
       	"Hierarchy_Parent_BusinessPartnerID" NVARCHAR(128),
		"Hierarchy_Child_BusinessPartnerID" NVARCHAR(128)
       )
       	
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        returns the hierarchy rank with parent and child 

        the HIERARCHY function is provided by SAP itself 
        the start of the hierarchy is per default null
    	the grandparent or the root of all, gets determined by beeing its own child
        
    ****************************/
    RETURN 
	SELECT 
		"HIERARCHY_RANK" AS  "Ide_Hierarchy_Rank",  
		COALESCE("PARENT_ID", "NODE_ID")  AS "Hierarchy_Parent_BusinessPartnerID",
		"NODE_ID" AS "Hierarchy_Child_BusinessPartnerID"
	FROM HIERARCHY (
		SOURCE ( 
			SELECT DISTINCT
				orgaUnit."ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID" AS node_id,
    			OrganizationalUnitHierarchyRelation."ASSOC_Parent_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID" AS parent_id
    		FROM "com.adweko.adapter.osx.synonyms::OrganizationalUnit_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) orgaUnit
			LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::OrganizationalUnitHierarchyRelation_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) OrganizationalUnitHierarchyRelation
				ON OrganizationalUnitHierarchyRelation."ASSOC_Child_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID" = orgaUnit."ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID"
			ORDER BY node_id, parent_id
		)
	); 
END;