FUNCTION "com.adweko.adapter.osx.inputdata.common::get_recoveryRate"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_STREAM_NAME NVARCHAR(128)
		)
RETURNS TABLE (
		"CreditRiskRecoveryRate_FinancialContract_FinancialContractID" NVARCHAR(128),
		"CreditRiskRecoveryRate_FinancialContract_IDSystem" NVARCHAR(40),
		"CreditRiskRecoveryRate_FinancialInstrument_FinancialInstrumentID" NVARCHAR(128),
		"CreditRiskRecoveryRate_RecoveryRateEstimationMethod" NVARCHAR(100),
		"CreditRiskRecoveryRate_RecoveryRateTimeHorizon" DECIMAL(34, 6),
		"CreditRiskRecoveryRate_RecoveryRateTimeHorizonUnit" NVARCHAR(128),
		"CreditRiskRecoveryRate_RiskProvisionScenario" NVARCHAR(100),
		"CreditRiskRecoveryRate_RecoveryRate" DECIMAL(15, 11),
		"CreditEnhancementStep" NVARCHAR(100)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS
BEGIN
    /*****************************
        Write your function logic
    ****************************/
RETURN

	SELECT
		rec."CreditRiskRecoveryRate_FinancialContract_FinancialContractID",
		rec."CreditRiskRecoveryRate_FinancialContract_IDSystem",
		rec."CreditRiskRecoveryRate_FinancialInstrument_FinancialInstrumentID",
		rec."CreditRiskRecoveryRate_RecoveryRateEstimationMethod",
		rec."CreditRiskRecoveryRate_RecoveryRateTimeHorizon",
		rec."CreditRiskRecoveryRate_RecoveryRateTimeHorizonUnit",
		rec."CreditRiskRecoveryRate_RiskProvisionScenario",
		"com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard"(rec."CreditRiskRecoveryRate_RecoveryRate", 'netRecoveryRate', kv."Value") AS "CreditRiskRecoveryRate_RecoveryRate",
		rec_map."CreditEnhancementStep"
		
	FROM "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskRecoveryRate"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS rec
	
	INNER JOIN "com.adweko.adapter.osx.mappings::map_recoveryRate_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS rec_map ON
		(rec."CreditRiskRecoveryRate_RecoveryRateEstimationMethod"=trim(rec_map."RecoveryRateEstimationMethod")
			OR trim(rec_map."RecoveryRateEstimationMethod") = '')
		AND	(rec."CreditRiskRecoveryRate_RecoveryRateTimeHorizon"= trim(rec_map."RecoveryRateTimeHorizon")
			OR trim(rec_map."RecoveryRateTimeHorizon") = '')
		AND	(rec."CreditRiskRecoveryRate_RecoveryRateTimeHorizonUnit"= trim(rec_map."RecoveryRateTimeHorizonUnit")
			OR trim(rec_map."RecoveryRateTimeHorizonUnit") = '')
		AND	(rec."CreditRiskRecoveryRate_RiskProvisionScenario"= trim(rec_map."RiskProvisionScenario")
			OR trim(rec_map."RiskProvisionScenario") = '')
		AND (rec_map."StreamName" = :I_STREAM_NAME
			OR trim(rec_map."StreamName") = '')
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP)  AS kv
			ON  kv."KeyID" = 'SAPPercentageStandard';
END;