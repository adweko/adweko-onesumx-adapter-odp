FUNCTION "com.adweko.adapter.osx.inputdata.sft.repo::RepurchaseAgreementUnderlying_get_ProductFSClass"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
       		"FinancialContract_FinancialContractID" NVARCHAR(128),
       		"FinancialContract_IDSystem" NVARCHAR(40),
       		"FinancialInstrument_FinancialInstrumentID" NVARCHAR(128),
       		"productFSClass" NVARCHAR(128)
       )
       LANGUAGE SQLSCRIPT
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN
	SELECT DISTINCT
		fc."FinancialContract_FinancialContractID",
		fc."FinancialContract_IDSystem",
		fc."FinancialInstrument_FinancialInstrumentID",
		
		CASE 
			WHEN fc."FinancialInstrument_IssuerOfSecurity_BusinessPartnerID" 
					= 
				MAP(IFNULL(keylegalEntity."Value",'OrganizationalUnitContractAssignment'),
					'OrganizationalUnitContractAssignment', ouca."ASSOC_OrgUnit_ASSOC_OrganizationHostingOrganizationalUnit_BusinessPartnerID",
					'BusinessPartnerContractAssignment', bpcdo."ASSOC_PartnerInParticipation_BusinessPartnerID")
				OR hier."Hierarchy_Child_BusinessPartnerID" IS NOT NULL
					THEN 'SECURITY_ISSUED'
			
			WHEN EURegRepForContract."EuropeanRegulatoryReportingForContract_LiquidityClassificationOfAsset" IN ('ExtremlyHighLiquid', 'HighLiquidLevelA', 'HighLiquidLevelB')
					THEN 'CAPITAL'
					
			ELSE productFSClass."productFSClass"		

		END AS "productFSClass"

	
    FROM "com.adweko.adapter.osx.inputdata.sft.repo::RepurchaseAgreementUnderlying_DatagroupAssignment_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) fc

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS keylegalEntity
		ON keylegalEntity."KeyID" = 'legalEntitySwitch'	
	
   	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_OrganizationalUnitContractAssignmentPrio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS ouca
   		ON ouca."ASSOC_Contract_FinancialContractID"	= fc."FinancialContract_FinancialContractID" 
   		AND ouca."ASSOC_Contract_IDSystem"				= fc."FinancialContract_IDSystem"
    	AND ouca."RoleOfOrganizationalUnit"				= 'ManagingUnit'
    	
    LEFT OUTER JOIN "com.adweko.adapter.osx.synonyms::BusinessPartnerContractAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) bpcdo
    	ON  bpcdo."ASSOC_FinancialContract_FinancialContractID"	= fc."FinancialContract_FinancialContractID" 
    	AND bpcdo."ASSOC_FinancialContract_IDSystem"			= fc."FinancialContract_IDSystem"
    	AND bpcdo."ContractDataOwner"							= true
    
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_hierarchy"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS hier
		ON hier."Hierarchy_Child_BusinessPartnerID" = fc."FinancialInstrument_IssuerOfSecurity_BusinessPartnerID"
    	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_EuropeanRegulatoryReportingForContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS EURegRepForContract
		ON EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialInstrument_FinancialInstrumentID" = fc."FinancialInstrument_FinancialInstrumentID"
		
    LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalogFI"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP ) pca 
    	ON pca."_FinancialInstrument_FinancialInstrumentID" = fc."FinancialInstrument_FinancialInstrumentID"

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_productFSClass_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP )  AS productFSClass
		ON productFSClass."ProductCatalogItem" = pca."StandardCatalog_ProductCatalogItem"
		
	WHERE IFNULL (keylegalEntity."Value",'OrganizationalUnitContractAssignment') IN ('OrganizationalUnitContractAssignment','BusinessPartnerContractAssignment')
	
	;
END;