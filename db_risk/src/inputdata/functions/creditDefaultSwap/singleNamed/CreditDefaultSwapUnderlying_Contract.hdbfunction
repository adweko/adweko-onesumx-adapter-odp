FUNCTION "com.adweko.adapter.osx.inputdata.creditDefaultSwap.singleNamed::CreditDefaultSwapUnderlying_Contract"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE(
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"valueDate" DATE,
		"maturityDate" DATE,
		"counterparty" NVARCHAR(128),
		"currentPrincipal" DECIMAL(34, 6),
		"quantity" DECIMAL(34, 6),
		"currency" NVARCHAR(3),
		"issuer" NVARCHAR(128),
		"isin" NVARCHAR(12),
		"priceAtCDD" DECIMAL(34, 6),
		"marketValueObserved" DECIMAL(34, 6),
		"marketValueDate" DATE,
		"contractDayCountMethod" NVARCHAR(2),
		"interestPaymentType" NVARCHAR(1),
		"businessDayConvention" NVARCHAR(2),
		"calendar" NVARCHAR(75),
		"eomConvention" NVARCHAR(1),
		"legalEntity" NVARCHAR(128),
		"coveredBond" NVARCHAR(5),
		"productFSClass" NVARCHAR(128),
		"contractFSType" NVARCHAR(256),
		"currentNominalInterestRate" DECIMAL(15, 11),
		"clearingHouse" NVARCHAR(128),
		"linkID" NVARCHAR(256),
		"linkType" NVARCHAR(3),
		"contractRole" INT,
		"referenceEntity" NVARCHAR(128),
		"isUnderLiquidityManagementControl" INT,
		"cbHaircutPercentage" DECIMAL(15, 11),
		"dataGroup" NVARCHAR(128),
		"lookbackPeriod" NVARCHAR(10),
		"rateCappingFlooring" NVARCHAR(40),
		"rateAveragingCap" DECIMAL(15, 11),
		"rateAveragingFloor" DECIMAL(15, 11),
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
		"lmEntityName" NVARCHAR(128),
		"bookValue" DECIMAL(34, 6),
		"exposureClass" NVARCHAR(255)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN

	SELECT
		"com.adweko.adapter.osx.inputdata.common::get_id" (
			root."contractID_Underlying",
			root."node_No",
			root."dataGroup"
		) AS "id",
		root."contractID_Underlying" AS "sourceSystemRecordNumber",
		root."FinancialContract_FinancialContractID" || '#CDS' || root."FinancialInstrument_FinancialInstrumentID" AS "sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		root."node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		root."FinancialInstrument_IssueDate" AS "dealDate",
		COALESCE(root."FinancialInstrument_IssueDate",root."CreditDefaultSwapProtectionLeg_ProtectionStartDate") AS "valueDate",
		root."FinancialContract_MaturityDate" AS "maturityDate",
		CASE
			WHEN (root."ProtectionBuyer_BusinessPartnerContractAssignment_ContractDataOwner" = true OR root."ProtectionSeller_BusinessPartnerContractAssignment_ContractDataOwner" = true)
				THEN CASE
					WHEN root."ProtectionBuyer_BusinessPartnerContractAssignment_ContractDataOwner" = false
						THEN root."ProtectionBuyer_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
					WHEN root."ProtectionSeller_BusinessPartnerContractAssignment_ContractDataOwner" = false
						THEN root."ProtectionSeller_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
				END
		END AS "counterparty",
		ABS(root."CreditDefaultSwapProtectionLeg_ProtectionAmount") * root."isActiveSign" AS "currentPrincipal",
		1 AS "quantity",
		root."CreditDefaultSwapProtectionLeg_ProtectionAmountCurrency" AS "currency",
		root."FinancialInstrument_IssuerOfSecurity_BusinessPartnerID" AS "issuer",
		root."isin" AS "isin",
		ABS(root."CreditDefaultSwapProtectionLeg_ProtectionAmount" * root."CreditDefaultSwapProtectionLeg_ReferencePrice") * root."isActiveSign" AS "priceAtCDD",
		ABS(root."EndOfDayPriceObservation_Close") * root."isActiveSign" AS "marketValueObserved",
		root."EndOfDayPriceObservation_BusinessValidFrom" AS "marketValueDate",
		inte."contractDayCountMethod",
		inte."interestPaymentType",
		inte."businessDayConvention",
		inte."calendar",
		inte."eomConvention",
		MAP(root."legalEntitySwitch",
			'OrganizationalUnitContractAssignment', root."OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",
			'BusinessPartnerContractAssignment', CASE
													WHEN root."ProtectionSeller_BusinessPartnerContractAssignment_ContractDataOwner" = true
														THEN root."ProtectionSeller_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
													WHEN root."ProtectionBuyer_BusinessPartnerContractAssignment_ContractDataOwner" = true
														THEN root."ProtectionBuyer_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
													WHEN root."Broker_BusinessPartnerContractAssignment_ContractDataOwner" = true
														THEN root."Broker_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
												END
		) AS "legalEntity",
		MAP(root."FinancialInstrument_CoveredBond",
			true, 1,
			false, 0
		) AS "coveredBond",
		productFSClass."productFSClass",
		"com.adweko.adapter.osx.inputdata.common::get_contractFSType"(
			pc."StandardCatalog_ProductCatalogItem",
			root."FinancialInstrument_PreferredOrCommonStock",
			root."FinancialInstrument_SecurityCategory",
			EURegRepForContract."EuropeanRegulatoryReportingForContract_TreatedAsUCITS",
			contractFSTypeSecurity."contractFSType"
		) AS "contractFSType",
		CAST("com.adweko.adapter.osx.inputdata.common::get_SAPpercentStandard" (inte."currentNominalInterestRate", 'currentNominalInterestRate', kv."Value") AS DECIMAL(15,11)) AS "currentNominalInterestRate",
		CASE
			WHEN root."BusinessPartner_RoleInclearing" IN ('ClearingHouse', 'CentralCounterparty')
				THEN CASE
					WHEN root."ProtectionBuyer_BusinessPartnerContractAssignment_ContractDataOwner" = false
						THEN root."ProtectionBuyer_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
					WHEN root."ProtectionSeller_BusinessPartnerContractAssignment_ContractDataOwner" = false
						THEN root."ProtectionSeller_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
				END
		END AS "clearingHouse",
		root."FinancialContract_FinancialContractID" AS "linkID",
		'6' AS "linkType",
		2 AS "contractRole",
		root."CreditDefaultSwapReferenceEntity_ReferenceEntity" AS "referenceEntity",
		1 AS "isUnderLiquidityManagementControl",
        IFNULL(cbHaircut."cbHaircutPercentage", 1) AS "cbHaircutPercentage",
		root."dataGroup",
		inte."lookbackPeriod",
		inte."rateCappingFlooring",
		inte."rateAveragingCap",
		inte."rateAveragingFloor",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",
		NULL AS "lmEntityName",
		bok."BookValue_BookValue" AS "bookValue",
		expop."exposureClass"
		
		FROM "com.adweko.adapter.osx.inputdata.creditDefaultSwap.singleNamed::CreditDefaultSwap_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS root
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditDefaultSwap.singleNamed::CreditDefaultSwapUnderlying_get_ProductFSClass"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS productFSClass
			 ON productFSClass."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
			AND productFSClass."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
			AND productFSClass."FinancialInstrument_FinancialInstrumentID" = root."FinancialInstrument_FinancialInstrumentID"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::BV_ProductCatalogFI"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP ) AS pc
    		 ON pc."_FinancialInstrument_FinancialInstrumentID" = root."FinancialInstrument_FinancialInstrumentID"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_EuropeanRegulatoryReportingForContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS EURegRepForContract
			 ON EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
			AND EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
			AND EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialInstrument_FinancialInstrumentID" = root."FinancialInstrument_FinancialInstrumentID"

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP, 'CreditDefaultSwap') AS expop
			 ON expop."CreditRiskExposure_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
			AND expop."CreditRiskExposure_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditDefaultSwap.singleNamed::CreditDefaultSwapUnderlying_get_InterestPrio"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS inte
			 ON inte."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
			AND inte."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
			AND inte."FinancialInstrument_FinancialInstrumentID" = root."FinancialInstrument_FinancialInstrumentID"
		
		LEFT OUTER JOIN  "com.adweko.adapter.osx.inputdata.common::get_cbHaircutPercentage"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'CreditDefaultSwapUnderlying') AS cbHaircut
			ON cbHaircut."FinancialInstrument_FinancialInstrumentID" = root."FinancialInstrument_FinancialInstrumentID"
			 
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_contractFSTypeSecurity"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS contractFSTypeSecurity
			 ON contractFSTypeSecurity."BusinessPartnerID" = root."FinancialInstrument_IssuerOfSecurity_BusinessPartnerID"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
			ON bok."BookValue_FinancialContract_IDSystem"					= root."FinancialContract_IDSystem"
			AND bok."BookValue_FinancialContract_FinancialContractID"		= root."FinancialContract_FinancialContractID"
			AND bok."BookValue_BookValueCurrency"							= root."CreditDefaultSwapProtectionLeg_ProtectionAmountCurrency"

		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE , :I_SYSTEM_TIMESTAMP  )  AS kv
			 ON  kv."KeyID" = 'SAPPercentageStandard';
END;