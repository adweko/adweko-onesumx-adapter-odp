FUNCTION "com.adweko.adapter.osx.inputdata.creditDefaultSwap.singleNamed::CreditDefaultSwapUnderlying_get_InterestPrio"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"FinancialContract_IDSystem" NVARCHAR(40),
		"FinancialInstrument_FinancialInstrumentID" NVARCHAR(128),
		"SequenceNumber" INTEGER,
		"contractDayCountMethod" NVARCHAR(2),
		"interestPaymentType" NVARCHAR(1),
		"businessDayConvention" NVARCHAR(2),
		"calendar" NVARCHAR(75),
		"eomConvention" NVARCHAR(1),
		"currentNominalInterestRate" DECIMAL(15, 11),
		"lookbackPeriod" NVARCHAR(10),
		"rateCappingFlooring" NVARCHAR(40),
		"rateAveragingCap" DECIMAL(15, 11),
		"rateAveragingFloor" DECIMAL(15, 11)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN

	SELECT
		root."FinancialContract_FinancialContractID",
		root."FinancialContract_IDSystem",
		root."FinancialInstrument_FinancialInstrumentID",
		inte."SequenceNumber",
		dcm."dayCountMethod" AS "contractDayCountMethod",
		"com.adweko.adapter.osx.inputdata.common::get_interestPaymentType"(inte."InterestInAdvance", 1, 2) AS "interestPaymentType",
		"com.adweko.adapter.osx.inputdata.common::get_businessDayConvention"(bdc."OsxBusinessDayConventionCode") AS "businessDayConvention",
		cal."calendar",
		"com.adweko.adapter.osx.inputdata.common::get_eomConvention" (inte."DayOfMonthOfInterestPeriodEnd") AS "eomConvention",		
		MAP(inte."InterestSpecificationCategory",
			'FixedRateSpecification', inte."FixedRate",
			'FloatingRateSpecification', appl."ApplicableInterestRate_Rate"
		) AS "currentNominalInterestRate",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
						inte."ResetCutoffLength",
						mcp."cyclePeriod")
		END AS "lookbackPeriod",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN 'MONETARY_REFERENCE'
		END AS "rateCappingFlooring",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", inte."FloatingRateMax", inte."VariableRateMax")
		END AS "rateAveragingCap",	
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", inte."FloatingRateMin", inte."VariableRateMin")
		END AS "rateAveragingFloor"

	FROM "com.adweko.adapter.osx.inputdata.creditDefaultSwap.singleNamed::CreditDefaultSwap_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS root

	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS inte
		 ON inte."_DebtInstrument_FinancialInstrumentID"			= root."FinancialInstrument_FinancialInstrumentID"
		AND :I_BUSINESS_DATE										>= "FirstInterestPeriodStartDate" 
		AND	:I_BUSINESS_DATE										<= IFNULL("LastInterestPeriodEndDate",'9999-12-30')
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ApplicableInterestRate"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS appl
		 ON appl."ApplicableInterestRate_FinancialInstrument_FinancialInstrumentID"	= root."FinancialInstrument_FinancialInstrumentID"
		AND appl."ApplicableInterestRate_InterestSequenceNumber" = inte."SequenceNumber"

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS dcm 
		 ON dcm."DayCountConvention"								= inte."DayCountConvention"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_BDC_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS bdc
		 ON bdc."CalculationDay"									= inte."BusinessDayConvention"
		AND bdc."PaymentDay"										= inte."DueScheduleBusinessDayConvention"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_calendar_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS cal 
		 ON cal."BusinessCalendar"									= inte."InterestBusinessCalendar"

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" = inte."ResetCutoffTimeUnit"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS cfss
		ON cfss."KeyID" = 'CapFloorSwitch'
	
	WHERE root."contractID_Underlying" IS NOT NULL;
END;