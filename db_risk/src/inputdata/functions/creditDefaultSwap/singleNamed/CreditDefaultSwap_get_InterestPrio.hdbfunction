FUNCTION "com.adweko.adapter.osx.inputdata.creditDefaultSwap.singleNamed::CreditDefaultSwap_get_InterestPrio"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"FinancialContract_IDSystem" NVARCHAR(40),
		"SequenceNumber" INTEGER,
		"currentNominalInterestRate" DECIMAL(15, 11),
		"contractDayCountMethod" NVARCHAR(2),
		"calendar" NVARCHAR(75),
		"eomConvention" NVARCHAR(1),
		"interestPaymentType" NVARCHAR(1),
		"lookbackPeriod" NVARCHAR(10),
		"rateCappingFlooring" NVARCHAR(40),
		"rateAveragingCap" DECIMAL(15, 11),
		"rateAveragingFloor" DECIMAL(15, 11)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN

	SELECT
		root."FinancialContract_FinancialContractID",
		root."FinancialContract_IDSystem",
		inte."SequenceNumber",
		CASE
			WHEN inte."RoleOfPayer" = root."BankLeg"
				THEN MAP(inte."InterestSpecificationCategory",
					'FixedRateSpecification', inte."FixedRate",
					'FloatingRateSpecification', appinte."ApplicableInterestRate_Rate"
				)
		END AS "currentNominalInterestRate",
		dcm."dayCountMethod" AS "contractDayCountMethod",
		cal."calendar" AS "calendar",
		"com.adweko.adapter.osx.inputdata.common::get_eomConvention"(inte."DayOfMonthOfInterestPeriodEnd") AS "eomConvention",
		NULL AS "interestPaymentType",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
						inte."ResetCutoffLength",
						mcp."cyclePeriod")
		END AS "lookbackPeriod",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN 'MONETARY_REFERENCE'
		END AS "rateCappingFlooring",
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", inte."FloatingRateMax", inte."VariableRateMax")
		END AS "rateAveragingCap",	
		CASE WHEN kvMonetaryRef."Value" = 'On' AND inte."InterestIsCompounded" = true
			THEN "com.adweko.adapter.osx.inputdata.common::get_floatingRateType"(cfss."Value", inte."FloatingRateMin", inte."VariableRateMin")
		END AS "rateAveragingFloor"
    
	FROM "com.adweko.adapter.osx.inputdata.creditDefaultSwap.singleNamed::CreditDefaultSwap_Root_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS inte
		 ON inte."FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND inte."IDSystem" = root."FinancialContract_IDSystem"
		AND inte."RoleOfPayer" = root."BankLeg"
		AND :I_BUSINESS_DATE >= "FirstInterestPeriodStartDate"
		AND	:I_BUSINESS_DATE <= IFNULL("LastInterestPeriodEndDate",'9999-12-30')
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ApplicableInterestRate"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS appinte
		 ON appinte."ApplicableInterestRate_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
		AND appinte."ApplicableInterestRate_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_dayCountMethod_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS dcm
		 ON	dcm."DayCountConvention" = inte."DayCountConvention"
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_calendar_View" ( :I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP ) AS cal 
		 ON cal."BusinessCalendar"     = inte."InterestBusinessCalendar"

	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" = inte."ResetCutoffTimeUnit"
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvMonetaryRef
		ON kvMonetaryRef."KeyID" = 'MonetaryReferenceFlag'
		
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS cfss
		ON cfss."KeyID" = 'CapFloorSwitch'	
	
	WHERE root."contractID_Underlying" IS NOT NULL;
END;