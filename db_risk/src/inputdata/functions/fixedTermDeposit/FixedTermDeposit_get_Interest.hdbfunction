FUNCTION "com.adweko.adapter.osx.inputdata.fixedTermDeposit::FixedTermDeposit_get_Interest"(IN I_BUSINESS_DATE DATE, IN I_SYSTEM_TIMESTAMP TIMESTAMP, IN I_DATAGROUP_STRING NVARCHAR(128))
       RETURNS TABLE (
		"FinancialContract_FinancialContractID" NVARCHAR(128),
		"FinancialContract_IDSystem"			NVARCHAR(40),
		"Interest_InterestCurrency"				NVARCHAR(3),
/*		"Interest_InterestType" NVARCHAR(40),
*/		"Interest_FirstInterestPeriodStartDate" DATE,
/*		"Interest_FirstInterestPeriodEndDate" DATE,	
*/		"Interest_LastInterestPeriodEndDate"	DATE,
		"arraySpecKind" 						NVARCHAR(5),
		"cycleDate"								DATE,
		"cyclePeriod"							NVARCHAR(10),	
		"repricingType"							NVARCHAR(1),
		"pricingMarketObject"					NVARCHAR(40),
		"rateAdd"								DECIMAL(15, 11),
		"rateMultiplier"						DECIMAL(15, 11),
		"rateTerm"								NVARCHAR(10),
		"fixingDays"							NVARCHAR(10),
		"interestPaymentType"					NVARCHAR(1),
		"eomConvention"							NVARCHAR(1),
		"Interest_InterestIsCompounded" 		INTEGER,
		"dataGroup" 							NVARCHAR(128)
		)
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
    RETURN
	    SELECT --Regular IP Periods after Stub
			fc."FinancialContractID"	AS "FinancialContract_FinancialContractID",
			fc."IDSystem"				AS "FinancialContract_IDSystem",
			fixedTermDeposit_InterestPrio."Interest_InterestCurrency" AS "Interest_InterestCurrency",
/*		acc."Interest_InterestType",
*/			fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodStartDate" AS "Interest_FirstInterestPeriodStartDate",
/*		acc."Interest_FirstInterestPeriodEndDate",
*/			fixedTermDeposit_InterestPrio."Interest_LastInterestPeriodEndDate" AS "Interest_LastInterestPeriodEndDate",
			'IP' AS "arraySpecKind",
			fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodEndDate" AS "cycleDate",
			"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
				fixedTermDeposit_InterestPrio."Interest_InterestPeriodLength",
				cyclePeriod."cyclePeriod"
			) AS "cyclePeriod",
			NULL AS "repricingType",
			NULL AS "pricingMarketObject",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			"com.adweko.adapter.osx.inputdata.common::get_interestPaymentType"(
				fixedTermDeposit_InterestPrio."Interest_InterestInAdvance",
			2, 1)
			AS "interestPaymentType",
			"com.adweko.adapter.osx.inputdata.common::get_eomConvention"(fixedTermDeposit_InterestPrio."Interest_DayOfMonthOfInterestPeriodEnd") AS "eomConvention",
			MAP(fixedTermDeposit_InterestPrio."Interest_InterestIsCompounded",
				true,	1,
				false,	0
			) AS "Interest_InterestIsCompounded",
			fc."dataGroup" AS "dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.fixedTermDeposit::FixedTermDeposit_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS fc
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.fixedTermDeposit::FixedTermDeposit_InterestPrio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fixedTermDeposit_InterestPrio
			ON fixedTermDeposit_InterestPrio."FinancialContract_FinancialContractID"	= fc."FinancialContractID"
	    		AND fixedTermDeposit_InterestPrio."FinancialContract_IDSystem"			= fc."IDSystem"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cyclePeriod
			ON cyclePeriod."BusinesscyclePeriod" =	fixedTermDeposit_InterestPrio."Interest_InterestPeriodTimeUnit"

		WHERE	(fixedTermDeposit_InterestPrio."Interest_InterestCategory"			= 'InterestPeriodSpecification'
					AND	fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodEndDate"	IS NOT NULL
					AND (fixedTermDeposit_InterestPrio."Interest_LastInterestPeriodEndDate"	> :I_BUSINESS_DATE 
						OR fixedTermDeposit_InterestPrio."Interest_LastInterestPeriodEndDate" IS NULL)
					AND	fixedTermDeposit_InterestPrio."Interest_LastInterestPeriodEndDate"	> fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodEndDate"
					AND	fixedTermDeposit_InterestPrio."Interest_InterestPeriodLength" 		IS NOT NULL
					AND	fixedTermDeposit_InterestPrio."Interest_InterestPeriodTimeUnit"		IS NOT NULL)
			
		UNION ALL
	
		SELECT --Stub Period and regular IP Periods
			fc."FinancialContractID"	AS "FinancialContract_FinancialContractID",
			fc."IDSystem"				AS "FinancialContract_IDSystem",
			fixedTermDeposit_InterestPrio."Interest_InterestCurrency" AS "Interest_InterestCurrency",
	/*		acc."Interest_InterestType",
*/			fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodStartDate" AS "Interest_FirstInterestPeriodStartDate",
/*			acc."Interest_FirstInterestPeriodEndDate",
*/			fixedTermDeposit_InterestPrio."Interest_LastInterestPeriodEndDate" AS "Interest_LastInterestPeriodEndDate",
			'IP' AS "arraySpecKind",
			fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodStartDate" AS "cycleDate",
			MAP(fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodEndDate",
				NULL, "com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(fixedTermDeposit_InterestPrio."Interest_InterestPeriodLength",
						cyclePeriod."cyclePeriod"),
				'999Y' 
			) AS "cyclePeriod",
			NULL AS "repricingType",
			NULL AS "pricingMarketObject",
			NULL AS "rateAdd",
			NULL AS "rateMultiplier",
			NULL AS "rateTerm",
			NULL AS "fixingDays",
			"com.adweko.adapter.osx.inputdata.common::get_interestPaymentType"(
				fixedTermDeposit_InterestPrio."Interest_InterestInAdvance",
				2, 1
			) AS "interestPaymentType",
			"com.adweko.adapter.osx.inputdata.common::get_eomConvention"(fixedTermDeposit_InterestPrio."Interest_DayOfMonthOfInterestPeriodEnd") AS "eomConvention",
			MAP(fixedTermDeposit_InterestPrio."Interest_InterestIsCompounded",
				true, 1,
				false, 0
			) AS "Interest_InterestIsCompounded",
			fc."dataGroup" AS "dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.fixedTermDeposit::FixedTermDeposit_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS fc
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.fixedTermDeposit::FixedTermDeposit_InterestPrio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fixedTermDeposit_InterestPrio ---EZ A sintePrio
			ON fixedTermDeposit_InterestPrio."FinancialContract_FinancialContractID"	= fc."FinancialContractID"
	    		AND fixedTermDeposit_InterestPrio."FinancialContract_IDSystem"			= fc."IDSystem"
				
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cyclePeriod
			ON cyclePeriod."BusinesscyclePeriod"	= fixedTermDeposit_InterestPrio."Interest_InterestPeriodTimeUnit"
	
		WHERE	(fixedTermDeposit_InterestPrio."Interest_InterestCategory"				= 'InterestPeriodSpecification'
					AND (fixedTermDeposit_InterestPrio."Interest_LastInterestPeriodEndDate" 	> :I_BUSINESS_DATE
						OR fixedTermDeposit_InterestPrio."Interest_LastInterestPeriodEndDate"	IS NULL)
					AND fixedTermDeposit_InterestPrio."Interest_InterestPeriodLength"			IS NOT NULL
					AND fixedTermDeposit_InterestPrio."Interest_InterestPeriodTimeUnit" 		IS NOT NULL)
	
		UNION ALL
	
		SELECT--regular DependentFloatingRateSpecification RP Period after Stub
			fc."FinancialContractID"	AS "FinancialContract_FinancialContractID",
			fc."IDSystem"				AS "FinancialContract_IDSystem",
			fixedTermDeposit_InterestPrio."Interest_InterestCurrency" AS "Interest_InterestCurrency",
/*			acc."Interest_InterestType",
*/			fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodStartDate" AS "Interest_FirstInterestPeriodStartDate",
/*		acc."Interest_FirstInterestPeriodEndDate",
*/			fixedTermDeposit_InterestPrio."Interest_LastInterestPeriodEndDate" AS "Interest_LastInterestPeriodEndDate",
			'RP' AS "arraySpecKind",
			fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodEndDate" AS "cycleDate",
			"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(fixedTermDeposit_InterestPrio."Interest_InterestPeriodLength", cyclePeriod_InterestPeriodTimeUnit."cyclePeriod")	AS "cyclePeriod",
			NULL AS "repricingType",
			pricingMarketObject."pricingMarketObject" AS "pricingMarketObject",
			fixedTermDeposit_InterestPrio."Interest_Spread" AS "rateAdd",
			fixedTermDeposit_InterestPrio."Interest_ReferenceRateFactor" AS "rateMultiplier",
			"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
				referenceRate."ReferenceRate_TimeToMaturity",
				cyclePeriod_TimeToMaturityUnit."cyclePeriod"
			) AS "rateTerm",
			"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(fixedTermDeposit_InterestPrio."Interest_ResetLagLength",	cyclePeriod_ResetLagTimeUnit."cyclePeriod") AS "fixingDays",
			NULL AS "interestPaymentType",
			NULL AS "eomConvention",
			MAP(fixedTermDeposit_InterestPrio."Interest_InterestIsCompounded",
				true,	1,
				false,	0
			) AS "Interest_InterestIsCompounded",
			fc."dataGroup" AS "dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.fixedTermDeposit::FixedTermDeposit_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS fc
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.fixedTermDeposit::FixedTermDeposit_InterestPrio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fixedTermDeposit_InterestPrio ---EZ A sintePrio
			ON fixedTermDeposit_InterestPrio."FinancialContract_FinancialContractID"	= fc."FinancialContractID"
	    		AND fixedTermDeposit_InterestPrio."FinancialContract_IDSystem"			= fc."IDSystem"
	    
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ReferenceRate"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS referenceRate
			ON referenceRate."ReferenceRate_ReferenceRateID" =	fixedTermDeposit_InterestPrio."Interest_ReferenceRateID"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cyclePeriod_InterestPeriodTimeUnit
			ON cyclePeriod_InterestPeriodTimeUnit."BusinesscyclePeriod" = fixedTermDeposit_InterestPrio."Interest_InterestPeriodTimeUnit"
																				
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cyclePeriod_TimeToMaturityUnit
			ON cyclePeriod_TimeToMaturityUnit."BusinesscyclePeriod" = referenceRate."ReferenceRate_TimeToMaturityUnit"
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cyclePeriod_ResetLagTimeUnit
			ON cyclePeriod_ResetLagTimeUnit."BusinesscyclePeriod" = fixedTermDeposit_InterestPrio."Interest_ResetLagTimeUnit"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_pricingMarketObject_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pricingMarketObject
			ON	pricingMarketObject."ReferenceRateID" =	fixedTermDeposit_InterestPrio."Interest_ReferenceRateID"
		
		WHERE	(fixedTermDeposit_InterestPrio."Interest_InterestSpecificationCategory"		= 'FloatingRateSpecification'
					AND fixedTermDeposit_InterestPrio."Interest_FixingRateSpecificationCategory"	='DependentFloatingRateSpecification'
					AND fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodEndDate" 		IS NOT NULL
					AND fixedTermDeposit_InterestPrio."Interest_LastInterestPeriodEndDate"			IS NOT NULL)
				
		UNION ALL
				
		SELECT--regular RP Periods and Stub Periods
			fc."FinancialContractID"	AS "FinancialContract_FinancialContractID",
			fc."IDSystem"				AS "FinancialContract_IDSystem",
			fixedTermDeposit_InterestPrio."Interest_InterestCurrency" AS "Interest_InterestCurrency",
/*			acc."Interest_InterestType",
*/			fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodStartDate" AS "Interest_FirstInterestPeriodStartDate",
/*		acc."Interest_FirstInterestPeriodEndDate",
*/			fixedTermDeposit_InterestPrio."Interest_LastInterestPeriodEndDate" AS "Interest_LastInterestPeriodEndDate",
			'RP' AS "arraySpecKind",
			CASE
				WHEN fixedTermDeposit_InterestPrio."Interest_FixingRateSpecificationCategory"	= 'IndependentFloatingRateSpecification'
					AND fixedTermDeposit_InterestPrio."Interest_InterestSpecificationCategory"	= 'FloatingRateSpecification'
						THEN fixedTermDeposit_InterestPrio."Interest_FirstRegularFloatingRateResetDate"
				WHEN fixedTermDeposit_InterestPrio."Interest_FixingRateSpecificationCategory" = 'DependentFloatingRateSpecification'
					THEN fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodStartDate"
				WHEN fixedTermDeposit_InterestPrio."Interest_InterestSpecificationCategory" = 'FixedRateSpecification'
					THEN fixedTermDeposit_InterestPrio."Interest_FirstInterestPeriodStartDate"
			END AS "cycleDate",
			CASE
				WHEN fixedTermDeposit_InterestPrio."Interest_InterestSpecificationCategory" IN ('FloatingRateSpecification', 'FixedRateSpecification')
					THEN CASE 
					        WHEN   fixedTermDeposit_InterestPrio."Interest_InterestSpecificationCategory" = 'FloatingRateSpecification' AND
								   fixedTermDeposit_InterestPrio."Interest_FixingRateSpecificationCategory"	= 'DependentFloatingRateSpecification' AND
								   fixedTermDeposit_InterestPrio."Interest_LastInterestPeriodEndDate"		IS NOT NULL
							THEN '999Y'
						ELSE
						"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
							fixedTermDeposit_InterestPrio."Interest_InterestPeriodLength",
							cyclePeriod_InterestPeriodTimeUnit."cyclePeriod")
						END
			END AS "cyclePeriod",
			"com.adweko.adapter.osx.inputdata.common::get_repricingType"(
							'',
							fixedTermDeposit_InterestPrio."Interest_InterestSpecificationCategory",
							fixedTermDeposit_InterestPrio."Interest_FixingRateSpecificationCategory",
							fixedTermDeposit_InterestPrio."Interest_ResetInArrears",
							fixedTermDeposit_InterestPrio."Interest_ResetAtMonthUltimo",
							fixedTermDeposit_InterestPrio."Interest_CutoffRelativeToDate"
			) AS "repricingType",
			pricingMarketObject."pricingMarketObject" AS "pricingMarketObject",
			MAP(fixedTermDeposit_InterestPrio."Interest_InterestSpecificationCategory",
				'FloatingRateSpecification', fixedTermDeposit_InterestPrio."Interest_Spread",
				'FixedRateSpecification', fixedTermDeposit_InterestPrio."Interest_FixedRate"
			) AS "rateAdd",
			MAP(fixedTermDeposit_InterestPrio."Interest_InterestSpecificationCategory",
				'FloatingRateSpecification', fixedTermDeposit_InterestPrio."Interest_ReferenceRateFactor",
				NULL
			) AS "rateMultiplier",
			"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(
				referenceRate."ReferenceRate_TimeToMaturity",
				cyclePeriod_TimeToMaturityUnit."cyclePeriod"
			) AS "rateTerm",
			"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod"(fixedTermDeposit_InterestPrio."Interest_ResetLagLength", cyclePeriod_ResetLagTimeUnit."cyclePeriod") AS "fixingDays",
			NULL AS "interestPaymentType",
			NULL AS "eomConvention",
			MAP(fixedTermDeposit_InterestPrio."Interest_InterestIsCompounded",
				true,	1,
				false,	0
			) AS "Interest_InterestIsCompounded",
			fc."dataGroup" AS "dataGroup"
		FROM "com.adweko.adapter.osx.inputdata.fixedTermDeposit::FixedTermDeposit_DatagroupAssignment_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS fc
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.fixedTermDeposit::FixedTermDeposit_InterestPrio"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS fixedTermDeposit_InterestPrio ---EZ A sintePrio
			ON fixedTermDeposit_InterestPrio."FinancialContract_FinancialContractID"	= fc."FinancialContractID"
	    		AND fixedTermDeposit_InterestPrio."FinancialContract_IDSystem"			= fc."IDSystem"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_ReferenceRate"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS referenceRate 
			ON referenceRate."ReferenceRate_ReferenceRateID" =	fixedTermDeposit_InterestPrio."Interest_ReferenceRateID"
				
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cyclePeriod_InterestPeriodTimeUnit
			ON cyclePeriod_InterestPeriodTimeUnit."BusinesscyclePeriod" = fixedTermDeposit_InterestPrio."Interest_InterestPeriodTimeUnit"
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_pricingMarketObject_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS pricingMarketObject
			ON pricingMarketObject."ReferenceRateID" = fixedTermDeposit_InterestPrio."Interest_ReferenceRateID"													
		
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cyclePeriod_TimeToMaturityUnit
			ON cyclePeriod_TimeToMaturityUnit."BusinesscyclePeriod" = referenceRate."ReferenceRate_TimeToMaturityUnit"
			
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS cyclePeriod_ResetLagTimeUnit
			ON cyclePeriod_ResetLagTimeUnit."BusinesscyclePeriod" =	fixedTermDeposit_InterestPrio."Interest_ResetLagTimeUnit"

		WHERE	fixedTermDeposit_InterestPrio."Interest_InterestSpecificationCategory"	IN ('FloatingRateSpecification','FixedRateSpecification');
END;