FUNCTION "com.adweko.adapter.osx.inputdata.equityInvestment::EquityInvestment_Contract" (
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP, 
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"sourceSystemRecordNumber" NVARCHAR(256),
		"sourceSystemID" NVARCHAR(128),
		"sourceSystemName" NVARCHAR(40),
		"node_No" NVARCHAR(4),
		"bookValueDate" DATE,
		"dealDate" DATE,
		"valueDate" DATE,
		"legalEntity" NVARCHAR(128),
		"issuer" NVARCHAR(128),
		"marketValueObserved" DECIMAL(34, 6),
		"marketValueDate" DATE,
		"counterparty" NVARCHAR(128),
		"currentPrincipal" DECIMAL(34, 6),
		"currency" NVARCHAR(3),
		"quantity" DECIMAL(34, 6),
		"priceAtCDD" DECIMAL(34, 6),
		"dataGroup" NVARCHAR(128),
		"contractFSType" NVARCHAR(256),
		"productFSClass" NVARCHAR(128),
		"instrumentType" NVARCHAR(128),
		"isUnderLiquidityManagementControl" INT,
		"StandardCatalog_ProductCatalogItem" NVARCHAR(128),
		"CustomCatalog_ProductCatalogItem" NVARCHAR(128),
		"lmEntityName" NVARCHAR(128),
		"writeOffAmount" DECIMAL(34, 6),
		"bookValue" DECIMAL(34, 6),
		"exposureClass" NVARCHAR(255)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
	RETURN

	 SELECT 
		"com.adweko.adapter.osx.inputdata.common::get_id"(
			root."contractID",
			root."node_No",
			root."dataGroup"
		) AS "id",
		root."contractID" AS "sourceSystemRecordNumber",
		root."FinancialContract_FinancialContractID" AS "sourceSystemID",
		root."FinancialContract_IDSystem" AS "sourceSystemName",
		root."node_No",
		:I_BUSINESS_DATE AS "bookValueDate",
		case
			when root."FinancialContract_OriginalSigningDate" is NULL or root."FinancialContract_OriginalSigningDate" = '' THEN
				root."FinancialContract_EquityInvestmentStartDate"
			ELSE
				root."FinancialContract_OriginalSigningDate"
			END AS "dealDate",
		root."FinancialContract_EquityInvestmentStartDate" AS "valueDate",
		MAP(root."legalEntitySwitch",
			'OrganizationalUnitContractAssignment', root."OrganizationalUnitContractAssignment_ManagingUnit_BusinessPartnerID",
			'BusinessPartnerContractAssignment', 
				CASE WHEN root."Shareholder_BusinessPartnerContractAssignment_ContractDataOwner" = true 
					THEN root."Shareholder_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID" END
		) AS "legalEntity",
		CASE WHEN root."Seller_BusinessPartnerContractAssignment_ContractDataOwner" = false 
			THEN root."Seller_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
		END AS "issuer",
		ABS(root."FinancialContract_NominalAmount"/IFNULL(root."FinancialContract_Quantity",1)) AS "marketValueObserved",
		:I_BUSINESS_DATE AS "marketValueDate",
		CASE WHEN root."Seller_BusinessPartnerContractAssignment_ContractDataOwner" = false 
			THEN root."Seller_BusinessPartnerContractAssignment_PartnerInParticipation_BusinessPartnerID"
		END AS "counterparty",
		ABS(root."FinancialContract_NominalAmount") AS "currentPrincipal", 
		root."FinancialContract_NominalAmountCurrency" AS "currency",
		root."FinancialContract_Quantity" AS "quantity",
		CASE 
			WHEN "FinancialContract_Quantity" IS NOT NULL THEN
				CASE
					WHEN "FinancialContract_Quantity" > 0 THEN
						TO_DECIMAL((ABS(root."FinancialContract_NominalAmount"/IFNULL(root."FinancialContract_Quantity",1)) * -1), 34, 6)
				END
		END AS "priceAtCDD",
		root."dataGroup",
		MAP(EURegRepForContract."EuropeanRegulatoryReportingForContract_OwnCapitalDesignation",
			'AdditionalTier1',		'CAPITAL_TIER_1',
			'CommonEquityTier1',	'CAPITAL_TIER_1',
			'Tier2',				'CAPITAL_TIER_2'
		) AS "contractFSType",
		productFSClass."productFSClass",
		"com.adweko.adapter.osx.inputdata.common::get_instrumentType"(
			root."StandardCatalog_ProductCatalogItem",
			NULL,
			NULL
		) AS "instrumentType",
		1 AS "isUnderLiquidityManagementControl",
		root."StandardCatalog_ProductCatalogItem",
		root."CustomCatalog_ProductCatalogItem",
		'Equity' AS "lmEntityName",
		MAP(writeOffKey."Value",
			'WriteDownAmountInPositionCurrency', writeOff."WriteDownAmountInPositionCurrency",
			'WriteDownAmount', writeOff."WriteDownAmount"
		) AS "writeOffAmount",
		bok."BookValue_BookValue" AS "bookValue",
		expop."exposureClass"

    FROM "com.adweko.adapter.osx.inputdata.equityInvestment::EquityInvestment_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP,:I_DATAGROUP_STRING) AS root

        LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_productFSClass_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS productFSClass
            ON productFSClass."ProductCatalogItem" = root."StandardCatalog_ProductCatalogItem"

        LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.creditrisk::BV_CreditRiskExposurePrio" (:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP, 'EqutiyInvestment') AS expop
			ON expop."CreditRiskExposure_FinancialContract_IDSystem" = root."FinancialContract_IDSystem"
			AND expop."CreditRiskExposure_FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_EuropeanRegulatoryReportingForContract"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS EURegRepForContract
			ON EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"
				AND EURegRepForContract."EuropeanRegulatoryReportingForContract_FinancialContract_IDSystem"			= root."FinancialContract_IDSystem"

    --writeOffAmount
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS writeOffKey
			ON writeOffKey."KeyID" = 'WriteDownPositionCurrenyOrAmount'

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_PriorityWriteOffFC"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP, 'Loan') AS writeOff
			ON writeOff."FinancialContract_FinancialContractID" = root."FinancialContract_FinancialContractID"
			AND writeOff."FinancialContract_IDSystem" = root."FinancialContract_IDSystem"

		LEFT OUTER JOIN "com.adweko.adapter.osx.inputdata.common::get_bookValue"(:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS bok
			ON bok."BookValue_FinancialContract_IDSystem"					= root."FinancialContract_IDSystem"
			AND bok."BookValue_FinancialContract_FinancialContractID"	= root."FinancialContract_FinancialContractID"
			AND bok."BookValue_BookValueCurrency"						= root."FinancialContract_NominalAmountCurrency"
    ;
END;
