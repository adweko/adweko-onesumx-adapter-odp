FUNCTION "com.adweko.adapter.osx.inputdata.amortizingShortGuarantee.amortizingShortGuaranteeUnderlyingArray::IP_afterStub"(
		I_BUSINESS_DATE DATE,
		I_SYSTEM_TIMESTAMP TIMESTAMP,
		I_DATAGROUP_STRING NVARCHAR(128)
		)
RETURNS TABLE (
		"id" NVARCHAR(32),
		"refid" NVARCHAR(32),
		"arraySpecKind" NVARCHAR(5),
		"cycleDate" DATE,
		"cyclePeriod" NVARCHAR(10),
		"ppAmount" DECIMAL(34, 6),
		"repricingType" NVARCHAR(1),
		"pricingMarketObject" NVARCHAR(40),
		"rateAdd" DECIMAL(15, 11),
		"rateMultiplier" DECIMAL(15, 11),
		"rateTerm" NVARCHAR(10),
		"fixingDays" NVARCHAR(10),
		"cap" DECIMAL(15, 11),
		"floor" DECIMAL(15, 11),
		"anchor" DATE,
		"amount" DECIMAL(34, 11),
		"ratingScale" NVARCHAR(75),
		"ratingClass" NVARCHAR(10),
		"dataGroup" NVARCHAR(128)
		) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS

BEGIN
	/*****************************
        Write your function logic
    ****************************/
RETURN

	SELECT
		"com.adweko.adapter.osx.inputdata.common::get_array_id"(
			"com.adweko.adapter.osx.inputdata.common::get_id"(root."contractID", root."node_No", root."dataGroup"),
			'IP',
			inte."FirstInterestPeriodEndDate" -- =cycleDate"
		) AS "id",
		"com.adweko.adapter.osx.inputdata.common::get_id"(root."contractID", root."node_No", root."dataGroup") AS "refid",
		'IP' AS "arraySpecKind",
		inte."FirstInterestPeriodEndDate" AS "cycleDate",
		"com.adweko.adapter.osx.inputdata.common::get_cyclePeriod" (
			inte."InterestPeriodLength",
			mcp."cyclePeriod"
		) AS "cyclePeriod",
		TO_DECIMAL(NULL, 34, 6) AS "ppAmount",
		NULL AS "repricingType",
		NULL AS "pricingMarketObject",
		TO_DECIMAL(NULL, 15, 11) AS "rateAdd",
		TO_DECIMAL(NULL, 15, 11) AS "rateMultiplier",
		NULL AS "rateTerm",
		NULL AS "fixingDays",
		TO_DECIMAL(NULL, 15, 11) AS "cap",
		TO_DECIMAL(NULL, 15, 11) AS "floor",
		NULL AS "anchor",
		TO_DECIMAL(NULL, 34, 11) AS "amount",
		NULL AS "ratingScale",
		NULL AS "ratingClass",
		root."dataGroup"

	FROM "com.adweko.adapter.osx.inputdata.amortizingShortGuarantee::AmortizingShortGuaranteeUnderlying_Root_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP, :I_DATAGROUP_STRING) AS root
	
	INNER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS kvInterestType
		ON kvInterestType."KeyID" = 'AmortizingShortGuarantee_InterestType'
	
	INNER JOIN "com.adweko.adapter.osx.inputdata.risk::BV_Interest"(:I_BUSINESS_DATE,:I_SYSTEM_TIMESTAMP) AS inte
        ON inte."FinancialContractID"				= root."FinancialContract_FinancialContractID"
       	AND inte."IDSystem" 						= root."FinancialContract_IDSystem"
		AND inte."InterestCategory"					= 'InterestPeriodSpecification'
		AND inte."InterestType" 					= kvInterestType."Value"
		AND inte."FirstInterestPeriodStartDate"		<= :I_BUSINESS_DATE
		AND	IFNULL("LastInterestPeriodEndDate",'9999-12-30') >= :I_BUSINESS_DATE
		AND inte."FirstInterestPeriodEndDate"		IS NOT NULL
		AND	(
			inte."LastInterestPeriodEndDate"		IS NULL
			OR
			inte."LastInterestPeriodEndDate"		> :I_BUSINESS_DATE
			AND inte."LastInterestPeriodEndDate"    > inte."FirstInterestPeriodEndDate"
		)
		AND inte."InterestPeriodLength"				IS NOT NULL
		AND inte."InterestPeriodTimeUnit"			IS NOT NULL
	
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_cyclePeriod_View" (:I_BUSINESS_DATE, :I_SYSTEM_TIMESTAMP) AS mcp
		ON  mcp."BusinesscyclePeriod" = inte."InterestPeriodTimeUnit";
END;
