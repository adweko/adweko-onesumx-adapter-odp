PROCEDURE "com.adweko.adapter.osx.configuration::Load_CodeListMapping"( 
	IN I_CodeListMappingId varchar(256),
	IN I_DeleteCustomValues boolean,
	IN I_CheckExistence boolean,
	OUT O_ReturnCode integer
)
   LANGUAGE SQLSCRIPT
   SQL SECURITY INVOKER AS
BEGIN
   /*************************************
       Loading procedure name will be called within the folder "com.adweko.adapter.osx.configuration"
       The entry from MappingConfiguration is then updated to "IsInitial" = 'false' and returncode 1 (success) is returned
       If the procedure is not found with "CheckProcedureExist", then 0 is returned
       
       @I_CodeListMappingId 
       Is the entry from the MappingConfiguration table and represents the procedure ID
       @I_DeleteCustomValues
       Just passing the paramter to the procedure
       @I_CheckExistence
       Check if CodeListMappingId is a valid procedure name. If true, ReturnCode will always be 1
       Because of calls outside of the adapter project, CheckProcedureExist cannot be used because the CURRENT_SCHEMA is different
      
       @O_ReturnCode
       returns 0 if called procedure does not exist
   *************************************/
   	--DECLARE v_callStmnt NVARCHAR(256);
	DECLARE v_procName NVARCHAR(256);
	DECLARE v_exist boolean;
	DECLARE mycond CONDITION FOR SQL_ERROR_CODE 10001;
	
	IF is_sql_injection_safe(I_CodeListMappingId) <> 1 THEN
		SIGNAL mycond SET MESSAGE_TEXT = 'The mapping id parameter passed carries the risk of an SQL injection';
	END IF;
	
	SELECT 
		CASE "UseMapping2"
			WHEN 'true'
			THEN ESCAPE_DOUBLE_QUOTES(ESCAPE_SINGLE_QUOTES("LoadProcedureName2"))
			ELSE ESCAPE_DOUBLE_QUOTES(ESCAPE_SINGLE_QUOTES("LoadProcedureName"))
		END AS "ProcName" INTO v_procName
	FROM "com.adweko.adapter.osx.configuration::MappingConfiguration" 
	WHERE "Id" = :I_CodeListMappingId;
	
	IF :v_procName <> '' THEN
		--IF is_sql_injection_safe(:v_procName) <> 1 THEN
			--SIGNAL mycond SET MESSAGE_TEXT = 'The procedure name related to the mapping ID '''||I_CodeListMappingId||''' carries the risk of an SQL injection';
		--END IF;

		IF :I_CheckExistence = TRUE THEN
	        v_exist = "com.adweko.adapter.osx.sys::check_ProcedureExist"(:v_procName);
		ELSE
			v_exist = TRUE;
		END IF;
		
		IF :v_exist = TRUE THEN
			EXECUTE IMMEDIATE 'CALL "'||:v_procName||'"(?)' USING :I_DeleteCustomValues;
			
			UPDATE "com.adweko.adapter.osx.configuration::MappingConfiguration"
				SET "IsInitial" = 'false'
				WHERE "Id" = :I_CodeListMappingId;
				
			O_ReturnCode := 1;
		ELSE
			O_ReturnCode := 0;
		END IF;
	END IF;
END
