--trigger for logging of changes on mapping table <map_OTCDerivativeContractCategory>
TRIGGER "com.adweko.adapter.osx.mappings::map_OTCDerivativeContractCategory_Trigger" 
--set action that will trigger an update of the log table
BEFORE INSERT OR UPDATE OR DELETE ON "com.adweko.adapter.osx.mappings::map_OTCDerivativeContractCategory"
--set variables referencing the mapping table for the old and the new data. These can be accessed to log the details of the change and add the User name of the last change in the mapping table itself  
REFERENCING NEW ROW new_row, OLD ROW old_row 
FOR EACH ROW
BEGIN 
	--Read the application user. In WEB IDE this is euqal to the logged in user, via JDBC it is the OS user calling the JDBC driver.
	DECLARE v_applicationUserName NVARCHAR(256) = SESSION_CONTEXT('APPLICATIONUSER');
	--Derive wether current action is an update, insert or delete
	DECLARE v_insertOrUpdate NVARCHAR(256) = CASE 
		WHEN :old_row."BusinessValidFrom" IS NOT NULL AND :new_row."BusinessValidFrom" IS NULL THEN 'DELETE' 
		WHEN :old_row."BusinessValidFrom" IS NULL AND :new_row."BusinessValidFrom" IS NOT NULL THEN 'INSERT' 
		ELSE 'UPDATE' 
	END;
	DECLARE v_sysTimestamp TIMESTAMP = NOW();

	--add logging to the new row in mapping table
	new_row."LastChangeTimestamp"					=	:v_sysTimestamp;
	new_row."LastChangeApplicationUserName"			=	:v_applicationUserName;

	--insert an entry for the current row in the log table
	INSERT INTO "com.adweko.adapter.osx.configuration::LOG_MAPPING_DATA" (
		"SYSTEM_TIMESTAMP",
		"OBJECT_NAME",
		"STATEMENT_TYPE",
		"TECHNICAL_USER_NAME",
		"APPLICATION_USER_NAME",
		"COMMENT")
	VALUES (
		:v_sysTimestamp,
		'"com.adweko.adapter.osx.mappings::map_OTCDerivativeContractCategory"',
		:v_insertOrUpdate,
		current_user,
		:v_applicationUserName,
		--log key of affected row. details can be seen by checking the corresponding history table
		CASE 
			WHEN :v_insertOrUpdate IN ('UPDATE','DELETE')
				THEN 'executed on ROW with Key: ' 
					|| 'OTCDerivativeContractCategory= '		|| COALESCE(:old_row."OTCDerivativeContractCategory",'NULL')
					|| ',BusinessValidFrom= '					|| COALESCE(TO_NVARCHAR(:old_row."BusinessValidFrom"),'NULL') 
					|| ',BusinessValidTo= '						|| COALESCE(TO_NVARCHAR(:old_row."BusinessValidTo"),'NULL') 
			--inserts
			ELSE 'new ROW with Key: ' 
					|| 'OTCDerivativeContractCategory= '		|| COALESCE(:new_row."OTCDerivativeContractCategory",'NULL')
					|| ',BusinessValidFrom= '					|| COALESCE(TO_NVARCHAR(:new_row."BusinessValidFrom"),'NULL') 
					|| ',BusinessValidTo= '						|| COALESCE(TO_NVARCHAR(:new_row."BusinessValidTo"),'NULL') 
		END
	);
END;