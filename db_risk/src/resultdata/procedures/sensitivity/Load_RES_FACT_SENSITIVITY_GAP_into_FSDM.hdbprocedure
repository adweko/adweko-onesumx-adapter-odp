PROCEDURE "com.adweko.adapter.osx.procedures::Load_RES_FACT_SENSITIVITY_GAP_into_FSDM"(
	IN I_SOLVE_JOB_ID BIGINT,
	IN I_MODEL_ID BIGINT
	)	
	LANGUAGE SQLSCRIPT SQL
	SECURITY INVOKER AS
BEGIN
   /*************************************
       Write your procedure logic 
       Load result data from RES_FACT_SENSITIVITY_GAP into Sensitivity Gap
   *************************************/
	DECLARE tab_out_proc_sensgap "com.adweko.adapter.osx.synonyms::SensitivityGapTT";
	DECLARE tab_out_proc_margin "com.adweko.adapter.osx.synonyms::MarginTT";
   
	tab_in_proc_sensgap = SELECT 
		"MarketRiskAnalysisType",
		"MarketRiskSplitPartType",
		"RiskProvisionScenario",
		"RoleOfCurrency",
		"_DynamicTimeBucket.MaturityBandID",
		"_DynamicTimeBucket.TimeBucketID",
		"_FinancialContract.FinancialContractID",
		"_FinancialContract.IDSystem",
		"_FinancialInstrument.FinancialInstrumentID",
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		"_RiskReportingNode.RiskReportingNodeID",
		"_SecuritiesAccount.FinancialContractID",
		"_SecuritiesAccount.IDSystem",
		"_TimeBucket.MaturityBandID",
		"_TimeBucket.TimeBucketID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"Currency",
		"DynamicTimeBucketEndDate",
		"DynamicTimeBucketStartDate",
		"GapValue",
		"GapValueInternalPureView",
		"GapValueInternalTreasuryView",
		"TimeBucketEndDate",
		"TimeBucketStartDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID"
	FROM "com.adweko.adapter.osx.sensitivity::get_RES_FACT_SENSITIVITY_GAP_Mapping"(:I_SOLVE_JOB_ID, :I_MODEL_ID);
	
	IF ::ROWCOUNT > 0 --usage of system variable in order to save a variable
		THEN :tab_out_proc_sensgap.(
									"MarketRiskAnalysisType",
									"MarketRiskSplitPartType",
									"RiskProvisionScenario",
									"RoleOfCurrency",
									"_DynamicTimeBucket.MaturityBandID",
									"_DynamicTimeBucket.TimeBucketID",
									"_FinancialContract.FinancialContractID",
									"_FinancialContract.IDSystem",
									"_FinancialInstrument.FinancialInstrumentID",
									"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
									"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
									"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
									"_ResultGroup.ResultDataProvider",
									"_ResultGroup.ResultGroupID",
									"_RiskReportingNode.RiskReportingNodeID",
									"_SecuritiesAccount.FinancialContractID",
									"_SecuritiesAccount.IDSystem",
									"_TimeBucket.MaturityBandID",
									"_TimeBucket.TimeBucketID",
									"BusinessValidFrom",
									"BusinessValidTo",
									"Currency",
									"DynamicTimeBucketEndDate",
									"DynamicTimeBucketStartDate",
									"GapValue",
									"GapValueInternalPureView",
									"GapValueInternalTreasuryView",
									"TimeBucketEndDate",
									"TimeBucketStartDate",
									"SourceSystemID",
									"ChangeTimestampInSourceSystem",
									"ChangingUserInSourceSystem",
									"ChangingProcessType",
									"ChangingProcessID")
									.INSERT(:tab_in_proc_sensgap);
		CALL "com.adweko.adapter.osx.synonyms::SensitivityGapLoad"(ROW => :tab_out_proc_sensgap);
	END IF;
	
	tab_in_proc_margin = SELECT 
		"MarginCategory",
		"RoleOfCurrency",
		"Scenario",
		"SplittingTranche",
		"_BusinessPartner.BusinessPartnerID",
		"_DynamicTimeBucket.MaturityBandID",
		"_DynamicTimeBucket.TimeBucketID",
		"_FinancialContract.FinancialContractID",
		"_FinancialContract.IDSystem",
		"_FinancialInstrument.FinancialInstrumentID",
		"_OrganizationalUnit.IDSystem",
		"_OrganizationalUnit.OrganizationalUnitID",
		"_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
		"_ProductCatalogItem.ProductCatalogItem",
		"_ProductCatalogItem._ProductCatalog.CatalogID",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		"_RiskReportingNode.RiskReportingNodeID",
		"_SecuritiesAccount.FinancialContractID",
		"_SecuritiesAccount.IDSystem",
		"_TimeBucket.MaturityBandID",
		"_TimeBucket.TimeBucketID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"Bindingness",
		"CalculationReason",
		"DynamicTimeBucketEndDate",
		"DynamicTimeBucketStartDate",
		"MarginAmount",
		"MarginAmountCurrency",
		"MarginRate",
		"OverallCalculationMethod",
		"TimeBucketEndDate",
		"TimeBucketStartDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType" ,
		"ChangingProcessID"
	FROM "com.adweko.adapter.osx.sensitivity::get_RES_FACT_SENSITIVITY_GAP_FTP_Mapping"(:I_SOLVE_JOB_ID, :I_MODEL_ID);
	
   	IF ::ROWCOUNT > 0 
		THEN :tab_out_proc_margin.(
			"MarginCategory",
			"RoleOfCurrency",
			"Scenario",
			"SplittingTranche",
			"_BusinessPartner.BusinessPartnerID",
			"_DynamicTimeBucket.MaturityBandID",
			"_DynamicTimeBucket.TimeBucketID",
			"_FinancialContract.FinancialContractID",
			"_FinancialContract.IDSystem",
			"_FinancialInstrument.FinancialInstrumentID",
			"_OrganizationalUnit.IDSystem",
			"_OrganizationalUnit.OrganizationalUnitID",
			"_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
			"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
			"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
			"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
			"_ProductCatalogItem.ProductCatalogItem",
			"_ProductCatalogItem._ProductCatalog.CatalogID",
			"_ResultGroup.ResultDataProvider",
			"_ResultGroup.ResultGroupID",
			"_RiskReportingNode.RiskReportingNodeID",
			"_SecuritiesAccount.FinancialContractID",
			"_SecuritiesAccount.IDSystem",
			"_TimeBucket.MaturityBandID",
			"_TimeBucket.TimeBucketID",
			"BusinessValidFrom",
			"BusinessValidTo",
			"Bindingness",
			"CalculationReason",
			"DynamicTimeBucketEndDate",
			"DynamicTimeBucketStartDate",
			"MarginAmount",
			"MarginAmountCurrency",
			"MarginRate",
			"OverallCalculationMethod",
			"TimeBucketEndDate",
			"TimeBucketStartDate",
			"SourceSystemID",
			"ChangeTimestampInSourceSystem",
			"ChangingUserInSourceSystem",
			"ChangingProcessType" ,
			"ChangingProcessID")
				.INSERT(:tab_in_proc_margin);
		CALL "com.adweko.adapter.osx.synonyms::MarginLoad"(ROW => :tab_out_proc_margin);
	END IF;
END
