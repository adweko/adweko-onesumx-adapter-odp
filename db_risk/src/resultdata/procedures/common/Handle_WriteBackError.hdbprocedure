PROCEDURE "com.adweko.adapter.osx.procedures::Handle_WriteBackError"(
	IN I_SOLVE_JOB_ID BIGINT,
   	IN I_MODEL_ID BIGINT,
   	IN I_OBJECT_NAME NVARCHAR(512),
    IN I_STATUS_CODE NVARCHAR(10),
    IN I_STATUS_MESSAGE NVARCHAR(1000),
    IN I_COMMENT NVARCHAR(1000) DEFAULT ''
)
	LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS
BEGIN
   /*************************************
       Write your procedure logic 
   *************************************/
   CALL "com.adweko.adapter.osx.procedures::Log_Error"(
   		:I_SOLVE_JOB_ID,
   		:I_MODEL_ID,
	   	:I_OBJECT_NAME,
	    :I_STATUS_CODE,
	    :I_STATUS_MESSAGE,
	    :I_COMMENT
   	);

   	tab_log = SELECT
   			"SYSTEM_TIMESTAMP",
		    "SLV_JOB_ID",
		    "MODEL_ID",
		    "TYPE",
		    "OBJECT_NAME",
		    "USER_NAME",
		    "STATUS_CODE",
		    "STATUS_MESSAGE",
		    "COMMENT",
		    "SRC_RESULT_TABLE_NAME",
        	"APPLICATION_USER_NAME"
   		FROM
   			"com.adweko.adapter.osx.tables.tmp::RESULT_DATA_LOG_TMP";
	ROLLBACK;
	
	--Autocommits are activated for sessions by default, so that the system takes care of commits and rollbacks independently. 
	--In the event of an exception, all changes made within the transaction are reversed. 
	--In order to keep the log for the run, an autonomous transaction is started in which the log is independently committed.
	--However, the use of an autonomous transaction starts a separate session which has no access to the entries in the global temporary log. 
	--For this reason the log is cached in a variable before.
   	BEGIN AUTONOMOUS TRANSACTION
   		INSERT INTO "com.adweko.adapter.osx::RESULT_DATA_LOG"(
   			"SYSTEM_TIMESTAMP",
		    "SLV_JOB_ID",
		    "MODEL_ID",
		    "TYPE",
		    "OBJECT_NAME",
		    "USER_NAME",
		    "STATUS_CODE",
		    "STATUS_MESSAGE",
		    "COMMENT",
		    "SRC_RESULT_TABLE_NAME",
        	"APPLICATION_USER_NAME"
   		)
   		SELECT * FROM :tab_log;
	    COMMIT;
	END;
	
	SET 'PROCESSING_FAILED' = true;	
END