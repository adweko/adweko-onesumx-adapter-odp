PROCEDURE "com.adweko.adapter.osx.procedures::Set_ContractInformationCache"(
    IN I_SOLVE_JOB_ID BIGINT,
    IN I_MODEL_ID BIGINT,
    IN I_TABLE_NAME NVARCHAR(128)
) LANGUAGE SQLSCRIPT SQL SECURITY INVOKER AS
BEGIN
   /*************************************
       Caches and derives all relevant contract information belonging to the given Solve job Model and result source table name
   *************************************/
    DECLARE tab_contract_info_tmp TABLE (
    	"CONTRACT_ID" BIGINT,
		"_FinancialContract.FinancialContractID" NVARCHAR(128),
		"_FinancialInstrument.FinancialInstrumentID" NVARCHAR(128),
		"IS_SYNTHETIC" BOOLEAN
    );
    DECLARE v_tab_name String;

	TRUNCATE TABLE "com.adweko.adapter.osx.tables.tmp::RES_CONTRACT_INFO_TMP";
	
    SELECT 
		"TABLE_NAME" 
	INTO v_tab_name
	FROM "com.adweko.adapter.osx.synonyms::TABLES"
	WHERE
	--	"SCHEMA_NAME" <> 'OSX_ACCESS'
	--	AND 
		"TABLE_NAME" LIKE 
		--'%::'||
		:I_TABLE_NAME;
	
   	CALL "com.adweko.adapter.osx.procedures::DeriveContractInformation_dynamically"( 
		:I_SOLVE_JOB_ID,
		:I_MODEL_ID,
		:v_tab_name,
		:tab_contract_info_tmp
	);
	
	INSERT INTO 
		"com.adweko.adapter.osx.tables.tmp::RES_CONTRACT_INFO_TMP"
	SELECT DISTINCT
		tmp."CONTRACT_ID",
		tmp."_FinancialContract.FinancialContractID" AS "FinancialContractID",
		tmp."_FinancialInstrument.FinancialInstrumentID" AS "FinancialInstrumentID",
		tmp."IS_SYNTHETIC",
		:I_SOLVE_JOB_ID AS "SOLVE_JOB_ID",
		:I_MODEL_ID AS "MODEL_ID"
	FROM 
		:tab_contract_info_tmp AS tmp
	WHERE 
		tmp."CONTRACT_ID" NOT IN (
			SELECT "CONTRACT_ID"
			FROM "com.adweko.adapter.osx.tables.tmp::RES_CONTRACT_INFO_TMP"
		);
END