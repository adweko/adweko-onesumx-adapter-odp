PROCEDURE "com.adweko.adapter.osx.procedures::Delete_ResultGroupResultData_from_FSDM"( 	
	IN I_SOLVE_JOB_ID BIGINT,
	IN I_MODEL_ID BIGINT
)
   LANGUAGE SQLSCRIPT
   SQL SECURITY INVOKER AS
BEGIN
   /*************************************
        Deletes all result data related Result Groups belonging to the given Solve job and Model Id 
   *************************************/
    DECLARE tab_in_proc_res "com.adweko.adapter.osx.synonyms::ResultGroupTT_Del";
    DECLARE v_resGrProvider NVARCHAR(256) = "com.adweko.adapter.osx.resultdata.common::get_ResultProvider"(:I_SOLVE_JOB_ID, :I_MODEL_ID);
    DECLARE v_resGrId NVARCHAR(128) = "com.adweko.adapter.osx.resultdata.common::get_ResulGroupId"(:I_SOLVE_JOB_ID, :I_MODEL_ID);
    
    DECLARE cols_ResGr NVARCHAR (1500);
    DECLARE EXIT HANDLER FOR sqlexception
	BEGIN
	   	CALL "com.adweko.adapter.osx.procedures::Handle_WriteBackError"(
			I_SOLVE_JOB_ID => :I_SOLVE_JOB_ID,
		   	I_MODEL_ID => :I_MODEL_ID,
		   	I_OBJECT_NAME => ::CURRENT_OBJECT_NAME,
		    I_STATUS_CODE => ::SQL_ERROR_CODE,
		    I_STATUS_MESSAGE => ::SQL_ERROR_MESSAGE
		);
	END;

	SELECT STRING_AGG('"' || innerselect."COLUMN_NAME" || '"',', ') INTO cols_ResGr FROM 
		(SELECT col."COLUMN_NAME",col."POSITION" FROM "com.adweko.adapter.osx.synonyms::TABLE_COLUMNS" AS col
			INNER JOIN "com.adweko.adapter.osx.synonyms::SYNONYMS" AS syn  --it is required to join sys synonym table to filter on the correct FSDM Schema used for current adapter schema since TABLE_COLUMNS contains all schemas that are visible for user.
			ON 	syn."OBJECT_SCHEMA" = col."SCHEMA_NAME"
			WHERE col."TABLE_NAME"	= 'sap.fsdm.tabletypes::ResultGroupTT_Del'
			AND syn."SYNONYM_NAME"  = 'com.adweko.adapter.osx.synonyms::ResultGroupTT_Del'
			AND syn."SCHEMA_NAME"	=	CURRENT_SCHEMA 
			ORDER BY col."POSITION") as innerselect
	;

	EXECUTE IMMEDIATE 'SELECT ' || :cols_ResGr || ' FROM "com.adweko.adapter.osx.synonyms::ResultGroup"'
						||' WHERE "ResultGroupID" = ?'
  						||' AND "ResultDataProvider" = ?'
						INTO tab_in_proc_res
						USING :v_resGrId, :v_resGrProvider;

   	IF ::ROWCOUNT > 0 --usage of system variable in order to save a variable
	THEN
		CALL "com.adweko.adapter.osx.procedures::Log_Info"(
			:I_SOLVE_JOB_ID,
			:I_MODEL_ID,
			'Deleting Results from FSDM Table ResultGroup. Number of rows: ' || ::ROWCOUNT
		);
		CALL "com.adweko.adapter.osx.synonyms::ResultGroupDelete"(:tab_in_proc_res);
	END IF;
END