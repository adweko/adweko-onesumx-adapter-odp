PROCEDURE "com.adweko.adapter.osx.procedures::Load_RES_FACT_BACKTESTED_VAR_into_FSDM"(
	IN I_SOLVE_JOB_ID BIGINT,
	IN I_MODEL_ID BIGINT
	) 
	LANGUAGE SQLSCRIPT 
	SQL SECURITY INVOKER AS
BEGIN
   /*************************************
       Write your procedure logic 
   *************************************/
   DECLARE tab_out_proc_back_var_interim "com.adweko.adapter.osx.synonyms::ValueAtRiskBacktestingInterimResultTT";
   DECLARE tab_out_proc_back_var_result "com.adweko.adapter.osx.synonyms::ValueAtRiskResultTT";
   
	tab_in_proc_back_var_interim = SELECT 
			"ProfitAndLossValueDate",
			"ProfitAndLossValueType",
			"_FinancialContract.FinancialContractID",
			"_FinancialContract.IDSystem",
			"_FinancialInstrument.FinancialInstrumentID",
			"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
			"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
			"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
			"_ResultGroup.ResultDataProvider",
			"_ResultGroup.ResultGroupID",
			"_SecuritiesAccount.FinancialContractID",
			"_SecuritiesAccount.IDSystem",
			"_ValueAtRiskResult.ConfidenceLevel",
			"_ValueAtRiskResult.RiskGroup",
			"_ValueAtRiskResult.RiskProvisionScenario",
			"_ValueAtRiskResult.RoleOfCurrency",
			"_ValueAtRiskResult._ResultGroup.ResultDataProvider",
			"_ValueAtRiskResult._ResultGroup.ResultGroupID",
			"_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
			"BusinessValidFrom",
			"BusinessValidTo",
			"Currency",
			"HoldingPeriod",
			"HoldingPeriodTimeUnit",
			"NetPresentValue",
			"NetPresentValueAtEndOfHoldingPeriod",
			"OptionNetPresentValue",
			"OptionNetPresentValueAtEndOfHoldingPeriod",
			"ProfitAndLossValue",
			"ValueAtRiskCalculationMethod",
			"SourceSystemID",
			"ChangeTimestampInSourceSystem",
			"ChangingUserInSourceSystem",
			"ChangingProcessType",
			"ChangingProcessID"
	
	FROM "com.adweko.adapter.osx.valueatrisk::get_RES_FACT_BACKTESTED_VAR_Interim_Mapping"(	:I_SOLVE_JOB_ID, :I_MODEL_ID);
	
	IF ::ROWCOUNT > 0 --usage of system variable in order to save a variable
		THEN :tab_out_proc_back_var_interim.(
			"ProfitAndLossValueDate",
			"ProfitAndLossValueType",
			"_FinancialContract.FinancialContractID",
			"_FinancialContract.IDSystem",
			"_FinancialInstrument.FinancialInstrumentID",
			"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
			"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
			"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
			"_ResultGroup.ResultDataProvider",
			"_ResultGroup.ResultGroupID",
			"_SecuritiesAccount.FinancialContractID",
			"_SecuritiesAccount.IDSystem",
			"_ValueAtRiskResult.ConfidenceLevel",
			"_ValueAtRiskResult.RiskGroup",
			"_ValueAtRiskResult.RiskProvisionScenario",
			"_ValueAtRiskResult.RoleOfCurrency",
			"_ValueAtRiskResult._ResultGroup.ResultDataProvider",
			"_ValueAtRiskResult._ResultGroup.ResultGroupID",
			"_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
			"BusinessValidFrom",
			"BusinessValidTo",
			"Currency",
			"HoldingPeriod",
			"HoldingPeriodTimeUnit",
			"NetPresentValue",
			"NetPresentValueAtEndOfHoldingPeriod",
			"OptionNetPresentValue",
			"OptionNetPresentValueAtEndOfHoldingPeriod",
			"ProfitAndLossValue",
			"ValueAtRiskCalculationMethod",
			"SourceSystemID",
			"ChangeTimestampInSourceSystem",
			"ChangingUserInSourceSystem",
			"ChangingProcessType",
			"ChangingProcessID")
		.INSERT(:tab_in_proc_back_var_interim);
		CALL "com.adweko.adapter.osx.synonyms::ValueAtRiskBacktestingInterimResultLoad"(ROW => :tab_out_proc_back_var_interim);
	END IF;
	
	tab_in_proc_back_var_result = SELECT 
			"ConfidenceLevel",
			"RiskGroup",
			"RiskProvisionScenario",
			"RoleOfCurrency",
			"_ResultGroup.ResultDataProvider",
			"_ResultGroup.ResultGroupID",
			"_RiskReportingNode.RiskReportingNodeID",
			"BusinessValidFrom",
			"BusinessValidTo",
			"Currency",
			"DecayFactor",
			"DeltaVariance",
			"ExpectedShortfall",
			"GammaVariance",
			"HistoricalPeriodEndDate",
			"HistoricalPeriodStartDate",
			"HoldingPeriod",
			"HoldingPeriodTimeUnit",
			"LowerBoundaryValue",
			"MeanNetPresentValue",
			"NumberOfSimulationRuns",
			"OverlapReturnHorizonPeriod",
			"OverlapReturnHorizonPeriodTimeUnit",
			"ReturnHorizonPeriod",
			"ReturnHorizonTimeUnit" ,
			"SimulatedMarketDataCreationMethod",
			"ThirdMomentDelta",
			"ThirdMomentGamma",
			"UpperBoundaryValue",
			"ValueAtRisk",
			"ValueAtRiskCalculationMethod",
			"ValueAtRiskResultCategory",
			"SourceSystemID",
			"ChangeTimestampInSourceSystem",
			"ChangingUserInSourceSystem",
			"ChangingProcessType",
			"ChangingProcessID"
	
	FROM "com.adweko.adapter.osx.valueatrisk::get_RES_FACT_BACKTESTED_VAR_Result_Mapping"(:I_SOLVE_JOB_ID, :I_MODEL_ID);
	
	IF ::ROWCOUNT > 0 --usage of system variable in order to save a variable
		THEN :tab_out_proc_back_var_result.(
			"ConfidenceLevel",
			"RiskGroup",
			"RiskProvisionScenario",
			"RoleOfCurrency",
			"_ResultGroup.ResultDataProvider",
			"_ResultGroup.ResultGroupID",
			"_RiskReportingNode.RiskReportingNodeID",
			"BusinessValidFrom",
			"BusinessValidTo",
			"Currency",
			"DecayFactor",
			"DeltaVariance",
			"ExpectedShortfall",
			"GammaVariance",
			"HistoricalPeriodEndDate",
			"HistoricalPeriodStartDate",
			"HoldingPeriod",
			"HoldingPeriodTimeUnit",
			"LowerBoundaryValue",
			"MeanNetPresentValue",
			"NumberOfSimulationRuns",
			"OverlapReturnHorizonPeriod",
			"OverlapReturnHorizonPeriodTimeUnit",
			"ReturnHorizonPeriod",
			"ReturnHorizonTimeUnit",
			"SimulatedMarketDataCreationMethod",
			"ThirdMomentDelta",
			"ThirdMomentGamma",
			"UpperBoundaryValue",
			"ValueAtRisk",
			"ValueAtRiskCalculationMethod",
			"ValueAtRiskResultCategory",
			"SourceSystemID",
			"ChangeTimestampInSourceSystem",
			"ChangingUserInSourceSystem",
			"ChangingProcessType",
			"ChangingProcessID")
		.INSERT(:tab_in_proc_back_var_result);
		CALL "com.adweko.adapter.osx.synonyms::ValueAtRiskResultLoad"(ROW => :tab_out_proc_back_var_result);
	END IF;
END