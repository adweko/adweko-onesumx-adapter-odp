FUNCTION "com.adweko.adapter.osx.sensitivity::get_RES_FACT_VOLA_EXP_Mapping"(
	IN I_SOLVE_JOB_ID BIGINT,
	IN I_MODEL_ID BIGINT
	)
       RETURNS TABLE(
       	"RES_FACT_VOLA_EXP_ID" BIGINT,
		"MarketRiskAnalysisType" NVARCHAR (100),
		"MarketRiskSplitPartType" NVARCHAR (100),
		"RiskProvisionScenario" NVARCHAR (100),
		"RoleOfCurrency" NVARCHAR (40),
		"_FinancialContract.FinancialContractID" NVARCHAR (128),
		"_FinancialContract.IDSystem" NVARCHAR (40),
		"_FinancialInstrument.FinancialInstrumentID" NVARCHAR (128),
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" NVARCHAR(3),
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" NVARCHAR(128),
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" NVARCHAR(40),
		"_ResultGroup.ResultDataProvider" NVARCHAR (256),
		"_ResultGroup.ResultGroupID" NVARCHAR (128),
		"_RiskReportingNode.RiskReportingNodeID" NVARCHAR (128),
		"_SecuritiesAccount.FinancialContractID" NVARCHAR (128),
		"_SecuritiesAccount.IDSystem" NVARCHAR (40),
		"_TimeBucket.MaturityBandID" NVARCHAR (128),
		"_TimeBucket.TimeBucketID" NVARCHAR (128),
		"BusinessValidFrom" DATE ,
		"BusinessValidTo" DATE ,
		"Currency" NVARCHAR (3),
		"ExposureValue" DECIMAL (34,6),
		"NetPresentValue" DECIMAL (34,6),
		"SecuritizationDelta" DECIMAL (34,6),
		"TimeBucketEndDate" DATE,
		"TimeBucketStartDate" DATE,
		"SourceSystemID" NVARCHAR (128),
		"ChangeTimestampInSourceSystem" TIMESTAMP ,
		"ChangingUserInSourceSystem" NVARCHAR (128),
		"ChangingProcessType" NVARCHAR (40),
		"ChangingProcessID" NVARCHAR (128)
		) 
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN
	SELECT
	MIN("RES_FACT_VOLA_EXP_ID") AS "RES_FACT_VOLA_EXP_ID",
	"MarketRiskAnalysisType",
	"MarketRiskSplitPartType",
	"RiskProvisionScenario",
	"RoleOfCurrency",
	"_FinancialContract.FinancialContractID",
	"_FinancialContract.IDSystem",
	"_FinancialInstrument.FinancialInstrumentID",
	"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
	"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
	"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
	"_ResultGroup.ResultDataProvider",
	"_ResultGroup.ResultGroupID",
	"_RiskReportingNode.RiskReportingNodeID",
	"_SecuritiesAccount.FinancialContractID",
	"_SecuritiesAccount.IDSystem",
	"_TimeBucket.MaturityBandID",
	"_TimeBucket.TimeBucketID",
	"BusinessValidFrom",
	"BusinessValidTo",
	"Currency",
	SUM("ExposureValue") AS "ExposureValue",
	SUM("NetPresentValue") AS "NetPresentValue",
	SUM("SecuritizationDelta") AS "SecuritizationDelta",
	"TimeBucketEndDate",
	"TimeBucketStartDate",
	"SourceSystemID",
	"ChangeTimestampInSourceSystem",
	"ChangingUserInSourceSystem",
	"ChangingProcessType",
	"ChangingProcessID"
	FROM(
		SELECT
		"RES_FACT_VOLA_EXP_ID",
		"MarketRiskAnalysisType",
		"MarketRiskSplitPartType",
		"RiskProvisionScenario",
		"RoleOfCurrency",
		"_FinancialContract.FinancialContractID",
		"_FinancialContract.IDSystem",
		"_FinancialInstrument.FinancialInstrumentID",
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		-- pivot for ChartOfAccounts and ClassificationTree
		MAP(ser3."ELEMENT_NUMBER",
			1, "Account_RiskReportingNode.RiskReportingNodeID",
			2, "Classification01_RiskReportingNode.RiskReportingNodeID",
			3, "Classification02_RiskReportingNode.RiskReportingNodeID",
			4, "Classification03_RiskReportingNode.RiskReportingNodeID",
			5, "Classification04_RiskReportingNode.RiskReportingNodeID",
			6, "Classification05_RiskReportingNode.RiskReportingNodeID",
			7, "Classification06_RiskReportingNode.RiskReportingNodeID",
			8, "Classification07_RiskReportingNode.RiskReportingNodeID",
			9, "Classification08_RiskReportingNode.RiskReportingNodeID",
			10, "Classification09_RiskReportingNode.RiskReportingNodeID",
			11, "Classification10_RiskReportingNode.RiskReportingNodeID",
			12, "Classification11_RiskReportingNode.RiskReportingNodeID",
			13, "Classification12_RiskReportingNode.RiskReportingNodeID",
			14, "Classification13_RiskReportingNode.RiskReportingNodeID",
			15, "Classification14_RiskReportingNode.RiskReportingNodeID",
			16, "Classification15_RiskReportingNode.RiskReportingNodeID")
		AS "_RiskReportingNode.RiskReportingNodeID",
		"_SecuritiesAccount.FinancialContractID",
		"_SecuritiesAccount.IDSystem",
		"_TimeBucket.MaturityBandID",
		"_TimeBucket.TimeBucketID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"Currency",
		"ExposureValue",
		"NetPresentValue",
		"SecuritizationDelta",
		"TimeBucketEndDate",
		"TimeBucketStartDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID"
	FROM(
		SELECT
		"RES_FACT_VOLA_EXP_ID",
		"MarketRiskAnalysisType",
		"MarketRiskSplitPartType",
		"RiskProvisionScenario",
		"RoleOfCurrency",
		"_FinancialContract.FinancialContractID",
		"_FinancialContract.IDSystem",
		"_FinancialInstrument.FinancialInstrumentID",
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		"Account_RiskReportingNode.RiskReportingNodeID",
		"Classification01_RiskReportingNode.RiskReportingNodeID",
		"Classification03_RiskReportingNode.RiskReportingNodeID",
		"Classification02_RiskReportingNode.RiskReportingNodeID",
		"Classification04_RiskReportingNode.RiskReportingNodeID",
		"Classification05_RiskReportingNode.RiskReportingNodeID",
		"Classification06_RiskReportingNode.RiskReportingNodeID",
		"Classification07_RiskReportingNode.RiskReportingNodeID",
		"Classification08_RiskReportingNode.RiskReportingNodeID",
		"Classification09_RiskReportingNode.RiskReportingNodeID",
		"Classification10_RiskReportingNode.RiskReportingNodeID",
		"Classification11_RiskReportingNode.RiskReportingNodeID",
		"Classification12_RiskReportingNode.RiskReportingNodeID",
		"Classification13_RiskReportingNode.RiskReportingNodeID",
		"Classification14_RiskReportingNode.RiskReportingNodeID",
		"Classification15_RiskReportingNode.RiskReportingNodeID",
		"_SecuritiesAccount.FinancialContractID",
		"_SecuritiesAccount.IDSystem",
		"_TimeBucket.MaturityBandID",
		"_TimeBucket.TimeBucketID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"Currency",
		"ExposureValue",
		"NetPresentValue",
		"SecuritizationDelta",
		"TimeBucketEndDate",
		"TimeBucketStartDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID"
	FROM(
		SELECT
		
	
		res_fact."RES_FACT_VOLA_EXP_ID" AS "RES_FACT_VOLA_EXP_ID",
		'Static' AS "MarketRiskAnalysisType",
		surf."POS" AS "MarketRiskSplitPartType",
		sc."STATIC_SCENARIO_NAME" AS "RiskProvisionScenario",
		'FunctionalCurrency' AS "RoleOfCurrency",
		CASE
			WHEN LEFT(IFNULL(res_fact."CONTRACT_NAME",''), 4) <> 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_VOLA_EXP_ID" AS NVARCHAR (128)))
			ELSE
				NULL
		END AS "_FinancialContract.FinancialContractID",
		CASE
			WHEN LEFT(res_fact."CONTRACT_NAME", 4) <> 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME")
			ELSE
				NULL
		END AS "_FinancialContract.IDSystem",
		CASE
				WHEN LEFT(IFNULL(res_fact."CONTRACT_NAME",''), 4) = 'I-_-'
					THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialInstrumentID"(res_fact."CONTRACT_NAME") 
				ELSE NULL	
			END AS "_FinancialInstrument.FinancialInstrumentID",
		"com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."CONTRACT_NAME") AS "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
		CASE
			WHEN "com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."CONTRACT_NAME") IS NOT NULL
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_VOLA_EXP_ID" AS NVARCHAR (128)))
				ELSE NULL
		END AS "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
		CASE
			WHEN "com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."CONTRACT_NAME") IS NOT NULL
				THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME")
			ELSE NULL
		END AS "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
		CAST("com.adweko.adapter.osx.resultdata.common::get_ResultProvider"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS NVARCHAR(128)) AS "_ResultGroup.ResultDataProvider",
		"com.adweko.adapter.osx.resultdata.common::get_ResulGroupId"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS "_ResultGroup.ResultGroupID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."ACCOUNT_ID") AS "Account_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_01") AS "Classification01_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_02") AS "Classification02_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_03") AS "Classification03_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_04") AS "Classification04_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_05") AS "Classification05_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_06") AS "Classification06_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_07") AS "Classification07_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_08") AS "Classification08_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_09") AS "Classification09_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_10") AS "Classification10_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_11") AS "Classification11_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_12") AS "Classification12_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_13") AS "Classification13_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_14") AS "Classification14_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_15") AS "Classification15_RiskReportingNode.RiskReportingNodeID",
		CASE
			WHEN LEFT(res_fact."CONTRACT_NAME", 4) = 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_VOLA_EXP_ID" AS NVARCHAR (128)))
			ELSE
				NULL
		END AS "_SecuritiesAccount.FinancialContractID",
		CASE
			WHEN LEFT(res_fact."CONTRACT_NAME", 4) = 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME")
			ELSE
				NULL
		END AS "_SecuritiesAccount.IDSystem",
		tbs."_TimeBucket.MaturityBandID" AS "_TimeBucket.MaturityBandID",
		tbs."_TimeBucket.TimeBucketID" AS "_TimeBucket.TimeBucketID",
		"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(slv."ANALYSIS_DATE") AS "BusinessValidFrom",
		"com.adweko.adapter.osx.resultdata.common::get_MaxDate"() AS "BusinessValidTo",
		cur."CURRENCY_NAME" AS "Currency",
		TO_DECIMAL(res_fact."VOL_EXP_BAS_CUR", 34, 6) AS "ExposureValue",
		TO_DECIMAL(res_fact."VOL_EXP_NPV_BAS_CUR", 34, 6) AS "NetPresentValue",
		TO_DECIMAL(res_fact."VOL_EXP_ABS_DELTA_BAS_CUR", 34, 6) AS "SecuritizationDelta",
		tbs."BUCKET_END" AS "TimeBucketEndDate",
		tbs."BUCKET_START" AS "TimeBucketStartDate",
		NULL AS "SourceSystemID",
		NULL AS "ChangeTimestampInSourceSystem",
		NULL AS "ChangingUserInSourceSystem",
		NULL AS "ChangingProcessType",
		NULL AS "ChangingProcessID"
		
		FROM "com.adweko.adapter.osx.synonyms::RES_FACT_VOLA_EXP" AS res_fact
		INNER JOIN "com.adweko.adapter.osx.synonyms::RES_VOLA_SURF_STRIKE_IDX" as surf
		ON res_fact."VOLA_SURF_STRIKE_IDX" = surf."RES_VOLA_SURF_STRIKE_IDX_ID"
		AND res_fact."SLV_JOB_ID"	= surf."SLV_JOB_ID"
		INNER JOIN "com.adweko.adapter.osx.synonyms::RES_DIM_ACCOUNT" AS acc
		ON  res_fact."ACCOUNT_ID"	= acc."ACCOUNT_ID" 
		AND res_fact."SLV_JOB_ID"	= acc."SLV_JOB_ID"
		AND acc."IS_VISIBLE"		= true
		INNER JOIN "com.adweko.adapter.osx.synonyms::RES_SOLVE" AS slv 
		ON (res_fact."SLV_JOB_ID"	= slv."SLV_JOB_ID" 
		AND res_fact."MODEL_ID" 	= slv."MODEL_ID")
		INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_static_scenario"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS sc
		ON (sc."RES_SOLVE_ID"		= slv."RES_SOLVE_ID"
		AND sc."STATIC_SCENARIO_ID" = res_fact."STAT_SCEN_ID")
		INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_base_currency"(:I_SOLVE_JOB_ID, :I_MODEL_ID ) AS cur
		ON cur."SLV_JOB_ID" = :I_SOLVE_JOB_ID
		AND cur."MODEL_ID"	= :I_MODEL_ID
		INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_static_tbs"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS tbs
		ON (tbs."BUCKET_NB" = res_fact."VOL_EXP_TB"
	 	AND tbs."RES_DIM_TBS_KIND"='VOL')

		WHERE res_fact."SLV_JOB_ID" = :I_SOLVE_JOB_ID
		AND res_fact."MODEL_ID"		= :I_MODEL_ID))
	-- pivot for ChartOfAccounts and ClassificationTree
	LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_generate_multiple_rows"(1, 1, 17) AS ser3
	ON true = true)
		
	WHERE "_RiskReportingNode.RiskReportingNodeID" IS NOT NULL
	
	GROUP BY
	    "MarketRiskAnalysisType",
	    "MarketRiskSplitPartType",
	    "RiskProvisionScenario",
	    "RoleOfCurrency",
	    "_FinancialContract.FinancialContractID",
	    "_FinancialContract.IDSystem",
	    "_FinancialInstrument.FinancialInstrumentID",
	    "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
	    "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
	    "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
	    "_ResultGroup.ResultDataProvider",
	    "_ResultGroup.ResultGroupID",
	    "_RiskReportingNode.RiskReportingNodeID",
	    "_SecuritiesAccount.FinancialContractID",
	    "_SecuritiesAccount.IDSystem",
	    "_TimeBucket.MaturityBandID",
	    "_TimeBucket.TimeBucketID",
	    "BusinessValidFrom",
	    "BusinessValidTo",
	    "Currency",
	    "TimeBucketEndDate",
	    "TimeBucketStartDate",
	    "SourceSystemID",
	    "ChangeTimestampInSourceSystem",
	    "ChangingUserInSourceSystem",
	    "ChangingProcessType",
	    "ChangingProcessID";

END;