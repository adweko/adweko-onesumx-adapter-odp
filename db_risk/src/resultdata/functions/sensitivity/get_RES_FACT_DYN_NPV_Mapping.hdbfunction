FUNCTION "com.adweko.adapter.osx.sensitivity::get_RES_FACT_DYN_NPV_Mapping"(
	IN I_SOLVE_JOB_ID BIGINT,
	IN I_MODEL_ID BIGINT
	)
       RETURNS TABLE(
       	"RES_FACT_DYN_NPV_ID" BIGINT,
		"MarketRiskAnalysisType" NVARCHAR (100),
		"MarketRiskSplitPartType" NVARCHAR (100),
		"RiskProvisionScenario" NVARCHAR (100),
		"RoleOfCurrency" NVARCHAR (40),
		"_DynamicTimeBucket.MaturityBandID" NVARCHAR (128),
		"_DynamicTimeBucket.TimeBucketID" NVARCHAR (128),
		"_FinancialContract.FinancialContractID" NVARCHAR (128),
		"_FinancialContract.IDSystem" NVARCHAR (40),
		"_FinancialInstrument.FinancialInstrumentID" NVARCHAR (128),
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" NVARCHAR(3),
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" NVARCHAR(128),
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" NVARCHAR(40),
		"_ResultGroup.ResultDataProvider" NVARCHAR (256),
		"_ResultGroup.ResultGroupID" NVARCHAR (128),
		"_RiskReportingNode.RiskReportingNodeID" NVARCHAR (128),
		"_SecuritiesAccount.FinancialContractID" NVARCHAR (128),
		"_SecuritiesAccount.IDSystem" NVARCHAR (40),
		"BusinessValidFrom" DATE ,
		"BusinessValidTo" DATE ,
		"AccruedInterest" DECIMAL (34,6),
		"AccruedInterestInternalPureView" DECIMAL (34,6),
		"AccruedInterestInternalTreasuryView" DECIMAL (34,6),
		"Currency" NVARCHAR (3),
		"DollarConvexity" DECIMAL (34,6),
		"DollarConvexityInternalPureView" DECIMAL (34,6),
		"DollarConvexityInternalTreasuryView" DECIMAL (34,6),
		"DollarDuration" DECIMAL (34,6),
		"DollarDurationInternalPureView" DECIMAL (34,6),
		"DollarDurationInternalTreasuryView" DECIMAL (34,6),
		"DollarInternalRateOfReturn" DECIMAL (34,6),
		"DynamicTimeBucketEndDate" DATE ,
		"DynamicTimeBucketStartDate" DATE ,
		"ModifiedDollarConvexity" DECIMAL (34,6),
		"ModifiedDollarDuration" DECIMAL (34,6),
		"ModifiedOptionDollarDuration" DECIMAL (34,6),
		"ModifiedTimeDecay" DECIMAL (34,6),
		"NetPresentValue" DECIMAL (34,6),
		"NetPresentValueInternalPureView" DECIMAL (34,6),
		"NetPresentValueInternalTreasuryView" DECIMAL (34,6),
		"OptionDollarConvexity" DECIMAL (34,6),
		"OptionDollarDelta" DECIMAL (34,6),
		"OptionDollarDuration" DECIMAL (34,6),
		"OptionDollarGamma" DECIMAL (34,6),
		"OptionDollarLambda" DECIMAL (34,6),
		"OptionNetPresentValue" DECIMAL (34,6),
		"OptionTimeDecay" DECIMAL (34,6),
		"OriginalIFRSSpread" DECIMAL (34,6),
		"TimeDecay" DECIMAL (34,6),
		"TimeDecayInternalPureView" DECIMAL (34,6),
		"TimeDecayInternalTreasuryView" DECIMAL (34,6),
		"SourceSystemID" NVARCHAR (128),
		"ChangeTimestampInSourceSystem" TIMESTAMP ,
		"ChangingUserInSourceSystem" NVARCHAR (128),
		"ChangingProcessType" NVARCHAR (40),
		"ChangingProcessID" NVARCHAR (128)
       ) 
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN
	SELECT
	MIN("RES_FACT_DYN_NPV_ID") AS "RES_FACT_DYN_NPV_ID",
	"MarketRiskAnalysisType",
	"MarketRiskSplitPartType",
	"RiskProvisionScenario",
	"RoleOfCurrency",
	"_DynamicTimeBucket.MaturityBandID",
	"_DynamicTimeBucket.TimeBucketID",
	"_FinancialContract.FinancialContractID",
	"_FinancialContract.IDSystem",
	"_FinancialInstrument.FinancialInstrumentID",
	"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
	"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
	"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
	"_ResultGroup.ResultDataProvider",
	"_ResultGroup.ResultGroupID",
	"_RiskReportingNode.RiskReportingNodeID",
	"_SecuritiesAccount.FinancialContractID",
	"_SecuritiesAccount.IDSystem",
	"BusinessValidFrom",
	"BusinessValidTo",
	SUM("AccruedInterest") AS "AccruedInterest",
	SUM("AccruedInterestInternalPureView") AS "AccruedInterestInternalPureView",
	SUM("AccruedInterestInternalTreasuryView") AS "AccruedInterestInternalTreasuryView",
	"Currency",
	SUM("DollarConvexity") AS "DollarConvexity",
	SUM("DollarConvexityInternalPureView") AS "DollarConvexityInternalPureView",
	SUM("DollarConvexityInternalTreasuryView") AS "DollarConvexityInternalTreasuryView",
	SUM("DollarDuration") AS "DollarDuration",
	SUM("DollarDurationInternalPureView") AS "DollarDurationInternalPureView",
	SUM("DollarDurationInternalTreasuryView") AS "DollarDurationInternalTreasuryView",
	SUM("DollarInternalRateOfReturn") AS "DollarInternalRateOfReturn",
	"DynamicTimeBucketEndDate",
	"DynamicTimeBucketStartDate",
	SUM("ModifiedDollarConvexity") AS "ModifiedDollarConvexity",
	SUM("ModifiedDollarDuration") AS "ModifiedDollarDuration",
	SUM("ModifiedOptionDollarDuration") AS "ModifiedOptionDollarDuration",
	SUM("ModifiedTimeDecay") AS "ModifiedTimeDecay",
	SUM("NetPresentValue") AS "NetPresentValue",
	SUM("NetPresentValueInternalPureView") AS "NetPresentValueInternalPureView",
	SUM("NetPresentValueInternalTreasuryView") AS "NetPresentValueInternalTreasuryView",
	SUM("OptionDollarConvexity") AS "OptionDollarConvexity",
	SUM("OptionDollarDelta") AS "OptionDollarDelta",
	SUM("OptionDollarDuration") AS "OptionDollarDuration",
	SUM("OptionDollarGamma") AS "OptionDollarGamma",
	SUM("OptionDollarLambda") AS "OptionDollarLambda",
	SUM("OptionNetPresentValue") AS "OptionNetPresentValue",
	SUM("OptionTimeDecay") AS "OptionTimeDecay",
	SUM("OriginalIFRSSpread") AS "OriginalIFRSSpread",
	SUM("TimeDecay") AS "TimeDecay",
	SUM("TimeDecayInternalPureView") AS "TimeDecayInternalPureView",
	SUM("TimeDecayInternalTreasuryView") AS "TimeDecayInternalTreasuryView",
	"SourceSystemID",
	"ChangeTimestampInSourceSystem",
	"ChangingUserInSourceSystem",
	"ChangingProcessType",
	"ChangingProcessID"
FROM(
	SELECT
	"RES_FACT_DYN_NPV_ID",
	"MarketRiskAnalysisType",
	"MarketRiskSplitPartType",
	"RiskProvisionScenario",
	"RoleOfCurrency",
	"_DynamicTimeBucket.MaturityBandID",
	"_DynamicTimeBucket.TimeBucketID",
	"_FinancialContract.FinancialContractID",
	"_FinancialContract.IDSystem",
	"_FinancialInstrument.FinancialInstrumentID",
	"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
	"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
	"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
	"_ResultGroup.ResultDataProvider",
	"_ResultGroup.ResultGroupID",
	-- pivot for ChartOfAccounts and ClassificationTree
	MAP(ser3."ELEMENT_NUMBER",
		1, "Account_RiskReportingNode.RiskReportingNodeID",
		2, "Classification01_RiskReportingNode.RiskReportingNodeID",
		3, "Classification02_RiskReportingNode.RiskReportingNodeID",
		4, "Classification03_RiskReportingNode.RiskReportingNodeID",
		5, "Classification04_RiskReportingNode.RiskReportingNodeID",
		6, "Classification05_RiskReportingNode.RiskReportingNodeID",
		7, "Classification06_RiskReportingNode.RiskReportingNodeID",
		8, "Classification07_RiskReportingNode.RiskReportingNodeID",
		9, "Classification08_RiskReportingNode.RiskReportingNodeID",
		10, "Classification09_RiskReportingNode.RiskReportingNodeID",
		11, "Classification10_RiskReportingNode.RiskReportingNodeID",
		12, "Classification11_RiskReportingNode.RiskReportingNodeID",
		13, "Classification12_RiskReportingNode.RiskReportingNodeID",
		14, "Classification13_RiskReportingNode.RiskReportingNodeID",
		15, "Classification14_RiskReportingNode.RiskReportingNodeID",
		16, "Classification15_RiskReportingNode.RiskReportingNodeID")
	AS "_RiskReportingNode.RiskReportingNodeID",
	"_SecuritiesAccount.FinancialContractID",
	"_SecuritiesAccount.IDSystem",
	"BusinessValidFrom",
	"BusinessValidTo",
	"AccruedInterest",
	"AccruedInterestInternalPureView",
	"AccruedInterestInternalTreasuryView",
	"Currency",
	"DollarConvexity",
	"DollarConvexityInternalPureView",
	"DollarConvexityInternalTreasuryView",
	"DollarDuration",
	"DollarDurationInternalPureView",
	"DollarDurationInternalTreasuryView",
	"DollarInternalRateOfReturn",
	"DynamicTimeBucketEndDate",
	"DynamicTimeBucketStartDate",
	"ModifiedDollarConvexity",
	"ModifiedDollarDuration",
	"ModifiedOptionDollarDuration",
	"ModifiedTimeDecay",
	"NetPresentValue",
	"NetPresentValueInternalPureView",
	"NetPresentValueInternalTreasuryView",
	"OptionDollarConvexity",
	"OptionDollarDelta",
	"OptionDollarDuration",
	"OptionDollarGamma",
	"OptionDollarLambda",
	"OptionNetPresentValue",
	"OptionTimeDecay",
	"OriginalIFRSSpread",
	"TimeDecay",
	"TimeDecayInternalPureView",
	"TimeDecayInternalTreasuryView",
	"SourceSystemID",
	"ChangeTimestampInSourceSystem",
	"ChangingUserInSourceSystem",
	"ChangingProcessType",
	"ChangingProcessID"
FROM(
	SELECT
	"RES_FACT_DYN_NPV_ID",
	"MarketRiskAnalysisType",
	"MarketRiskSplitPartType",
	"RiskProvisionScenario",
	"RoleOfCurrency",
	"_DynamicTimeBucket.MaturityBandID",
	"_DynamicTimeBucket.TimeBucketID",
	"_FinancialContract.FinancialContractID",
	"_FinancialContract.IDSystem",
	"_FinancialInstrument.FinancialInstrumentID",
	"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
	"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
	"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
	"_ResultGroup.ResultDataProvider",
	"_ResultGroup.ResultGroupID",
	"Account_RiskReportingNode.RiskReportingNodeID",
	"Classification01_RiskReportingNode.RiskReportingNodeID",
	"Classification03_RiskReportingNode.RiskReportingNodeID",
	"Classification02_RiskReportingNode.RiskReportingNodeID",
	"Classification04_RiskReportingNode.RiskReportingNodeID",
	"Classification05_RiskReportingNode.RiskReportingNodeID",
	"Classification06_RiskReportingNode.RiskReportingNodeID",
	"Classification07_RiskReportingNode.RiskReportingNodeID",
	"Classification08_RiskReportingNode.RiskReportingNodeID",
	"Classification09_RiskReportingNode.RiskReportingNodeID",
	"Classification10_RiskReportingNode.RiskReportingNodeID",
	"Classification11_RiskReportingNode.RiskReportingNodeID",
	"Classification12_RiskReportingNode.RiskReportingNodeID",
	"Classification13_RiskReportingNode.RiskReportingNodeID",
	"Classification14_RiskReportingNode.RiskReportingNodeID",
	"Classification15_RiskReportingNode.RiskReportingNodeID",
	"_SecuritiesAccount.FinancialContractID",
	"_SecuritiesAccount.IDSystem",
	"BusinessValidFrom",
	"BusinessValidTo",
	"AccruedInterest",
	"AccruedInterestInternalPureView",
	"AccruedInterestInternalTreasuryView",
	"Currency",
	"DollarConvexity",
	"DollarConvexityInternalPureView",
	"DollarConvexityInternalTreasuryView",
	"DollarDuration",
	"DollarDurationInternalPureView",
	"DollarDurationInternalTreasuryView",
	"DollarInternalRateOfReturn",
	"DynamicTimeBucketEndDate",
	"DynamicTimeBucketStartDate",
	"ModifiedDollarConvexity",
	"ModifiedDollarDuration",
	"ModifiedOptionDollarDuration",
	"ModifiedTimeDecay",
	"NetPresentValue",
	"NetPresentValueInternalPureView",
	"NetPresentValueInternalTreasuryView",
	"OptionDollarConvexity",
	"OptionDollarDelta",
	"OptionDollarDuration",
	"OptionDollarGamma",
	"OptionDollarLambda",
	"OptionNetPresentValue",
	"OptionTimeDecay",
	"OriginalIFRSSpread",
	"TimeDecay",
	"TimeDecayInternalPureView",
	"TimeDecayInternalTreasuryView",
	"SourceSystemID",
	"ChangeTimestampInSourceSystem",
	"ChangingUserInSourceSystem",
	"ChangingProcessType",
	"ChangingProcessID"
FROM(
		SELECT
		res_fact."RES_FACT_DYN_NPV_ID" AS "RES_FACT_DYN_NPV_ID",
		CASE
			WHEN res_fact."DYN_MC_RUN_NUMBER" IS NULL
				THEN 'Dynamic'
			ELSE 'MonteCarloSimulation'
		END AS "MarketRiskAnalysisType",
		NULL AS "MarketRiskSplitPartType",
		CASE
			WHEN res_fact."DYN_WHATIF_ID" IS NOT NULL
				THEN wi."WHATIF_NAME"
			ELSE CAST(res_fact."DYN_MC_RUN_NUMBER" AS NVARCHAR(100))
		END AS "RiskProvisionScenario",
		MAP(ser."ELEMENT_NUMBER",
			1, 'FunctionalCurrency',
			2, 'ContractCurrency'
			) AS "RoleOfCurrency",
		tbs."_TimeBucket.MaturityBandID" AS "_DynamicTimeBucket.MaturityBandID",
		tbs."_TimeBucket.TimeBucketID" AS "_DynamicTimeBucket.TimeBucketID",
		CASE
			WHEN LEFT(IFNULL(res_fact."CONTRACT_NAME",''), 4) <> 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_DYN_NPV_ID" AS NVARCHAR (128)))
			ELSE
				NULL
		END AS "_FinancialContract.FinancialContractID",
		CASE
			WHEN LEFT(res_fact."CONTRACT_NAME", 4) <> 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME")
			ELSE
				NULL
		END AS "_FinancialContract.IDSystem",
		CASE
			WHEN LEFT(IFNULL(res_fact."CONTRACT_NAME",''), 4) = 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialInstrumentID"(res_fact."CONTRACT_NAME") 
			ELSE NULL	
		END AS "_FinancialInstrument.FinancialInstrumentID",
		"com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."CONTRACT_NAME") AS "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
		CASE
			WHEN "com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."CONTRACT_NAME") IS NOT NULL
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_DYN_NPV_ID" AS NVARCHAR (128)))
				ELSE NULL
		END AS "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
		CASE
			WHEN "com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."CONTRACT_NAME") IS NOT NULL
				THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME")
			ELSE NULL
		END AS "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
		CAST("com.adweko.adapter.osx.resultdata.common::get_ResultProvider"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS NVARCHAR(128)) AS "_ResultGroup.ResultDataProvider",
		"com.adweko.adapter.osx.resultdata.common::get_ResulGroupId"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS "_ResultGroup.ResultGroupID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."ACCOUNT_ID") AS "Account_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_01") AS "Classification01_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_02") AS "Classification02_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_03") AS "Classification03_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_04") AS "Classification04_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_05") AS "Classification05_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_06") AS "Classification06_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_07") AS "Classification07_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_08") AS "Classification08_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_09") AS "Classification09_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_10") AS "Classification10_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_11") AS "Classification11_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_12") AS "Classification12_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_13") AS "Classification13_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_14") AS "Classification14_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_15") AS "Classification15_RiskReportingNode.RiskReportingNodeID",
				CASE
			WHEN LEFT(res_fact."CONTRACT_NAME", 4) = 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_DYN_NPV_ID" AS NVARCHAR (128)))
			ELSE
				NULL
		END AS "_SecuritiesAccount.FinancialContractID",
		CASE
			WHEN LEFT(res_fact."CONTRACT_NAME", 4) = 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME")
			ELSE
				NULL
		END AS "_SecuritiesAccount.IDSystem",
		"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(slv."ANALYSIS_DATE") AS "BusinessValidFrom",
		"com.adweko.adapter.osx.resultdata.common::get_MaxDate"() AS "BusinessValidTo",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."AI_VALUE_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."AI_VALUE_CT_CUR", 34 ,6)
			) AS "AccruedInterest",
		NULL AS "AccruedInterestInternalPureView",
		NULL AS "AccruedInterestInternalTreasuryView",
		MAP(ser."ELEMENT_NUMBER",
			1, basecur."CURRENCY_NAME",
			2, contcur."CURRENCY_NAME"
			) AS "Currency",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."DOL_CONV_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."DOL_CONV_CT_CUR", 34, 6)
			) AS "DollarConvexity",
		NULL AS "DollarConvexityInternalPureView",
		NULL AS "DollarConvexityInternalTreasuryView",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."DOL_DURATION_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."DOL_DURATION_CT_CUR", 34, 6)
			) AS "DollarDuration",
		NULL AS "DollarDurationInternalPureView",
		NULL AS "DollarDurationInternalTreasuryView",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."DOL_INT_RATE_OF_RET_BAS_CUR", 34, 6),
			2, NULL
			) AS "DollarInternalRateOfReturn",
		tbs."BUCKET_END" AS "DynamicTimeBucketEndDate",
		tbs."BUCKET_START" AS "DynamicTimeBucketStartDate",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."MOD_DOL_CONV_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."MOD_DOL_CONV_CT_CUR", 34, 6)
			) AS "ModifiedDollarConvexity",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."MOD_DOL_DUR_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."MOD_DOL_DUR_CT_CUR", 34, 6)
			) AS "ModifiedDollarDuration",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."MOD_OPT_DOL_DUR_BAS_CUR", 34, 6),
			2, NULL
			) AS "ModifiedOptionDollarDuration",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."MOD_TM_DECAY_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."MOD_TM_DECAY_CT_CUR", 34, 6)
			) AS "ModifiedTimeDecay",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."NPV_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."NPV_CT_CUR", 34, 6)
			) AS "NetPresentValue",
		NULL AS "NetPresentValueInternalPureView",
		NULL AS "NetPresentValueInternalTreasuryView",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."OPT_DOLLAR_CONV_BAS_CUR", 34, 6),
			2, NULL
			) AS "OptionDollarConvexity",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."OPT_DELTA_VAL_BAS_CUR", 34, 6),
			2, NULL
			) AS "OptionDollarDelta",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."OPT_DOLLAR_DUR_BAS_CUR", 34, 6),
			2, NULL
			) AS "OptionDollarDuration",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."OPT_GAMMA_VAL_BAS_CUR", 34, 6),
			2, NULL
			) AS "OptionDollarGamma",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."OPT_DOLLAR_LAMBDA_BAS_CUR", 34, 6),
			2, NULL
			) AS "OptionDollarLambda",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."OPT_NPV_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."OPT_NPV_CT_CUR", 34, 6)
			) AS "OptionNetPresentValue",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."OPT_TIME_DECAY_BAS_CUR", 34, 6),
			2, NULL
			) AS "OptionTimeDecay",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."IFRS_ORIG_SPREAD_NPV_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."IFRS_ORIG_SPREAD_NPV_CT_CUR", 34, 6)
			) AS "OriginalIFRSSpread",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."TM_DECAY_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."TM_DECAY_CT_CUR", 34, 6)
			) AS "TimeDecay",
		NULL AS "TimeDecayInternalPureView",
		NULL AS "TimeDecayInternalTreasuryView",
		NULL AS "SourceSystemID",
		NULL AS "ChangeTimestampInSourceSystem",
		NULL AS "ChangingUserInSourceSystem",
		NULL AS "ChangingProcessType",
		NULL AS "ChangingProcessID",
		ROW_NUMBER() OVER (PARTITION BY res_fact."DYN_MC_RUN_NUMBER",
											res_fact."DYN_WHATIF_ID",
											ser."ELEMENT_NUMBER",
											res_fact."DYN_SIMUL_BUCKET_IDX",
											COALESCE(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_DYN_NPV_ID" AS NVARCHAR (255)))
		) AS "Row_Num",
		-- Stores information in which currency types (base, contract, event) the results where calculated.
		-- Is needed to filter out useless rows at the end of the mapping process.
		CASE
			WHEN res_fact."CONTRACT_CURRENCY_ID" IS NOT NULL
				THEN 1
			ELSE 4
		END AS "UsedCurrencyTypes"
		
		FROM "com.adweko.adapter.osx.synonyms::RES_FACT_DYN_NPV" AS res_fact
		INNER JOIN "com.adweko.adapter.osx.synonyms::RES_DIM_ACCOUNT" AS acc
  		ON  res_fact."ACCOUNT_ID"	= acc."ACCOUNT_ID" 
  		AND res_fact."SLV_JOB_ID"	= acc."SLV_JOB_ID"
		AND acc."IS_VISIBLE"		= true
		INNER JOIN "com.adweko.adapter.osx.synonyms::RES_SOLVE" AS slv 
		ON (res_fact."SLV_JOB_ID"	= slv."SLV_JOB_ID"
   		AND res_fact."MODEL_ID"		= slv."MODEL_ID")
   		LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_dynamic_whatif"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS wi
   		ON (wi."RES_SOLVE_ID"	= slv."RES_SOLVE_ID"
   		AND wi."WHATIF_ID"		= res_fact."DYN_WHATIF_ID")
   		INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_static_tbs"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS tbs
   		ON (tbs."BUCKET_NB" = res_fact."DYN_SIMUL_BUCKET_IDX"
   		AND tbs."RES_DIM_TBS_KIND"='DSIM')
   		INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_base_currency"(:I_SOLVE_JOB_ID, :I_MODEL_ID ) AS basecur
		ON basecur."SLV_JOB_ID" = :I_SOLVE_JOB_ID
		AND basecur."MODEL_ID"	= :I_MODEL_ID
		LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_ct_ev_currency"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS contcur
		ON contcur."SLV_JOB_ID" 	= :I_SOLVE_JOB_ID
		AND contcur."SLV_JOB_ID"	= :I_SOLVE_JOB_ID
		AND contcur."CURRENCY_ID"	= res_fact."CONTRACT_CURRENCY_ID"
		LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_generate_multiple_rows"(1, 1, 3) AS ser
		ON true = true
	
		WHERE res_fact."SLV_JOB_ID" = :I_SOLVE_JOB_ID
   		AND res_fact."MODEL_ID" 	= :I_MODEL_ID)
   		
   	WHERE 
		-- filter out useless rows (rows without filled measures) and duplicated rows
		("Row_Num" = 1 AND 
		("UsedCurrencyTypes" = 1 
		AND "RoleOfCurrency" IN ('FunctionalCurrency','ContractCurrency')))
		OR
		("Row_Num" = 1 AND
		("UsedCurrencyTypes" = 4
		AND "RoleOfCurrency" = 'FunctionalCurrency')))
		-- pivot for ChartOfAccounts and ClassificationTree
		LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_generate_multiple_rows"(1, 1, 17) AS ser3
		ON true = true)
			
		WHERE "_RiskReportingNode.RiskReportingNodeID" IS NOT NULL
		
		GROUP BY
	        "MarketRiskAnalysisType",
	        "MarketRiskSplitPartType",
	        "RiskProvisionScenario",
	        "RoleOfCurrency",
	        "_DynamicTimeBucket.MaturityBandID",
	        "_DynamicTimeBucket.TimeBucketID",
	        "_FinancialContract.FinancialContractID",
	        "_FinancialContract.IDSystem",
	        "_FinancialInstrument.FinancialInstrumentID",
	        "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
	        "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
	        "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
	        "_ResultGroup.ResultDataProvider",
	        "_ResultGroup.ResultGroupID",
	        "_RiskReportingNode.RiskReportingNodeID",
	        "_SecuritiesAccount.FinancialContractID",
	        "_SecuritiesAccount.IDSystem",
	        "BusinessValidFrom",
	        "BusinessValidTo",
	        "Currency",
	        "DynamicTimeBucketEndDate",
	        "DynamicTimeBucketStartDate",
	        "SourceSystemID",
	        "ChangeTimestampInSourceSystem",
	        "ChangingUserInSourceSystem",
	        "ChangingProcessType",
	        "ChangingProcessID";

END;