FUNCTION "com.adweko.adapter.osx.cashflows::get_RES_FINANCIAL_EVENT_Mapping"(	
	IN I_SOLVE_JOB_ID BIGINT,
	IN I_MODEL_ID BIGINT
	)
		RETURNS TABLE(
		"RES_FINANCIAL_EVENT_ID" BIGINT,
		"CashFlowEndDate" TIMESTAMP ,
		"CashFlowScenario" NVARCHAR (100),
		"CashFlowSource" NVARCHAR (10),
		"CashFlowStartDate" TIMESTAMP ,
		"CashFlowType" NVARCHAR (120),
		"ComponentNumber" INTEGER ,
		"ItemNumber" INTEGER ,
		"LotID" NVARCHAR (128),
		"RoleOfPayer" NVARCHAR (50),
		"ASSOC_Contract.FinancialContractID" NVARCHAR (128),
		"ASSOC_Contract.IDSystem" NVARCHAR (40),
		"ASSOC_InterestSpecification.SequenceNumber" INTEGER ,
		"ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" NVARCHAR (3),
		"ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" NVARCHAR (128),
		"ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" NVARCHAR (40),
		"ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID" NVARCHAR (128),
		"ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem" NVARCHAR (40),
		"ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID" NVARCHAR (128),
		"ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber" INTEGER ,
		"ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" NVARCHAR (128),
		"ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" NVARCHAR (40),
		"ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber" INTEGER ,
		"ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" NVARCHAR (128),
		"ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem" NVARCHAR (40),
		"ASSOC_InterestSpecification._Trade.IDSystem" NVARCHAR (40),
		"ASSOC_InterestSpecification._Trade.TradeID" NVARCHAR (128),
		"ASSOC_PositionCurrencyOfCashFlow.PositionCurrency" NVARCHAR (3),
		"ASSOC_PositionCurrencyOfCashFlow.ASSOC_MultiCcyAccnt.FinancialContractID" NVARCHAR (128),
		"ASSOC_PositionCurrencyOfCashFlow.ASSOC_MultiCcyAccnt.IDSystem" NVARCHAR (40),
		"_AccountingSystem.AccountingSystemID" NVARCHAR (128),
		"_CollectionOfCashFlow.CollectionID" NVARCHAR (128),
		"_CollectionOfCashFlow.IDSystem" NVARCHAR (40),
		"_CollectionOfCashFlow._Client.BusinessPartnerID" NVARCHAR (128),
		"_Instrument.FinancialInstrumentID" NVARCHAR (128),
		"_ResultGroup.ResultDataProvider" NVARCHAR (256),
		"_ResultGroup.ResultGroupID" NVARCHAR (128),
		"_SecuritiesAccount.FinancialContractID" NVARCHAR (128),
		"_SecuritiesAccount.IDSystem" NVARCHAR (40),
		"BusinessValidFrom" DATE ,
		"BusinessValidTo" DATE ,
		"ASSOC_ReferenceRate.ReferenceRateID" NVARCHAR (40),
		"_FSSubLedgerDocumentItem.CompanyCode" NVARCHAR (4),
		"_FSSubLedgerDocumentItem.DocumentNumber" NVARCHAR (60),
		"_FSSubLedgerDocumentItem.FiscalYear" NVARCHAR (10),
		"_FSSubLedgerDocumentItem.ItemNumber" BIGINT ,
		"_FSSubLedgerDocumentItem.PostingDate" DATE ,
		"_FSSubLedgerDocumentItem._AccountingSystem.AccountingSystemID" NVARCHAR (128),
		"_FSSubLedgerDocumentItem._BusinessSegment.IDSystem" NVARCHAR (40),
		"_FSSubLedgerDocumentItem._BusinessSegment.OrganizationalUnitID" NVARCHAR (128),
		"_FSSubLedgerDocumentItem._BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" NVARCHAR (128),
		"_FSSubLedgerDocumentItem._BusinessSegmentAtCounterparty.IDSystem" NVARCHAR (40),
		"_FSSubLedgerDocumentItem._BusinessSegmentAtCounterparty.OrganizationalUnitID" NVARCHAR (128),
		"_FSSubLedgerDocumentItem._BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" NVARCHAR (128),
		"AmountInPositionCurrency" DECIMAL (34,6),
		"AmountInTransactionCurrency" DECIMAL (34,6),
		"ApplicableInterestRate" DECIMAL (15,11),
		"CashFlowCategory" NVARCHAR (40),
		"CashFlowItemCategory" NVARCHAR (40),
		"CashFlowItemType" NVARCHAR (80),
		"ContractCashFlowCategory" NVARCHAR (40),
		"CreationType" NVARCHAR (100),
		"CurrencyOfPrincipalAmount" NVARCHAR (3),
		"DiscountedAmountInPositionCurrency" DECIMAL (34,6),
		"DiscountedAmountInTransactionCurrency" DECIMAL (34,6),
		"DueDate" DATE ,
		"InstrumentCashFlowCategory" NVARCHAR (100),
		"InterestFixingDate" DATE ,
		"InterestPeriodEndDate" DATE ,
		"InterestPeriodStartDate" DATE ,
		"InvestmentHoldingNominalAmount" DECIMAL (34,6),
		"InvestmentHoldingNominalAmountCurrency" NVARCHAR (3),
		"InvestmentHoldingQuantity" DECIMAL (34,6),
		"InvestmentHoldingUnit" NVARCHAR (10),
		"NumberOfBaseDaysUsedForInterestCalculation" INTEGER ,
		"NumberOfDaysUsedForInterestCalculation" INTEGER ,
		"PositionCurrency" NVARCHAR (3),
		"PostingDirection" NVARCHAR (6),
		"PrincipalAmount" DECIMAL (34,6),
		"SettledDate" DATE ,
		"SpreadIncludedInApplicableRate" DECIMAL (15,11),
		"TradeDate" DATE ,
		"TransactionCurrency" NVARCHAR (3),
		"UnsignedAmountInPositionCurrency" DECIMAL (34,6),
		"UnsignedAmountInTransactionCurrency" DECIMAL (34,6),
		"UsedDiscountFactor" DECIMAL (15,11),
		"UsedExchangeRate" DECIMAL (34,6),
		"ValueDate" DATE ,
		"SourceSystemID" NVARCHAR (128),
		"ChangeTimestampInSourceSystem" TIMESTAMP ,
		"ChangingUserInSourceSystem" NVARCHAR (128),
		"ChangingProcessType" NVARCHAR (40),
		"ChangingProcessID" NVARCHAR (128)
		)
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN
	SELECT
		"RES_FINANCIAL_EVENT_ID",
		"CashFlowEndDate",
		"CashFlowScenario",
		"CashFlowSource",
		"CashFlowStartDate",
		"CashFlowType",
		"ComponentNumber",
		"Row_Num" AS "ItemNumber",
		"LotID",
		"RoleOfPayer",
		"ASSOC_Contract.FinancialContractID",
		"ASSOC_Contract.IDSystem",
		"ASSOC_InterestSpecification.SequenceNumber",
		"ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency",
		"ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID",
		"ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem",
		"ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID",
		"ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem",
		"ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID",
		"ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber",
		"ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID",
		"ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem",
		"ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber",
		"ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID",
		"ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem",
		"ASSOC_InterestSpecification._Trade.IDSystem",
		"ASSOC_InterestSpecification._Trade.TradeID",
		"ASSOC_PositionCurrencyOfCashFlow.PositionCurrency",
		"ASSOC_PositionCurrencyOfCashFlow.ASSOC_MultiCcyAccnt.FinancialContractID",
		"ASSOC_PositionCurrencyOfCashFlow.ASSOC_MultiCcyAccnt.IDSystem",
		"_AccountingSystem.AccountingSystemID",
		"_CollectionOfCashFlow.CollectionID",
		"_CollectionOfCashFlow.IDSystem",
		"_CollectionOfCashFlow._Client.BusinessPartnerID",
		"_Instrument.FinancialInstrumentID",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		"_SecuritiesAccount.FinancialContractID",
		"_SecuritiesAccount.IDSystem",
		"BusinessValidFrom",
		"BusinessValidTo",
		"ASSOC_ReferenceRate.ReferenceRateID",
		"_FSSubLedgerDocumentItem.CompanyCode",
		"_FSSubLedgerDocumentItem.DocumentNumber" ,
		"_FSSubLedgerDocumentItem.FiscalYear",
		"_FSSubLedgerDocumentItem.ItemNumber",
		"_FSSubLedgerDocumentItem.PostingDate",
		"_FSSubLedgerDocumentItem._AccountingSystem.AccountingSystemID",
		"_FSSubLedgerDocumentItem._BusinessSegment.IDSystem",
		"_FSSubLedgerDocumentItem._BusinessSegment.OrganizationalUnitID",
		"_FSSubLedgerDocumentItem._BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
		"_FSSubLedgerDocumentItem._BusinessSegmentAtCounterparty.IDSystem",
		"_FSSubLedgerDocumentItem._BusinessSegmentAtCounterparty.OrganizationalUnitID",
		"_FSSubLedgerDocumentItem._BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
		"AmountInPositionCurrency",
		"AmountInTransactionCurrency",
		"ApplicableInterestRate",
		"CashFlowCategory",
		"CashFlowItemCategory",
		"CashFlowItemType",
		"ContractCashFlowCategory",
		"CreationType",
		"CurrencyOfPrincipalAmount",
		"DiscountedAmountInPositionCurrency",
		"DiscountedAmountInTransactionCurrency",
		"DueDate",
		"InstrumentCashFlowCategory",
		"InterestFixingDate",
		"InterestPeriodEndDate",
		"InterestPeriodStartDate",
		"InvestmentHoldingNominalAmount",
		"InvestmentHoldingNominalAmountCurrency",
		"InvestmentHoldingQuantity",
		"InvestmentHoldingUnit",
		"NumberOfBaseDaysUsedForInterestCalculation",
		"NumberOfDaysUsedForInterestCalculation",
		"PositionCurrency",
		"PostingDirection",
		"PrincipalAmount",
		"SettledDate",
		"SpreadIncludedInApplicableRate",
		"TradeDate",
		"TransactionCurrency",
		"UnsignedAmountInPositionCurrency",
		"UnsignedAmountInTransactionCurrency",
		"UsedDiscountFactor",
		"UsedExchangeRate",
		"ValueDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID"
		
		FROM (
	
	SELECT
		"RES_FINANCIAL_EVENT_ID",
		"CashFlowEndDate",
		"CashFlowScenario",
		"CashFlowSource",
		"CashFlowStartDate",
		"CashFlowType",
		"ComponentNumber",
		"ItemNumber",
		"LotID",
		"RoleOfPayer",
		"ASSOC_Contract.FinancialContractID",
		"ASSOC_Contract.IDSystem",
		"ASSOC_InterestSpecification.SequenceNumber",
		"ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency",
		"ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID",
		"ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem",
		"ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID",
		"ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem",
		"ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID",
		"ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber",
		"ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID",
		"ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem",
		"ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber",
		"ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID",
		"ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem",
		"ASSOC_InterestSpecification._Trade.IDSystem",
		"ASSOC_InterestSpecification._Trade.TradeID",
		"ASSOC_PositionCurrencyOfCashFlow.PositionCurrency",
		"ASSOC_PositionCurrencyOfCashFlow.ASSOC_MultiCcyAccnt.FinancialContractID",
		"ASSOC_PositionCurrencyOfCashFlow.ASSOC_MultiCcyAccnt.IDSystem",
		"_AccountingSystem.AccountingSystemID",
		"_CollectionOfCashFlow.CollectionID",
		"_CollectionOfCashFlow.IDSystem",
		"_CollectionOfCashFlow._Client.BusinessPartnerID",
		"_Instrument.FinancialInstrumentID",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		"_SecuritiesAccount.FinancialContractID",
		"_SecuritiesAccount.IDSystem",
		"BusinessValidFrom",
		"BusinessValidTo",
		"ASSOC_ReferenceRate.ReferenceRateID",
		"_FSSubLedgerDocumentItem.CompanyCode",
		"_FSSubLedgerDocumentItem.DocumentNumber" ,
		"_FSSubLedgerDocumentItem.FiscalYear",
		"_FSSubLedgerDocumentItem.ItemNumber",
		"_FSSubLedgerDocumentItem.PostingDate",
		"_FSSubLedgerDocumentItem._AccountingSystem.AccountingSystemID",
		"_FSSubLedgerDocumentItem._BusinessSegment.IDSystem",
		"_FSSubLedgerDocumentItem._BusinessSegment.OrganizationalUnitID",
		"_FSSubLedgerDocumentItem._BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
		"_FSSubLedgerDocumentItem._BusinessSegmentAtCounterparty.IDSystem",
		"_FSSubLedgerDocumentItem._BusinessSegmentAtCounterparty.OrganizationalUnitID",
		"_FSSubLedgerDocumentItem._BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
		"AmountInPositionCurrency",
		"AmountInTransactionCurrency",
		"ApplicableInterestRate",
		"CashFlowCategory",
		"CashFlowItemCategory",
		"CashFlowItemType",
		"ContractCashFlowCategory",
		"CreationType",
		"CurrencyOfPrincipalAmount",
		"DiscountedAmountInPositionCurrency",
		"DiscountedAmountInTransactionCurrency",
		"DueDate",
		"InstrumentCashFlowCategory",
		"InterestFixingDate",
		"InterestPeriodEndDate",
		"InterestPeriodStartDate",
		"InvestmentHoldingNominalAmount",
		"InvestmentHoldingNominalAmountCurrency",
		"InvestmentHoldingQuantity",
		"InvestmentHoldingUnit",
		"NumberOfBaseDaysUsedForInterestCalculation",
		"NumberOfDaysUsedForInterestCalculation",
		"PositionCurrency",
		"PostingDirection",
		"PrincipalAmount",
		"SettledDate",
		"SpreadIncludedInApplicableRate",
		"TradeDate",
		"TransactionCurrency",
		"UnsignedAmountInPositionCurrency",
		"UnsignedAmountInTransactionCurrency",
		"UsedDiscountFactor",
		"UsedExchangeRate",
		"ValueDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID",
		ROW_NUMBER() OVER (PARTITION BY "ASSOC_Contract.FinancialContractID", "ASSOC_Contract.IDSystem", "_Instrument.FinancialInstrumentID", "_SecuritiesAccount.FinancialContractID",
		"_SecuritiesAccount.IDSystem", "ASSOC_PositionCurrencyOfCashFlow.PositionCurrency", "CashFlowScenario"
		    ORDER BY "ValueDate" asc, "CashFlowType" asc, "RES_FINANCIAL_EVENT_ID" asc) 
		AS "Row_Num"
		
	FROM (
		SELECT
		res_fact."RES_FINANCIAL_EVENT_ID" AS "RES_FINANCIAL_EVENT_ID",
		enddate."CashFlowEndDate" AS "CashFlowEndDate",
		res_fact."STAT_SCEN_NAME" AS "CashFlowScenario", 
		'OneSumX-RP' AS "CashFlowSource",
		"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(slv."ANALYSIS_DATE") AS "CashFlowStartDate",
		CASE
			WHEN res_fact."SOURCE_SYSTEM_NAME" = 'crl_drawing'
				THEN 'BestEstimatedCashFlowStream'
			ELSE dis."CashflowType" 
		END AS "CashFlowType",
		TO_INTEGER(NULL) AS "ComponentNumber",
		NULL AS "ItemNumber",
		NULL AS "LotID",
		CASE
	        WHEN RIGHT(res_fact."SOURCE_SYSTEM_RECORD_NUM", 2) = '.1'
	            THEN 'Leg1'
	        WHEN RIGHT(res_fact."SOURCE_SYSTEM_RECORD_NUM", 2) = '.2'
	            THEN 'Leg2'
	        ELSE NULL
	    END AS "RoleOfPayer",
		CASE
			WHEN LEFT(res_fact."SOURCE_SYSTEM_RECORD_NUM", 4) <> 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."SOURCE_SYSTEM_RECORD_NUM")
			ELSE
				NULL
		END AS "ASSOC_Contract.FinancialContractID",
		CASE
			WHEN LEFT(res_fact."SOURCE_SYSTEM_RECORD_NUM", 4) <> 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."SOURCE_SYSTEM_RECORD_NUM")
			ELSE
				NULL
		END AS "ASSOC_Contract.IDSystem",
		TO_INTEGER(NULL) AS "ASSOC_InterestSpecification.SequenceNumber",
		NULL AS "ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency",
		NULL AS "ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID",
		NULL AS "ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem",
		NULL AS "ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID",
		NULL AS "ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem",
		NULL AS "ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID",
		TO_INTEGER(NULL) AS "ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber",
		NULL AS "ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID",
		NULL AS "ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem",
		TO_INTEGER(NULL) AS "ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber",
		NULL AS "ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID",
		NULL AS "ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem",
		NULL AS "ASSOC_InterestSpecification._Trade.IDSystem",
		NULL AS "ASSOC_InterestSpecification._Trade.TradeID",
		"com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."SOURCE_SYSTEM_RECORD_NUM") AS "ASSOC_PositionCurrencyOfCashFlow.PositionCurrency",
		CASE
			WHEN "com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."SOURCE_SYSTEM_RECORD_NUM") IS NOT NULL
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."SOURCE_SYSTEM_RECORD_NUM")
			ELSE NULL
		END AS "ASSOC_PositionCurrencyOfCashFlow.ASSOC_MultiCcyAccnt.FinancialContractID",
		CASE
			WHEN "com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."SOURCE_SYSTEM_RECORD_NUM") IS NOT NULL
				THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."SOURCE_SYSTEM_RECORD_NUM")
			ELSE NULL
		END AS "ASSOC_PositionCurrencyOfCashFlow.ASSOC_MultiCcyAccnt.IDSystem",
		NULL AS "_AccountingSystem.AccountingSystemID",
		NULL AS "_CollectionOfCashFlow.CollectionID",
		NULL AS "_CollectionOfCashFlow.IDSystem",
		NULL AS "_CollectionOfCashFlow._Client.BusinessPartnerID",
		CASE
			WHEN LEFT(IFNULL(res_fact."SOURCE_SYSTEM_RECORD_NUM",''), 4) = 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialInstrumentID"(res_fact."SOURCE_SYSTEM_RECORD_NUM")
			ELSE
				NULL
		END AS "_Instrument.FinancialInstrumentID",
		-- assign static result group if configured in key value parameters
		CASE 
		    WHEN statresgr."Value" IS NOT NULL
		        THEN 'OneSumX-RP'
	        ELSE CAST("com.adweko.adapter.osx.resultdata.common::get_ResultProvider"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS NVARCHAR(128))
	    END AS "_ResultGroup.ResultDataProvider",
	    -- assign static result group if configured in key value parameters
		CASE 
		    WHEN statresgr."Value" IS NOT NULL
		        THEN statresgr."Value"
	        ELSE "com.adweko.adapter.osx.resultdata.common::get_ResulGroupId"(:I_SOLVE_JOB_ID, :I_MODEL_ID)
	    END AS "_ResultGroup.ResultGroupID",
		CASE
			WHEN LEFT(res_fact."SOURCE_SYSTEM_RECORD_NUM", 4) = 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."SOURCE_SYSTEM_RECORD_NUM")
			ELSE
				NULL
		END AS "_SecuritiesAccount.FinancialContractID",
		CASE
			WHEN LEFT(res_fact."SOURCE_SYSTEM_RECORD_NUM", 4) = 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."SOURCE_SYSTEM_RECORD_NUM")
			ELSE
				NULL
		END AS "_SecuritiesAccount.IDSystem",
		validity."analysisDate" AS "BusinessValidFrom",
		validity."maxDate" AS "BusinessValidTo",
		NULL AS "ASSOC_ReferenceRate.ReferenceRateID",
		NULL AS "_FSSubLedgerDocumentItem.CompanyCode",
		NULL AS "_FSSubLedgerDocumentItem.DocumentNumber",
		NULL AS "_FSSubLedgerDocumentItem.FiscalYear",
		TO_BIGINT(NULL) AS "_FSSubLedgerDocumentItem.ItemNumber",
		NULL AS "_FSSubLedgerDocumentItem.PostingDate",
		NULL AS "_FSSubLedgerDocumentItem._AccountingSystem.AccountingSystemID",
		NULL AS "_FSSubLedgerDocumentItem._BusinessSegment.IDSystem",
		NULL AS "_FSSubLedgerDocumentItem._BusinessSegment.OrganizationalUnitID",
		NULL AS "_FSSubLedgerDocumentItem._BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
		NULL AS "_FSSubLedgerDocumentItem._BusinessSegmentAtCounterparty.IDSystem",
		NULL AS "_FSSubLedgerDocumentItem._BusinessSegmentAtCounterparty.OrganizationalUnitID",
		NULL AS "_FSSubLedgerDocumentItem._BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
		CASE 
			WHEN kv2."Value" = 'Position'
				THEN TO_DECIMAL(res_fact."FEV_VALUE", 34 ,6)
		END AS "AmountInPositionCurrency",
		CASE 
			WHEN kv2."Value" = 'Transaction'
				THEN TO_DECIMAL(res_fact."FEV_VALUE", 34 ,6)
		END AS "AmountInTransactionCurrency",
		"com.adweko.adapter.osx.resultdata.common::get_SAPpercentStandard"(
				i_PercentValue => res_fact."NOMINAL_INTEREST_RATE",
				i_SourceFieldName => 'NOMINAL_INTEREST_RATE',
				i_TargetFieldName => 'ApplicableInterestRate',
				i_SourceTableName => 'RES_FINANCIAL_EVENT',
				i_Switch => kv."Value") AS "ApplicableInterestRate",
		CASE
			WHEN LEFT(res_fact."SOURCE_SYSTEM_RECORD_NUM", 4) <> 'I-_-'
				THEN 'ContractCashFlowStream'
			ELSE 'InstrumentCashFlowStream'
		END AS "CashFlowCategory",
		CASE WHEN dis."CashFlowItemCategory" = ''
				THEN NULL
			ELSE dis."CashFlowItemCategory"
		END AS "CashFlowItemCategory",
		dis."CashFlowItemType" AS "CashFlowItemType",
		NULL AS "ContractCashFlowCategory",
        NULL AS "CreationType",
		CAST(res_fact."CURRENCY_DEF_NAME" AS NVARCHAR(3)) AS "CurrencyOfPrincipalAmount",
		CASE 
			WHEN kv2."Value" = 'Position'
				THEN TO_DECIMAL(res_fact."FEV_NPV", 34 ,6)
		END AS "DiscountedAmountInPositionCurrency",
		CASE
			WHEN kv2."Value" = 'Transaction'
				THEN TO_DECIMAL(res_fact."FEV_NPV", 34, 6)
		END AS "DiscountedAmountInTransactionCurrency",
		"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(res_fact."FEV_PAYM_COMP_JULIAN") AS "DueDate",
		NULL AS "InstrumentCashFlowCategory",
		NULL AS "InterestFixingDate",
		CASE
			WHEN
				dis."CashFlowItemCategory" = 'InterestCashFlowItem'
				THEN
					"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(res_fact."AI_END_DATE")
				ELSE NULL
		END AS "InterestPeriodEndDate",
		CASE
			WHEN
				dis."CashFlowItemCategory" = 'InterestCashFlowItem'
				THEN
					"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(res_fact."AI_BEGIN_DATE")
				ELSE NULL
		END AS "InterestPeriodStartDate",
		TO_DECIMAL(NULL, 34, 6) AS "InvestmentHoldingNominalAmount",
		NULL AS "InvestmentHoldingNominalAmountCurrency",
		TO_DECIMAL(NULL, 34, 6) AS "InvestmentHoldingQuantity",
		NULL AS "InvestmentHoldingUnit",
		TO_INTEGER(NULL) AS "NumberOfBaseDaysUsedForInterestCalculation",
		TO_INTEGER(NULL) AS "NumberOfDaysUsedForInterestCalculation",
		CASE 
			WHEN kv2."Value" = 'Position'
				THEN res_fact."CURRENCY_DEF_NAME"
		END AS "PositionCurrency",
		NULL AS "PostingDirection",
		TO_DECIMAL(res_fact."PRINCIPAL", 34, 6) AS "PrincipalAmount", 
		"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(res_fact."FEV_PAYM_COMP_JULIAN") AS "SettledDate",
		TO_DECIMAL(NULL, 15, 11) AS  "SpreadIncludedInApplicableRate",
		NULL AS "TradeDate",
		CASE 
			WHEN kv2."Value" = 'Transaction'
				THEN res_fact."CURRENCY_DEF_NAME"
		END AS "TransactionCurrency",
		--CAST(res_fact."CURRENCY_DEF_NAME" AS NVARCHAR(3)) AS "TransactionCurrency",
		TO_DECIMAL(NULL, 34, 6) AS "UnsignedAmountInPositionCurrency",
		TO_DECIMAL(NULL, 34, 6) AS "UnsignedAmountInTransactionCurrency",
		TO_DECIMAL(res_fact."DISCOUNT_FACTOR", 15, 11) AS "UsedDiscountFactor",
		CASE
			WHEN res_fact."SPOT_FX_TO_BASE" IS NOT NULL
				THEN TO_DECIMAL(res_fact."SPOT_FX_TO_BASE", 34, 6)
			ELSE TO_DECIMAL(res_fact."FORWARD_FX_TO_BASE", 34, 6)
		END AS "UsedExchangeRate",
		"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(res_fact."FEV_CALC_COMP_JULIAN") AS "ValueDate",
		CAST("com.adweko.adapter.osx.resultdata.common::get_ResultProvider"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS NVARCHAR(128)) AS "SourceSystemID",
		NULL AS "ChangeTimestampInSourceSystem",
		NULL AS "ChangingUserInSourceSystem",
		NULL AS "ChangingProcessType",
		NULL AS "ChangingProcessID"
	/*	ROW_NUMBER() OVER (PARTITION BY res_fact."SOURCE_SYSTEM_RECORD_NUM", res_fact."SOURCE_SYSTEM_NAME", res_fact."STAT_SCEN_NAME" ORDER BY res_fact."FEV_CALC_COMP_JULIAN" asc, res_fact."DISCRIMINATOR" asc, res_fact."RES_FINANCIAL_EVENT_ID" asc) AS "Row_Num"*/
		FROM "com.adweko.adapter.osx.synonyms::RES_FINANCIAL_EVENT" AS res_fact
		INNER JOIN "com.adweko.adapter.osx.synonyms::RES_SOLVE" AS slv 
		ON (res_fact."SLV_JOB_ID"	= slv."SLV_JOB_ID" 
		AND res_fact."MODEL_ID" 	= slv."MODEL_ID")
		INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_base_currency"(:I_SOLVE_JOB_ID, :I_MODEL_ID ) AS basecur
		ON basecur."SLV_JOB_ID" = :I_SOLVE_JOB_ID
		AND basecur."MODEL_ID"	= :I_MODEL_ID
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(current_Date , current_utctimestamp)  AS kv2
		ON  kv2."KeyID" = 'OutboundTransactionOrPosition'
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(current_Date , current_utctimestamp)  AS kv
		ON  kv."KeyID" = 'SAPPercentageStandard'
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(current_Date , current_utctimestamp)  AS statresgr
		ON  kv."KeyID" = 'StaticResultGroup'
		INNER JOIN "com.adweko.adapter.osx.mappings::map_Discriminator_View"(current_Date , current_utctimestamp) AS Dis
		ON Dis."Discriminator" = res_fact."DISCRIMINATOR"
		LEFT OUTER JOIN "com.adweko.adapter.osx.cashflows::get_CashFlowEndDate"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS enddate
		ON res_fact."STAT_SCEN_NAME" = enddate."STAT_SCEN_NAME"
		AND res_fact."SOURCE_SYSTEM_RECORD_NUM" = enddate."SOURCE_SYSTEM_RECORD_NUM"
		LEFT OUTER JOIN "com.adweko.adapter.osx.resultdata.common::get_businessValidity"(:I_SOLVE_JOB_ID, :I_MODEL_ID ) AS validity
		ON validity."SLV_JOB_ID" = :I_SOLVE_JOB_ID
		AND validity."MODEL_ID"	= :I_MODEL_ID
		
		WHERE res_fact."SLV_JOB_ID" = :I_SOLVE_JOB_ID
		AND res_fact."MODEL_ID" 	= :I_MODEL_ID
		AND res_fact."SOURCE_SYSTEM_RECORD_NUM" IS NOT NULL
	)
	);
END;