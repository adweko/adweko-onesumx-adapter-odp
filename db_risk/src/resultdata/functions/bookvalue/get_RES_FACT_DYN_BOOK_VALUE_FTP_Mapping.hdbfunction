FUNCTION "com.adweko.adapter.osx.bookvalue::get_RES_FACT_DYN_BOOK_VALUE_FTP_Mapping"( 
	IN I_SOLVE_JOB_ID BIGINT,
	IN I_MODEL_ID BIGINT
	)
	RETURNS TABLE(
		"RES_FACT_DYN_BOOK_VALUE_ID" BIGINT,
		"CashFlowItemType" NVARCHAR (80),
        "FiscalYear" NVARCHAR(10),
		"MarginCategory" NVARCHAR (100),
        "PostingDate" DATE,
        "PostingDirection" NVARCHAR(6),
		"RoleOfCurrency" NVARCHAR (40),
		"Scenario" NVARCHAR (100),
		"SplittingTranche" NVARCHAR (256),
		"_BusinessPartner.BusinessPartnerID" NVARCHAR (128),
		"_DynamicTimeBucket.MaturityBandID" NVARCHAR (128),
		"_DynamicTimeBucket.TimeBucketID" NVARCHAR (128),
		"_FinancialContract.FinancialContractID" NVARCHAR (128),
		"_FinancialContract.IDSystem" NVARCHAR (40),
		"_FinancialInstrument.FinancialInstrumentID" NVARCHAR (128),
		"_OrganizationalUnit.IDSystem" NVARCHAR (40),
		"_OrganizationalUnit.OrganizationalUnitID" NVARCHAR (128),
		"_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" NVARCHAR (128),
		"_PlanBudgetForecast.ID" NVARCHAR (128),
	    "_PlanBudgetForecast.PlanBudgetForecastScenario" NVARCHAR (100),
	    "_PlanBudgetForecast.VersionID" NVARCHAR (128),
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" NVARCHAR (3),
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" NVARCHAR (128),
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" NVARCHAR (40),
		"_ProductCatalogItem.ProductCatalogItem" NVARCHAR (128),
		"_ProductCatalogItem._ProductCatalog.CatalogID" NVARCHAR (128),
		"_ResultGroup.ResultDataProvider" NVARCHAR (256),
		"_ResultGroup.ResultGroupID" NVARCHAR (128),
		"_RiskReportingNode.RiskReportingNodeID" NVARCHAR (128),
		"_SecuritiesAccount.FinancialContractID" NVARCHAR (128),
		"_SecuritiesAccount.IDSystem" NVARCHAR (40),
		"_TimeBucket.MaturityBandID" NVARCHAR (128),
		"_TimeBucket.TimeBucketID" NVARCHAR (128),
		"BusinessValidFrom" DATE ,
		"BusinessValidTo" DATE ,
		"Bindingness" NVARCHAR (100),
		"CalculationReason" NVARCHAR (100),
		"DynamicTimeBucketEndDate" DATE ,
		"DynamicTimeBucketStartDate" DATE ,
		"MarginAmount" DECIMAL (34,6),
		"MarginAmountCurrency" NVARCHAR (3),
		"MarginRate" DECIMAL (15,11),
		"OverallCalculationMethod" NVARCHAR (100),
		"TimeBucketEndDate" DATE ,
		"TimeBucketStartDate" DATE ,
		"SourceSystemID" NVARCHAR (128),
		"ChangeTimestampInSourceSystem" TIMESTAMP ,
		"ChangingUserInSourceSystem" NVARCHAR (128),
		"ChangingProcessType" NVARCHAR (40),
		"ChangingProcessID" NVARCHAR (128)
	)
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN
   SELECT
		MIN("RES_FACT_DYN_BOOK_VALUE_ID") AS "RES_FACT_DYN_BOOK_VALUE_ID",
		"CashFlowItemType",
        "FiscalYear",
		"MarginCategory",
		"PostingDate",
        -- derive posting direction by looking at the sign of the amount
        CASE
            WHEN SUM("MarginAmount") > 0
                THEN 'Credit'
            WHEN SUM("MarginAmount") < 0
                THEN 'Debit'
            ELSE NULL
        END AS "PostingDirection",
		"RoleOfCurrency",
		"Scenario",
		"SplittingTranche",
		"_BusinessPartner.BusinessPartnerID",
		"_DynamicTimeBucket.MaturityBandID",
		"_DynamicTimeBucket.TimeBucketID",
		"_FinancialContract.FinancialContractID",
		"_FinancialContract.IDSystem",
		"_FinancialInstrument.FinancialInstrumentID",
		"_OrganizationalUnit.IDSystem",
		"_OrganizationalUnit.OrganizationalUnitID",
		"_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
		"_PlanBudgetForecast.ID",
	    "_PlanBudgetForecast.PlanBudgetForecastScenario",
	    "_PlanBudgetForecast.VersionID",
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
		"_ProductCatalogItem.ProductCatalogItem",
		"_ProductCatalogItem._ProductCatalog.CatalogID",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		"_RiskReportingNode.RiskReportingNodeID",
		"_SecuritiesAccount.FinancialContractID",
		"_SecuritiesAccount.IDSystem",
		"_TimeBucket.MaturityBandID",
		"_TimeBucket.TimeBucketID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"Bindingness",
		"CalculationReason",
		"DynamicTimeBucketEndDate",
		"DynamicTimeBucketStartDate",
		SUM("MarginAmount") AS "MarginAmount",
		"MarginAmountCurrency",
		AVG("MarginRate") AS "MarginRate",
		"OverallCalculationMethod",
		"TimeBucketEndDate",
		"TimeBucketStartDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType" ,
		"ChangingProcessID"

	FROM (
    	SELECT
    	"RES_FACT_DYN_BOOK_VALUE_ID",
    	"CashFlowItemType",
        "FiscalYear",
		"MarginCategory",
		"PostingDate",
        "PostingDirection",
		"RoleOfCurrency",
		"Scenario",
		"SplittingTranche",
		"_BusinessPartner.BusinessPartnerID",
		"_DynamicTimeBucket.MaturityBandID",
		"_DynamicTimeBucket.TimeBucketID",
		"_FinancialContract.FinancialContractID",
		"_FinancialContract.IDSystem",
		"_FinancialInstrument.FinancialInstrumentID",
		"_OrganizationalUnit.IDSystem",
		"_OrganizationalUnit.OrganizationalUnitID",
		"_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
		"_PlanBudgetForecast.ID",
	    "_PlanBudgetForecast.PlanBudgetForecastScenario",
	    "_PlanBudgetForecast.VersionID",
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
		"_ProductCatalogItem.ProductCatalogItem",
		"_ProductCatalogItem._ProductCatalog.CatalogID",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		-- pivot for ChartOfAccounts and ClassificationTree
			MAP(ser3."ELEMENT_NUMBER",
			1, "Account_RiskReportingNode.RiskReportingNodeID",
			2, "Classification01_RiskReportingNode.RiskReportingNodeID",
			3, "Classification02_RiskReportingNode.RiskReportingNodeID",
			4, "Classification03_RiskReportingNode.RiskReportingNodeID",
			5, "Classification04_RiskReportingNode.RiskReportingNodeID",
			6, "Classification05_RiskReportingNode.RiskReportingNodeID",
			7, "Classification06_RiskReportingNode.RiskReportingNodeID",
			8, "Classification07_RiskReportingNode.RiskReportingNodeID",
			9, "Classification08_RiskReportingNode.RiskReportingNodeID",
			10, "Classification09_RiskReportingNode.RiskReportingNodeID",
			11, "Classification10_RiskReportingNode.RiskReportingNodeID",
			12, "Classification11_RiskReportingNode.RiskReportingNodeID",
			13, "Classification12_RiskReportingNode.RiskReportingNodeID",
			14, "Classification13_RiskReportingNode.RiskReportingNodeID",
			15, "Classification14_RiskReportingNode.RiskReportingNodeID",
			16, "Classification15_RiskReportingNode.RiskReportingNodeID")
		AS "_RiskReportingNode.RiskReportingNodeID",
		"_SecuritiesAccount.FinancialContractID",
		"_SecuritiesAccount.IDSystem",
		"_TimeBucket.MaturityBandID",
		"_TimeBucket.TimeBucketID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"Bindingness",
		"CalculationReason",
		"DynamicTimeBucketEndDate",
		"DynamicTimeBucketStartDate",
		"MarginAmount",
		"MarginAmountCurrency",
		"MarginRate",
		"OverallCalculationMethod",
		"TimeBucketEndDate",
		"TimeBucketStartDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType" ,
		"ChangingProcessID"

	FROM (
    	SELECT
    		"RES_FACT_DYN_BOOK_VALUE_ID",
    		"CashFlowItemType",
            "FiscalYear",
    		MAP(ser2."ELEMENT_NUMBER",
				1, 'TransferRateIncome',
				2, 'TreasurySpreadIncome',
				3, 'TreasuryRateIncome',
				4, 'BoniMaliSpreadIncome',
				5, 'CommitmentSpreadIncome',
				6, 'LiquiditySpreadIncome',
				7, 'CreditRiskSpreadIncome',
				8, 'InstitutionSpreadIncome',
				9, 'EffectiveCapital',
				10, 'DailyTransferRateIncome',
				11, 'DailyTreasuryRateIncome',
				12, 'DailyTreasurySpreadIncome',
				13, 'DailyBoniMaliSpreadIncome',
				14, 'DailyCommitmentSpreadIncome',
				15, 'DailyLiquiditySpreadIncome',
				16, 'DailyCreditRiskSpreadIncome',
				17, 'DailyInstitutionSpreadIncome',
				18, 'InterestIncome',
				19, 'OtherIncomeOrExpenses',
				20, 'FXIncome',
				21, 'SaleIncome',
				22, 'PrepaymentIncome',
				23, 'FeeIncome',
				24, 'DailyInterestIncome',
				25, 'CoveredLiquidityIncome',
			    26, 'UncoveredLiquidityIncome'
				)
			AS "MarginCategory",
			"PostingDate",
            "PostingDirection",
			"RoleOfCurrency",
			"Scenario",
			"SplittingTranche",
			"_BusinessPartner.BusinessPartnerID",
			"_DynamicTimeBucket.MaturityBandID",
			"_DynamicTimeBucket.TimeBucketID",
			"_FinancialContract.FinancialContractID",
			"_FinancialContract.IDSystem",
			"_FinancialInstrument.FinancialInstrumentID",
			"_OrganizationalUnit.IDSystem",
			"_OrganizationalUnit.OrganizationalUnitID",
			"_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
			"_PlanBudgetForecast.ID",
	        "_PlanBudgetForecast.PlanBudgetForecastScenario",
	        "_PlanBudgetForecast.VersionID",
			"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
			"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
			"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
			"_ProductCatalogItem.ProductCatalogItem",
			"_ProductCatalogItem._ProductCatalog.CatalogID",
			"_ResultGroup.ResultDataProvider",
			"_ResultGroup.ResultGroupID",
			"Account_RiskReportingNode.RiskReportingNodeID",
			"Classification01_RiskReportingNode.RiskReportingNodeID",
			"Classification02_RiskReportingNode.RiskReportingNodeID",
			"Classification03_RiskReportingNode.RiskReportingNodeID",
			"Classification04_RiskReportingNode.RiskReportingNodeID",
			"Classification05_RiskReportingNode.RiskReportingNodeID",
			"Classification06_RiskReportingNode.RiskReportingNodeID",
			"Classification07_RiskReportingNode.RiskReportingNodeID",
			"Classification08_RiskReportingNode.RiskReportingNodeID",
			"Classification09_RiskReportingNode.RiskReportingNodeID",
			"Classification10_RiskReportingNode.RiskReportingNodeID",
			"Classification11_RiskReportingNode.RiskReportingNodeID",
			"Classification12_RiskReportingNode.RiskReportingNodeID",
			"Classification13_RiskReportingNode.RiskReportingNodeID",
			"Classification14_RiskReportingNode.RiskReportingNodeID",
			"Classification15_RiskReportingNode.RiskReportingNodeID",
			"_SecuritiesAccount.FinancialContractID",
			"_SecuritiesAccount.IDSystem",
			"_TimeBucket.MaturityBandID",
			"_TimeBucket.TimeBucketID",
			"BusinessValidFrom",
			"BusinessValidTo",
			"Bindingness",
			"CalculationReason",
			"DynamicTimeBucketEndDate",
			"DynamicTimeBucketStartDate",
			MAP(ser2."ELEMENT_NUMBER",
				1, "TransferRateIncome",
				2, "TreasurySpreadIncome",
				3, "TreasuryRateIncome",
				4, "BoniMaliSpreadIncome",
				5, "CommitmentSpreadIncome",
				6, "LiquiditySpreadIncome",
				7, "CreditRiskSpreadIncome",
				8, "InstitutionSpreadIncome",
				9, "EffectiveCapital",
				10, "DailyTransferRateIncome",
				11, "DailyTreasuryRateIncome",
				12, "DailyTreasurySpreadIncome",
				13, "DailyBoniMaliSpreadIncome",
				14, "DailyCommitmentSpreadIncome",
				15, "DailyLiquiditySpreadIncome",
				16, "DailyCreditRiskSpreadIncome",
				17, "DailyInstitutionSpreadIncome",
				18, "InterestIncome",
				19, "OtherIncomeOrExpenses",
				20, "FXIncome",
				21, "SaleIncome",
				22, "PrepaymentIncome",
				23, "FeeIncome",
				24, "DailyInterestIncome",
				25, "CoveredLiquidityIncome",
			    26, "UncoveredLiquidityIncome"
			)
			AS "MarginAmount",
			"MarginAmountCurrency",
			"MarginRate",
			"OverallCalculationMethod",
			"TimeBucketEndDate",
			"TimeBucketStartDate",
			"SourceSystemID",
			"ChangeTimestampInSourceSystem",
			"ChangingUserInSourceSystem",
			"ChangingProcessType" ,
			"ChangingProcessID"
		
			FROM (
    			SELECT
    				res_fact."RES_FACT_DYN_BOOK_VALUE_ID" AS "RES_FACT_DYN_BOOK_VALUE_ID",
    				NULL AS "CashFlowItemType",
                    NULL AS "FiscalYear",
    				NULL AS "MarginCategory",
    				NULL AS "PostingDate",
                    NULL AS "PostingDirection",
    				MAP(ser."ELEMENT_NUMBER",
						1, 'FunctionalCurrency',
						2, 'ContractCurrency'
					) AS "RoleOfCurrency",
					CASE 
						WHEN res_fact."DYN_WHATIF_ID" IS NOT NULL 
							THEN wi."WHATIF_NAME" 
						ELSE CAST(res_fact."DYN_MC_RUN_NUMBER" AS NVARCHAR(100))
					END AS "Scenario",
					NULL AS "SplittingTranche",
					NULL AS "_BusinessPartner.BusinessPartnerID",
					tbs."_TimeBucket.TimeBucketID" AS "_DynamicTimeBucket.TimeBucketID",
					tbs."_TimeBucket.MaturityBandID" AS "_DynamicTimeBucket.MaturityBandID",
					CASE
						WHEN LEFT(IFNULL(res_fact."CONTRACT_NAME",''), 4) <> 'I-_-' 
							THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_DYN_BOOK_VALUE_ID" AS NVARCHAR (128)))
						ELSE NULL
					END AS "_FinancialContract.FinancialContractID",
					CASE
						WHEN LEFT(res_fact."CONTRACT_NAME", 4) <> 'I-_-'
							THEN CAST("com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME") AS NVARCHAR(40))
						ELSE NULL
					END AS "_FinancialContract.IDSystem",
					CASE
						WHEN LEFT(IFNULL(res_fact."CONTRACT_NAME",''), 4) = 'I-_-'
							THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialInstrumentID"(res_fact."CONTRACT_NAME") 
						ELSE NULL	
					END AS "_FinancialInstrument.FinancialInstrumentID",
					NULL AS "_OrganizationalUnit.IDSystem",
					NULL AS "_OrganizationalUnit.OrganizationalUnitID",
					NULL AS "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
					NULL AS "_PlanBudgetForecast.ID",
	                NULL AS "_PlanBudgetForecast.PlanBudgetForecastScenario",
	                NULL AS "_PlanBudgetForecast.VersionID",
					"com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."CONTRACT_NAME") AS "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
					CASE
						WHEN "com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."CONTRACT_NAME") IS NOT NULL
							THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_DYN_BOOK_VALUE_ID" AS NVARCHAR (128)))
							ELSE NULL
					END AS "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
					CASE
						WHEN "com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."CONTRACT_NAME") IS NOT NULL
							THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME")
						ELSE NULL
					END AS "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
					NULL AS "_ProductCatalogItem.ProductCatalogItem",
					NULL AS "_ProductCatalogItem._ProductCatalog.CatalogID",
					CAST("com.adweko.adapter.osx.resultdata.common::get_ResultProvider"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS NVARCHAR(128)) AS "_ResultGroup.ResultDataProvider",
					"com.adweko.adapter.osx.resultdata.common::get_ResulGroupId"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS "_ResultGroup.ResultGroupID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."ACCOUNT_ID") AS "Account_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_01") AS "Classification01_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_02") AS "Classification02_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_03") AS "Classification03_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_04") AS "Classification04_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_05") AS "Classification05_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_06") AS "Classification06_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_07") AS "Classification07_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_08") AS "Classification08_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_09") AS "Classification09_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_10") AS "Classification10_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_11") AS "Classification11_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_12") AS "Classification12_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_13") AS "Classification13_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_14") AS "Classification14_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_15") AS "Classification15_RiskReportingNode.RiskReportingNodeID",
					"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."ACCOUNT_ID") AS "_RiskReportingNode.RiskReportingNodeID",
					CASE
						WHEN LEFT(res_fact."CONTRACT_NAME", 4) = 'I-_-'
							THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_DYN_BOOK_VALUE_ID" AS NVARCHAR (128)))
						ELSE NULL
					END AS "_SecuritiesAccount.FinancialContractID",
					CASE
						WHEN LEFT(res_fact."CONTRACT_NAME", 4) = 'I-_-'
							THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME")
						ELSE NULL
					END AS "_SecuritiesAccount.IDSystem",
					NULL AS "_TimeBucket.MaturityBandID",
					NULL AS "_TimeBucket.TimeBucketID",
					"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(slv."ANALYSIS_DATE") AS "BusinessValidFrom",
					"com.adweko.adapter.osx.resultdata.common::get_MaxDate"() AS "BusinessValidTo",
					NULL AS "Bindingness",
					NULL AS "CalculationReason",
					tbs."BUCKET_START" AS "DynamicTimeBucketStartDate",  
					tbs."BUCKET_END" AS "DynamicTimeBucketEndDate",
					NULL AS "MarginAmount",
					MAP(ser."ELEMENT_NUMBER",
						1, basecur."CURRENCY_NAME",
						2, contcur."CURRENCY_NAME"
					) AS "MarginAmountCurrency",
					NULL AS "MarginRate",
					'DynamicSimulatedBookValueAnalysis' AS "OverallCalculationMethod",
    				NULL AS "TimeBucketEndDate",
					NULL AS "TimeBucketStartDate",
					NULL AS "SourceSystemID",
					NULL AS "ChangeTimestampInSourceSystem",
					NULL AS "ChangingUserInSourceSystem",
					NULL AS "ChangingProcessType",
					NULL AS "ChangingProcessID",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."TRANS_RATE_INCOME_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."TRANS_RATE_INCOME_CT_CUR", 34, 6)
					) AS "TransferRateIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."TREAS_RATE_INCOME_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."TREAS_RATE_INCOME_CT_CUR", 34, 6)
					) AS "TreasuryRateIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."TREAS_SPREAD_INCOME_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."TREAS_SPREAD_INCOME_CT_CUR", 34, 6)
					) AS "TreasurySpreadIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."BONI_MALI_SPREAD_IN_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."BONI_MALI_SPREAD_IN_CT_CUR", 34, 6)
					) AS "BoniMaliSpreadIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."COMMITMENT_SPREAD_IN_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."COMMITMENT_SPREAD_IN_CT_CUR", 34, 6)
					) AS "CommitmentSpreadIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."LIQ_SPREAD_INCOME_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."LIQ_SPREAD_INCOME_CT_CUR", 34, 6)
					) AS "LiquiditySpreadIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."CR_RISK_SPREAD_IN_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."CR_RISK_SPREAD_IN_CT_CUR", 34, 6)
					) AS "CreditRiskSpreadIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."INSTIT_SPREAD_IN_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."INSTIT_SPREAD_IN_CT_CUR", 34, 6)
					) AS "InstitutionSpreadIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."FTP_REFERENCE_VALUE_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."FTP_REFERENCE_VALUE_CT_CUR", 34, 6)
					) AS "EffectiveCapital",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."D_TRANS_RATE_IN_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."D_TRANS_RATE_IN_CT_CUR", 34, 6)
					) AS "DailyTransferRateIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."D_TREAS_RATE_IN_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."D_TREAS_RATE_IN_CT_CUR", 34, 6)
					) AS "DailyTreasuryRateIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."D_TREAS_SPREAD_IN_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."D_TREAS_SPREAD_IN_CT_CUR", 34, 6)
					) AS "DailyTreasurySpreadIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."D_BONI_MALI_SPREAD_IN_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."D_BONI_MALI_SPREAD_IN_CT_CUR", 34, 6)
					) AS "DailyBoniMaliSpreadIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."D_COMMITMENT_SPREAD_IN_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."D_COMMITMENT_SPREAD_IN_CT_CUR", 34, 6)
					) AS "DailyCommitmentSpreadIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."D_LIQ_SPREAD_IN_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."D_LIQ_SPREAD_IN_CT_CUR", 34, 6)
					) AS "DailyLiquiditySpreadIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."D_CR_RISK_SPREAD_IN_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."D_CR_RISK_SPREAD_IN_CT_CUR", 34, 6)
					) AS "DailyCreditRiskSpreadIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."D_INSTIT_SPREAD_IN_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."D_INSTIT_SPREAD_IN_CT_CUR", 34, 6)
					) AS "DailyInstitutionSpreadIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."INTEREST_INCOME_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."INTEREST_INCOME_CT_CUR", 34, 6)
					) AS "InterestIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."OTHER_INCOME_EXPENSE_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."OTHER_INCOME_EXPENSE_CT_CUR", 34, 6)
					) AS "OtherIncomeOrExpenses",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."FX_INCOME_EXPENSE_BAS_CUR", 34, 6),
						2, NULL
					) AS "FXIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."SALE_INCOME_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."SALE_INCOME_CT_CUR", 34, 6)
					) AS "SaleIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."PREPAYMENT_INCOME_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."PREPAYMENT_INCOME_CT_CUR", 34, 6)
					) AS "PrepaymentIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."FEE_INCOME_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."FEE_INCOME_CT_CUR", 34, 6)
					) AS "FeeIncome",
					MAP(ser."ELEMENT_NUMBER",
						1, TO_DECIMAL(res_fact."DAILY_INTEREST_INCOME_BAS_CUR", 34, 6),
						2, TO_DECIMAL(res_fact."DAILY_INTEREST_INCOME_CT_CUR", 34, 6)
					) AS "DailyInterestIncome",
					MAP(ser."ELEMENT_NUMBER",
			        	1, TO_DECIMAL(res_fact."FDS_COVLIQ_INCOME_BAS_CUR", 34, 6),
			        	2, TO_DECIMAL(res_fact."FDS_COVLIQ_INCOME_CT_CUR", 34, 6)
			        ) AS "CoveredLiquidityIncome",
			        MAP(ser."ELEMENT_NUMBER",
			        	1, TO_DECIMAL(res_fact."FDS_UNCLIQ_INCOME_BAS_CUR", 34, 6),
			        	2, TO_DECIMAL(res_fact."FDS_UNCLIQ_INCOME_CT_CUR", 34, 6)
			        ) AS "UncoveredLiquidityIncome",
					ROW_NUMBER() OVER (PARTITION BY res_fact."DYN_WHATIF_ID",
													COALESCE(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_DYN_BOOK_VALUE_ID" AS NVARCHAR (255))),
													ser."ELEMENT_NUMBER",
													res_fact."DYN_SIMUL_BUCKET_IDX"
													) AS "Row_Num"
												
					FROM "com.adweko.adapter.osx.synonyms::RES_FACT_DYN_BOOK_VALUE" AS res_fact
					INNER JOIN "com.adweko.adapter.osx.synonyms::RES_DIM_ACCOUNT" AS acc
  					ON  res_fact."ACCOUNT_ID"	= acc."ACCOUNT_ID" 
  					AND res_fact."SLV_JOB_ID"	= acc."SLV_JOB_ID"
					AND acc."IS_VISIBLE"		= true
					INNER JOIN "com.adweko.adapter.osx.synonyms::RES_SOLVE" AS slv 
					ON (res_fact."SLV_JOB_ID"	= slv."SLV_JOB_ID" 
					AND res_fact."MODEL_ID" 	= slv."MODEL_ID")
					INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_static_tbs"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS tbs
					ON (tbs."BUCKET_NB" = res_fact."DYN_SIMUL_BUCKET_IDX"
   					AND tbs."RES_DIM_TBS_KIND"='DSIM')
					LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_dynamic_whatif"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS wi
					ON (wi."RES_SOLVE_ID"	= slv."RES_SOLVE_ID"
					AND wi."WHATIF_ID"		= res_fact."DYN_WHATIF_ID")
					INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_base_currency"(:I_SOLVE_JOB_ID, :I_MODEL_ID ) AS basecur
					ON basecur."SLV_JOB_ID" = :I_SOLVE_JOB_ID
					AND basecur."MODEL_ID"	= :I_MODEL_ID
					LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_ct_ev_currency"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS contcur
					ON contcur."SLV_JOB_ID" 	= :I_SOLVE_JOB_ID
					AND contcur."SLV_JOB_ID"	= :I_SOLVE_JOB_ID
					AND contcur."CURRENCY_ID"	= res_fact."CONTRACT_CURRENCY_ID"
					LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_generate_multiple_rows"(1, 1, 3) AS ser
					ON true = true
					
					WHERE res_fact."SLV_JOB_ID" = :I_SOLVE_JOB_ID
					AND res_fact."MODEL_ID" 	= :I_MODEL_ID)
			
			LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_generate_multiple_rows"(1, 1, 27) AS ser2
			ON true = true
	
			WHERE 
				"Row_Num" = 1
			)
				-- pivot for ChartOfAccounts and ClassificationTree
		LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_generate_multiple_rows"(1, 1, 17) AS ser3
		ON true = true
		WHERE
			"MarginAmount" IS NOT NULL)
			WHERE "_RiskReportingNode.RiskReportingNodeID" IS NOT NULL
			
		GROUP BY
		"CashFlowItemType",
        "FiscalYear",
		"MarginCategory",
		"PostingDate",
        "PostingDirection",
		"RoleOfCurrency",
		"Scenario",
		"SplittingTranche",
		"_BusinessPartner.BusinessPartnerID",
		"_DynamicTimeBucket.MaturityBandID",
		"_DynamicTimeBucket.TimeBucketID",
		"_FinancialContract.FinancialContractID",
		"_FinancialContract.IDSystem",
		"_FinancialInstrument.FinancialInstrumentID",
		"_OrganizationalUnit.IDSystem",
		"_OrganizationalUnit.OrganizationalUnitID",
		"_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
		"_PlanBudgetForecast.ID",
	    "_PlanBudgetForecast.PlanBudgetForecastScenario",
	    "_PlanBudgetForecast.VersionID",
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
		"_ProductCatalogItem.ProductCatalogItem",
		"_ProductCatalogItem._ProductCatalog.CatalogID",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		"_RiskReportingNode.RiskReportingNodeID",
		"_SecuritiesAccount.FinancialContractID",
		"_SecuritiesAccount.IDSystem",
		"_TimeBucket.MaturityBandID",
		"_TimeBucket.TimeBucketID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"Bindingness",
		"CalculationReason",
		"DynamicTimeBucketEndDate",
		"DynamicTimeBucketStartDate",
		"MarginAmountCurrency",
		"OverallCalculationMethod",
		"TimeBucketEndDate",
		"TimeBucketStartDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType" ,
		"ChangingProcessID";
END;