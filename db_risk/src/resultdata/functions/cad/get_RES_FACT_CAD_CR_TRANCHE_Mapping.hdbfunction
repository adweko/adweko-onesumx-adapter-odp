FUNCTION "com.adweko.adapter.osx.cad::get_RES_FACT_CAD_CR_TRANCHE_Mapping"(
	IN I_SOLVE_JOB_ID BIGINT,
	IN I_MODEL_ID BIGINT
	)
       RETURNS TABLE(
       	"RES_FACT_CAD_CR_TRANCHE_ID" BIGINT,
		"CreditRiskCalculationLevel" NVARCHAR (100),
		"CreditRiskCalculationMethod" NVARCHAR (20),
		"CreditRiskSplitPartType" NVARCHAR (100),
		"CreditRiskType" NVARCHAR (80),
		"OffBalanceSheet" BOOLEAN ,
		"ReducedRiskWeight" BOOLEAN ,
		"RiskProvisionScenario" NVARCHAR (100),
		"ASSOC_CollateralPortion.PortionNumber" INTEGER ,
		"ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" NVARCHAR (128),
		"ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" NVARCHAR (40),
		"ASSOC_FinancialContract.FinancialContractID" NVARCHAR (128),
		"ASSOC_FinancialContract.IDSystem" NVARCHAR (40),
		"_CollectionOfInstruments.CollectionID" NVARCHAR (128),
		"_CollectionOfInstruments.IDSystem" NVARCHAR (40),
		"_CollectionOfInstruments._Client.BusinessPartnerID" NVARCHAR (128),
		"_CombinedCollateralAgreement.FinancialContractID" NVARCHAR (128),
		"_CombinedCollateralAgreement.IDSystem" NVARCHAR (40),
		"_FinancialContractUsedAsCollateral.FinancialContractID" NVARCHAR (128),
		"_FinancialContractUsedAsCollateral.IDSystem" NVARCHAR (40),
		"_FinancialInstrument.FinancialInstrumentID" NVARCHAR (128),
		"_FinancialInstrumentUsedAsCollateral.FinancialInstrumentID" NVARCHAR (128),
		"_PhysicalAssetUsedAsCollateral.PhysicalAssetID" NVARCHAR (168),
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" NVARCHAR (3),
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" NVARCHAR (128),
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" NVARCHAR (40),
		"_ReceivableUsedAsCollateral.ReceivableID" NVARCHAR (168),
		"_ResultGroup.ResultDataProvider" NVARCHAR (256),
		"_ResultGroup.ResultGroupID" NVARCHAR (128),
		"_SecuritiesAccount.FinancialContractID" NVARCHAR (128),
		"_SecuritiesAccount.IDSystem" NVARCHAR (40),
		"_SecuritiesAccountOfFinancialInstrumentUsedAsCollateral.FinancialContractID" NVARCHAR (128),
		"_SecuritiesAccountOfFinancialInstrumentUsedAsCollateral.IDSystem" NVARCHAR (40),
		"_SimpleCollateralAgreement.FinancialContractID" NVARCHAR (128),
		"_SimpleCollateralAgreement.IDSystem" NVARCHAR (40),
		"BusinessValidFrom" DATE ,
		"BusinessValidTo" DATE ,
		"_OriginalCounterparty.BusinessPartnerID" NVARCHAR (128),
		"_UsedBusinessPartner.BusinessPartnerID" NVARCHAR (128),
		"AddOnAmount" DECIMAL (34,6),
		"AddOnFactor" DECIMAL (15,11),
		"ApplicableMasterRating" NVARCHAR (10),
		"ApplicableRating" NVARCHAR (10),
		"ApplicableRatingAgency" NVARCHAR (128),
		"ApplicableRatingCategory" NVARCHAR (40),
		"ApplicableRatingIsFxRating" BOOLEAN ,
		"ApplicableRatingMethod" NVARCHAR (60),
		"ApplicableRatingTimeHorizon" NVARCHAR (10),
		"AverageScalingFactorForSmallMidsizeEnterprise" DECIMAL (15,11),
		"CalculationApproach" NVARCHAR (100),
		"CalculationSubApproach" NVARCHAR (100),
		"CreditConversionFactor" DECIMAL (15,11),
		"CreditConversionFactorClass" NVARCHAR (100),
		"Currency" NVARCHAR (3),
		"CurrentExposure" DECIMAL (34,6),
		"DeductionFromCapital" BOOLEAN ,
		"Defaulted" BOOLEAN ,
		"ExpectedLoss" DECIMAL (34,6),
		"ExposureAtDefault" DECIMAL (34,6),
		"ExposureAtDefaultCalculationBase" DECIMAL (34,6),
		"ExposureAtDefaultWeightedLossGivenDefault" DECIMAL (15,11),
		"ExposureClass" NVARCHAR (60),
		"HaircutCollateral" DECIMAL (15,11),
		"HaircutExposure" DECIMAL (15,11),
		"HaircutFX" DECIMAL (15,11),
		"LGDClass" NVARCHAR (100),
		"LossGivenDefault" DECIMAL (15,11),
		"Maturity" DECIMAL (34,6),
		"MaturityMismatchFactor" DECIMAL (15,11),
		"MaturityPeriodTimeUnit" NVARCHAR (12),
		"MaximumExposure" DECIMAL (34,6),
		"NominalAmount" DECIMAL (34,6),
		"OriginalExposureClass" NVARCHAR (60),
		"ProbabilityOfDefault" DECIMAL (15,11),
		"ProportionalExposureAtDefault" DECIMAL (34,6),
		"Quantity" DECIMAL (34,6),
		"RegulatoryCapitalRequirements" DECIMAL (34,6),
		"RegulatoryCapitalRequirementsForSecuritizationWithoutPenaltyFactor" DECIMAL (34,6),
		"RiskWeight" DECIMAL (15,11),
		"RiskWeightedAssets" DECIMAL (34,6),
		"RiskWeightedAssetsBeforeCreditRiskMitigation" DECIMAL (34,6),
		"Unit" NVARCHAR (10),
		"UsedProtectionAmount" DECIMAL (34,6),
		"UsedProtectionAmountCurrency" NVARCHAR (3),
		"SourceSystemID" NVARCHAR (128),
		"ChangeTimestampInSourceSystem" TIMESTAMP ,
		"ChangingUserInSourceSystem" NVARCHAR (128),
		"ChangingProcessType" NVARCHAR (40),
		"ChangingProcessID" NVARCHAR (128),
		"_DynamicTimeBucket.MaturityBandID" NVARCHAR (128),
		"_DynamicTimeBucket.TimeBucketID" NVARCHAR (128),
		"P_0001210074_RoleOfCurrency" NVARCHAR (40),
		"P_0001210074_DynamicTimeBucketStartDate" DATE ,
		"P_0001210074_DynamicTimeBucketEndDate" DATE ,
		"P_0001210074_PayableAmount" DECIMAL (34,6),
		"P_0001210074_Gain" DECIMAL (34,6)
       ) 
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN
	SELECT
	res_fact."RES_FACT_CAD_CR_TRANCHE_ID" AS "RES_FACT_CAD_CR_TRANCHE_ID",
	NULL AS "CreditRiskCalculationLevel",
	'StandardizedApproach' AS "CreditRiskCalculationMethod",
	NULL AS "CreditRiskSplitPartType",
	'RegulatoryCreditRiskRequirements' AS "CreditRiskType",
	false AS "OffBalanceSheet",
	false AS "ReducedRiskWeight",
	CASE 
		WHEN res_fact."DYN_WHATIF_ID" IS NOT NULL
			THEN wi."WHATIF_NAME"
		ELSE sc."STATIC_SCENARIO_NAME"
	END AS "RiskProvisionScenario",
	TO_INTEGER(NULL) AS "ASSOC_CollateralPortion.PortionNumber",
	NULL AS "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
	NULL AS "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
	CASE
		WHEN LEFT(res_fact."EXPOSURE_SSRN", 4) <> 'I-_-'
			THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."EXPOSURE_SSRN",CAST(res_fact."RES_FACT_CAD_CR_TRANCHE_ID" AS NVARCHAR (128)))
		ELSE
			NULL
	END AS "ASSOC_FinancialContract.FinancialContractID",
	CASE
		WHEN LEFT(res_fact."EXPOSURE_SSRN", 4) <> 'I-_-'
			THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."EXPOSURE_SSRN")
		ELSE
			NULL
	END AS "ASSOC_FinancialContract.IDSystem",
	NULL AS "_CollectionOfInstruments.CollectionID",
	NULL AS "_CollectionOfInstruments.IDSystem",
	NULL AS "_CollectionOfInstruments._Client.BusinessPartnerID",
	NULL AS "_CombinedCollateralAgreement.FinancialContractID",
	NULL AS "_CombinedCollateralAgreement.IDSystem",
	CASE
		WHEN LEFT(IFNULL(res_fact."ENHANCEMENT_SSRN",''), 4) <> 'I-_-'
			THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialInstrumentID"(res_fact."ENHANCEMENT_SSRN") 
		ELSE NULL	
	END AS "_FinancialContractUsedAsCollateral.FinancialContractID",
	CASE
		WHEN LEFT(IFNULL(res_fact."ENHANCEMENT_SSRN",''), 4) <> 'I-_-'
			THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."ENHANCEMENT_SSRN") 
		ELSE NULL	
	END AS "_FinancialContractUsedAsCollateral.IDSystem",
	CASE
		WHEN LEFT(IFNULL(res_fact."EXPOSURE_SSRN",''), 4) = 'I-_-'
			THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialInstrumentID"(res_fact."EXPOSURE_SSRN") 
		ELSE NULL	
	END AS "_FinancialInstrument.FinancialInstrumentID",
	CASE
		WHEN LEFT(IFNULL(res_fact."ENHANCEMENT_SSRN",''), 4) = 'I-_-'
			THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialInstrumentID"(res_fact."ENHANCEMENT_SSRN") 
		ELSE NULL	
	END AS "_FinancialInstrumentUsedAsCollateral.FinancialInstrumentID",
	NULL AS "_PhysicalAssetUsedAsCollateral.PhysicalAssetID",
	"com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."EXPOSURE_SSRN") AS "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
	CASE
		WHEN "com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."EXPOSURE_SSRN") IS NOT NULL
			THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."EXPOSURE_SSRN",CAST(res_fact."RES_FACT_CAD_CR_TRANCHE_ID" AS NVARCHAR (128)))
		ELSE NULL
	END AS "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
	CASE
		WHEN "com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."EXPOSURE_SSRN") IS NOT NULL
			THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."EXPOSURE_SSRN")
		ELSE NULL
	END AS "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
	NULL AS "_ReceivableUsedAsCollateral.ReceivableID",
	CAST("com.adweko.adapter.osx.resultdata.common::get_ResultProvider"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS NVARCHAR(128)) AS "_ResultGroup.ResultDataProvider",
	"com.adweko.adapter.osx.resultdata.common::get_ResulGroupId"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS "_ResultGroup.ResultGroupID",
	CASE
		WHEN LEFT(res_fact."EXPOSURE_SSRN", 4) = 'I-_-'
			THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."EXPOSURE_SSRN",CAST(res_fact."RES_FACT_CAD_CR_TRANCHE_ID" AS NVARCHAR (128)))
		ELSE
			NULL
	END AS "_SecuritiesAccount.FinancialContractID",
	CASE
		WHEN LEFT(res_fact."EXPOSURE_SSRN", 4) = 'I-_-'
			THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."EXPOSURE_SSRN")
		ELSE
			NULL
	END AS "_SecuritiesAccount.IDSystem",
	CASE
		WHEN LEFT(res_fact."ENHANCEMENT_SSRN", 4) = 'I-_-'
			THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."ENHANCEMENT_SSRN",CAST(res_fact."RES_FACT_CAD_CR_TRANCHE_ID" AS NVARCHAR (128)))
		ELSE
			NULL
	END AS "_SecuritiesAccountOfFinancialInstrumentUsedAsCollateral.FinancialContractID",
	CASE
		WHEN LEFT(res_fact."ENHANCEMENT_SSRN", 4) = 'I-_-'
			THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."ENHANCEMENT_SSRN")
		ELSE
			NULL
	END AS "_SecuritiesAccountOfFinancialInstrumentUsedAsCollateral.IDSystem",
	NULL AS "_SimpleCollateralAgreement.FinancialContractID",
	NULL AS "_SimpleCollateralAgreement.IDSystem",
	"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(slv."ANALYSIS_DATE") AS "BusinessValidFrom",
	"com.adweko.adapter.osx.resultdata.common::get_MaxDate"() AS "BusinessValidTo",
	NULL AS "_OriginalCounterparty.BusinessPartnerID",
	NULL AS "_UsedBusinessPartner.BusinessPartnerID",
	TO_DECIMAL(NULL, 34, 6) AS "AddOnAmount",
	TO_DECIMAL(NULL, 15, 11) AS "AddOnFactor",
	NULL AS "ApplicableMasterRating",
	NULL AS "ApplicableRating",
	NULL AS "ApplicableRatingAgency",
	NULL AS "ApplicableRatingCategory",
	false AS "ApplicableRatingIsFxRating",
	NULL AS "ApplicableRatingMethod",
	NULL AS "ApplicableRatingTimeHorizon",
	TO_DECIMAL(NULL, 15, 11) AS "AverageScalingFactorForSmallMidsizeEnterprise",
	'SimpleStandardizedApproach' AS "CalculationApproach",
	NULL AS "CalculationSubApproach",
	TO_DECIMAL(res_fact."CCF", 15, 11) AS "CreditConversionFactor",
	NULL AS "CreditConversionFactorClass",
	NULL AS "Currency",
	TO_DECIMAL(res_fact."ALLOCATED_EXPOSURE", 34, 6) AS "CurrentExposure",
	false AS "DeductionFromCapital",
	false AS "Defaulted",
	TO_DECIMAL(NULL, 34, 6) AS "ExpectedLoss",
	TO_DECIMAL(NULL, 34, 6) AS "ExposureAtDefault",
	TO_DECIMAL(NULL, 34, 6) AS "ExposureAtDefaultCalculationBase",
	TO_DECIMAL(NULL, 15, 11) AS "ExposureAtDefaultWeightedLossGivenDefault",
	NULL AS "ExposureClass",
	TO_DECIMAL(NULL, 15, 11) AS "HaircutCollateral",
	TO_DECIMAL(res_fact."EXPOSURE_HAIRCUT", 15, 11) AS "HaircutExposure",
	TO_DECIMAL(res_fact."FX_HAIRCUT", 15, 11) AS "HaircutFX",
	NULL AS "LGDClass",
	TO_DECIMAL(res_fact."LOSS_GIVEN_DEFAULT", 15, 11) AS "LossGivenDefault",
	TO_DECIMAL(res_fact."EFFECTIVE_MATURITY", 34 ,6) AS "Maturity",
	TO_DECIMAL(res_fact."MATURITY_MISMATCH", 15, 11) AS "MaturityMismatchFactor",
	NULL AS "MaturityPeriodTimeUnit",
	TO_DECIMAL(NULL, 34, 6) AS "MaximumExposure",
	TO_DECIMAL(NULL, 34, 6) AS "NominalAmount",
	NULL AS "OriginalExposureClass",
	 "com.adweko.adapter.osx.resultdata.common::get_SAPpercentStandard"(
		i_PercentValue => res_fact."PROB_OF_DEFAULT",
		i_SourceFieldName => 'PROB_OF_DEFAULT',
		i_TargetFieldName => 'ProbabilityOfDefault',
		i_SourceTableName => 'RES_FACT_CAD_CR_TRANCHE',
		i_Switch => kv."Value") AS "ProbabilityOfDefault",
	TO_DECIMAL(NULL, 34, 6) AS "ProportionalExposureAtDefault",
	TO_DECIMAL(NULL, 34, 6) AS "Quantity",
	TO_DECIMAL(NULL, 34, 6) AS "RegulatoryCapitalRequirements",
	TO_DECIMAL(NULL, 34, 6) AS "RegulatoryCapitalRequirementsForSecuritizationWithoutPenaltyFactor",
	TO_DECIMAL(res_fact."RISK_WEIGHT", 15, 11) AS "RiskWeight",
	TO_DECIMAL(NULL, 34, 6) AS "RiskWeightedAssets",
	TO_DECIMAL(res_fact."RISK_WEIGHT_BFR_CRM", 34, 6) AS "RiskWeightedAssetsBeforeCreditRiskMitigation",
	NULL AS "Unit",
	TO_DECIMAL(NULL, 34, 6) AS "UsedProtectionAmount",
	NULL AS "UsedProtectionAmountCurrency",
	NULL AS "SourceSystemID",
	NULL AS "ChangeTimestampInSourceSystem",
	NULL AS "ChangingUserInSourceSystem",
	NULL AS "ChangingProcessType",
	NULL AS "ChangingProcessID",
	NULL AS "_DynamicTimeBucket.MaturityBandID",
	NULL AS "_DynamicTimeBucket.TimeBucketID",
	'FunctionalCurrency' AS "P_0001210074_RoleOfCurrency",
	NULL AS "P_0001210074_DynamicTimeBucketStartDate",
	NULL AS "P_0001210074_DynamicTimeBucketEndDate",
	TO_DECIMAL(NULL, 34, 6) AS "P_0001210074_PayableAmount",
	TO_DECIMAL(NULL, 34, 6) AS "P_0001210074_Gain"
	
	FROM "com.adweko.adapter.osx.synonyms::RES_FACT_CAD_CR_TRANCHE" AS res_fact
	INNER JOIN "com.adweko.adapter.osx.synonyms::RES_SOLVE" AS slv 
	ON (res_fact."SLV_JOB_ID"	= slv."SLV_JOB_ID" 
	AND res_fact."MODEL_ID" 	= slv."MODEL_ID")
	INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_base_currency" (:I_SOLVE_JOB_ID, :I_MODEL_ID) AS cur
	ON (cur."SLV_JOB_ID"	= :I_SOLVE_JOB_ID
	AND cur."MODEL_ID"		= :I_MODEL_ID)
	LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_dynamic_whatif"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS wi
	ON (wi."RES_SOLVE_ID"	= slv."RES_SOLVE_ID"
	AND wi."WHATIF_ID"		= res_fact."DYN_WHATIF_ID")
	LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_static_scenario"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS sc
   	ON (sc."RES_SOLVE_ID" 		= slv."RES_SOLVE_ID"
	AND sc."STATIC_SCENARIO_ID"	= res_fact."STAT_SCEN_ID")
	LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(current_Date , current_timestamp)  AS kv
	ON  kv."KeyID" = 'SAPPercentageStandard'
	
	WHERE res_fact."SLV_JOB_ID"	= :I_SOLVE_JOB_ID
	AND res_fact."MODEL_ID" 	= :I_MODEL_ID;

END;