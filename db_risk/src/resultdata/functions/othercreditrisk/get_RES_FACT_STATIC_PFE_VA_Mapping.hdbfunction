FUNCTION "com.adweko.adapter.osx.othercreditrisk::get_RES_FACT_STATIC_PFE_VA_Mapping"(
	IN I_SOLVE_JOB_ID BIGINT,
	IN I_MODEL_ID BIGINT
	)
       RETURNS TABLE(
       		"RES_FACT_STATIC_PFE_ID" BIGINT,
			"CalculationMethod" NVARCHAR (100),
			"ConfidenceLevel" DECIMAL (15,11),
			"RiskProvisionScenario" NVARCHAR (100),
			"RoleOfCurrency" NVARCHAR (40),
			"_BusinessPartner.BusinessPartnerID" NVARCHAR (128),
			"_FinancialContract.FinancialContractID" NVARCHAR (128),
			"_FinancialContract.IDSystem" NVARCHAR (40),
			"_FinancialInstrument.FinancialInstrumentID" NVARCHAR (128),
			"_ResultGroup.ResultDataProvider" NVARCHAR (256),
			"_ResultGroup.ResultGroupID" NVARCHAR (128),
			"_RiskReportingNode.RiskReportingNodeID" NVARCHAR (128),
			"_SecuritiesAccount.FinancialContractID" NVARCHAR (128),
			"_SecuritiesAccount.IDSystem" NVARCHAR (40),
			"_TimeBucket.MaturityBandID" NVARCHAR (128),
			"_TimeBucket.TimeBucketID" NVARCHAR (128),
			"BusinessValidFrom" DATE ,
			"BusinessValidTo" DATE ,
			"BilateralCreditValuationAdjustmentAmount" DECIMAL (34,6),
			"CapitalValuationAdjustmentAmount" DECIMAL (34,6),
			"CollateralValuationAdjustmentAmount" DECIMAL (34,6),
			"CreditValuationAdjustmentAmount" DECIMAL (34,6),
			"Currency" NVARCHAR (3),
			"DebtValuationAdjustmentAmount" DECIMAL (34,6),
			"FundingCostAdjustmentAmount" DECIMAL (34,6),
			"FundingValuationAdjustmentAmount" DECIMAL (34,6),
			"MarginValuationAdjustmentAmount" DECIMAL (34,6),
			"OtherValuationAdjustmentAmount" DECIMAL (34,6),
			"TimeBucketEndDate" DATE ,
			"TimeBucketStartDate" DATE ,
			"SourceSystemID" NVARCHAR (128),
			"ChangeTimestampInSourceSystem" TIMESTAMP ,
			"ChangingUserInSourceSystem" NVARCHAR (128),
			"ChangingProcessType" NVARCHAR (40),
			"ChangingProcessID" NVARCHAR (128)
       ) 
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN
	SELECT
	"RES_FACT_STATIC_PFE_ID",
	"CalculationMethod",
	"ConfidenceLevel",
	"RiskProvisionScenario",
	"RoleOfCurrency",
	"_BusinessPartner.BusinessPartnerID",
	"_FinancialContract.FinancialContractID",
	"_FinancialContract.IDSystem",
	"_FinancialInstrument.FinancialInstrumentID",
	"_ResultGroup.ResultDataProvider",
	"_ResultGroup.ResultGroupID",
	"_RiskReportingNode.RiskReportingNodeID",
	"_SecuritiesAccount.FinancialContractID",
	"_SecuritiesAccount.IDSystem",
	"_TimeBucket.MaturityBandID",
	"_TimeBucket.TimeBucketID",
	"BusinessValidFrom",
	"BusinessValidTo",
	"BilateralCreditValuationAdjustmentAmount",
	"CapitalValuationAdjustmentAmount",
	"CollateralValuationAdjustmentAmount",
	"CreditValuationAdjustmentAmount",
	"Currency",
	"DebtValuationAdjustmentAmount",
	"FundingCostAdjustmentAmount",
	"FundingValuationAdjustmentAmount",
	"MarginValuationAdjustmentAmount",
	"OtherValuationAdjustmentAmount",
	"TimeBucketEndDate",
	"TimeBucketStartDate",
	"SourceSystemID",
	"ChangeTimestampInSourceSystem",
	"ChangingUserInSourceSystem",
	"ChangingProcessType",
	"ChangingProcessID"
	FROM(
		SELECT
		"RES_FACT_STATIC_PFE_ID",
		"CalculationMethod",
		"ConfidenceLevel",
		"RiskProvisionScenario",
		"RoleOfCurrency",
		"_BusinessPartner.BusinessPartnerID",
		"_FinancialContract.FinancialContractID",
		"_FinancialContract.IDSystem",
		"_FinancialInstrument.FinancialInstrumentID",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		-- pivot for ChartOfAccounts and ClassificationTree
		MAP(ser3."ELEMENT_NUMBER",
			1, "_RiskReportingNode.RiskReportingNodeID",
			2, "Classification01_RiskReportingNode.RiskReportingNodeID",
			3, "Classification02_RiskReportingNode.RiskReportingNodeID",
			4, "Classification03_RiskReportingNode.RiskReportingNodeID",
			5, "Classification04_RiskReportingNode.RiskReportingNodeID",
			6, "Classification05_RiskReportingNode.RiskReportingNodeID",
			7, "Classification06_RiskReportingNode.RiskReportingNodeID",
			8, "Classification07_RiskReportingNode.RiskReportingNodeID",
			9, "Classification08_RiskReportingNode.RiskReportingNodeID",
			10, "Classification09_RiskReportingNode.RiskReportingNodeID",
			11, "Classification10_RiskReportingNode.RiskReportingNodeID",
			12, "Classification11_RiskReportingNode.RiskReportingNodeID",
			13, "Classification12_RiskReportingNode.RiskReportingNodeID",
			14, "Classification13_RiskReportingNode.RiskReportingNodeID",
			15, "Classification14_RiskReportingNode.RiskReportingNodeID",
			16, "Classification15_RiskReportingNode.RiskReportingNodeID")
		AS "_RiskReportingNode.RiskReportingNodeID",
		"_SecuritiesAccount.FinancialContractID",
		"_SecuritiesAccount.IDSystem",
		"_TimeBucket.MaturityBandID",
		"_TimeBucket.TimeBucketID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"BilateralCreditValuationAdjustmentAmount",
		"CapitalValuationAdjustmentAmount",
		"CollateralValuationAdjustmentAmount",
		"CreditValuationAdjustmentAmount",
		"Currency",
		"DebtValuationAdjustmentAmount",
		"FundingCostAdjustmentAmount",
		"FundingValuationAdjustmentAmount",
		"MarginValuationAdjustmentAmount",
		"OtherValuationAdjustmentAmount",
		"TimeBucketEndDate",
		"TimeBucketStartDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID"
	FROM(
		SELECT
		"RES_FACT_STATIC_PFE_ID",
		"CalculationMethod",
		"ConfidenceLevel",
		"RiskProvisionScenario",
		"RoleOfCurrency",
		"_BusinessPartner.BusinessPartnerID",
		"_FinancialContract.FinancialContractID",
		"_FinancialContract.IDSystem",
		"_FinancialInstrument.FinancialInstrumentID",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		"_RiskReportingNode.RiskReportingNodeID",
		"Classification01_RiskReportingNode.RiskReportingNodeID",
		"Classification03_RiskReportingNode.RiskReportingNodeID",
		"Classification02_RiskReportingNode.RiskReportingNodeID",
		"Classification04_RiskReportingNode.RiskReportingNodeID",
		"Classification05_RiskReportingNode.RiskReportingNodeID",
		"Classification06_RiskReportingNode.RiskReportingNodeID",
		"Classification07_RiskReportingNode.RiskReportingNodeID",
		"Classification08_RiskReportingNode.RiskReportingNodeID",
		"Classification09_RiskReportingNode.RiskReportingNodeID",
		"Classification10_RiskReportingNode.RiskReportingNodeID",
		"Classification11_RiskReportingNode.RiskReportingNodeID",
		"Classification12_RiskReportingNode.RiskReportingNodeID",
		"Classification13_RiskReportingNode.RiskReportingNodeID",
		"Classification14_RiskReportingNode.RiskReportingNodeID",
		"Classification15_RiskReportingNode.RiskReportingNodeID",
		"_SecuritiesAccount.FinancialContractID",
		"_SecuritiesAccount.IDSystem",
		"_TimeBucket.MaturityBandID",
		"_TimeBucket.TimeBucketID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"BilateralCreditValuationAdjustmentAmount",
		"CapitalValuationAdjustmentAmount",
		"CollateralValuationAdjustmentAmount",
		"CreditValuationAdjustmentAmount",
		"Currency",
		"DebtValuationAdjustmentAmount",
		"FundingCostAdjustmentAmount",
		"FundingValuationAdjustmentAmount",
		"MarginValuationAdjustmentAmount",
		"OtherValuationAdjustmentAmount",
		"TimeBucketEndDate",
		"TimeBucketStartDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID"
	FROM(
		SELECT
	
		res_fact."RES_FACT_STATIC_PFE_ID" AS "RES_FACT_STATIC_PFE_ID",
		MAP(ser."ELEMENT_NUMBER",
				1, 'Regulatory',
				2, 'NonRegulatory'
				)  AS "CalculationMethod",
		"com.adweko.adapter.osx.resultdata.common::get_SAPpercentStandard"(
			i_PercentValue => res_fact."CONF_LEVEL_NAME",
			i_SourceFieldName => 'CONF_LEVEL_NAME',
			i_TargetFieldName => 'ConfidenceLevel',
			i_SourceTableName => 'RES_FACT_STATIC_PFE',
			i_Switch => kv_perc."Value") AS "ConfidenceLevel",
		sc."STATIC_SCENARIO_NAME" AS "RiskProvisionScenario",
		'FunctionalCurrency' AS "RoleOfCurrency",
		CASE
			WHEN kv."Value" = 'BusinessPartner' 
				THEN CAST(SUBSTR_AFTER(res_fact."CT_GROUPING_NODE_NAME", '/') AS NVARCHAR(128))
			ELSE NULL
		END AS "_BusinessPartner.BusinessPartnerID",
		CASE
			WHEN kv."Value" = 'Contract' AND LEFT(IFNULL(SUBSTR_AFTER(res_fact."CT_GROUPING_NODE_NAME", '/'),''), 4) <> 'I-_-'
				THEN CAST("com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(SUBSTR_AFTER(res_fact."CT_GROUPING_NODE_NAME", '/')) AS NVARCHAR(128))
			ELSE NULL
		END AS "_FinancialContract.FinancialContractID",
		CASE
			WHEN kv."Value" = 'Contract' AND LEFT(SUBSTR_AFTER(res_fact."CT_GROUPING_NODE_NAME", '/'), 4) <> 'I-_-'
				THEN CAST("com.adweko.adapter.osx.resultdata.common::get_IDSystem"(SUBSTR_AFTER(res_fact."CT_GROUPING_NODE_NAME", '/')) AS NVARCHAR(128))
			ELSE NULL
		END AS "_FinancialContract.IDSystem",
		CASE
			WHEN kv."Value" = 'Contract' AND LEFT(SUBSTR_AFTER(res_fact."CT_GROUPING_NODE_NAME", '/'), 4) = 'I-_-'
				THEN CAST("com.adweko.adapter.osx.resultdata.common::get_FinancialInstrumentID"(SUBSTR_AFTER(res_fact."CT_GROUPING_NODE_NAME", '/')) AS NVARCHAR(128))
			ELSE NULL
		END AS "_FinancialInstrument.FinancialInstrumentID",
		CAST("com.adweko.adapter.osx.resultdata.common::get_ResultProvider"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS NVARCHAR(128)) AS "_ResultGroup.ResultDataProvider",
		"com.adweko.adapter.osx.resultdata.common::get_ResulGroupId"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS "_ResultGroup.ResultGroupID",
		CAST(res_fact."CT_GROUPING_NODE_NAME" AS NVARCHAR(128)) AS "_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_01") AS "Classification01_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_02") AS "Classification02_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_03") AS "Classification03_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_04") AS "Classification04_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_05") AS "Classification05_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_06") AS "Classification06_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_07") AS "Classification07_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_08") AS "Classification08_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_09") AS "Classification09_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_10") AS "Classification10_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_11") AS "Classification11_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_12") AS "Classification12_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_13") AS "Classification13_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_14") AS "Classification14_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_15") AS "Classification15_RiskReportingNode.RiskReportingNodeID",
		CASE
			WHEN kv."Value" = 'Contract' AND LEFT(SUBSTR_AFTER(res_fact."CT_GROUPING_NODE_NAME", '/'), 4) = 'I-_-'
				THEN CAST("com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(SUBSTR_AFTER(res_fact."CT_GROUPING_NODE_NAME", '/')) AS NVARCHAR(128))
			ELSE NULL
		END AS "_SecuritiesAccount.FinancialContractID",
		CASE
			WHEN kv."Value" = 'Contract' AND LEFT(SUBSTR_AFTER(res_fact."CT_GROUPING_NODE_NAME", '/'), 4) = 'I-_-'
				THEN CAST("com.adweko.adapter.osx.resultdata.common::get_IDSystem"(SUBSTR_AFTER(res_fact."CT_GROUPING_NODE_NAME", '/')) AS NVARCHAR(128))
			ELSE NULL
		END AS "_SecuritiesAccount.IDSystem",
		tbs."_TimeBucket.MaturityBandID" AS "_TimeBucket.MaturityBandID",
		tbs."_TimeBucket.TimeBucketID" AS "_TimeBucket.TimeBucketID",
		"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(slv."ANALYSIS_DATE") AS "BusinessValidFrom",
		"com.adweko.adapter.osx.resultdata.common::get_MaxDate"() AS "BusinessValidTo",
		NULL AS "BilateralCreditValuationAdjustmentAmount",
		NULL AS "CapitalValuationAdjustmentAmount",
		NULL AS "CollateralValuationAdjustmentAmount",
		MAP(ser."ELEMENT_NUMBER",
				1, TO_DECIMAL(res_fact."CVA_BAS_CUR", 34, 6),
				2, TO_DECIMAL(res_fact."CVA_BAS_CUR", 34, 6)
				) AS "CreditValuationAdjustmentAmount",
		cur."CURRENCY_NAME" AS "Currency",
		MAP(ser."ELEMENT_NUMBER",
				1, TO_DECIMAL(res_fact."ACCOUNTING_CVA_BAS_CUR", 34, 6),
				2, TO_DECIMAL(res_fact."ACCOUNTING_DVA_BAS_CUR", 34, 6)
				) AS "DebtValuationAdjustmentAmount",
		NULL AS "FundingCostAdjustmentAmount",
		NULL AS "FundingValuationAdjustmentAmount",
		NULL AS "MarginValuationAdjustmentAmount",
		NULL AS "OtherValuationAdjustmentAmount",
		tbs."BUCKET_END"  AS "TimeBucketEndDate",
		tbs."BUCKET_START" AS "TimeBucketStartDate",
		NULL AS "SourceSystemID",
		NULL AS "ChangeTimestampInSourceSystem",
		NULL AS "ChangingUserInSourceSystem",
		NULL AS "ChangingProcessType",
		NULL AS "ChangingProcessID"
		
		FROM "com.adweko.adapter.osx.synonyms::RES_FACT_STATIC_PFE" AS res_fact
		INNER JOIN "com.adweko.adapter.osx.synonyms::RES_SOLVE" AS slv
		ON res_fact."SLV_JOB_ID"	= slv."SLV_JOB_ID" 
		AND res_fact."MODEL_ID"		= slv."MODEL_ID"
		INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_static_scenario"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS sc
		ON sc."RES_SOLVE_ID"		= slv."RES_SOLVE_ID"
	  	AND sc."STATIC_SCENARIO_ID" = res_fact."STAT_SCEN_ID"
	   INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_static_tbs"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS tbs 
		ON (tbs."BUCKET_NB" = res_fact."PFE_GAP_TB"
	  	AND tbs."RES_DIM_TBS_KIND"='POTF')
	  	INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_base_currency"(:I_SOLVE_JOB_ID, :I_MODEL_ID ) AS cur
		ON cur."SLV_JOB_ID" = :I_SOLVE_JOB_ID
		AND cur."MODEL_ID"	= :I_MODEL_ID
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(current_date, current_timestamp)  AS kv
		ON  kv."KeyID" = 'PotentialFutureExposureGroupingNode'
		LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_generate_multiple_rows"(1, 1, 3) AS ser
		ON true = true
		LEFT OUTER JOIN "com.adweko.adapter.osx.mappings::map_keyValue_View"(current_Date , current_timestamp)  AS kv_perc
		ON  kv_perc."KeyID" = 'SAPPercentageStandard'
		
		WHERE res_fact."SLV_JOB_ID"	= :I_SOLVE_JOB_ID
		AND res_fact."MODEL_ID" 	= :I_MODEL_ID))
	   	-- pivot for ChartOfAccounts and ClassificationTree
		LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_generate_multiple_rows"(1, 1, 17) AS ser3
		ON true = true)
			
		WHERE "_RiskReportingNode.RiskReportingNodeID" IS NOT NULL;

END;