FUNCTION "com.adweko.adapter.osx.othercreditrisk::get_RES_FACT_STATIC_ECL_Mapping"( 
	IN I_SOLVE_JOB_ID BIGINT,
	IN I_MODEL_ID BIGINT
	)
       RETURNS TABLE(
       	"RES_FACT_STATIC_ECL_ID" BIGINT,
		"CalculationMethod" NVARCHAR (100),
		"RiskProvisionScenario" NVARCHAR (100),
		"RoleOfCurrency" NVARCHAR (40),
		"_DynamicTimeBucket.MaturityBandID" NVARCHAR (128),
		"_DynamicTimeBucket.TimeBucketID" NVARCHAR (128),
		"_FinancialContract.FinancialContractID" NVARCHAR (128),
		"_FinancialContract.IDSystem" NVARCHAR (40),
		"_FinancialInstrument.FinancialInstrumentID" NVARCHAR (128),
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" NVARCHAR(3),
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" NVARCHAR(128),
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" NVARCHAR(40),
		"_ResultGroup.ResultDataProvider" NVARCHAR (256),
		"_ResultGroup.ResultGroupID" NVARCHAR (128),
		"_RiskReportingNode.RiskReportingNodeID" NVARCHAR (128),
		"_TimeBucket.MaturityBandID" NVARCHAR (128),
		"_TimeBucket.TimeBucketID" NVARCHAR (128),
		"BusinessValidFrom" DATE ,
		"BusinessValidTo" DATE ,
		"Currency" NVARCHAR (3),
		"CurrentExposureAmount" DECIMAL (34,6),
		"DynamicTimeBucketEndDate" DATE,
		"DynamicTimeBucketStartDate" DATE,
		"EffectiveExpectedPositiveExposureAmount" DECIMAL (34,6),
		"ExpectedCreditGainOrLossAmount" DECIMAL (34,6),
		"ExpectedPositiveExposureAmount" DECIMAL (34,6),
		"LostCashFlowsAmount" DECIMAL (34,6),
		"PastDueAmount" DECIMAL (34,6),
		"RecoveredAmount" DECIMAL (34,6),
		"TimeBucketEndDate" DATE ,
		"TimeBucketStartDate" DATE ,
		"SourceSystemID" NVARCHAR (128),
		"ChangeTimestampInSourceSystem" TIMESTAMP ,
		"ChangingUserInSourceSystem" NVARCHAR (128),
		"ChangingProcessType" NVARCHAR (40),
		"ChangingProcessID" NVARCHAR (128)
       ) 
       LANGUAGE SQLSCRIPT 
       SQL SECURITY INVOKER AS 
BEGIN 
    /*****************************
        Write your function logic
    ****************************/
RETURN
	SELECT
	"RES_FACT_STATIC_ECL_ID",
	"CalculationMethod",
	"RiskProvisionScenario",
	"RoleOfCurrency",
	"_DynamicTimeBucket.MaturityBandID",
	"_DynamicTimeBucket.TimeBucketID",
	"_FinancialContract.FinancialContractID",
	"_FinancialContract.IDSystem",
	"_FinancialInstrument.FinancialInstrumentID",
	"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
	"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
	"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
	"_ResultGroup.ResultDataProvider",
	"_ResultGroup.ResultGroupID",
	"_RiskReportingNode.RiskReportingNodeID",
	"_TimeBucket.MaturityBandID",
	"_TimeBucket.TimeBucketID",
	"BusinessValidFrom",
	"BusinessValidTo",
	"Currency",
	"CurrentExposureAmount",
	"DynamicTimeBucketEndDate",
	"DynamicTimeBucketStartDate",
	"EffectiveExpectedPositiveExposureAmount",
	"ExpectedCreditGainOrLossAmount",
	"ExpectedPositiveExposureAmount",
	"LostCashFlowsAmount",
	"PastDueAmount",
	"RecoveredAmount",
	"TimeBucketEndDate",
	"TimeBucketStartDate",
	"SourceSystemID",
	"ChangeTimestampInSourceSystem",
	"ChangingUserInSourceSystem",
	"ChangingProcessType",
	"ChangingProcessID"

	FROM(
		SELECT
		"RES_FACT_STATIC_ECL_ID",
		"CalculationMethod",
		"RiskProvisionScenario",
		"RoleOfCurrency",
		"_DynamicTimeBucket.MaturityBandID",
		"_DynamicTimeBucket.TimeBucketID",
		"_FinancialContract.FinancialContractID",
		"_FinancialContract.IDSystem",
		"_FinancialInstrument.FinancialInstrumentID",
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		-- pivot for ChartOfAccounts and ClassificationTree
		MAP(ser3."ELEMENT_NUMBER",
			1, "Account_RiskReportingNode.RiskReportingNodeID",
			2, "Classification01_RiskReportingNode.RiskReportingNodeID",
			3, "Classification02_RiskReportingNode.RiskReportingNodeID",
			4, "Classification03_RiskReportingNode.RiskReportingNodeID",
			5, "Classification04_RiskReportingNode.RiskReportingNodeID",
			6, "Classification05_RiskReportingNode.RiskReportingNodeID",
			7, "Classification06_RiskReportingNode.RiskReportingNodeID",
			8, "Classification07_RiskReportingNode.RiskReportingNodeID",
			9, "Classification08_RiskReportingNode.RiskReportingNodeID",
			10, "Classification09_RiskReportingNode.RiskReportingNodeID",
			11, "Classification10_RiskReportingNode.RiskReportingNodeID",
			12, "Classification11_RiskReportingNode.RiskReportingNodeID",
			13, "Classification12_RiskReportingNode.RiskReportingNodeID",
			14, "Classification13_RiskReportingNode.RiskReportingNodeID",
			15, "Classification14_RiskReportingNode.RiskReportingNodeID",
			16, "Classification15_RiskReportingNode.RiskReportingNodeID")
		AS "_RiskReportingNode.RiskReportingNodeID",
		"_TimeBucket.MaturityBandID",
		"_TimeBucket.TimeBucketID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"Currency",
		"CurrentExposureAmount",
		"DynamicTimeBucketEndDate",
		"DynamicTimeBucketStartDate",
		"EffectiveExpectedPositiveExposureAmount",
		"ExpectedCreditGainOrLossAmount",
		"ExpectedPositiveExposureAmount",
		"LostCashFlowsAmount",
		"PastDueAmount",
		"RecoveredAmount",
		"TimeBucketEndDate",
		"TimeBucketStartDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID"

	FROM(
		SELECT
		"RES_FACT_STATIC_ECL_ID",
		"CalculationMethod",
		"RiskProvisionScenario",
		"RoleOfCurrency",
		"_DynamicTimeBucket.MaturityBandID",
		"_DynamicTimeBucket.TimeBucketID",
		"_FinancialContract.FinancialContractID",
		"_FinancialContract.IDSystem",
		"_FinancialInstrument.FinancialInstrumentID",
		"_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
		"_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
		"_ResultGroup.ResultDataProvider",
		"_ResultGroup.ResultGroupID",
		"Account_RiskReportingNode.RiskReportingNodeID",
		"Classification01_RiskReportingNode.RiskReportingNodeID",
		"Classification03_RiskReportingNode.RiskReportingNodeID",
		"Classification02_RiskReportingNode.RiskReportingNodeID",
		"Classification04_RiskReportingNode.RiskReportingNodeID",
		"Classification05_RiskReportingNode.RiskReportingNodeID",
		"Classification06_RiskReportingNode.RiskReportingNodeID",
		"Classification07_RiskReportingNode.RiskReportingNodeID",
		"Classification08_RiskReportingNode.RiskReportingNodeID",
		"Classification09_RiskReportingNode.RiskReportingNodeID",
		"Classification10_RiskReportingNode.RiskReportingNodeID",
		"Classification11_RiskReportingNode.RiskReportingNodeID",
		"Classification12_RiskReportingNode.RiskReportingNodeID",
		"Classification13_RiskReportingNode.RiskReportingNodeID",
		"Classification14_RiskReportingNode.RiskReportingNodeID",
		"Classification15_RiskReportingNode.RiskReportingNodeID",
		"_TimeBucket.MaturityBandID",
		"_TimeBucket.TimeBucketID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"Currency",
		"CurrentExposureAmount",
		"DynamicTimeBucketEndDate",
		"DynamicTimeBucketStartDate",
		"EffectiveExpectedPositiveExposureAmount",
		"ExpectedCreditGainOrLossAmount",
		"ExpectedPositiveExposureAmount",
		"LostCashFlowsAmount",
		"PastDueAmount",
		"RecoveredAmount",
		"TimeBucketEndDate",
		"TimeBucketStartDate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID"

	FROM(
		SELECT
		res_fact."RES_FACT_STATIC_ECL_ID" AS "RES_FACT_STATIC_ECL_ID",
		CASE
			WHEN res_fact."REF_ECL_CALC_HORIZON_DIM_ID" = 1
				THEN 'One Year'
			WHEN res_fact."REF_ECL_CALC_HORIZON_DIM_ID" = 2
				THEN 'Lifetime'
			ELSE NULL
		END AS "CalculationMethod",
		sc."STATIC_SCENARIO_NAME" AS "RiskProvisionScenario",
		MAP(ser."ELEMENT_NUMBER",
			1, 'FunctionalCurrency',
			2, 'ContractCurrency'
			) AS "RoleOfCurrency",
		NULL AS	"_DynamicTimeBucket.MaturityBandID",
		NULL AS "_DynamicTimeBucket.TimeBucketID",
		CASE
			WHEN LEFT(IFNULL(res_fact."CONTRACT_NAME",''), 4) <> 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_STATIC_ECL_ID" AS NVARCHAR (128))) 
			ELSE NULL
		END AS "_FinancialContract.FinancialContractID",
		CASE
			WHEN LEFT(IFNULL(res_fact."CONTRACT_NAME",''), 4) <> 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME") 
			ELSE NULL
		END AS "_FinancialContract.IDSystem",
		CASE
			WHEN LEFT(IFNULL(res_fact."CONTRACT_NAME",''), 4) = 'I-_-'
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialInstrumentID"(res_fact."CONTRACT_NAME") 
			ELSE NULL	
		END AS "_FinancialInstrument.FinancialInstrumentID",
		"com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."CONTRACT_NAME") AS "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
		CASE
			WHEN "com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."CONTRACT_NAME") IS NOT NULL
				THEN "com.adweko.adapter.osx.resultdata.common::get_FinancialContractID"(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_STATIC_ECL_ID" AS NVARCHAR (128)))
				ELSE NULL
		END AS "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
		CASE
			WHEN "com.adweko.adapter.osx.resultdata.common::get_PositionCurrency"(res_fact."CONTRACT_NAME") IS NOT NULL
				THEN "com.adweko.adapter.osx.resultdata.common::get_IDSystem"(res_fact."CONTRACT_NAME")
			ELSE NULL
		END AS "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
		CAST("com.adweko.adapter.osx.resultdata.common::get_ResultProvider"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS NVARCHAR(128)) AS "_ResultGroup.ResultDataProvider",
		"com.adweko.adapter.osx.resultdata.common::get_ResulGroupId"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS "_ResultGroup.ResultGroupID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."ACCOUNT_ID") AS "Account_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_01") AS "Classification01_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_02") AS "Classification02_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_03") AS "Classification03_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_04") AS "Classification04_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_05") AS "Classification05_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_06") AS "Classification06_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_07") AS "Classification07_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_08") AS "Classification08_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_09") AS "Classification09_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_10") AS "Classification10_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_11") AS "Classification11_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_12") AS "Classification12_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_13") AS "Classification13_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_14") AS "Classification14_RiskReportingNode.RiskReportingNodeID",
		"com.adweko.adapter.osx.resultdata::get_RiskReportingNodeId"(:I_SOLVE_JOB_ID, :I_MODEL_ID, res_fact."UDB_CLASSIFICATION_15") AS "Classification15_RiskReportingNode.RiskReportingNodeID",
		NULL AS "_TimeBucket.MaturityBandID",
		NULL AS "_TimeBucket.TimeBucketID",
		"com.adweko.adapter.osx.resultdata.common::get_convertJulianDateIntoStandardDate"(slv."ANALYSIS_DATE") AS "BusinessValidFrom",
		"com.adweko.adapter.osx.resultdata.common::get_MaxDate"() AS "BusinessValidTo",
		MAP(ser."ELEMENT_NUMBER",
			1, basecur."CURRENCY_NAME",
			2, contcur."CURRENCY_NAME"
			) AS "Currency",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."ECL_CURRENT_EXPO_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."ECL_CURRENT_EXPO_CT_CUR", 34, 6)
			) AS "CurrentExposureAmount",
		NULL AS "DynamicTimeBucketEndDate",
		NULL AS "DynamicTimeBucketStartDate",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."EFF_EXP_POSITIVE_EXPO_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."EFF_EXP_POSITIVE_EXPO_CT_CUR", 34, 6)
			) AS "EffectiveExpectedPositiveExposureAmount",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."EXP_CR_GAIN_LOSS_BAS_CUR",34, 6),
			2, TO_DECIMAL(res_fact."EXP_CR_GAIN_LOSS_CT_CUR", 34, 6)
			) AS "ExpectedCreditGainOrLossAmount",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."EXP_POSITIVE_EXPO_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."EXP_POSITIVE_EXPO_CT_CUR", 34, 6)
			) AS "ExpectedPositiveExposureAmount",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."LOST_CASH_FLOWS_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."LOST_CASH_FLOWS_CT_CUR", 34, 6)
			) AS "LostCashFlowsAmount",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."PAST_DUE_AMOUNT_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."PAST_DUE_AMOUNT_CT_CUR", 34, 6)
			) AS "PastDueAmount",
		MAP(ser."ELEMENT_NUMBER",
			1, TO_DECIMAL(res_fact."RECOVERED_AMOUNT_BAS_CUR", 34, 6),
			2, TO_DECIMAL(res_fact."RECOVERED_AMOUNT_CT_CUR", 34, 6)
			) AS "RecoveredAmount",
		NULL AS "TimeBucketEndDate",
		NULL AS "TimeBucketStartDate",
		NULL AS "SourceSystemID",
		NULL AS "ChangeTimestampInSourceSystem",
		NULL AS "ChangingUserInSourceSystem",
		NULL AS "ChangingProcessType",
		NULL AS "ChangingProcessID",
		ROW_NUMBER() OVER (PARTITION BY COALESCE(res_fact."CONTRACT_NAME",CAST(res_fact."RES_FACT_STATIC_ECL_ID" AS NVARCHAR (255))),
										res_fact."STAT_SCEN_ID",
										res_fact."REF_ECL_CALC_HORIZON_DIM_ID",
										ser."ELEMENT_NUMBER") AS "Row_Num",
		-- Stores information in which currency types (base, contract, event, account) the results where calculated.
		-- Is needed to filter out useless rows at the end of the mapping process.
		CASE
			WHEN res_fact."CONTRACT_CURRENCY_ID" IS NOT NULL
				THEN 1
			ELSE 4
		END AS "UsedCurrencyTypes"
		
		FROM "com.adweko.adapter.osx.synonyms::RES_FACT_STATIC_ECL" AS res_fact
		INNER JOIN "com.adweko.adapter.osx.synonyms::RES_DIM_ACCOUNT" AS acc
  		ON  res_fact."ACCOUNT_ID"	= acc."ACCOUNT_ID" 
  		AND res_fact."SLV_JOB_ID"	= acc."SLV_JOB_ID"
		AND acc."IS_VISIBLE"		= true
		INNER JOIN "com.adweko.adapter.osx.synonyms::RES_SOLVE" AS slv
		ON res_fact."SLV_JOB_ID"	= slv."SLV_JOB_ID" 
		AND res_fact."MODEL_ID"		= slv."MODEL_ID"
		INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_static_scenario"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS sc
		ON sc."RES_SOLVE_ID"		= slv."RES_SOLVE_ID"
   		AND sc."STATIC_SCENARIO_ID" = res_fact."STAT_SCEN_ID"
		INNER JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_base_currency"(:I_SOLVE_JOB_ID, :I_MODEL_ID ) AS basecur
		ON basecur."SLV_JOB_ID" = :I_SOLVE_JOB_ID
		AND basecur."MODEL_ID"	= :I_MODEL_ID
		LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_dimension_ct_ev_currency"(:I_SOLVE_JOB_ID, :I_MODEL_ID) AS contcur
		ON contcur."SLV_JOB_ID" 	= :I_SOLVE_JOB_ID
		AND contcur."SLV_JOB_ID"	= :I_SOLVE_JOB_ID
		AND contcur."CURRENCY_ID"	= res_fact."CONTRACT_CURRENCY_ID"
		LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_generate_multiple_rows"(1, 1, 3) AS ser
		ON true = true
		
		WHERE res_fact."SLV_JOB_ID"	= :I_SOLVE_JOB_ID
		AND res_fact."MODEL_ID" 	= :I_MODEL_ID)
		
	WHERE 
		-- filter out useless rows (rows without filled measures) and duplicated rows
		("Row_Num" = 1 AND 
		("UsedCurrencyTypes" = 1 
		AND "RoleOfCurrency" IN ('FunctionalCurrency','ContractCurrency')))
		OR
		("Row_Num" = 1 AND
		("UsedCurrencyTypes" = 4
		AND "RoleOfCurrency" = 'FunctionalCurrency')))
	-- pivot for ChartOfAccounts and ClassificationTree
	LEFT JOIN "com.adweko.adapter.osx.resultdata.common::get_generate_multiple_rows"(1, 1, 17) AS ser3
	ON true = true)
		
	WHERE "_RiskReportingNode.RiskReportingNodeID" IS NOT NULL;


END;